function search_by_datatable(table_id, url, data, column, remove_icon) {

    if (remove_icon) {
        var table = $('#' + table_id).DataTable();
        table.destroy();
        $('#' + table_id).DataTable({
            "processing": true,
            "serverSide": true,
            "lengthChange": true,
            "searching": true,
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': remove_icon
            }],
            "ajax": {
                url: url,
                dataType: "json",
                type: "POST",
                data: data
            },
            "columns": column
        });
    } else {
        var table = $('#' + table_id).DataTable();
        table.destroy();

        $('#' + table_id).DataTable({
            "processing": true,
            "serverSide": true,
            "searching": false,
            "lengthChange": false,
            "ajax": {
                url: url,
                dataType: "json",
                type: "POST",
                data: data
            },
            "columns": column
        });
    }



}

function error_message(msg, url = false) {

    iziToast.error({
        title: 'Error',
        message: msg,
        position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        onOpening: function () {   $('.form-post-btn').prop('disabled', false); },
        onOpened: function () {
            if (url) {
                window.location.href = url;
            }

        },
        onClosing: function () { $('.form-post-btn').prop('disabled', false); },
        onClosed: function () { $('.form-post-btn').prop('disabled', false); }
    });


}

function success_message(msg, status = 0, url = "") {

    iziToast.success({
        title: 'Success',
        message: msg,
        position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        zindex: 99999999999999999999,
        onOpening: function () {
            $('.form-post-btn').prop('disabled', false);
        },
        onOpened: function () {
            if (status == 0)
            {
                window.location.reload();
            } else if(status == 1) {
                window.location.href = url;
            }
            },
        onClosing: function () { $('.form-post-btn').prop('disabled', false); },
        onClosed: function () { $('.form-post-btn').prop('disabled', false); }
    });

}


function add_update_details(form_id, url)
{
  /*  $('.form-post-btn').prop('disabled', true);*/
    var data = $('#' + form_id).serialize();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data)
        {

            if (data.status == 400)
            {
                error_message(data.msg,"");
            }
            if (data.status == 401)
            {
                error_message(data.msg,  data.url);
            }
            if (data.status == 200)
            {
                success_message(data.msg);
            }
            if (data.status == 201)
            {
                var r_url = data.url;
                success_message(data.msg, 1, r_url);
            }
        }
    });
}


function add_update_image_details(form_id, url)
{
    var data = new FormData(document.getElementById(form_id));

    $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        dataType: 'JSON',
        data: data,
        success: function (data)
        {

            if (data.status == 400)
            {
                error_message(data.msg,"");
            }
            if (data.status == 401)
            {
                error_message(data.msg,  data.url);
            }
            if (data.status == 200)
            {
                success_message(data.msg);
            }
            if (data.status == 201)
            {
                var r_url = data.url;
                success_message(data.msg, 1, r_url);
            }
        }
    });
}

