<?php  

/* 
|--------------------------------------------------------
| SEND SMS API (SMSRank, Nexmo, Twilio, BudgetSMS, Infobip)
| @author : Md. Tuhin Sarker
| @email  : <tuhinsorker92@gmail.com>
| @created at: 11 May 2018
|--------------------------------------------------------
| $this->load->library('sms_lib');
| $this->sms_lib->send(array(
|     'to'       => +8801746406801, 
|     'template' => 'Hello %x%', 
|     'template_config' => array('x'=>'Mr. X'), 
| ));
|--------------------------------------------------------
*/
 
class Sms_lib
{


    private function _template($template = null, $data = array())
    {
    
        $newStr = $template;

        foreach ($data as $key => $value) {
            $newStr = str_replace("%$key%", $value, $newStr);
        } 
        return $newStr; 

         
    }

 
}
 



 

