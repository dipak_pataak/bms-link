<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
| __________________________________________________________________
|                CODEIGNITER UPS SHIPPING LIBRARY
| __________________________________________________________________
|				developed by Md. Tuhin Sarker
| 				email <tuhinsorker92@gmail.com>
| __________________________________________________________________
| HOW TO USE
| An example is given below  
| __________________________________________________________________
|
| examples
| $this->load->library('Upsshipping');
| __________________________________________________________________
*/ 

class Upsshipping {

	private $CI;
	private $fields = array();


	public function __construct() {

		// Load config
        $this->CI =& get_instance();
        // Load config
        //$this->CI->load->config('ups');
    }



	public function addField($field, $value) {
		$this->fields[$field] = $value;
	}


	public function processShipAccept($shiperInfo) {

		try {

			$shipmentData = $this->getProcessShipAccept($shiperInfo);

			$shipmentData = json_encode( $shipmentData );

			$url = ($shiperInfo['mode'] == 'test'?'https://wwwcie.ups.com/rest/Ship':'https://onlinetools.ups.com/rest/Ship');

			/* Curl start to call UPS shipping API */
			$ch = curl_init($url);//test url

			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $shipmentData);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			
			if ( !($res = curl_exec($ch)) ) {
				die(date('[Y-m-d H:i e] '). "Got " . curl_error($ch) . " when processing data");
				curl_close($ch);
				exit;
			}
			curl_close($ch);
			/* Curl End */
			
			if( is_string( $res ) ) {
				$resObject = json_decode( $res );
			}
			if( isset( $resObject->Fault ) && !empty( $resObject->Fault ) ) {
				return array( $res, 403 );
			} else if( isset( $resObject->ShipmentResponse ) && !empty( $resObject->ShipmentResponse ) ) {
				return array( $res, 200 );
			}
		}
		catch(Exception $ex) {
			return array( $ex, 403 );
		}
	}

	public function getProcessShipAccept($shiperInfo) {
		// echo "<pre>";
		// print_r($shiperInfo);exit;

		$userNameToken['Username'] = $shiperInfo['username'];
		$userNameToken['Password'] = $shiperInfo['password'];
		$UPSSecurity['UsernameToken'] = $userNameToken;
		$accessLicenseNumber['AccessLicenseNumber'] = $shiperInfo['access_token'];
		$UPSSecurity['ServiceAccessToken'] = $accessLicenseNumber;
		$request['UPSSecurity'] = $UPSSecurity;

		/* Important */
		$requestoption['RequestOption'] = 'nonvalidate';
		$request['ShipmentRequest']['Request'] = $requestoption;

		/* Important */
		$shipment['Description'] = $shiperInfo['description'];
		$shipper['Name'] = $shiperInfo['name'];
		$shipper['AttentionName'] = $shiperInfo['attentionname'];
		$shipper['ShipperNumber'] = $shiperInfo['account_id'];
		$address['AddressLine'] = $shiperInfo['address'];
		$address['City'] = $shiperInfo['city'];
		$address['StateProvinceCode'] = $shiperInfo['state'];
		$address['PostalCode'] = $shiperInfo['zip_code'];
		$address['CountryCode'] = $shiperInfo['country_code'];
		$shipper['Address'] = $address;
		$phone['Number'] = $shiperInfo['phone'];
		$shipper['Phone'] = $phone;
		$shipment['Shipper'] = $shipper;

		/* Important */
		/* Shipping to Customer Address */		
		$shipto['Name'] = $this->fields['ShipTo_Name'];
		$shipto['AttentionName'] = $this->fields['ShipTo_Name'];
		$addressTo['AddressLine'] = $this->fields['ShipTo_AddressLine'];
		$addressTo['City'] = $this->fields['ShipTo_City'];
		$addressTo['StateProvinceCode'] = $this->fields['ShipTo_StateProvinceCode'];
		$addressTo['PostalCode'] = $this->fields['ShipTo_PostalCode'];
		$addressTo['CountryCode'] = $this->fields['ShipTo_CountryCode'];
		$shipto['Address'] = $addressTo;
		$phone2['Number'] = $this->fields['ShipTo_Number'];
		$shipto['Phone'] = $phone2;
		$shipment['ShipTo'] = $shipto;

		/* Important */
		$shipmentcharge['Type'] = '01';

		//$creditcard['Type'] = '06';
		// $creditcard['Number'] = $this->CI->config->item('ups')['cc']['CC_Number'];
		// $creditcard['SecurityCode'] = $this->CI->config->item('ups')['cc']['CC_SecurityCode'];
		// $creditcard['ExpirationDate'] = $this->CI->config->item('ups')['cc']['CC_ExpirationDate'];
		// $creditCardAddress['AddressLine'] = $this->CI->config->item('ups')['cc']['CC_AddressLine'];
		// $creditCardAddress['City'] = $this->CI->config->item('ups')['cc']['CC_City'];
		// $creditCardAddress['StateProvinceCode'] = $this->CI->config->item('ups')['cc']['CC_StateProvinceCode'];
		// $creditCardAddress['PostalCode'] = $this->CI->config->item('ups')['cc']['CC_PostalCode'];
		// $creditCardAddress['CountryCode'] = $this->CI->config->item('ups')['cc']['CC_CountryCode'];

		// $creditcard['Address'] = $creditCardAddress;
		// $billshipper['CreditCard'] = $creditcard;
		
		$shipmentcharge['BillShipper']['AccountNumber'] = $shiperInfo['account_id'];
		$paymentinformation['ShipmentCharge'] = $shipmentcharge;
		$shipment['PaymentInformation'] = $paymentinformation;
		
		/* Important */
		$service['Code'] = $shiperInfo['service_type'];
	 	$service['Description'] = 'Express';
		$shipment['Service'] = $service;

		/* Important */
		$packaging['Code'] = '02';
		$package['Packaging'] = $packaging;
		/* Important */
		$package_array = array();
		$weight = $this->fields['weight'];


		// foreach( $this->fields['dimensions'] as $dimension ) {
		// 	$weight = $weight + $dimension['Weight']*$dimension['Qty'];
		// }

		$punit['Code'] = 'LBS';
		$punit['Description'] = 'Pounds';
		$packageweight['Weight'] = "$weight";
		$packageweight['UnitOfMeasurement'] = $punit;
		$package['PackageWeight'] = $packageweight;
		$shipment['Package'] = array( $package );
		
		/* Important */
		$labelimageformat['Code'] = 'GIF';
		$labelimageformat['Description'] = 'GIF';
		$labelspecification['LabelImageFormat'] = $labelimageformat;
		$labelspecification['HTTPUserAgent'] = 'Mozilla/4.5';
		$shipment['LabelSpecification'] = $labelspecification;
		$request['ShipmentRequest']['Shipment'] = $shipment;
		return $request;
		
	}



}