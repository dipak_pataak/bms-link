<?php
class Authenticate
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }
    public function check_user_login(){
        if ($this->CI->session->userdata('user_type') == 'c') {

            $this->CI->db->where('id', $this->CI->session->userdata('user_id'));
            $res       = $this->CI->db->get('user_info');
            $user_data = $res->row_array();

            if (isset($user_data['id']) && $user_data['id'] != '') {
                // Valid User
            } else {
                // In Valid User
                $this->CI->session->sess_destroy();
                redirect('retailer');
            }
        }
    }
}
