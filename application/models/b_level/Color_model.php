<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Color_model extends CI_Model{
    
    function __construct() {
        // Set table name
        $this->table = 'color_tbl';
        // Set orderable column fields
        $this->column_order = array(null, 'color_name','color_number','pattern_name', 'color_tbl.created_date');
        // Set searchable column fields
        $this->column_search = array('color_name','color_number', 'pattern_name', 'color_tbl.created_date');
        // Set default order
        $this->order = array('id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }
    
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->from($this->table);
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = '.$this->table.'.pattern_id', 'LEFT');
        $this->db->join('category_tbl', 'pattern_model_tbl.category_id = category_tbl.category_id', 'LEFT');
        $this->db->where($this->table.'.created_by',$this->level_id);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
         
        $this->db->from($this->table);
        $this->db->select($this->table.'.*,pattern_model_tbl.pattern_name,category_tbl.category_name');
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = '.$this->table.'.pattern_id', 'LEFT');
        $this->db->join('category_tbl', 'pattern_model_tbl.category_id = category_tbl.category_id', 'LEFT');
        $this->db->where($this->table.'.created_by',$this->level_id);
        $i = 0;
        $colorname = $this->session->userdata('search_colorname');
        $colornumber = $this->session->userdata('search_colornumber');
        $pattern_name = $this->session->userdata('search_pattern_name');
        if ($colorname) {
            $this->db->like("color_name", $colorname, 'both');
        }
        if($colornumber) {
            $this->db->like('color_number', $colornumber, 'both');
        }
        if($pattern_name) {
            $this->db->like('pattern_model_tbl.pattern_name', $pattern_name, 'both');
        }
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

}
?>