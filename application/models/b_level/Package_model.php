<?php

class Package_model extends CI_model {

    public function get_package_list() {
        
        $query = $this->db->select('*')->from('wholesaler_hana_package')
                          ->get()->result();
        return $query;
    }

    public function get_package_option_list() {
        
        $query = $this->db->select('*')->from('hana_package_option')
                          ->get()->result();
        return $query;
    }

    public function get_package_transaction_list() {
        $user_id=$this->session->userdata('user_id');
        

        $query = $this->db->select('wholesaler_package_transaction.*,wholesaler_hana_package.package')
        ->from('wholesaler_package_transaction')
        ->where('user_id',$user_id)
        ->order_by('wholesaler_package_transaction.id','DESC')
        ->join('wholesaler_hana_package','wholesaler_hana_package.id=wholesaler_package_transaction.package_id')
        ->get()->result();
        return $query;
    }


}
