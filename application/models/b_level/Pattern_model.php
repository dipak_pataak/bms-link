<?php

class Pattern_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'pattern_model_tbl';
        // Set orderable column fields
        $this->column_order = array(null,null, 'a.pattern_name', 'b.category_name');
        // Set searchable column fields
        $this->column_search = array('a.pattern_name', 'b.category_name');
        // Set default order
        $this->order = array('a.pattern_model_id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }

    //*************** For datatable: START ************************/
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->select("a.*, b.*, a.category_id as pattern_category_id, a.status as pattern_status")
                        ->from($this->table.' a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->where('a.created_by',  $this->level_id);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
         
      $this->db->select("a.*, b.*, a.category_id as pattern_category_id, a.status as pattern_status")
                        ->from($this->table.' a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->where('a.created_by',  $this->level_id);
        
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    //*************** For datatable: END **************************/

    //============ its for customer_list method ==============
    public function pattern_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
           $level_id = $this->session->userdata('user_id');
       } else {
           $level_id = $this->session->userdata('admin_created_by');
       }
        $query = $this->db->select("a.*, b.*, a.category_id as pattern_category_id, a.status as pattern_status")
                        ->from('pattern_model_tbl a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->where('a.created_by', $level_id)
                        ->order_by('a.pattern_model_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    //    =========== its for pattern_edit ==============
    public function pattern_edit($id) {
        $query = $this->db->select('*')
                ->from('pattern_model_tbl')
                ->where('pattern_model_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//========= its for get_patern_model =============
    public function get_patern_model() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('pattern_model_tbl')
                        ->where('status', 1)
                    ->where('created_by', $level_id)
                        ->order_by('pattern_name', 'asc')
                        ->get()->result();
        return $query;
    }

//============= its for assigned_pattern_product =============
    public function assigned_pattern_product($id) {
        $sql_query = 'SELECT product_id, product_name FROM product_tbl WHERE pattern_models_ids = ' . $id;
        $sql_results = $this->db->query($sql_query);
        return $sql_results->result();
//        $this->db->select('product_name');
//        $this->db->from('product_tbl');
//        $this->db->where('pattern_models_ids', $id);
//        $query = $this->db->get();
//        return $query->result();
    }

//    ===========its for pattern_model_filter =============
    public function pattern_model_filter($pattern_name, $parent_cat, $status) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, a.category_id as pattern_category_id, b.category_name');
        $this->db->from('pattern_model_tbl a');
        $this->db->join('category_tbl b', 'b.category_id = a.category_id', 'left');
        if ($pattern_name && $parent_cat && $status) {
            $this->db->like('a.pattern_name', $pattern_name, 'both');
            $this->db->like('a.category_id', $parent_cat, 'both');
            $this->db->like('a.status', $status, 'both');
        } elseif ($pattern_name) {
            $this->db->like('a.pattern_name', $pattern_name, 'both');
        } elseif ($parent_cat) {
            $this->db->like('a.category_id', $parent_cat, 'both');
        } elseif ($status != '') {
            if ($status == 1) {
                $this->db->like('a.status', $status, 'both');
            } else {
                $this->db->like('a.status', 0, 'both');
            }
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.pattern_model_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_pattern_search_result($keyword) {
        $query = $this->db->select("a.*, b.*, a.category_id as pattern_category_id, a.status as pattern_status")
                        ->from('pattern_model_tbl a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->like('a.pattern_name', $keyword, 'both')
                        ->order_by('pattern_model_id', 'DESC')
                        ->limit(50)
                        ->get()->result();
        return $query;
    }

}
