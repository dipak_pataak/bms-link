<?php

class Sqm_price_mapping_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'sqm_price_model_mapping_tbl';
        // Set orderable column fields
        $this->column_order = array(null,null, 'p.product_name', 'pm.pattern_name', 'smm.price');
        // Set searchable column fields
        $this->column_search = array('p.product_name', 'pm.pattern_name', 'smm.price');
        // Set default order
        $this->order = array('smm.id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }

    //*************** For datatable: START ************************/
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->select('smm.*,p.product_name,pm.pattern_name')
                        ->from($this->table.' smm')
                        ->join('product_tbl p', 'p.product_id=smm.product_id', 'left')
                        ->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id', 'left')
                        ->where('pm.created_by',$this->level_id)
                        ->where('p.created_by',$this->level_id)
                        ->where('p.price_style_type', '5');
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
         
        $this->db->select('smm.*,p.product_name,pm.pattern_name')
        				->from($this->table.' smm')
                        ->join('product_tbl p', 'p.product_id=smm.product_id', 'left')
                        ->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id', 'left')
                        ->where('pm.created_by',$this->level_id)
                        ->where('p.created_by',$this->level_id)
                        ->where('p.price_style_type', '5');
                   
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    //*************** For datatable: END **************************/
} 