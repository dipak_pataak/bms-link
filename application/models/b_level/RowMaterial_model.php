<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RowMaterial_model extends CI_model {

//    ========= its for get_raw_material ============
    public function get_raw_material() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.id, a.material_name');
        $this->db->from('row_material_tbl a');
        $this->db->where('a.created_by',$level_id);
        $this->db->order_by('a.material_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

//    ====== its for raw material onkey search ===========
    public function get_rawmaterial_search_result($keyword) {
        $query = $this->db->select('a.*, b.uom_name, c.pattern_name')
                        ->from('row_material_tbl a')
                        ->join('unit_of_measurement b', 'b.uom_id=a.uom', 'left')
                        ->join('pattern_model_tbl c', 'c.pattern_model_id=a.pattern_model_id', 'left')
                        ->like('a.material_name', $keyword, 'both')
                        ->order_by('a.id', 'desc')
                        ->get()->result();
        return $query;
    }

//    ============ its for  =============
    public function get_quantity_check($material_id, $color_id, $pattern_model_id) {
        $sql_query = "SELECT 
	(SELECT sum(a.in_qty) FROM row_material_stock_tbl a WHERE a.row_material_id = $material_id AND a.pattern_model_id = $pattern_model_id AND a.color_id = $color_id) as total_in_qty,
                  (SELECT SUM(b.out_qty) FROM row_material_stock_tbl b WHERE b.row_material_id = $material_id AND b.pattern_model_id = $pattern_model_id AND b.color_id = $color_id) total_out_qty";
        $sql = $this->db->query($sql_query)->row();
//        echo $this->db->last_query();
//        $available_quantity = ($sql->total_in_qty) - ($sql->total_out_qty);
        return $sql;
    }

}
