<?php

class Attribute_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'attribute_tbl';
        // Set orderable column fields
        $this->column_order = array(null, 'a.position','c.category_name','a.attribute_name');
        // Set searchable column fields
        $this->column_search = array('position','category_name', 'attribute_name');
        // Set default order
        $this->order = array('a.position' => 'asc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }

    //============ its for get_attribute_type method ==============
    public function get_attribute_type() {
        $query = $this->db->select('*')
                        ->from('attribute_type_tbl')
                        ->get()->result();
        return $query;
    }

    //    =========== its for attribute_edit ==============
    public function attribute_edit($id) {
        $query = $this->db->select('*')
                ->from('attribute_tbl')
                ->where('attribute_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //============ its for attribute_type_list method ==============
    public function attribute_type_list($offset = null, $limit = null) {
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('admin_created_by');
//        }
        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
//                        ->where('level_id', $level_id)
                        ->order_by('attribute_type_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    =============its for attribute_type_edit ===============
    public function attribute_type_edit($id) {
        $query = $this->db->select('a.*')
                ->from('attribute_type_tbl a')
                ->where('a.attribute_type_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_attribute_types() {
        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
                        ->order_by('attribute_type_id', 'ASC')
                        ->get()->result();
        return $query;
    }

    public function get_attr_by_attr_types() {

        $query = $this->db->select("*")
                        ->from('attribute_type_tbl')
                        ->order_by('attribute_type_id', 'ASC')
                        ->get()->result();
        return $query;
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->select('a.*,c.category_name');
        $this->db->from($this->table. ' a');
        $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        $this->db->where('a.created_by',$this->level_id);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
         
        $this->db->select('a.*,c.category_name');
        $this->db->from($this->table. ' a');
        $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        $this->db->where('a.created_by',$this->level_id);

        $i = 0;
        $attribute_name = $this->session->userdata('search_attribute_name');
        $search_category_id = $this->session->userdata('search_category_id');


        if ($attribute_name) {
            $this->db->like("a.attribute_name", $attribute_name, 'both');
        }
        if($search_category_id) {
            $this->db->where('a.category_id', $search_category_id);
        }
        
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

}
