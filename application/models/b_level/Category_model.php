<?php

class Category_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'category_tbl';
        // Set orderable column fields
        $this->column_order = array(null,null,'c.category_name','parent_category_name','c.description',null,null,null,null);
        // Set searchable column fields
        $this->column_search = array('c.category_name','pc.category_name', 'c.description', 'c.fractions');
        // Set default order
        $this->order = array('category_id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }

//============= its for category list ==============
    public function category_list($offset, $limit) {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->order_by('category_id', 'desc')
                        ->where('created_by',$created_by)
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//=========== its for get_category ================
    public function get_category() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->where('parent_category', 0)
                        ->where('status', 1)
                        ->where('created_by',$created_by)
                        ->order_by('category_name', 'asc')
                        ->get()->result();
        return $query;
    }

//    =========== its for category_edit ==============
    public function category_edit($id) {
        $query = $this->db->select('*')
                ->from('category_tbl')
                ->where('category_id', $id)
                ->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =========== its for category_assigned_product ==============
    public function category_assigned_product($id) {
        $query = $this->db->select('*')
                ->from('product_tbl')
                ->where('category_id', $id)
                ->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =============its for get_filter_data =============
    public function get_filter_data($category_name, $parent_category, $status) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $check = 1;
        $this->db->select('*');
        $this->db->from('category_tbl a');
        if ($category_name && $parent_category && $status) {
            $this->db->like('a.category_name', $category_name, 'both');
            $this->db->like('a.parent_category', $parent_category, 'both');
            $this->db->like('a.status', $status, 'both');
        } elseif ($category_name) {
            $this->db->like('a.category_name', $category_name, 'both');
        } elseif ($parent_category) {
            $this->db->like('a.parent_category', $parent_category, 'both');
        } elseif ($status != '') {
            if ($status == 1) {
                $this->db->like('a.status', $status, 'both');
            } else {
                $this->db->like('a.status', 0, 'both');
            }
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.category_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_category_search_result($keyword) {
        $sql = "SELECT a.* FROM category_tbl a WHERE (a.category_name LIKE '%$keyword%') ORDER BY category_id DESC ";
//        echo $sql;        die();
        $query = $this->db->query($sql);
        return $query->result();
    }


    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll(){
        $this->db->select('c.*,IFNULL(pc.category_name,"None") as parent_category_name');
        $this->db->from($this->table.' c');
        $this->db->join($this->table.' pc', 'c.parent_category = pc.category_id', 'LEFT');
        $this->db->where('c.created_by',$this->level_id);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData){
         
        $this->db->select('c.*,IFNULL(pc.category_name,"None") as parent_category_name');
        $this->db->from($this->table.' c');
        $this->db->join($this->table.' pc', 'c.parent_category = pc.category_id', 'LEFT');
        $this->db->where('c.created_by',$this->level_id);
        $i = 0;
        $categoryname = $this->session->userdata('search_categoryname');
        $parent_category = $this->session->userdata('search_parent_category');
        $status = $this->session->userdata('search_status');
        if ($categoryname) {
            $this->db->like("c.category_name", $categoryname, 'both');
        }
        if($parent_category) {
            $this->db->like('c.parent_category', $parent_category, 'both');
        }
        if($status != '') {
            $this->db->like('c.status', $status, 'both');
        }
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_all_category($user_id){
        $query = $this->db->select('*')
        ->from('category_tbl')
        ->where('parent_category', 0)
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',0)
        ->get()->result_array();
        return $query;
    }

    public function updateQuickbooksStatus($ids){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('category_id',$ids);
        $this->db->update('category_tbl');

        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateQuickbooksId($id,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('category_id',$id);
        $this->db->update('category_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_all_category_quick_book_id($user_id,$category_id){
        $query = $this->db->select('*')
        ->from('category_tbl')
        ->where('created_by', $user_id)
        ->where('category_id', $category_id)
        ->where('is_added_in_quickbooks',1)
        ->get()->row_array();
        return $query;
    }

}
