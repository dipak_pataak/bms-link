<?php

class Mapping_model extends CI_model {

    public function get_unit_data(){
    	$res = $this->db->where('user_id',$this->session->userdata('user_id'))->get('unit_of_measurement')->result();
    	return $res;
    }

    public function get_mapping_data($from_id,$to_id) {
    	$res = $this->db->where('user_id',$this->session->userdata('user_id'))->where('from_measure_id',$from_id)->where('to_measure_id',$to_id)->limit(1)->get('mapping_measurement')->row();
    	return $res;
    }
}    