<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Condition_model extends CI_model {

//    ============= its for condition_list ============
    public function condition_list($offset, $limit) {
        $query = $this->db->select('a.*, b.*, c.fraction_value as w_fraction_value, d.fraction_value as h_fraction_value, a.category_id as condition_category_id')
                        ->from('product_conditions_tbl a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->join('width_height_fractions c', 'c.id = a.width_fraction_id', 'left')
                        ->join('width_height_fractions d', 'd.id = a.height_fraction_id', 'left')
//                        ->where('a.is_active', 1)
                        ->order_by('a.condition_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    //    =========== its for condition_edit ==============
    public function condition_edit($id) {
        $query = $this->db->select('*')
                ->from('product_conditions_tbl')
                ->where('condition_id', $id)
                ->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//    ============= its for condition_check_from_product ==========
    public function condition_check_from_product($id) {
        $this->db->select('*');
        $this->db->from('product_tbl');
        $this->db->where('condition_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

//    ====== its for product_conditions_tbl ==============
    public function get_product_conditions() {
        $this->db->select('*');
        $this->db->from('product_conditions_tbl a');
        $this->db->order_by('condition_text', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

//    =========== its for category_wise_condition =============
    public function category_wise_condition($category_id) {
        $query = $this->db->select('*')->from('product_conditions_tbl')->where('category_id', $category_id)
                        ->where('is_active', 1)
                        ->get()->result();
        return $query;
    }

//    ========= its for checked_category_wise_condition ============
    public function checked_product_wise_condition($product_id) {
        $query = $this->db->select('*')->from('product_condition_mapping')->where('product_id', $product_id)
                        ->get()->result();
        return $query;
    }

//    ============== its for get_condition_filter ==========
    public function get_condition_filter($category_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('*');
        $this->db->from('product_conditions_tbl a');
        $this->db->join('category_tbl b', 'b.category_id = a.category_id');
        $this->db->order_by('a.condition_id', 'desc');
        if ($category_name) {
            $this->db->like('a.category_id', $category_name, 'both');
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $query = $this->db->get()->result();
        return $query;
    }

}
