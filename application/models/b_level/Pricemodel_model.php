<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricemodel_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'row_column';
        // Set orderable column fields
        $this->column_order = array(null,null, 'rc.style_name');

        // Set searchable column fields
        $this->column_search = array('rc.style_name');
        // Set default order
        $this->order = array('rc.style_id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }

//    ========== its for price_manage_filter ============
    public function price_manage_filter($price_sheet_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('*');
        $this->db->from('row_column a');
        $this->db->join('product_tbl b', 'b.price_rowcol_style_id =  a.style_id', 'left');
        if ($price_sheet_name) {
            $this->db->like('a.style_name', $price_sheet_name, 'both');
        } else {
            $this->db->where('a.create_by', $level_id);
        }
        $this->db->order_by('a.style_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    ========== its for group_manage_filter ============
    public function group_manage_filter($price_sheet_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, a.active_status as status, b.product_name, b.active_status');
        $this->db->from('row_column a');
        $this->db->join('product_tbl b', 'b.price_rowcol_style_id=a.style_id', 'left');
         if ($price_sheet_name) {
            $this->db->like('a.style_name', $price_sheet_name, 'both');
        } else {
            $this->db->where('a.create_by', $level_id);
        }
        $this->db->where('a.style_type', 4);
        $this->db->order_by('a.style_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }


     //*************** For datatable: START ************************/
    /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
    public function getRows($postData,$type='1'){
        $this->_get_datatables_query($postData,$type);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result();
    }
    
    /*
     * Count all records
     */
    public function countAll($type='1'){
        $this->db->select('rc.*, rc.active_status as status')
                        ->from($this->table.' rc')
                        ->where('rc.style_type', $type)
                        ->where('rc.create_by', $this->level_id);
        return $this->db->count_all_results();
    }
    
    /*
     * Count records based on the filter params
     * @param $_POST filter data based on the posted parameters
     */
    public function countFiltered($postData,$type='1'){
        $this->_get_datatables_query($postData,$type);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
    private function _get_datatables_query($postData,$type){
         
        $this->db->select('rc.*, rc.active_status as status')
                        ->from($this->table.' rc')
                        ->where('rc.style_type', $type)
                        ->where('rc.create_by', $this->level_id);
                   
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    //*************** For datatable: END **************************/

}
