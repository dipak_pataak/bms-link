<?php

class Catalog_request_model extends CI_model
{

    public function get_catalog_request($offset = null, $limit = null)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select("a.*, CONCAT_WS(' ', b.first_name, b.last_name) as full_name,b.company,b.phone,b.email,b.address,b.city,b.state,b.zip_code,b.country_code")
            ->join('user_info b', 'b.id = a.requested_by', 'left')
            ->from('b_user_catalog_request AS a')
            ->where('a.b_user_id', $level_id)
            ->order_by('a.request_id', 'desc')
            ->limit($offset, $limit)
            ->get()->result();
        return $query;

    }

    function get_approve_product_list($b_user_id)
    {
        $query = $this->db->select('a.*,c.category_name,b.request_id,b.approve_status AS request_approve_status,b.id AS request_product_id,b.product_link,b.pattern_link,pattern.pattern_name')
            ->join('b_user_catalog_products b','b.product_id=a.product_id','join')
            ->join('b_user_catalog_request req','req.request_id=b.request_id','join')
            ->join('pattern_model_tbl pattern','pattern.pattern_model_id=b.pattern_model_id','join')
            ->join('category_tbl c', 'c.category_id=a.category_id', 'left')
            ->from('product_tbl a')
            ->where('req.b_user_id',$b_user_id)
           /* ->where('b.approve_status',1)*/
            ->group_by('b.id')
            ->get()
            ->result();

        return $query;

    }

    function get_catalog_category($request_id)
    {
        $query = $this->db->select('*')
            ->from('b_user_catalog_products a')
            ->join('product_tbl b','b.product_id=a.product_id')
            ->join('category_tbl c','c.category_id=b.category_id')
            ->where('a.request_id', $request_id)
            ->where('a.approve_status', 1)
            ->order_by('category_name', 'asc')
            ->group_by('c.category_id')
            ->get()->result();
        return $query;

    }

    function get_catalog_product_by_category($category_id,$request_id)
    {
        $query = $this->db->select('b.product_id,b.category_id,b.product_name')
            ->from('b_user_catalog_products a')
            ->join('product_tbl b','b.product_id=a.product_id')
            ->where('a.request_id', $request_id)
            ->where('a.approve_status', 1)
            ->where('b.category_id', $category_id)
            ->group_by('b.product_id')
            ->get()->result();

        return $query;
    }

    function get_customer_catalog_product_by_category($category_id,$request_id)
    {
        $query = $this->db->select('b.product_id,b.category_id,b.product_name')
            ->from('b_user_catalog_products a')
            ->join('product_tbl b','b.product_id=a.product_id')
            ->where('a.request_id', $request_id)
            ->where('b.category_id', $category_id)
            ->group_by('b.product_id')
            ->get()->result();

        return $query;
    }
}
