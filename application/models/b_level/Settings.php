<?php

class Settings extends CI_model {

    function get_userlist($limit, $start)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('user_type', 'b');
        $this->db->where('created_by!=', '');
        $this->db->where('created_by',$created_by);
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->result();
        } else {

            return false;
        }
    }

    function get_user_by_id($user_id) {
        $this->db->select("*");
        $this->db->from('user_info a');
        $this->db->join('log_info b', 'b.user_id = a.id');
        $this->db->where('a.user_type', 'b');
        $this->db->where('a.created_by!=', '');
        $this->db->where('a.id', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->row();
        } else {

            return false;
        }
    }

//    ============= its for my_account ==========
    public function my_account() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('user_info a');
        $this->db->where('a.id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

//    ============= its for company_profile ================
    public function company_profile() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
//        echo $level_id;die();
        $query = $this->db->select('*')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function single_company_profile($level_id) {

        $query = $this->db->select('*')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

//    =========== its for uom_list ==============
    public function uom_list($offset = null, $limit = null) {
        $query = $this->db->select("*")
                        ->from('unit_of_measurement')
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->order_by('uom_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    ============ its for get uom list =============
    public function get_uom_list() {
        $query = $this->db->select("*")
                        ->from('unit_of_measurement')
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->order_by('uom_name', 'Asc')
                        ->get()->result();
        return $query;
    }

//============ its for uom_edit =============
    public function uom_edit($id) {
        $query = $this->db->select('a.*')
                ->from('unit_of_measurement a')
                ->where('a.uom_id', $id)
                ->where('user_id', $this->session->userdata('user_id'))
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ================= its for get_mail_config ==============
    public function get_mail_config() {
        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

//=========== its for web_setting ===============
    public function gateway_list($offset, $limit) {
        $query = $this->db->select('*')
                        ->from('gateway_tbl')
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->where('created_by', $this->session->userdata('user_id'))
                        ->get()->result();
        return $query;
    }

//==================== its for gateway_edit ==============
    public function gateway_edit($id) {
        $query = $this->db->select('*')
                ->from('gateway_tbl a')
                ->where('a.id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    ============ its for get_sms_config ================
    public function get_sms_config() {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $this->db->order_by('a.gateway_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

//    =============== its for sms_config_edit ==============
    public function sms_config_edit($gateway_id) {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('a.gateway_id', $gateway_id);
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//==================== its for us_state_edit ==============
    public function us_state_edit($id) {
        $query = $this->db->select('*')
                ->from('us_state_tbl a')
                ->where('a.state_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

//    =========== its for uom_list ==============
    public function us_state_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.created_by=" . $user_id;
        }
        $query = $this->db->select("*")
                        ->from('us_state_tbl a')
                        ->where($filter)
                        ->order_by('a.state_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

    //    ========== its for get_us_state_search_result =============
    public function get_us_state_search_result($keyword) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id";
//        echo $sql;        die();
        $query = $this->db->query($sql);
//        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
//        $this->db->from('customer_info a');
//        if ($keyword) {
//            $this->db->like('a.company', $keyword, 'both');
//            $this->db->or_like('a.first_name', $keyword, 'both');
//            $this->db->or_like('a.phone', $keyword, 'both');
//            $this->db->or_like('a.email', $keyword, 'both');
//            $this->db->where('a.level_id', $level_id);
//        } else {
//            $this->db->where('a.level_id', $level_id);
//        }
//        $query = $this->db->get();
        return $query->result();
    }

//    ============= its for user_filter ============
    public function user_filter($first_name, $email, $type) {
        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        if ($first_name && $email && $type) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->like('email', $email, 'both');
            $this->db->like('user_type', $type, 'both');
        }
        if ($first_name) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($email) {
            $this->db->like('email', $email, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($type) {
            $this->db->like('user_type', $type, 'both');
            $this->db->where('created_by!=', '');
        }
        $this->db->where('created_by!=', '');
        $this->db->where('user_type', 'b');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

//=========== its for b level color_filter ===============
    public function color_filter($colorname, $colornumber) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('*');
        $this->db->from('color_tbl a');
        if ($colorname && $colornumber) {
            $this->db->like('a.color_name', $colorname, 'both');
            $this->db->like('a.color_number', $colornumber, 'both');
        } elseif ($colorname) {
            $this->db->like('a.color_name', $colorname, 'both');
        } elseif ($colornumber) {
            $this->db->like('a.color_number', $colornumber, 'both');
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function access_logs($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.user_name=" . $user_id;
        }
        $query = $this->db->select('a.*, CONCAT( b.first_name, " ", b.last_name) AS name')
                        ->from('accesslog a')
                        ->join('user_info b', 'b.id = user_name')
//                        ->where('user_name', $level_id)
                        ->where($filter)
                        ->order_by('entry_date', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

//    ========== its for send Custom email ===============
    public function sendCstomEmail($email, $message, $isverified = true) {
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config =$CI->Common_model->mailConfig($this->session->userdata('user_id'), $isverified);
        if(isset($config['error'])) {
            return false;
        }
        
        $this->load->library('email', $config);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("BMSLINK");
       /*  $fp = @fsockopen(base_url(), $config['smtp_port'], $errno, $errstr);
        if ($fp) { */
            $this->email->message($message);
            if($this->email->send()) {
                $email_data = array(
                    'from' => $config['smtp_user'],
                    'to' => $email,
                    'message' => $message,
                    'created_by' => $this->session->userdata('user_id'),
                );
                $this->db->insert('custom_email_tbl', $email_data);
                return true;
            } else {
                return false;
            }
        // }
    }

    // Get measurement Unit
    public function company_unit() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('unit')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data->unit;
        } else {
            return false;
        }
    }

    //get employee data
    public function get_all_employee($user_id){
        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('is_added_in_quickbooks',0);
        $this->db->where('created_by',$user_id);
        return $this->db->get()->result_array();
    } 

    //update employee status
    public function updateEmployeeQuickbooksStatus($id='')
    {
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where_in('id',$id);
        $this->db->update('user_info');
        return true;
    }

    //update employee quickbooks Id
    public function updateEmployeeQuickbooksId($empid,$quickbooksId)
    {
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('id',$empid);
        $this->db->update('user_info');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
   
    //get quickbooks setting data
    public function getAllQuickBooksSettingData(){
        return $this->db->where('status',1)->get('quickbook_setting_tbl')->result_array();
    }
     //get quickbooks data by user id
    public function getQuickBooksSettingData($user_id){
        return $this->db->where('user_id',$user_id)->get('quickbook_setting_tbl')->row();
    }
    //get quickbooks data by id
    public function getQuickBooksSettingDataById($id){
        return $this->db->where('id',$id)->get('quickbook_setting_tbl')->row();
    }

    //Change quickbboks setting data from wholeseller setting module
    public function update_quickbooks_setting_data($data,$user_id){
        $this->db->where('user_id',$user_id);
        $this->db->update('quickbook_setting_tbl',$data);
        return true;
    }
    //Insert quickbooks data
    public function add_quickbooks_setting_data($data){
        $this->db->insert('quickbook_setting_tbl',$data);
        return $this->db->insert_id();
    }

    //get quickbooks data with refresh time data
    public function getQuickBooksRefreshTimeData($current_time,$refresh_time){
      return  $this->db->where('refresh_time >=',$current_time)
        ->where('refresh_time <=',$refresh_time)
        ->where('status',1)
        ->get('quickbook_setting_tbl')->result_array();
    }

    public function getQuickBooksSettingActiveData($user_id){
        return $this->db->where('user_id',$user_id)->get('quickbook_setting_tbl')->row();
    }

    //get invoices of b-level data
    public function get_all_invoices_data($user_id){
        return $this->db->select('a.*,customer_info.quick_books_id as customer_quickbooks_id')
        ->from('b_level_quatation_tbl a')
        ->join('customer_info','a.customer_id = customer_info.customer_id','left')
        ->order_by('a.id', 'DESC')
        ->where('a.created_by',$user_id)
        ->where('a.is_added_in_quickbooks',0)
        ->get()->result();
    }
    
    //get invoices of c-level data
    public function get_all_c_level_invoices_data($user_id){
        return $this->db->select("a.*,customer_info.quick_books_id as customer_quickbooks_id")
        ->from('quatation_tbl a')
        ->join('customer_info','a.customer_id = customer_info.customer_id','left')
        ->order_by('a.id', 'DESC')
        ->where('a.created_by',$user_id)
        ->where('a.is_added_in_quickbooks',0)
        ->get()->result();
    }
    
    public function changeQuickbooksStatus($id='')
    {
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where_in('id',$id);
        $this->db->update('b_level_quatation_tbl');
        return true;
    }
    
    public function get_product_data($user_id){
        return $this->db->where('created_by',$user_id)->where('is_added_in_quickbooks',0)->get('product_tbl')->result();
    }
    
    public function updateProductQuickbooksId($product_id,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('product_id',$product_id);
        $this->db->update('product_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    
    public function updateproductQuickbooksStatus($product_id){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('product_id',$product_id);
        $this->db->update('product_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    
    public function get_all_product_quick_book_id($user_id){
        $query = $this->db->select('*')
        ->from('product_tbl')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',1)
        ->get()->result_array();
        return $query;
    }

    public function updateInvoiceQuickbooksId($invoiceId,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('id',$invoiceId);
        $this->db->update('quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateInvoiceQuickbooksStatus($invoiceId){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('id',$invoiceId);
        $this->db->update('quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateBLevelInvoiceQuickbooksId($invoiceId,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('id',$invoiceId);
        $this->db->update('b_level_quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateBLevelInvoiceQuickbooksStatus($invoiceId){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('id',$invoiceId);
        $this->db->update('b_level_quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_user_data($user_id){
        $this->db->select("*");
        $this->db->from('user_info');
        $this->db->where('id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {

            return $query->row();
        } else {

            return false;
        }
    }

    public function get_orderd_details_by_id($order_id){

        $query = $this->db->select("qutation_details.*,
            product_tbl.product_id,
            product_tbl.quick_books_id as product_quick_books_id,
            product_tbl.product_name,
            category_tbl.category_name,
            quatation_attributes.product_attribute,
            qutation_details.product_qty as qty,
            qutation_details.unit_total_price as unit_price,
            qutation_details.discount as discount,
            qutation_details.list_price as list_price,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number,
            qutation_details.height,
            qutation_details.width,
            qutation_details.height_fraction_id,
            qutation_details.width_fraction_id")
            ->from('qutation_details')
            ->join('product_tbl','product_tbl.product_id=qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=qutation_details.category_id','left')
            ->join('quatation_attributes','quatation_attributes.fk_od_id=qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id','left')
            ->join('color_tbl','color_tbl.id=qutation_details.color_id','left')
            ->where('qutation_details.order_id', $order_id)
            ->get()->result_array();
        return $query;
    }


    public function get_b_level_orderd_details_by_id($order_id){
        return $this->db->select('b_level_qutation_details.*,
        product_tbl.product_name,
        product_tbl.product_id,
        product_tbl.quick_books_id as product_quick_books_id,
        b_level_quatation_attributes.product_attribute,
        b_level_qutation_details.product_qty as qty,
        b_level_qutation_details.unit_total_price as unit_price,
        b_level_qutation_details.discount as discount,
        b_level_qutation_details.list_price as list_price,
        pattern_model_tbl.pattern_name,
        color_tbl.color_name,
        color_tbl.color_number,
        b_level_qutation_details.room,
        b_level_qutation_details.unit_total_price,
        b_level_qutation_details.height,
        b_level_qutation_details.width,
        b_level_qutation_details.height_fraction_id,
        b_level_qutation_details.width_fraction_id')
        ->from('b_level_qutation_details')
        ->join('product_tbl','product_tbl.product_id = b_level_qutation_details.product_id','left')
        ->join('b_level_quatation_attributes','b_level_quatation_attributes.fk_od_id=b_level_qutation_details.row_id','left')
        ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id','left')
        ->join('color_tbl','color_tbl.id=b_level_qutation_details.color_id','left')
        ->where('b_level_qutation_details.order_id',$order_id)
        ->get()->result_array();
    }


    public function get_all_state($user_id){
        $query = $this->db->select('*')
        ->from('city_state_tbl')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',0)
        ->get()->result_array();
        return $query;
    }

    public function updateTaxAgencyQuickbooksId($state_id,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('id',$state_id);
        $this->db->update('city_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateTaxAgencyQuickbooksStatus($state_id){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('id',$state_id);
        $this->db->update('city_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_all_c_level_state_rates($user_id){
        $query = $this->db->select('*')
        ->from('c_us_state_tbl')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',0)
        ->get()->row_array();
        return $query;
    }
    public function get_all_b_level_state_rates($user_id){
        $query = $this->db->select('*')
        ->from('us_state_tbl')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',0)
        ->get()->row_array();
        return $query;
    }

    public function get_tax_agency_data($user_id){
        $query = $this->db->select('*')
        ->from('city_state_tbl')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',1)
        ->get()->result_array();
        return $query;
    }

    public function updateClevelTaxServiceQuickbooksId($state_id,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('state_id',$state_id);
        $this->db->update('c_us_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateClevelTaxServiceQuickbooksStatus($state_id){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('state_id',$state_id);
        $this->db->update('c_us_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateTaxServiceQuickbooksId($state_id,$quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('state_id',$state_id);
        $this->db->update('us_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateTaxServiceQuickbooksStatus($state_id){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('state_id',$state_id);
        $this->db->update('us_state_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_product_by_id($product_id){
        $query = $this->db->select('*')
        ->from('product_tbl')
        ->where('is_added_in_quickbooks',1)
        ->where('product_id',$product_id)
        ->get()->row_array();
        return $query;
    }

    public function get_row_material_data($user_id){
        $query = $this->db->select('a.*,b.pattern_name,b.category_id,s.supplier_id')
        ->from('row_material_tbl a')
        ->join('pattern_model_tbl b','a.pattern_model_id = b.pattern_model_id','left')
        ->join('supplier_raw_material_mapping s','a.id = s.raw_material_id','left')
        ->where('a.is_added_in_quickbooks',0)
        ->where('a.created_by',$user_id)
        ->get()->result_array();
        return $query;
    }

    public function get_product_data_by_pattern_id($pattern_model_id, $user_id){
        $query = $this->db->select('*')
        ->from('product_tbl')
        ->where_in('pattern_models_ids',$pattern_model_id)
        ->where('is_added_in_quickbooks',1)
        ->where('created_by',$user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_company_profile($user_id){
        $query = $this->db->select('*')
        ->from('company_profile')
        ->where('user_id',$user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_supplier_data($supplier_id){
        $query = $this->db->select('*')
        ->from('supplier_tbl')
        ->where('supplier_id',$supplier_id)
        ->where('is_added_in_quickbooks',1)
        ->get()->row_array();
        return $query;
    }

    public function updatePurchaseOrderQuickbooksStatus($id){
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('id',$id);
        $this->db->update('purchase_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
    public function updatePurchaseOrderQuickbooksId($id, $quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('id',$id);
        $this->db->update('purchase_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_purchase_order_data($user_id){
        return $this->db->select('*')
        ->from('purchase_tbl')
        ->where('created_by',$user_id)
        ->where('is_added_in_quickbooks',0)
        ->get()->result_array();
    }

    public function check_supplier_data($supplier_id, $user_id){
        $query = $this->db->select('*')
        ->from('supplier_tbl')
        ->where('supplier_id',$supplier_id)
        ->where('is_added_in_quickbooks',1)
        ->where('created_by',$user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_purchase_order_details_data($puchase_id){
        return $this->db->select('purchase_details_tbl.*,
            purchase_details_tbl.quantity as qty,
            purchase_details_tbl.discount as discount,
            purchase_details_tbl.purchase_price as list_price,
            purchase_details_tbl.discount,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number')
            ->from('purchase_details_tbl')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=purchase_details_tbl.pattern_model_id','left')
            ->join('color_tbl','color_tbl.id=purchase_details_tbl.color_id','left')
            ->where('purchase_details_tbl.purchase_id',$puchase_id)
            ->get()->result_array();
    }

    public function get_all_c_level_taxes($user_id,$state_name){
        $query = $this->db->select('*')
        ->from('c_us_state_tbl')
        ->where('is_added_in_quickbooks',1)
        ->where('shortcode',$state_name)
        ->where('created_by',$user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_all_b_level_taxes($user_id,$state_name){
        $query = $this->db->select('*')
        ->from('us_state_tbl')
        ->where('is_added_in_quickbooks',1)
        ->where('shortcode',$state_name)
        ->where('created_by',$user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_customer_id_data($customer_id){
        $query = $this->db->select('*')
        ->from('customer_info')
        ->where('customer_id', $customer_id)
        ->get()->row_array();
        
        return $query;
    }

    public function get_company_profile_data($customer_user_id){
        $query = $this->db->select('*')
        ->from('company_profile')
        ->where('user_id', $customer_user_id)
        ->get()->row_array();
        return $query;
    }

    public function get_b_level_commision_list($user_id){
        $this->db->select("SUM(q.commission_amt) as total_commission,q.created_date, CONCAT_WS(' ', u.first_name, u.last_name) as full_name,q.created_by,GROUP_CONCAT(q.id) as quatation_id");
        $this->db->from('b_level_quatation_tbl q');
        $this->db->join('user_info u','q.created_by = u.id','FULL');
        $this->db->where('u.created_by',$user_id);
        $this->db->where('q.is_added_commission_quick_book_status',0);
        $this->db->group_by('q.created_by');
        $this->db->order_by('q.created_date', 'desc');
        $query = $this->db->get()->result_array();                           
        return $query;
    }

    public function get_c_level_commision_list($user_id){
        $this->db->select("SUM(q.commission_amt) as total_commission,q.created_date, CONCAT_WS(' ', u.first_name, u.last_name) as full_name,q.created_by,GROUP_CONCAT(q.id) as quatation_id");
        $this->db->from('quatation_tbl q');
        $this->db->join('user_info u','q.created_by = u.id','FULL');
        $this->db->where('q.created_by',$user_id);
        $this->db->where('q.is_added_commission_quick_book_status',0);
        $this->db->group_by('q.created_by');
        $this->db->order_by('q.created_date', 'desc');
        $query = $this->db->get()->result_array();                           
        return $query;
    }

    public function check_employee_data($user_id){
        $this->db->select("*");
        $this->db->from('user_info');
        $this->db->where('id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->row_array();
        } else {

            return false;
        }
    }

    

    public function updateclevelCommissionQuickbooksStatus($quatationId){
        $this->db->set('is_added_commission_quick_book_status',1);
        $this->db->where_in('id',$quatationId);
        $this->db->update('quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function updateblevelCommissionQuickbooksStatus($quatationId){
        $this->db->set('is_added_commission_quick_book_status',1);
        $this->db->where_in('id',$quatationId);
        $this->db->update('b_level_quatation_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
}
