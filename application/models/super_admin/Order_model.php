<?php

class Order_model extends CI_model {

    public function search_order_list($request)
    {
        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        if(isset($_POST['filter_order_stage']) && !empty($_POST['filter_order_stage']))
        {
            $this->db->where('b_level_quatation_tbl.order_stage', $_POST['filter_order_stage']);
        }
        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('b_level_quatation_tbl.order_id', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('b_level_quatation_tbl.side_mark', $search_value);
            $this->db->or_like('b_level_quatation_tbl.order_date', $search_value);
            $this->db->or_like('b_level_quatation_tbl.due', $search_value);
            $this->db->group_end(); //close bracket
        }

        if(!empty($order) && !empty($order_type)) // here order processing
        {
            $this->db->order_by($order, $order_type);
        }


        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("b_level_quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('b_level_quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left');
        if(isset($_POST['filter_order_stage']) && !empty($_POST['filter_order_stage']))
        {
            $this->db->where('b_level_quatation_tbl.order_stage', $_POST['filter_order_stage']);
        }
        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('b_level_quatation_tbl.order_id', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('b_level_quatation_tbl.side_mark', $search_value);
            $this->db->or_like('b_level_quatation_tbl.order_date', $search_value);
            $this->db->or_like('b_level_quatation_tbl.due', $search_value);
            $this->db->group_end(); //close bracket
        }
        $total_query = $this->db->get()->result();
        $total = count($total_query);

        $data = array();
        if (count($response) != 0)
        {
            $currency = '$';
            foreach ($response as $value)
            {
                $nestedData["order_id"] = $value->order_id;
                $nestedData["first_name"] = $value->customer_name;
                $nestedData["side_mark"] = $value->side_mark;
                $nestedData["order_date"] = $value->order_date;
                $nestedData["due"] = $currency.$value->due;
                $order_stage = get_single_order_stage($value->order_stage);
                $nestedData["order_stage"] = '<span class="'.$order_stage["class"].'">'.$order_stage['stage'].'</span>';
                $nestedData["action"] = '
                        <a href="' . base_url() . 'super-admin/orders/receipt/' . $value->order_id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-eye"></i>
                        </a>';
                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

    public function get_orderd_by_id($order_id){

        $query = $this->db->select("b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
            ->from('b_level_quatation_tbl')
            ->join('customer_info','customer_info.customer_id=b_level_quatation_tbl.customer_id','left')
            ->where('b_level_quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;

    }

    public function get_orderd_details_by_id($order_id){

        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")

            ->from('b_level_qutation_details')
            ->join('product_tbl','product_tbl.product_id=b_level_qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=b_level_qutation_details.category_id','left')
            ->join('b_level_quatation_attributes','b_level_quatation_attributes.fk_od_id=b_level_qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id','left')

            ->join('color_tbl','color_tbl.id=b_level_qutation_details.color_id','left')

            ->where('b_level_qutation_details.order_id', $order_id)

            ->get()->result();



        return $query;

    }

    /**********************************************************/

public function search_customer_order_list($request)
    {
        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=quatation_tbl.customer_id','left');
        if(isset($_POST['filter_order_stage']) && !empty($_POST['filter_order_stage']))
        {
            $this->db->where('quatation_tbl.order_stage', $_POST['filter_order_stage']);
        }
        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('quatation_tbl.order_id', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('quatation_tbl.side_mark', $search_value);
            $this->db->or_like('quatation_tbl.order_date', $search_value);
            $this->db->or_like('quatation_tbl.due', $search_value);
            $this->db->group_end(); //close bracket
        }

        if(!empty($order) && !empty($order_type)) // here order processing
        {
            $this->db->order_by($order, $order_type);
        }

        $this->db->group_by('order_id');
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info','customer_info.customer_id=quatation_tbl.customer_id','left');
        if(isset($_POST['filter_order_stage']) && !empty($_POST['filter_order_stage']))
        {
            $this->db->where('quatation_tbl.order_stage', $_POST['filter_order_stage']);
        }
        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('quatation_tbl.order_id', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('quatation_tbl.side_mark', $search_value);
            $this->db->or_like('quatation_tbl.order_date', $search_value);
            $this->db->or_like('quatation_tbl.due', $search_value);
            $this->db->group_end(); //close bracket
        }
        $this->db->group_by('order_id');
        $total_query = $this->db->get()->result();
        $total = count($total_query);

        $data = array();
        if (count($response) != 0)
        {
            $currency = '$';
            foreach ($response as $value)
            {
                $nestedData["order_id"] = $value->order_id;
                $nestedData["first_name"] = $value->customer_name;
                $nestedData["side_mark"] = $value->side_mark;
                $nestedData["order_date"] = $value->order_date;
                $nestedData["due"] = $currency.$value->due;
                $order_stage = get_single_order_stage($value->order_stage);
                $nestedData["order_stage"] = '<span class="'.$order_stage["class"].'">'.$order_stage['stage'].'</span>';
                $nestedData["action"] = '
                        <a href="' . base_url() . 'super-admin/customer-orders/receipt/' . $value->order_id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-eye"></i>
                        </a>';
                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

    public function get_customer_orderd_by_id($order_id){

        $query = $this->db->select("quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
            ->from('quatation_tbl')
            ->join('customer_info','customer_info.customer_id=quatation_tbl.customer_id','left')
            ->where('quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;

    }

    public function get_customer_orderd_details_by_id($order_id){

        $query = $this->db->select("qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")

            ->from('qutation_details')
            ->join('product_tbl','product_tbl.product_id=qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=qutation_details.category_id','left')
            ->join('b_level_quatation_attributes','b_level_quatation_attributes.fk_od_id=qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id','left')

            ->join('color_tbl','color_tbl.id=qutation_details.color_id','left')

            ->where('qutation_details.order_id', $order_id)

            ->get()->result();



        return $query;

    }



}
