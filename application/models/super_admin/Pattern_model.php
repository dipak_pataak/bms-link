<?php

class Pattern_model extends CI_model
{

    public function customer_catalog_pattern_list($request)
    {

        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');
        //$this->db->where('t1.created_by', $_POST['level_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("t1.pattern_name", $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('t1.created_by', $_POST['level_id']);
                $this->db->where('t1.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('t1.created_by', $_POST['customer_user_id']);
            $this->db->where('t1.created_status', 1);
            $this->db->group_end(); //close bracket


        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');
        $this->db->where('t1.created_by', $_POST['level_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("t1.pattern_name", $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('t1.created_by', $_POST['level_id']);
                $this->db->where('t1.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('t1.created_by', $_POST['customer_user_id']);
            $this->db->where('t1.created_status', 1);
            $this->db->group_end(); //close bracket

        $total_query = $this->db->get()->result();
        $total = count($total_query);
        $data = array();

        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                $nestedData["serial_no"] = ++$sl;
                $nestedData["pattern_name"] = $value->pattern_name;
                $nestedData["category_name"] = $value->category_name;

                $sql = "SELECT product_name FROM product_tbl WHERE pattern_models_ids = $value->pattern_model_id";
                $results = $this->db->query($sql)->result();

                $product_name = '';
                if (count($results) != 0)
                {
                    $product_name .= "<ul>";
                    $k = 0;
                    foreach ($results as $result)
                    {
                        $k++;
                        $product_name .= "<li>" . $result->product_name . "</li>";
                    }
                    $product_name .= "</ul>";
                } else
                {
                    $product_name .= '<p>The products will be assigned at the later stage</p>';
                }
                $nestedData["assigned_product"] = $product_name;


                if ($value->status == '1')
                {
                    $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else
                {
                    $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


    public function b_user_catalog_pattern_list($request)
    {

        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');
        $this->db->where('t1.created_by', $_POST['b_user_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("t1.pattern_name", $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');
        $this->db->where('t1.created_by', $_POST['b_user_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("t1.pattern_name", $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query = $this->db->get()->result();
        $total = count($total_query);
        $data = array();

        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                $nestedData["serial_no"] = ++$sl;
                $nestedData["pattern_name"] = $value->pattern_name;
                $nestedData["category_name"] = $value->category_name;

                $sql = "SELECT product_name FROM product_tbl WHERE pattern_models_ids = $value->pattern_model_id";
                $results = $this->db->query($sql)->result();

                $product_name = '';
                if (count($results) != 0)
                {
                    $product_name .= "<ul>";
                    $k = 0;
                    foreach ($results as $result)
                    {
                        $k++;
                        $product_name .= "<li>" . $result->product_name . "</li>";
                    }
                    $product_name .= "</ul>";
                } else
                {
                    $product_name .= '<p>The products will be assigned at the later stage</p>';
                }
                $nestedData["assigned_product"] = $product_name;


                if ($value->status == '1')
                {
                    $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else
                {
                    $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

}
