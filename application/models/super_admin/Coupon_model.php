<?php

class Coupon_model extends CI_model
{

    public function search_coupon_list($request)
    {
        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("*");
        $this->db->from('coupon');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('coupon', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->limit($limit, $offset);

        $response = $this->db->get()->result();

        $this->db->select("*");
        $this->db->from('coupon');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('coupon', $search_value);
            $this->db->group_end(); //close bracket
        }


        $total_query  = $this->db->get()->result();
        $total = count($total_query);

        $data = array();
        if (count($response) != 0)
        {
            foreach ($response as  $value)
            {
                $nestedData["coupon"] = $value->coupon;
                
                if ($value->status == '1') 
                {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }
                 $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$this->session->userdata('user_id'))->get()->result();
                        $ids=$user_role_info[0]->role_id;
                        $user_data = $this->db->select('*')
                                                ->from('admin_role_permission_tbl')
                                                ->where('menu_id =',33)
                                                ->where('role_id', $ids)
                                                ->get()->result();
                                                 $nestedData["action"] = '';
                        if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){
                $nestedData["action"] = '
                        <a href="' . base_url() . 'super-admin/coupon/edit/' . $value->id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>';
                        }
                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


}
