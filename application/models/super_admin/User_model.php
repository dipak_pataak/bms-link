<?php

class User_model extends CI_model
{

    public function search_b_user_list($request)
    {
        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name");
        $this->db->from('user_info');
        $this->db->where('user_info.user_type', 'b');
        $this->db->where('user_info.created_by', '');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('user_info.company', $search_value);
            $this->db->or_like('user_info.first_name', $search_value);
            $this->db->or_like('user_info.last_name', $search_value);
            $this->db->or_like('user_info.phone', $search_value);
            $this->db->or_like('user_info.email', $search_value);
            $this->db->or_like('user_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        if(!empty($order) && !empty($order_type)) // here order processing
        {
            $this->db->order_by($order, $order_type);
        }

        $this->db->group_by('user_info.id');
        $this->db->limit($limit, $offset);

        $response = $this->db->get()->result();

        $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name");
        $this->db->from('user_info');
        $this->db->where('user_info.user_type', 'b');
        $this->db->where('user_info.created_by', '');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('user_info.company', $search_value);
            $this->db->or_like('user_info.first_name', $search_value);
            $this->db->or_like('user_info.last_name', $search_value);
            $this->db->or_like('user_info.phone', $search_value);
            $this->db->or_like('user_info.email', $search_value);
            $this->db->or_like('user_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query  = $this->db->get()->result();
        $total = count($total_query);

        $data = array();
        if (count($response) != 0)
        {
            foreach ($response as $value)
            {

                $log_info_detail = $this->db->select('*')->from('log_info')->where('user_id',$value->id)->get()->row();
                $user_info_detail = $this->db->select('*')->from('user_info')->where('id',$value->id)->get()->result();
                $nestedData["company"] = $value->company;
                $nestedData["email"] = $value->email;
                $nestedData["phone"] = $value->phone;
                $nestedData["address"] = $value->address;

                $class = 'kt-badge--primary';
                if ($value->user_type_status == 1)
                    $class = 'kt-badge--success';
                $nestedData["user_type_status"] = '<span class="kt-badge ' . $class . ' kt-badge--inline kt-badge--pill">' . get_single_b_user_type_status($value->user_type_status) . '</span>';

                $nestedData["status"] = '';
                if(!empty($log_info_detail)) 
                {
                    $check = $log_info_detail->status;
                    if($check==0) 
                    {
                        $nestedData["status"] .=  '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactive</span>';
                    }
                    if($check==1) 
                    {
                        $nestedData["status"] .=  '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Active</span>';
                    }
                }

                
                        $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$this->session->userdata('user_id'))->get()->result();
                        $ids=$user_role_info[0]->role_id;
                        $user_data = $this->db->select('*')
                                                ->from('admin_role_permission_tbl')
                                                ->where('menu_id =',13)
                                                ->where('role_id', $ids)
                                                ->get()->result();
                                                 $nestedData["action"] = '';
                        if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){
                $nestedData["action"] = '
                       <a href="' . base_url() . 'super-admin/wholesaler/edit/' . $value->id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>
                        
                         <div class="dropdown">
                                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="la la-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">';

                                if (!empty($log_info_detail)) 
                                {
                                    $check = $log_info_detail->status;
                                    if($check==0) {
                                      $nestedData["action"] .= '<a href="" onclick="user_status_change('.$value->id.')" class="dropdown-item" title="View"> <i class="la la-check"></i> Activate user </a>';
                                    }
                                    if($check==1) {
                                       $nestedData["action"] .= '<a href="" onclick="user_status_change('.$value->id.')" class="dropdown-item" title="View"> <i class="la la-times"></i> Inactivate user </a>';
                                    }
                                } 

                                $nestedData["action"] .= '<a href="" onclick="user_delete('.$value->id.')" class="dropdown-item" title="delete"><i class="la la-trash"></i> Delete </a>
                                    <a href="'.base_url().'super-admin/wholesaler/company-edit/'.$value->id.'" class="dropdown-item" title="delete"><i class="la la-pencil"></i> Company Detail </a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/category/list/'.$value->id.'"><i class="la la-eye"></i> Category Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/color/list/'.$value->id.'"><i class="la la-eye"></i> Color Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/pattern/list/'.$value->id.'"><i class="la la-eye"></i> Pattern Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/product/list/'.$value->id.'"><i class="la la-eye"></i> Product Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/attribute/list/'.$value->id.'"><i class="la la-eye"></i> Attribute Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/price/list/'.$value->id.'"><i class="la la-eye"></i> Price Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/wholesaler-catalog/group-price/list/'.$value->id.'"><i class="la la-eye"></i> Group price Details</a>
                                </div>
                            </div>';
                            $crt_id =$user_info_detail[0]->created_by;
                            $pck_id=$user_info_detail[0]->package_id;
                            if ($pck_id!=1 || !empty($crt_id)) {
                           $nestedData["action"] .= '
                            <a target="_blank" href="'.base_url().'super_admin/b_user_controller/login_b_user/'.$value->id.'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                              Login
                            </a>';
                            }
                            $nestedData["subscription"] = '';
                          if ($value->subscription_id!=0) {
                            $nestedData["subscription"] = '<a href="'.base_url().'change-wholesaler-subscription-date/'.$value->subscription_id.'/'.$value->id.'" class="btn btn-brand btn-elevate btn-icon-sm">Change Billing Date</a>';
                        }
                        }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];


    }
    
    //    ========== its for sendLink ===============
    public function sendLink_super($user_id, $data, $email, $password) {

        $data['baseurl'] = base_url();
        print_r($data['baseurl']);
        die();
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));
//        echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->b_user_info($user_id);
        $data['username'] = $email;
        $data['password'] = $password;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('b_level/settings/send_user_info', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");

        /* $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) { */

            $this->email->message($mesg);
            $this->email->send();

        // }
    }

    public function get_b_user_list()
    {
        $response = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name")->from('user_info')->where('user_info.user_type', 'b')->where('user_info.created_by', '')/*  ->order_by($order, $order_type)*/ ->group_by('user_info.id')->get()->result();
        return $response;
    }

    function check_column_exist($where, $id_field, $id, $table)
    { $this->db->where($where);
        $this->db->where($id_field . '!=', $id);
        $data = $this->db->get($table)->row();
        return $data;
    }



}
