<?php

class Domain_model extends CI_model
{

    public function search_reserve_domain_list($request)
    {
        $columns = $request['column_value'];
        if (isset($_POST['order']['0']['dir']))
            $order_type = $_POST['order']['0']['dir'];
        if (isset($_POST['order']['0']['column']))
            $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("*");
        $this->db->from('reserve_domain');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('subdomain', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->limit($limit, $offset);

        $response = $this->db->get()->result();

        $this->db->select("*");
        $this->db->from('reserve_domain');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('subdomain', $search_value);
            $this->db->group_end(); //close bracket
        }


        $total_query  = $this->db->get()->result();
        $total = count($total_query);

        $data = array();
        if (count($response) != 0)
        {
            foreach ($response as $value)
            {
                $nestedData["subdomain"] = $value->subdomain;

                $nestedData["action"] = '
                        <a href="' . base_url() . 'super-admin/reserve-domain/edit/' . $value->id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>';
                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];


    }

    public function get_b_user_list()
    {
        $response = $this->db->select("*, CONCAT_WS(' ', first_name, last_name) as full_name")->from('user_info')->where('user_info.user_type', 'b')->where('user_info.created_by', '')/*  ->order_by($order, $order_type)*/ ->group_by('user_info.id')->get()->result();
        return $response;
    }

    function check_column_exist($where, $id_field, $id, $table)
    {
        $this->db->where($where);
        $this->db->where($id_field . '!=', $id);
        $data = $this->db->get($table)->row();

        return $data;

    }


}
