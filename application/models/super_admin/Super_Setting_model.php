<?php

class Super_Setting_model extends CI_model {


 public function get_sms_conf() {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $this->db->order_by('a.gateway_id', 'desc');
        $query = $this->db->get();
        return $query->result();

    }

     public function get_mail_config() {
        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }
    //    =============== its for sms_config_edit ==============
    public function sms_config_edit($gateway_id) {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('a.gateway_id', $gateway_id);
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function sendCstomEmail($email, $message, $isverified = true) {
       $CI = &get_instance();

        $CI->load->model('Common_model');
        $config =$CI->Common_model->mailConfig($this->session->userdata('user_id'), $isverified);

      
        if(isset($config['error'])) {
            return false;
           // die();
        }
        
        $this->load->library('email', $config);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("BMSLINK");
        $this->email->message($message);  

        //$is_send = $this->email->send();
           
            if($this->email->send()) {
                $email_data = array(
                    'from' => $config['smtp_user'],
                    'to' => $email,
                    'message' => $message,
                    'created_by' => $this->session->userdata('user_id'),
                );
                $this->db->insert('custom_email_tbl', $email_data);
                return true;
            } else {
                return false;
            }
        // }
    }

    function user_type_data($user_types)
    {    
    	print_r($user_ids);
    	$this->db->select('*');   
    	$this->db->from('user_info');   
        $this->db->where('user_type',$user_types);          
        $result =  $this->db->get()->result_array();
        return $result;      
   }
   

    public function get_customer_b()
    {
        $query = $this->db->select('*')
            ->from('user_info')
            ->where('user_type','b')
            ->get()->result();        
        return $query;
    }
     public function get_customer_c()
    {
        $query = $this->db->select('*')
            ->from('user_info')
            ->where('user_type','c')
            ->get()->result();      
        return $query;
    }

      public function sendLink_super($user_id, $data, $email, $password) {

        $data['baseurl'] = base_url();
        $CI = &get_instance();
        $CI->load->model('Common_model');

        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));
    // echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->b_user_info($user_id);
        $data['username'] = $email;
        $data['password'] = $password;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('super_admin/setting/send_user_info', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");
// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
// . "\n\nThanks\nMetallica Gifts");
// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");

        /* $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) { */

            $this->email->message($mesg);
            $this->email->send();

        // }

    }
      public function b_user_info($user_id) {
        $query = $this->db->select('*')
                        ->from('log_info a')
                        ->join('user_info b', 'b.id = a.user_id', 'left')
                        ->where('a.row_id', $user_id)
                        ->get()->row();
        return $query;
    }


    function get_userlist($limit, $start)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');
        $this->db->where('user_type', 's');
        $this->db->where('created_by!=', '');
        $this->db->where('created_by',$created_by);
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->result();
        } else {

            return false;
        }
    }

    function get_user_by_id($user_id) {
        $this->db->select("*");
        $this->db->from('user_info a');
        $this->db->join('log_info b', 'b.user_id = a.id');
        $this->db->where('a.user_type', 's');
        $this->db->where('a.created_by!=', '');
        $this->db->where('a.id', $user_id);

        $query = $this->db->get();

        if ($query->num_rows() >= 1) {

            return $query->row();
        } else {

            return false;
        }
    }
     public function user_filter($first_name, $email, $type) {

        $this->db->select("*,CONCAT_WS(' ', first_name, last_name) AS fullname");
        $this->db->from('user_info');

        if ($first_name && $email && $type) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->like('email', $email, 'both');
            $this->db->like('user_type', $type, 'both');
        }
        if ($first_name) {
            $this->db->like('first_name', $first_name, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($email) {
            $this->db->like('email', $email, 'both');
            $this->db->where('created_by!=', '');
        }
        if ($type) {
            $this->db->like('user_type', $type, 'both');
            $this->db->where('created_by!=', '');
        }
        $this->db->where('created_by=', '1');
        $this->db->where('user_type', 's');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }


}
