<?php

class Settings_model extends CI_model {

 public function get_sms_config() {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $this->db->order_by('a.gateway_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
}