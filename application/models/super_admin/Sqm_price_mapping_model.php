<?php

class Sqm_price_mapping_model extends CI_model
{

    public function customer_catalog_sqm_price_mapping_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

            $this->db->select('smm.*,p.product_name,p.created_status,pm.pattern_name');
            $this->db->from('sqm_price_model_mapping_tbl smm');
            $this->db->join('product_tbl p', 'p.product_id=smm.product_id');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("p.product_name", $search_value);
            $this->db->or_like('pm.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->group_start();
            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('p.created_by', $_POST['level_id']);
                $this->db->where('p.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();

            $this->db->where('p.created_by', $_POST['customer_user_id']);
            $this->db->where('p.created_status', 1);
            $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket


        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


            $this->db->select('smm.*,p.product_name,p.created_status,pm.pattern_name');
            $this->db->from('sqm_price_model_mapping_tbl smm');
            $this->db->join('product_tbl p', 'p.product_id=smm.product_id');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id');


        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("p.product_name", $search_value);
            $this->db->or_like('pm.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->group_start();
            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('p.created_by', $_POST['level_id']);
                $this->db->where('p.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();

            $this->db->where('p.created_by', $_POST['customer_user_id']);
            $this->db->where('p.created_status', 1);
            $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket

        $total_query =  $this->db->get()->result();
        //echo $this->db->last_query();die();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                // $sl = 0 + $pagenum;
                // $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $value->category_id)->get()->result();
                // $sl++;

                $nestedData["serial_no"] = ++$sl;
                $nestedData["product_name"] = $value->product_name;
                $nestedData["pattern_name"] = $value->pattern_name;
                $nestedData["sqm_price"] = $value->price;

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


    // public function b_user_catalog_product_list($request)
    // {

    //     $columns = $request['column_value'];
    //     if(isset($_POST['order']['0']['dir']))
    //     $order_type = $_POST['order']['0']['dir'];
    //     if(isset($_POST['order']['0']['column']))
    //     $order = $columns[$_POST['order']['0']['column']];

    //     $limit = $request['length'];
    //     $offset = $request['start'];



    //     $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
    //     $this->db->from('product_tbl a');
    //     $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
    //     $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
    //     $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
    //     $this->db->where('a.created_by',$_POST['b_user_id']);
    //     $this->db->order_by('a.product_id', 'desc');

    //     if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
    //     {
    //         $search_value = $_POST['search']['value'];
    //         $this->db->group_start();
    //         $this->db->like("a.product_name", $search_value);
    //         $this->db->or_like('c.category_name', $search_value);
    //         $this->db->or_like('a.dealer_price', $search_value);
    //         $this->db->group_end(); //close bracket
    //     }
    //     $this->db->limit($limit, $offset);
    //     $response = $this->db->get()->result();


    //     $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
    //     $this->db->from('product_tbl a');
    //     $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
    //     $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
    //     $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
    //     $this->db->where('a.created_by',$_POST['b_user_id']);
    //     $this->db->order_by('a.product_id', 'desc');

    //     if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
    //     {
    //         $search_value = $_POST['search']['value'];
    //         $this->db->group_start();
    //         $this->db->like("a.product_name", $search_value);
    //         $this->db->or_like('c.category_name', $search_value);
    //         $this->db->or_like('a.dealer_price', $search_value);
    //         $this->db->group_end(); //close bracket
    //     }

    //     $total_query =  $this->db->get()->result();
    //     $total = count($total_query);
    //     $data = array();


    //     if (count($response) != 0)
    //     {
    //         $sl = $offset;
    //         foreach ($response as $key => $value)
    //         {
    //             // $sl = 0 + $pagenum;
    //             // $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $value->category_id)->get()->result();
    //             // $sl++;

    //             $nestedData["serial_no"] = ++$sl;
    //             $nestedData["product_name"] = $value->product_name;
    //             $nestedData["category_name"] = $value->category_name;

    //             if ($value->price_style_type == 1) {
    //                 $nestedData["price_style_type"] = "Row/Column style";
    //             } elseif ($value->price_style_type == 2) {
    //                 $nestedData["price_style_type"] = "Price by Sq.ft style";
    //             } elseif ($value->price_style_type == 4) {
    //                 $nestedData["price_style_type"] = "Group Price";
    //             } elseif($value->price_style_type == 3){
    //                 $nestedData["price_style_type"] = "Fixed product Price Style";
    //             }
    //             elseif($value->price_style_type == 5){
    //                 $nestedData["price_style_type"] = "Sqm price style";
    //             }


    //             $nestedData["discount"] = $value->dealer_price;

    //             if ($value->active_status == '1') 
    //             {
    //                  $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
    //             } else {
    //                  $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
    //             }

    //             $data[] = $nestedData;
    //         }
    //     }

    //     return ['data' => $data, 'total' => $total];
    // }

}
