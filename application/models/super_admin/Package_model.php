<?php

class Package_model extends CI_model {

    public function get_package_list()
    {
        $this->db->select("*");
        $this->db->from('hana_package');
        $response = $this->db->get()->result();
        return $response;
    }
     public function get_package_list_wholesaler()
    {
        $this->db->select("*");
        $this->db->from('wholesaler_hana_package');
        $response = $this->db->get()->result();
        return $response;
    }

    public function get_package_option_list()
    {
        $this->db->select("*");
        $this->db->from('hana_package_option');
        $response = $this->db->get()->result();
        return $response;
    }
    public function get_package_option_list_wholesaler()
    {
        $this->db->select("*");
        $this->db->from('wholesaler_package_option');
        $response = $this->db->get()->result();
        return $response;
    }

    function get_single_package($package_id)
    {
        $query = $this->db->select("*")
            ->from('hana_package')
            ->where('id',$package_id)
            ->get()->row();

        return $query;
    }

    function get_wholesaler_single_package($package_id)
    {
        $query = $this->db->select("*")
            ->from('wholesaler_hana_package')
            ->where('id',$package_id)
            ->get()->row();

        return $query;
    }

    function update_package_data($request)
    {
        $update_data['package'] = $request['package'];
        $update_data['installation_price'] = $request['installation_price'];
        $update_data['monthly_price'] = $request['monthly_price'];
        $update_data['yearly_price'] = $request['yearly_price'];
        $update_data['trial_days'] = $request['trial_days'];

        $this->db->where('id', $request['id']);
        $this->db->update('hana_package', $update_data);
        return true;
    }
    
    function update_wholesaler_package_data($request)
    {
        $update_data['package'] = $request['package'];
        $update_data['installation_price'] = $request['installation_price'];
        $update_data['monthly_price'] = $request['monthly_price'];
        $update_data['yearly_price'] = $request['yearly_price'];
        $update_data['trial_days'] = $request['trial_days'];

        $this->db->where('id', $request['id']);
        $this->db->update('wholesaler_hana_package', $update_data);
        return true;
    }

    function update_package_option($request)
    {

            $update_data = array($request['column']=>0);
            $this->db->update('hana_package_option', $update_data);

       foreach($request['package_value'] as $key => $value) 
       {
            $update_data = array($request['column']=>1);
            $this->db->where('package_key', $key);
            $this->db->update('hana_package_option', $update_data);
       }
       
       return true;
    }
    
     function update_wholesaler_package_option($request)
    {

            $update_data = array($request['column']=>0);
            $this->db->update('wholesaler_package_option', $update_data);

       foreach($request['package_value'] as $key => $value) 
       {
            $update_data = array($request['column']=>1);
            $this->db->where('package_key', $key);
            $this->db->update('wholesaler_package_option', $update_data);
       }
       
       return true;
    }


}
