<?php

class Customer_model extends CI_model
{


    //============ its for customer_list method ==============
    public function b_level_customer_list($request)
    {
        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("customer_info.*, CONCAT_WS(' ', customer_info.first_name, customer_info.last_name) as full_name,
        CONCAT_WS(' ', user_info.first_name, user_info.last_name) as b_user_full_name, user_info.package_id as customar_package_id,user_info.subscription_id
        ");
        $this->db->from('customer_info');
        $this->db->join('user_info', 'user_info.id=customer_info.customer_user_id');
        //$this->db->where('user_info.user_type', 'b');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('customer_info.company', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('customer_info.phone', $search_value);
            $this->db->or_like('customer_info.email', $search_value);
            $this->db->or_like('customer_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->group_by('customer_info.customer_id');
        if(!empty($order) && !empty($order_type)) // here order processing
        {
            if($order == 'b_user_full_name')
                $this->db->order_by('user_info.first_name', $order_type);
            elseif($order == 'full_name')
                $this->db->order_by('customer_info.first_name', $order_type);
            else
                $this->db->order_by($order, $order_type);
        }
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("customer_info.*, CONCAT_WS(' ', customer_info.first_name, customer_info.last_name) as full_name,
        CONCAT_WS(' ', user_info.first_name, user_info.last_name) as b_user_full_name, user_info.package_id as customar_package_id,user_info.subscription_id
        ");
        $this->db->from('customer_info');
        $this->db->join('user_info', 'user_info.id=customer_info.customer_user_id');
        //$this->db->where('user_info.user_type', 'b');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('customer_info.company', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('customer_info.phone', $search_value);
            $this->db->or_like('customer_info.email', $search_value);
            $this->db->or_like('customer_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();

        if (count($response) != 0)
        {
            foreach ($response as $value)
            {
                $log_info_detail = $this->db->select('*')->from('log_info')->where('user_id',$value->customer_user_id)->get()->row();
                 $user_info_details = $this->db->select('*')->from('user_info')->where('id',$value->customer_user_id)->get()->result();

                $nestedData["company"] = $value->company;
                $nestedData["b_user_full_name"] = $value->b_user_full_name;
                $nestedData["full_name"] = $value->full_name;
                $nestedData["phone"] = $value->phone;
                $nestedData["email"] = $value->email;
                $nestedData["address"] = $value->address;
                
                    $nestedData["status"] = '';
                    if(!empty($log_info_detail)) 
                    {
                        $check = $log_info_detail->status;
                        if($check==0) 
                        {
                            $nestedData["status"] .=  '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactive</span>';
                        }
                        if($check==1) 
                        {
                            $nestedData["status"] .=  '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Active</span>';
                        }
                    }

                    $nestedData["action"] = '<div class="dropdown">
                                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="la la-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">';
                                    if (!empty($log_info_detail)) 
                                    {
                                        $check = $log_info_detail->status;
                                        if($check==0) {
                                          $nestedData["action"] .= '<a onclick="user_status_change('.$value->customer_user_id.')" class="dropdown-item" title="View"> <i class="la la-check"></i> Activate user </a>';
                                        }
                                        if($check==1) {
                                           $nestedData["action"] .= '<a onclick="user_status_change('.$value->customer_user_id.')" class="dropdown-item" title="View"> <i class="la la-times"></i> Inactivate user </a>';
                                        }
                                    }

                                            $nestedData["action"] .= '<a  onclick="user_delete('.$value->customer_user_id.')" class="dropdown-item" title="delete"><i class="la la-trash"></i> Delete </a>';

                                            $nestedData["action"] .= '<a href="'.base_url().'super-admin/customer/company-edit/'.$value->customer_user_id.'" class="dropdown-item" title="delete"><i class="la la-pencil"></i> Company Detail </a>
                                    </div>
                            </div>'; 

                $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$this->session->userdata('user_id'))->get()->result();
                        $ids=$user_role_info[0]->role_id;
                        //print_r($user_role_info);
                        $user_data = $this->db->select('*')
                                                ->from('admin_role_permission_tbl')
                                                ->where('menu_id =',14)
                                                ->where('role_id', $ids)
                                                ->get()->result();
                                                 $nestedData["action"] = '';
                                                 $nestedData["subscription"] = '';

                if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){
                $nestedData["action"] .= '
                        <a href="' . base_url() . 'super-admin/customer/edit/' . $value->customer_id . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>';

                    $crt_id=$user_info_details[0]->created_by;
                    if ($value->customar_package_id!=1 || !empty($crt_id)){
                    $nestedData["action"] .= '
                        <a target="_blank" href="'.base_url().'super_admin/customer_controller/login_c_user/'.$value->customer_user_id.'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                              Login
                        </a>';
                        }


                $nestedData["subscription"] = '';
                    if ($value->subscription_id!=0) {
                        if($value->customar_package_id==2) 
                        {
                            $nestedData["subscription"] = '<a href="'.base_url().'change-subscription-date/'.$value->subscription_id.'/'.$value->customer_user_id.'" class="btn btn-brand btn-elevate btn-icon-sm">Change Billing Date</a>';
                        }
                        if($value->customar_package_id==3) 
                        {
                            $nestedData["subscription"] = '<a href="'.base_url().'change-subscription-date/'.$value->subscription_id.'/'.$value->customer_user_id.'" class="btn btn-brand btn-elevate btn-icon-sm">Change Billing Date</a>';
                        }
                    }
                    
           }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

    public function headcode()
    {
        $query = $this->db->query("SELECT MAX(HeadCode) as HeadCode FROM b_acc_coa WHERE HeadLevel='4' And HeadCode LIKE '1020301-%' ORDER BY row_id DESC LIMIT 1");
        return $query->row();
    }

    function get_single_customer_by_id($id)
    {
        $response = $this->db->select('customer_info.*,log_info.email as username, 
            customer_phone_type_tbl.phone_type,
            customer_phone_type_tbl.phone')

            ->from('customer_info')
            ->join('log_info','log_info.user_id=customer_info.customer_id','left')
            ->join('customer_phone_type_tbl','customer_phone_type_tbl.customer_id=customer_info.customer_id','left')
            ->where('customer_info.customer_id',$id)
            ->get()->row();

        return $response;
    }

    public function customer_catalog_list($request)
    {
        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("customer_info.*, CONCAT_WS(' ', customer_info.first_name, customer_info.last_name) as full_name");
        $this->db->from('customer_info');
        $this->db->join('user_info', 'user_info.id=customer_info.level_id');
        $this->db->where('customer_info.customer_type', 'business');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('customer_info.company', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('customer_info.phone', $search_value);
            $this->db->or_like('customer_info.email', $search_value);
            $this->db->or_like('customer_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->group_by('customer_info.customer_id');
        if(!empty($order) && !empty($order_type)) // here order processing
        {
            if($order == 'b_user_full_name')
                $this->db->order_by('user_info.first_name', $order_type);
            elseif($order == 'full_name')
                $this->db->order_by('customer_info.first_name', $order_type);
            else
                $this->db->order_by($order, $order_type);
        }
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();

        $this->db->select("customer_info.*, CONCAT_WS(' ', customer_info.first_name, customer_info.last_name) as full_name,");
        $this->db->from('customer_info');
        $this->db->join('user_info', 'user_info.id=customer_info.level_id');
        $this->db->where('customer_info.customer_type', 'business');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('customer_info.company', $search_value);
            $this->db->or_like('customer_info.first_name', $search_value);
            $this->db->or_like('customer_info.last_name', $search_value);
            $this->db->or_like('customer_info.phone', $search_value);
            $this->db->or_like('customer_info.email', $search_value);
            $this->db->or_like('customer_info.address', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
            foreach ($response as $value)
            {
                $nestedData["company"] = $value->company;
                $nestedData["full_name"] = $value->full_name;
                $nestedData["phone"] = $value->phone;
                $nestedData["email"] = $value->email;
                $nestedData["address"] = $value->address;
                $nestedData["action"] = '
                        <div class="dropdown">
                                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                    <i class="la la-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/catagory/list/'.$value->customer_id.'"><i class="la la-eye"></i> Category Details</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/color/list/'.$value->customer_id.'"><i class="la la-leaf"></i> Color</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/pattern/list/'.$value->customer_id.'"><i class="la la-leaf"></i> Pattern</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/product/list/'.$value->customer_id.'"><i class="la la-leaf"></i> Product</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/attribute/list/'.$value->customer_id.'"><i class="la la-leaf"></i> Attribute</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/price/list/'.$value->customer_id.'"><i class="la la-dollar"></i> Price</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/group-price/list/'.$value->customer_id.'"><i class="la la-dollar"></i> Group Price</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/sqm-price-mapping/list/'.$value->customer_id.'"><i class="la la-dollar"></i> Sqm Price Mapping</a>
                                    <a class="dropdown-item" href="'.base_url().'super-admin/customer-catalog/group-price-mapping/list/'.$value->customer_id.'"><i class="la la-dollar"></i> Group Price Mapping</a>

                                </div>
                            </div>
                        ';

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


     public function get_customer()
    {
        echo"string";
        die();
        
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('level_id', $level_id)
            ->where('created_by', $this->session->userdata('user_id'))
            ->order_by('customer_id', 'desc')
            ->get()->result();
        return $query;
    }


}
