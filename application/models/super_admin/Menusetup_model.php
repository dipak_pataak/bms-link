<?php

class Menusetup_model extends CI_model {

    public function admin_menu_setuplist($offset, $limit) {
        $query = $this->db->select('*')
                        ->from('admin_menusetup_tbl')
                        ->where('created_by', $this->session->userdata('user_id'))
                        ->order_by('id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

     public function checkMenuSetup($menu_name, $module) {
     $query = $this->db->select('*')->from('admin_menusetup_tbl')
                        ->where('menu_title', $menu_name)
                        ->where('module', $module)
                        ->get()->result();
        return $query;
    }

    public function menusetup_save($allMenu){
        $this->db->insert('admin_menusetup_tbl', $allMenu);
        $this->session->set_flashdata('success', "<div class='alert alert-success'>Menu save successfully!");
    }
    public function single_menu_edit($id = null) {
        $query = $this->db->select('*')
                        ->from('admin_menusetup_tbl')
                        ->where('id', $id)
                        ->get()->row();
        return $query;

    }

    public function menu_csv_data($offset,$limit) {
    $this->db->select('bms.id, bms.menu_title, bms.korean_name, bms.page_url, bms.module, bms.parent_menu');
    $this->db->from('admin_menusetup_tbl  AS bms');
    $this->db->where('created_by', $this->session->userdata('user_id'));
    $this->db->order_by('id', 'desc');
    $this->db->limit($limit, $offset);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
        $results = $query->result_array();
        foreach($results AS $key => $row) {
            $results[$key]['menu_title'] = str_replace("_", " ", ucfirst(@$row['menu_title']));
            $results[$key]['korean_name'] = str_replace("_", " ", ucfirst(@$row['korean_name']));
            $parent_menu = $this->db->select('*')->where('id', $row['parent_menu'])->get('admin_menusetup_tbl')->row();
            $results[$key]['parent_menu'] = str_replace("_", " ", ucfirst(@$parent_menu->menu_title));
        }
        return $results;
    }
    return false;
}


//    ========== its for b_level_menu_search ================
    public function super_admin_menu_search($keyword) {
        $this->db->select('*');
        $this->db->from('admin_menusetup_tbl');
        $this->db->like('menu_title', $keyword, 'both');
        // $this->db->or_like('page_url', $keyword, 'both');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $query = $this->db->get();
       // echo $this->db->last_query();die;
        return $query->result();
    }
    


      public function menu_type_wise_parent_menu() {
        print_r($_POST);
        die();
        $menu_type = $this->input->post('type_id');
        $parent_menu = $this->db->select('*')->from('admin_menusetup_tbl')->where('menu_type', $menu_type)->get()->result();
        $arr = array();
        $i = 0;
        foreach ($parent_menu as $single) {
            $arr[$i]['id'] = $single->id;
            $arr[$i]['menu_title'] = str_replace("_", " ", ucfirst($single->menu_title));
            $i++;
        }
        echo json_encode($arr);
    }


} 
