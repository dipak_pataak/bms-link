<?php

class Color_model extends CI_model
{

      public function customer_catalog_color_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->from('color_tbl');
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT');
        // $this->db->where('color_tbl.created_by',$_POST['level_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("color_tbl.color_name", $search_value);
            $this->db->or_like('color_tbl.color_number', $search_value);
            $this->db->or_like('pattern_model_tbl.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            
            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('color_tbl.created_by', $_POST['level_id']);
                $this->db->where('color_tbl.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('color_tbl.created_by', $_POST['customer_user_id']);
            $this->db->where('color_tbl.created_status', 1);
            $this->db->group_end(); //close bracket



        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();




        $this->db->from('color_tbl');
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT');
        //$this->db->where('color_tbl.created_by',$_POST['level_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("color_tbl.color_name", $search_value);
            $this->db->or_like('color_tbl.color_number', $search_value);
            $this->db->or_like('pattern_model_tbl.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('color_tbl.created_by', $_POST['level_id']);
                $this->db->where('color_tbl.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();

            $this->db->where('color_tbl.created_by', $_POST['customer_user_id']);
            $this->db->where('color_tbl.created_status', 1);
            $this->db->group_end(); //close bracket

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();



        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                $nestedData["serial_no"] = ++$sl;
                $nestedData["color_name"] = $value->color_name;
                $nestedData["color_number"] = $value->color_number;
                $nestedData["pattern_name"] = $value->pattern_name;

                if ($value->status == '1') 
                {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


    public function b_user_catalog_color_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->from('color_tbl');
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT');
        $this->db->where('color_tbl.created_by',$_POST['b_user_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("color_tbl.color_name", $search_value);
            $this->db->or_like('color_tbl.color_number', $search_value);
            $this->db->or_like('pattern_model_tbl.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->from('color_tbl');
        $this->db->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT');
        $this->db->where('color_tbl.created_by',$_POST['b_user_id']);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("color_tbl.color_name", $search_value);
            $this->db->or_like('color_tbl.color_number', $search_value);
            $this->db->or_like('pattern_model_tbl.pattern_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();



        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                $nestedData["serial_no"] = ++$sl;
                $nestedData["color_name"] = $value->color_name;
                $nestedData["color_number"] = $value->color_number;
                $nestedData["pattern_name"] = $value->pattern_name;

                if ($value->status == '1') 
                {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

}
