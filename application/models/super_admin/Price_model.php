<?php

class Price_model extends CI_model
{

    public function customer_catalog_price_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select('row_column.*, row_column.active_status as status');
        $this->db->from('row_column');
        $this->db->where('row_column.style_type', 1);
        //$this->db->where('row_column.create_by', $_POST['level_id']);
        $this->db->order_by('row_column.style_id', 'desc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("row_column.style_name", $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('row_column.create_by', $_POST['level_id']);
                $this->db->where('row_column.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('row_column.create_by', $_POST['customer_user_id']);
            $this->db->where('row_column.created_status', 1);
            $this->db->group_end(); //close bracket
                      
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select('row_column.*, row_column.active_status as status');
        $this->db->from('row_column');
        $this->db->where('row_column.style_type', 1);
        $this->db->where('row_column.create_by', $_POST['level_id']);
        $this->db->order_by('row_column.style_id', 'desc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("row_column.style_name", $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('row_column.create_by', $_POST['level_id']);
                $this->db->where('row_column.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('row_column.create_by', $_POST['customer_user_id']);
            $this->db->where('row_column.created_status', 1);
            $this->db->group_end(); //close bracket

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
             $sl = $offset;
            foreach ($response as $key => $value)
            {

                $nestedData["serial_no"] = ++$sl;
                $nestedData["price_sheet_name"] = $value->style_name;

                    $this->db->select('*');
                    $this->db->from('product_tbl');
                    $this->db->where('price_rowcol_style_id', $value->style_id);
                    $results = $this->db->get()->result();
                    
                    $nestedData["assigned_product"] = '';
                    if (!empty($results)) 
                    {
                            $nestedData["assigned_product"] .="<ul>";
                            $k = 0;
                            foreach ($results as $result) 
                            {
                                $k++;
                                $nestedData["assigned_product"] .="<li>" . $k . ") " . $result->product_name . "</li>";
                            }
                            $nestedData["assigned_product"] .="</ul>";
                    }
                    else 
                    {
                        $nestedData["assigned_product"] = '<p>The products will be assigned at the later stage</p>';
                    }

                    
                    // if (isset($value->product_name) && !empty($value->product_name)) 
                    // {
                    //     $nestedData["assigned_product"] = $value->product_name;  
                    // }else{
                    //     $nestedData["assigned_product"] = '';
                    // }


                if ($value->status==1) 
                {
                    $nestedData["status"]  = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                }else{
                    $nestedData["status"]  = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

    public function b_user_catalog_price_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select('row_column.*, row_column.active_status as status');
        //$this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        //$this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 1);
        $this->db->where('row_column.create_by', $_POST['b_user_id']);
        $this->db->order_by('row_column.style_id', 'desc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("row_column.style_name", $search_value);
            //$this->db->or_like('product_tbl.product_name', $search_value);
            $this->db->group_end(); //close bracket
        }
                      
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();



        $this->db->select('row_column.*, row_column.active_status as status');
        //$this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        //$this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 1);
        $this->db->where('row_column.create_by', $_POST['b_user_id']);
        $this->db->order_by('row_column.style_id', 'desc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("row_column.style_name", $search_value);
            //$this->db->or_like('product_tbl.product_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
             $sl = $offset;
            foreach ($response as $key => $value)
            {

                $nestedData["serial_no"] = ++$sl;
                $nestedData["price_sheet_name"] = $value->style_name;

                    $this->db->select('*');
                    $this->db->from('product_tbl');
                    $this->db->where('price_rowcol_style_id', $value->style_id);
                    $results = $this->db->get()->result();
                    
                    $nestedData["assigned_product"] = '';
                    if (!empty($results)) 
                    {
                            $nestedData["assigned_product"] .="<ul>";
                            $k = 0;
                            foreach ($results as $result) 
                            {
                                $k++;
                                $nestedData["assigned_product"] .="<li>" . $k . ") " . $result->product_name . "</li>";
                            }
                            $nestedData["assigned_product"] .="</ul>";
                    }
                    else 
                    {
                        $nestedData["assigned_product"] = '<p>The products will be assigned at the later stage</p>';
                    }

                    // if (isset($value->product_name) && !empty($value->product_name)) 
                    // {
                    //     $nestedData["assigned_product"] = $value->product_name;  
                    // }else{
                    //     $nestedData["assigned_product"] = '';
                    // }

                if ($value->status==1) 
                {
                    $nestedData["status"]  = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                }else{
                    $nestedData["status"]  = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

}
