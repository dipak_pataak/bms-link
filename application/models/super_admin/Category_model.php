<?php

class Category_model extends CI_model
{

    public function customer_catalog_category_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select('t1.*,t2.category_name AS parent_category_name');
        $this->db->from('category_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't1.parent_category = t2.category_id', 'left');
        $this->db->order_by('t1.category_id', 'desc');


        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('t1.category_name', $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }


            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('t1.created_by', $_POST['level_id']);
                $this->db->where('t1.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('t1.created_by', $_POST['customer_user_id']);
            $this->db->where('t1.created_status', 1);
            $this->db->group_end(); //close bracket



        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select('t1.*,t2.category_name AS parent_category_name');
        $this->db->from('category_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't1.parent_category = t2.category_id', 'left');
        $this->db->order_by('t1.category_id', 'desc');


        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('t1.category_name', $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }


            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('t1.created_by', $_POST['level_id']);
                $this->db->where('t1.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('t1.created_by', $_POST['customer_user_id']);
            $this->db->where('t1.created_status', 1);
            $this->db->group_end(); //close bracket


        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();



        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                // $sl = 0 + $pagenum;
                // $parent_category = $this->db->select('*')->where('category_id', $value->parent_category)->get('category_tbl')->row();
                $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $value->category_id)->get()->result();


                $nestedData["serial_no"] = ++$sl;
                $nestedData["category_name"] = $value->category_name;

                if(!empty($value->parent_category_name)) 
                {
                    $nestedData["parent_category"] = $value->parent_category_name;
                } else {
                    $nestedData["parent_category"] = "None";
                }
                $nestedData["description"] = $value->description;

                if ($assigned_products) {
                    foreach ($assigned_products as $product) {
                        $product_name =  "<ul><li>".$product->product_name."</li></ul";
                        $nestedData["assign_product"] = $product_name;
                    }
                } else {
                   $nestedData["assign_product"] = "The products will be assigned at the later stage";
                }

                if ($value->status == '1') 
                {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


    public function b_user_catalog_category_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select('t1.*,t2.category_name AS parent_category_name');
        $this->db->from('category_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't1.parent_category = t2.category_id', 'left');
        $this->db->order_by('t1.category_id', 'desc');

        $this->db->where('t1.created_by', $_POST['b_user_id']);
        $this->db->where('t1.created_status', 0);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('t1.category_name', $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();




        $this->db->select('t1.*,t2.category_name AS parent_category_name');
        $this->db->from('category_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't1.parent_category = t2.category_id', 'left');
        $this->db->order_by('t1.category_id', 'desc');

        $this->db->where('t1.created_by', $_POST['b_user_id']);
        $this->db->where('t1.created_status', 0);

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like('t1.category_name', $search_value);
            $this->db->or_like('t2.category_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();



        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
                // $sl = 0 + $pagenum;
                // $parent_category = $this->db->select('*')->where('category_id', $value->parent_category)->get('category_tbl')->row();
                $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $value->category_id)->get()->result();


                $nestedData["serial_no"] = ++$sl;
                $nestedData["category_name"] = $value->category_name;

                if(!empty($value->parent_category_name)) 
                {
                    $nestedData["parent_category"] = $value->parent_category_name;
                } else {
                    $nestedData["parent_category"] = "None";
                }
                $nestedData["description"] = $value->description;

                if ($assigned_products) {
                    foreach ($assigned_products as $product) {
                        $product_name =  "<ul><li>".$product->product_name."</li></ul";
                        $nestedData["assign_product"] = $product_name;
                    }
                } else {
                   $nestedData["assign_product"] = "The products will be assigned at the later stage";
                }

                if ($value->status == '1') 
                {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>';
                } else {
                     $nestedData["status"] = '<span class="kt-badge kt-badge--danger kt-badge--inline">Inactive</span>';
                }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

}
