<?php

class Attribute_model extends CI_model
{

    public function customer_catalog_attribute_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];


        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');
        //$this->db->where('attribute_tbl.created_by',$_POST['level_id']);
        $this->db->order_by('attribute_tbl.position', 'asc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("category_tbl.category_name", $search_value);
            $this->db->or_like('attribute_tbl.attribute_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('attribute_tbl.created_by', $_POST['level_id']);
                $this->db->where('attribute_tbl.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('attribute_tbl.created_by', $_POST['customer_user_id']);
            $this->db->where('attribute_tbl.created_status', 1);
            $this->db->group_end(); //close bracket
                      
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();



        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');
        //$this->db->where('attribute_tbl.created_by',$_POST['level_id']);
        $this->db->order_by('attribute_tbl.position', 'asc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("category_tbl.category_name", $search_value);
            $this->db->or_like('attribute_tbl.attribute_name', $search_value);
            $this->db->group_end(); //close bracket
        }

            if (!empty($_POST['level_id'])) {
                $this->db->group_start();
                $this->db->where('attribute_tbl.created_by', $_POST['level_id']);
                $this->db->where('attribute_tbl.created_status', 0);
                $this->db->group_end(); //close bracket
            }

            if(empty($_POST['customer_user_id']))
            $this->db->group_start();
            else
            $this->db->or_group_start();


            $this->db->where('attribute_tbl.created_by', $_POST['customer_user_id']);
            $this->db->where('attribute_tbl.created_status', 1);
            $this->db->group_end(); //close bracket

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
              if ($value->attribute_type) {
                // get option
                $option = "SELECT * FROM attr_options WHERE attribute_id = $value->attribute_id";
                $option_result = $this->db->query($option)->result();
            }


                $nestedData["serial_no"] = ++$sl;
                $nestedData["category_name"] = $value->category_name;
                $nestedData["attribute_name"] = $value->attribute_name;
                $nestedData["option"]  = '';
                
                 if ($option_result != NULL) {
                    foreach ($option_result as $key => $op) {
                        $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                        $option_option_result = $this->db->query($option_option)->result();

                 
        $nestedData["option"] .=     "<li>". $op->option_name."
                                        <ul>";
                                    
                                    foreach ($option_option_result as $key => $opop) {
                                        $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                                    
        $nestedData["option"] .=                "<li>
                                                    =>".$opop->op_op_name."
                                                    <ul>";
                                                
                                                foreach ($opopops as $key => $opopop) {
                                                    $opopopops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();
                                                    
        $nestedData["option"] .=                        "<li>";
                                                     if($opopop->att_op_op_op_type == 2){
        $nestedData["option"] .=                        "=>". @$opopop->att_op_op_op_name;
                                                       }else{
        $nestedData["option"] .=                        "=>". @$opopop->att_op_op_op_name; 
                                                     }

                                                     foreach ($opopopops as $key => $opopopop) { 
        $nestedData["option"] .=                        "<li>
                                                        =>". @$opopopop->att_op_op_op_op_name."
                                                        </li>";
                                                     } 
        $nestedData["option"] .=                        "</li>";
                                         } 
        $nestedData["option"] .=                    "</ul>
                                            </li>";
                                 } 
        $nestedData["option"] .=        "</ul>
                                    </li>";
                            
                        }
                    }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }


    public function b_user_catalog_attribute_list($request)
    {

        $columns = $request['column_value'];
        if(isset($_POST['order']['0']['dir']))
        $order_type = $_POST['order']['0']['dir'];
        if(isset($_POST['order']['0']['column']))
        $order = $columns[$_POST['order']['0']['column']];

        $limit = $request['length'];
        $offset = $request['start'];

        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');
        $this->db->where('attribute_tbl.created_by',$_POST['b_user_id']);
        $this->db->order_by('attribute_tbl.position', 'asc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("category_tbl.category_name", $search_value);
            $this->db->or_like('attribute_tbl.attribute_name', $search_value);
            $this->db->group_end(); //close bracket
        }
                      
        $this->db->limit($limit, $offset);
        $response = $this->db->get()->result();


        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');
        $this->db->where('attribute_tbl.created_by',$_POST['b_user_id']);
        $this->db->order_by('attribute_tbl.position', 'asc');

        if (isset($_POST['search']['value']) && !empty($_POST['search']['value']))
        {
            $search_value = $_POST['search']['value'];
            $this->db->group_start();
            $this->db->like("category_tbl.category_name", $search_value);
            $this->db->or_like('attribute_tbl.attribute_name', $search_value);
            $this->db->group_end(); //close bracket
        }

        $total_query =  $this->db->get()->result();
        $total = count($total_query);
        $data = array();


        if (count($response) != 0)
        {
            $sl = $offset;
            foreach ($response as $key => $value)
            {
              if ($value->attribute_type) {
                // get option
                $option = "SELECT * FROM attr_options WHERE attribute_id = $value->attribute_id";
                $option_result = $this->db->query($option)->result();
            }


                $nestedData["serial_no"] = ++$sl;
                $nestedData["category_name"] = $value->category_name;
                $nestedData["attribute_name"] = $value->attribute_name;
                $nestedData["option"]  = '';
                
                 if ($option_result != NULL) {
                    foreach ($option_result as $key => $op) {
                        $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                        $option_option_result = $this->db->query($option_option)->result();

                 
        $nestedData["option"] .=     "<li>". $op->option_name."
                                        <ul>";
                                    
                                    foreach ($option_option_result as $key => $opop) {
                                        $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                                    
        $nestedData["option"] .=                "<li>
                                                    =>".$opop->op_op_name."
                                                    <ul>";
                                                
                                                foreach ($opopops as $key => $opopop) {
                                                    $opopopops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();
                                                    
        $nestedData["option"] .=                        "<li>";
                                                     if($opopop->att_op_op_op_type == 2){
        $nestedData["option"] .=                        "=>". @$opopop->att_op_op_op_name;
                                                       }else{
        $nestedData["option"] .=                        "=>". @$opopop->att_op_op_op_name; 
                                                     }

                                                     foreach ($opopopops as $key => $opopopop) { 
        $nestedData["option"] .=                        "<li>
                                                        =>". @$opopopop->att_op_op_op_op_name."
                                                        </li>";
                                                     } 
        $nestedData["option"] .=                        "</li>";
                                         } 
        $nestedData["option"] .=                    "</ul>
                                            </li>";
                                 } 
        $nestedData["option"] .=        "</ul>
                                    </li>";
                            
                        }
                    }

                $data[] = $nestedData;
            }
        }

        return ['data' => $data, 'total' => $total];
    }

}
