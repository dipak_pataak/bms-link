<?php

class Appointment_model extends CI_model {

//    ========= its for get_appointment_info ================
    public function get_appointment_info($level_id) {


        $query = $this->db->select('*,COUNT(appointment_date)as title')
                ->from('appointment_calendar')
                ->where('c_level_id', $level_id)
                ->group_by('appointment_date')
                ->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
