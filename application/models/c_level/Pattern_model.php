<?php

class Pattern_model extends CI_model {

    function __construct() {
        // Set table name
        $this->table = 'pattern_model_tbl';
        // Set orderable column fields
        $this->column_order = array(null,null, 'a.pattern_name', 'b.category_name');
        // Set searchable column fields
        $this->column_search = array('a.pattern_name', 'b.category_name');
        // Set default order
        $this->order = array('a.pattern_model_id' => 'desc');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
    }



//========= its for get_patern_model =============
    public function get_patern_model() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
                        ->from('pattern_model_tbl')
                        ->where('status', 1)
                    ->where('created_by', $level_id)
                        ->order_by('pattern_name', 'asc')
                        ->get()->result();
        return $query;
    }



}
