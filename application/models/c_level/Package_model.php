<?php

class Package_model extends CI_model {

    public function get_package_list() {
        
        $query = $this->db->select('*')->from('hana_package')
                          ->get()->result();
        return $query;
    }

    public function get_package_option_list() {
        
        $query = $this->db->select('*')->from('hana_package_option')
                          ->get()->result();
        return $query;
    }

    public function get_package_transaction_list() {
        $user_id=$this->session->userdata('user_id');
        

        $query = $this->db->select('package_transaction.*,hana_package.package')
        ->from('package_transaction')
        ->where('user_id',$user_id)
        ->order_by('package_transaction.id','DESC')
        ->join('hana_package','hana_package.id=package_transaction.package_id')
        ->get()->result();
        return $query;
    }


}
