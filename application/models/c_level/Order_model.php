<?php

class Order_model extends CI_model
{

    public function smsSend($dataArray)
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $info = (object) $dataArray;

        $customer_info = $this->db->where('customer_id', $info->customer_id)->get('customer_info')->row();

        if ($customer_info->phone != NULL) {

            $this->load->library('twilio');
            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                $name = @$customer_info->first_name . ' ' . @$customer_info->last_name;

                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $main_b_id)->where('is_verify', '1')->where('default_status', 1)->get()->row();

                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $customer_info->phone;
                $message = $info->message;
                $response = $this->twilio->sms($from, $to, $message);
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
            }
            return 1;
        }
    }

    public function send_link($orderData)
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by', $main_b_id);
        $this->db->limit('1');
        $data['get_mail_config'] = $this->db->get()->result();

        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));


        $data['orderData'] = (object) $orderData;
        $data['baseurl'] = base_url();

        // customer information
        $data['customer_info'] = $this->db->where('customer_id', $data['orderData']->customer_id)->get('customer_info')->row();

        // company profile
        $data['company_profile'] = $this->db->select('*')
            ->where('user_id', $data['customer_info']->level_id)
            ->get('company_profile')->row();
        // order data
        $data['orderd'] = $this->get_orderd_by_id($data['orderData']->order_id);
        // order details
        $data['order_details'] = $this->get_orderd_details_by_id($data['orderData']->order_id);
        
        // Load Html template
        $subject = "Invoice Copy for ".$data['company_profile']->company_name." Order ID ". $data['orderd']->order_id ;
        $mesg = $this->load->view('c_level/invo', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], $data['company_profile']->company_name);
        $this->email->to($data['customer_info']->email);
        $this->email->subject($subject);

        /* $fp = @fsockopen(base_url(), 465, $errno, $errstr);
        
        if ($fp) { */

        $this->email->message($mesg);
        $this->email->send();

        // }


        if ($data['customer_info']->phone != NULL) {

            $this->load->library('twilio');

            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                $name = @$data['customer_info']->first_name . ' ' . @$data['customer_info']->last_name;

                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $data['customer_info']->phone;
                $message = 'Hi! ' . $name . ' Order Successfully. OrderId ' . @$data['orderData']->order_id;
                $response = $this->twilio->sms($from, $to, $message);
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
            }
        }

        return 1;
    }

    public function customer_info($log_id)
    {

        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('customer_id', $log_id)
            ->get()->row();
        return $query;
    }

    //============ its for get_customer method ==============
    public function get_customer()
    {


        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('level_id', $level_id)
            ->order_by('customer_id', 'desc')
            ->get()->result();
        return $query;
    }

    //========= its for get category =============
    public function get_category($status=0)
    {


        $main_b_id = $this->session->userdata('main_b_id');

        if($status==1) 
        {
            $main_b_id = '';  // for wholsaler Connection
        }

        $this->db->select('*');
        $this->db->from('category_tbl');
        $this->db->where('parent_category', 0);
        $this->db->where('status', 1);
        // $this->db->where('created_by', $main_b_id);

        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('created_by', $main_b_id);
            $this->db->where('created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if (empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('created_by', $user_id);
        $this->db->where('created_status', 1);
        $this->db->group_end(); //close bracket


        $this->db->order_by('category_name', 'asc');
        $query = $this->db->get()->result();
        return $query;
    }

    function get_customer_category()
    {

        $main_b_id = $this->session->userdata('main_b_id');

        $query = $this->db->select('a.*')
            ->from('category_tbl a')
            ->where('a.parent_category', 0)
            ->where('a.status', 1)
            ->where('a.created_by', $main_b_id)
            ->get()->result();

        /*$query1 = $this->db->last_query();

        $this->db->select('c.*')
            ->from('b_user_catalog_products a')
            ->join('b_user_catalog_request', 'b_user_catalog_request.request_id=a.request_id')
            ->join('product_tbl b', 'b.product_id=a.product_id')
            ->join('category_tbl c', 'c.category_id=b.category_id')
            ->where('b_user_catalog_request.requested_by', $main_b_id)
            ->where('a.approve_status', 1)
            ->group_by('c.category_id')
            ->get()->result();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1 . " UNION " . $query2);*/

        return $query;
    }

    function get_customer_product_by_category($category_id)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $query = $this->db->select('product_id,category_id,product_name')
            ->where('category_id', $category_id)
            ->where('active_status', 1)
            ->where('created_by', $main_b_id)
            ->get('product_tbl')->result();


        /*  $query1 = $this->db->last_query();

        $this->db->select('a.product_id,a.category_id,a.product_name')
            ->from('product_tbl a')
            ->join('b_user_catalog_products b', 'b.product_id=a.product_id')
            ->join('b_user_catalog_request', 'b_user_catalog_request.request_id=b.request_id')
            ->where('b_user_catalog_request.requested_by', $main_b_id)
            ->where('a.category_id', $category_id)
            ->where('b.approve_status', 1)
            ->group_by('a.product_id')
            ->get()->result();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1 . " UNION " . $query2);*/

        return $query;
    }

    //========= its for get_patern_model =============
    public function get_patern_model()
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $query = $this->db->select('*')
            ->from('pattern_model_tbl')
            ->where('status', 1)
            ->where('created_by', $main_b_id)
            //                        ->where('parent_category', 0)
            ->order_by('pattern_name', 'asc')
            ->get()->result();
        return $query;
    }

    //========= its for get_patern_model =============

    public function get_product1()
    {

        $query = $this->db->select('*')
            ->from('product_tbl')
            ->where('parent_category', 0)
            ->where('active_status', 1)
            ->order_by('product_name', 'asc')
            ->get()->result();
        return $query;
    }
    public function get_product()
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id  = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('*')
            ->from('product_tbl')
            ->where('active_status', 1)
            ->where('created_by', $level_id)
            //                        ->where('parent_category', 0)
            ->order_by('product_name', 'asc')
            ->get()->result();
        return $query;
    }

    public function get_all_cancel_orderd($search = NULL)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }


        if ($search->order_id != NULL) {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }
        $this->db->where('quatation_tbl.level_id', $level_id);

        $this->db->where('quatation_tbl.order_stage', 6);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();
        return $query;
    }

    public function get_all_invoice_orderd($search = NULL, $offset = NULL, $limit = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,phone");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_stage != NULL) {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_id != NULL) {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }

        if ($search->search_daterange != '') {
            $daterange_date = $search->search_daterange;
            $range_date = explode(" - ", $daterange_date);
            $date1 = $range_date[0];
            $date2 = $range_date[1];

            $date11 = explode("/", $date1);
            $date22 = explode("/", $date2);

            if (count($date11) > 2 && count($date22) > 2) {
                $from_date = $date11[2] . "-" . $date11[0] . "-" . $date11[1];
                $to_date = $date22[2] . "-" . $date22[0] . "-" . $date22[1];

                $this->db->where("quatation_tbl.order_date >=", $from_date);
                $this->db->where("quatation_tbl.order_date <=", $to_date);
            }
        }

        $this->db->where('quatation_tbl.created_by', $this->session->userdata('user_id'));

        // $this->db->where('quatation_tbl.order_stage', 2);

        $this->db->limit($offset, $limit);


        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }



    public function get_all_orderd($search = NULL, $offset = NULL, $limit = NULL)
    {
        
        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL) {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_date != NULL) {

            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        if (@$search->user_id != NULL) {

            $this->db->where('quatation_tbl.created_by', $search->user_id);
        }

        if (@$search->level_id != NULL) {

            $this->db->where('quatation_tbl.level_id', $search->level_id);
        }

        if (@$search->order_id != NULL) {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }

        if (@$search->synk_status != NULL) {

            $this->db->where('quatation_tbl.synk_status', $search->synk_status);
        }

        if ($search->search_daterange != '') {
            $daterange_date = $search->search_daterange;
            $range_date = explode(" - ", $daterange_date);
            $date1 = $range_date[0];
            $date2 = $range_date[1];

            $date11 = explode("/", $date1);
            $date22 = explode("/", $date2);

            if (count($date11) > 2 && count($date22) > 2) {
                $from_date = $date11[2] . "-" . $date11[0] . "-" . $date11[1];
                $to_date = $date22[2] . "-" . $date22[0] . "-" . $date22[1];

                $this->db->where("quatation_tbl.order_date >=", $from_date);
                $this->db->where("quatation_tbl.order_date <=", $to_date);
            }
        }

        $this->db->where('quatation_tbl.order_stage!=', 6);

        $this->db->limit($offset, $limit);


        $this->db->order_by('id', 'DESC');
        $this->db->group_by('order_id');

        $query = $this->db->get()->result();

        return $query;
    }



    public function order_row_count($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');
        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL) {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_date != NULL) {

            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        if (@$search->user_id != NULL) {

            $this->db->where('quatation_tbl.created_by', $search->user_id);
        }
        if (@$search->level_id != NULL) {

            $this->db->where('quatation_tbl.level_id', $search->level_id);
        }
        if (@$search->order_id != NULL) {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }
        if (@$search->synk_status != NULL) {

            $this->db->where('quatation_tbl.synk_status', $search->synk_status);
        }
        $this->db->where('quatation_tbl.order_stage!=', 6);
        $total = $this->db->get()->num_rows();
        return $total;
    }



    public function get_orderd_by_id($order_id)
    {

        $query = $this->db->select("quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.email,
            customer_info.customer_id")

            ->from('quatation_tbl')
            ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left')
            ->where('quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;
    }

    public function get_orderd_details_by_id($order_id)
    {

        $query = $this->db->select("qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number,category_tbl.category_name")
            ->from('qutation_details')
            ->join('product_tbl', 'product_tbl.product_id=qutation_details.product_id', 'left')
            ->join('quatation_attributes', 'quatation_attributes.fk_od_id=qutation_details.row_id', 'left')
            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id', 'left')
            ->join('color_tbl', 'color_tbl.id=qutation_details.color_id', 'left')
            ->join('category_tbl', 'category_tbl.category_id=product_tbl.category_id', 'left')
            ->where('qutation_details.order_id', $order_id)
            ->get()->result();
        return $query;
    }
    public function get_wholesale_orderd_details_by_id($order_id)
    {

        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
            ->from('b_level_qutation_details')
            ->join('product_tbl', 'product_tbl.product_id=b_level_qutation_details.product_id', 'left')
            ->join('quatation_attributes', 'quatation_attributes.fk_od_id=b_level_qutation_details.row_id', 'left')
            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id', 'left')
            ->join('color_tbl', 'color_tbl.id=b_level_qutation_details.color_id', 'left')
            ->where('b_level_qutation_details.order_id', $order_id)
            ->get()->result();

        return $query;
    }

    public function get_quote_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 1);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_paid_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 2);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_partially_paid_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 3);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_shipping_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 4);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }

    public function get_cancelled_orderd($search = NULL)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");

        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }

        if ($search->order_date != NULL) {
            $this->db->where('quatation_tbl.order_date', $search->order_date);
        }

        $this->db->where('quatation_tbl.order_stage', 5);

        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get()->result();

        return $query;
    }


    function get_order_item_details($row_id)
    {
        $this->db->select('*');
        $this->db->from('qutation_details');
        $this->db->where('row_id = ' . $row_id);
        $query = $this->db->get()->row();
        return $query;
    }

    public function get_order_item_attr($where)
    {
        $this->db->select('product_attribute');
        $this->db->from('quatation_attributes');
        $this->db->where($where);
        $query = $this->db->get()->row()->product_attribute;
        return $query;
    }


    function get_manufactur_count($search)
    {
        $user_id = $this->session->userdata('user_id');
        if (!empty($user_id)) {
            $this->db->select('c_manufactur.*,user_info.first_name,user_info.last_name');
            $this->db->from('c_manufactur');
            $this->db->join('user_info', 'c_manufactur.created_by=user_info.id', 'left');
            $this->db->where('c_manufactur.created_by', $user_id);

            if ($search->company_id != NULL) {
                $this->db->where('c_manufactur.customer_id', $search->company_id);
            }
            if($search->sidemark!=NULL){
                $this->db->where('c_manufactur.side_mark',$search->sidemark);
            }
            if ($search->order_date != NULL) {
                $this->db->where('c_manufactur.order_date', $search->order_date);
            }
            if ($search->order_stage != NULL) {

                $this->db->where('c_manufactur.order_stage', $search->order_stage);
            }
        }
        $data =  $this->db->get()->result();
        return count($data);
    }

    function search_manufactur($search, $offset = null, $limit = null)
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select("c_manufactur.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('c_manufactur');
        $this->db->join('customer_info','customer_info.customer_id=c_manufactur.customer_id','left');
        $this->db->where('c_manufactur.created_by', $user_id);

        if ($search->company_id != NULL) {
            $this->db->where('c_manufactur.customer_id', $search->company_id);
        }
        if($search->sidemark!=NULL){
            $this->db->where('c_manufactur.side_mark',$search->sidemark);
        }
        if ($search->order_date != NULL) {
            $this->db->where('c_manufactur.order_date', $search->order_date);
        }
        if ($search->order_stage != NULL) {
            $this->db->where('c_manufactur.order_stage', $search->order_stage);
        }else{
            $this->db->where('c_manufactur.order_stage',11);
        }

        $this->db->order_by('c_manufactur.id', 'desc');

        $this->db->limit($offset, $limit);
        $response = $this->db->get()->result();

        return $response;
    }

    function get_order_and_manufacturing_order_by_id($order_id)
    {

        $query = $this->db->select("c_manufactur.order_id,c_manufactur.side_mark,c_manufactur.order_date,c_manufactur.level_id,c_manufactur.barcode,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
            ->from('c_manufactur')
            ->join('customer_info', 'customer_info.customer_id=c_manufactur.customer_id', 'left')
            ->where('c_manufactur.order_id', $order_id)
            ->get();

        $result = $query->row();

        return $result;
    }

    public function get_manufacture_orderd_details_by_id($order_id)
    {
        $query = $this->db->select("
                            qutation_details.row_id,
                            qutation_details.room,
                            qutation_details.product_id,
                            qutation_details.category_id,
                            qutation_details.product_qty,
                            qutation_details.unit_total_price,
                            qutation_details.list_price,
                            qutation_details.discount,
                            qutation_details.pattern_model_id,
                            qutation_details.color_id,
                            qutation_details.width,
                            qutation_details.height,
                            qutation_details.height_fraction_id,
                            qutation_details.width_fraction_id,
                            qutation_details.notes,
                            product_tbl.product_name,
                            category_tbl.category_name,
                            quatation_attributes.product_attribute,
                            pattern_model_tbl.pattern_name,
                            color_tbl.color_name,
                            color_tbl.color_number
                        ")

            ->from('qutation_details')
            ->join('product_tbl','product_tbl.product_id=qutation_details.product_id','left')
            ->join('category_tbl','category_tbl.category_id=qutation_details.category_id','left')
            ->join('quatation_attributes','quatation_attributes.fk_od_id=qutation_details.row_id','left')
            ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id','left')
            ->join('color_tbl','color_tbl.id=qutation_details.color_id','left')
            ->where('qutation_details.order_id', $order_id)
            ->get();


        // $query = $this->db->select("
        //                     c_manufactur_details.row_id,
        //                     c_manufactur_details.room,
        //                     c_manufactur_details.product_id,
        //                     c_manufactur_details.category_id,
        //                     c_manufactur_details.product_qty,
        //                     c_manufactur_details.unit_total_price,
        //                     c_manufactur_details.list_price,
        //                     c_manufactur_details.discount,
        //                     c_manufactur_details.pattern_model_id,
        //                     c_manufactur_details.color_id,
        //                     c_manufactur_details.width,
        //                     c_manufactur_details.height,
        //                     c_manufactur_details.height_fraction_id,
        //                     c_manufactur_details.width_fraction_id,
        //                     c_manufactur_details.notes,
        //                     product_tbl.product_name,
        //                     category_tbl.category_name,
        //                     quatation_attributes.product_attribute,
        //                     pattern_model_tbl.pattern_name,
        //                     color_tbl.color_name,
        //                     color_tbl.color_number
        //                 ")

        //     ->from('c_manufactur_details')
        //     ->join('product_tbl', 'product_tbl.product_id=c_manufactur_details.product_id', 'left')
        //     ->join('category_tbl', 'category_tbl.category_id=c_manufactur_details.category_id', 'left')
        //     ->join('quatation_attributes', 'quatation_attributes.fk_od_id=c_manufactur_details.row_id', 'left')
        //     ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=c_manufactur_details.pattern_model_id', 'left')
        //     ->join('color_tbl', 'color_tbl.id=c_manufactur_details.color_id', 'left')
        //     ->where('c_manufactur_details.order_id', $order_id)
        //     ->get();


        // echo $this->db->last_query();die;
        $result = $query->result();

        return $result;
    }

    public function get_name($att, $attribute_name)
    {

        if ($attribute_name == 'Mount') {

            $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
            return @$ops->option_name;
        }


        if ($attribute_name == 'Tilt Type' || $attribute_name == 'Tilt type') {

            $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
            return @$ops->option_name;
        }



        if ($attribute_name == 'Control Type' || $attribute_name == 'Control pype') {

            $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
            return @$ops->option_name;
        }



        if ($attribute_name == 'Control Position' || $attribute_name == 'Control position') {

            $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
            return @$ops->option_name;
        }



        if ($attribute_name == 'Control Length' || $attribute_name == 'Control length') {

            $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
            return @$ops->option_name;
        }
    }

    function transfer_order_email($orderdata, $order_detail,$transfer_email='')
    {
        $this->load->model('Common_model');
        $config = $this->Common_model->mailConfig($this->session->userdata('user_id'));

        $data['order_data'] = $orderdata;
        $data['c_company_profile'] = $this->Setting_model->company_profile();
        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                ->where('order_id', $orderdata['order_id'])->get('shipment_data')->row();

        // Load Html template
        $data['customer'] = $this->db->where('customer_id', $orderdata['customer_id'])->get('customer_info')->row();

        $orderDetailsData =  $this->Order_model->get_orderd_details_by_id($orderdata['order_id']);
        
        $data['order_details'] = $orderDetailsData;

        $mesg = $this->load->view('c_level/email-template/email_template_transfer_order_email', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($transfer_email);
        $this->email->subject("Welcome to BMSLINK");

        $this->email->message($mesg);
        $this->email->send();
    }


    function order_confirm_email($orderdata)
    {

        $this->load->model('Common_model');
        $config = $this->Common_model->mailConfig($this->session->userdata('user_id'));

        $data['order_data'] = $orderdata;
        $data['c_company_profile'] = $this->Setting_model->company_profile();
        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                ->where('order_id', $orderdata['order_id'])->get('shipment_data')->row();

        // Load Html template
        $data['customer'] = $this->db->where('customer_id', $orderdata['customer_id'])->get('customer_info')->row();

        $orderDetailsData =  $this->Order_model->get_orderd_details_by_id($orderdata['order_id']);
        // echo "<pre>";
        // print_r($orderDetailsData);die();
        // foreach ($orderDetailsData as $key => $value) {
        //     $orderDetailsData[$key]->link_status = 0;
        //     $check = $this->db->where('pattern_link', $value->pattern_model_id)->where('product_link', $value->product_id)->where('approve_status', 1)->get('b_user_catalog_products')->row();
        //     if (!empty($check))
        //         $orderDetailsData[$key]->link_status = 1;
        // }
        $data['order_details'] = $orderDetailsData;

        $mesg = $this->load->view('c_level/email-template/email_template', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($data['customer']->email);
        $this->email->subject("Welcome to BMSLINK");

        $this->email->message($mesg);

        $is_send = $this->email->send();

        if($is_send) 
        {
            return true;
        }
    }


    // public function get_orderd_details_by_id($order_id){

    //     $query = $this->db->select("quatation_tbl.*,
    //         product_tbl.product_name,
    //         category_tbl.category_name,
    //         quatation_attributes.product_attribute,
    //         pattern_model_tbl.pattern_name,
    //         color_tbl.color_name,
    //         color_tbl.color_number")

    //         ->from('quatation_tbl')
    //         ->join('product_tbl','product_tbl.product_id=quatation_tbl.product_id','left')
    //         ->join('category_tbl','category_tbl.category_id=quatation_tbl.category_id','left')
    //         ->join('quatation_attributes','quatation_attributes.fk_od_id=quatation_tbl.row_id','left')
    //         ->join('pattern_model_tbl','pattern_model_tbl.pattern_model_id=quatation_tbl.pattern_model_id','left')

    //         ->join('color_tbl','color_tbl.id=quatation_tbl.color_id','left')

    //         ->where('quatation_tbl.order_id', $order_id)

    //         ->get()->result();



    //     return $query;

    // }

    // Get order for wholesaler user and order_id
    public function get_orderd_by_id_for_blevel($order_id)
    {
        $query = $this->db->select("b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
            ->from('b_level_quatation_tbl')
            ->join('customer_info', 'customer_info.customer_id=b_level_quatation_tbl.customer_id', 'left')
            ->where('b_level_quatation_tbl.order_id', $order_id)
            ->get()->row();
        return $query;
    }

    // Get order detail for wholesaler user and order_id
    public function get_orderd_details_by_id_for_blevel($order_id)
    {
        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            category_tbl.category_name,
            b_level_quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
            ->from('b_level_qutation_details')
            ->join('product_tbl', 'product_tbl.product_id=b_level_qutation_details.product_id', 'left')
            ->join('category_tbl', 'category_tbl.category_id=b_level_qutation_details.category_id', 'left')
            ->join('b_level_quatation_attributes', 'b_level_quatation_attributes.fk_od_id=b_level_qutation_details.row_id', 'left')
            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id', 'left')
            ->join('color_tbl', 'color_tbl.id=b_level_qutation_details.color_id', 'left')
            ->where('b_level_qutation_details.order_id', $order_id)
            ->get()->result();
        return $query;
    }

    public function get_product_details_by_id($order_id, $product_id)
    {

        $query = $this->db->select("qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
            ->from('qutation_details')
            ->join('product_tbl', 'product_tbl.product_id=qutation_details.product_id', 'left')
            ->join('quatation_attributes', 'quatation_attributes.fk_od_id=qutation_details.row_id', 'left')
            ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id', 'left')
            ->join('color_tbl', 'color_tbl.id=qutation_details.color_id', 'left')
            ->where('qutation_details.order_id', $order_id)
            ->where('qutation_details.product_id', $product_id)
            ->get()->row();
        return $query;
    }
}
