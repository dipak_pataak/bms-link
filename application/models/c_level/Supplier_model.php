<?php

class Supplier_model extends CI_model {

//============ its for customer_list method ==============
    public function supplier_list($offset = null, $limit = null) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select("*")
                        ->from('supplier_tbl')
                        ->where('created_by', $level_id)
                        ->order_by('supplier_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }


     public function get_raw_material() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.id, a.material_name');
        $this->db->from('row_material_tbl a');
        $this->db->where('a.created_by',$level_id);
        $this->db->order_by('a.material_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

//    ============= its for supplier_edit =============
    public function supplier_edit($id) {
        $query = $this->db->select('*')
                ->from('supplier_tbl')
                ->where('supplier_id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //    ============ its for accounts table hit ============
    public function headcode() {
        $query = $this->db->query("SELECT MAX(HeadCode) as HeadCode FROM b_acc_coa WHERE HeadLevel='4' And HeadCode LIKE '5020201%'");
        return $query->row();
    }

//============= its for get_supplier ===============
    public function get_supplier() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select("*")
                        ->from('supplier_tbl')
                        ->where('created_by',$level_id)
                        ->order_by('supplier_name', 'Asc')
                        ->get()->result();
        return $query;
    }

//    ============== its for supplier_wise_material =============
    public function supplier_wise_material($supplier_id) {
        $this->db->select('*');
        $this->db->from('supplier_raw_material_mapping a');
        $this->db->where('a.supplier_id', $supplier_id);
        $query = $this->db->get();
        return $query->result();
    }

//    ============== its for get_supplier_filter ======================
    public function get_supplier_filter($sku, $name, $email, $phone) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select("*");
        $this->db->from('supplier_tbl a');
        if ($sku && $name && $email && $phone) {
            $this->db->like('a.supplier_sku', $sku, 'both');
            $this->db->like('a.supplier_name', $name, 'both');
            $this->db->like('a.email', $email, 'both');
            $this->db->like('a.phone', $phone, 'both');
            $this->db->where('a.created_by', $level_id);
        }
        if ($sku) {
            $this->db->like('a.supplier_sku', $sku, 'both');
            $this->db->where('a.created_by', $level_id);
        }
        if ($name) {
            $this->db->like('a.supplier_name', $name, 'both');
            $this->db->where('a.created_by', $level_id);
        }
        if ($email) {
            $this->db->like('a.email', $email, 'both');
            $this->db->where('a.created_by', $level_id);
        }
        if ($phone) {
            $this->db->like('a.phone', $phone, 'both');
            $this->db->where('a.created_by', $level_id);
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.supplier_id', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }

//    ============ its for supplier wise purchase list ==============
    public function get_purchase_list($supplier_id) {
       /* if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }*/
        $query = $this->db->select('*')->from('purchase_tbl a')
                        ->join('supplier_tbl b', 'b.supplier_id = a.supplier_id', 'left')
                        ->where('a.supplier_id', $supplier_id)
                        /*->where('a.created_by', $level_id)*/
                        ->order_by('a.id', 'desc')->get()->result();
        return $query;
    }

//    ========== its for supplier  onkeyup search =============
    public function supplier_search($keyword) {

        $query = $this->db->select("*")
                        ->from('supplier_tbl')
                        ->like('supplier_name', $keyword, 'both')
                        ->or_like('company_name', $keyword, 'both')
                        ->or_like('email', $keyword, 'both')
                        ->or_like('phone', $keyword, 'both')
                        ->order_by('supplier_id','DESC')
                        ->limit(50)
                        ->get()->result();
        return $query;

    }

    public function getAllRemainingSuppliers($user_id)
    {
        $suppliers = $this->db->where('is_added_in_quickbooks',0)->where('created_by',$user_id)->get('supplier_tbl')->result_array();
        return $suppliers;
    }

    public function updateQuickbooksStatus($ids)
    {
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('supplier_id',$ids);
        $this->db->update('supplier_tbl');

        if($this->db->affected_rows() > 0) {
            return ture;
        }else {
            return false;
        }
    }

    public function updateQuickbooksId($supplier_id, $quickbooksId){
        $this->db->set('quick_books_id',$quickbooksId);
        $this->db->where('supplier_id',$supplier_id);
        $this->db->update('supplier_tbl');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }
}
