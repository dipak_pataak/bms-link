 <?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_return_model extends CI_model {

//============ its for customer_list method ==============
    public function customer_return($offset = null, $limit = null) {

            if ($this->session->userdata('isAdmin') == 1) {
                $level_id = $this->session->userdata('user_id');
            } else {
                $level_id = $this->session->userdata('admin_created_by');
            }

        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name, b.created_by as c_u_id, b.side_mark, b.company')
                        ->from('order_return_tbl a')
                        ->join('customer_info b', 'b.customer_id = a.customer_id')
                        //->join('company_profile c', 'c.user_id = b.c_u_id')
                        ->where('a.level_id', $level_id)
						//->where('a.order_stage', 7)
                        ->order_by('a.return_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();

        return $query;

    }

     //============= its for get_raw_material_supplier_return list ==============
    public function get_raw_material_supplier_return($offset, $limit) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('a.*, c.supplier_id, c.supplier_name, CONCAT(d.first_name , " " , d.last_name) as name, CONCAT(e.first_name , " " , e.last_name) as approve_name')
                        ->from('raw_material_return_tbl a')
                        ->join('purchase_tbl b', 'b.purchase_id = a.purchase_id')
                        ->join('supplier_tbl c', 'c.supplier_id = b.supplier_id')
                        ->join('user_info d', 'd.id = a.returned_by', 'left')
                        ->join('user_info e', 'e.id = a.approved_by', 'left')
                        ->where('a.created_by', $level_id)
                        ->order_by('a.return_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }


    //    ================== its for  supplier_return_filter ===============
    public function supplier_return_filter($invoice_no, $supplier_name) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, c.supplier_id, c.supplier_name, CONCAT(d.first_name , " " , d.last_name) as name, CONCAT(e.first_name , " " , e.last_name) as approve_name');
        $this->db->from('raw_material_return_tbl a');
        $this->db->join('purchase_tbl b', 'b.purchase_id = a.purchase_id');
        $this->db->join('supplier_tbl c', 'c.supplier_id = b.supplier_id');
        $this->db->join('user_info d', 'd.id = a.returned_by', 'left');
        $this->db->join('user_info e', 'e.id = a.approved_by', 'left');
        if ($invoice_no && $supplier_name) {
            $this->db->like('b.purchase_id', $invoice_no);
            $this->db->like('b.supplier_id', $supplier_name);
            $this->db->where('a.created_by', $level_id);
        }
        if ($invoice_no) {
            $this->db->like('b.purchase_id', $invoice_no);
            $this->db->where('a.created_by', $level_id);
        }
        if ($supplier_name) {
            $this->db->like('b.supplier_id', $supplier_name);
            $this->db->where('a.created_by', $level_id);
        } else {
            $this->db->where('a.created_by', $level_id);
        }
        $this->db->order_by('a.return_id', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }

    // ---------------------RMA module functions start-----------------------------

    //----------------- RMA Pending Status Query Start -------------------

    public function get_return_items_employee($offset, $limit, $eid){

        $this->db->select('order_return_tbl.*,product_name,return_type,order_return_details.return_comments,is_approved,return_qty,replace_qty,order_return_details.id,CONCAT(customer_info.first_name , " " , customer_info.last_name) as cust_name,order_return_details.product_id');
                        $this->db->from('order_return_tbl');
                        $this->db->join('order_return_details', 'order_return_tbl.return_id = order_return_details.return_id');
                        $this->db->join('product_tbl', 'product_tbl.product_id = order_return_details.product_id');
                        $this->db->join('customer_info', 'customer_info.customer_id = order_return_tbl.customer_id','left');
                        if($this->session->userdata('isAdmin') != 1){
                            $this->db->where('order_return_tbl.assigned_to', $eid);
                        }
                        $this->db->where('order_return_tbl.is_approved', 0);
                        $this->db->where('order_return_tbl.return_status', 0);
                        $this->db->where('order_return_details.return_qty!=', 0);
                        $this->db->limit($offset, $limit);

        $query = $this->db->get()->result();
        return $query;
    }

    public function get_return_items_employee_count($eid){
        $this->db->select('order_return_tbl.*');
        $this->db->from('order_return_tbl');
        $this->db->join('order_return_details', 'order_return_tbl.return_id = order_return_details.return_id');
        if($this->session->userdata('isAdmin') != 1){
            $this->db->where('order_return_tbl.assigned_to', $eid);
        }
        $this->db->where('order_return_tbl.is_approved', 0);
        $this->db->where('order_return_tbl.return_status', 0);
        $this->db->where('order_return_details.return_qty!=', 0);
        $query = $this->db->count_all_results();
        return $query;
    }

    public function insert_return_item_status($data){
        $this->db->insert('manage_return_item_status',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function insert_return_item_history($data){
        $this->db->insert('manage_return_history',$data);
        return true;
    }

    public function insert_wholeseler_return_data($data){
        $this->db->insert('manage_wholeseler_return',$data);
        return true;
    }

     public function change_item_return_status($data,$oid){
        $this->db->where('order_id',$oid);
        $this->db->update('order_return_tbl',$data);
        return true;
    }
    //----------------- RMA Pending Status Query end -------------------


    //----------------- RMA In Progress Status Query Start -------------------

    public function get_return_in_progress_items_employee($offset, $limit, $uid){
        $this->db->select('manage_return_item_status.*,product_name,CONCAT(customer_info.first_name , " " , customer_info.last_name) as cust_name,return_type');
        $this->db->from('manage_return_item_status');
        $this->db->join('product_tbl', 'product_tbl.product_id = manage_return_item_status.product_id','left');
        $this->db->join('customer_info', 'customer_info.customer_id = manage_return_item_status.customer_id','left');
        $this->db->join('order_return_details', 'order_return_details.id = manage_return_item_status.return_detail_id','left');
        if($this->session->userdata('isAdmin') != 1){
            $this->db->where('manage_return_item_status.user_id', $uid);
        }
        $this->db->where('manage_return_item_status.status',1);
        $this->db->or_where('manage_return_item_status.status',2);
        $this->db->limit($offset, $limit);
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_return_in_progress_items_count($uid){
        if($this->session->userdata('isAdmin') != 1){
            $this->db->where('user_id',$uid);
        }
        $this->db->where('manage_return_item_status.status',1);
        $this->db->or_where('manage_return_item_status.status',2);
        $this->db->from("manage_return_item_status");
        $query = $this->db->count_all_results();
        return $query;
    }

    public function get_return_id_data($id){
        $query = $this->db->select('manage_return_item_status.*,product_name,CONCAT(customer_info.first_name , " " , customer_info.last_name) as cust_name,customer_info.email,customer_info.phone')
                        ->from('manage_return_item_status')
                        ->join('product_tbl', 'product_tbl.product_id = manage_return_item_status.product_id','left')
                        ->join('customer_info', 'customer_info.customer_id = manage_return_item_status.customer_id','left')
                        ->where('manage_return_item_status.id', $id)
                        ->get()->row();

                        // echo $this->db->last_query();die;
        return $query;
    }

    public function change_item_status($data,$id){
        $this->db->where('id',$id);
        $this->db->update('manage_return_item_status',$data);
        return true;
    }

    //----------------- RMA In Progress Status Query End -------------------


    //----------------- RMA Completed Status Query Start -------------------

    public function get_return_completed_items_count($uid){
        if($this->session->userdata('isAdmin') != 1){
            $this->db->where('user_id',$uid);
        }
        $this->db->where('status',3);
        $this->db->from("manage_return_item_status");
        $query = $this->db->count_all_results();
        return $query;
    }

    public function get_return_completed_items_employee($offset, $limit, $uid){
        $this->db->select('manage_return_item_status.*,product_name,CONCAT(customer_info.first_name , " " , customer_info.last_name) as cust_name,return_type');
        $this->db->from('manage_return_item_status');
        $this->db->join('product_tbl', 'product_tbl.product_id = manage_return_item_status.product_id','left');
        $this->db->join('customer_info', 'customer_info.customer_id = manage_return_item_status.customer_id','left');
        $this->db->join('order_return_details', 'order_return_details.id = manage_return_item_status.return_detail_id','left');
        if($this->session->userdata('isAdmin') != 1){
            $this->db->where('manage_return_item_status.user_id', $uid);
        }
        $this->db->where('manage_return_item_status.status',3);
        $this->db->limit($offset, $limit);
        $query = $this->db->get()->result();
        return $query;
    }

    //----------------- RMA Completed Status Query End -------------------

    // ---------------------RMA module functions end-----------------------------


}