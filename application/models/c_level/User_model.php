<?php

class User_model extends CI_model {

//============= its for get_users =================
    public function get_users() {
         if($this->session->userdata('isAdmin') == 1){
            $level_id = $this->session->userdata('user_id');
        }else{
          $level_id = $this->session->userdata('admin_created_by'); 
        }

       // dd($level_id);
        $query = $this->db->select('*')
                        ->from('user_info a')
                        // ->join('log_info b', 'b.user_id = a.id', 'left')
                        ->where('a.created_by', $level_id)
//                        ->where('is_admin', 2)
                        ->order_by('a.id', 'desc')
                        ->get()->result();
        return $query;
    }

//    =========== its for user_edit ==============
    public function user_edit($id) {
        $query = $this->db->select('*')
                ->from('user_info a')
                ->where('a.id', $id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
  //    ============== its for checkEmail =============
    public function checkEmail($email) {
        return $query = $this->db->select('*')->from('log_info')->where('email', $email)->where('user_type','c')->get()->row();
    }
    
//    ========== its for sendLink ===============
    public function sendLink($log_id, $email, $data, $random_key) {
//        echo $data['get_mail_config'][0]->protocol;
//        echo '<pre>';        print_r($data);die();
        $data['baseurl'] = base_url();
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));

        $data['author_info'] = $this->author_info($log_id);
        $data['random_key'] = $random_key;
        $mesg = $this->load->view('c_level/sendPasswordLink', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BDtask");
       /* $fp = @fsockopen(base_url(), 465, $errno, $errstr);
        
        if ($fp) { */

            $this->email->message($mesg);
            $this->email->send();

        //}
    }
    
    //    =========== its for author_info ==============
    public function author_info($log_id) {
        $query = $this->db->select('*')
                        ->from('log_info a')
                        ->join('user_info c', 'c.id = a.user_id', 'left')
                        ->where('a.row_id', $log_id)
                        ->get()->row();
        return $query;
    }
    
//    ========== its for sendLink ===============
    public function sendUserInfo($user_id, $data, $email, $password) {
        $data['baseurl'] = base_url();
        
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));

        $login_user_info = $this->db->select('*')->from('user_info a')->where('a.id', $this->session->userdata('user_id'))->get()->row();

//        echo '<pre>';        print_r($config);die();
        $data['author_info'] = $this->author_info($user_id);
        $data['username'] = $email;
        $data['password'] = $password;
        $data['user_company'] = $login_user_info->company;
        $data['user_phone'] = $login_user_info->phone;
//        echo $data['author_info']->doctor_name; 
//        echo $data['random_key'];die();
//        echo '<pre>';        print_r($data['author_info']);die();
        $mesg = $this->load->view('c_level/users/send_user_info', $data, TRUE);
        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMSLINK");

        /* $fp = @fsockopen(base_url(), $data['get_mail_config'][0]->smtp_port, $errno, $errstr);
        if ($fp) { */

            $this->email->message($mesg);
            $this->email->send();

        //}


    }


      function getData($table, $where = '') {
        $this->db->select('*');
        if (isset($where) && $where != '') {
            $this->db->where($where);
        }
        $this->db->from($table);
        $query = $this->db->get();
        return $query;
    }
     function addData($table, $insertInfo) {
        $this->db->trans_start();
        $this->db->insert($table, $insertInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateData($table, $id, $updateInfo) {
        $this->db->where('id', $id);
        $this->db->update($table, $updateInfo);
        return $this->db->affected_rows();
    }
     function updateTable($table, $where, $updateInfo) {
        $this->db->where($where);
        $this->db->update($table, $updateInfo);
        $q = $this->db->affected_rows();
        if($q > 0){
             return $q;
        }else{
           return $this->db->last_query();
        }
       
    }
      function deleteUser($userId, $userInfo) {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);

        return $this->db->affected_rows();
    }

    function deleteRow($del_id, $del_tbl) {
        $this->db->where('id', $del_id);
        $this->db->delete($del_tbl);
        return $this->db->affected_rows();
    }

}
