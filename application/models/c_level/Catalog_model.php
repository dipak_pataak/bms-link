<?php

class Catalog_model extends CI_model {

    function add_customer_category($user_id,$request)
    {
        $data['created_by'] = $user_id;
        $data['category_name'] = $request['category_name'];
        $data['parent_category'] = $request['parent_category'];
        $data['description'] = $request['description'];
        $data['created_date'] = Date('Y-m-d');
        $data['status'] = $request['status'];
        $data['created_status'] = 1;
        $this->db->insert('category_tbl',$data);
        return true;
    }

    function update_customer_category($user_id,$request)
    {
       /* $data['created_by'] = $user_id;*/
        $data['category_name'] = $request['category_name'];
        $data['parent_category'] = $request['parent_category'];
        $data['description'] = $request['description'];
        $data['updated_date'] = Date('Y-m-d');
        $data['status'] = $request['status'];
        $data['created_status'] = 1;
        $this->db->where('category_id',$request['category_id']);
        $this->db->update('category_tbl',$data);
        return true;
    }

    function get_patern_model($user_id) 
    {
        $this->db->select('*');
        $this->db->from('pattern_model_tbl');
        $this->db->where('status', 1);
        $this->db->where('created_by', $user_id);
        $this->db->order_by('pattern_name', 'asc');
        $this->db->where('created_status',1);
        $data =  $this->db->get()->result();
        return $data;
    }

    function get_product_conditions() 
    {
        $this->db->select('*');
        $this->db->from('product_conditions_tbl a');
        $this->db->order_by('condition_text', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function get_customer_parent_category($user_id)
    {
       $this->db->select('*');
       $this->db->from('category_tbl');
       $this->db->where('created_by',$user_id);
       $this->db->where('status',1);
       $this->db->where('created_status',1);
       $this->db->where('parent_category',0);
       $data = $this->db->get()->result();
       return $data;

    }
    function get_category_count($b_user_id)
    {
        if ($search->cat_name!='') {
            $this->db->like("category_tbl.category_name", $search->cat_name, 'both');
        }
        if($search->parent_cat!='') {
            $this->db->like('category_tbl.parent_category', $search->parent_cat, 'both');
        }
        if($search->category_status != '') {
            $this->db->like('category_tbl.status', $search->category_status, 'both');
        }
        $this->db->group_start();

        $user_id = $this->session->userdata('user_id');
        if (!empty($b_user_id)) {
            $this->db->group_start();
            $this->db->where('created_by', $b_user_id);
            $this->db->where('created_status', 0);
            $this->db->group_end(); //close bracket
        }

        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('created_by', $user_id);
        $this->db->where('created_status', 1);
        $this->db->group_end(); //close bracket

        $this->db->group_end(); //close bracket


        $data =  $this->db->get('category_tbl')->result();
        return count($data);
    }

    function get_all_category_level_wise(){
        
        $user_id = $this->session->userdata('user_id');
        $b_user_id = $this->session->userdata('main_b_id');

        if (!empty($b_user_id)) {
            $this->db->group_start();
            $this->db->where('created_by', $b_user_id);
            $this->db->where('created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $this->db->or_group_start();
        $this->db->where('created_by', $user_id);
        $this->db->where('created_status', 1);
        $this->db->group_end(); //close bracket

        $data =  $this->db->get('category_tbl')->result();
        return $data;
    }

    function search_category($offset = null, $limit = null,$search='')
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('t1.*,t2.category_name AS parent_category_name');
        $this->db->from('category_tbl AS t1');
        $this->db->join('category_tbl AS t2','t2.category_id=t1.parent_category','left');

        if ($search->cat_name!='') {
            $this->db->like("t1.category_name", $search->cat_name, 'both');
        }
        if($search->parent_cat!='') {
            $this->db->like('t1.parent_category', $search->parent_cat, 'both');
        }
        if($search->category_status != '') {
            $this->db->like('t1.status', $search->category_status, 'both');
        }
        $this->db->group_start();

        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('t1.created_by', $main_b_id);
            $this->db->where('t1.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
        $this->db->group_start();
        else
        $this->db->or_group_start();
        $this->db->where('t1.created_by', $user_id);
        $this->db->where('t1.created_status', 1);
        $this->db->group_end(); //close bracket

        $this->db->group_end(); //close bracket
 


        $this->db->order_by('t1.category_id','desc');
        $this->db->limit($offset, $limit);
        $response = $this->db->get()->result();

        return $response;
    }

    function add_customer_color($user_id,$request,$upload_file='')
    {
        $data['created_by'] = $user_id;
        $data['color_name'] = $request['color_name'];
        $data['color_number'] = $request['color_number'];
        $data['pattern_id'] = $request['pattern_id'];
        $data['color_img'] = $upload_file;
        $data['created_date'] = Date('Y-m-d');
        $data['created_status'] = 1;
        $this->db->insert('color_tbl',$data);
        return true;
    }

    function update_customer_color($user_id,$request,$upload_file='')
    {
        $data['created_by'] = $user_id;
        $data['color_name'] = $request['color_name'];
        $data['color_number'] = $request['color_number'];
        $data['pattern_id'] = $request['pattern_id'];
        $data['color_img'] = $upload_file;
        $data['updated_date'] = Date('Y-m-d');
        $data['created_status'] = 1;

        $this->db->where('id',$request['color_id']);
        $this->db->update('color_tbl',$data);
        return true;
    }

    function get_color_count($b_user_id, $search)
    {
        if ($search->colorname!='') {
            $this->db->like("color_tbl.color_name", $search->colorname, 'both');
        }
        if($search->colornumber!='') {
            $this->db->like('color_tbl.color_number', $search->colornumber, 'both');
        }
        $this->db->where('created_by',$b_user_id);
        $data = $this->db->get('color_tbl')->result();
        return count($data);
    }

    function search_color($offset = null, $limit = null, $search)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('t1.*,t2.pattern_name');
        $this->db->from('color_tbl AS t1');
        $this->db->join('pattern_model_tbl AS t2', 't2.pattern_model_id = t1.pattern_id', 'LEFT');

        if ($search->colorname!='') {
            $this->db->like("t1.color_name", $search->colorname, 'both');
        }
        if($search->colornumber!='') {
            $this->db->like('t1.color_number', $search->colornumber, 'both');
        }
        if($search->pattern_name != '') {
            $this->db->like('t2.pattern_name', $search->pattern_name, 'both');
        }

        $this->db->group_start();

        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('t1.created_by', $main_b_id);
            $this->db->where('t1.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('t1.created_by', $user_id);
        $this->db->where('t1.created_status', 1);
        $this->db->group_end(); //close bracket


        $this->db->group_end(); //close bracket


        $this->db->order_by('t1.id','desc');
        $this->db->limit($offset, $limit);
        $response = $this->db->get()->result();
        return $response;
    }

    function get_product_count($main_b_id,$search)
    {
        $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        $this->db->from('product_tbl a');
        $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');

            if ($search->category_id!='') {
                $this->db->like("a.category_id", $search->category_id, 'both');
            }
            if($search->price_style_type!='') {
                $this->db->like('a.price_style_type', $search->price_style_type, 'both');
            }
            if($search->status != '') {
                $this->db->like('a.active_status', $search->status, 'both');
            }

            $this->db->group_start();


        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('a.created_by', $main_b_id);
            $this->db->where('a.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('a.created_by', $user_id);
        $this->db->where('a.created_status', 1);
        $this->db->group_end(); //close bracket

        $this->db->group_end(); //close bracket


        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('a.product_id', 'desc');
        $response = $this->db->get()->result();
        return count($response);
    }

    function search_product($offset = null, $limit = null,$search)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        $this->db->from('product_tbl a');
        $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');

            if ($search->category_id!='') {
                $this->db->like("a.category_id", $search->category_id, 'both');
            }
            if($search->price_style_type!='') {
                $this->db->like('a.price_style_type', $search->price_style_type, 'both');
            }
            if($search->status != '') {
                $this->db->like('a.active_status', $search->status, 'both');
            }

            $this->db->group_start();
        
        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('a.created_by', $main_b_id);
            $this->db->where('a.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('a.created_by', $user_id);
        $this->db->where('a.created_status', 1);
        $this->db->group_end(); //close bracket

        $this->db->group_end(); //close bracket


        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('a.product_id', 'desc');
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();

        return $response;
    }
/******************Sqm Price*****************/
    function get_sqm_price_count($main_b_id)
    {
        //$main_b_id = $this->session->userdata('main_b_id');

            $this->db->select('smm.*,p.product_name,p.created_status,pm.pattern_name');
            $this->db->from('sqm_price_model_mapping_tbl smm');
            $this->db->join('product_tbl p', 'p.product_id=smm.product_id');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id');

        // $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        // $this->db->from('product_tbl a');
        // $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        // $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        // $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
        
        // if (!empty($main_b_id)) {
        //     $this->db->group_start();
        //     $this->db->where('pm.created_by',$main_b_id);
        //     $this->db->where('p.created_by',$main_b_id);
        //     // $this->db->where('pm.created_status',0)
        //     $this->db->where('p.created_status',0);
        //     $this->db->group_end(); //close bracket
        // }

        $user_id = $this->session->userdata('user_id');
        // if(empty($main_b_id))
            $this->db->group_start();
        // else
        //     $this->db->or_group_start();
        $this->db->where('pm.created_by',$user_id);
        $this->db->where('p.created_by',$user_id);
        // $this->db->where('pm.created_status',1)
        $this->db->where('p.created_status',1);
        $this->db->group_end(); //close bracket

        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('p.product_id', 'desc');
        $response = $this->db->get()->result();
        return count($response);
    }

    function search_sqm_price($offset = null, $limit = null)
    {

        //$main_b_id = $this->session->userdata('main_b_id');

            $this->db->select('smm.*,p.product_name,p.created_status,pm.pattern_name');
            $this->db->from('sqm_price_model_mapping_tbl smm');
            $this->db->join('product_tbl p', 'p.product_id=smm.product_id');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=smm.pattern_id');

        // $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        // $this->db->from('product_tbl a');
        // $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        // $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        // $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
        
        // if (!empty($main_b_id)) {
        //     $this->db->group_start();
        //     $this->db->where('pm.created_by',$main_b_id);
        //     $this->db->where('p.created_by',$main_b_id);
        //     // $this->db->where('pm.created_status',0)
        //     $this->db->where('p.created_status',0);
        //     $this->db->group_end(); //close bracket
        // }

        $user_id = $this->session->userdata('user_id');
        // if(empty($main_b_id))
            $this->db->group_start();
        // else
        //     $this->db->or_group_start();
        $this->db->where('pm.created_by',$user_id);
        $this->db->where('p.created_by',$user_id);
        // $this->db->where('pm.created_status',1)
        $this->db->where('p.created_status',1);
        $this->db->group_end(); //close bracket

        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('p.product_id', 'desc');
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();

        return $response;
    }
/******************Sqm Price*****************/

/*****************Group Price Maping*********/
    function get_group_price_mapping_count($main_b_id)
    {
         //$main_b_id = $this->session->userdata('main_b_id');

            $this->db->select('pmm.*,p.product_name,p.created_status,pm.pattern_name,rc.style_name');
            $this->db->from('price_model_mapping_tbl pmm');
            $this->db->join('product_tbl p', 'p.product_id=pmm.product_id', 'left');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=pmm.pattern_id', 'left');
            $this->db->join('row_column rc', 'rc.style_id=pmm.group_id', 'left');

        // $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        // $this->db->from('product_tbl a');
        // $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        // $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        // $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
        
        // if (!empty($main_b_id)) {
        //     $this->db->group_start();
        //     $this->db->where('pm.created_by',$main_b_id);
        //     $this->db->where('p.created_by',$main_b_id);
        //     // $this->db->where('pm.created_status',0)
        //     $this->db->where('p.created_status',0);
        //     $this->db->group_end(); //close bracket
        // }

        $user_id = $this->session->userdata('user_id');
        // if(empty($main_b_id))
            $this->db->group_start();
        // else
        //     $this->db->or_group_start();
        $this->db->where('rc.create_by',$user_id);
        $this->db->where('pm.created_by',$user_id);
        $this->db->where('p.created_by',$user_id);
        // $this->db->where('pm.created_status',1)
        $this->db->where('p.created_status',1);
        $this->db->group_end(); //close bracket

        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('p.product_id', 'desc');
        $response = $this->db->get()->result();
        return count($response);
    }

    function search_group_price_mapping($offset = null, $limit = null)
    {

        //$main_b_id = $this->session->userdata('main_b_id');

            $this->db->select('pmm.*,p.product_name,p.created_status,pm.pattern_name,rc.style_name');
            $this->db->from('price_model_mapping_tbl pmm');
            $this->db->join('product_tbl p', 'p.product_id=pmm.product_id', 'left');
            $this->db->join('pattern_model_tbl pm', 'pm.pattern_model_id=pmm.pattern_id', 'left');
            $this->db->join('row_column rc', 'rc.style_id=pmm.group_id', 'left');

        // $this->db->select('a.*,b.style_name, c.category_name, a.category_id as product_cat_id');
        // $this->db->from('product_tbl a');
        // $this->db->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left');
        // $this->db->join('category_tbl c', 'c.category_id=a.category_id', 'left');
        // $this->db->join('pattern_model_tbl d', 'd.pattern_model_id=a.pattern_models_ids', 'left');
        
        // if (!empty($main_b_id)) {
        //     $this->db->group_start();
        //     $this->db->where('pm.created_by',$main_b_id);
        //     $this->db->where('p.created_by',$main_b_id);
        //     // $this->db->where('pm.created_status',0)
        //     $this->db->where('p.created_status',0);
        //     $this->db->group_end(); //close bracket
        // }

        $user_id = $this->session->userdata('user_id');
        // if(empty($main_b_id))
            $this->db->group_start();
        // else
        //     $this->db->or_group_start();
        $this->db->where('rc.create_by',$user_id);
        $this->db->where('pm.created_by',$user_id);
        $this->db->where('p.created_by',$user_id);
        // $this->db->where('pm.created_status',1)
        $this->db->where('p.created_status',1);
        $this->db->group_end(); //close bracket

        //$this->db->where('a.created_by',$main_b_id);
        $this->db->order_by('p.product_id', 'desc');
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();

        return $response;
    }
/*****************Group Price Maping*********/

    function get_customer_product_list()
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select('product_tbl.*');
        $this->db->from('product_tbl');
        $this->db->where('created_by',$user_id);
        $this->db->where('created_status',1);
        $result = $this->db->get()->result();
        return $result;
    }

    function get_customer_row_column_list()
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select('row_column.*');
        $this->db->from('row_column');
        $this->db->where('style_type','4');
        $this->db->where('create_by',$user_id);
        $this->db->where('created_status',1);
        $result = $this->db->get()->result();
        return $result;
    }

    function add_customer_pattern($user_id,$request)
    {
        $data['created_by'] = $user_id;
        $data['pattern_name'] = $request['pattern_name'];
        $data['category_id'] = $request['category_id'];
        $data['created_date'] = Date('Y-m-d');
        $data['status'] = $request['status'];
        $data['pattern_type'] = 'Pattern';
        $data['created_status'] = 1;
        $this->db->insert('pattern_model_tbl',$data);
        return true;
    }

    function update_customer_pattern($user_id,$request)
    {
        $data['created_by'] = $user_id;
        $data['pattern_name'] = $request['pattern_name'];
        $data['category_id'] = $request['category_id'];
        $data['updated_date'] = Date('Y-m-d');
        $data['status'] = $request['status'];
        $data['pattern_type'] = 'Pattern';
        $data['created_status'] = 1;


        $this->db->where('pattern_model_id',$request['pattern_model_id']);
        $this->db->update('pattern_model_tbl',$data);
        return true;
    }
    function get_pattern_count($main_b_id)
    {
        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');
        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('t1.created_by', $main_b_id);
            $this->db->where('t1.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('t1.created_by', $user_id);
        $this->db->where('t1.created_status', 1);
        $this->db->group_end(); //close bracket
        $response = $this->db->get()->result();
        return count($response);
    }

    function search_pattern($offset = null, $limit = null)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select("t1.*, t2.*, t1.category_id as pattern_category_id, t1.status as pattern_status");
        $this->db->from('pattern_model_tbl AS t1');
        $this->db->join('category_tbl AS t2', 't2.category_id = t1.category_id', 'left');


        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('t1.created_by', $main_b_id);
            $this->db->where('t1.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('t1.created_by', $user_id);
        $this->db->where('t1.created_status', 1);
        $this->db->group_end(); //close bracket
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();
        return $response;
    }

    function get_attribute_count($main_b_id,$search='')
    {
        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');

            if($search->attribute_name!='') {
                $this->db->like('attribute_tbl.attribute_name', $search->attribute_name, 'both');
            }
            if ($search->category_id!='') {
                $this->db->like("attribute_tbl.category_id", $search->category_id, 'both');
            }

            $this->db->group_start();


         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('attribute_tbl.created_by', $main_b_id);
            $this->db->where('attribute_tbl.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('attribute_tbl.created_by', $user_id);
        $this->db->where('attribute_tbl.created_status', 1);
        $this->db->group_end(); //close bracket

        $this->db->group_end(); //close bracket

        $response = $this->db->get()->result();
        return count($response);
    }

    function search_attribute($offset = null, $limit = null,$search='')
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');

            if($search->attribute_name!='') {
                $this->db->like('attribute_tbl.attribute_name', $search->attribute_name, 'both');
            }
            if ($search->category_id!='') {
                $this->db->like("attribute_tbl.category_id", $search->category_id, 'both');
            }

            $this->db->group_start();

         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('attribute_tbl.created_by', $main_b_id);
            $this->db->where('attribute_tbl.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('attribute_tbl.created_by', $user_id);
        $this->db->where('attribute_tbl.created_status', 1);
        $this->db->group_end(); //close bracket
        
        $this->db->group_end(); //close bracket

        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();
       return $response;
    }

    function get_price_count($main_b_id)
    {
        $this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        $this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 1);


        $this->db->group_start();
         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('row_column.create_by', $main_b_id);
            $this->db->where('row_column.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('row_column.create_by', $user_id);
        $this->db->where('row_column.created_status', 1);
        $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket


        // $this->db->where('row_column.create_by',$main_b_id);
        $response = $this->db->get()->result();
        return count($response);

    }

    function search_price($offset = null, $limit = null)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        $this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 1);
        
        $this->db->group_start();
         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('row_column.create_by', $main_b_id);
            $this->db->where('row_column.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('row_column.create_by', $user_id);
        $this->db->where('row_column.created_status', 1);
        $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket


        // $this->db->where('row_column.create_by',$main_b_id);
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();

        return $response;
    }

    function get_group_price_count($main_b_id)
    {
        $this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        $this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 4);

         $this->db->group_start();
         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('row_column.create_by', $main_b_id);
            $this->db->where('row_column.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('row_column.create_by', $user_id);
        $this->db->where('row_column.created_status', 1);
        $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket

        // $this->db->where('row_column.create_by',$main_b_id);
        $response = $this->db->get()->result();
        return count($response);
    }

    function search_group_price($offset = null, $limit = null)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status');
        $this->db->from('row_column');
        $this->db->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left');
        $this->db->where('row_column.style_type', 4);

        $this->db->group_start();
         if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('row_column.create_by', $main_b_id);
            $this->db->where('row_column.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('row_column.create_by', $user_id);
        $this->db->where('row_column.created_status', 1);
        $this->db->group_end(); //close bracket
        $this->db->group_end(); //close bracket

        // $this->db->where('row_column.create_by',$main_b_id);
        $this->db->limit($offset,$limit);
        $response = $this->db->get()->result();
        return $response;
    }


    function add_attribute()
    {
        $user_id = $this->session->userdata('user_id');
        $createdate = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'created_by' => $user_id,
            'created_date' => $createdate,
            'created_status' => 1,
        );

        $this->db->insert('attribute_tbl', $attributeData);
        $attribute_id = $this->db->insert_id();

        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');

        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $op_op_price = $this->input->post('op_op_price');
        $op_op_price_type = $this->input->post('op_op_price_type');
        $op_op_condition = $this->input->post('op_op_condition');

        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');

        //print_r($op_op_op_name);
        //echo "<pre>";

        if (!empty($option_name)) {

            $k = 0;
            foreach ($option_name as $option) {

                // For attribute image upload : START
                if (@$_FILES['attr_img']['name'][$k]) {

                    $_FILES['final_file']['name']     = $_FILES['attr_img']['name'][$k];
                    $_FILES['final_file']['type']     = $_FILES['attr_img']['type'][$k];
                    $_FILES['final_file']['tmp_name'] = $_FILES['attr_img']['tmp_name'][$k];
                    $_FILES['final_file']['error']    = $_FILES['attr_img']['error'][$k];
                    $_FILES['final_file']['size']     = $_FILES['attr_img']['size'][$k];

                    $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                    $config['allowed_types'] = 'jpeg|jpg|png';
                    $config['overwrite'] = false;
                    $config['max_size'] = 2048;
                    $config['remove_spaces'] = true;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('final_file')) {
                        $upload_file = '';
                    } else {
                        $data = $this->upload->data();
                        $upload_file = $config['upload_path'] . $data['file_name'];
                    }
                } else {
                    $upload_file = '';
                }
                // For attribute image upload : END

                $optionData = array(
                    'option_name' => $option,
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k],
                    'attributes_images' => $upload_file,
                );

                //print_r($optionData); 

                $this->db->insert('attr_options', $optionData);
                $att_op_id = $this->db->insert_id();

                if (!empty($option_optin_name[$k])) {

                    $o = 0;
                    $oi = 0;

                    foreach ($option_optin_name[$k] as $option_option) {

                        // For sub attribute image upload : START
                        if (@$_FILES['op_op_attr_img']['name'][$k][$oi]) {

                            $_FILES['final_file']['name']     = $_FILES['op_op_attr_img']['name'][$k][$oi];
                            $_FILES['final_file']['type']     = $_FILES['op_op_attr_img']['type'][$k][$oi];
                            $_FILES['final_file']['tmp_name'] = $_FILES['op_op_attr_img']['tmp_name'][$k][$oi];
                            $_FILES['final_file']['error']    = $_FILES['op_op_attr_img']['error'][$k][$oi];
                            $_FILES['final_file']['size']     = $_FILES['op_op_attr_img']['size'][$k][$oi];

                            $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                            $config['allowed_types'] = 'jpeg|jpg|png';
                            $config['overwrite'] = false;
                            $config['max_size'] = 2048;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('final_file')) {
                                $upload_file = '';
                            } else {
                                $data = $this->upload->data();
                                $upload_file = $config['upload_path'] . $data['file_name'];
                            }
                        } else {
                            $upload_file = '';
                        }
                        // For sub attribute image upload : END

                        $optionOptionData = array(
                            'op_op_name' => $option_option,
                            'type' => @$option_option_type[$k][$oi],
                            'option_id' => @$att_op_id,
                            'attribute_id' => @$attribute_id,
                            'att_op_op_price_type' => @$op_op_price_type[$k][$oi],
                            'att_op_op_price' => @$op_op_price[$k][$oi],
                            'att_op_op_condition' => @$op_op_condition[$k][$oi],
                            'att_op_op_images' => $upload_file,
                        );

                        //print_r($optionOptionData);

                        if ($this->db->insert('attr_options_option_tbl', $optionOptionData)) {

                            $op_op_id = $this->db->insert_id();

                            // echo "<br>=======".$k."_".$o."=======<br>";
                            //print_r($op_op_op_name[1][3]);

                            if (!empty($op_op_op_name[$k][$o])) {

                                // print_r($op_op_op_name[$k][$o]);

                                $j = 0;

                                foreach ($op_op_op_name[$k][$o] as $op_op_op) {

                                    // For sub sub attribute image upload : START
                                    if (@$_FILES['op_op_op_attr_img']['name'][$k][$o][$j]) {

                                        $_FILES['final_file']['name']     = $_FILES['op_op_op_attr_img']['name'][$k][$o][$j];
                                        $_FILES['final_file']['type']     = $_FILES['op_op_op_attr_img']['type'][$k][$o][$j];
                                        $_FILES['final_file']['tmp_name'] = $_FILES['op_op_op_attr_img']['tmp_name'][$k][$o][$j];
                                        $_FILES['final_file']['error']    = $_FILES['op_op_op_attr_img']['error'][$k][$o][$j];
                                        $_FILES['final_file']['size']     = $_FILES['op_op_op_attr_img']['size'][$k][$o][$j];

                                        $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                                        $config['allowed_types'] = 'jpeg|jpg|png';
                                        $config['overwrite'] = false;
                                        $config['max_size'] = 2048;
                                        $config['remove_spaces'] = true;

                                        $this->load->library('upload', $config);
                                        $this->upload->initialize($config);

                                        if (!$this->upload->do_upload('final_file')) {
                                            $upload_file = '';
                                        } else {
                                            $data = $this->upload->data();
                                            $upload_file = $config['upload_path'] . $data['file_name'];
                                        }
                                    } else {
                                        $upload_file = '';
                                    }
                                    // For sub sub attribute image upload : END

                                    $OpOpOpData = array(
                                        'att_op_op_op_name' => $op_op_op,
                                        'att_op_op_op_type' => @$op_op_op_type[@$k][@$o][@$j],
                                        'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                        'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                        'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j],
                                        'op_id' => @$att_op_id,
                                        'att_op_op_id' => @$op_op_id,
                                        'attribute_id' => @$attribute_id,
                                        'att_op_op_op_images' => $upload_file
                                    );

                                    $j++;

                                    // print_r($OpOpOpData);

                                    $this->db->insert('attr_options_option_option_tbl', $OpOpOpData);

                                    // echo $this->db->last_query();die;
                                }
                            }
                        }

                        $oi++;
                        $o++;
                    }
                }

                $k++;
            }
            //exit;
        }
    }





    function edit_attribute($attribute_id,$request)
    {
        $user_id = $this->session->userdata('user_id');
        $updated_date = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attribute_id = $this->input->post('attribute_id');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'updated_by' => $user_id,
            'updated_date' => $updated_date,
        );

        $this->db->where('attribute_id', $attribute_id)->update('attribute_tbl', $attributeData);


        $option_ids = $this->input->post('option_id');
        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');
        $hid_attr_img = $this->input->post('hid_attr_img');

        $op_op_ids = $this->input->post('op_op_id');
        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $att_op_op_price_type = $this->input->post('op_op_price_type');
        $att_op_op_price = $this->input->post('op_op_price');
        $att_op_op_condition = $this->input->post('op_op_condition');
        $hid_op_op_attr_img = $this->input->post('hid_op_op_attr_img');


        $op_op_op_ids = $this->input->post('op_op_op_id');
        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');
        $hid_op_op_op_attr_img = $this->input->post('hid_op_op_op_attr_img');

        $k = 0;
        for ($k = 0; $k <= count($option_ids); $k++) {
            if (array_key_exists($k, $option_ids)) {

                // For attribute image upload : START
                if (@$_FILES['attr_img']['name'][$k]) {

                    $_FILES['final_file']['name']     = $_FILES['attr_img']['name'][$k];
                    $_FILES['final_file']['type']     = $_FILES['attr_img']['type'][$k];
                    $_FILES['final_file']['tmp_name'] = $_FILES['attr_img']['tmp_name'][$k];
                    $_FILES['final_file']['error']    = $_FILES['attr_img']['error'][$k];
                    $_FILES['final_file']['size']     = $_FILES['attr_img']['size'][$k];

                    $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                    $config['allowed_types'] = 'jpeg|jpg|png';
                    $config['overwrite'] = false;
                    $config['max_size'] = 2048;
                    $config['remove_spaces'] = true;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('final_file')) {
                        $upload_file = '';
                    } else {
                        $data = $this->upload->data();
                        $upload_file = $config['upload_path'] . $data['file_name'];
                    }

                    $this->remove_old_images($hid_attr_img[$k]);
                } else {
                    $upload_file = $hid_attr_img[$k];
                }
                // For attribute image upload : END

                $optionData = array(
                    'option_name' => $option_name[$k],
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k],
                    'attributes_images' => $upload_file,
                );

                $this->db->where('att_op_id', $option_ids[$k])->update('attr_options', $optionData);

                if (!empty($op_op_ids[@$k])) {
                    $o = 0;
                    for ($o = 0; $o <= count($op_op_ids[$k]); $o++) {

                        if (array_key_exists($o, $op_op_ids[$k])) {

                            // For sub attribute image upload : START
                            if (@$_FILES['op_op_attr_img']['name'][$k][$o]) {

                                $_FILES['final_file']['name']     = $_FILES['op_op_attr_img']['name'][$k][$o];
                                $_FILES['final_file']['type']     = $_FILES['op_op_attr_img']['type'][$k][$o];
                                $_FILES['final_file']['tmp_name'] = $_FILES['op_op_attr_img']['tmp_name'][$k][$o];
                                $_FILES['final_file']['error']    = $_FILES['op_op_attr_img']['error'][$k][$o];
                                $_FILES['final_file']['size']     = $_FILES['op_op_attr_img']['size'][$k][$o];

                                $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                                $config['allowed_types'] = 'jpeg|jpg|png';
                                $config['overwrite'] = false;
                                $config['max_size'] = 2048;
                                $config['remove_spaces'] = true;

                                $this->load->library('upload', $config);
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('final_file')) {
                                    $upload_file = '';
                                } else {
                                    $data = $this->upload->data();
                                    $upload_file = $config['upload_path'] . $data['file_name'];
                                }

                                $this->remove_old_images($hid_op_op_attr_img[$k][$o]);
                            } else {
                                $upload_file = $hid_op_op_attr_img[$k][$o];
                            }
                            // For sub attribute image upload : END

                            $optionOptionData = array(
                                'op_op_name' => $option_optin_name[@$k][@$o],
                                'type' => $option_option_type[$k][$o],
                                'option_id' => $option_ids[$k],
                                'attribute_id' => $attribute_id,
                                'att_op_op_price_type' => $att_op_op_price_type[$k][$o],
                                'att_op_op_price' => $att_op_op_price[$k][$o],
                                'att_op_op_condition' => $att_op_op_condition[$k][$o],
                                'att_op_op_images' => $upload_file,
                            );
                            $this->db->where('op_op_id', $op_op_ids[$k][$o])->update('attr_options_option_tbl', $optionOptionData);

                            if (!empty($op_op_op_ids[@$k][$o])) {

                                for ($j = 0; $j <= count($op_op_op_ids[$k][$o]); $j++) {

                                    if (!empty($op_op_op_ids[$k][$o][$j])) {

                                        // For sub sub attribute image upload : START
                                        if (@$_FILES['op_op_op_attr_img']['name'][$k][$o][$j]) {

                                            $_FILES['final_file']['name']     = $_FILES['op_op_op_attr_img']['name'][$k][$o][$j];
                                            $_FILES['final_file']['type']     = $_FILES['op_op_op_attr_img']['type'][$k][$o][$j];
                                            $_FILES['final_file']['tmp_name'] = $_FILES['op_op_op_attr_img']['tmp_name'][$k][$o][$j];
                                            $_FILES['final_file']['error']    = $_FILES['op_op_op_attr_img']['error'][$k][$o][$j];
                                            $_FILES['final_file']['size']     = $_FILES['op_op_op_attr_img']['size'][$k][$o][$j];

                                            $config['upload_path'] = './assets/retailer/uploads/attributes_images/';
                                            $config['allowed_types'] = 'jpeg|jpg|png';
                                            $config['overwrite'] = false;
                                            $config['max_size'] = 2048;
                                            $config['remove_spaces'] = true;

                                            $this->load->library('upload', $config);
                                            $this->upload->initialize($config);

                                            if (!$this->upload->do_upload('final_file')) {
                                                $upload_file = '';
                                            } else {
                                                $data = $this->upload->data();
                                                $upload_file = $config['upload_path'] . $data['file_name'];
                                            }
                                            $this->remove_old_images($hid_op_op_op_attr_img[$k][$o][$j]);
                                        } else {
                                            $upload_file = $hid_op_op_op_attr_img[$k][$o][$j];
                                        }
                                        // For sub sub attribute image upload : END

                                        $optionOptionOptionData = array(
                                            'att_op_op_op_name' => $op_op_op_name[@$k][@$o][$j],
                                            'att_op_op_op_type' => $op_op_op_type[$k][$o][$j],
                                            'attribute_id' => $attribute_id,
                                            'op_id' => $option_ids[$k],
                                            'att_op_op_id' => $op_op_ids[$k][$o],
                                            'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                            'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                            'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j],
                                            'att_op_op_op_images' => $upload_file
                                        );

                                        $this->db->where('att_op_op_op_id', $op_op_op_ids[$k][$o][$j])->update('attr_options_option_option_tbl', $optionOptionOptionData);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute updated successfully!</div>");
        redirect("customer/catalog/attribute/edit/" . $attribute_id);

    }

    public function remove_old_images($path){
        if(is_file($path)){
            unlink($path);
        }
    }

     function category_wise_condition($category_id) {
        $query = $this->db->select('*')->from('product_conditions_tbl')->where('category_id', $category_id)
                        ->where('is_active', 1)
                        ->get()->result();
        return $query;
    }

    function checked_product_wise_condition($product_id) {
        $query = $this->db->select('*')->from('product_condition_mapping')->where('product_id', $product_id)
                        ->get()->result();
        return $query;
    }

    function get_product_by_id($product_id) {
        $query = $this->db->select('a.*,b.style_name, c.category_name,')
                        ->from('product_tbl a')
                        ->join('row_column b', 'b.style_id=a.price_rowcol_style_id', 'left')
                        ->join('category_tbl c', 'c.category_id=a.category_id', 'left')
                        ->where('a.product_id', $product_id)
                        ->get()->row();
        return $query;
    }

    public function get_category() {

        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $query = $this->db->select('*')
                        ->from('category_tbl')
                        ->where('parent_category', 0)
                        ->where('status', 1)
                        ->where('created_by',$created_by)
                        ->order_by('category_name', 'asc')
                        ->get()->result();
        return $query;
    }

    function get_retailer_upcharge_price_attribute($search='')
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->db->select("attribute_tbl.*,category_tbl.category_name");
        $this->db->from('attribute_tbl');
        $this->db->join('category_tbl', 'category_tbl.category_id=attribute_tbl.category_id', 'left');

        if($search->attribute_name!='') {
            $this->db->like('attribute_tbl.attribute_name', $search->attribute_name, 'both');
        }
        if ($search->category_id!='') {
            $this->db->like("attribute_tbl.category_id", $search->category_id, 'both');
        }

        $this->db->group_start();

        if (!empty($main_b_id)) {
            $this->db->group_start();
            $this->db->where('attribute_tbl.created_by', $main_b_id);
            $this->db->where('attribute_tbl.created_status', 0);
            $this->db->group_end(); //close bracket
        }

        $user_id = $this->session->userdata('user_id');
        if(empty($main_b_id))
            $this->db->group_start();
        else
            $this->db->or_group_start();
        $this->db->where('attribute_tbl.created_by', $user_id);
        $this->db->where('attribute_tbl.created_status', 1);
        $this->db->group_end(); //close bracket
        
        $this->db->group_end(); //close bracket

        $response = $this->db->get()->result();
       return $response;
    }

}
