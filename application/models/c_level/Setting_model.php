<?php

class Setting_model extends CI_model
{

    //    ============= its for my_account ==========
    public function my_account()
    {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('user_info a');
        $this->db->where('a.id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    //    =========== its for get_product ============
    public function get_product()
    {
        $admin_created_by = $this->session->userdata('admin_created_by');
        $user_id = $this->session->userdata('user_id');
        $where = "(created_by = " . $admin_created_by . " OR created_by = " . $user_id . ")";
        $query = $this->db->select('a.product_id, a.product_name')
            ->from('product_tbl a')
            ->where('active_status', 1)
            ->where($where)
            ->order_by('a.product_name', 'asc')
            ->get()->result();
        return $query;
    }

    //    ========= its for  get_only_product for retailer cost factor===========
    public function get_only_product_c_cost_factor($level_id)
    {
        $this->db->select('a.product_id, a.product_name, b.individual_cost_factor, b.costfactor_discount');
        $this->db->from('product_tbl a');
        $this->db->join('c_cost_factor_tbl b', 'b.product_id = a.product_id', 'left');
        $this->db->where('b.level_id', $level_id);
        $query = $this->db->get();
        return $query->result();
    }

    //    ========= its for  get_only_product for retailer cost factor by product id===========
    public function get_product_c_cost_factor_by_id($level_id, $product_id)
    {
        $this->db->select('a.product_id, a.product_name, b.individual_cost_factor, b.costfactor_discount');
        $this->db->from('product_tbl a');
        $this->db->join('c_cost_factor_tbl b', 'b.product_id = a.product_id', 'left');
        $this->db->where('b.level_id', $level_id);
        $this->db->where('b.product_id', $product_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    //    ============= its for company_profile ================
    public function company_profile()
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        echo $level_id;die();
        $query = $this->db->select('company_profile.*')
            ->from('company_profile')
            //                ->join('customer_info','customer_info.customer_user_id=company_profile.user_id')
            ->where('company_profile.user_id', $level_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function single_company_profile($level_id) {

        $query = $this->db->select('*')
                ->from('company_profile')
                ->where('user_id', $level_id)
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //    ============ its for check check_iframe_code exists =============
    public function check_iframe_code()
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('user_created_by');
        }
        $query = $this->db->select('*')
            ->from('iframe_code_tbl a')
            ->where('created_by', $level_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //=========== its for web_setting ===============
    public function gateway_list($offset, $limit)
    {
        $query = $this->db->select('*')
            ->from('gateway_tbl')
            ->order_by('id', 'desc')
            ->limit($offset, $limit)
            ->where('created_by', $this->session->userdata('user_id'))
            ->get()->result();
        return $query;
    }

    //==================== its for gateway_edit ==============
    public function gateway_edit($id)
    {
        $query = $this->db->select('*')
            ->from('gateway_tbl a')
            ->where('a.id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //    =========== its for uom_list ==============
    public function us_state_list($offset = null, $limit = null)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.created_by=" . $user_id;
        }
        $query = $this->db->select("*")
            ->from('c_us_state_tbl a')
            ->where($filter)
            ->order_by('a.state_id', 'desc')
            ->limit($offset, $limit)
            ->get()->result();
        return $query;
    }

    //==================== its for us_state_edit ==============
    public function us_state_edit($id)
    {
        $query = $this->db->select('*')
            ->from('c_us_state_tbl a')
            ->where('a.state_id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function access_logs($offset = null, $limit = null)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "a.level_id=" . $level_id;
        } else {
            $filter = "a.user_name=" . $user_id;
        }
        $query = $this->db->select('a.*, CONCAT( b.first_name, " ", b.last_name) AS name')
            ->from('accesslog a')
            ->join('user_info b', 'b.id = user_name')
            //                        ->where('a.user_name', $level_id)
            ->where($filter)
            ->order_by('a.entry_date', 'desc')
            ->limit($offset, $limit)
            ->get()->result();
        return $query;
    }

    public function sendStaffAppointmentMail($c_level_staff_id, $email, $staff_name, $customer_name, $customer_phone, $data, $created_by = '') {
        $data['name'] = $staff_name;
        $data['customer_name'] = $customer_name;
        $data['customer_phone'] = $customer_phone;
        $subject = 'Appointment Information';
        $message = $data['name'];
        $CI = &get_instance();
        $CI->load->model('Common_model');
        if (empty($created_by)) {
            $created_by = $this->session->userdata('user_id');
        }
        $config = $CI->Common_model->mailConfig($created_by);
        $mesg = $this->load->view('c_level/settings/send_appointment', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMS Decor");
        $this->email->message($mesg);
        $this->email->send();
    }

    public function sendClientAppointmentMail($customer_id, $email, $customer_name, $staff_name, $staff_phone, $data, $created_by = '') {
        $data['name'] = $customer_name;
        $data['customer_name'] = $staff_name;
        $data['customer_phone'] = $staff_phone;
        $subject = 'Appointment Information';
        $message = $data['name'];
        $CI = &get_instance();
        $CI->load->model('Common_model');
        if (empty($created_by)) {
            $created_by = $this->session->userdata('user_id');
        }
        $config = $CI->Common_model->mailConfig($created_by);
        $mesg = $this->load->view('c_level/settings/send_client_appointment', $data, TRUE);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMS Decor");
        $this->email->message($mesg);
        $this->email->send();
    }

    //    =========== its for customer_user_info ==============
    public function user_information($c_level_staff_id)
    {
        $query = $this->db->select('*')
            ->from('user_info a')
            //                        ->join('user_info b', 'b.id = a.user_id', 'left')
            ->where('a.id', $c_level_staff_id)
            ->get()->row();
        return $query;
    }

    //    ============ its for staff appointment sms send =============
    public function sendStaffAppointmentSms($c_level_staff_id, $staff_name, $customer_name, $customer_phone, $data, $created_by = '')
    {
        $user_info = $this->db->where('id', $c_level_staff_id)->get('user_info')->row();
        $appointment_date = $data['get_form_data']['appointment_date'];
        $appointment_time = $data['get_form_data']['appointment_time'];
        $appointment_remarks = $data['get_form_data']['remarks'];
        // echo $appointment_remarks;        die();
        if ($user_info->phone != NULL) {
            $this->load->library('twilio');
            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                if (empty($created_by)) {
                    $created_by = $this->session->userdata('user_id');
                }
                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $created_by)->where('is_verify', '1')->where('default_status', 1)->get()->row();
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $user_info->phone;
                // $message = 'Hi! ' . $staff_name . ', Your Appointment scheduled with ' . @$customer_name . " ph : " . @$customer_phone . " on " . $appointment_date . " " . $appointment_time . ". " . $appointment_remarks . ".";
                $message = 'Hi! ' . $staff_name . ', Your Appointment scheduled with ' . @$customer_name . " ph : " . @$customer_phone . " on " . $appointment_date . " " . $appointment_time . ".";
                if ($sms_gateway_info->provider_name == 'Twilio') {
                    $response = $this->twilio->sms($from, $to, $message);
                    if ($response->IsError) {
                        echo 'Error: ' . $response->ErrorMessage;
                    } else {
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                    }
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
            }
            return 1;
        }
        //    }
    }

    public function sendClientAppointmentSms($customer_id, $customer_name, $staff_name, $staff_phone, $data, $created_by = '')
    {
        $user_info = $this->db->select('*')->from('customer_info a')->where('a.customer_id', $customer_id)->get()->row();
        $appointment_date = $data['get_form_data']['appointment_date'];
        $appointment_time = $data['get_form_data']['appointment_time'];
        $appointment_remarks = $data['get_form_data']['remarks'];
        $user_company = $data['get_form_data']['user_company'];
        if ($user_info->phone != NULL) {
            $this->load->library('twilio');
            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                if (empty($created_by)) {
                    $created_by = $this->session->userdata('user_id');
                }
                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $created_by)->where('is_verify', '1')->where('default_status', 1)->get()->row();
                $from = $sms_gateway_info->phone; 
                $to = $user_info->phone;
                $message = 'Hi! ' . $customer_name . ', Your Appointment scheduled with ' . @$staff_name . " ph : " . @$staff_phone . " on " . $appointment_date . " " . $appointment_time . ". " . $user_company . ".";
                if ($sms_gateway_info->provider_name == 'Twilio') {
                    $response = $this->twilio->sms($from, $to, $message);
                    if ($response->IsError) {
                        echo 'Error: ' . $response->ErrorMessage;
                    } else {
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                    }
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
            }
            return 1;
        }
        //    }
    }

    // ============ its for get_sms_config ================
    public function get_sms_config()
    {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->order_by('a.gateway_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    // =============== its for sms_config_edit ==============
    public function sms_config_edit($gateway_id)
    {
        $this->db->select('*');
        $this->db->from('sms_gateway a');
        $this->db->where('a.gateway_id', $gateway_id);
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //================= its for get_mail_config ==============
    public function get_mail_config()
    {
        $this->db->select('*');
        $this->db->from('mail_config_tbl a');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }
    //========== its for send Custom email ===============
    public function sendCstomEmail($email, $message, $isverified = true)
    {
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'), $isverified);
        if (isset($config['error'])) {
            return false;
        }
        $this->load->library('email', $config);

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("BMSLINK");
        $this->email->message($message);
            // echo "<pre>";
            // print_r($this->email);die;
        if ($this->email->send()) {
            $email_data = array(
                'from' => $config['smtp_user'],
                'to' => $email,
                'message' => $message,
                'created_by' => $this->session->userdata('user_id'),
            );
            $this->db->insert('custom_email_tbl', $email_data);
            return true;
        } else {
            return false;
        }
    }

    //=============wholesaler cost-factor 
    public function blevel_cost_data()
    {

        $user_id = $this->session->userdata('user_id');
        $this->db->distinct();
        $this->db->select('b_cost_factor_tbl.*,
            product_tbl.product_name,
            customer_info.first_name,
            customer_info.last_name,
            customer_info.company
            ');
        $this->db->from('b_cost_factor_tbl');
        $this->db->join('user_info', 'b_cost_factor_tbl.created_by = user_info.created_by');
        $this->db->join('product_tbl', 'product_tbl.product_id=b_cost_factor_tbl.product_id');
        $this->db->join('customer_info', 'customer_info.customer_id=b_cost_factor_tbl.customer_id');
        $this->db->where('customer_info.customer_user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    // ============ its for send email to customer for appointment =============
    public function sendStaffAppointmentMailCustomer($email, $staff_name, $data, $created_by)
    {
        $subject = 'Appointment Information';
        $CI = &get_instance();
        $CI->load->model('Common_model');
        $config = $CI->Common_model->mailConfig($created_by);
        $mesg = "Appointment Date: " . $data['get_form_data']['appointment_date'] . "<br/>Appointment Time:" . $data['get_form_data']['appointment_time'];
        $mesg .= '<br/> Staff Name: ' . $staff_name . '<br/> Company: ' . $data['get_form_data']['company'];

        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        $this->email->set_header('Content-type', 'text/html');

        $this->load->library('email', $config);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user'], "Support Center");
        $this->email->to($email);
        $this->email->subject("Welcome to BMS Decor");
        $this->email->message($mesg);
        $this->email->send();
    }

    //    ============ its for staff appointment sms send =============
    public function sendStaffAppointmentSmsCustomer($customer_phone, $staff_name, $data, $created_by)
    {
        $appointment_date = $data['get_form_data']['appointment_date'];
        $appointment_time = $data['get_form_data']['appointment_time'];
        $company = $data['get_form_data']['company'];
        if ($customer_phone != NULL) {
            $this->load->library('twilio');
            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $created_by)->where('is_verify', '1')->where('default_status', 1)->get()->row();
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = "+88" . $customer_phone;
                $message = "Appointment Date: " . $appointment_date . " ,Appointment Time:" . $appointment_time . ", Staff Name: " . $staff_name . ", Company: " . $company;
                if ($sms_gateway_info->provider_name == 'Twilio') {
                    $response = $this->twilio->sms($from, $to, $message);
                    if ($response->IsError) {
                        echo 'Error: ' . $response->ErrorMessage;
                    } else {
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                    }
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
            }
            return 1;
        }
    }

    public function b_company_data() {
        $created_by = $this->session->userdata('admin_created_by');
        $this->db->select('logo,company_name');
        $this->db->from('company_profile');        
        $this->db->where('user_id', $created_by);
        $query = $this->db->get();
        return $query->row();
    }

    public function c_company_data() {
        $user_id = $this->session->userdata('user_id');
        $this->db->select('logo');
        $this->db->from('company_profile');        
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function uom_list($offset = null, $limit = null) {
        $query = $this->db->select("*")
                        ->from('unit_of_measurement')
                        ->where('user_id', $this->session->userdata('user_id'))
                        ->order_by('uom_id', 'desc')
                        ->limit($offset, $limit)
                        ->get()->result();
        return $query;
    }

     public function get_unit_data(){
        $res = $this->db->where('user_id',$this->session->userdata('user_id'))->get('unit_of_measurement')->result();
        return $res;
    }

    public function get_mapping_data($from_id,$to_id) {
        $res = $this->db->where('user_id',$this->session->userdata('user_id'))->where('from_measure_id',$from_id)->where('to_measure_id',$to_id)->limit(1)->get('mapping_measurement')->row();
        return $res;
    }

    public function uom_edit($id) {
        $query = $this->db->select('a.*')
                ->from('unit_of_measurement a')
                ->where('a.uom_id', $id)
                ->where('user_id', $this->session->userdata('user_id'))
                ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function add_quickbooks_setting_data($data){
        $this->db->insert('quickbook_setting_tbl',$data);
        return true;
    }

    //get quickbooks setting data
    public function getQuickBooksSettingData($user_id){
        return $this->db->where('user_id',$user_id)->get('quickbook_setting_tbl')->row();
    }
}
