<?php

class Customer_model extends CI_model
{

    //    ========= its for doctor certificate save ==========
    public function save_customer_file($customerFileinfo = array())
    {
        $this->db->insert_batch('customer_file_tbl', $customerFileinfo);
    }

    //============ its for get_customer method ==============
    public function get_customer($offset = null, $limit = null)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $first_name = $this->session->userdata('search_first_name');
        $sidemark = $this->session->userdata('search_sidemark');
        $phone = $this->session->userdata('search_phone');
        $address = $this->session->userdata('search_address');
        $filter_status = $this->session->userdata('search_filter_status');

        if ($first_name) {
            $this->db->like("CONCAT(c.first_name,  ' ', c.last_name)", $first_name, 'both');
        }
        if ($sidemark) {
            $this->db->like('c.side_mark', $sidemark, 'both');
        }
        if ($phone) {
            $this->db->like('c.phone', $phone, 'both');
        }
        if ($address) {
            $this->db->like('c.address', $address, 'both');
        }
        if ($filter_status) {
            $this->db->where('c.now_status', $filter_status);
        }

        if ($offset != '') {
            $this->db->limit($offset, $limit);
        }

        $query = $this->db->select('c.*, CONCAT(" ", c.first_name,  c.last_name) as full_name,c.is_active')
            ->from('customer_info c')
            ->where('c.level_id', $level_id)
            ->or_where('c.created_by', $this->session->userdata('user_id'))
            ->order_by('c.customer_id', 'desc')
            ->get()->result();  
            return $query;
    }

    //    =========== its for customer_edit ==============
    public function customer_edit($id)
    {
        $query = $this->db->select('a.*')
            ->from('customer_info a')
            //                ->join('customer_phone_type_tbl b', 'b.customer_id = a.customer_id', 'left')
            ->where('a.customer_id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //    ========== its for get_states =================
    public function get_states()
    {
        $query = $this->db->select('*')->from('city_state_tbl')->group_by('state_name')->get()->result();
        return $query;
    }

    //    =============== its for get_states_wise_city ===============
    public function get_states_wise_city($state_id)
    {
        $query = $this->db->select('*')->from('city_state_tbl')->where('state_id', $state_id)->get()->result();
        return $query;
    }

    //    =============== its for customer_view ================= 
    public function customer_view($id)
    {
        $query = $this->db->select('a.*')
            ->from('customer_info a')
            ->where('a.customer_id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //    ============= its for get_customer_phones =============
    public function get_customer_phones($id)
    {
        $this->db->select('*');
        $this->db->from('customer_phone_type_tbl a');
        $this->db->where('a.customer_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    //    =============== its for show_customer_record ================= 
    public function show_customer_record($id)
    {
        $query = $this->db->select('a.*, b.created_by AS c_level_parent_user, CONCAT( b.first_name, "&nbsp;", b.last_name) AS comment_by')
            ->from('customer_info a')
            ->join('user_info b', 'b.id = a.created_by')
            ->where('a.customer_id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //    ============= its for show_customer_appointment_record ============
    public function show_customer_appointment_record($id)
    {
        $this->db->select('*');
        $this->db->from('appointment_calendar a');
        $this->db->where('a.customer_id', $id);
        $this->db->order_by('a.appointment_id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    //    ============== its for get_customer_filter_info =============
    public function get_customer_filter_info($first_name, $sidemark, $phone, $address)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.*, CONCAT(" ", first_name,  last_name) as full_name');
        $this->db->from('customer_info a');
        if ($first_name) {
            $this->db->like('a.first_name', $first_name, 'both');
        }
        if ($sidemark) {
            $this->db->like('a.side_mark', $sidemark, 'both');
        }
        if ($phone) {
            $this->db->like('a.phone', $phone, 'both');
        }
        if ($address) {
            $this->db->like('a.address', $address, 'both');
        }
        $this->db->where('a.level_id', $level_id);
        $query = $this->db->get();
        return $query->result();
    }

    //    ============ its for accounts table hit ============
    public function headcode($level_id)
    {
        $query = $this->db->query("SELECT HeadCode FROM acc_coa WHERE level_id='" . $level_id . "' AND HeadLevel='4' AND HeadCode LIKE '1020301-%' ORDER BY row_id DESC LIMIT 1");
        return $query->row();
    }

    //    ========== its for get_customer_search_result =============
    public function get_customer_search_result($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $sql = "SELECT a.*
            	FROM
            	customer_info a
            	WHERE (a.address LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id ORDER BY a.customer_id DESC LIMIT 50";
        //        echo $sql;        die();
        $query = $this->db->query($sql);
        return $query->result();
    }

    //    ========= its for customer_csv_data ================
    public function customer_csv_data($offset, $limit)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.side_mark, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($offset, $limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function customer_pdf_data($offset, $limit)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select('a.customer_id, a.first_name, a.last_name, a.side_mark, a.phone, a.email, a.company, a.address, a.city, a.state, a.zip_code, a.country_code');
        $this->db->from('customer_info a');
        $this->db->where('a.level_id', $level_id);
        $this->db->limit($offset, $limit);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    //    =========== its for get_top_search_customer_info =============
    public function get_top_search_customer_info($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $sql = "SELECT a.*
	FROM
	customer_info a
	WHERE (a.company_customer_id LIKE '%$keyword%' OR a.company LIKE '%$keyword%' OR a.first_name LIKE '%$keyword%' 
                 OR a.phone LIKE '%$keyword%' OR a.email LIKE '%$keyword%')
                AND a.level_id = $level_id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //    ========== its for  get_top_search_order_info ===========
    public function get_top_search_order_info($keyword)
    {
        $this->db->select("a.*, CONCAT(b.first_name,' ',b.last_name) as customer_name");
        $this->db->from('quatation_tbl a');
        $this->db->join('customer_info b', 'b.customer_id = a.customer_id');
        $this->db->like('a.order_id', $keyword);
        $query = $this->db->get();
        return $query->result();
    }

    //    ============ its for get_customer_order ===========
    public function get_customer_order($customer_id)
    {
        $this->db->select("a.*,CONCAT(b.first_name,' ',b.last_name) as customer_name");
        $this->db->from('quatation_tbl a');
        $this->db->join('customer_info b', 'b.customer_id = a.customer_id', 'left');
        $this->db->where('a.customer_id', $customer_id);
        $this->db->order_by('a.order_date', 'DESC');
        $query = $this->db->get()->result();
        return $query;
    }

    //    ========== its for  get_top_search_quotation_info ===========
    public function get_top_search_quotation_info($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select("c_quatation_table.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
            ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
            ->where('c_quatation_table.qt_created_by', $level_id)
            ->group_start()
            ->like('c_quatation_table.qt_id', $keyword)
            ->or_like('CONCAT(customer_info.first_name," ",customer_info.last_name)', $keyword)
            ->or_like('customer_info.first_name', $keyword)
            ->or_like('customer_info.last_name', $keyword)
            ->or_like('c_quatation_table.qt_order_date', $keyword)
            ->or_like('c_quatation_table.qt_grand_total', $keyword)
            ->group_end();
        $query =  $this->db->get('c_quatation_table')->result();
        return $query;
    }

    //    ========== its for  get_top_search_order_cancel_info ===========
    public function get_top_search_order_cancel_info($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
            ->from('quatation_tbl')
            ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left')
            ->where('quatation_tbl.level_id', $level_id)
            ->where('quatation_tbl.order_stage', 6)
            ->group_start()
            ->like('quatation_tbl.order_id', $keyword)
            ->or_like('CONCAT(customer_info.first_name," ",customer_info.last_name)', $keyword)
            ->or_like('customer_info.first_name', $keyword)
            ->or_like('customer_info.last_name', $keyword)
            ->or_like('quatation_tbl.order_date', $keyword)
            ->or_like('quatation_tbl.cancel_comment', $keyword)
            ->or_like('quatation_tbl.grand_total', $keyword)
            ->group_end();
        $query =  $this->db->get()->result();
        return $query;
    }

    //    ========== its for  get_top_search_order_cancel_info ===========
    public function get_top_search_customer_return_info($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name, b.side_mark')
            ->from('order_return_tbl a')
            ->join('customer_info b', 'b.customer_id = a.customer_id')
            ->where('a.level_id', $level_id)
            ->group_start()
            ->like('a.order_id', $keyword)
            ->or_like('CONCAT(b.first_name , " " , b.last_name)', $keyword)
            ->or_like('b.first_name', $keyword)
            ->or_like('b.last_name', $keyword)
            ->or_like('b.side_mark', $keyword)
            ->or_like('a.return_comments', $keyword)
            ->or_like('date_format(a.return_date, "%m-%d-%y")', $keyword)
            ->group_end()
            ->get()->result();
        return $query;
    }

    //    ========== its for  get_top_search_order_cancel_info ===========
    public function get_top_search_purchase_quotation_info($keyword)
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $query = $this->db->select('a.*, CONCAT(b.first_name , " " , b.last_name) as customer_name')
            ->from('b_level_quatation_tbl a')
            ->join('customer_info b', 'b.customer_id = a.customer_id')
            ->where('a.level_id', $level_id)
            ->where('a.status', 1)
            ->group_start()
            ->like('a.order_id', $keyword)
            ->or_like('CONCAT(b.first_name , " " , b.last_name)', $keyword)
            ->or_like('b.first_name', $keyword)
            ->or_like('b.last_name', $keyword)
            ->or_like('a.side_mark', $keyword)
            ->or_like('date_format(a.order_date, "%m-%d-%y")', $keyword)
            ->group_end()
            ->get()->result();
        return $query;
    }

    public function get_all_customers($user_id){
        $query = $this->db->select('c.*, CONCAT(" ", c.first_name,  c.last_name) as full_name,c.is_active,u.user_type')
        ->join('user_info as u','c.level_id = u.id','left')
        ->where('c.is_added_in_quickbooks',0)
        ->where('c.created_by',$user_id)
        ->from('customer_info c')
        ->get()->result();
        return $query;
    }

    public function updateQuickbooksStatus($id='')
    {
        $this->db->set('is_added_in_quickbooks',1);
        $this->db->where('customer_id',$id);
        $this->db->update('customer_info');
        return true;
    }

    public function updateQuickbooksId($customer_id, $quickBooksId){
        $this->db->set('quick_books_id',$quickBooksId);
        $this->db->where('customer_id',$customer_id);
        $this->db->update('customer_info');
        if($this->db->affected_rows() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function get_all_customer_quick_book_id($user_id){
        $query = $this->db->select('*')
        ->from('customer_info')
        ->where('created_by', $user_id)
        ->where('is_added_in_quickbooks',1)
        ->where('quick_books_id >',0)
        ->get()->result_array();
        return $query;
    }

    public function get_by_id($id){
        $query = $this->db->select('a.*')
            ->from('customer_info a')
            ->where('a.customer_id', $id)
            ->where('a.is_added_in_quickbooks', 1)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
}
