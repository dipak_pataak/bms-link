<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_model {

	public function payment_type(){
		return $data = array('1' => 'Cash','2'=>'Check' );
    }	
    
    // START For Multiple delete : Insys
	public function DeleteSelected($table,$fields='id'){
		$this->db->where_in($fields, $this->input->post('Id_List'));
		$query = $this->db->delete($table);
		return $query;
	}
	// END For Multiple delete : Insys
    
    // START For Mail configuration : Insys
	public function mailConfig($userId, $isVerified = true) {
        $this->db->select('*');
        $this->db->from('mail_config_tbl');
        $this->db->where('created_by', $userId);
      
        if($isVerified) {
            $this->db->where('is_verified', 1);
        }
        $query = $this->db->get();
        $mail_config = $query->row_array();
        if($mail_config){
            $config = Array(
                'protocol' => $mail_config['protocol'], //'smtp',
                'smtp_host' => $mail_config['smtp_host'], //'ssl://smtp.gmail.com',
                'smtp_port' => $mail_config['smtp_port'], //465,
                'smtp_user' => $mail_config['smtp_user'], //'khs2010welfare@gmail22.com', // change it to yours
                'smtp_pass' => $mail_config['smtp_pass'], // 'bvrayygbwwmxnkd111j', // change it to yours
                'mailtype' => $mail_config['mailtype'], //'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );
        } else {
            $config = Array(
                'error' => true,
            );
        }
        return $config;
	}
	// END For Mail configuration : Insys

    // START For Generate order id : Insys
    // Param 1 : $customer_id : Customer ID
    // Param 2 : $table_name : Table Name
    // Param 3 : $select_field : Select field from that table
    // Param 4 : $order_field : For get last id from that table

    public function generate_order_id($customer_id,$table_name,$select_field='order_id',$order_field='id'){

        $customer_wise_sidemark = $this->db->select('customer_info.*')
                        ->from('customer_info')
                        ->where('customer_info.customer_id', $customer_id)
                        ->get()->row();

        $user_info_data = $this->db->from('user_info')->where('id', $this->session->userdata('user_id'))->get()->row();

        $company_name = $user_info_data->company;

         // Get Company name : START
        if ($company_name != '') {
            $comp_name = str_replace(' ', '', $company_name);
            $comp_name_length = strlen(utf8_decode($comp_name));
            if ($comp_name_length >= 4) {
                $comp_name = mb_substr($comp_name, 0, 4, "utf-8");
            } else if ($comp_name_length == 3) {
                $comp_name = $comp_name . mb_substr($comp_name, 2, 1, "utf-8");
            } else if ($comp_name_length == 2) {
                $comp_name = $comp_name . $comp_name;
            } else if ($comp_name_length == 1) {
                $comp_name = $comp_name . $comp_name . $comp_name . $comp_name;
            } else {
                $comp_name = "XXXX";
            }
        } else {
            $comp_name = "XXXX";
        }

        // Get Company name : END

        //Get Side Mark : START
        $custom_sidemark = $customer_wise_sidemark->side_mark;
        if ($custom_sidemark == '') {
            $final_site_mark = "XXXX-XXXX";
        } else {
            $sidemark_temp = explode('-', $custom_sidemark);
            $sidemark_first = $sidemark_temp[0];
            $sidemark_second = $sidemark_temp[1];

            $sidemark_first_length = strlen(utf8_decode($sidemark_first));
            $sidemark_second_length = strlen(utf8_decode($sidemark_second));

            if ($sidemark_first_length >= 4) {
                $sidemark_first = mb_substr($sidemark_first, 0, 4, "utf-8");
            } else if ($sidemark_first_length == 3) {
                $sidemark_first = $sidemark_first . mb_substr($sidemark_first, 2, 1, "utf-8");
            } else if ($sidemark_first_length == 2) {
                $sidemark_first = $sidemark_first . $sidemark_first;
            } else if ($sidemark_first_length == 1) {
                $sidemark_first = $sidemark_first . $sidemark_first . $sidemark_first . $sidemark_first;
            } else {
                $sidemark_first = "XXXX";
            }

            if ($sidemark_second_length >= 4) {
                $sidemark_second = mb_substr($sidemark_second, 0, 4, "utf-8");
            } else if ($sidemark_second_length == 3) {
                $sidemark_second = $sidemark_second . mb_substr($sidemark_second, 2, 1, "utf-8");
            } else if ($sidemark_second_length == 2) {
                $sidemark_second = $sidemark_second . $sidemark_second;
            } else if ($sidemark_second_length == 1) {
                $sidemark_second = $sidemark_second . $sidemark_second . $sidemark_second . $sidemark_second;
            } else {
                $sidemark_second = "XXXX";
            }

            $final_site_mark = $sidemark_first . "-" . $sidemark_second;
        }
        //Get Side Mark : END

        // Get Last Order Id : START
        $last_order_id = $this->db->select($select_field)->from($table_name)->order_by($order_field, 'desc')->get()->row();
        $custom_id = @$last_order_id->$select_field;

        if (empty($custom_id)) {
            $custom_id = "ABCD-EFGH-1000-000";
            // First 4 company name, 2nd and 3rd side mark , last order id.
        } else {
            $custom_id = $custom_id;
        }
        $order_id = explode('-', $custom_id);

        if (isset($order_id[3]) && $order_id[3] != '') {
            $order_id = @$order_id[3] + 1;
            if (strlen($order_id) < 3) {
                $order_id = sprintf('%03d', $order_id);
            }
        } else {
            $order_id = '001';
        }
        // Get Last Order Id : END

        $final_order_id = $comp_name . "-" . $final_site_mark . "-" . $order_id;
        return $final_order_id;

    }
    
    // Param 1 : $customer_id : Customer ID
    // Param 2 : $table_name : Table Name
    // Param 3 : $select_field : Select field from that table
    // Param 4 : $order_field : For get last id from that table

    public function generate_shared_order_id($sender_user_id,$receiver_user_id,$customer_user_id){

        $sender_user_info_data = $this->db->from('user_info')->where('id', $sender_user_id)->get()->row();
        $sender_company_name = $sender_user_info_data->company;

        $receiver_user_info_data = $this->db->from('user_info')->where('id', $receiver_user_id)->get()->row();
        $receiver_company_name = $receiver_user_info_data->company;

         // Get Company name : START
        if ($receiver_company_name != '') {
            $comp_name = str_replace(' ', '', $receiver_company_name);
            $comp_name_length = strlen(utf8_decode($comp_name));
            if ($comp_name_length >= 4) {
                $comp_name = mb_substr($comp_name, 0, 4, "utf-8");
            } else if ($comp_name_length == 3) {
                $comp_name = $comp_name . mb_substr($comp_name, 2, 1, "utf-8");
            } else if ($comp_name_length == 2) {
                $comp_name = $comp_name . $comp_name;
            } else if ($comp_name_length == 1) {
                $comp_name = $comp_name . $comp_name . $comp_name . $comp_name;
            } else {
                $comp_name = "XXXX";
            }
        } else {
            $comp_name = "XXXX";
        }

        // Get Company name : END

        //Get Sender company name : START
        if ($sender_company_name != '') {
            $sender_comp_name = str_replace(' ', '', $sender_company_name);
            $comp_name_length = strlen(utf8_decode($sender_comp_name));
            if ($comp_name_length >= 4) {
                $sender_comp_name = mb_substr($sender_comp_name, 0, 4, "utf-8");
            } else if ($comp_name_length == 3) {
                $sender_comp_name = $sender_comp_name . mb_substr($sender_comp_name, 2, 1, "utf-8");
            } else if ($comp_name_length == 2) {
                $sender_comp_name = $sender_comp_name . $sender_comp_name;
            } else if ($comp_name_length == 1) {
                $sender_comp_name = $sender_comp_name . $sender_comp_name . $sender_comp_name . $sender_comp_name;
            } else {
                $sender_comp_name = "XXXX";
            }
        } else {
            $sender_comp_name = "XXXX";
        }
        //Get Sender company name : END

        //Get Side mark : START
        $binfo = $this->db->where('user_id', $customer_user_id)->get('company_profile')->row();
        $sidemark = $binfo->company_name;
        if ($sidemark != '') {
            $side_mark = str_replace(' ', '', $sidemark);
            $side_mark_length = strlen(utf8_decode($side_mark));
            if ($side_mark_length >= 4) {
                $side_mark = mb_substr($side_mark, 0, 4, "utf-8");
            } else if ($side_mark_length == 3) {
                $side_mark = $side_mark . mb_substr($side_mark, 2, 1, "utf-8");
            } else if ($side_mark_length == 2) {
                $side_mark = $side_mark . $side_mark;
            } else if ($side_mark_length == 1) {
                $side_mark = $side_mark . $side_mark . $side_mark . $side_mark;
            } else {
                $side_mark = "XXXX";
            }
        } else {
            $side_mark = "XXXX";
        }
        //Get Side mark : END

        // Get Last Order Id : START
        $last_order_id = $this->db->select('order_id')->from('b_to_b_level_quatation_tbl')->order_by('id', 'desc')->get()->row();
        $custom_id = @$last_order_id->order_id;

        if (empty($custom_id)) {
            $custom_id = "ABCD-EFGH-1000-000";
            // First 4 company name, 2nd and 3rd side mark , last order id.
        } else {
            $custom_id = $custom_id;
        }
        $order_id = explode('-', $custom_id);

        if (isset($order_id[3]) && $order_id[3] != '') {
            $order_id = @$order_id[3] + 1;
            if (strlen($order_id) < 3) {
                $order_id = sprintf('%03d', $order_id);
            }
        } else {
            $order_id = '001';
        }
        // Get Last Order Id : END

        $final_order_id = $comp_name ."-".$sender_comp_name. "-" . $side_mark . "-" . $order_id;
        return $final_order_id;

    }
    // END For Generate order id : Insys


    public function default_shipping_method($user_id){
        $shipping_methods = ['UPS','Customer Pickup','Delivery in Person'];
        foreach ($shipping_methods as $key => $shipping) {
            $methodData = array(
                'method_name' => $shipping,
                'created_by' => $user_id,
                'updated_by' => $user_id,
                'is_default' => 1,
            );
            $this->db->insert('shipping_method', $methodData);
        }
        return true;
    }
}
