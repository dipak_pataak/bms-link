<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('remove_space')) {

    function remove_space($var = '')
    {
        $string = str_replace(' ', '-', $var);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }
}

if (!function_exists('dd')) {

    function dd($data = '')
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit();
    }
}

function convert_unit($value, $b_user_id, $created_by)
{

    $ci = &get_instance();
    $b_user_company = $ci->db->where('user_id', $b_user_id)->get('company_profile')->row();
    $created_by_company = $ci->db->where('user_id', $created_by)->get('company_profile')->row();
    if (isset($b_user_company->unit) && !empty($b_user_company->unit) && isset($created_by_company->unit) && !empty($created_by_company->unit)) {
        if ($b_user_company->unit == $created_by_company->unit) {
            return $value;
        } else {
            if ($b_user_company->unit == 'inches' && $created_by_company->unit == 'cm') {
                $inch = 0.3937 * $value;
                return number_format($inch, 2);
            } elseif ($b_user_company->unit == 'cm' && $created_by_company->unit == 'inches') {
                $cm = 2.54 * $value;
                return number_format($cm, 2);
            }
        }
    }
    return $value;
}

function get_currency_code($symbol)
{
    if ($symbol == '$')
        return 'USD';
    elseif ($symbol == 'AU$')
        return 'AUD';
    elseif ($symbol == 'ƒ')
        return 'AWD';
    elseif ($symbol == '¥')
        return 'BRL';
    elseif ($symbol == '₡')
        return 'CRC';
    elseif ($symbol == 'kn')
        return 'HRK';
    elseif ($symbol == '£')
        return 'EGP';
    elseif ($symbol == '€')
        return 'EUR';
    elseif ($symbol == 'Rs')
        return 'INR';
    elseif ($symbol == 'R')
        return 'ZAR';
    elseif ($symbol == '₩')
        return 'KRW';
    elseif ($symbol == '৳')
        return 'BDT';
    elseif ($symbol == '₨')
        return 'PKR';
    elseif ($symbol == '₣')
        return 'CHF';
    elseif ($symbol == 'ر.س')
        return 'SAR';
    elseif ($symbol == 'د.إ')
        return 'AED';
    else
        return '';
}

function convert_price($b_user_id, $created_by, $convert_status = 'CONVERT')
{

    $ci = &get_instance();
    $login_user = $ci->session->userdata('user_id');
    if ($b_user_id == $login_user && $convert_status == 'CONVERT') {

        return 1;
    }
    $b_user_company = $ci->db->where('user_id', $b_user_id)->get('company_profile')->row();
    $created_by_company = $ci->db->where('user_id', $created_by)->get('company_profile')->row();
    if (isset($b_user_company->currency) && !empty($b_user_company->currency) && isset($created_by_company->currency) && !empty($created_by_company->currency)) {

        if ($b_user_company->currency == $created_by_company->currency) {
            return 1;
        } else {
            $b_user_currency = get_currency_code($b_user_company->currency);
            $created_by_currency = get_currency_code($created_by_company->currency);
            if ($convert_status == 'CONVERT_BACK') {
                $b_user_currency = get_currency_code($created_by_company->currency);
                $created_by_currency = get_currency_code($b_user_company->currency);
            }
            $curl = curl_init();
            $price_url = "https://api.exchangeratesapi.io/api/latest?symbols=" . $b_user_currency . "," . $created_by_currency . "&base=" . $b_user_currency;

            curl_setopt_array($curl, array(
                CURLOPT_URL => $price_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Postman-Token: 153c5162-161f-405d-a26d-eb4f9fce217e",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);

            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return 1;
            } else {
                $result  = json_decode($response);

                if (isset($result->rates)) {
                    $rates = $result->rates;
                    foreach ($rates as $key => $r) {
                        if ($key != $b_user_currency)
                            return $r;
                    }
                }
            }
        }
    }
    return 1;
}

function get_product_row_col_price_for_mapping($height = NULL, $width = NULL, $product_id = NULL, $pattern_id = NULL)
{
    $ci = &get_instance();
    $q = "";
    $st = "";
    $row = "";
    $col = "";
    $prince = "";

    if (!empty($height) && !empty($width)) {

        $p = $ci->db->where('product_id', $product_id)->get('product_tbl')->row();


        if (!empty($p->price_style_type) && $p->price_style_type == 1) {

            $prince = $ci->db->where('style_id', $p->price_rowcol_style_id)
                ->where('row', $width)
                ->where('col', $height)
                ->get('price_style')->row();

            $pc = ($prince != NULL ? $prince->price : 0);

            if (!empty($prince)) {

                $q = $pc;
                $st = 1;

                $row = $prince->row;
                $col = $prince->col;
                $prince = $pc;
            } else {

                $prince = $ci->db->where('style_id', $p->price_rowcol_style_id)
                    ->where('row >=', $width)
                    ->where('col >=', $height)
                    ->order_by('row_id', 'asc')
                    ->limit(1)
                    ->get('price_style')->row();


                $pc = ($prince != NULL ? $prince->price : 0);

                $q = $pc;

                $row = $prince->row;
                $col = $prince->col;
                $prince = $pc;
                $st = 2;
            }
        } elseif (!empty($p->price_style_type) && $p->price_style_type == 2) {

            $prince = $p->sqft_price;
        } elseif (!empty($p->price_style_type) && $p->price_style_type == 3) {

            $prince = $p->fixed_price;
        } elseif (!empty($p->price_style_type) && $p->price_style_type == 4) {

            $pg = $ci->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();
            $price = $ci->db->where('style_id', $pg->group_id)
                ->where('row >=', $width)
                ->where('col >=', $height)
                ->order_by('row_id', 'asc')
                ->limit(1)
                ->get('price_style')->row();


            $pc = ($price != NULL ? $price->price : 0);

            $q = $pc;

            $row = $price->row;
            $col = $price->col;

            $prince = $pc;
            $st = 2;
        } elseif (!empty($p->price_style_type) && $p->price_style_type == 5) {

            $pg = $ci->db->select('*')->from('sqm_price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();
            $pc = ($pg->price != NULL ? $pg->price : 0);


            $total_area = $height * $width;
            $sqm = ($total_area / 10000) * ($pc);
            $prince = $sqm;

            $q = $prince;

            $st = 2;
        }
    } else {
        $q = 0;
        $st = 1;

        $row = $prince->row;
        $col = $prince->col;
        $prince = 0;
    }
    $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
    return $arr;
}

function load_view($file, $arr = [])
{
    $ci = &get_instance();
    $ci->load->view($file, $arr);
}

function SuperAdminAuth()
{
    $ci = &get_instance();
    $sessionData = $ci->session->userdata('user_type');
    if ($sessionData != 's') {
        redirect('super-admin');
    }
}

function get_b_user_type_status()
{
    return [
        ['id' => 0, 'status' => 'Wholesaler'],
        ['id' => 1, 'status' => 'Wholesaler Bridge']
    ];
}

function get_single_b_user_type_status($id)
{
    foreach (get_b_user_type_status() as $value) {
        if ($value['id'] == $id) {
            return $value['status'];
            break;
        }
    }
}

function GetSuperAdmin()
{
    $ci = &get_instance();
    $sessionData = $ci->session->all_userdata();
    return $sessionData;
}

function get_order_stage_list()
{
    return [
        ['order_stage' => 1, 'stage' => 'Quote', 'class' => 'kt-badge kt-badge--primary kt-badge--inline'],
        ['order_stage' => 2, 'stage' => 'Paid', 'class' => 'kt-badge kt-badge--success kt-badge--inline'],
        ['order_stage' => 3, 'stage' => 'Partially Paid', 'class' => 'kt-badge kt-badge--info kt-badge--inline'],
        ['order_stage' => 4, 'stage' => 'Manufacturing', 'class' => 'kt-badge kt-badge--warning kt-badge--inline'],
        ['order_stage' => 5, 'stage' => 'Shipping', 'class' => 'kt-badge kt-badge--warning kt-badge--inline'],
        ['order_stage' => 6, 'stage' => 'Cancelled', 'class' => 'kt-badge kt-badge--danger kt-badge--inline'],
        ['order_stage' => 7, 'stage' => 'Delivered', 'class' => 'kt-badge kt-badge--success kt-badge--inline'],
        ['order_stage' => 8, 'stage' => 'Ready to be Shipped', 'class' => 'kt-badge kt-badge--info kt-badge--inline'],
        ['order_stage' => 9, 'stage' => 'In Transit', 'class' => 'kt-badge kt-badge--primary kt-badge--inline']
    ];
}

function get_single_order_stage($order_stage)
{
    foreach (get_order_stage_list() as $key => $value) {
        if ($value['order_stage'] == $order_stage) {
            return $value;
            break;
        }
    }
}


function check_package_key_access($package_key = '')
{
    $ci = &get_instance();
    $user_id = $ci->session->userdata('user_id');
    $user_detail = $ci->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
    $package_id = $user_detail->package_id;

    switch ($package_id) {
        case 1:
            $column = 'basic';
            break;
        case 2:
            $column = 'advanced';
            break;
        case 3:
            $column = 'pro';
            break;
    }

    $package_option_detail = $ci->db->select($column)->from('hana_package_option')->where('package_key', $package_key)->get()->row();

    if ($package_option_detail->$column == 1) {
        return true;
    } else {
        return false;
    }
}

function package_access($menu_id)
{
    if ($menu_id == 1)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 5)
        return check_package_key_access('setting_advanced');

    // else if ($menu_id == 97)
    //     return check_package_key_access('setting_basic');

    else if ($menu_id == 6)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 7)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 8)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 9)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 28)
        return check_package_key_access('setting_basic');

    else if ($menu_id == 29)
        return check_package_key_access('setting_basic');

    else if ($menu_id == 30)
        return check_package_key_access('setting_basic');

    else if ($menu_id == 31)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 32)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 34)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 39)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 41)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 47)
        return check_package_key_access('customer');  //header

    else if ($menu_id == 49)
        return check_package_key_access('appointment');

    else if ($menu_id == 51)
        return check_package_key_access('setting_basic'); //header

    else if ($menu_id == 53)
        return check_package_key_access('setting_advanced'); //header

    else if ($menu_id == 54)
        return check_package_key_access('setting_basic'); //header

    else if ($menu_id == 55)
        return check_package_key_access('setting_basic'); //header

    else if ($menu_id == 56)
        return check_package_key_access('setting_advanced'); //header    
   
    else if ($menu_id == 60)
        return check_package_key_access('setting_basic');
    
    else if ($menu_id == 61)
        return check_package_key_access('setting_basic');

    else if ($menu_id == 66)
        return check_package_key_access('sales_commission'); //parent

    else if ($menu_id == 67)
        return check_package_key_access('setting_basic'); //parent

    else if ($menu_id == 68)
        return check_package_key_access('setting_advanced'); //parent

    else if ($menu_id == 69)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 69)
        return check_package_key_access('bulk_sms');

    else if ($menu_id == 70)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 70)
        return check_package_key_access('bulk_email');

    else if ($menu_id == 71)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 73)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 74)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 75)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 76)
        return check_package_key_access('catalog');

    else if ($menu_id == 84)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 101)
        return check_package_key_access('setting_pro');

    else if ($menu_id == 87)
        return check_package_key_access('production');

    else if ($menu_id == 103)
        return check_package_key_access('setting_advanced');

    else if ($menu_id == 104)
        return check_package_key_access('setting_advanced');

    return true;
}

// Get favorites information for b level
// PARAM: $page_url - fetch page current url
function get_b_favorite_detail($page_url){
    $CI =& get_instance();
    $user_id = $CI->session->userdata('user_id');
    $favorites = $CI->db->select('*')->from('b_favorites')->where('user_id', $user_id)->where('page_url', $page_url)->get()->row_array();
    if(!empty($favorites)){
        return $favorites;
    } else {
        return false;
    }
}

// Get favorites information for c level
// PARAM: $page_url - fetch page current url
function get_c_favorite_detail($page_url){
    $CI =& get_instance();
    $user_id = $CI->session->userdata('user_id');
    $favorites = $CI->db->select('*')->from('c_favorites')->where('user_id', $user_id)->where('page_url', $page_url)->get()->row_array();
    if(!empty($favorites)){
        return $favorites;
    } else {
        return false;
    }
}
 function getCreditCardType($str, $format = 'string')
    {
        if (empty($str)) {
            return 'unknown';
        }
        $matchingPatterns = [
            'jcb' => '/^(?:2131|1800|35)[0-9]{0,}$/',
            'amex' => '/^3[47][0-9]{0,}$/',
            'diners' => '/^3(?:0[0-59]{1}|[689])[0-9]{0,}$/',
            'visa' => '/^4[0-9]{0,}$/',
            'mastercard' => '/^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/',
            'discover' => '/^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}$/',
            'maestro' => '/^(5[06789]|6)[0-9]{0,}$/'
        ];
        
        foreach ($matchingPatterns as $key=>$pattern) {
            if (preg_match($pattern, $str)) {
                return $key;
            }
        }
        return 'unknown';
    }

    function access_role_permission($menu_id=0)
    {
        $CI =& get_instance();
         $user_id = $CI->session->userdata('user_id');
        $user_role_info = $CI->db->select('*')->from('user_access_tbl')->where('user_id',$user_id)->get()->result();
          
        $id=$user_role_info[0]->role_id;
        $user_data = $CI->db->select('*')
                                            ->from('role_permission_tbl')
                                            ->where('menu_id =',$menu_id)
                                            ->where('role_id', $id)
                                            ->get()->result();

         return $user_data;                         
       
    }

    function employ_role_access(){

        $CI =& get_instance();
        $user_id = $CI->session->userdata('user_id');
        $log_data = $CI->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->result();
        return $log_data;
    }
     function access_role_retailer(){
        $CI =& get_instance();
        $user_id = $CI->session->userdata('user_id');
        $log_datas = $CI->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->result();
        $log_data=$log_datas[0]->is_admin;
        return $log_data ;
    }


    function access_role_permission_page($menu_id=0)
    {
        $CI =& get_instance();
         $user_id = $CI->session->userdata('user_id');
        $user_role_info = $CI->db->select('*')->from('user_access_tbl')->where('user_id',$user_id)->get()->result();
          
        $id=$user_role_info[0]->role_id;
        $user_data = $CI->db->select('*')
        ->from('role_permission_tbl')
        ->where('menu_id =',$menu_id)
        ->where('role_id', $id)
        ->get()->result();
   
        if($user_data[0]->can_access==1){
            return 1 ;
        }elseif($user_data[0]->can_create==1) {
            return 1 ;
        }elseif($user_data[0]->can_edit==1) {
            return 1 ;
        }elseif($user_data[0]->can_delete==1) {
            return 1 ;
        }else{
            return 0 ;
        }                                      
       
    }

    function b_access_role_permission($menu_id=0)
    {
        $CI =& get_instance();
         $user_id = $CI->session->userdata('user_id');
        $user_role_info = $CI->db->select('*')->from('b_user_access_tbl')->where('user_id',$user_id)->get()->result();
          
        $id=$user_role_info[0]->role_id;
        $user_data['access_permission'] = $CI->db->select('*')
                                            ->from('b_role_permission_tbl')
                                            ->where('menu_id =',$menu_id)
                                            ->where('role_id', $id)
                                            ->get()->result();
        $user_data['is_admin'] = $CI->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->result();                            
        return $user_data;                         
       
    }
    function b_access_role_permission_page($menu_id=0)
    {
        $CI =& get_instance();
        $user_id = $CI->session->userdata('user_id');
        $user_role_info = $CI->db->select('*')->from('b_user_access_tbl')->where('user_id',$user_id)->get()->result();
          
        $id=$user_role_info[0]->role_id;
        $user_data['access_permission'] = $CI->db->select('*')
                                            ->from('b_role_permission_tbl')
                                            ->where('menu_id =',$menu_id)
                                            ->where('role_id', $id)
                                            ->get()->result();
        $user_data['is_admin'] = $CI->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->result();
   
        if($user_data['access_permission'][0]->can_access==1 || $user_data['is_admin'][0]->is_admin==1){
            return 1 ;
        }elseif($user_data['access_permission'][0]->can_create==1 || $user_data['is_admin'][0]->is_admin==1) {
            return 1 ;
        }elseif($user_data['access_permission'][0]->can_edit==1 || $user_data['is_admin'][0]->is_admin==1) {
            return 1 ;
        }elseif($user_data['access_permission'][0]->can_delete==1 || $user_data['is_admin'][0]->is_admin==1) {
            return 1 ;
        }else{
            return 0 ;
        }                                      
       
    }

    function check_coupon_date_validation($coupon)
    {
        $CI =& get_instance();
        $coupon_info = $CI->db->where('coupon',$coupon)->get('coupon')->row();

        $start_date = date("d-m-Y", strtotime($coupon_info->start_date));
        $exp_date = date("d-m-Y", strtotime($coupon_info->exp_date));

        $today_date = date("d-m-Y");

        $convert_start_date =  strtotime($start_date);
        $convert_exp_date = strtotime($exp_date);
        $convert_today_date = strtotime($today_date);

        $con1 = false;
        $con2 = false;
        if ($convert_today_date > $convert_start_date) 
        {
            $con1 = true;
        }
        if ($convert_today_date < $convert_exp_date) 
        {
            $con2 = true;
        }

        
        if($con1==true && $con2==true) 
        {
            return true;
        }else{
            return false;
        }

    }



    function check_admin_auth()
    {
        $CI =& get_instance();
        $is_super_admin_ligin = $CI->session->userdata('user_type');

        if($is_super_admin_ligin=='s')
        {
            return true;
        }else{
            return false;
        }
    }

    function generate_token_for_user_login($user_id)
    {
        $CI =& get_instance();
        $token = rand(11111,99999).$user_id.time();
        $data = array('login_token'=>$token);
        $is_update = $CI->db->where('id', $user_id)->update('user_info', $data);

        if ($is_update) 
        {
            return $token;
        }
    }


    function update_customer_employee()
    {
        $CI =& get_instance();
        $CI->db->select('t1.*');
        $CI->db->from('user_info AS t1');
        $CI->db->join('user_info AS t2','t1.created_by = t2.id');
        $CI->db->where('t1.user_type','c');
        $CI->db->where('t2.user_type','b');
        $query = $CI->db->get();
        $data = $query->result_array();

        foreach($data as $key => $value) 
        {
          $is_update = $CI->db->where('created_by', $value['id'])->update('user_info',array('package_id'=> $value['package_id']));
  
        }
        return true;
    }
    

      function update_customer_employee_C()
    {
        $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('user_info');
        $CI->db->where('user_type','c');
        $query = $CI->db->get();
        $data = $query->result_array(); 
        foreach($data as $key => $value) 
        {    
          $is_update = $CI->db->where('created_by', $value['id'])->update('user_info',array('package_id'=> $value['package_id']));
          //echo $CI->db->last_query();echo "</br>";
        }
        return true;
    }
      function update_customer_employee_B()
    {
        $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('user_info');
        $CI->db->where('user_type','b');
        $query = $CI->db->get();
        $data = $query->result_array(); 
        foreach($data as $key => $value) 
        {    
          $is_update = $CI->db->where('created_by', $value['id'])->update('user_info',array('package_id'=> $value['package_id']));
          //echo $CI->db->last_query();echo "</br>";
        }
        return true;
    }
