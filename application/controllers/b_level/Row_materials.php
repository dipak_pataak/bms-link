<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Row_materials extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
           $packageid = $this->session->userdata('packageid');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');
        $packageid = $this->session->userdata('packageid');

        $this->load->model(array(
            'b_level/pattern_model', 'b_level/RowMaterial_model', 'b_level/Settings', 'b_level/purchase_model'
        ));
    }

    public function index() {
        
    }

    public function add_row_material() {

        $this->permission->check_label(83)->create()->redirect();

        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->get('color_tbl')->result();
        $data['get_uom_list'] = $this->Settings->get_uom_list();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/row_material/add_row_material', $data);
        $this->load->view('b_level/footer');
    }

    public function edit_row_material($id) {
        
        $this->permission->check_label(64)->create()->redirect();

        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['get_uom_list'] = $this->Settings->get_uom_list();
        $data['material_id_wise_color'] = $this->db->select('color_id')->from('raw_material_color_mapping_tbl a')->where('a.raw_material_id', $id)
                        ->get()->result();

        $data['mtt'] = $this->db->select('row_material_tbl.*,color_tbl.color_name,pattern_model_tbl.pattern_name')
                        ->join('color_tbl', 'color_tbl.id=row_material_tbl.color_id', 'left')
                        ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=row_material_tbl.pattern_model_id', 'left')
                        ->where('row_material_tbl.id', $id)
                        ->where('row_material_tbl.created_by', $this->level_id)
                        ->get('row_material_tbl')->row();
//        dd($data['mtt']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/row_material/edit_row_material', $data);
        $this->load->view('b_level/footer');
    }

//    public function sendmail() {
//        $email = 'shahabuddinp91@gmail.com';
//        $config = Array(
//            'protocol' => 'smtp',
//            'smtp_host' => 'ssl://smtp.gmail.com',
//            'smtp_port' => 465,
//            'smtp_user' => 'mdshaifullah@gmail.com', // change it to yours
//            'smtp_pass' => 'Shaiful@#19961', // change it to yours
//            'mailtype' => 'html',
//            'charset' => 'iso-8859-1',
//            'wordwrap' => TRUE
//        );
//        $data['test'] = "Pabnar Siafullah";
//        $mesg = $this->load->view('b_level/testmail', $data, TRUE);
//        $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
//        $this->email->set_header('Content-type', 'text/html');
//
//        $this->load->library('email', $config);
//        $this->email->initialize($config);
//        $this->email->set_newline("\r\n");
//        $this->email->from('khs2010welfare@gmail.com', "Support Center");
//        $this->email->to($email);
//        $this->email->subject("Welcome to BMSLINK");
//// $this->email->message("Dear $name ,\nYour order submitted successfully!"."\n\n"
//// . "\n\nThanks\nMetallica Gifts");
//// $this->email->message($mesg. "\n\n http://metallicagifts.com/mcg/verify/" . $verificationText . "\n" . "\n\nThanks\nMetallica Gifts");
//        $this->email->message($mesg);
//        $this->email->send();
//    }

    public function update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "raw material information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $id = $this->input->post('id');
        $color_id = $this->input->post('color_id');
        $FormData = array(
            'material_name' => $this->input->post('material_name'),
            'pattern_model_id' => $this->input->post('pattern_model_id'),
            'uom' => $this->input->post('uom'),
            'measurement_type' => $this->input->post('measurement_type'),
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d')
        );

        $this->db->where('id', $id)->update('row_material_tbl', $FormData);
//        =========== its for mapping raw material color delete ===========
        $this->db->where('raw_material_id', $id)->delete('raw_material_color_mapping_tbl');
        for ($i = 0; $i < count($color_id); $i++) {
            $raw_material_color_mapping_tbl = array(
                'color_id' => $color_id[$i],
                'raw_material_id' => $id,
            );
            $this->db->insert('raw_material_color_mapping_tbl', $raw_material_color_mapping_tbl);
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Row material update successfull </div>");
        redirect('row-material-list');
    }

    public function save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "raw material information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $color_id = $this->input->post('color_id');
//        echo '<pre>';        print_r($color_id); die();
        $FormData = array(
            'material_name' => $this->input->post('material_name'),
            'pattern_model_id' => $this->input->post('pattern_model_id'),
            'uom' => $this->input->post('uom'),
            'measurement_type' => $this->input->post('measurement_type'),
//            'color_id'          =>  $this->input->post('color_id'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d')
        );
        $this->db->insert('row_material_tbl', $FormData);
        $raw_materials_id = $this->db->insert_id();
        for ($i = 0; $i < count($color_id); $i++) {
            $raw_material_color_mapping_tbl = array(
                'color_id' => $color_id[$i],
                'raw_material_id' => $raw_materials_id,
            );
            $this->db->insert('raw_material_color_mapping_tbl', $raw_material_color_mapping_tbl);
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Row material save successfull </div>");
        redirect('row-material-list');
    }

    public function row_material_list() {

         $permission_accsess=b_access_role_permission_page(64);
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['get_uom_list'] = $this->Settings->get_uom_list();
        $data['row_materials'] = $this->db->select('a.*, b.uom_name, c.pattern_name')
                        ->from('row_material_tbl a')
                        ->join('unit_of_measurement b', 'b.uom_id=a.uom', 'left')
                        ->join('pattern_model_tbl c', 'c.pattern_model_id=a.pattern_model_id', 'left')
                        ->where('a.created_by',$this->level_id)
                        ->order_by('id', 'desc')
                        ->get()->result();


        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/row_material/row_material_list', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }



    public function delete_row_material($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "raw material information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('id', $id)->delete('row_material_tbl');

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Row material delete successfull </div>");
        redirect('row-material-list');
    }



//    ======= its for raw_material_csv_upload ==============
    public function raw_material_csv_upload() {
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['material_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['pattern_model_id'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['color_id'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                    $insert_csv['uom'] = (!empty($csv_line[3]) ? $csv_line[3] : null);
                    $insert_csv['measurement_type'] = (!empty($csv_line[4]) ? $csv_line[4] : null);
                }

                if ($count > 0) {
                    $colors_arr = explode(",", $insert_csv['color_id']);
                    $get_pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('created_by', $this->user_id)->where('pattern_name', $insert_csv['pattern_model_id'])->get()->result();
                    if(count($get_pattern_info) > 0){
                        $get_uom_info = $this->db->select('*')->from('unit_of_measurement')->where('uom_name', $insert_csv['uom'])->where('user_id', $this->user_id)->get()->result();
                        
                        if(count($get_uom_info) > 0){
                            $data = array(
                                'material_name' => $insert_csv['material_name'],
                                'pattern_model_id' => @$get_pattern_info[0]->pattern_model_id,
                                // 'color_id' => $get_color_info[0]->id,
                                'uom' => $get_uom_info[0]->uom_id,
                                'measurement_type' => $insert_csv['measurement_type'],
                                'created_by' => $this->user_id,
                                'created_date' => date('Y-m-d'),
                            );

                            $result = $this->db->select('*')
                                ->from('row_material_tbl')
                                ->where('material_name', $data['material_name'])
                                ->where('created_by', $this->user_id)
                                ->get()
                                ->num_rows();
                            if ($result == 0 && !empty($data['material_name'])) {
                                $this->db->insert('row_material_tbl', $data);
                                $raw_material_lastid = $this->db->insert_id();
                                //   ========== its for color entry with raw material info save ========                      
                                for ($i = 0, $j = 0; $i < count($colors_arr); $i++) {
                                    $get_color_info = $this->db->select('*')->from('color_tbl')->where('created_by', $this->user_id)->where('color_name', $colors_arr[$j])->where('pattern_id',$get_pattern_info[0]->pattern_model_id)->get()->result();
                                    if ($get_color_info) {
                                        $raw_material_color_mapping_tbl = array(
                                            'color_id' => $get_color_info[0]->id, //$colors_arr[$i],
                                            'raw_material_id' => $raw_material_lastid,
                                        );
                                        $this->db->insert('raw_material_color_mapping_tbl', $raw_material_color_mapping_tbl);
                                        $j++;
                                    }
                                }
                            } else {
                                $check_material_name = $this->db->select('*')->from('row_material_tbl')
                                                ->where('material_name', $data['material_name'])->where('created_by', $this->user_id)->get()->result();
                            
                                $get_pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('pattern_name', $insert_csv['pattern_model_id'])->where('created_by', $this->user_id)->get()->result();

                                // Delete raw_material_color_mapping_tbl data : START
                                $check_material_ids_colors = $this->db->select('*')->from('raw_material_color_mapping_tbl')
                                            ->where('raw_material_id', $check_material_name[0]->id)->get()->result();
                                if ($check_material_ids_colors) {
                                    foreach ($check_material_ids_colors as $ids_color) {
                                        $this->db->where('raw_material_id', $ids_color->raw_material_id)->delete('raw_material_color_mapping_tbl');
                                    }
                                }
                                // Delete raw_material_color_mapping_tbl data : END

                                if(count($get_pattern_info) > 0){
                                    //  ========== its for color entry with raw material info save ========                      
                                    for ($i = 0, $j = 0; $i < count($colors_arr); $i++) {
                                        $get_color_info = $this->db->select('*')->from('color_tbl')->where('color_name', $colors_arr[$j])->where('created_by', $this->user_id)->where('pattern_id',$get_pattern_info[0]->pattern_model_id)->get()->result();
                                        if ($get_color_info) {
                                            $raw_material_color_mapping_tbl = array(
                                                'color_id' => $get_color_info[0]->id, //$colors_arr[$i],
                                                'raw_material_id' => $check_material_name[0]->id,
                                            );
                                            $this->db->insert('raw_material_color_mapping_tbl', $raw_material_color_mapping_tbl);
                                            $j++;
                                        } 
                                    }
                                }    
                                
                                $get_uom_info = $this->db->select('*')->from('unit_of_measurement')->where('uom_name', $insert_csv['uom'])->where('user_id', $this->user_id)->get()->result();
                                if (!empty($get_pattern_info) || !empty($get_uom_info)) {
                                    $data = array(
                                        'material_name' => $insert_csv['material_name'],
                                        'pattern_model_id' => @$get_pattern_info[0]->pattern_model_id,
                                        // 'color_id' => $get_color_info[0]->id,
                                        'uom' => @$get_uom_info[0]->uom_id,
                                        'measurement_type' => $insert_csv['measurement_type'],
                                        'updated_by' => $this->user_id,
                                        'updated_date' => date('Y-m-d'),
                                    );
                                    $this->db->where('id', $check_material_name[0]->id);
                                    $this->db->update('row_material_tbl', $data);
                                } 
                            }
                        }    
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Raw materials bulk uploaded successfully!</div>");
        redirect('row-material-list');
    }

//    ======== its for raw material onkey search =============
    public function b_level_rawmaterial_search() {
        $keyword = $this->input->post('keyword');
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->get('color_tbl')->result();
        $data['get_uom_list'] = $this->Settings->get_uom_list();
        $data['row_materials'] = $this->RowMaterial_model->get_rawmaterial_search_result($keyword);
        $this->load->view('b_level/row_material/raw_material_search', $data);
    }

//    ========= its for raw_material_usage ============
    public function raw_material_usage() {
        $permission_accsess=b_access_role_permission_page(114);
        $data['rmtts'] = $this->purchase_model->get_row_material_list();
        $data['colors'] = $this->db->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['get_b_level_quatation'] = $this->db->select('*')->from('b_level_quatation_tbl')->where('created_by',$this->level_id)->where('order_stage', 4)->get()->result();
        $data['get_quatation'] = $this->db->select('*')->from('quatation_tbl')
            ->where('created_by',$this->level_id)
            ->where('order_stage', 4)->get()->result();
         if ($permission_accsess==1) {
        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/row_material/raw_material_usage');
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

//    ============ its for quantity_check ===========
    public function quantity_check() {
        $material_id = $this->input->post('material_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $quantity = $this->input->post('quantity');
        $get_quantity_check = $this->RowMaterial_model->get_quantity_check($material_id, $color_id, $pattern_model_id);
//        print_r($get_quantity_check);
        if ($quantity > $get_quantity_check->total_in_qty) {
            echo "Quantity is greater than In Quantity";
        }
        if ($get_quantity_check->total_out_qty > $get_quantity_check->total_in_qty) {
            echo "Quantity is not available";
        }
    }

//    =============== its for raw_material_usage_save ===============
    public function raw_material_usage_save() {
	//echo '<pre>';print_r($_POST);exit;
       $item_name_order = $this->input->post('item_name_order');
		$rmtt_id = $this->input->post('rmtt_id');
		$color_id = $this->input->post('color_id');
		$pattern_model_id = $this->input->post('pattern_model_id');
		$quantity = $this->input->post('quantity');
		 $order_id = $this->input->post('order_id');
        $date = $this->input->post('date');
	   foreach($item_name_order as $key=>$val)
	   {
	   		foreach($val as $key1=>$val1)
			{
				//print_r($val1);exit;
				foreach($rmtt_id[$key] as $k=>$v){
				$row_id						=  $val1;
				$rmtt_id1					 =	$rmtt_id[$key][$k];
				$color_id1 					=	$color_id[$key][$k];
				$pattern_model_id1 			=$pattern_model_id[$key][$k];
				$quantity1 					=$quantity[$key][$k];
				
				$raw_materials_usage = array(
				'row_material_id' => $rmtt_id1,
				'row_id' => $row_id,
				'color_id' => $color_id1,
				'pattern_model_id' => $pattern_model_id1,
				'out_qty' => $quantity1,
				'from_module' => $order_id,
				'stock_date' => $date,
				);
				//print_r($raw_materials_usage);exit;
				$this->db->insert('row_material_stock_tbl', $raw_materials_usage);
				}
				//print_r($$raw_materials_usage);exit;
			}
			
	   }
	 /* print_r($raw_materials_usage);exit;
	    $order_id = $this->input->post('order_id');
        $date = $this->input->post('date');
        $rmtt_id = $this->input->post('rmtt_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $quantity = $this->input->post('quantity');

        for ($i = 0; $i < count($rmtt_id); $i++) {
            $raw_materials_usage = array(
                'row_material_id' => $rmtt_id[$i],
                'color_id' => $color_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'out_qty' => $quantity[$i],
                'from_module' => $order_id,
                'stock_date' => $date,
            );
//            echo '<pre>';            print_r($raw_materials_usage); 
            $this->db->insert('row_material_stock_tbl', $raw_materials_usage);
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "raw material usages information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);*/
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Raw materials usages information save successfully!</div>");
        redirect('raw-material-usage');
    }
/** Start added by insys */
//=========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('row_material_tbl','id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Raw material has been Deleted successfully.</div>");
        }
        redirect("row-material-list");
    } 
/** End added by insys */

public function get_item_details(){
		$order_id=$this->input->post("item_order_id");
		$get_order_itemwise = $this->db->query("SELECT * FROM b_level_qutation_details WHERE order_id='".$order_id."'")->result();
		$html='<option value="">Sleect Item</option>';
		if(!empty($get_order_itemwise))
		{
			foreach($get_order_itemwise as $row)
			{
				$get_product_name= $this->db->query("SELECT * FROM product_tbl WHERE product_id='".$row->product_id."'")->row();
				$html.='<option value="'.$row->row_id.'">'.$get_product_name->product_name.'</option>';
			}
		}
		echo $html;exit;
	}
	
	public function get_item_dimension(){
		$item_raw_id=$this->input->post("item_raw_id");
		$get_order_itemwise = $this->db->query("SELECT * FROM b_level_qutation_details WHERE row_id=".$item_raw_id)->row();
		if(!empty($get_order_itemwise))
		{
			$get_width=$get_order_itemwise->width;
			$get_height=$get_order_itemwise->height;
			$get_total=$get_order_itemwise->width*$get_order_itemwise->height;
			
		}
			$result=array('get_width'=>$get_width,'get_height'=>$get_height,'get_total'=>$get_total);
			echo json_encode($result);exit;
	}

    public function export_raw_material(){

        $raw_material_data = $this->db->select('a.*, b.uom_name, c.pattern_name')
                        ->from('row_material_tbl a')
                        ->join('unit_of_measurement b', 'b.uom_id=a.uom', 'left')
                        ->join('pattern_model_tbl c', 'c.pattern_model_id=a.pattern_model_id', 'left')
                        ->where('a.created_by',$this->level_id)
                        ->order_by('id', 'desc')
                        ->get()->result();

        if(count($raw_material_data) > 0) {
            
            // file name 
            $filename = 'export_raw_material.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            
            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('material_name', 'pattern_model_id', 'color_id', 'uom', 'measurement_type (Unit(1) & Qty(2))');
            fputcsv($file, $header);
            foreach ($raw_material_data as $record) {
                
                // Get Color material wise : START
                $results = $this->db->select('a.id, a.color_name')
                        ->from('color_tbl a')
                        ->join('raw_material_color_mapping_tbl b', 'b.color_id = a.id', 'left')
                        ->where('b.raw_material_id',$record->id)
                        ->where('a.created_by',$this->level_id)
                        ->order_by('id', 'asc')
                        ->get()->result();
                // Get Color material wise : END
                $colors = '';
                foreach ($results as $result) {
                    $colors .= $result->color_name.",";
                }   

                if($colors != ''){
                    $colors = rtrim($colors,",");
                }
                
                $data = array($record->material_name,$record->pattern_name,$colors,$record->uom_name,$record->measurement_type);
                fputcsv($file, $data);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No Data Found</div>");
            redirect('row-material-list');
        }
    }
	
}
