<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pattern_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
          $package=$this->session->userdata('packageid');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Pattern_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    ===============its for add_pattern ===============
    public function add_pattern() {

       // $this->permission->check_label('manage_pattern')->create()->redirect();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_add');
        $this->load->view('b_level/footer');
    }

//=============== its for pattern_save ==============
    public function pattern_save() {
        $createdate = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $pattern_name = $this->input->post('pattern_name');
        $pattern_type = $this->input->post('pattern_type');
        $status = $this->input->post('status');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Pattern information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $pattern_data = array(
            'category_id' => $category_id,
            'pattern_type' => $pattern_type,
            'pattern_name' => $pattern_name,
            'status' => $status,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
            'status' => 1
        );
//        echo '<pre>';        print_r($pattern_data); die();
        $this->db->insert('pattern_model_tbl', $pattern_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern save successfully!</div>");
        redirect("manage-pattern");
    }

//    ===============its for manage_pattern ===============
    public function manage_pattern() {


      //  $this->permission->check_label('pattern')->create()->redirect();
        $permission_accsess=b_access_role_permission_page(16);
        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')
            ->result();
        if ($permission_accsess==1) {
            $this->load->view('b_level/header', $data);
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/pattern/pattern_manage', $data);
            $this->load->view('b_level/footer');
        }else{
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar'); 
            $this->load->view('b_level/upgrade_error'); 
            $this->load->view('b_level/footer');
        }
    }

    function getPatternLists(){
        $data = $row = array();
        // Fetch member's records
        $patterns = $this->Pattern_model->getRows($_POST);
        /* echo "Tets<pre>";
        print_r($patterns);die; */
        foreach($patterns as $record){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$record->pattern_model_id.'" class="checkbox_list">';
            
            $record->category_name = ($record->pattern_category_id == 0) ? 'None' : $record->category_name;
            $record->pattern_status = ($record->pattern_status == 1) ? 'Active' : 'Inactive';

            $sql = "SELECT product_name FROM product_tbl WHERE pattern_models_ids = $record->pattern_model_id";
            $results = $this->db->query($sql)->result();
            $product_name ='';
            if ($results) {
                $product_name .="<ul>";
                $k = 0;
                foreach ($results as $result) {
                    $k++;
                    $product_name .="<li>" . $k . ") " . $result->product_name . "</li>";
                }
                $product_name .="</ul>";
            } else {
                $product_name .='<p>The products will be assigned at the later stage</p>';
            }
            $record->assigned_product = $product_name;
            $package=$this->session->userdata('packageid');
            if (!empty($package)) {
            $menu_permission= b_access_role_permission(16);
            if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1)
            {
                $action = '<a href="javascript:void(0)" class="btn btn-warning default btn-sm" onclick="pattern_edit_form('.$record->pattern_model_id.')" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';
            }
            if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1)
            {
                $action .='  <a href="'.base_url().'pattern-delete/'.$record->pattern_model_id.'" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm(\'Do you want to delete it?\')"><i class="fa fa-trash"></i></a>';
            }
        }

            $data[] = array($chk, $i, $record->pattern_name, $record->category_name, $record->assigned_product, $record->pattern_status, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Pattern_model->countAll(),
            "recordsFiltered" => $this->Pattern_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

//    ===============its for pattern_edit ===============
    public function pattern_edit($id) {
        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('created_by',$this->level_id)
            ->where('parent_category', 0)
                        ->where('status', 1)->get('category_tbl')->result();
        $data['pattern_edit'] = $this->Pattern_model->pattern_edit($id);
        $data['assigned_pattern_product'] = $this->Pattern_model->assigned_pattern_product($id);

//        $this->load->view('b_level/header', $data);
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_edit', $data);
//        $this->load->view('b_level/footer');
    }

//=============== its for pattern_update ==============
    public function pattern_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "Updated";
        $remarks = "pattern information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $update_date = date('Y-m-d');
        $category_id = $this->input->post('category_id');
        $pattern_name = $this->input->post('pattern_name');
        $pattern_type = $this->input->post('pattern_type');
        $status = $this->input->post('status');

        $pattern_data = array(
            'category_id' => $category_id,
            'pattern_type' => $pattern_type,
            'pattern_name' => $pattern_name,
            'updated_by' => $this->user_id,
            'updated_date' => $update_date,
            'status' => $status,
        );
//        echo '<pre>';        print_r($pattern_data); die();
        $this->db->where('pattern_model_id', $id);
        $this->db->update('pattern_model_tbl', $pattern_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern updated successfully!</div>");
        redirect("manage-pattern");
    }

//    ============== its for pattern_model_filter ============
    public function pattern_model_filter() {
        $data['parent_category'] = $this->db->select('category_id, category_name')->where('parent_category', 0)->where('status', 1)->get('category_tbl')->result();
        $data['pattern_name'] = $this->input->post('pattern_name');
        $data['parent_cat'] = $this->input->post('parent_cat');
        $data['pattern_status'] = $this->input->post('pattern_status');
        $data['pattern_model_filter'] = $this->Pattern_model->pattern_model_filter($data['pattern_name'], $data['parent_cat'], $data['pattern_status']);


        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pattern/pattern_model_filter');
        $this->load->view('b_level/footer');
    }

    public function category_wise_pattern($category_id = null) {
        if ($category_id == 'none') {
            $category_id = ' ';
            $where = "category_id < 0";
        } else {
            $where = "category_id = $category_id";
        }
        $category_wise_pattern = $this->db->select('*')
                        ->where($where)
                        ->where('status', 1)
                        ->where('created_by', $this->level_id)
                        ->order_by('pattern_name', 'asc')
                        ->get('pattern_model_tbl')->result();

        $q = "";
        if (!empty($category_wise_pattern)) {

            $q .= '<select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                                ';
            foreach ($category_wise_pattern as $value) {
                $q .= "<option value='$value->pattern_model_id'>$value->pattern_name</option>";
            }
            $q .= '</select>';
        }
        echo $q;
    }

//    ============ its for pattern_delete =============
    public function pattern_delete($pattern_id) {
        $this->db->select('pattern_models_ids');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($pattern_id, pattern_models_ids)");
        $check_pattern = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_pattern->num_rows() > 0) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Can't delete it. pattern already assigned!</div>");
            redirect("manage-pattern");
        } else {
            $this->db->where('pattern_model_id', $pattern_id)->delete('pattern_model_tbl');
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "deleted";
            $remarks = "pattern information deleted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern deleted successfully!</div>");
            redirect("manage-pattern");
        }
    }

//    =============== b_level_pattern_search ============
    public function b_level_pattern_search() {
        $keyword = $this->input->post('keyword');
        $data['pattern_list'] = $this->Pattern_model->get_pattern_search_result($keyword);
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/pattern/pattern_search', $data);
    }

//    ============= its for b_assinged_product_delete ==============
    public function b_assinged_product_delete() {
        $product_id = $this->input->post('product_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
//        echo $product_id;        echo $pattern_model_id;
        $array1 = Array($pattern_model_id);
        $all_patters = $this->db->select('pattern_models_ids')->from('product_tbl')->where('product_id', $product_id)->get()->row();
        $list = $all_patters->pattern_models_ids;
        $array2 = explode(',', $list);
        $array3 = array_diff($array2, $array1);
        $output = implode(',', $array3);
        $data = array(
            'pattern_models_ids' => $output,
        );
        $this->db->where('product_id', $product_id)->update('product_tbl', $data);
    }
/** Start added by insys */
//    =========== its for manage_action ==============
public function manage_action(){
    if($this->input->post('action')=='action_delete')
    {
        $this->load->model('Common_model');
        $res = $this->Common_model->DeleteSelected('pattern_model_tbl','pattern_model_id');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Pattern has been Deleted successfully.</div>");
    }
    redirect("manage-pattern");
} 
/** End added by insys */


    //=========== its for import_pattern_save ===========
    public function import_pattern_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Pattern csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    //$insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['pattern_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['category_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                }
                $data = array(
                    'pattern_name' => $insert_csv['pattern_name'],
                    'category_name' => $insert_csv['category_name'],
                    'created_by' => $this->user_id,
                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
                    $category_exist = $this->db->select('*')
                                ->from('category_tbl')
                                ->where('category_name', $data['category_name'])
                                ->where('created_by', $this->session->userdata('user_id'))
                                ->get()
                                ->row_array();

                    if(count($category_exist) > 0) {
                        $result = $this->db->select('*')
                        ->from('pattern_model_tbl')
                        ->where('pattern_name', $data['pattern_name'])
                        ->where('category_id', $category_exist['category_id'])
                        ->where('created_by', $this->level_id)
                        ->get()
                        ->num_rows();
                        
                        
                        echo "<br/>".$data['pattern_name']."=".$category_exist['category_id']." =".$result;

                        $data_add = array(
                            'category_id' => $category_exist['category_id'],
                            'pattern_name' => $insert_csv['pattern_name'],
                            'status' => '1',
                            'created_by' => $this->user_id,
                            'created_date' => date('Y-m-d'),
                        );

                        if ($result == 0 && !empty($data['pattern_name'])) {
                            $this->db->insert('pattern_model_tbl', $data_add);
                            // $sql = $this->db->set($data_add)->get_compiled_insert('pattern_model_tbl');

                        }
                        /* else {
                            $data = array(
                                'color_name' => $insert_csv['color_name'],
                                'color_number' => $insert_csv['color_number'],
                                'pattern_id' => $pattern_exist['pattern_model_id'],
                                'updated_by' => $this->user_id,
                                'updated_date' => date('Y-m-d'),
                            );
                            $this->db->where('color_name', $data['color_name']);
                            $this->db->update('color_tbl', $data);
                        } */
                    } else {
                        echo "<br/>".$data['category_name']." = Not exist";
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern imported successfully!</div>");
        redirect('manage-pattern');
    }

    public function export_pattern(){

        // $pattern_data = $this->db->select("a.*, b.*, a.category_id as pattern_category_id, a.status as pattern_status")
        $pattern_data = $this->db->select("a.pattern_name, b.category_name")
                        ->from('pattern_model_tbl a')
                        ->join('category_tbl b', 'b.category_id = a.category_id', 'left')
                        ->where('a.created_by',  $this->level_id)
                        ->order_by('a.pattern_model_id', 'desc')
                        ->get()->result();


        if(count($pattern_data) > 0) {
            // file name 
            $filename = 'export_pattern.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            
            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('pattern_name', 'category_name');
            fputcsv($file, $header);
            foreach ($pattern_data as $record) {
                $data = array($record->pattern_name,$record->category_name);
                fputcsv($file, $data);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No Data Found</div>");
            redirect('manage-pattern');
        }
    }
}
