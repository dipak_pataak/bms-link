<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $packageid=$this->session->userdata('packageid');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/BAccount_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

//    ============ its for add_category ===================
    public function add_category() {


        $this->permission->check_label(11)->create()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')->where('parent_category', 0)->where('created_by',$this->user_id)->where('status', 1)->get('category_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_add', $data);
        $this->load->view('b_level/footer');
    }

//==================== its for save_category ================
    public function save_category() {
        $createdate = date('Y-m-d');
        $category_name = $this->input->post('category_name');
        $parent_category = $this->input->post('parent_category');
        $description = $this->input->post('description');
        $status = $this->input->post('status');
         $fractions1 = $this->input->post('fractions');


        $tst = explode(',', $fractions1);
      //  $tst = explode(',', $fractions1);
        if (count($tst) > 1) {
            foreach ($tst as $val) {
                $fractions[] = trim($val);
            }
            $fractions = implode(',', $fractions);
        } else if (count($tst) == 1) {
            $fractions = $fractions1;
        } else {
            $fractions = '';
        }
        $category_data = array(
            'category_name' => $category_name,
            'parent_category' => $parent_category,
            'description' => $description,
            'status' => $status,
              'fractions' => $fractions,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->insert('category_tbl', $category_data);
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "Category information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category save successfully!</div>");
        redirect("manage-category");
    }

//    ============ its for manage_category ===================
    public function manage_category() {

        $this->permission->check_label(10)->read()->redirect();
        $permission_accsess=b_access_role_permission_page(10);
        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')->result();
        if($permission_accsess==1){
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_manage', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }


    function getCategoryLists(){
        $data = $row = array();
        
        // Fetch member's records
        $category_list = $this->Category_model->getRows($_POST);
        foreach($category_list as $category){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$category->category_id.'" class="checkbox_list">';
              $packageid=$this->session->userdata('packageid');
            if (!empty($packageid)){
            
             $action = '<a href="javascript:void(0)" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="show_category_edit('.$category->category_id.');"><i class="fa fa-pencil"></i></a>
                <a href="'.base_url('wholesaler-category-delete/'.$category->category_id).'" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm(\'Do you want to delete it?\')"><i class="fa fa-trash"></i></a>';
             }

            $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $category->category_id)->get()->result();
            $assigned_products_list = '';
            if ($assigned_products) {
                foreach ($assigned_products as $product) {
                    $assigned_products_list .= "<ul>";
                    $assigned_products_list .= "<li> => " . $product->product_name . "</li>";
                    $assigned_products_list .= "</ul>";
                }
            } else {
                $assigned_products_list .= "The products will be assigned at the later stage";
            }

            $category_status = '';
            if ($category->status == '1') {
                $category_status =  "Active";
            } else {
                $category_status = 'Inactive';
            }
                                    
            $data[] = array($chk,$i,$category->category_name, $category->parent_category_name, $category->description,$category->fractions,$assigned_products_list,$category_status,$action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Category_model->countAll(),
            "recordsFiltered" => $this->Category_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

//    ============ its for category_edit ===================
    public function category_edit($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $created_by)
            ->get('category_tbl')->result();
        $data['category_edit'] = $this->Category_model->category_edit($id);
        $data['category_assigned_product'] = $this->Category_model->category_assigned_product($id);
//        dd($data['category_edit']);
//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_edit', $data);
//        $this->load->view('b_level/footer');
    }

//==================== its for category_update ================
    public function category_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "Category information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updatedate = date('Y-m-d');
        $category_name = $this->input->post('category_name');
        $parent_category = $this->input->post('parent_category');
        $description = $this->input->post('description');
        $fractions1 = $this->input->post('fractions');

        $tst = explode(',', $fractions1);
        if (count($tst) > 1) {
            foreach ($tst as $val) {
                $fractions[] = trim($val);
            }
            $fractions = implode(',', $fractions);
        } else if (count($tst) == 1) {
            $fractions = $fractions1;
        } else {
            $fractions = '';
        }
        $status = $this->input->post('status');
        $category_data = array(
            'category_name' => $category_name,
            'parent_category' => $parent_category,
            'description' => $description,
            'fractions' => $fractions,
            'status' => $status,
            'updated_by' => $this->user_id,
            'updated_date' => $updatedate,
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->where('category_id', $id);
        $this->db->update('category_tbl', $category_data);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category updated successfully!</div>");
        redirect("manage-category");
    }

//    ============ its for category_assign ===================
    public function category_assign()
    {
        if ($this->session->userdata('isAdmin') == 1) {
            $created_by = $this->session->userdata('user_id');
        } else {
            $created_by = $this->session->userdata('admin_created_by');
        }

        $this->permission->check_label(14)->create()->redirect();

        $data['parent_category'] = $this->db->select('category_id, category_name')
            ->where('parent_category', 0)
            ->where('status', 1)
            ->where('created_by', $created_by)
            ->get('category_tbl')->result();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/category/category_assign', $data);
        $this->load->view('b_level/footer');
    }

    public function load_category($cat_id) {

        $cat = $this->db->select('category_id, category_name')
                        ->where('parent_category', 0)->where('category_id', $cat_id)
                        ->get('category_tbl')->row();

        $category_wise_subcategory = $this->db->select('*')
                        ->where('parent_category', $cat_id)
                        ->get('category_tbl')->result();

        $q = "";
        $q .= '<li><a href="#">' . $cat->category_name . '</a><ul>';
        foreach ($category_wise_subcategory as $key => $value) {
            $q .= '<li>' . $value->category_name . '</li>';
        }
        $q .= '</ul></li>';
        echo $q;
    }

//    =============== its for category_filter ============
    public function category_filter() {
        // echo $this->input->post('category_status');die;
        if($this->input->post('Search')) {
            $this->session->set_userdata('search_categoryname',$this->input->post('cat_name'));
            $this->session->set_userdata('search_parent_category',$this->input->post('parent_cat'));
            $this->session->set_userdata('search_status',$this->input->post('category_status'));
        } else {
            $this->session->unset_userdata('search_categoryname');
            $this->session->unset_userdata('search_parent_category');
            $this->session->unset_userdata('search_status');
        }
        redirect('manage-category');
    }

//    ========== its for category delete =============
    public function b_category_delete($category_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "category information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('category_id', $category_id)->get()->result();
        if ($pattern_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some patterns!</div>");
            redirect("manage-category");
        }
        $condition_info = $this->db->select('*')->from('product_conditions_tbl')->where('category_id', $category_id)->get()->result();
        if ($condition_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some conditions!</div>");
            redirect("manage-category");
        }
        $product_cat_info = $this->db->select('*')->from('product_tbl')->where('category_id', $category_id)->get()->result();
        if ($product_cat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some products!</div>");
            redirect("manage-category");
        }
        $product_subcat_info = $this->db->select('*')->from('product_tbl')->where('subcategory_id', $category_id)->get()->result();
        if ($product_subcat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some sub categories!</div>");
            redirect("manage-category");
        }
        $attributes_info = $this->db->select('*')->from('attribute_tbl')->where('category_id', $category_id)->get()->result();
        if ($attributes_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some attributes!</div>");
            redirect("manage-category");
        }
        $this->db->where('category_id', $category_id)->delete('category_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category deleted successfully!</div>");
        redirect("manage-category");
    }

//    =========== its for assinged_category_product_delete ===========
    public function assinged_category_product_delete() {
        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $data = array(
            'category_id' => 0,
        );
        $this->db->where('product_id', $product_id)->update('product_tbl', $data);
    }

    /** Start added by insys */
    //    =========== its for manage_action ==============
    public function manage_action() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('category_tbl', 'category_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("manage-category");
    }

    /** End added by insys */

    public function category_csv_upload() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Category csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    //$insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['category_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['parent_category_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['description'] = (!empty($csv_line[2]) ? $csv_line[2] : null);
                    $insert_csv['fractions'] = (!empty($csv_line[3]) ? $csv_line[3] : '');
                    $insert_csv['status'] = (!empty($csv_line[4]) ? $csv_line[4] : null);
                }

                $data = [];
                if ($count > 0) {
                    
                    // Get parent category id : START
                    $parent_category_id = '';
                    if($insert_csv['parent_category_name'] != ''){
                        $get_parent_category = $this->db->select('*')
                                    ->from('category_tbl')
                                    ->where('category_name', $insert_csv['parent_category_name'])
                                    ->where('created_by', $this->level_id)
                                    ->get()
                                    ->row_array();

                        if(isset($get_parent_category['category_id']) && $get_parent_category['category_id'] != ''){
                            $parent_category_id = $get_parent_category['category_id'];
                        }  
                    }    
                    // Get parent category id : END

                    // Get status : START
                    $cstatus = 0;
                    if($insert_csv['status'] == 'active'){
                        $cstatus = 1;
                    }
                    // Get status : END

                    // check category name exist or not : START
                    $category_exist = $this->db->select('*')
                                ->from('category_tbl')
                                ->where('category_name', $insert_csv['category_name'])
                                ->where('created_by', $this->level_id)
                                ->get()
                                ->row_array();       
                    // check category name exist or not : END   

                    $data = array(
                        'category_name' => $insert_csv['category_name'],
                        'parent_category' => $insert_csv['parent_category_name'],
                        'description' => $insert_csv['description'],
                        'fractions' => $insert_csv['fractions'],
                        'status' => $cstatus,
                    );            


                    if(isset($category_exist['category_id']) && $category_exist['category_id'] != '') {
                        
                        // Update category : START
                        $data['updated_by'] = $this->level_id;
                        $data['updated_date'] = date('Y-m-d');

                        $this->db->where('category_id',$category_exist['category_id']);
                        $this->db->update('category_tbl',$data);
                        // Update category : END
                    } else {
                        
                        // Insert new category : START
                        $data['created_by'] = $this->level_id;
                        $data['created_date'] = date('Y-m-d');

                        $this->db->insert('category_tbl', $data);
                        // Insert new category : END
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category imported successfully!</div>");
        redirect('manage-category');
    }

    public function export_category(){

        $category_data = $this->db->select("ct.*,ct1.category_name as parent_category_name")
                        ->from('category_tbl ct')
                        ->join('category_tbl ct1', 'ct.parent_category = ct1.category_id', 'left')
                        ->where('ct.created_by',  $this->level_id)
                        ->order_by('ct.category_id', 'desc')
                        ->get()->result();

        if(count($category_data) > 0) {
            // file name 
            $filename = 'export_category.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            
            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('category_name','parent_category_name','description','fractions','status');
            fputcsv($file, $header);
            foreach ($category_data as $record) {
                
                $sstatus = 'inactive';
                if($record->status){
                    $sstatus = 'active';
                }
                $data = array($record->category_name,$record->parent_category_name,$record->description,$record->fractions,$sstatus);
                fputcsv($file, $data);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No Data Found</div>");
            redirect('manage-category');
        }
    }
}
