<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Package_model');
        $this->load->model('common_model');
        $this->load->model('b_level/settings');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

    public function package()
    {

        $data['package_data'] = $this->Package_model->get_package_list();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/package/package',$data);
        $this->load->view('b_level/footer');
    }
    
    public function package_transaction(){
         $user_id = $this->session->userdata('user_id');
        $data['package_data_list'] = $this->Package_model->get_package_transaction_list();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/package/package_transaction_list',$data);  
        $this->load->view('b_level/footer');
    }
    
    public function default_package($package_id)
    {
        $user_id = $this->session->userdata('user_id');

        $expire_date = Date('Y-m-d', strtotime("+30 days"));
        $trail_data = array('package_id'=>1,'package_expire'=>$expire_date,'package_type'=>1);

                     $this->db->where('id',$user_id);
        $is_update = $this->db->update('user_info',$trail_data);

        update_customer_employee_C();
        
        if($is_update) 
        {
            redirect('wholesaler-package');
        }
    }

    public function trail_upgrade_package($package_id)
    {
        $user_id = $this->session->userdata('user_id');
        $package_details = $this->db->where('id',$package_id)->get('wholesaler_hana_package')->row();

        switch ($package_details->id) {
            case 1:
                $trail_column = 'advanced_trail';
                break;
            case 2:
                $trail_column = 'pro_trail';
                break;
        }

        $expire_date = Date('Y-m-d', strtotime("+".$package_details->trial_days." days"));
        $trail_data = array('package_id'=>$package_details->id,'package_expire'=>$expire_date,$trail_column=>1,'package_type'=>1);

        update_customer_employee_B();

                     $this->db->where('id',$user_id);
        $is_update = $this->db->update('user_info',$trail_data);
        if($is_update) 
        {
            redirect('wholesaler-package');
        }
    }

    public function confirm_paid_upgrade_package($package_id)
    {

        $user_id = $this->session->userdata('user_id');
        $data['card_details'] = $this->db->where(array('is_active'=>1,'created_by'=>$user_id))->get('wholesaler_card_info')->row();
        
        $data['currencys'] = $this->db->where('user_id',1)->get('company_profile')->row();
 
        $data['package_details'] = $this->db->where('id',$package_id)->get('wholesaler_hana_package')->row();

         $this->load->view('b_level/header');
         $this->load->view('b_level/sidebar');
         $this->load->view('b_level/package/package_confirm',$data);
         $this->load->view('b_level/footer');
    }

    function get_package_detal($package_id)
    {
        $package_details = $this->db->where('id',$package_id)->get('wholesaler_hana_package')->row();
        echo json_encode($package_details);
    }


    function paid_upgrade_package()
    {
       if($_POST['duration']=='monthly') 
       {
           $this->session->set_userdata('temp_duration','monthly');
       }
       if($_POST['duration']=='yearly') 
       {
           $this->session->set_userdata('temp_duration','yearly');
       }


        if (empty($_POST['card_number'])) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                please Select Valid Payment method !</div>");
            redirect('wholesaler-confirm-paid-upgrade-package/'.$_POST['package_id'].'/'.$_POST['card_id']);
        }
        if (empty($_POST['expiry_month'])) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                please Enter Expire Month !</div>");
            redirect('wholesaler-confirm-paid-upgrade-package/'.$_POST['package_id'].'/'.$_POST['card_id']);
        }
        if (empty($_POST['expiry_year'])) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                please Enter Expire Year !</div>");
            redirect('wholesaler-confirm-paid-upgrade-package/'.$_POST['package_id'].'/'.$_POST['card_id']);
        }
        if (empty($_POST['cvv'])) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                please Enter CVV !</div>");
            redirect('wholesaler-confirm-paid-upgrade-package/'.$_POST['package_id'].'/'.$_POST['card_id']);
        }
        if (empty($_POST['card_holder_name'])) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                please Enter Card Holder Name !</div>");
            redirect('wholesaler-confirm-paid-upgrade-package/'.$_POST['package_id']);
        }
        unset($_POST['card_no']);

            $this->db->where('id',$_POST['card_id']);
            $this->db->update('wholesaler_card_info',array('cvv'=>$_POST['cvv']));  

        $this->load->library('gwapi');
        $user_id = $this->session->userdata('user_id');
        $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();

        $package_details = $this->db->where('id',$_POST['package_id'])->get('wholesaler_hana_package')->row();

        $installment_amount = $package_details->installation_price;

        $remaining_amount = 0;
        if($_POST['duration'] =='monthly') 
        {
            $amount = $package_details->monthly_price;
            $plan_id = $package_details->monthly_plan_id;
        }
        if($_POST['duration'] =='yearly') 
        {
            $amount = $package_details->yearly_price;
            $plan_id = $package_details->yearly_plan_id;
            
            if ($user_deatail->package_type==2) 
            {
                $now = strtotime(Date('Y-m-d h:i:s'));
                $expire_date = strtotime($user_deatail->package_expire);  
                $diff = abs($expire_date - $now);  
                $years = floor($diff / (365*60*60*24));            
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  

                // for current package remaining amount
                $package_details_for_remaining_amount = $this->db->where('id',$user_deatail->package_id)->get('wholesaler_hana_package')->row();
                // for current package remaining amount

                $remaining_amount = $months * $package_details_for_remaining_amount->monthly_price;
                // echo $remaining_amount;die();
            }
            // Old Package Amount Reduse
        }

        $recurring_amount = $amount-$remaining_amount;

        $installment_payable_amount = $installment_amount-$remaining_amount;

        $txn_coupon = '';
        $txn_discount = '';
        if($_POST['coupon_status']==1) 
        {
            $coupon_info = $this->db->where('coupon',$_POST['coupon_code'])->get('coupon')->row();
            $txn_coupon = $_POST['coupon_code'];
            /*************************/
                $min_purchase = $coupon_info->min_purchase;
                if($coupon_info->type==1) 
                {
                    $flat = $coupon_info->flat;
                    if($installment_payable_amount > $min_purchase) 
                    {
                       $installment_payable_amount = $installment_payable_amount - $flat;
                       $txn_discount = $flat;
                    }
                }
                if($coupon_info->type==2) 
                {
                    if($installment_payable_amount > $min_purchase) 
                    {
                       $percent = $coupon_info->percent;
                       $max_discount = $coupon_info->max_discount;

                       $percent_ot_total_amount = $installment_payable_amount * $percent/100;

                       if ($percent_ot_total_amount > $max_discount) 
                       {
                            $discount = $max_discount;
                       }else{
                            $discount = $percent_ot_total_amount;
                       }
                       $installment_payable_amount = $installment_payable_amount - $discount;
                       $txn_discount = $discount;
                    }
                }
            /***********************************/
           
        }


        $cusstomer_info     = $this->db->where('id',$user_id)->get('user_info')->row();

        $card_number = $_POST['card_number'];

        $payment_data = array('recurring_amount'=>$recurring_amount,
                              'installment_payable_amount'=>$installment_payable_amount,
                              'customer_no'=>$cusstomer_info->id,
                              'customer_id'=>$cusstomer_info->id,
                              'package_id'=>$package_details->id,
                              'plan_id'=>$plan_id,
                              'payment_method' => 'card',
                              'card_number' => $_POST['card_number'],
                              'expiry_month' => $_POST['expiry_month'],
                              'expiry_year' => $_POST['expiry_year'],
                              'cvv' => $_POST['cvv'],
                              'card_holder_name' => $_POST['card_holder_name']
                            );


        $res = $this->receive_payment_to_admin($payment_data);

        $user_id = $this->session->userdata('user_id');

                    if ($installment_payable_amount < 0 || $installment_payable_amount == 0) 
                    {
                        $installment_payable_amount = 0;
                    }        

            if($res['instalment_response']->response=='1')
            {
                $transaction_data = array('user_id'=>$user_id,
                                          'package_id'=>$package_details->id,
                                          'amount'=>$installment_payable_amount,
                                          'transaction_type'=>'credit',
                                          'transaction_id'=>$res['instalment_response']->transactionid,
                                          'status'=>1,
                                          'coupon'=>$txn_coupon,
                                          'discount'=>$txn_discount,
                                          'card_no'=>$card_number);
                $this->db->insert('wholesaler_package_transaction',$transaction_data);
            }
            if($res['instalment_response']->response=='2')
            {
                $transaction_data = array('user_id'=>$user_id,
                                          'package_id'=>$package_details->id,
                                          'amount'=>$installment_payable_amount,
                                          'transaction_type'=>'credit',
                                          'transaction_id'=>$res['instalment_response']->transactionid,
                                          'status'=>0,
                                          'coupon'=>$txn_coupon,
                                          'discount'=>$txn_discount,
                                          'card_no'=>$card_number);
                $this->db->insert('wholesaler_package_transaction',$transaction_data);
               redirect(base_url().'wholesaler-payment-failed');
            }
            
            if($res['instalment_response']->response=='3')
            {
                $this->session->set_flashdata('error', $res['instalment_response']->responsetext);
                redirect(base_url().'wholesaler-payment-failed');
            }



        if($res['instalment_response']->response=='1')
        {
            $package_details = $this->db->where('id',$_POST['package_id'])->get('wholesaler_hana_package')->row();

            $package_duration = 0;
            if($_POST['duration'] =='monthly') 
            {
                $package_duration = 30;
                $package_duration_type = 1;
            }
            if($_POST['duration'] =='yearly') 
            {
                $package_duration = 365;
                $package_duration_type = 2;
            }

            $card_id = $_POST['card_id']; 

            $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


            $paid_data = array('package_id'=>$package_details->id,'package_expire'=>$expire_date,'package_type'=>2,'package_duration'=>$package_duration_type,'card_id'=>$card_id);

                         $this->db->where('id',$user_id);
            $is_update = $this->db->update('user_info',$paid_data);  

            update_customer_employee_B();

                if(isset($res['recurring_response']->subscription_id) && !empty($res['recurring_response']->subscription_id))
                {

                    $add_sub = array('subscription_id'=>$res['recurring_response']->subscription_id,'billing_date'=>date('d-m-Y'));
                    $this->db->where('id',$user_id);
                    $this->db->update('user_info',$add_sub);  
                }

            if($is_update)
            {
                $this->session->set_flashdata('success', "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                You Have Sucessfully subscribed".$package_details->package." package</div>");
                redirect(base_url().'wholesaler-package-my-subscription');
            }
        }


        
    }


        

    function receive_payment_to_admin($payment_data)
    { 


        $customer_id                    = $this->session->userdata('user_id');
        $customer_no                    = $payment_data['customer_no'];
        $payment_method                 = $payment_data['payment_method'];
        $package_id                     = $payment_data['package_id'];
        $recurring_amount               = $payment_data['recurring_amount'];
        $installment_payable_amount     = $payment_data['installment_payable_amount'];
        $ccnumber                       = str_replace('-','',$payment_data['card_number']);
        $card_holder_name               = $payment_data['card_holder_name'];
        $ccexp                          = $payment_data['expiry_month'].'/'.$payment_data['expiry_year'];
        $cvv                            = $payment_data['cvv'];
        $ip                             = $this->input->ip_address();
        $plan_id                        = $payment_data['plan_id'];

        $cusstomer_info = $this->db->where('id',$customer_id)->get('user_info')->row();
       
        // $orderd = $this->db->select("*")->from('quatation_tbl')->where('package_id', $package_id)->get()->row();
        $cHead      = $this->db->select('HeadCode,level_id')->where('HeadCode',$customer_no)->get('acc_coa')->row();

        $cmp_info   = $this->settings->company_profile(); 
        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();


#----------------------------
#   payment gatway
#----------------------------      

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($package_id, $orderdescription='Package payment', $tax=0,$Shipping=0,$ip);

        $response = array();

/* less then zero amount */
            if ($installment_payable_amount < 0 || $installment_payable_amount == 0) 
            {
                $NewDate=Date('m/d/Y', strtotime("+30 days"));

                $perameter['recurring'] = "add_subscription";
                $perameter['redirect_url'] = "https://www.google.com";
                $perameter['plan_id'] = $plan_id;
                $perameter['account_type'] = "savings";
                $perameter['entity_type'] = "business";
                $perameter['payment'] = "creditcard";   
                $perameter['start_date'] = date("Ymd", strtotime($NewDate));
                $this->gwapi->sub_perameter($perameter);

                $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);
                $response['recurring_response'] = $recurring_response;


                            $installment_response = (object)[];
                            $installment_response->response = 1;
                            $installment_response->responsetext = 'SUCCESS';
                            $installment_response->authcode = '123456';
                            $installment_response->transactionid = '0000000000';
                            $installment_response->avsresponse = 'N';
                            $installment_response->cvvresponse = 'N';
                            $installment_response->orderid = $package_id;
                            $installment_response->type = 'sale';
                            $installment_response->response_code = '100';

                            $response['instalment_response'] = $installment_response;


                                                   #----------------------------
                                                    #   payment set database
                                                    #----------------------------

                                                    if($installment_response->response=='1')
                                                    {

                                                                if(!empty($cHead->HeadCode)){

                                                                    $voucher_no = $package_id;
                                                                    $Vtype = "INV";
                                                                    $VDate = date('Y-m-d');
                                                                    $paid_amount = $this->input->post('paid_amount');
                                                                    $cAID = $cHead->HeadCode;
                                                                    $IsPosted = 1;
                                                                    $CreateBy = $this->session->userdata('user_id');
                                                                    $createdate = date('Y-m-d H:i:s');

                                                                    //customer credit insert acc_transaction
                                                                    $customerCredit = array(

                                                                        'VNo'           => $voucher_no,
                                                                        'Vtype'         => $Vtype,
                                                                        'VDate'         => $VDate,
                                                                        'Debit'         => 0,
                                                                        'Credit'        => $paid_amount,
                                                                        'COAID'         => $cAID,
                                                                        'level_id'      => $cHead->cHead,
                                                                        'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                                                                        'IsPosted'      => $IsPosted,
                                                                        'CreateBy'      => $CreateBy,
                                                                        'CreateDate'    => $createdate,
                                                                        'IsAppove'      => 1
                                                                    );

                                                                    $this->db->insert('acc_transaction', $customerCredit);
                                                                    //------------------------------------

                                                                    // debit acc_transaction
                                                                    $payment_method = $this->input->post('payment_method');
                                                                    
                                                                    $COAID = '102010202';
                                                                    

                                                                    $b_levelDebit = array(
                                                                        'VNo' => $voucher_no,
                                                                        'Vtype' => $Vtype,
                                                                        'VDate' => $VDate,
                                                                        'Debit' => $paid_amount,
                                                                        'Credit' => 0,
                                                                        'COAID' => $COAID,
                                                                        'level_id'      => $cHead->cHead,
                                                                        'Narration' => "Amount received for invoice #".$voucher_no,
                                                                        'IsPosted' => $IsPosted,
                                                                        'CreateBy' => $CreateBy,
                                                                        'CreateDate' => $createdate,
                                                                        'IsAppove' => 1
                                                                    );

                                                                    $this->db->insert('acc_transaction', $b_levelDebit);
                                                                    //------------------------------------

                                                                }

                                                                // C level notification

                                                                $cNotificationData = array(
                                                                    'notification_text' => 'Payment has been received for '.$package_id,
                                                                    'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                                                                    'created_by'        => $this->session->userdata('user_id'),
                                                                    'date'              => date('Y-m-d')
                                                                );

                                                                $this->db->insert('b_notification_tbl',$cNotificationData);

                                                                //-------------------------


                                                                // $this->Order_model->smsSend(

                                                                //     $data = array(
                                                                //         'customer_id' => $customer_id,
                                                                //         'message'     => 'Payment has been done for Package ',
                                                                //         'subject'     => 'Package payment'
                                                                //     )

                                                                // );


                                                    } 

                return $response;
     
            }

//for direct payment diduct


        $installment_response = (object)$this->gwapi->doSale($installment_payable_amount, $ccnumber, $ccexp, $cvv);
        $response['instalment_response'] = $installment_response;

        if($installment_response->response!='1')
        {
            return $response;
        }
//for direct payment diduct
        $NewDate=Date('m/d/Y', strtotime("+30 days"));

//for recurring
        $perameter['recurring'] = "add_subscription";
        $perameter['redirect_url'] = "https://www.google.com";
        $perameter['plan_id'] = $plan_id;
        $perameter['account_type'] = "savings";
        $perameter['entity_type'] = "business";
        $perameter['payment'] = "creditcard";   
        $perameter['start_date'] = date("Ymd", strtotime($NewDate));
        $this->gwapi->sub_perameter($perameter);

        $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

        $response['recurring_response'] = $recurring_response;
//for recurring



#----------------------------



#----------------------------
#   payment set database
#----------------------------

        if($installment_response->response=='1')
        {

                    if(!empty($cHead->HeadCode)){

                        $voucher_no = $package_id;
                        $Vtype = "INV";
                        $VDate = date('Y-m-d');
                        $paid_amount = $this->input->post('paid_amount');
                        $cAID = $cHead->HeadCode;
                        $IsPosted = 1;
                        $CreateBy = $this->session->userdata('user_id');
                        $createdate = date('Y-m-d H:i:s');

                        //customer credit insert acc_transaction
                        $customerCredit = array(

                            'VNo'           => $voucher_no,
                            'Vtype'         => $Vtype,
                            'VDate'         => $VDate,
                            'Debit'         => 0,
                            'Credit'        => $paid_amount,
                            'COAID'         => $cAID,
                            'level_id'      => $cHead->cHead,
                            'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                            'IsPosted'      => $IsPosted,
                            'CreateBy'      => $CreateBy,
                            'CreateDate'    => $createdate,
                            'IsAppove'      => 1
                        );

                        $this->db->insert('acc_transaction', $customerCredit);
                        //------------------------------------

                        // debit acc_transaction
                        $payment_method = $this->input->post('payment_method');
                        
                        $COAID = '102010202';
                        

                        $b_levelDebit = array(
                            'VNo' => $voucher_no,
                            'Vtype' => $Vtype,
                            'VDate' => $VDate,
                            'Debit' => $paid_amount,
                            'Credit' => 0,
                            'COAID' => $COAID,
                            'level_id'      => $cHead->cHead,
                            'Narration' => "Amount received for invoice #".$voucher_no,
                            'IsPosted' => $IsPosted,
                            'CreateBy' => $CreateBy,
                            'CreateDate' => $createdate,
                            'IsAppove' => 1
                        );

                        $this->db->insert('acc_transaction', $b_levelDebit);
                        //------------------------------------

                    }

                    // C level notification

                    $cNotificationData = array(
                        'notification_text' => 'Payment has been received for '.$package_id,
                        'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                        'created_by'        => $this->session->userdata('user_id'),
                        'date'              => date('Y-m-d')
                    );

                    $this->db->insert('b_notification_tbl',$cNotificationData);

                    //-------------------------


                    // $this->Order_model->smsSend(

                    //     $data = array(
                    //         'customer_id' => $customer_id,
                    //         'message'     => 'Payment has been done for Package ',
                    //         'subject'     => 'Package payment'
                    //     )

                    // );


        } 

        return $response;
#--------------------------
        
    }


    function payment_failed()
    {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/package/payment_failed');
        $this->load->view('b_level/footer');
    }


    function get_coupon_detail($coupon)
    {
        $coupon_info = $this->db->where(array('coupon'=>$coupon,'status'=>1))->get('coupon')->row();
        $coupon_expire_status = check_coupon_date_validation($coupon);

        if ($coupon_expire_status==false) 
        {
            echo json_encode(false);
            exit;
        }


        if(!empty($coupon_info)) 
        {
            echo json_encode($coupon_info);
            exit;
        }else{
            echo json_encode(false);
            exit;
        }
    }

    function old_package_remaining_amount()
    {
        $user_id = $this->session->userdata('user_id');
        $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();
        $remaining_amount = 0;
        if ($user_deatail->package_type==2) 
        {
            $now = strtotime(Date('Y-m-d h:i:s'));
            $expire_date = strtotime($user_deatail->package_expire);  
            $diff = abs($expire_date - $now);  
            $years = floor($diff / (365*60*60*24));            
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  

            // for current package remaining amount
            $package_details_for_remaining_amount = $this->db->where('id',$user_deatail->package_id)->get('wholesaler_hana_package')->row();
            // for current package remaining amount

            $remaining_amount = $months * $package_details_for_remaining_amount->monthly_price;
            // echo $remaining_amount;die();
        }
        
        if ($remaining_amount > 0) 
        {
            echo json_encode($remaining_amount);
        }else{
            echo json_encode(false);
        }
        
    }


    function my_subscription()
    {
       
        $user_id = $this->session->userdata('user_id');
        $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();

        $data['package_details'] = $this->db->where('id',$user_deatail->package_id)->get('wholesaler_hana_package')->row();
    
        $data['package_transaction'] = $this->db->where('user_id',$user_id)->order_by('id','DESC')->get('wholesaler_package_transaction')->result();

       
        $data['user_deatail'] = $user_deatail;
      
        $data['card_detail'] = $this->db->where('id',$user_deatail->card_id)->get('wholesaler_card_info')->row();
       
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/package/my_subscription',$data);
        $this->load->view('b_level/footer');
    }





    function card_list($package_id='')
    {
        $user_id = $this->session->userdata('user_id');
        $data['card_list'] = $this->db->where('user_id',$user_id)->get('card_details')->result();

        $data['package_id'] = $package_id;
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/package/card_list',$data);
        $this->load->view('c_level/footer');
    }

    function save_card()
    {
            $user_id = $this->session->userdata('user_id');
            
            $card_data['user_id']   = $user_id;

            $card_data['type'] = $_POST['type'];
            $card_data['card_number'] = $_POST['card_number'];
            $card_data['card_holder_name'] = $_POST['card_holder_name'];
            $card_data['exp_month']   = $_POST['exp_month'];
            $card_data['exp_year']    = $_POST['exp_year'];

            $is_insert = $this->db->insert('card_details',$card_data);

            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Insert Successfull !</div>");
            redirect(base_url().'payment-method/'.$_POST['package_id']);
    }

    function edit_card($card_id='')
    {
        $data['card_detail'] = $this->db->where('id',$card_id)->get('card_details')->row();

        if (!empty($_POST)) 
        {
            $card_data['type'] = $_POST['type'];
            $card_data['card_number'] = $_POST['card_number'];
            $card_data['card_holder_name'] = $_POST['card_holder_name'];
            $card_data['exp_month']   = $_POST['exp_month'];
            $card_data['exp_year']    = $_POST['exp_year'];

            $this->db->where('id',$_POST['id']);
            $is_update = $this->db->update('card_details',$card_data);

            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Update Successfull !</div>");
            redirect('retailer-package-edit-card/'.$_POST['id']);
        }

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/package/edit_card',$data);
        $this->load->view('c_level/footer');

    }

    function remove_card($card_id)
    {
        $this->db->where('id', $card_id);
        $this->db->delete('card_details');

        echo json_encode(true);
    }

    function set_primary_card($card_id)
    {
        $user_id = $this->session->userdata('user_id');
        $is_update = $this->db->where('created_by',$user_id)->update('c_card_info',array('is_active'=>0));

        if($is_update) 
        {
            $this->db->where(array('id'=>$card_id,'created_by'=>$user_id))->update('c_card_info',array('is_active'=>1));
            echo json_encode(true);
        }

    }

    function delete_subscription()
    {
        $user_id = $this->session->userdata('user_id');
        $user_detail = $this->db->where('id',$user_id)->get('user_info')->row();
        
        // $this->load->library('gwapi');

        // $type = 'delete_subscription';
        // $subscription_id = $user_detail->subscription_id;


        // $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();
        // $this->gwapi->setLogin($machentinfo);

        // $r = (object)$this->gwapi->delete_subscription($type, $subscription_id);

       // if($r->response=='1') 
        //{
        $this->db->where('id',$user_id)->update('user_info',array('subscription_id'=>0,'package_id'=>0,'package_type'=>0,'package_duration'=>null,'advanced_trail'=>0));
         
           // update_customer_employee_B();
          // redirect('wholesaler-logout');
            echo json_encode(true);

        //}
        //if($r->response=='2') 
       // {
           // echo json_encode(false);
        //}


    }

    function add_subdomain($subdomain)
    {
        $reserve_domain = $this->db->where('subdomain',$subdomain)->get('reserve_domain')->row();
        if($reserve_domain) 
        {
            echo json_encode(false);
            exit;
        }

        $domain_exist = $this->db->where('sub_domain',$subdomain)->get('user_info')->row();
        if($domain_exist) 
        {
            echo json_encode(false);
            exit;
        }

        $is_update = $this->db->where('id',$this->session->userdata('user_id'))->update('user_info',array('sub_domain'=>$subdomain));

        if ($is_update) 
        {
           echo json_encode(true);
        }
    }

}
