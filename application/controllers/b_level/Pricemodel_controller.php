<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricemodel_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $package = $this->session->userdata('packageid');
        $this->load->model('b_level/Pricemodel_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }



//    ===============its for add_price ===============
    public function add_price() {
        $this->permission->check_label(30)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(23);
        $data['style_slist'] = $this->db->get('row_column')->result();
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";
        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pricemodel/price_add', $data);
        $this->load->view('b_level/footer');
         }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

    public function save_price_style() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "price model information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $dataName = array(
            'style_name' => $this->input->post('price_style_name'),
            'style_type' => $this->input->post('style_type'),
            'create_data_time' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('user_id'),
            'active_status' => 1,
        );

        $this->db->insert('row_column', $dataName);

        // echo $this->db->last_query();die;

        $getStyle_id = $this->db->insert_id();

        if ($getStyle_id) {

            $getData = $this->input->post('row');
            $price_chaildData = [];
            foreach ($getData as $key => $value) {
                $price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($value, ','));
                $rowData[] = explode(',', $value);
            }


            $j = 0;
            foreach ($rowData as $row => $columns) {

                $j++;
                $i = 0;
                $rss = '';
                foreach ($columns as $row2 => $column2) {
                    $i++;

                    if ($j > 1) {

                        if ($_POST['test1' . $j] != '' && $_POST['test' . $i . "1"] != '') {

                            $priceData[] = array(
                                'style_id' => $getStyle_id,
                                'row' => $_POST['test' . $i . "1"],
                                'col' => $_POST['test1' . $j],
                                'price' => $_POST['test' . $i . $j]
                            );
                        }
                    }
                    $rss .= $_POST['test' . $i . $j] . ',';
                }

                //$price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($rss, ','));
            }

            $this->db->insert_batch('price_style', $priceData);
            $this->db->insert_batch('price_chaild_style', $price_chaildData);

            echo 1;
        } else {
            echo 2;
        }
    }

    public function edit($id) {

        $data = $this->db->where('style_id', $id)->get('price_chaild_style')->result();

        $table = '';
        $i = 1;

        foreach ($data as $key => $value) {

            $rowData = explode(',', $value->row);
            $table .= '<tr>';
            $table .= '<input type="hidden" name="row[]" value="' . $value->row . '">';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" class="price_input" id="' . $ids . '" autocomplete="off" /></td>';
                } else {
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" autocomplete="off" /></td>';
                }
                $j++;
            }
            $table .= '</tr><br/>';
            $i++;
        }

        $table .= '</table>';

        echo $table;
    }

//    ===============its for manage_price ===============
    public function manage_price() {
        $this->permission->check_label(24)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(24);
        if ($permission_accsess==1) {
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar');
            $this->load->view('b_level/pricemodel/price_manage');
            $this->load->view('b_level/footer');
        }else{
            $this->load->view('b_level/header');
            $this->load->view('b_level/sidebar'); 
            $this->load->view('b_level/upgrade_error'); 
            $this->load->view('b_level/footer');
        }
    }

    function get_row_column_Lists(){
        
        $data = array();
        // Fetch member's records
        $row_price = $this->Pricemodel_model->getRows($_POST);
        // echo "<pre>";print_r($sqm_price);die;
        foreach($row_price as $record){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$record->style_id.'" class="checkbox_list">';

            $status = ($record->status == 1 ? 'Active' : 'Inactive');
             $package = $this->session->userdata('packageid');
              if (!empty($package)){
             $menu_permission= b_access_role_permission(24);
             $action = ''; 
            if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){
            $action .='<a href="'.base_url('edit-price/'.$record->style_id).'" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" ><i class="fa fa-pencil"></i></a>';
             }
            if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){
            $action .= '<a href="'.base_url('b_level/pricemodel_controller/delete_price_style/'.$record->style_id).'" onclick="return confirm(\'Are you sure\')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" ><i class="fa fa-trash"></i></a>';
            }
            }
            // Get Assigned Product name : START
            $sql = "SELECT product_name FROM product_tbl WHERE price_rowcol_style_id = $record->style_id";
            $results = $this->db->query($sql)->result();
            $product_name ='';
            if ($results) {
                $product_name .="<ul>";
                $k = 0;
                foreach ($results as $result) {
                    $k++;
                    $product_name .="<li>" . $k . ") " . $result->product_name . "</li>";
                }
                $product_name .="</ul>";
            } else {
                $product_name .='<p>The products will be assigned at the later stage</p>';
            }
            // Get Assigned Product name : END
            
            $data[] = array($chk, $i, $record->style_name, $product_name, $status ,$action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Pricemodel_model->countAll(),
            "recordsFiltered" => $this->Pricemodel_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

    public function delete_price_style($style_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "price model information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->set('price_rowcol_style_id', 0)->where('price_rowcol_style_id', $style_id)->update('product_tbl');
        $this->db->where('style_id', $style_id)->delete('row_column');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Style Delete successfully!</div>");
        redirect("manage-price");
    }

//    ===============its for add_group ===============
    public function edit_style($style_id) {

        $datas = $this->db->where('style_id', $style_id)->get('price_chaild_style')->result();
        $table = '';
        $i = 1;

        foreach ($datas as $key => $value) {

            $rowData = explode(',', $value->row);

            //print_r($rowData);

            $table .= '<tr>';
            $table .= '<input type="hidden" name="row[]" value="' . $value->row . '">';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" class="price_input" id="' . $ids . '" autocomplete="off" /></td>';
                } else {
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" autocomplete="off" /></td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $table .= '<br/>';
            $i++;
        }


        $table .= '</table>';

        $data['style_slist'] = $this->db->where('style_id', $style_id)->get('row_column')->row();
        $data['list'] = $table;
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pricemodel/edit_style', $data);
        $this->load->view('b_level/footer');
    }

    public function update_price_style() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "price model information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $style_id = $this->input->post('style_id');
        $dataName = array(
            'style_name' => $this->input->post('price_style_name'),
            'style_type' => $this->input->post('style_type'),
            'create_data_time' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('user_id'),
        );

        $this->db->where('style_id', $style_id)->update('row_column', $dataName);
        $getStyle_id = $style_id;

        if (!empty($getStyle_id)) {
            $this->db->where('style_id', $getStyle_id)->delete('price_style');
            $this->db->where('style_id', $getStyle_id)->delete('price_chaild_style');
        }

        if ($getStyle_id) {

            $getData = $this->input->post('row');

            foreach ($getData as $key => $value) {

                $rowData[] = explode(',', $value);
            }


            $j = 0;
            foreach ($rowData as $row => $columns) {

                $j++;
                $i = 0;
                $rss = '';
                foreach ($columns as $row2 => $column2) {
                    $i++;

                    if ($j > 1) {

                        if ($_POST['test1' . $j] != '' && $_POST['test' . $i . "1"] != '') {
                            $priceData[] = array(
                                'style_id' => $getStyle_id,
                                'row' => $_POST['test' . $i . "1"],
                                'col' => $_POST['test1' . $j],
                                'price' => $_POST['test' . $i . $j]
                            );
                        }
                    }
                    $rss .= $_POST['test' . $i . $j] . ',';
                }

                $price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($rss, ','));
            }



            $this->db->insert_batch('price_style', $priceData);
            $this->db->insert_batch('price_chaild_style', $price_chaildData);

            echo 1;
        } else {
            echo 2;
        }
    }


//    ============== its for add_group_price ==================
    public function add_group_price() {

        $this->permission->check_label(108)->create()->redirect();
         $permission_accsess=b_access_role_permission_page(108);
        $data['style_slist'] = $this->db->get('row_column')->result();
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";
        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pricemodel/add_group_price', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

//    ============== its for manage_group_price ==================
    public function manage_group_price() {

        $this->permission->check_label(78)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(78);
        // $data['style_slist'] = $this->db->select('row_column.*, row_column.active_status as status,product_tbl.product_name, product_tbl.active_status')
        //                 ->join('product_tbl', 'product_tbl.price_rowcol_style_id=row_column.style_id', 'left')
        //                 ->where('row_column.style_type', 4)
        //                 ->where('row_column.create_by', $this->level_id)
        //                 ->order_by('row_column.style_id', 'desc')
        //                 ->get('row_column')->result();

        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pricemodel/manage_group_price', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
    }
        
    }


    function get_group_price_Lists(){
        
        $data = array();
        
        // Fetch member's records
        $row_price = $this->Pricemodel_model->getRows($_POST,'4');
        // echo "<pre>";print_r($sqm_price);die;
        foreach($row_price as $record){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$record->style_id.'" class="checkbox_list">';

            $status = ($record->status == 1 ? 'Active' : 'Inactive');
            $action = '';
            $menu_permission= b_access_role_permission(109); 

            $package = $this->session->userdata('packageid');
            if (!empty($package)){
                if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){
                    $action .= '<a href="'.base_url('edit-group-price/'.$record->style_id).'" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" ><i class="fa fa-pencil"></i></a>&nbsp;';
                }
            }

            if($this->permission->check_label('group_price')->delete()->access()){
                $package = $this->session->userdata('packageid');
                if (!empty($package)){
                    if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){
                        $action .=  '<a href="'.base_url('b_level/pricemodel_controller/delete_price_style/'.$record->style_id).'" onclick="return confirm(\'Are you sure\')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" ><i class="fa fa-trash"></i></a>';
                    }
                }
            }


            // Get Assigned Product name : START
            $sql = "SELECT product_name FROM product_tbl WHERE price_rowcol_style_id = $record->style_id";
            $results = $this->db->query($sql)->result();
            $product_name ='';
            if ($results) {
                $product_name .="<ul>";
                $k = 0;
                foreach ($results as $result) {
                    $k++;
                    $product_name .="<li>" . $k . ") " . $result->product_name . "</li>";
                }
                $product_name .="</ul>";
            } else {
                $product_name .='<p>The products will be assigned at the later stage</p>';
            }
            // Get Assigned Product name : END
            
            $data[] = array($chk, $i, $record->style_name, $product_name, $status ,$action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Pricemodel_model->countAll('4'),
            "recordsFiltered" => $this->Pricemodel_model->countFiltered($_POST,'4'),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }
    
    //    ===============its for add_group ===============
    public function edit_group_price_style($style_id) {

        $datas = $this->db->where('style_id', $style_id)->get('price_chaild_style')->result();
        $table = '';
        $i = 1;


        foreach ($datas as $key => $value) {

            $rowData = explode(',', $value->row);

            //print_r($rowData);

            $table .= '<tr>';
            $table .= '<input type="hidden" name="row[]" value="' . $value->row . '">';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" class="price_input" id="' . $ids . '" autocomplete="off" /></td>';
                } else {
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" autocomplete="off" /></td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $table .= '<br/>';
            $i++;
        }


        $table .= '</table>';

        $data['style_slist'] = $this->db->where('style_id', $style_id)->get('row_column')->row();
        $data['list'] = $table;
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/pricemodel/edit_group_price_style', $data);
        $this->load->view('b_level/footer');
    }
    /** Start added by insys */
    //    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('row_column','style_id');
            if($this->input->post('action_type')=='price_manage') {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Price has been Deleted successfully.</div>");
                redirect("manage-price");
            } else {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Group Price has been Deleted successfully.</div>");
                redirect("manage-group-price");
            }
        }
    } 
    /** End added by insys */
}
