<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturer_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        $packageid = $this->session->userdata('packageid');

        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
         
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Order_model');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Customer_model');
        $this->load->model('b_level/settings');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }

    public function save_blind_menufactur() {

        $order_id = $this->input->post('orderid');
        $product_id = $this->input->post('product_id');


        $sc_chk = $this->input->post('sc_chk');
        $val_cut_chk = $this->input->post('val_cut_chk');
        $chk = $this->input->post('chk');


        $hr = $this->input->post('hr');
        $br = $this->input->post('br');
        $tb = $this->input->post('tb');
        $sc = $this->input->post('sc');
        $cl = $this->input->post('cl');


        $blindData = [];
        foreach ($product_id as $key => $id) {

            $opdata = array(
                'sc_chk' => @$sc_chk[$id],
                'val_cut_chk' => @ $val_cut_chk[$id],
                'chk' => @$chk[$id],
                'hr' => @$hr[$id],
                'br' => @$br[$id],
                'tb' => @$tb[$id],
                'sc' => @$sc[$id],
                'cl' => @$cl[$id]
            );

            $blindData[] = array(
                'order_id' => $order_id,
                'product_id' => $id,
                'chk_option' => json_encode($opdata)
            );


            $this->db->where('order_id', $order_id)->where('product_id', $id)->delete('manufactur_data');
        }


        $this->db->insert_batch('manufactur_data', $blindData);

        echo '1';
    }

//    ============== its for add_manufacturer ===================
    public function add_manufacturer() {


        $this->permission->check_label(37)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(37);
        $search = (object) array(
                    'product_id' => $this->input->post('product_id'),
                    'company_id' => $this->input->post('company_id'),
                    'sidemark' => $this->input->post('sidemark'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => $this->input->post('order_stage')
        );

        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->company_id;
        $data['side_mark'] = $search->sidemark;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;

        $total_rows = $this->Order_model->count_all_manufactur_orderd($search);

        $per_page = 10;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page);
        $this->pagination->initialize($config);

        $data["links"] = $this->pagination->create_links();

        $data['orderd'] = $this->Order_model->get_all_manufactur_orderd($search, $per_page, $page);

        $data['b_user_orderd'] = $this->Order_model->get_all_manufactur_orderd_from_b_user($search, $per_page, $page);



        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['search'] = $search;


        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/add_manufacturer', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

    function pagination($total_rows, $per_page, $page) {

        $config["base_url"] = base_url('add-manufacturer');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $page;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        return $config;
    }

    public function quatation($order_id) {

        $this->permission->check_label(35)->create()->redirect();

        $data['orderd'] = $this->Order_model->get_order_and_manufacturing_order_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_manufacture_orderd_details_by_id($order_id);
        if (empty($data['orderd'])) {
            $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
            $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);
        }


        $data['chit'] = $this->chit($data['orderd'], $data['order_details']);


        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();

        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();
        $data['for'] = $this->db->where('user_id', $data['orderd']->customer_id)->get('company_profile')->row();
        $data['categories'] = array();
        $order_details = $data['order_details'];
        foreach ($order_details as $key => $blind) {
            if (!in_array($blind->category_name, $data['categories'])) {
                $data['categories'][] = $blind->category_name;
            }
        }

        $data['chit'] = $this->allchit($data['orderd'], $order_details);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturing', $data);
        $this->load->view('b_level/footer');
    }

    public function manufacturing_level($order_id) {

        $this->permission->check_label(36)->read()->redirect();

        $data['orderd'] = $this->Order_model->get_order_and_manufacturing_order_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_manufacture_orderd_details_by_id($order_id);

        if (empty($data['orderd'])) {
            $data['orderd'] = $this->Order_model->get_b_to_b_orderd_by_id($order_id);
            $data['order_details'] = $this->Order_model->get_b_to_b_orderd_details_by_id($order_id);
        }
        $company = $this->db->select('company')
                        ->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $data['orderd']->company = $company->company;
        $data['chit'] = $this->chit($data['orderd'], $data['order_details']);


        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->settings->company_profile();

        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $data['for'] = $this->db->where('user_id', $data['orderd']->level_id)->get('company_profile')->row();

        $data['categories'] = array();
        $order_details = $data['order_details'];
        foreach ($order_details as $key => $blind) {
            if (!in_array($blind->category_name, $data['categories'])) {
                $data['categories'][] = $blind->category_name;
            }
        }

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturing_level', $data);
        $this->load->view('b_level/footer');
    }

    function chit($orderd, $order_details) {

        $this->load->model('b_level/Manufactur_model');

        $blinds = [];
        $Shades = [];
        if (!empty($order_details)) {

            $i = 1;
            $its = '';
            foreach ($order_details as $key => $value) {


                $fraction_width_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->width_fraction_id)->get()->row();
                $fraction_height_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->height_fraction_id)->get()->row();

                $fraction_width = $fraction_width_data->fraction_value;
                $fraction_height = $fraction_height_data->fraction_value;
                $fraction_width_decimal = $fraction_width_data->decimal_value;
                $fraction_height_decimal = $fraction_height_data->decimal_value;

                if ($value->category_name == 'Blinds') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }

                        $MountData = '';
                        $tile = '';

                        if (!empty($attr)) {
                            foreach ($attr as $key => $att) {
                                // echo "<pre>";
                                // print_r($att);
                                // exit;

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                                if ($a->attribute_name == "Mount") {

                                    $MountData = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }


                                if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {

                                    $tile = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                $room = $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';

                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.'-'.$i.@$its."#".$value->row_id;

                        $this->br_code->gcode1($barcode_img_path);


                        $blinds[] = '<div class="table col-sm-4 mt-3">
                                <table class="table table-hover" style="width:76.2mm;height:38.1mm;font-size: 14px;border-spacing: 0px;">                                
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">
                                            Product Name: ' . $value->product_name . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">'. $value->pattern_name . '<br/> ' . $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->room . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="Blinds' . $i . '" class="hidden table table-hover" style="width:76.2mm;height:38.1mm;font-size: 10px;border-spacing: 0px;">                                
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">
                                            Product Name: ' . $value->product_name . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">'. $value->pattern_name . '<br/> ' . $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->room . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                            <button type="button" class="btn btn-success" onclick="printContent(\'Blinds' . $i . '\')" style="margin-top: 10px;">Print labels</button>
                        </div>
                        ';
                    }
                }



                if ($value->category_name == 'Shades') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }


                        $controlType = '';
                        $controlPosition = '';
                        $controlLength = '';



                        if (!empty($attr)) {

                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();



                                if ($a->attribute_name == 'Control Type' || $a->attribute_name == 'Control type') {

                                    $controlType = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                if ($a->attribute_name == 'Control Position' || $a->attribute_name == 'Control position') {

                                    $controlPosition = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }
                                if ($a->attribute_name == 'Control Length' || $a->attribute_name == 'Control length') {

                                    $controlLength = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                //$room =  $value->room;
                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';
                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.' '.$i.@$its;

                        $this->br_code->gcode1($barcode_img_path);

                        $Shades[] = '<div class="table col-sm-4 mt-3">
                            <table class="table table-hover" style="width:76.2mm;height:38.1mm;font-size: 14px;border-spacing: 0px;">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">
                                        Product Name: ' . $value->product_name . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . '<br> size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </td>
                                        </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->room . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id="Shades' . $i . '" class="hidden table table-hover" style="width:76.2mm;height:38.1mm;font-size: 10px;border-spacing: 0px;">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">
                                        Product Name: ' . $value->product_name . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . '<br> size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </td>
                                        </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->room . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success" onclick="printContent(\'Shades' . $i . '\')" style="margin-top: 10px;">Print labels</button>
                            </div>
                            ';
                    }
                }


                $i++;
            }
        }

        return $arrayName = array('shades' => @$Shades, 'blinds' => @$blinds);
    }

    function allchit($orderd, $order_details) {
        $this->load->model('b_level/Manufactur_model');
        $blinds = [];
        $Shades = [];
        $selected_rooms = array();
        if (!empty($order_details)) {

            $i = 1;
            $its = '';
            foreach ($order_details as $key => $value) {
                $fraction_width_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->width_fraction_id)->get()->row();
                $fraction_height_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->height_fraction_id)->get()->row();

                $fraction_width = $fraction_width_data->fraction_value;
                $fraction_height = $fraction_height_data->fraction_value;
                $fraction_width_decimal = $fraction_width_data->decimal_value;
                $fraction_height_decimal = $fraction_height_data->decimal_value;


                if ($value->category_name == 'Blinds') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }

                        $MountData = '';
                        $tile = '';

                        if (!empty($attr)) {
                            foreach ($attr as $key => $att) {
                                //  print_r($order_details);
                                $pat = $this->db->where('pattern_model_id', $value->pattern_model_id)->get('pattern_model_tbl')->row();

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                                if ($a->attribute_name == "Mount") {

                                    $MountData = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }


                                if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {

                                    $tile = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                $room = $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';

                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.'-'.$i.@$its."#".$value->row_id;

                        $this->br_code->gcode1($barcode_img_path);

                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }
                        $blinds[] = '<table class="table" style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10px;border-spacing: 0px;">                          
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 2px 2px 2px 5px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;">
                                            Product Name: ' . $value->product_name .'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;">' . $pat->pattern_name . '<br>' . $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr style="height: 4mm;">
                                            <td style="padding: 2px 2px 2px 5px;border: 0px;">' . $vroom . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border: 0px;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                        ';
                    }
                }



                if ($value->category_name == 'Shades') {

                    $attr = json_decode($value->product_attribute);



                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }


                        $controlType = '';
                        $controlPosition = '';
                        $controlLength = '';



                        if (!empty($attr)) {

                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();



                                if ($a->attribute_name == 'Control Type' || $a->attribute_name == 'Control type') {

                                    $controlType = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                if ($a->attribute_name == 'Control Position' || $a->attribute_name == 'Control position') {

                                    $controlPosition = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }
                                if ($a->attribute_name == 'Control Length' || $a->attribute_name == 'Control length') {

                                    $controlLength = $this->Manufactur_model->get_name($att, $a->attribute_name);
                                }

                                //$room =  $value->room;
                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';
                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.' '.$i.@$its;

                        $this->br_code->gcode1($barcode_img_path);

                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }

                        $Shades[] = '<table class="table" style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10px;border-spacing: 0px;">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 2px 2px 2px 5px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">Company Name : ' . $orderd->company . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">
                                        Product Name: ' . $value->product_name . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                       <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . '<br> size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border: 0px;">' . $vroom . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border: 0px;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 2px 2px 2px 5px;border: 0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            ';
                    }
                }


                $i++;
            }
        }

        return $arrayName = array('shades' => @$Shades, 'blinds' => @$blinds);
    }

//    ============== its for manage_manufacturer ===================
    public function manage_manufacturer() {

        $this->permission->check_label(38)->create()->redirect();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manage_manufacturer');
        $this->load->view('b_level/footer');
    }

//    ============== its for manufacturer_edit ===================
    public function manufacturer_edit($id) {

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/manufacturer/manufacturer_edit');
        $this->load->view('b_level/footer');
    }

}
