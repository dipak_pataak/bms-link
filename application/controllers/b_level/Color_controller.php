<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Color_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
         $package=$this->session->userdata('packageid');
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');


        $this->load->model(array(
            'b_level/pattern_model', 'b_level/Settings', 'b_level/color_model'
        ));
    }

    public function index() {
        
    }

    public function add_color() {
        $this->permission->check_label(78)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(63);
        $config["base_url"] = base_url('b_level/Color_controller/add_color');
        
        $data["patterns"] = $this->db->where('created_by',$this->user_id)->get('pattern_model_tbl')->result();
        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/color/add_color', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        } 
    }

    function getColorLists(){
        $data = $row = array();
        
        // Fetch member's records
        $colors = $this->color_model->getRows($_POST);
        foreach($colors as $color){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$color->id.'" class="checkbox_list">';
            $package=$this->session->userdata('packageid');
            $menu_permission= b_access_role_permission(63);
            if (!empty($package)){
            if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){

            $action = '<a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="'. $color->id.'" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';
            }
            if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){
             $action .=' <a href="'.base_url('b_level/color_controller/delete_color/').$color->id.'" onclick="return confirm(\'Are you sure want to delete it\')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';

            }
         }
            $data[] = array($chk, $color->color_name, $color->color_number, $color->pattern_name,$color->category_name, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->color_model->countAll(),
            "recordsFiltered" => $this->color_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

    //=============== its for b_level_color_filter ==============
    public function b_level_color_filter() {
        if($this->input->post('Search')) {
            $this->session->set_userdata('search_colorname',$this->input->post('colorname'));
            $this->session->set_userdata('search_colornumber',$this->input->post('colornumber'));
            $this->session->set_userdata('search_pattern_name',$this->input->post('pattern_name'));
        } else {
            $this->session->unset_userdata('search_colorname');
            $this->session->unset_userdata('search_colornumber');
            $this->session->unset_userdata('search_pattern_name');
        }
        redirect('color');
    }

    //======== its for exists get_color check ===========
    public function get_color_check($color) {
//        echo $mobile_phone; die();
        $query = $this->db->where('color_name', $color)->or_where('color_number', $color)
                ->get('color_tbl')
                ->row();
        if (!empty($query)) {
            echo $query->color_name;
        } else {
            echo 0;
        }
    }

    public function save_color() {

        $pattern = $this->input->post('pattern_id');

        if (@$_FILES['color_img']['name']) {

            $config['upload_path'] = './assets/wholesaler/uploads/colors/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 2048;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_img')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("color");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            $upload_file = '';
        }

        foreach ($pattern as $pattern_id) {
            $colorData = array(
                'color_name' => $this->input->post('color_name'),
                'color_number' => $this->input->post('color_number'),
                'pattern_id' => $pattern_id,
                'color_img' => $upload_file,
                'created_by' => $this->session->userdata('user_id'),
                'created_date' => date('Y-m-d')
            );
            $this->db->insert('color_tbl', $colorData);
        }

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Color information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color save successfull </div>");
        redirect('color');
    }

    public function delete_color($id) {
        $this->db->select('colors');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($id, colors)");
        $check_colors = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_colors->num_rows() > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Can't delete it. Color already assigned!  </div>");
            redirect('color');
        } else {
            $this->db->where('id', $id)->delete('color_tbl');
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "deleted";
            $remarks = "Color information deleted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color deleted successfully! </div>");
            redirect('color');
        }
    }

    public function get_color($id) {
        $result = $this->db->where('id', $id)->get('color_tbl')->row();
        echo json_encode($result);
    }

    public function update_color() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Color information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        if (@$_FILES['color_img']['name']) {

            $config['upload_path'] = './assets/wholesaler/uploads/colors/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 2048;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_img')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("color");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            $upload_file = $this->input->post('hid_color_img');
        }

        $colorData = array(
            'pattern_id' => $this->input->post('edit_pattern_id')[0],
            'color_name' => $this->input->post('color_name'),
            'color_number' => $this->input->post('color_number'),
            'color_img' => $upload_file,
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d')
        );

        $id = $this->input->post('id');

        $this->db->where('id', $id)->update('color_tbl', $colorData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color update successfull </div>");
        redirect('color');
    }

//    =========== its for import_color_save ===========
    public function import_color_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "color csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    //$insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['color_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['color_number'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['pattern_name'] = (!empty($csv_line[2]) ? $csv_line[2] : 0);
                }
                $data = array(
                    'pattern_name' => $insert_csv['pattern_name'],
                    'color_name' => $insert_csv['color_name'],
                    'color_number' => $insert_csv['color_number'],
                    'created_by' => $this->user_id,
                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
                    $pattern_exist = $this->db->select('*')
                                ->from('pattern_model_tbl')
                                ->where('pattern_name', $data['pattern_name'])
                                ->where('created_by', $this->user_id)
                                ->get()
                                ->row_array();

                    
                    if(count($pattern_exist) > 0) {
                        $result = $this->db->select('*')
                            ->from('color_tbl')
                            ->where('color_name', $data['color_name'])
                            ->where('color_number', $data['color_number'])
                            ->where('pattern_id', $pattern_exist['pattern_model_id'])
                            ->where('created_by', $this->level_id)
                            ->get()
                            ->num_rows();
                            
                        $data_add = array(
                            'pattern_id' => $pattern_exist['pattern_model_id'],
                            'color_name' => $insert_csv['color_name'],
                            'color_number' => $insert_csv['color_number'],
                            'created_by' => $this->user_id,
                            'created_date' => date('Y-m-d'),
                        );

                        if ($result == 0 && !empty($data['color_name'])) {
                            $this->db->insert('color_tbl', $data_add);
                        }
                        /* else {
                            $data = array(
                                'color_name' => $insert_csv['color_name'],
                                'color_number' => $insert_csv['color_number'],
                                'pattern_id' => $pattern_exist['pattern_model_id'],
                                'updated_by' => $this->user_id,
                                'updated_date' => date('Y-m-d'),
                            );
                            $this->db->where('color_name', $data['color_name']);
                            $this->db->update('color_tbl', $data);
                        } */
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Color imported successfully!</div>");
        redirect('color');
    }

//    ========== its for color_filter ==================
    public function color_filter() {
        $data['colorname'] = $this->input->post('colorname');
        $data['colornumber'] = $this->input->post('colornumber');
        $data['get_color_filter'] = $this->Settings->color_filter($data['colorname'], $data['colornumber']);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/color/color_filter', $data);
        $this->load->view('b_level/footer');
    }

//    =============== b_level_pattern_search ============
    public function b_level_color_search() {
        $keyword = $this->input->post('keyword');
        $data['colors'] = $this->db->select('*')
                                    ->from('color_tbl')
                                    ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id = color_tbl.pattern_id', 'LEFT')
                                    ->where('color_tbl.created_by',$this->level_id)
                                    ->like('color_name', $keyword, 'both')
                                    ->or_like('color_number', $keyword, 'both')
                                    ->or_like('pattern_name', $keyword, 'both')
                                    ->order_by('id', 'desc')
                                    ->get()->result();
                                   // echo $this->db->last_query();die;
//        echo '<pre>';        print_r($data['get_menu_search_result']);die();
        $this->load->view('b_level/color/color_search', $data);
    }

    /** Start added by insys */
    //    =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('color_tbl','id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Color has been Deleted successfully.</div>");
        }
        redirect("color");
    } 
    /** End added by insys */

    public function export_color(){

        $color_data =   $this->db->select('co.color_name,co.color_number,pm.pattern_name,c.category_name')
                        ->from('color_tbl co')
                        ->join('pattern_model_tbl pm', 'pm.pattern_model_id = co.pattern_id', 'LEFT')
                        ->join('category_tbl c', 'pm.category_id = c.category_id', 'LEFT')
                        ->where('co.created_by',$this->level_id)
                        ->order_by('co.id', 'desc')
                        ->get()->result();
                
        if(count($color_data) > 0) {
            // file name 
            $filename = 'export_color.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            
            // file creation 
            $file = fopen('php://output', 'w');

            $header = array('color_name', 'color_number', 'pattern_name', 'category_name');
            fputcsv($file, $header);
            foreach ($color_data as $record) {
                $data = array($record->color_name,$record->color_number,$record->pattern_name,$record->category_name);
                fputcsv($file, $data);
            }
            fclose($file);
            exit;
        } else {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No Data Found</div>");
            redirect('color');
        }
    }
}
