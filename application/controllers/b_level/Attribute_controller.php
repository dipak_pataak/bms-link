<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $packageid = $this->session->userdata('packageid');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Attribute_model');
        $this->load->model('b_level/Category_model');
    }



    public function index() {

        //        $this->load->view('b_level/header');
        //        $this->load->view('b_level/sidebar');
        //        $this->load->view('b_level/customers/customer_edit');
        //        $this->load->view('b_level/footer');
    }



    //===============its for add_attribute ===============
    public function add_attribute() {
        $permission_accsess=b_access_role_permission_page(18);
        $this->permission->check_label(22)->create()->redirect();
        //$data['get_attribute_type'] = $this->Attribute_model->get_attribute_type();

        $data['categorys'] = $this->db->select('category_name,category_id')->where('parent_category', 0)->where('status', 1)->where('created_by',$this->level_id)->order_by('category_name','asc')->get('category_tbl')->result();
        if($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_add', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

    //========== its for save_attribute ===========
    public function save_attribute() {

        // echo "<pre>";print_r($_POST);
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "insert";
        $remarks = "attribute information save";
        
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks'       => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //  ============== close access log info =================

        $createdate = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'created_by' => $this->user_id,
            'created_date' => $createdate,
        );

        $this->db->insert('attribute_tbl', $attributeData);
        $attribute_id = $this->db->insert_id();

        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');

        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $op_op_price = $this->input->post('op_op_price');
        $op_op_price_type = $this->input->post('op_op_price_type');
        $op_op_condition = $this->input->post('op_op_condition');

        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');

        //print_r($op_op_op_name);
        //echo "<pre>";

        if (!empty($option_name)) {

            $k = 0;
            foreach ($option_name as $option) {
                
                // For attribute image upload : START
                if (@$_FILES['attr_img']['name'][$k]) {

                    $_FILES['final_file']['name']     = $_FILES['attr_img']['name'][$k];
                    $_FILES['final_file']['type']     = $_FILES['attr_img']['type'][$k];
                    $_FILES['final_file']['tmp_name'] = $_FILES['attr_img']['tmp_name'][$k];
                    $_FILES['final_file']['error']    = $_FILES['attr_img']['error'][$k];
                    $_FILES['final_file']['size']     = $_FILES['attr_img']['size'][$k];

                    $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                    $config['allowed_types'] = 'jpeg|jpg|png';
                    $config['overwrite'] = false;
                    $config['max_size'] = 2048;
                    $config['remove_spaces'] = true;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('final_file')) {
                        $upload_file = '';
                    } else {
                        $data = $this->upload->data();
                        $upload_file = $config['upload_path'] . $data['file_name'];
                    }
                } else {
                    $upload_file = '';
                }
                // For attribute image upload : END


                $optionData = array(
                    'option_name' => $option,
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k],
                    'attributes_images' => $upload_file,
                );

                //print_r($optionData); 

                $this->db->insert('attr_options', $optionData);
                $att_op_id = $this->db->insert_id();

                if (!empty($option_optin_name[$k])) {

                    $o = 0;
                    $oi = 0;

                    foreach ($option_optin_name[$k] as $option_option) {

                        // For sub attribute image upload : START
                        if (@$_FILES['op_op_attr_img']['name'][$k][$oi]) {

                            $_FILES['final_file']['name']     = $_FILES['op_op_attr_img']['name'][$k][$oi];
                            $_FILES['final_file']['type']     = $_FILES['op_op_attr_img']['type'][$k][$oi];
                            $_FILES['final_file']['tmp_name'] = $_FILES['op_op_attr_img']['tmp_name'][$k][$oi];
                            $_FILES['final_file']['error']    = $_FILES['op_op_attr_img']['error'][$k][$oi];
                            $_FILES['final_file']['size']     = $_FILES['op_op_attr_img']['size'][$k][$oi];

                            $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                            $config['allowed_types'] = 'jpeg|jpg|png';
                            $config['overwrite'] = false;
                            $config['max_size'] = 2048;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('final_file')) {
                                $upload_file = '';
                            } else {
                                $data = $this->upload->data();
                                $upload_file = $config['upload_path'] . $data['file_name'];
                            }
                        } else {
                            $upload_file = '';
                        }
                        // For sub attribute image upload : END

                        $optionOptionData = array(
                            'op_op_name' => $option_option,
                            'type' => @$option_option_type[$k][$oi],
                            'option_id' => @$att_op_id,
                            'attribute_id' => @$attribute_id,
                            'att_op_op_price_type' => @$op_op_price_type[$k][$oi],
                            'att_op_op_price' => @$op_op_price[$k][$oi],
                            'att_op_op_condition' => @$op_op_condition[$k][$oi],
                            'att_op_op_images' => $upload_file,
                        );

                        //print_r($optionOptionData);

                        if ($this->db->insert('attr_options_option_tbl', $optionOptionData)) {

                            $op_op_id = $this->db->insert_id();

                            // echo "<br>=======".$k."_".$o."=======<br>";
                            //print_r($op_op_op_name[1][3]);

                            if (!empty($op_op_op_name[$k][$o])) {

                                // print_r($op_op_op_name[$k][$o]);

                                $j = 0;

                                foreach ($op_op_op_name[$k][$o] as $op_op_op) {


                                    // For sub sub attribute image upload : START
                                    if (@$_FILES['op_op_op_attr_img']['name'][$k][$o][$j]) {

                                        $_FILES['final_file']['name']     = $_FILES['op_op_op_attr_img']['name'][$k][$o][$j];
                                        $_FILES['final_file']['type']     = $_FILES['op_op_op_attr_img']['type'][$k][$o][$j];
                                        $_FILES['final_file']['tmp_name'] = $_FILES['op_op_op_attr_img']['tmp_name'][$k][$o][$j];
                                        $_FILES['final_file']['error']    = $_FILES['op_op_op_attr_img']['error'][$k][$o][$j];
                                        $_FILES['final_file']['size']     = $_FILES['op_op_op_attr_img']['size'][$k][$o][$j];

                                        $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                                        $config['allowed_types'] = 'jpeg|jpg|png';
                                        $config['overwrite'] = false;
                                        $config['max_size'] = 2048;
                                        $config['remove_spaces'] = true;

                                        $this->load->library('upload', $config);
                                        $this->upload->initialize($config);

                                        if (!$this->upload->do_upload('final_file')) {
                                            $upload_file = '';
                                        } else {
                                            $data = $this->upload->data();
                                            $upload_file = $config['upload_path'] . $data['file_name'];
                                        }
                                    } else {
                                        $upload_file = '';
                                    }
                                    // For sub sub attribute image upload : END

                                    $OpOpOpData = array(
                                        'att_op_op_op_name' => $op_op_op,
                                        'att_op_op_op_type' => @$op_op_op_type[@$k][@$o][@$j],
                                        'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                        'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                        'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j],
                                        'op_id' => @$att_op_id,
                                        'att_op_op_id' => @$op_op_id,
                                        'attribute_id' => @$attribute_id,
                                        'att_op_op_op_images' => $upload_file
                                    );

                                    $j++;

                                    // print_r($OpOpOpData);

                                    $this->db->insert('attr_options_option_option_tbl', $OpOpOpData);

                                    // echo $this->db->last_query();die;
                                }
                            }
                        }

                        $oi++;
                        $o++;
                    }
                }

                $k++;
            }
            //exit;
        }
        // die;

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute save successfully!</div>");
        redirect("add-attribute");
    }

//  ===============its for manage_attribute ===============
    public function manage_attribute() {

        $this->permission->check_label(19)->read()->redirect();
        $permission_accsess=b_access_role_permission_page(19);
        $data['get_category'] = $this->Category_model->get_category();
        $data['js'] = "b_level/attributes/attribute_js";
        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_manage', $data);
        $this->load->view('b_level/footer');
         }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

     function getAttributeLists(){
        $data = $row = array();
        // Fetch member's records
        $attributes_data = $this->Attribute_model->getRows($_POST);
        $i = 0;
        foreach($attributes_data as $record){
            $i++;

            // Option Data : START
            if ($record->attribute_type) {
                $option = "SELECT * FROM attr_options WHERE attribute_id = '".$record->attribute_id."'";
                $option_result = $this->db->query($option)->result();
            }
            $option_field_data = '';
            if ($option_result != NULL) {
                foreach ($option_result as $key => $op) {
                    $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                    $option_option_result = $this->db->query($option_option)->result();

                    $option_field_data .= '<li>'.$op->option_name;
                    $option_field_data .= '<ul>';
                                    
                    foreach ($option_option_result as $key => $opop) {
                        $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();
                        
                        $option_field_data .= '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=>'.$opop->op_op_name;
                        $option_field_data .= '<ul>';
                                                
                        foreach ($opopops as $key => $opopop) {

                            $opopopops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();
                            
                            $option_field_data .= '<li>';
                            
                            if($opopop->att_op_op_op_type == 2) {

                                $option_field_data .= '<a href="javascript:void(0)" onclick="addOpOpOp('.$opopop->att_op_op_op_id . ')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=>'.@$opopop->att_op_op_op_name.'</a>';

                            } else {
                                                            
                                $option_field_data .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=>'.@$opopop->att_op_op_op_name; 

                            }

                            foreach ($opopopops as $key => $opopopop) { 
                                $option_field_data .= '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=>'.@$opopopop->att_op_op_op_op_name.' &nbsp;&nbsp;<i class="fa fa-remove text-danger" onclick="addOpOpOpOp('.$opopopop->att_op_op_op_op_id.')"></i></a></li>';
                            }
                            $option_field_data .= '</li>';
                        }
                        $option_field_data .= '</ul>';
                        $option_field_data .= '</li>';
                    }
                    $option_field_data .= '</ul>';
                    $option_field_data .= '</li>';
                }
            }

            // Option Data : END

            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$record->attribute_id.'" class="checkbox_list">';

            $action = '';
            $packageid = $this->session->userdata('packageid');
            if (!empty($packageid)){
              $menu_permission= b_access_role_permission(19);
            if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){ 
                $action .= '<a href="'.base_url('attribute-edit/'.$record->attribute_id).'" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;';
            }
            if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){
                $action .= '<a href="'.base_url('attribute-delete/'.$record->attribute_id).'" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm(\'Are you sure want to delete it\')"><i class="fa fa-trash"></i></a>';
            }
                
            }

            $data[] = array($chk, $record->position, $record->category_name, $record->attribute_name, $option_field_data, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Attribute_model->countAll(),
            "recordsFiltered" => $this->Attribute_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }


    //============= its for attribute_filter ============
    public function attribute_filter() {
        if($this->input->post('Search')) {
            $this->session->set_userdata('search_attribute_name',$this->input->post('attribute_name'));
            $this->session->set_userdata('search_category_id',$this->input->post('category_id'));
        } else {
            $this->session->unset_userdata('search_attribute_name');
            $this->session->unset_userdata('search_category_id');
        }
        redirect('manage-attribute');
    }

    public function option_option_option_option_save() {

        $option_name = $this->input->post('option_name');
        $att_op_op_op_type = $this->input->post('op_op_op_op_type');
        $att_op_op_op_price_type = $this->input->post('op_op_op_op_price_type');
        $att_op_op_op_price = $this->input->post('op_op_op_op_price');
        $att_op_op_op_condition = $this->input->post('op_op_op_op_condition');

        $op_op_op_id = $this->input->post('op_op_op_id');

        $data = $this->db->where('att_op_op_op_id', $op_op_op_id)->get('attr_options_option_option_tbl')->row();


        $saveData = [];
        foreach ($option_name as $key => $value) {
            
            // For attribute image upload : START
            if (@$_FILES['op_op_op_op_attr_img']['name'][$key]) {

                $_FILES['final_file']['name']     = $_FILES['op_op_op_op_attr_img']['name'][$key];
                $_FILES['final_file']['type']     = $_FILES['op_op_op_op_attr_img']['type'][$key];
                $_FILES['final_file']['tmp_name'] = $_FILES['op_op_op_op_attr_img']['tmp_name'][$key];
                $_FILES['final_file']['error']    = $_FILES['op_op_op_op_attr_img']['error'][$key];
                $_FILES['final_file']['size']     = $_FILES['op_op_op_op_attr_img']['size'][$key];

                $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['overwrite'] = false;
                $config['max_size'] = 2048;
                $config['remove_spaces'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('final_file')) {
                    $upload_file = '';
                } else {
                    $data = $this->upload->data();
                    $upload_file = $config['upload_path'] . $data['file_name'];
                }
            } else {
                $upload_file = '';
            }
            // For attribute image upload : END

            $saveData[] = array(
                'att_op_op_op_op_name' => $value,
                'att_op_op_op_op_type' => $att_op_op_op_type[$key],
                'att_op_op_op_op_price_type' => $att_op_op_op_price_type[$key],
                'att_op_op_op_op_price' => $att_op_op_op_price[$key],
                'att_op_op_op_op_condition' => $att_op_op_op_condition[$key],
                'op_id' => $data->op_id,
                'op_op_id' => $data->att_op_op_id,
                'op_op_op_id' => $op_op_op_id,
                'attribute_id' => $data->attribute_id,
                'att_op_op_op_op_images' => $upload_file,
            );
        }

        $this->db->insert_batch('attr_op_op_op_op_tbl', $saveData);

        echo 1;
    }

    public function option_option_option_option_delete($att_op_op_op_op_id) {

        $this->db->where('att_op_op_op_op_id', $att_op_op_op_op_id)->delete('attr_op_op_op_op_tbl');
        echo 1;
    }

    public function attribute_edit($id) {

        $this->permission->check_label('attribute')->update()->redirect();

        $data['attribute'] = $this->db->where('attribute_id', $id)->where('created_by',$this->level_id)->get('attribute_tbl')->row();
        $data['option'] = $this->db->where('attribute_id', $id)->get('attr_options')->result();
        // $data['categorys'] = $this->db->select('category_name,category_id')->where('created_by',$this->level_id)->order_by('category_name','asc')->get('category_tbl')->result();
        $data['categorys'] = $this->db->select('category_name,category_id')->where('parent_category', 0)->where('status', 1)->where('created_by',$this->level_id)->order_by('category_name','asc')->get('category_tbl')->result();

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_edit', $data);
        $this->load->view('b_level/footer');
    }

    public function remove_old_images($path){
        if(is_file($path)){
            unlink($path);
        }
    }

    // ========== its for attribute_update ===========
    public function attribute_update() {

        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "attribute information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'levle_id' => $this->levle_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $updated_date = date('Y-m-d');
        $attribute = $this->input->post('attribute');
        $attribute_type = $this->input->post('attribute_type');
        $category = $this->input->post('category');
        $position = $this->input->post('position');

        $attribute_id = $this->input->post('attribute_id');

        $attributeData = array(
            'attribute_name' => $attribute,
            'attribute_type' => $attribute_type,
            'category_id' => $category,
            'position' => $position,
            'updated_by' => $this->user_id,
            'updated_date' => $updated_date,
        );

        $this->db->where('attribute_id', $attribute_id)->update('attribute_tbl', $attributeData);


        $option_ids = $this->input->post('option_id');
        $option_name = $this->input->post('option_name');
        $option_type = $this->input->post('option_type');
        $price_type = $this->input->post('price_type');
        $price = $this->input->post('price');
        $condition = $this->input->post('condition');
        $hid_attr_img = $this->input->post('hid_attr_img');

        $op_op_ids = $this->input->post('op_op_id');
        $option_optin_name = $this->input->post('option_option_name');
        $option_option_type = $this->input->post('option_option_type');
        $att_op_op_price_type = $this->input->post('op_op_price_type');
        $att_op_op_price = $this->input->post('op_op_price');
        $att_op_op_condition = $this->input->post('op_op_condition');
        $hid_op_op_attr_img = $this->input->post('hid_op_op_attr_img');


        $op_op_op_ids = $this->input->post('op_op_op_id');
        $op_op_op_name = $this->input->post('op_op_op_name');
        $op_op_op_type = $this->input->post('op_op_op_type');
        $op_op_op_price_type = $this->input->post('op_op_op_price_type');
        $op_op_op_price = $this->input->post('op_op_op_price');
        $op_op_op_condition = $this->input->post('op_op_op_condition');
        $hid_op_op_op_attr_img = $this->input->post('hid_op_op_op_attr_img');

        $k = 0;
        for ($k = 0; $k <= count($option_ids); $k++) {
            if (array_key_exists($k, $option_ids)) {
                
                // For attribute image upload : START
                if (@$_FILES['attr_img']['name'][$k]) {

                    $_FILES['final_file']['name']     = $_FILES['attr_img']['name'][$k];
                    $_FILES['final_file']['type']     = $_FILES['attr_img']['type'][$k];
                    $_FILES['final_file']['tmp_name'] = $_FILES['attr_img']['tmp_name'][$k];
                    $_FILES['final_file']['error']    = $_FILES['attr_img']['error'][$k];
                    $_FILES['final_file']['size']     = $_FILES['attr_img']['size'][$k];

                    $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                    $config['allowed_types'] = 'jpeg|jpg|png';
                    $config['overwrite'] = false;
                    $config['max_size'] = 2048;
                    $config['remove_spaces'] = true;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('final_file')) {
                        $upload_file = '';
                    } else {
                        $data = $this->upload->data();
                        $upload_file = $config['upload_path'] . $data['file_name'];
                    }

                    $this->remove_old_images($hid_attr_img[$k]);
                } else {
                    $upload_file = $hid_attr_img[$k];
                }
                // For attribute image upload : END

                $optionData = array(
                    'option_name' => $option_name[$k],
                    'option_type' => $option_type[$k],
                    'attribute_id' => $attribute_id,
                    'price' => $price[$k],
                    'price_type' => $price_type[$k],
                    'op_condition' => $condition[$k],
                    'attributes_images' => $upload_file,
                );

                $this->db->where('att_op_id', $option_ids[$k])->update('attr_options', $optionData);

                if (!empty($op_op_ids[@$k])) {
                    $o = 0;
                    for ($o = 0; $o <= count($op_op_ids[$k]); $o++) {

                        if (array_key_exists($o, $op_op_ids[$k])) {

                            // For sub attribute image upload : START
                            if (@$_FILES['op_op_attr_img']['name'][$k][$o]) {

                                $_FILES['final_file']['name']     = $_FILES['op_op_attr_img']['name'][$k][$o];
                                $_FILES['final_file']['type']     = $_FILES['op_op_attr_img']['type'][$k][$o];
                                $_FILES['final_file']['tmp_name'] = $_FILES['op_op_attr_img']['tmp_name'][$k][$o];
                                $_FILES['final_file']['error']    = $_FILES['op_op_attr_img']['error'][$k][$o];
                                $_FILES['final_file']['size']     = $_FILES['op_op_attr_img']['size'][$k][$o];

                                $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                                $config['allowed_types'] = 'jpeg|jpg|png';
                                $config['overwrite'] = false;
                                $config['max_size'] = 2048;
                                $config['remove_spaces'] = true;

                                $this->load->library('upload', $config);
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('final_file')) {
                                    $upload_file = '';
                                } else {
                                    $data = $this->upload->data();
                                    $upload_file = $config['upload_path'] . $data['file_name'];
                                }

                                $this->remove_old_images($hid_op_op_attr_img[$k][$o]);
                            } else {
                                $upload_file = $hid_op_op_attr_img[$k][$o];
                            }
                            // For sub attribute image upload : END

                            $optionOptionData = array(
                                'op_op_name' => $option_optin_name[@$k][@$o],
                                'type' => $option_option_type[$k][$o],
                                'option_id' => $option_ids[$k],
                                'attribute_id' => $attribute_id,
                                'att_op_op_price_type' => $att_op_op_price_type[$k][$o],
                                'att_op_op_price' => $att_op_op_price[$k][$o],
                                'att_op_op_condition' => $att_op_op_condition[$k][$o],
                                'att_op_op_images' => $upload_file,
                            );
                            $this->db->where('op_op_id', $op_op_ids[$k][$o])->update('attr_options_option_tbl', $optionOptionData);

                            if (!empty($op_op_op_ids[@$k][$o])) {

                                for ($j = 0; $j <= count($op_op_op_ids[$k][$o]); $j++) {

                                    if (!empty($op_op_op_ids[$k][$o][$j])) {

                                        // For sub sub attribute image upload : START
                                        if (@$_FILES['op_op_op_attr_img']['name'][$k][$o][$j]) {

                                            $_FILES['final_file']['name']     = $_FILES['op_op_op_attr_img']['name'][$k][$o][$j];
                                            $_FILES['final_file']['type']     = $_FILES['op_op_op_attr_img']['type'][$k][$o][$j];
                                            $_FILES['final_file']['tmp_name'] = $_FILES['op_op_op_attr_img']['tmp_name'][$k][$o][$j];
                                            $_FILES['final_file']['error']    = $_FILES['op_op_op_attr_img']['error'][$k][$o][$j];
                                            $_FILES['final_file']['size']     = $_FILES['op_op_op_attr_img']['size'][$k][$o][$j];

                                            $config['upload_path'] = './assets/wholesaler/uploads/attributes_images/';
                                            $config['allowed_types'] = 'jpeg|jpg|png';
                                            $config['overwrite'] = false;
                                            $config['max_size'] = 2048;
                                            $config['remove_spaces'] = true;

                                            $this->load->library('upload', $config);
                                            $this->upload->initialize($config);

                                            if (!$this->upload->do_upload('final_file')) {
                                                $upload_file = '';
                                            } else {
                                                $data = $this->upload->data();
                                                $upload_file = $config['upload_path'] . $data['file_name'];
                                            }
                                            $this->remove_old_images($hid_op_op_op_attr_img[$k][$o][$j]);
                                        } else {
                                            $upload_file = $hid_op_op_op_attr_img[$k][$o][$j];
                                        }
                                        // For sub sub attribute image upload : END

                                        $optionOptionOptionData = array(
                                            'att_op_op_op_name' => $op_op_op_name[@$k][@$o][$j],
                                            'att_op_op_op_type' => $op_op_op_type[$k][$o][$j],
                                            'attribute_id' => $attribute_id,
                                            'op_id' => $option_ids[$k],
                                            'att_op_op_id' => $op_op_ids[$k][$o],
                                            'att_op_op_op_price_type' => $op_op_op_price_type[$k][$o][$j],
                                            'att_op_op_op_price' => $op_op_op_price[$k][$o][$j],
                                            'att_op_op_op_condition' => $op_op_op_condition[$k][$o][$j],
                                            'att_op_op_op_images' => $upload_file
                                        );

                                        $this->db->where('att_op_op_op_id', $op_op_op_ids[$k][$o][$j])->update('attr_options_option_option_tbl', $optionOptionOptionData);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute updated successfully!</div>");
        redirect("attribute-edit/" . $attribute_id);
    }

//    ============== its for attribute_type ===============
    public function attribute_type() {

        $config["base_url"] = base_url('b_level/Attribute_controller/attribute_type');
        $config["total_rows"] = $this->db->count_all('attribute_type_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["attribute_type_list"] = $this->Attribute_model->attribute_type_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_type_list', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for attribute_type_save ===============
    public function attribute_type_save() {

        $attribute_type_save = $this->input->post('types_name');
        $attribute_type_data = array(
            'attribute_type_name' => $attribute_type_save
        );
        $this->db->insert('attribute_type_tbl', $attribute_type_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute types save successfully!</div>");
        redirect('attribute-type');
    }

//    ============ its for uom_edit ==============
    public function attribute_type_edit($id) {
        $data['attribute_type_edit'] = $this->Attribute_model->attribute_type_edit($id);

        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/attributes/attribute_type_edit', $data);
        $this->load->view('b_level/footer');
    }

//    =========== its for uom_update ===============
    public function attribute_type_update($id) {
        $types_name = $this->input->post('types_name');
        $attribute_type_data = array(
            'attribute_type_name' => $types_name
        );
        $this->db->where('attribute_type_id', $id);
        $this->db->update('attribute_type_tbl', $attribute_type_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute types updated successfully!</div>");
        redirect('attribute-type');
    }



    public function get_attr_by_attr_types($type_id) {

        $query = $this->db->select("*")
                        ->from('attribute_tbl')
                        ->where('attribute_type_id', $type_id)
                        ->get()->result();

        echo '<table width="80%" class="border-0" border="0">';

        foreach ($query as $key => $value) {

            echo '<tr>
                        <td id="Prod_01" class="border-0"><input type="checkbox" class="checkboxes" name="attribute_id[]" value="' . $value->attribute_id . '" onChange="shuffle_pricebox(' . $value->attribute_id . ');"> </td>
                        <td class="border-0"><b>' . $value->attribute_name . '</b></td>
                        <td class="border-0"><input type="number" name="attribute_price_' . $value->attribute_id . '" id="' . $value->attribute_id . '" placeholder="$0.00" disabled></td>
                        <td class="border-0">
                            <div><input type="radio" name="price_type_' . $value->attribute_id . '" value="1" checked> $ &nbsp;
                            <input type="radio" name="price_type_' . $value->attribute_id . '" value="2"> %</div>
                        </td>
                    </tr>';
        }
        echo '</table>';
    }




    public function attribute_delete($attr_id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "attribute information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => @$this->user_id,
            'level_id' => @$this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $this->db->where('attribute_id', $attr_id)->delete('attribute_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_option_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute delete successfully!</div>");
        redirect('manage-attribute');
    }

    /** Start added by insys */
    // =========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('attribute_tbl','attribute_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Attribute has been Deleted successfully.</div>");
        }
        redirect("manage-attribute");
    } 

    // For delete main attribut option 
    function remove_main_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('op_id',$option_id);
        $this->remove_image_sub_option('option_id',$option_id);
        $this->remove_image_main_option('att_op_id',$option_id);
        // Remove image : END


        $this->db->where('op_id',$option_id)->delete('attr_options_option_option_tbl');
        $this->db->where('option_id',$option_id)->delete('attr_options_option_tbl');
        $this->db->where('att_op_id',$option_id)->delete('attr_options');

        return true;
    }

    function remove_sub_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('att_op_op_id',$option_id);
        $this->remove_image_sub_option('op_op_id',$option_id);
        // Remove image : END

        $this->db->where('att_op_op_id',$option_id)->delete('attr_options_option_option_tbl');
        $this->db->where('op_op_id',$option_id)->delete('attr_options_option_tbl');
        return true;
    }

    function remove_sub_sub_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('att_op_op_op_id',$option_id);
        // Remove image : END

        $this->db->where('att_op_op_op_id',$option_id)->delete('attr_options_option_option_tbl');
        return true;
    }


    function remove_image_main_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options')->result_array();
        foreach ($data as $key => $value) {
            if($value['attributes_images'] != ''){
                $this->remove_old_images($value['attributes_images']);
            }
        }
    }

    function remove_image_sub_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options_option_tbl')->result_array();
        foreach ($data as $key => $value) {
            if($value['att_op_op_images'] != ''){
                $this->remove_old_images($value['att_op_op_images']);
            }
        }
    }

    function remove_image_sub_sub_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options_option_option_tbl')->result_array();
        foreach ($data as $key => $value) {
            if($value['att_op_op_op_images'] != ''){
                $this->remove_old_images($value['att_op_op_op_images']);
            }
        }
    }

    /** End added by insys */
}
