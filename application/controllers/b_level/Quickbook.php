<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(__DIR__ . '/vendor_cust/autoload.php');

/**
 * QB OAuth Endpoint
 * 
 * Quickbooks Online OAuth2 authorization endpoint.
 * 
 * @author Pavel Espinal
 */
class Quickbook extends CI_Controller {

    /**
     * QB OAuth Endpoint
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * QB Oauth Endpoint start
     * 
     * @throws Exception
     */
    public function index() {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/quickbook/quickbook');
        $this->load->view('b_level/footer');
    }

}
