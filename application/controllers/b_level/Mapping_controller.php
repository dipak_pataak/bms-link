<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mapping_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('b_level/Mapping_model','mapping');
    }

    public function index() {

        $data['unit_data'] = $this->mapping->get_unit_data();
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/mapping/mapping_manage',$data);
        $this->load->view('b_level/footer');
    }

    public function save_mapping(){
        $mapping_id = $this->input->post('mapping_id');
        $from_measure_id = $this->input->post('from_measure_id');
        $to_measure_id = $this->input->post('to_measure_id');
        $measure_value = $this->input->post('measure_value');

        foreach ($measure_value as $k => $measure) {
            if($mapping_id[$k] == ''){
                $measure_data = array(
                    'from_measure_id' => $from_measure_id[$k],
                    'to_measure_id' => $to_measure_id[$k],
                    'measure_value' => $measure_value[$k],
                    'user_id' => $this->session->userdata('user_id'),
                ); 
                $this->db->insert('mapping_measurement', $measure_data);
            }else{
                $measure_data = array(
                    'measure_value' => $measure_value[$k],
                ); 
                $this->db->where('id', $mapping_id[$k]);
                $this->db->update('mapping_measurement', $measure_data);
            }    
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping updated successfully!</div>");
        redirect("mapping");
    }
}    

