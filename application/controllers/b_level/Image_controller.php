<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Image_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');
         $packageid = $this->session->userdata('packageid');

        $this->load->model(array(
            'b_level/pattern_model', 'b_level/Settings'
        ));
    }

    public function index() {
        
    }

    public function add_image() {
        $this->permission->check_label(143)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(119);
        $config["base_url"] = base_url('b_level/Image_controller/add_image');        
        $data["tags"] = $this->db->from('tag_tbl')->where('created_by', $this->level_id)->order_by('id','asc')->get()->result();
        if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/add_image', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }


    public function save_image() {
        $this->permission->check_label(143)->create()->redirect();
        //echo "<pre>";print_r($_FILES);die;

        $user_folder = $this->level_id;
        $final_path = "./assets/b_level/uploads/gallery/".$user_folder;
        // Check directory exist or not : START
        if (!is_dir($final_path)) {
            mkdir($final_path, 0777, TRUE);
        }
        // Check directory exist or not : END

        $upload_file_arr = [];

        // File Upload Setting : START
        $files = $_FILES;
        $config['upload_path'] = $final_path.'/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['overwrite'] = false;
        $config['max_size'] = 0;
        $config['remove_spaces'] = true;
        $this->load->library('upload', $config);
        // File Upload Setting : END

        $cpt = count($_FILES['file_upload']['name']);
        for($i=0; $i<$cpt; $i++){
            if ($files['file_upload']['name'][$i] != '') {
                $_FILES['file_upload']['name']= $files['file_upload']['name'][$i];
                $_FILES['file_upload']['type']= $files['file_upload']['type'][$i];
                $_FILES['file_upload']['tmp_name']= $files['file_upload']['tmp_name'][$i];
                $_FILES['file_upload']['error']= $files['file_upload']['error'][$i];
                $_FILES['file_upload']['size']= $files['file_upload']['size'][$i]; 
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('file_upload')) {
                    $error = $this->upload->display_errors();
                } else {
                    $data = $this->upload->data();
                    $upload_file_arr[] = $config['upload_path'] . $data['file_name'];
                }
            } else {
                $error = "Something went to wrong. Please try again";
            }
        }
        if(count($upload_file_arr) == 0){
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
            redirect("add_wholesaler_gallery_image");
        }
        

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $success_image = '';
        foreach ($upload_file_arr as $k => $upload_file) {
            $imageData = array(
                'image' => $upload_file,
                'created_by'  => $this->level_id
            );

            if ($this->db->insert('gallery_image_tbl', $imageData)) {
                $image_id = $this->db->insert_id();
                $tagdata = $this->db->from('tag_tbl')->where('created_by', $this->level_id)->order_by('id','asc')->get()->result_array();
                foreach ($tagdata as $tag) {
                    $tag_id = $tag['id'];
                    $tag_value = $this->input->post('tag_'.$tag['id']);

                    if($tag_value != '') {
                        $galleryTagData = array(
                            'image_id' => $image_id,
                            'tag_id' => $tag_id,
                            'tag_value' => $tag_value,                    
                            'created_date' => date('Y-m-d'),
                            'updated_date' => '',
		            'updated_by' => $this->user_id,
                            'created_by' => $level_id
                        );
                        $this->db->insert('gallery_tag_tbl', $galleryTagData);
                    }
                }
                $success_image = 1;
            } 
        }

        if($success_image == 1){
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Image added successfully! </div>");
        }else{
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
        }
        
        redirect("manage_wholesaler_gallery_image");
    }

    public function manage_image() {

        $this->permission->check_label(143)->create()->redirect();
        $permission_accsess=b_access_role_permission_page(120);
        $config["base_url"] = base_url('manage_wholesaler_gallery_image');
        $config["total_rows"] = $this->db->where('created_by', $this->level_id)->from('gallery_image_tbl')->count_all_results();
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $tags = $this->db->where('created_by', $this->level_id)->order_by('id')->get('tag_tbl')->result_array();

        $filter_arr = array();
        $a = 0 ;
        $final_img_ids = array();
        $is_filter = $is_valid = 0;
        foreach($tags as $key => $tag) {
            $filter_val = $this->input->post('tag_'.$tag['id']);
            if($filter_val != '') {
                $is_filter = 1;
                $image_ids = $this->db->select('image_id')
                        ->where('tag_id', $tag['id'])
                        ->where('tag_value', $filter_val)
                        ->where('created_by',$this->level_id)
                        ->get('gallery_tag_tbl')
                        ->result_array();
                if(count($image_ids) > 0) {
                    $ids = array_column($image_ids, 'image_id');
                    if(count($final_img_ids) > 0 || $is_valid == 1) {
                        $final_img_ids = array_intersect ($final_img_ids, $ids);
                        $is_valid = 1;
                    } else {
                        $final_img_ids = $ids;
                    }
                }
            }
            $tag_value = $this->db->select('DISTINCT(tag_value)')
                            ->where('tag_id', $tag['id'])
                            ->where('created_by',$this->level_id)
                            ->get('gallery_tag_tbl')
                            ->result_array();

            if(count($tag_value) > 0) {
                $filter_arr[$a]['value']= $tag_value;
                $filter_arr[$a]['tag_name'] = $tag['tag_name'];
                $filter_arr[$a]['tag_id'] = $tag['id'];
                $filter_arr[$a]['selected_val'] = $filter_val;
                $a++;
            }
            
        }


        $data['filter_arr'] = $filter_arr;
        if(count($final_img_ids) > 0) {
            $this->db->where_in('id', $final_img_ids);
        } else if($is_filter == 1){
            $this->db->where('1', '0');
        }
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["images"] = $this->db->from('gallery_image_tbl')
                            ->where('created_by', $this->level_id)
                            ->order_by('id', 'desc')
                            ->where('created_by',$this->level_id)
                            ->limit($config["per_page"], $page)
                            ->get()
                            ->result();

        $data["links"] = $this->pagination->create_links();
        $data["patterns"] = $this->db->get('pattern_model_tbl')->result();
        $data['pagenum'] = $page;

         if ($permission_accsess==1) {
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/manage_image', $data);
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

    public function delete_image($id) {
       
        // Get gallery data : START
        $gallery_data = $this->db->select('*')->from('gallery_image_tbl')->where_in('id', $id)->where('created_by', $this->level_id)->get()->result();

        foreach ($gallery_data as $key => $gallery) {
            // Remove image from folder once we delete record from table : START
            $img_path = FCPATH . $gallery->image;
            if(file_exists($img_path)){
                unlink($img_path);
            }
            // Remove image from folder once we delete record from table : END
        }
        // Get gallery data : END

        $this->db->where('id', $id)->where('created_by', $this->level_id)->delete('gallery_image_tbl');

        $this->db->where('image_id', $id)->where('created_by', $this->level_id)->delete('gallery_tag_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "Tag information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                image deleted successfully! </div>");
        redirect('manage_wholesaler_gallery_image');
    }

    public function manage_action()
    {
        // echo "<pre>";print_r($_POST);die;
        if ($this->input->post('action') == 'action_delete') {

            $gallery_img_ids = $this->input->post('gallery_img_id');
            if (count($gallery_img_ids) > 0) {
                
                // Get gallery data : START
                $gallery_data = $this->db->select('*')->from('gallery_image_tbl')->where_in('id', $gallery_img_ids)->where('created_by', $this->level_id)->get()->result();
                
                foreach ($gallery_data as $key => $gallery) {
                    // Remove image from folder once we delete record from table : START
                    $img_path = FCPATH . $gallery->image;
                    if(file_exists($img_path)){
                        unlink($img_path);
                    }
                    // Remove image from folder once we delete record from table : END
                }
                // Get gallery data : END

                // Delete data from gallery table : START
                $this->db->where_in('id', $gallery_img_ids);
                $this->db->where('created_by', $this->level_id);
                $query = $this->db->delete('gallery_image_tbl');
                // Delete data from gallery table : END

                // Delete data from gallery tag table : START
                $this->db->where_in('image_id', $gallery_img_ids)->where('created_by', $this->level_id)->delete('gallery_tag_tbl');
                // Delete data from gallery tag table : END

                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected images has been Deleted successfully.</div>");
            }
        }
        redirect("manage_wholesaler_gallery_image");
    }

    public function get_image($id) {
        $tag_data = $this->db->select('gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value')
                                ->from('gallery_tag_tbl')
                                ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id')
                                ->where('image_id',$id)
                                ->where('tag_tbl.created_by', $this->level_id)
                                ->get()->result_array();
                                
        $data['tag_data'] = array();
        if(count($tag_data) > 0) {
            foreach($tag_data as $row) {
                $data['tag_ids'][] = $row['tag_id'];
                $data['tag_data'][$row['tag_id']] = $row['tag_value'];
            }
        }

        $data['images_tag_data'] = $this->db->select('gallery_image_tbl.*, gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value, tag_tbl.tag_name')->from('gallery_image_tbl')
                                    ->join('gallery_tag_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
                                    ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
                                    ->where('gallery_image_tbl.id',$id)
                                    ->where('gallery_image_tbl.created_by', $this->level_id)
                                    ->get()
                                    ->row();;
        /* $data['images_tag_data'] = $this->db->from('gallery_tag_tbl')
                                        ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
                                        ->join('gallery_image_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
                                        ->where('image_id',$id)
                                        ->where('tag_tbl.created_by', $this->level_id)
                                        ->where('gallery_image_tbl.created_by', $this->level_id)
                                        ->get()
                                        ->row(); */
        $data['image_id'] = $id;
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/edit_image', $data);
        $this->load->view('b_level/footer');

    }
    public function view_image($id) {
        $tag_data = $this->db->select('gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value')
            ->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->where('image_id',$id)
            ->where('tag_tbl.created_by', $this->level_id)
            ->get()->result_array();

        $data['tag_data'] = array();
        if(count($tag_data) > 0) {
            foreach($tag_data as $row) {
                $data['tag_ids'][] = $row['tag_id'];
                $data['tag_data'][$row['tag_id']] = $row['tag_value'];
            }
        }
        $data['images_tag_data'] = $this->db->select('gallery_image_tbl.*, gallery_tag_tbl.tag_id, gallery_tag_tbl.tag_value, tag_tbl.tag_name')->from('gallery_image_tbl')
            ->join('gallery_tag_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->where('gallery_image_tbl.id',$id)
            //->where('tag_tbl.created_by', $this->level_id)
            ->where('gallery_image_tbl.created_by', $this->level_id)
            ->get()
            ->row();
        /* $data['images_tag_data'] = $this->db->from('gallery_tag_tbl')
            ->join('tag_tbl', 'tag_tbl.id =gallery_tag_tbl.tag_id', 'left')
            ->join('gallery_image_tbl', 'gallery_image_tbl.id =gallery_tag_tbl.image_id', 'left')
            ->where('image_id',$id)
            ->where('tag_tbl.created_by', $this->level_id)
            ->where('gallery_image_tbl.created_by', $this->level_id)
            ->get()
            ->row(); */
          
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/image/view_image', $data);
        $this->load->view('b_level/footer');

    }

    public function update_image($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Tag information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        
        $user_folder = $this->level_id;
        $final_path = "./assets/b_level/uploads/gallery/".$user_folder;
        // Check directory exist or not : START
        if (!is_dir($final_path)) {
            mkdir($final_path, 0777, TRUE);
        }
        // Check directory exist or not : END


        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = $final_path.'/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {

            @$upload_file = $this->input->post('hidden_image');
        }

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        $imageData = array(
            'image' => $upload_file
        );

        $this->db->where('id', $id)->where('gallery_image_tbl.created_by', $this->level_id)->update('gallery_image_tbl', $imageData);

        $tagdata = $this->db->from('tag_tbl')
                         ->where('tag_tbl.created_by', $this->level_id)
                         ->order_by('id','asc')
                        ->get()
                        ->result_array();
        foreach ($tagdata as $tag) {
            $tag_value = $this->input->post('tag_'.$tag['id']);
            
            $record_exist = $this->db->from('gallery_tag_tbl')
                        ->where('image_id', $id)
                        ->where('tag_id', $tag['id'])
                        ->get()
                        ->row_array();
            $galleryTagData = [];
            if(!$record_exist) {
                if($tag_value != '') {
                    $galleryTagData = array(
                        'image_id' => $id,
                        'tag_id' => $tag['id'],
                        'tag_value' => $tag_value,
                        'created_by' => $this->level_id,                          
                        'created_date' => date('Y-m-d')
                    );
                    $this->db->insert('gallery_tag_tbl', $galleryTagData);
                }
            } else {
                $galleryTagData = array(
                    'tag_value' => $tag_value,   
                    'updated_by' => $this->level_id,                
                    'updated_date' => date('Y-m-d')
                );
                $this->db->where('image_id', $id)
                    ->where('tag_id', $tag['id'])
                    ->update('gallery_tag_tbl', $galleryTagData);
                $this->db->where('tag_value', '')->delete('gallery_tag_tbl');
            }
        }
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Image update successfully </div>");
        redirect('manage_wholesaler_gallery_image');
    }

    public function export_gallery_image(){
        
        $gallery_data = $this->db->where('created_by', $this->level_id)->order_by('id','asc')->get('gallery_image_tbl')->result();

        $tag_data = $this->db->where('created_by', $this->level_id)->order_by('id','asc')->get('tag_tbl')->result();

        $tag_arr = [];
        foreach ($tag_data as $key => $tag) {
            $tag_arr[] = $tag->tag_name;
        }

        $filename = 'gallery_data.csv';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/csv; ");
 
        // file creation
        $file = fopen('php://output', 'w');
 
        $header = array("gallery_id","url");
        $header = array_merge($header,$tag_arr);
        fputcsv($file, $header);
 
        foreach ($gallery_data as $key => $gallery) {

            $tag_info = [];
            foreach ($tag_data as $key => $tag) {
                $gallery_tag_data = $this->db->from('gallery_tag_tbl')
                        ->where('image_id', $gallery->id)
                        ->where('tag_id', $tag->id)
                        ->where('created_by', $this->level_id)
                        ->get()
                        ->row();       

                if(isset($gallery_tag_data->id)){
                    $tag_info[] = $gallery_tag_data->tag_value;
                }else{
                    $tag_info[] = '';
                }
            }
            $final_arr = [];
            $final_arr[0] = $gallery->id;
            $final_arr[1] = $gallery->image;
            $final_arr = array_merge($final_arr,$tag_info);
            fputcsv($file,$final_arr);
        }
        fclose($file);
        exit;
    }

    //=========== its for import_image_tag ===========
    public function import_image_tag_save() {
       
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        $valid_csv_format = 'valid';
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            $tag_data = $this->db->where('created_by', $this->level_id)->order_by('id','asc')->get('tag_tbl')->result();

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['gallery_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['url'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                }

                // For check csv format valid or not : START
                if($count == 0){
                    foreach ($tag_data as $key => $tag) {    
                        $tag_key = 2 + $key;
                        $tag_value = (!empty($csv_line[$tag_key]) ? $csv_line[$tag_key] : '');
                        if($tag->tag_name != $tag_value){
                            $valid_csv_format = 'not_valid';
                        }
                    }
                }
                // For check csv format valid or not : END

                if($valid_csv_format == 'valid'){
                    if ($count > 0) {
                        
                        // Get gallery data based on id : START
                        $gallery_data = $this->db->where('created_by', $this->level_id)->where('id',$insert_csv['gallery_id'])->get('gallery_image_tbl')->result();
                        // Get gallery data based on id : END

                        if(count($gallery_data) > 0){
                            foreach ($tag_data as $key => $tag) {
                            
                                // check gallery tag data exist : START
                                $gallery_tag_data = $this->db->from('gallery_tag_tbl')
                                        ->where('image_id', $insert_csv['gallery_id'])
                                        ->where('tag_id', $tag->id)
                                        ->where('created_by', $this->level_id)
                                        ->get()
                                        ->row();     
                                // check gallery tag data exist : END  

                                $tag_key = 2 + $key;        
                                $tag_value = (!empty($csv_line[$tag_key]) ? $csv_line[$tag_key] : '');

                                if(isset($gallery_tag_data->id)){
                                    // Update gallery tag data : START
                                    $data = array(
                                        'tag_value' => $tag_value,
                                        'updated_by' => $this->level_id,
                                        'updated_date' => date('Y-m-d'),
                                    );
                                    $this->db->where('id', $gallery_tag_data->id);
                                    $this->db->update('gallery_tag_tbl', $data);
                                    // Update gallery tag data : END
                                }else{
                                    if($tag_value != ''){
                                        // Insert gallery tag data : START
                                        $data = array(
                                            'image_id' => $insert_csv['gallery_id'],
                                            'tag_id' => $tag->id,
                                            'tag_value' => $tag_value,
                                            'created_date' => date('Y-m-d'),
                                            'created_by' => $this->level_id
                                        );
                                        $this->db->insert('gallery_tag_tbl', $data);
                                        // Insert gallery tag data : END
                                    }    
                                }
                            }
                        }    
                    }
                }else{
                     $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please upload valid csv file.</div>");
                    redirect('manage_wholesaler_gallery_image');
                }    
                $count++;
            }
        }

        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Image Tag imported successfully!</div>");
        redirect('manage_wholesaler_gallery_image');
    }
}
