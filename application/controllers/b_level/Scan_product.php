<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scan_product extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Kolkata');
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
//        echo $session_id . " ". $user_type;//exit();
        $admin_created_by = $this->session->userdata('admin_created_by');
        if ($session_id == '' || $user_type != 'b') {
            redirect('wholesaler-logout');
        }
        $this->load->model('b_level/Auth_model');
        $this->load->model('b_level/settings');
        $this->load->model('b_level/role_model');
    }

    public function index() {
        $permission_accsess=b_access_role_permission_page(129);
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        if($permission_accsess==1) {
        $this->load->view('b_level/header', $data);
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/scan_product');
        $this->load->view('b_level/footer');
        }else{
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar'); 
        $this->load->view('b_level/upgrade_error'); 
        $this->load->view('b_level/footer');
        }
    }

    public function get_order_product_data()
    {
        $post_orderIds   = $this->input->post('orderId');
        $orderIds        = rtrim($post_orderIds, ',');
        $orderIds_array  = explode(',', $orderIds);
        $data['data']    = $this->all_order_data($orderIds_array);
        echo json_encode($data);
    }

    public function update_status($orderids = 0)
    {
        $post_orderRowIds  = $this->input->post('orderRowIds');
        $orderRowIds_array = explode(',', $post_orderRowIds);

        $count_arr = array();
        $count_total = array();

        foreach ($orderRowIds_array as $key => $rowId) {

            $rowIdData = $this->get_rowid_data($rowId);
            $ord_id = $rowIdData->order_id;

            $count = $this->get_orderid_data_count($ord_id);

            foreach ($count as $co_key => $co_value) {
                array_push($count_arr, $co_value['order_id']);
                array_push($count_total, $co_value['total']);
            }

            $order_id_unique = array_unique($count_arr);
            $unique_total = array_unique($count_total);
        }
        $ord_array = array();
        foreach ($order_id_unique as $uid_key => $u_id) {
            $status_manufacture_count = $this->get_manufacture_status_count($u_id);
            $status_shipping_count = $this->get_shipping_status_count($u_id);

            foreach ($unique_total as $utotal_key => $u_total) {
                if($status_manufacture_count == 0){
                    array_push($ord_array, $u_id);
                    $order_unique_data = array(
                        'order_stage'      => 8,
                        "manufacture_order_date"  => date('Y-m-d h:i:s')
                    );
                    $this->change_order_status($order_unique_data,$u_id);
                }

                if($status_shipping_count == 0){
                    $order_unique_data = array(
                        'order_stage'      => 9,
                        "ship_order_date"  => date('Y-m-d h:i:s')
                    );
                    $this->change_order_status($order_unique_data,$u_id);
                }
            } 
        }

        $message_string = " <div class='alert alert-success'> <button type='button' class='close order-list-sucess-msg' data-dismiss='alert' aria-hidden='true'>×</button>Scanned item updated successfully.</div>";

        $this->session->set_flashdata('message',$message_string);
    }

    public function all_order_data($orderids)
    {
        $query = $this->db->select('b_level_qutation_details.*,product_tbl.product_name,color_tbl.color_name,b_level_quatation_tbl.side_mark')
                          ->from('b_level_qutation_details')
                          ->join('b_level_quatation_tbl','b_level_quatation_tbl.order_id = b_level_qutation_details.order_id','left')
                          ->join('product_tbl','product_tbl.product_id = b_level_qutation_details.product_id','left')
                          ->join('color_tbl','color_tbl.id = b_level_qutation_details.color_id','left')
                          // ->where('b_level_quatation_tbl.created_by',$this->session->userdata('user_id'))
                          ->where_in('b_level_qutation_details.row_id',$orderids)
                          ->get()->result();
                          
        // echo $this->db->last_query();die;

        foreach ($query as $key => $value) {
            $value->key = $key+1;
        }

        return $query;
    }

    public function get_rowid_data($rowId)
    {
        $query = $this->db->select('*')
                          ->from('b_level_qutation_details')
                          ->where('row_id', $rowId)
                          ->get()->row();
        return $query;
    }

    public function change_status($data = array(),$id)
    {
        $this->db->where('row_id',$id);
        return $this->db->update('b_level_qutation_details',$data);
    }

    public function get_orderid_data_count($id)
    {
        $this->db->select('order_id, COUNT(order_id) as total');
        $this->db->group_by('order_id');  
        $this->db->where('order_id', $id);
        return $this->db->get('b_level_qutation_details')->result_array();
    }

    public function get_manufacture_status_count($id)
    {
        $query = $this->db->select('order_id')
                          ->from('b_level_qutation_details')
                          ->where('manufactured_scan_date',NULL)
                          ->where('order_id', $id)
                          ->get()->num_rows();
        return $query;
    }

    public function get_shipping_status_count($id)
    {
        $query = $this->db->select('order_id')
                          ->from('b_level_qutation_details')
                          ->where('shipping_scan_date',NULL)
                          ->where('order_id', $id)
                          ->get()->num_rows();
        return $query;
    }

    public function change_order_status($data = array(),$oid)
    {
        $this->db->where('order_id',$oid);
        $this->db->update('b_level_quatation_tbl',$data);
    }

    public function change_manufacture_date_status($data = array(),$id)
    {
        $this->db->where('row_id',$id);
        return $this->db->update('b_level_qutation_details',$data);
    }
    public function update_manufacture_date()
    {
        $user_id = $this->session->userdata('user_id');
        
        $rolData = $this->get_userRoleDetails($user_id);

        // if($rolData->is_admin == 1)
        // {
            $userRole = $this->role_model->UserRoleById($user_id);

            $userManufacturingRoleDetail    = $this->get_userRole_permission($userRole->role_id, MANUFACTURING);
            $userShippingRoleDetail         = $this->get_userRole_permission($userRole->role_id, SHIPPING);

            $rowIdDetailData = $this->get_rowid_data($this->input->post('manFactId'));
            $rowIdDetailDeleteData = $this->get_rowid_data($this->input->post('manFactIdNull'));

            if($this->input->post('manFactIdNull'))
            {
                if($rowIdDetailDeleteData->manufactured_scan_date != NULL && $rowIdDetailDeleteData->shipping_scan_date != NULL)
                {
                    $post_manFactIdNull  = $this->input->post('manFactIdNull');
                    $data = array(
                        "status"              => 'scanned',
                        "shipping_scan_date"  => NULL,
                        "shipping_scan_by"    => NULL
                    );
                    $this->change_manufacture_date_status($data,$post_manFactIdNull);
                }
                else
                {       
                    $post_manFactIdNull  = $this->input->post('manFactIdNull');
                    $data = array(
                        "status"                  => NULL,
                        "manufactured_scan_date"  => NULL,
                        "manufactured_scan_by"    => NULL
                    );
                    $this->change_manufacture_date_status($data,$post_manFactIdNull);
                }
            }


            if($rowIdDetailData->manufactured_scan_date == NULL)
            {
                if($userManufacturingRoleDetail->can_access == 1 || $rolData->is_admin == 1)
                {
                    if($this->input->post('manFactId'))
                    {
                        $post_manFactId  = $this->input->post('manFactId');
                        $data = array(
                            "status"                  => "scanned",
                            "manufactured_scan_date"  => date('Y-m-d h:i:s'),
                            "manufactured_scan_by"    => $this->session->userdata('user_id')
                        );
                        $this->change_manufacture_date_status($data,$post_manFactId);
                    }
                }
                else
                {
                    $data['error'] = "Sorry! - You are not Admin or don't have Manufacturing permission..!!";
                    echo json_encode($data['error']);
                }
            }
            else
            {
                if($userShippingRoleDetail->can_access == 1 || $rolData->is_admin == 1)
                {
                    if($this->input->post('manFactId'))
                    {
                        $post_manFactId  = $this->input->post('manFactId');
                        $data = array(
                            "status"              => "In Transit",
                            "shipping_scan_date"  => date('Y-m-d h:i:s'),
                            "shipping_scan_by"    => $this->session->userdata('user_id')
                        );

                        $this->change_manufacture_date_status($data,$post_manFactId);
                    }
                }
                else
                {
                    $data['error'] = "Sorry! - You are not Admin or don't have Shippping permission..!!";
                    echo json_encode($data['error']);
                }
            }
        // }
        // else
        // {
        //     $data['error'] = "Sorry! - You are not Admin..!!";
        //     echo json_encode($data['error']);
        // }
    }
    public function get_userRole_permission($rid, $mid)
    {
        $query = $this->db->select('*')
                          ->from('b_role_permission_tbl')
                          ->where('role_id',$rid)
                          ->where('menu_id',$mid)
                          ->get()->row();
        return $query;
    }

    public function get_userRoleDetails($uid)
    {
        $query = $this->db->select('*')
                          ->from('log_info')
                          ->where('user_id',$uid)
                          ->get()->row();
        return $query;
    }

}
