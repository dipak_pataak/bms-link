<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Front_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('c_level/setting_model');
        $this->load->model('b_level/settings');
    }

    public function index($b_user_id = 0 ,$customer_id = 0) {

         $this->load->model('front_section/User_model');
    
            $data = [];
        if(!empty($b_user_id))
        {
            $userData     = $this->db->where('id', $b_user_id)->get('user_info')->row();
            $data['data'] = $userData;



        }
        $data['b_user_list'] = $this->db->query('SELECT * FROM user_info where id != ' . $b_user_id . ' AND user_type_status = 1 AND user_type = "b" AND ( bridge_b IS NULL OR bridge_b = ' . $b_user_id . ')')->result();
      


        $data['b_user_data'] = $this->User_model->get_b_user_list();
        $data['package_list'] = $this->User_model->get_package_list();
        if (!empty($customer_id)) {
            $data['data'] = $this->User_model->get_single_customer_by_id($customer_id);
            $data['user_info_detail'] = $this->db->where('id', $data['data']->customer_user_id)->get('user_info')->row();
        }


        $this->load->view('front_section/front',$data);     
    }


    public function wholesaler_user_save(){

        //  echo "<pre>"; print_r($_POST);die();
        $this->load->model('front_section/User_model');
        $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
        $this->form_validation->set_rules('sub_domain', 'Sub Domain', 'required');

        if(!isset($_POST['id']) || empty(trim($_POST['id'])))
        {

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        }
        $this->form_validation->set_rules('address', 'Address', 'max_length[1000]');
        if($this->form_validation->run() == true)
        {
            if(isset($_POST['id']) && !empty(trim($_POST['id'])))
            {

                $where['email'] = $_POST['email'];
                $check = $this->User_model->check_column_exist($where, 'user_id', $_POST['id'], 'log_info');
                if(!empty($check))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Email already exist']);
                    exit();
                }
            }

            $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();

            if(!empty($reserve_domain_exist))
            {
                echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                exit();
            }


            if(isset($_POST['id']) && !empty(trim($_POST['id'])))
            {
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->where('id !=', $_POST['id'])->get()->result();
            }
            else
            {
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
                if(!empty($sub_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                    exit();
                }
            }

            if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
            {
                echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                exit();
            }
            $package_id = $_POST['select_b_package_id'];

            $user_data['first_name'] = $_POST['first_name'];
            $user_data['last_name']  = $_POST['last_name'];
            $user_data['email']      = $_POST['email'];
            $user_data['address']    = $_POST['address'];
            $user_data['phone']      = $_POST['phone'];

            $user_data['company']          = $_POST['company'];
            $user_data['city']             = $_POST['city'];
            $user_data['state']            = $_POST['state'];
            $user_data['zip_code']         = $_POST['zip_code'];
            $user_data['country_code']     = $_POST['country_code'];
            $user_data['sub_domain']       = $_POST['sub_domain'];

            if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
            {
                $user_data['bridge_b'] = $_POST['bridge_b'];
            }

            if(!isset($_POST['id']) || empty(trim($_POST['id'])))
            {

                $user_data['user_type']   = 'b';
                $user_data['create_date'] = date('Y-m-d');
                $user_data['language']    = 'English';
                $this->db->insert('user_info', $user_data);
                $user_id = $this->db->insert_id();

                if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
                {
                    $this->update_wholesales_bridge_user($_POST['bridge_b'],$user_id);
                }


                // Add Default shipping info while create b user : START
                $this->load->model('Common_model');
                $this->Common_model->default_shipping_method($user_id);
                // Add Default shipping info while create b user : END

                //$this->b_level_acc_coa($user_id);
                // $this->b_level_menu_transfer($user_id);
                $loginfo = array('user_id' => $user_id, 'email' => $_POST['email'], 'password' => md5($_POST['password']), 'user_type' => 'b', 'is_admin' => 1);
                $this->db->insert('log_info', $loginfo);

                //============ its for access log info collection ===============
                $accesslog_info = array('action_page' => $this->uri->segment(1), 'action_done' => 'insert', 'remarks' => "User information save", 'user_name' => $user_id, 'entry_date' => date("Y-m-d H:i:s"),);
                $this->db->insert('accesslog', $accesslog_info);
                //echo json_encode(['status' => 201, 'msg' => 'User details has been added successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
                //echo json_encode(['status' => 200, 'msg' => 'User details has been added successfully', 'user_id' => $user_id]);
                echo json_encode(['status' => 200, 'msg' => 'Wholesaler details has been added successfully', 'user_id' => $user_id,'package_id'=>$package_id,'duration'=>$_POST['select_b_duration']]);
                // exit();
            }
            else
            {
                //passworde update
                if($_POST['password'] != '')
                {
                    if($_POST['password'] == $_POST['confirm'])
                    {
                        $pass_data['password'] = md5($_POST['password']);
                        $this->db->where('user_id', $_POST['id'])->update('log_info', $pass_data);
                    }
                    else
                    {
                        echo json_encode(['status' => 400, 'msg' => 'Password And confirm password not same']);
                        exit();
                    }

                }
                //passworde update
                $this->db->where('id', $_POST['id'])->update('user_info', $user_data);

                if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
                {
                    $this->update_wholesales_bridge_user($_POST['bridge_b'],$_POST['id']);
                }
                redirect('wholesaler-package');

                // echo json_encode(['status' => 201, 'msg' => 'User details has been updated successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
                // exit();
            }
        }
        else
        {
            $error_arr = $this->form_validation->error_array();
            foreach($error_arr as $key => $msg)
            {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }


    }

    function add_customer_api() {


        $this->load->model('front_section/Customer_model');

        $this->form_validation->set_rules('first_name_2', 'First name', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name_2', 'Last name', 'required|max_length[50]');
        $this->form_validation->set_rules('email_2', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
        $this->form_validation->set_rules('password_2', 'Password', 'required|max_length[32]');

        $this->form_validation->set_rules('company', 'Company', 'required|max_length[1000]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[1000]');
        $this->form_validation->set_rules('sub_domain_2', 'Sub Domain', 'required');
        if ($this->form_validation->run() == true) {



//            $auth_user = GetSuperAdmin();
//            $super_admin_id = $auth_user['user_id'];
//            $b_user_id = $_POST['b_user_id'];
            $b_user_id = 1;
            $package_id = $_POST['select_c_package_id'];
            $remarks = "Customer information save";
            $created_date = date('Y-m-d');
            $first_name = $this->input->post('first_name_2');
            $last_name = $this->input->post('last_name_2');
            $email = $this->input->post('email_2');
            $phone = $this->input->post('phone_2');
            $phone_type = $this->input->post('phone_type');
            $company = $this->input->post('company');
            $customer_type = "business";
            $address = $this->input->post('address');
            $address_explode = explode(",", $address);
            $address = $address_explode[0];
            $street_no = explode(' ', $address);
            $street_no = $street_no[0];
            $side_mark = $first_name . "-" . $street_no;
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $zip_code = $this->input->post('zip_code');
            $country_code = $this->input->post('country_code');
            $password = $this->input->post('password_2');
            $username = $this->input->post('email_2');
            $sub_domain = $this->input->post('sub_domain_2');
            $user_insert_id = "";

            if ($customer_type == 'business') {

                if(!empty($sub_domain)){


                    /*******************************************/
                    $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain_2'])->get()->result();
                    if(!empty($reserve_domain_exist))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                        exit();
                    }


                    if(isset($_POST['customer_id']) && !empty(trim($_POST['customer_id'])))
                    {
                        $customer_Data = $this->db->where('customer_id', $_POST['customer_id'])->get('customer_info')->row();

                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain_2'])->where('id !=', $customer_Data->customer_user_id)->get()->result();
                        if (!empty($sub_domain_exist))
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }

                    }
                    else
                    {

                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain_2'])->get()->result();

                        if(!empty($sub_domain_exist))
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }
                    }

                    if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain_2']))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                        exit();
                    }
                    /*******************************************/
                }

                $customer_userinfo_data['created_by'] = '1';
                //$customer_userinfo_data['package_id'] = $package_id;
                $customer_userinfo_data['first_name'] = $first_name;
                $customer_userinfo_data['last_name'] = $last_name;
                $customer_userinfo_data['company'] = $company;
                $customer_userinfo_data['address'] = $address;
                $customer_userinfo_data['city'] = $city;
                $customer_userinfo_data['state'] = $state;
                if (isset($zip_code))
                    $customer_userinfo_data['zip_code'] = $zip_code;
                $customer_userinfo_data['country_code'] = $country_code;
                $customer_userinfo_data['phone'] = $phone;
                $customer_userinfo_data['language'] = 'English';
                $customer_userinfo_data['user_type'] = 'c';
                $customer_userinfo_data['create_date'] = $created_date;
                $customer_userinfo_data['reference'] = "";
                $customer_userinfo_data['sub_domain'] =$sub_domain;
                $customer_userinfo_data['updated_by'] ="1";
                $customer_userinfo_data['update_date'] =$created_date;

                $company_profile['company_name'] = $company;
                $company_profile['phone'] = $phone;
                $company_profile['address'] = $address;
                $company_profile['city'] = $city;
                $company_profile['state'] = $state;

                $company_profile['zip_code'] = $zip_code;

                $company_profile['country_code'] = $country_code;
                $company_profile['created_by'] = $b_user_id;
                $company_profile['created_at'] = $created_date;

                $customer_userinfo_data['email'] = $email;


                $this->db->insert('user_info', $customer_userinfo_data);
                //  echo $this->db->last_query();
                //  die('here');
                $user_insert_id = $this->db->insert_id();

                // Add Default shipping info while create c user : START
                $this->load->model('Common_model');
                $this->Common_model->default_shipping_method($user_insert_id);
                // Add Default shipping info while create c user : END

                //$this->c_level_menu_transfer($user_insert_id);
                // $this->c_level_acc_coa($user_insert_id);
                // =========== its for save customer data in the users table and login info ===============

                $customer_loginfo_data = array('user_id' => $user_insert_id, 'email' => $username, 'password' => md5($password), 'user_type' => 'c', 'is_admin' => '1',);

                $this->db->insert('log_info', $customer_loginfo_data);

                $company_profile['user_id'] = $user_insert_id;
                $company_profile['email'] = $email;
                $this->db->insert('company_profile', $company_profile);
            }


            //        ============ its for accounts coa table ===============
            $coa = $this->Customer_model->headcode();
            if ($coa->HeadCode != NULL) {
                $hc = explode("-", $coa->HeadCode);
                $nxt = $hc[1] + 1;
                $headcode = $hc[0] . "-" . $nxt;
            } else {
                $headcode = "1020301-1";
            }

            $lastid = $this->db->select("*")->from('customer_info')->where('level_id', 1)
            ->order_by('customer_id', 'desc')->get()->row();

            $sl = $lastid->customer_no;
            if (empty($sl)) {
                $sl = "CUS-0001";
            } else {
                $sl = $sl;
            }
            $supno = explode('-', $sl);
            $nextno = $supno[1] + 1;
            $si_length = strlen((int) $nextno);

            $str = '0000';
            $cutstr = substr($str, $si_length);
            $sino = "CUS" . "-" . $cutstr . $nextno;
            $customer_no = $sino . '-' . $first_name . " " . $last_name;
            //        ================= close =======================
            //        =============== its for company name with customer id start =============
            $last_c_id = $lastid->customer_id;
            $cn = strtoupper(substr($company, 0, 3)) . "-";
            if (empty($last_c_id)) {
                $last_c_id = $cn . "1";
            } else {
                $last_c_id = $last_c_id;
            }
            $cust_nextid = $last_c_id + 1;
            $company_custid = $cn . $cust_nextid;



            $customer_data['package_id'] = $package_id;

            $package_detail = $this->db->where('id', $package_id)->get('hana_package')->row();
//package update

//            if($package_id==2 || $package_id==3)
//            {
//                $amount = 0;
//                $package_duration = 0;
//                if($_POST['duration'] =='monthly')
//                {
//                    $amount = $package_detail->monthly_price;
//                    $package_duration = 30;
//                }
//                if($_POST['duration'] =='yearly')
//                {
//                    $amount = $package_detail->yearly_price;
//                    $package_duration = 365;
//                }
//
//                $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));
//
//
//                $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>2);
//
//                $this->db->where('id',$user_insert_id);
//                $is_update = $this->db->update('user_info',$paid_data);
//
//                //transaction
//                $transaction_data = array('user_id'=>$user_insert_id,
//                    'package_id'=>$package_id,
//                    'amount'=>$amount,
//                    'transaction_type'=>'credit',
//                    'transaction_id'=>'',
//                    'status'=>1,
//                    'remark'=>'Package Upgrade By Admin'
//                );
//                $this->db->insert('package_transaction',$transaction_data);
//                //transaction
//            }
//            if($package_id==1)
//            {
//                $package_duration = 30;
//
//                $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));
//
//
//                $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>1);
//
//                $this->db->where('id',$user_insert_id);
//                $is_update = $this->db->update('user_info',$paid_data);
//            }
//            $customer_data['package_id'] = $package_id;


// package update end

            $customer_data['first_name'] = $first_name;
            $customer_data['last_name'] = $last_name;
            $customer_data['email'] = $email;
            $customer_data['phone'] = $phone;
            $customer_data['company'] = $company;
            $customer_data['customer_type'] = $customer_type;
            $customer_data['address'] = $address;
            $customer_data['city'] = $city;
            $customer_data['state'] = $state;
            $customer_data['zip_code'] = $zip_code;
            $customer_data['country_code'] = $country_code;
            $customer_data['street_no'] = $street_no;
            $customer_data['side_mark'] = $side_mark;
            $customer_data['level_id'] = $b_user_id;
            $customer_data['created_by'] = $b_user_id;
            $customer_data['create_date'] = $created_date;


            $customer_data['customer_user_id'] = $user_insert_id;
            $customer_data['customer_no'] = $customer_no;
            $customer_data['company_customer_id'] = $company_custid;
            $this->db->flush_cache();
            $this->db->insert('customer_info', $customer_data);
            $customer_inserted_id = $this->db->insert_id();

                $this->c_level_menu_transfer($user_insert_id);
            // $this->c_level_menu_transfer($user_insert_id);
            //transfar acc_coa_table data
            //  $this->c_level_acc_coa($user_insert_id);
            //        ======== its for customer COA data array ============
            $customer_coa = array('HeadCode' => $headcode, 'HeadName' => $customer_no, 'PHeadName' => 'Customer Receivable', 'HeadLevel' => '4', 'IsActive' => '1', 'IsTransaction' => '1', 'IsGL' => '0', 'HeadType' => 'A', 'IsBudget' => '0', 'IsDepreciation' => '0', 'DepreciationRate' => '0', 'CreateBy' => $b_user_id, 'CreateDate' => $created_date,);
            //        dd($customer_coa);
            $this->db->insert('b_acc_coa', $customer_coa);



                //            for ($i = 0; $i < count($phone); $i++) {
                //                $phone_types_number = array('phone' => $phone[$i], 'phone_type' => $phone_type[$i], 'customer_id' => $customer_inserted_id, 'customer_user_id' => $user_insert_id,);
                //                $this->db->insert('customer_phone_type_tbl', $phone_types_number);
                //            }
//            $url = base_url('#step-3');
//            echo json_encode(['status' => 201, 'msg' => 'Customer info save successfully!', 'url' => $url]);
            echo json_encode(['status' => 200, 'msg' => 'Customer info save successfully!', 'user_id' => $user_insert_id,'package_id'=>$package_id,'duration'=>$_POST['select_c_duration']]);
            exit();
        } else {
            $error_arr = $this->form_validation->error_array();
            foreach ($error_arr as $key => $msg) {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }
    }

    public function c_level_menu_transfer($user_insert_id) {

        $this->db->where('level_id', $user_insert_id)->delete('c_menusetup_tbl');

        $get_all_c_default_menu = $this->db->get('menusetup_tbl')->result();

        //        $menu_title = $get_all_c_default_menu
        foreach ($get_all_c_default_menu as $single) {
            $transfer_menu = array('menu_id' => $single->id,
                'menu_title' => $single->menu_title,
                'korean_name' => $single->korean_name,
                'page_url' => $single->page_url,
                'module' => $single->module,
                'ordering' => $single->ordering,
                'parent_menu' => $single->parent_menu,
                'menu_type' => $single->menu_type,
                'is_report' => $single->is_report,
                'icon' => $single->icon,
                'status' => $single->status,
                'level_id' => $user_insert_id,
                'created_by' => $user_insert_id,
                'create_date' => date('Y-m-d'),);
            //            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
            $this->db->insert('c_menusetup_tbl', $transfer_menu);
        }
        //        dd($get_all_c_default_menu);
        return true;
    }

    function payment_pay_now()
    {
        $this->form_validation->set_rules('card_number', 'Card Number', 'required');
        $this->form_validation->set_rules('expiry_month', 'Expiry Month', 'required|max_length[2]');
        $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'required|max_length[2]');
        $this->form_validation->set_rules('cvv', 'Cvv', 'required|max_length[3]');
        $this->form_validation->set_rules('user_id', 'User id', 'required');
        $this->form_validation->set_rules('package_id', 'Package id', 'required');
        $this->form_validation->set_rules('duration', 'Duration', 'required');

        if($this->form_validation->run() == true)
        {
            $user_detail = $this->db->select('*')
                                    ->from('user_info')
                                    ->where(array('id'=>$_POST['user_id']))
                                    ->get()
                                    ->row();
            if($user_detail->user_type =='c')
            {
 /**************************************************************************************/
               // echo "<pre>";print_r($_POST);die();
                if($_POST['duration']=='monthly')
                {
                    $this->session->set_userdata('temp_duration','monthly');
                }
                if($_POST['duration']=='yearly')
                {
                    $this->session->set_userdata('temp_duration','yearly');
                }

//                unset($_POST['card_no']);
//
//                $this->db->where('id',$_POST['card_id']);
//                $this->db->update('c_card_info',array('cvv'=>$_POST['cvv']));

                $this->load->library('gwapi');
                $user_id = $_POST['user_id'];
                $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();

                $package_details = $this->db->where('id',$_POST['package_id'])->get('hana_package')->row();

                $installment_amount = $package_details->installation_price;

                $remaining_amount = 0;
                if($_POST['duration'] =='monthly')
                {
                    $amount = $package_details->monthly_price;
                    $plan_id = $package_details->monthly_plan_id;
                }
                if($_POST['duration'] =='yearly')
                {
                    $amount = $package_details->yearly_price;
                    $plan_id = $package_details->yearly_plan_id;


                    // Old Package Amount Reduse

                    if ($user_deatail->package_type==2)
                    {
                        $now = strtotime(Date('Y-m-d h:i:s'));
                        $expire_date = strtotime($user_deatail->package_expire);
                        $diff = abs($expire_date - $now);
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

                        // for current package remaining amount
                        $package_details_for_remaining_amount = $this->db->where('id',$user_deatail->package_id)->get('hana_package')->row();
                        // for current package remaining amount

                        $remaining_amount = $months * $package_details_for_remaining_amount->monthly_price;
                        // echo $remaining_amount;die();
                    }
                    // Old Package Amount Reduse
                }

                $recurring_amount = $amount-$remaining_amount;

                $installment_payable_amount = $installment_amount-$remaining_amount;

                $txn_coupon = '';
                $txn_discount = '';

                $cusstomer_info     = $this->db->where('customer_user_id',$user_id)->get('customer_info')->row();

                $card_number = str_replace("-","",$_POST['card_number']);

                $payment_data = array('user_id' => $user_id,
                    'recurring_amount'=>$recurring_amount,
                    'installment_payable_amount'=>$installment_payable_amount,
                    'customer_no'=>$cusstomer_info->customer_no,
                    'customer_id'=>$cusstomer_info->customer_id,
                    'package_id'=>$package_details->id,
                    'plan_id'=>$plan_id,
                    'payment_method' => 'card',
                    'card_number' => $_POST['card_number'],
                    'expiry_month' => $_POST['expiry_month'],
                    'expiry_year' => $_POST['expiry_year'],
                    'cvv' => $_POST['cvv'],
                    'card_holder_name' => $_POST['card_holder_name']
                );



                $res = $this->receive_payment_to_admin($payment_data);

                $user_id = $_POST['user_id'];

                if ($installment_payable_amount < 0 || $installment_payable_amount == 0)
                {
                    $installment_payable_amount = 0;
                }

                if($res['instalment_response']->response=='1')
                {
                    $transaction_data = array('user_id'=>$user_id,
                        'package_id'=>$package_details->id,
                        'amount'=>$installment_payable_amount,
                        'transaction_type'=>'credit',
                        'transaction_id'=>$res['instalment_response']->transactionid,
                        'status'=>1,
                        'coupon'=>$txn_coupon,
                        'discount'=>$txn_discount,
                        'card_no'=>$card_number);
                    $this->db->insert('package_transaction',$transaction_data);
                }
                if($res['instalment_response']->response=='2')
                {
                    $transaction_data = array('user_id'=>$user_id,
                        'package_id'=>$package_details->id,
                        'amount'=>$installment_payable_amount,
                        'transaction_type'=>'credit',
                        'transaction_id'=>$res['instalment_response']->transactionid,
                        'status'=>0,
                        'coupon'=>$txn_coupon,
                        'discount'=>$txn_discount,
                        'card_no'=>$card_number);
                    $this->db->insert('package_transaction',$transaction_data);
                    //redirect(base_url().'payment-failed');
                    echo json_encode(['status' => 400, 'msg' => 'Your Payment has Failed']);
                    exit();
                }

                if($res['instalment_response']->response=='3')
                {
                    //$this->session->set_flashdata('error', $res['instalment_response']->responsetext);
                    //redirect(base_url().'payment-failed');
                    if (isset($res['instalment_response']->responsetext) && ($res['instalment_response']->response_code==300))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'TMS Payment Not Configure. Please Contact To Administrater']);
                        exit();

                    }
                   if (isset($res['instalment_response']->responsetext) && !empty($res['instalment_response']->responsetext))
                   {
                       echo json_encode(['status' => 400, 'msg' => $res['instalment_response']->responsetext]);
                       exit();

                   }
                    echo json_encode(['status' => 400, 'msg' => 'Your Payment has Failed']);
                    exit();

                }



                if($res['instalment_response']->response=='1')
                {
                    $package_details = $this->db->where('id',$_POST['package_id'])->get('hana_package')->row();

                    $package_duration = 0;
                    if($_POST['duration'] =='monthly')
                    {
                        $package_duration = 30;
                        $package_duration_type = 1;
                    }
                    if($_POST['duration'] =='yearly')
                    {
                        $package_duration = 365;
                        $package_duration_type = 2;
                    }

                    $new_card_detail = array('card_number'=>$_POST['card_number'],
                                             'expiry_month'=>$_POST['expiry_month'],
                                             'expiry_year'=>$_POST['expiry_year'],
                                             'cvv'=>$_POST['cvv'],
                                             'card_holder_first_name'=>$_POST['card_holder_name'],
                                             'created_by'=>$_POST['user_id']);
                    $this->db->insert('c_card_info',$new_card_detail);
                    $card_id = $this->db->insert_id();

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_details->id,'package_expire'=>$expire_date,'package_type'=>2,'package_duration'=>$package_duration_type,'card_id'=>$card_id);
                    $this->db->where('id',$user_id);
                    $is_update = $this->db->update('user_info',$paid_data);

                    update_customer_employee_C();

                    if(isset($res['recurring_response']->subscription_id) && !empty($res['recurring_response']->subscription_id))
                    {
                        $add_sub = array('subscription_id'=>$res['recurring_response']->subscription_id,'billing_date'=>date('d-m-Y'));
                        $this->db->where('id',$user_id);
                        $this->db->update('user_info',$add_sub);
                    }

                    if($is_update)
                    {
                        $url = base_url('payment-success');
                        echo json_encode(['status' => 201, 'msg' => 'You Have Sucessfully subscribed','url' => $url]);
                        exit();

//                        $this->session->set_flashdata('success', "<div class='alert alert-success'>
//                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
//                You Have Sucessfully subscribed".$package_details->package." package</div>");
//                        redirect(base_url().'retailer-package-my-subscription');
                    }
                }
/**************************************************************************************/
            }
            else if($user_detail->user_type == 'b')
            {
/*######################################################################################*/
                if($_POST['duration']=='monthly')
                {
                    $this->session->set_userdata('temp_duration','monthly');
                }
                if($_POST['duration']=='yearly')
                {
                    $this->session->set_userdata('temp_duration','yearly');
                }

                $this->load->library('gwapi');
                $user_id = $_POST['user_id'];
                $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();

                $package_details = $this->db->where('id',$_POST['package_id'])->get('wholesaler_hana_package')->row();

                $installment_amount = $package_details->installation_price;

                $remaining_amount = 0;
                if($_POST['duration'] =='monthly')
                {
                    $amount = $package_details->monthly_price;
                    $plan_id = $package_details->monthly_plan_id;
                }
                if($_POST['duration'] =='yearly')
                {
                    $amount = $package_details->yearly_price;
                    $plan_id = $package_details->yearly_plan_id;

                    if ($user_deatail->package_type==2)
                    {
                        $now = strtotime(Date('Y-m-d h:i:s'));
                        $expire_date = strtotime($user_deatail->package_expire);
                        $diff = abs($expire_date - $now);
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

                        // for current package remaining amount
                        $package_details_for_remaining_amount = $this->db->where('id',$user_deatail->package_id)->get('wholesaler_hana_package')->row();
                        // for current package remaining amount

                        $remaining_amount = $months * $package_details_for_remaining_amount->monthly_price;
                        // echo $remaining_amount;die();
                    }
                    // Old Package Amount Reduse
                }

                $recurring_amount = $amount-$remaining_amount;

                $installment_payable_amount = $installment_amount-$remaining_amount;

                $txn_coupon = '';
                $txn_discount = '';


                $cusstomer_info     = $this->db->where('id',$user_id)->get('user_info')->row();

                $card_number = $card_number = str_replace("-","",$_POST['card_number']);;

                $payment_data = array('user_id'=>$user_id,
                    'recurring_amount'=>$recurring_amount,
                    'installment_payable_amount'=>$installment_payable_amount,
                    'customer_no'=>$cusstomer_info->id,
                    'customer_id'=>$cusstomer_info->id,
                    'package_id'=>$package_details->id,
                    'plan_id'=>$plan_id,
                    'payment_method' => 'card',
                    'card_number' => $_POST['card_number'],
                    'expiry_month' => $_POST['expiry_month'],
                    'expiry_year' => $_POST['expiry_year'],
                    'cvv' => $_POST['cvv'],
                    'card_holder_name' => $_POST['card_holder_name']
                );



                $res = $this->receive_payment_to_admin_of_wholeseller($payment_data);
                $user_id = $_POST['user_id'];

                if ($installment_payable_amount < 0 || $installment_payable_amount == 0)
                {
                    $installment_payable_amount = 0;
                }

                if($res['instalment_response']->response=='1')
                {
                    $transaction_data = array('user_id'=>$user_id,
                        'package_id'=>$package_details->id,
                        'amount'=>$installment_payable_amount,
                        'transaction_type'=>'credit',
                        'transaction_id'=>$res['instalment_response']->transactionid,
                        'status'=>1,
                        'coupon'=>$txn_coupon,
                        'discount'=>$txn_discount,
                        'card_no'=>$card_number);
                    $this->db->insert('wholesaler_package_transaction',$transaction_data);
                }
                if($res['instalment_response']->response=='2')
                {
                    $transaction_data = array('user_id'=>$user_id,
                        'package_id'=>$package_details->id,
                        'amount'=>$installment_payable_amount,
                        'transaction_type'=>'credit',
                        'transaction_id'=>$res['instalment_response']->transactionid,
                        'status'=>0,
                        'coupon'=>$txn_coupon,
                        'discount'=>$txn_discount,
                        'card_no'=>$card_number);
                    $this->db->insert('wholesaler_package_transaction',$transaction_data);
                    //redirect(base_url().'wholesaler-payment-failed');
                    echo json_encode(['status' => 400, 'msg' => 'Your Payment has Failed']);
                    exit();
                }

                if($res['instalment_response']->response=='3')
                {
                    if (isset($res['instalment_response']->responsetext) && ($res['instalment_response']->response_code==300))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'TMS Payment Not Configure. Please Contact To Administrater']);
                        exit();

                    }
//                    $this->session->set_flashdata('error', $res['instalment_response']->responsetext);
//                    redirect(base_url().'wholesaler-payment-failed');
                    if (isset($res['instalment_response']->responsetext) && !empty($res['instalment_response']->responsetext))
                    {
                        echo json_encode(['status' => 400, 'msg' => $res['instalment_response']->responsetext]);
                        exit();

                    }
                    echo json_encode(['status' => 400, 'msg' => 'Your Payment has Failed']);
                    exit();
                }



                if($res['instalment_response']->response=='1')
                {
                    $package_details = $this->db->where('id',$_POST['package_id'])->get('wholesaler_hana_package')->row();

                    $package_duration = 0;
                    if($_POST['duration'] =='monthly')
                    {
                        $package_duration = 30;
                        $package_duration_type = 1;
                    }
                    if($_POST['duration'] =='yearly')
                    {
                        $package_duration = 365;
                        $package_duration_type = 2;
                    }


                    $new_card_detail = array('card_number'=>$_POST['card_number'],
                        'expiry_month'=>$_POST['expiry_month'],
                        'expiry_year'=>$_POST['expiry_year'],
                        'cvv'=>$_POST['cvv'],
                        'card_holder_first_name'=>$_POST['card_holder_name'],
                        'created_by'=>$_POST['user_id']);
                    $this->db->insert('c_card_info',$new_card_detail);
                    $card_id = $this->db->insert_id();

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_details->id,'package_expire'=>$expire_date,'package_type'=>2,'package_duration'=>$package_duration_type,'card_id'=>$card_id);

                    $this->db->where('id',$user_id);
                    $is_update = $this->db->update('user_info',$paid_data);

                    update_customer_employee_B();

                    if(isset($res['recurring_response']->subscription_id) && !empty($res['recurring_response']->subscription_id))
                    {

                        $add_sub = array('subscription_id'=>$res['recurring_response']->subscription_id,'billing_date'=>date('d-m-Y'));
                        $this->db->where('id',$user_id);
                        $this->db->update('user_info',$add_sub);

                    }

                    if($is_update)
                    {
                        $url = base_url('payment-success');
                        echo json_encode(['status' => 201, 'msg' => 'You Have Sucessfully subscribed','url' => $url]);
                        exit();

//                        $this->session->set_flashdata('success', "<div class='alert alert-success'>
//                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
//                You Have Sucessfully subscribed".$package_details->package." package</div>");
//                        redirect(base_url().'wholesaler-package-my-subscription');
                    }
                }
                echo "<pre>"; print_r($res);die();
/*################################################################################*/

            }
            echo "<pre>";
            print_r($_POST);
            die();
        }
        else
        {
            $error_arr = $this->form_validation->error_array();
            foreach($error_arr as $key => $msg)
            {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }
    }


    function receive_payment_to_admin($payment_data)
    {

        $customer_id                    = $payment_data['user_id'];
        $customer_no                    = $payment_data['customer_no'];
        $payment_method                 = $payment_data['payment_method'];
        $package_id                     = $payment_data['package_id'];
        $recurring_amount               = $payment_data['recurring_amount'];
        $installment_payable_amount     = $payment_data['installment_payable_amount'];
        $ccnumber                       = str_replace('-','',$payment_data['card_number']);
        $card_holder_name               = $payment_data['card_holder_name'];
        $ccexp                          = $payment_data['expiry_month'].'/'.$payment_data['expiry_year'];
        $cvv                            = $payment_data['cvv'];
        $ip                             = $this->input->ip_address();
        $plan_id                        = $payment_data['plan_id'];



        $cusstomer_info = $this->db->where('customer_user_id',$customer_id)->get('customer_info')->row();

        // $orderd = $this->db->select("*")->from('quatation_tbl')->where('package_id', $package_id)->get()->row();

        $cHead      = $this->db->select('HeadCode,level_id')->where('HeadName',$customer_no)->get('acc_coa')->row();
        $cmp_info   = $this->setting_model->company_profile();

        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();


#----------------------------
#   payment gatway
#----------------------------

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($package_id, $orderdescription='Package payment', $tax=0,$Shipping=0,$ip);

        $response = array();


        /* less then zero amount */
        if ($installment_payable_amount < 0 || $installment_payable_amount == 0)
        {
            $NewDate=Date('m/d/Y', strtotime("+30 days"));

            $perameter['recurring'] = "add_subscription";
            $perameter['redirect_url'] = "https://www.google.com";
            $perameter['plan_id'] = $plan_id;
            $perameter['account_type'] = "savings";
            $perameter['entity_type'] = "business";
            $perameter['payment'] = "creditcard";
            $perameter['start_date'] = date("Ymd", strtotime($NewDate));
            $this->gwapi->sub_perameter($perameter);

            $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

            $response['recurring_response'] = $recurring_response;


            $installment_response = (object)[];
            $installment_response->response = 1;
            $installment_response->responsetext = 'SUCCESS';
            $installment_response->authcode = '123456';
            $installment_response->transactionid = '0000000000';
            $installment_response->avsresponse = 'N';
            $installment_response->cvvresponse = 'N';
            $installment_response->orderid = $package_id;
            $installment_response->type = 'sale';
            $installment_response->response_code = '100';

            $response['instalment_response'] = $installment_response;


            #----------------------------
            #   payment set database
            #----------------------------

            if($installment_response->response=='1')
            {

                if(!empty($cHead->HeadCode)){

                    $voucher_no = $package_id;
                    $Vtype = "INV";
                    $VDate = date('Y-m-d');
                    $paid_amount = $this->input->post('paid_amount');
                    $cAID = $cHead->HeadCode;
                    $IsPosted = 1;
                    $CreateBy = $this->session->userdata('user_id');
                    $createdate = date('Y-m-d H:i:s');

                    //customer credit insert acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $cAID,
                        'level_id'      => $cHead->cHead,
                        'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('acc_transaction', $customerCredit);
                    //------------------------------------

                    // debit acc_transaction
                    $payment_method = $this->input->post('payment_method');

                    $COAID = '102010202';


                    $b_levelDebit = array(
                        'VNo' => $voucher_no,
                        'Vtype' => $Vtype,
                        'VDate' => $VDate,
                        'Debit' => $paid_amount,
                        'Credit' => 0,
                        'COAID' => $COAID,
                        'level_id'      => $cHead->cHead,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted' => $IsPosted,
                        'CreateBy' => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove' => 1
                    );

                    $this->db->insert('acc_transaction', $b_levelDebit);
                    //------------------------------------

                }

                // C level notification

                $cNotificationData = array(
                    'notification_text' => 'Payment has been received for '.$package_id,
                    'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                    'created_by'        => $this->session->userdata('user_id'),
                    'date'              => date('Y-m-d')
                );

                $this->db->insert('c_notification_tbl',$cNotificationData);

                //-------------------------


                // $this->Order_model->smsSend(

                //     $data = array(
                //         'customer_id' => $customer_id,
                //         'message'     => 'Payment has been done for Package ',
                //         'subject'     => 'Package payment'
                //     )

                // );


            }

            return $response;

        }
        /* less then zero Amount */


//for direct payment diduct

        $installment_response = (object)$this->gwapi->doSale($installment_payable_amount, $ccnumber, $ccexp, $cvv);


        $response['instalment_response'] = $installment_response;

        if($installment_response->response!='1')
        {
            return $response;
        }
//for direct payment diduct
        $NewDate=Date('m/d/Y', strtotime("+30 days"));

//for recurring
        $perameter['recurring'] = "add_subscription";
        $perameter['redirect_url'] = "https://www.google.com";
        $perameter['plan_id'] = $plan_id;
        $perameter['account_type'] = "savings";
        $perameter['entity_type'] = "business";
        $perameter['payment'] = "creditcard";
        $perameter['start_date'] = date("Ymd", strtotime($NewDate));
        $this->gwapi->sub_perameter($perameter);

        $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

        $response['recurring_response'] = $recurring_response;
//for recurring



#----------------------------



#----------------------------
#   payment set database
#----------------------------

        if($installment_response->response=='1')
        {

            if(!empty($cHead->HeadCode)){

                $voucher_no = $package_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $this->session->userdata('user_id');
                $createdate = date('Y-m-d H:i:s');

                //customer credit insert acc_transaction
                $customerCredit = array(

                    'VNo'           => $voucher_no,
                    'Vtype'         => $Vtype,
                    'VDate'         => $VDate,
                    'Debit'         => 0,
                    'Credit'        => $paid_amount,
                    'COAID'         => $cAID,
                    'level_id'      => $cHead->cHead,
                    'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                    'IsPosted'      => $IsPosted,
                    'CreateBy'      => $CreateBy,
                    'CreateDate'    => $createdate,
                    'IsAppove'      => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------

                // debit acc_transaction
                $payment_method = $this->input->post('payment_method');

                $COAID = '102010202';


                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'level_id'      => $cHead->cHead,
                    'Narration' => "Amount received for invoice #".$voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $b_levelDebit);
                //------------------------------------

            }

            // C level notification

            $cNotificationData = array(
                'notification_text' => 'Payment has been received for '.$package_id,
                'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                'created_by'        => $this->session->userdata('user_id'),
                'date'              => date('Y-m-d')
            );

            $this->db->insert('c_notification_tbl',$cNotificationData);

            //-------------------------


            // $this->Order_model->smsSend(

            //     $data = array(
            //         'customer_id' => $customer_id,
            //         'message'     => 'Payment has been done for Package ',
            //         'subject'     => 'Package payment'
            //     )

            // );


        }

        return $response;
#--------------------------

    }


    function receive_payment_to_admin_of_wholeseller($payment_data)
    {

        $customer_id                    = $payment_data['user_id'];
        $customer_no                    = $payment_data['customer_no'];
        $payment_method                 = $payment_data['payment_method'];
        $package_id                     = $payment_data['package_id'];
        $recurring_amount               = $payment_data['recurring_amount'];
        $installment_payable_amount     = $payment_data['installment_payable_amount'];
        $ccnumber                       = str_replace('-','',$payment_data['card_number']);
        $card_holder_name               = $payment_data['card_holder_name'];
        $ccexp                          = $payment_data['expiry_month'].'/'.$payment_data['expiry_year'];
        $cvv                            = $payment_data['cvv'];
        $ip                             = $this->input->ip_address();
        $plan_id                        = $payment_data['plan_id'];

        $cusstomer_info = $this->db->where('id',$customer_id)->get('user_info')->row();

        // $orderd = $this->db->select("*")->from('quatation_tbl')->where('package_id', $package_id)->get()->row();
        $cHead      = $this->db->select('HeadCode,level_id')->where('HeadCode',$customer_no)->get('acc_coa')->row();

        $cmp_info   = $this->settings->company_profile();
        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();


#----------------------------
#   payment gatway
#----------------------------

        $this->gwapi->setLogin($machentinfo);
        $this->gwapi->setBilling($cusstomer_info);
        $this->gwapi->setShipping($cusstomer_info);
        $this->gwapi->setOrder($package_id, $orderdescription='Package payment', $tax=0,$Shipping=0,$ip);

        $response = array();

        /* less then zero amount */
        if ($installment_payable_amount < 0 || $installment_payable_amount == 0)
        {
            $NewDate=Date('m/d/Y', strtotime("+30 days"));

            $perameter['recurring'] = "add_subscription";
            $perameter['redirect_url'] = "https://www.google.com";
            $perameter['plan_id'] = $plan_id;
            $perameter['account_type'] = "savings";
            $perameter['entity_type'] = "business";
            $perameter['payment'] = "creditcard";
            $perameter['start_date'] = date("Ymd", strtotime($NewDate));
            $this->gwapi->sub_perameter($perameter);

            $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);
            $response['recurring_response'] = $recurring_response;


            $installment_response = (object)[];
            $installment_response->response = 1;
            $installment_response->responsetext = 'SUCCESS';
            $installment_response->authcode = '123456';
            $installment_response->transactionid = '0000000000';
            $installment_response->avsresponse = 'N';
            $installment_response->cvvresponse = 'N';
            $installment_response->orderid = $package_id;
            $installment_response->type = 'sale';
            $installment_response->response_code = '100';

            $response['instalment_response'] = $installment_response;


            #----------------------------
            #   payment set database
            #----------------------------

            if($installment_response->response=='1')
            {

                if(!empty($cHead->HeadCode)){

                    $voucher_no = $package_id;
                    $Vtype = "INV";
                    $VDate = date('Y-m-d');
                    $paid_amount = $this->input->post('paid_amount');
                    $cAID = $cHead->HeadCode;
                    $IsPosted = 1;
                    $CreateBy = $payment_data['user_id'];
                    $createdate = date('Y-m-d H:i:s');

                    //customer credit insert acc_transaction
                    $customerCredit = array(

                        'VNo'           => $voucher_no,
                        'Vtype'         => $Vtype,
                        'VDate'         => $VDate,
                        'Debit'         => 0,
                        'Credit'        => $paid_amount,
                        'COAID'         => $cAID,
                        'level_id'      => $cHead->cHead,
                        'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                        'IsPosted'      => $IsPosted,
                        'CreateBy'      => $CreateBy,
                        'CreateDate'    => $createdate,
                        'IsAppove'      => 1
                    );

                    $this->db->insert('acc_transaction', $customerCredit);
                    //------------------------------------

                    // debit acc_transaction
                    $payment_method = $this->input->post('payment_method');

                    $COAID = '102010202';


                    $b_levelDebit = array(
                        'VNo' => $voucher_no,
                        'Vtype' => $Vtype,
                        'VDate' => $VDate,
                        'Debit' => $paid_amount,
                        'Credit' => 0,
                        'COAID' => $COAID,
                        'level_id'      => $cHead->cHead,
                        'Narration' => "Amount received for invoice #".$voucher_no,
                        'IsPosted' => $IsPosted,
                        'CreateBy' => $CreateBy,
                        'CreateDate' => $createdate,
                        'IsAppove' => 1
                    );

                    $this->db->insert('acc_transaction', $b_levelDebit);
                    //------------------------------------

                }

                // C level notification

                $cNotificationData = array(
                    'notification_text' => 'Payment has been received for '.$package_id,
                    'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                    'created_by'        => $payment_data['user_id'],
                    'date'              => date('Y-m-d')
                );

                $this->db->insert('b_notification_tbl',$cNotificationData);

                //-------------------------


                // $this->Order_model->smsSend(

                //     $data = array(
                //         'customer_id' => $customer_id,
                //         'message'     => 'Payment has been done for Package ',
                //         'subject'     => 'Package payment'
                //     )

                // );


            }

            return $response;

        }

//for direct payment diduct


        $installment_response = (object)$this->gwapi->doSale($installment_payable_amount, $ccnumber, $ccexp, $cvv);
        $response['instalment_response'] = $installment_response;

        if($installment_response->response!='1')
        {
            return $response;
        }
//for direct payment diduct
        $NewDate=Date('m/d/Y', strtotime("+30 days"));

//for recurring
        $perameter['recurring'] = "add_subscription";
        $perameter['redirect_url'] = "https://www.google.com";
        $perameter['plan_id'] = $plan_id;
        $perameter['account_type'] = "savings";
        $perameter['entity_type'] = "business";
        $perameter['payment'] = "creditcard";
        $perameter['start_date'] = date("Ymd", strtotime($NewDate));
        $this->gwapi->sub_perameter($perameter);

        $recurring_response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

        $response['recurring_response'] = $recurring_response;
//for recurring



#----------------------------



#----------------------------
#   payment set database
#----------------------------

        if($installment_response->response=='1')
        {

            if(!empty($cHead->HeadCode)){

                $voucher_no = $package_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $payment_data['user_id'];
                $createdate = date('Y-m-d H:i:s');

                //customer credit insert acc_transaction
                $customerCredit = array(

                    'VNo'           => $voucher_no,
                    'Vtype'         => $Vtype,
                    'VDate'         => $VDate,
                    'Debit'         => 0,
                    'Credit'        => $paid_amount,
                    'COAID'         => $cAID,
                    'level_id'      => $cHead->cHead,
                    'Narration'     => "Customer ".$cAID." paid for invoice #".$voucher_no,
                    'IsPosted'      => $IsPosted,
                    'CreateBy'      => $CreateBy,
                    'CreateDate'    => $createdate,
                    'IsAppove'      => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------

                // debit acc_transaction
                $payment_method = $this->input->post('payment_method');

                $COAID = '102010202';


                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'level_id'      => $cHead->cHead,
                    'Narration' => "Amount received for invoice #".$voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $b_levelDebit);
                //------------------------------------

            }

            // C level notification

            $cNotificationData = array(
                'notification_text' => 'Payment has been received for '.$package_id,
                'go_to_url'         => 'retailer-invoice-receipt/'.$package_id,
                'created_by'        => $payment_data['user_id'],
                'date'              => date('Y-m-d')
            );

            $this->db->insert('b_notification_tbl',$cNotificationData);

            //-------------------------


            // $this->Order_model->smsSend(

            //     $data = array(
            //         'customer_id' => $customer_id,
            //         'message'     => 'Payment has been done for Package ',
            //         'subject'     => 'Package payment'
            //     )

            // );


        }

        return $response;
    }

    function payment_success()
    {
        $this->load->view('front_section/payment_success');
    }

    public function first_step_validation($user_type='r'){
        if($user_type == 'r'):
            $this->load->model('front_section/User_model');
            $this->form_validation->set_rules('first_name_2', 'First name', 'required|max_length[50]');
            $this->form_validation->set_rules('last_name_2', 'Last name', 'required|max_length[50]');
            $this->form_validation->set_rules('email_2', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
            $this->form_validation->set_rules('password_2', 'Password', 'required|max_length[32]');
            $this->form_validation->set_rules('confirm_2', 'Password', 'required|max_length[32]|matches[password_2]');
            $this->form_validation->set_rules('sub_domain_2', 'Sub Domain', 'required|is_unique[user_info.sub_domain]');
            if($this->form_validation->run() == true)
            {
                $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain_2'])->get()->result();
                if(!empty($reserve_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                    exit();
                }
    
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain_2'])->get()->result();
                if(!empty($sub_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                    exit();
                }
    
                if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain_2']))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                    exit();
                }
                echo json_encode(['status' => 200, 'msg' => '']);
                exit();
    
            }
            else
            {
                $error_arr = $this->form_validation->error_array();
                foreach($error_arr as $key => $msg)
                {
                    echo json_encode(['status' => 400, 'msg' => $msg]);
                    exit();
                    break;
                }
            }
        else:
            $this->load->model('front_section/User_model');
            $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
            $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
            $this->form_validation->set_rules('confirm', 'Password', 'required|max_length[32]|matches[password]');
            $this->form_validation->set_rules('sub_domain', 'Sub Domain', 'required|is_unique[user_info.sub_domain]');
            if($this->form_validation->run() == true)
            {
                $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();
                if(!empty($reserve_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                    exit();
                }
    
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
                if(!empty($sub_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                    exit();
                }
    
                if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                    exit();
                }
                echo json_encode(['status' => 200, 'msg' => '']);
                exit();
    
            }
            else
            {
                $error_arr = $this->form_validation->error_array();
                foreach($error_arr as $key => $msg)
                {
                    echo json_encode(['status' => 400, 'msg' => $msg]);
                    exit();
                    break;
                }
            }
        endif;
    }
 
}
