<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        // $this->load->view('welcome_message');
        $data['baseurl'] = $this->baseurl;
        $this->load->view('c_level/c_level_login', $data);
    }


    public function test(){

        $this->load->library('twilio');

        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();

            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = '+8801751194212';
            $message = "Hi tuhin, your order is successfully, order id  #120011";

            $response = $this->twilio->sms($from, $to, $message);     

            print_r($response);
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }    

    }

}
