<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Vendor;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Employee;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\TaxAgency;
use QuickBooksOnline\API\Facades\TaxService;
use QuickBooksOnline\API\Facades\TaxRate;
use QuickBooksOnline\API\Facades\Account;
use QuickBooksOnline\API\Facades\PurchaseOrder;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\Line;

/**
* 
* @file name   : Authentication_controller
* @Auther      : DM
* @Date        : 27-12-2019
* @Description : Quick books account Details
*
*/

Class Authentication_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/Supplier_model');
        $this->load->model('b_level/settings');
        $this->load->model('b_level/Category_model');
        $this->load->model('b_level/Product_model');
        $this->load->model('c_level/Setting_model');
    }
 
    /**
	* 
	* @function name : get_authentication
	* @description   : create a connection with quick books account
	* @return        : Array
	*
	*/
    public function get_authentication(){
        $user_id = $this->session->userdata('user_id');
        $time = date('Y-m-d H:i:s');
        $refresh_time = date('Y-m-d H:i:s',strtotime('+50 minutes',strtotime($time)));
        $quickbooksdata = $this->settings->getQuickBooksSettingData($user_id); 
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            if(empty($quickbooksdata)){
                $quickbook_setting_data = array(
                    'user_id'           =>  $user_id,
                    'client_id'         =>  $this->input->post('client_id'),
                    'client_secret'     =>  $this->input->post('client_secret'),
                    'updated_at'        =>  date('Y-m-d H:i:s'),
                    'refresh_time'      =>  $refresh_time,
                );
                $last_id = $this->settings->add_quickbooks_setting_data($quickbook_setting_data);
            }else{
                $quickbook_setting_data = array(
                    'user_id'           =>  $user_id,
                    'client_id'         =>  $this->input->post('client_id'),
                    'client_secret'     =>  $this->input->post('client_secret'),
                    'updated_at'        =>  date('Y-m-d H:i:s'),
                    'refresh_time'      =>  $refresh_time,
                );
                $this->settings->update_quickbooks_setting_data($quickbook_setting_data,$user_id);
            }
        }
        $quickbooksiddata = $this->settings->getQuickBooksSettingData($user_id); 

        $redirecturi = base_url()."quickbooks-authentication";

         $dataService = DataService::Configure(array(
            'auth_mode'    => 'oauth2',
            'ClientID'     =>  $quickbooksiddata->client_id,
            'ClientSecret' =>  $quickbooksiddata->client_secret,
            'RedirectURI'  =>  $redirecturi,
            'scope'        =>  "com.intuit.quickbooks.accounting",
            'baseUrl'      =>  "Development"
        ));

        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        if ( ! $this->input->get('code')) 
        {
            $lsAuthorizationUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();
            header('Location: ' .$lsAuthorizationUrl);
            exit;
        } 
        else
        {
            $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($_GET['code'], $_GET['realmId']);
            $quickbook_setting_data = array(
                'access_token_key'  =>  $accessTokenObj->getAccessToken(),
                'refresh_token_key' =>  $accessTokenObj->getRefreshToken(),
                'qbo_realm_id'      =>  $_GET['realmId'],
                'updated_at'        =>  date('Y-m-d H:i:s'),
                'refresh_time'      =>  $refresh_time,
                'status'            =>  1,
            );
            $this->settings->update_quickbooks_setting_data($quickbook_setting_data,$user_id);
            header('Location: '.$this->session->userdata('current_url'));
            exit;
        }
    }

    /**
	* 
	* @function name : storeCustomer
	* @description   : Store Customer data using quickbooks account
	* @return        : Array
	*
	*/
    public function storeCustomer()
    {
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $customer_error_array = array();
        $customer_success = array();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
              'auth_mode'       => 'oauth2',
              'ClientID'        => $quickbooks_data['client_id'],
              'ClientSecret'    => $quickbooks_data['client_secret'],
              'accessTokenKey'  => $quickbooks_data['access_token_key'],
              'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
              'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
              'baseUrl'         => "Development"
            ));
        
            //get customer data
            $customer_data = $this->Customer_model->get_all_customers($quickbooks_data['user_id']);         

            foreach($customer_data as $customer_key => $customer_val){

                $user_data = $this->settings->get_user_data($customer_val->created_by);
                $address = $customer_val->street_no.' '.$customer_val->address;
                if($user_data->user_type == 'b'){
                    $display_name =  $customer_val->company;
                }else{
                    $display_name = $customer_val->side_mark;
                }
    
              $theResourceObj = Customer::create([
                  "BillAddr" => [
                      "Line1" => $customer_val->address,
                      "City" => $customer_val->city,
                      "Country" => $customer_val->country_code,
                      "CountrySubDivisionCode" => $customer_val->state,
                      "PostalCode" => $customer_val->zip_code
                  ],
                  "GivenName" => $customer_val->first_name,
                  "FamilyName" => $customer_val->last_name,
                  "FullyQualifiedName" => $display_name,
                  "CompanyName" => $customer_val->company,
                  "DisplayName" => $display_name,
                  "PrimaryPhone" => [
                      "FreeFormNumber" => $customer_val->phone
                  ],
                  "PrimaryEmailAddr" => [
                      "Address" => $customer_val->email
                  ]
              ]);

              $resultingObj = $dataService->Add($theResourceObj);
              $error = $dataService->getLastError();
              if ($error) {
                  $customer_error_array[$customer_key]['status'] = 'error';
                  $customer_error_array[$customer_key]['status_message'] = $display_name.' '. $error->getResponseBody();
              }
              else {
                  $customer_error_array[$customer_key]['status'] = 'Success';
                  $customer_error_array[$customer_key]['status_message'] = "Created Id={$resultingObj->Id}. Reconstructed response body";
                    //update quickboooks id in category table
                    $category_data = $this->Customer_model->updateQuickbooksId($customer_val->customer_id,$resultingObj->Id);
                    $customer_status = $this->Customer_model->updateQuickbooksStatus($customer_val->customer_id);
                    log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
              }
            }
            echo '<pre>';
            print_r($customer_error_array);
            exit;
        }
    }

    /**
	* 
	* @function name : storeSuppliers
	* @description   : Store Suppliers data using quickbooks account
	* @return        : Array
	*
	*/
    public function storeSuppliers(){
        $suppliers_error_array = array();
        $suppliers_success = array();
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
              ));

              // get all the supplier
              $suppliers = $this->Supplier_model->getAllRemainingSuppliers($quickbooks_data['user_id']);
              foreach ($suppliers as $supp_key => $supplier) {
      
                  $theResourceObj = Vendor::create([
                      "BillAddr" => [
                          "Line1" => $supplier['address'],
                          "City" => $supplier['city'],
                          "Country" => $supplier['country_code'],
                          "CountrySubDivisionCode" => $supplier['country_code'],
                          "PostalCode" => $supplier['zip']
                      ],
                    //   "TaxIdentifier" => $supplier['supplier_no'],
                    //   "AcctNum" => $supplier['supplier_id'],
                      "GivenName" => $supplier['company_name'],
                      "FamilyName" => $supplier['supplier_name'],
                      "CompanyName" => $supplier['company_name'],
                      "DisplayName" => $supplier['company_name'],
                      "PrintOnCheckName" => $supplier['company_name'],
                      "Mobile" => [
                          "FreeFormNumber" => $supplier['phone']
                      ],
                      "PrimaryEmailAddr" => [
                          "Address" => $supplier['email']
                      ]
                  ]);
                  $resultingObj = $dataService->Add($theResourceObj);
                  $error = $dataService->getLastError();
                  if ($error) {
                      $suppliers_error_array[$supp_key]['status'] = 'error';
                      $suppliers_error_array[$supp_key]['status_message'] = $supplier['company_name'].' '.$error->getResponseBody();
                  }
                  else {
                      $suppliers_error_array[$supp_key]['status'] = 'success';
                      $suppliers_error_array[$supp_key]['status_message'] = "Created Id={$resultingObj->Id}. Reconstructed response body";
                      $supplier_data = $this->Supplier_model->updateQuickbooksId($supplier['supplier_id'],$resultingObj->Id);
                      $supplier_status = $this->Supplier_model->updateQuickbooksStatus($supplier['supplier_id']);
                      log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
                  }
              }
              echo '<pre>';
              print_r($suppliers_error_array);
              exit;
        }

    } 

    /**
	* 
	* @function name : createInvoice
	* @description   : Generate Invoice data using quickbooks account
	* @return        : Array
	*
	*/
    public function createInvoice()
    {
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $invoice_error_array = array();
        $invoice_success = array();

        $item_arr = array();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));

            //get user data
            $user_data = $this->settings->get_user_data($quickbooks_data['user_id']);
            if($user_data->user_type == 'c'){
                 //get all c-level invoice data
                $invoice_data = $this->settings->get_all_c_level_invoices_data($quickbooks_data['user_id']);
                // $get_tax_data = $this->settings->get_all_c_level_taxes($quickbooks_data['user_id']);
            }else{
                 //get all b-level invoice data
                $invoice_data = $this->settings->get_all_invoices_data($quickbooks_data['user_id']);
                // $get_tax_data = $this->settings->get_all_b_level_taxes($quickbooks_data['user_id']);
            }

           
            $quick_books_product = $dataService->FindbyId('CompanyInfo', $quickbooks_data['qbo_realm_id']);
          

            //get comapny state
            $company_state = $quick_books_product->CompanyAddr->CountrySubDivisionCode;
            foreach($invoice_data as $invoice_key => $invoice_value){
                $created_user_data = $this->settings->get_user_data($invoice_value->created_by);
                if($created_user_data->user_type == 'b'){
                    if($invoice_value->is_different_shipping == 1){
                        $address = $invoice_value->different_shipping_address;
                        $exploded_add = explode(',',$address);
                        $ship_address = $exploded_add[2];
                    }else{
                        $get_customer_data = $this->settings->get_customer_id_data($invoice_value->customer_id);
                        $get_company_profile_data = $this->settings->get_company_profile_data($get_customer_data['customer_user_id']);
                        $ship_address = $get_company_profile_data['state'];
                    }
                }
                
                $sub_total = $invoice_value->grand_total;
                $tax_amount = (($sub_total) * (8)) / 100; 
                $final_amount = $sub_total  +  $tax_amount;
                //get customer from quick boooks account
                $customer_data = $this->Customer_model->get_by_id($invoice_value->customer_id);
                if(!empty($customer_data)){
                    $quick_books_customer = $dataService->FindbyId('customer',$customer_data['quick_books_id']);
                  

                    if($customer_data['quick_books_id'] == $quick_books_customer->Id){
                        $customer_id = $quick_books_customer->Id;
                        //get customer state
                        $customer_state = $quick_books_customer->BillAddr->CountrySubDivisionCode;
                    }else{
                        $invoice_error_array[$invoice_key]['status'] = 'error';
                        $invoice_error_array[$invoice_key]['status_message'] = $dataService->getLastError();
                    }
                }else{
                    $invoice_error_array[$invoice_key]['status'] = 'error';
                    $invoice_error_array[$invoice_key]['status_message'] = 'Something went wrong!';
                }
                //due date
                $order_due_date = date('Y-m-d',strtotime($invoice_value->order_date. ' + 45 days'));
                $i = 0;
                if($user_data->user_type == 'c'){
                    //get all order data for c level
                    $order_data = $this->settings->get_orderd_details_by_id($invoice_value->order_id);
                }else{
                    //get all order data for b level
                    $order_data = $this->settings->get_b_level_orderd_details_by_id($invoice_value->order_id);
                }

                
                $lineArray = array();
                foreach($order_data as $order_key  => $order_val){
                    //get product data from database
                    $product_data = $this->settings->get_product_by_id($order_val['product_id']);
                    if(!empty($product_data)){
                        $quick_books_product = $dataService->FindbyId('item', $product_data['quick_books_id']);
                      
                        if($product_data['quick_books_id'] == $quick_books_product->Id){
                            $product_id = $quick_books_product->Id;
                            $product_name = $quick_books_product->Name;
                        }else{
                            $invoice_error_array[$invoice_key]['status'] = 'error';
                            $invoice_error_array[$invoice_key]['status_message'] = $dataService->getLastError();
                        }
                    }else{
                        $invoice_error_array[$invoice_key]['status'] = 'error';
                        $invoice_error_array[$invoice_key]['status_message'] = "Something went wrong";
                    }

                    if($company_state == $customer_state && $customer_state == $ship_address){
                        $tax = "TAX";
                    }else{
                        $tax = "NON";
                    }
                    $child_arr = array();
                    $discount = $order_val['discount'];
                    $discount_amount = (($order_val['list_price']) * ($discount)) / 100; 
                    $company_unit = $this->settings->company_unit();
                    $width_fraction = $this->db->where('id', $order_val['width_fraction_id'])->get('width_height_fractions')->row();
                    $height_fraction = $this->db->where('id', $order_val['height_fraction_id'])->get('width_height_fractions')->row();
                    if (isset($selected_rooms[$order_val['room']])) {
                        $selected_rooms[$order_val['room']] ++;
                    } else {
                        $selected_rooms[$order_val['room']] = 2;
                    }
                    $description = $order_val['pattern_name'].', W'.$order_val['width'].' '.$width_fraction->fraction_value.' '.strtoupper($company_unit).', H'.$order_val['height'].' '.$height_fraction->fraction_value.' '.strtoupper($company_unit).' '.$order_val['color_number'].' '.$order_val['color_name'].' '.$order_val['room'] .' 1' ;
                    //create line object of item
                    $LineObj = Line::create([
                        "LineNum" => ++$i,
                        "Description" => $description,
                        "Amount" => (float)$order_val['unit_price'],
                        "DetailType" => "SalesItemLineDetail",
                        "SalesItemLineDetail" => [
                            "ItemRef" => [
                                "value" => $product_id,
                                "name" => $product_name
                            ],
                            "UnitPrice" => $order_val['unit_price'],
                            "Qty" => $order_val['qty'],
                            "TaxCodeRef" => [
                                "value" => $tax
                            ]
                        ]
                    ]);
                    $lineArray[] = $LineObj;
                }
                //checking the company state and customer state
                if($company_state == $customer_state && $customer_state == $ship_address){
                    if($user_data->user_type == 'c'){
                        //get all taxes data for c level
                        $get_tax_data = $this->settings->get_all_c_level_taxes($quickbooks_data['user_id'],$company_state);
                    }else{
                        //get all taxes data for b level
                        $get_tax_data = $this->settings->get_all_b_level_taxes($quickbooks_data['user_id'],$company_state);
                    }

                    $quickbook_tax_code = $dataService->FindbyId('taxcode', $get_tax_data['quick_books_id']);
                    if(!empty($quickbook_tax_code)){
                        if($quickbook_tax_code->Id == $get_tax_data['quick_books_id']){
                            $tax_code_id = $quickbook_tax_code->Id;
                            $tax_rate_id = $quickbook_tax_code->SalesTaxRateList->TaxRateDetail->TaxRateRef;
                            //get tax reference data from quick books account
                            $quickbook_tax_ref = $dataService->FindbyId('taxrate', $tax_rate_id);
                            $state_name =  $quickbook_tax_ref->Name;
                            $rate_value =  $quickbook_tax_ref->RateValue;
                        }
                    }else{
                        $invoice_error_array[$invoice_key]['status'] = 'error';
                        $invoice_error_array[$invoice_key]['status_message'] = "Tax data not found!";
                    }

                    $theResourceObj = Invoice::create([
                        "DocNumber" =>  $invoice_value->order_id,
                        "TxnDate" =>  $invoice_value->order_date,
                        "DueDate" =>  $order_due_date,
                        "Line" => $lineArray,
                        "CustomerRef"=> [
                              "value"=> $customer_id
                        ],
                        "TxnTaxDetail" => [
                            "TxnTaxCodeRef" => [
                                "value" => $tax_code_id
                            ],
                            "TaxLine" => [
                                [
                                    "DetailType" => "TaxLineDetail",
                                    "TaxLineDetail" => [
                                        "TaxRateRef" => [
                                            "value" => $tax_rate_id
                                        ],
                                        "PercentBased" => true,
                                    ]
                                ],
                            ]
                        ],
                    ]);
                }else{
                    $theResourceObj = Invoice::create([
                        "DocNumber" =>  $invoice_value->order_id,
                        "TxnDate" =>  $invoice_value->order_date,
                        "DueDate" =>  $order_due_date,
                        "Line" => $lineArray,
                        "CustomerRef"=> [
                              "value"=> $customer_id
                        ],
                        "TxnTaxDetail" => [
                            "TaxLine" => [
                                [
                                    "DetailType" => "TaxLineDetail",
                                    "TaxLineDetail" => [
                                        "TaxRateRef" => [
                                            "value" => 2
                                        ],
                                        "PercentBased" => true,
                                    ]
                                ],
                            ]
                        ],
                    ]);
                }
                $resultingObj = $dataService->Add($theResourceObj);
                $error = $dataService->getLastError();
                if ($error) {
                    $invoice_error_array[$invoice_key]['status'] = 'error';
                    $invoice_error_array[$invoice_key]['status_message'] = $error->getResponseBody();
                }else{
                    $invoice_error_array[$invoice_key]['status'] = 'Success';
                    $invoice_error_array[$invoice_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                    if($user_data->user_type == 'c'){
                        $invoice_data = $this->settings->updateInvoiceQuickbooksId($invoice_value->id,$resultingObj->Id);
                        $invoice_status = $this->settings->updateInvoiceQuickbooksStatus($invoice_value->id);
                    }else{
                        $invoice_data = $this->settings->updateBLevelInvoiceQuickbooksId($invoice_value->id,$resultingObj->Id);
                        $invoice_status = $this->settings->updateBLevelInvoiceQuickbooksStatus($invoice_value->id);
                    }
                    log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
                }
            }
        }
        echo '<pre>';
        print_r($invoice_error_array);
        die;
    }

    /**
	* 
	* @function name : createTaxAgency
	* @description   : Store Tax Agency data in quickbooks account
	* @return        : Array
	*
	*/
    public function createTaxAgency(){
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $tax_agency_error_array = array();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));

            //get state data 
            $state_data = $this->settings->get_all_state($quickbooks_data['user_id']);

            foreach($state_data as $state_key => $state_val){
                $theResourceObj = TaxAgency::create([
                    'DisplayName' => $state_val['state_name']
                ]);
                
                //save tax agency in quickbooks account
                $resultingObj = $dataService->Add($theResourceObj);
                $error = $dataService->getLastError();
                if ($error) {
                    $tax_agency_error_array[$state_key]['status'] = 'error';
                    $tax_agency_error_array[$state_key]['status_message'] = $state_val['state_name'].' '.$error->getResponseBody();
                } else {
                    $tax_agency_error_array[$state_key]['status'] = 'Success';
                    $tax_agency_error_array[$state_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                    //update quickboooks id in city_state_tbl table
                    $tax_agency_data = $this->settings->updateTaxAgencyQuickbooksId($state_val['id'],$resultingObj->Id);
                    $tax_agency_status =  $this->settings->updateTaxAgencyQuickbooksStatus($state_val['id']);
                    log_message("info","Created Id={$resultingObj->id}. Reconstructed response body");
                }
            }
        }
        echo '<pre>';
        print_r($tax_agency_error_array);
        die;
    }

    /**
	* 
	* @function name : createTaxService
	* @description   : Store Tax Service data in quickbooks account
	* @return        : Array
	*
	*/
    public function createTaxService()
    {
            $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
            $tax_service_error_array = array();
            foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
                $dataService = DataService::Configure(array(
                    'auth_mode'       => 'oauth2',
                    'ClientID'        => $quickbooks_data['client_id'],
                    'ClientSecret'    => $quickbooks_data['client_secret'],
                    'accessTokenKey'  => $quickbooks_data['access_token_key'],
                    'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                    'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                    'baseUrl'         => "Development"
                ));
                //get user data
                $user_data = $this->settings->get_user_data($quickbooks_data['user_id']);
                if($user_data->user_type == 'c'){
                    //get all c-level tax rate data
                    $tax_rate_data = $this->settings->get_all_c_level_state_rates($quickbooks_data['user_id']);
                }else{
                    //get all b-level tax rate data
                    $tax_rate_data = $this->settings->get_all_b_level_state_rates($quickbooks_data['user_id']);
                }
                $TaxRateDetails = array();
                $tax_agency_data = $this->settings->get_tax_agency_data($quickbooks_data['user_id']);
                foreach ($tax_agency_data as $tax_agency_key => $tax_agency_value) {
                    $quick_books_tax_agency = $dataService->FindbyId('taxagency',$tax_agency_value['quick_books_id']);
                    if($tax_agency_value['quick_books_id'] == $quick_books_tax_agency->Id){
                        $tax_agency_id = $quick_books_tax_agency->Id;
                    } 
                }
                $currentTaxServiceDetail = TaxRate::create([
                    "TaxRateName" => $tax_rate_data['state_name'],
                    "RateValue" =>  $tax_rate_data['tax_rate'],
                    "TaxAgencyId" => $tax_agency_id,
                    "TaxApplicableOn" => "Sales"
                ]);
                $TaxRateDetails[] = $currentTaxServiceDetail;
                $theResourceObj = TaxService::create([
                    "TaxCode" => $tax_rate_data['state_name'],
                    "TaxRateDetails" => $TaxRateDetails
                ]);
                    //store quickbooks tax service in quick books account
                $resultingObj = $dataService->Add($theResourceObj);
                $error = $dataService->getLastError();
                if ($error) {
                    $tax_service_error_array[$tax_key]['status'] = 'error';
                    $tax_service_error_array[$tax_key]['status_message'] = $tax_rate_data['state_id'].' '.$error->getResponseBody();
                } else {
                    $tax_service_error_array[$tax_key]['status'] = 'Success';
                    $tax_service_error_array[$tax_key]['status_message'] =  "Created Id={$resultingObj->TaxService->TaxCodeId}. Reconstructed response body";
                    
                    if($user_data->user_type == 'c'){
                        $tax_service_data = $this->settings->updateClevelTaxServiceQuickbooksId($tax_rate_data['state_id'],$resultingObj->TaxService->TaxCodeId);
                        $tax_service_status = $this->settings->updateClevelTaxServiceQuickbooksStatus($tax_rate_data['state_id']);
                    }else{
                        $tax_service_data = $this->settings->updateTaxServiceQuickbooksId($tax_rate_data['state_id'],$resultingObj->TaxService->TaxCodeId);
                        $tax_service_status = $this->settings->updateTaxServiceQuickbooksStatus($tax_rate_data['state_id']);
                    }
                    log_message("info","Created Id={$resultingObj->TaxService->TaxCodeId}. Reconstructed response body");
                }
            }
            echo  '<pre>';
            print_r($tax_service_error_array);
            die;
    }
   
    /**
	* 
	* @function name : storeEmployee
	* @description   : Store Employee data in quickbooks account
	* @return        : Array
	*
	*/
    public function storeEmployee(){
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $employee_error_array = array();

        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
        
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));

            //get employee data
            $employee_data = $this->settings->get_all_employee($quickbooks_data['user_id']);
            foreach($employee_data  as $emp_key => $emp_val){
                $theResourceObj = Employee::create([
                    "PrimaryAddr" => [
                        "Line1" => $emp_val['address'],
                    ],
                    "GivenName" => $emp_val['first_name'],
                    "FamilyName" => $emp_val['last_name'],
                    "DisplayName" => $emp_val['fullname'],
                    "PrintOnCheckName" => $emp_val['fullname'],
                    "PrimaryPhone" => [
                        "FreeFormNumber" => $emp_val['phone']
                    ],
                    "PrimaryEmailAddr" => [
                        "Address" => $emp_val['email']
                    ]
                ]);
                $resultingObj = $dataService->Add($theResourceObj);
                    $error = $dataService->getLastError();
                    if ($error) {
                    $employee_error_array[$emp_key]['status'] = 'error';
                    $employee_error_array[$emp_key]['status_message'] = $emp_val['fullname'].' '.$error->getResponseBody();
                } else {
                    $employee_error_array[$emp_key]['status'] = 'Success';
                    $employee_error_array[$emp_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                    $employee_success[] = $emp_val['id'];
                    $employees_status =  $this->settings->updateEmployeeQuickbooksId($emp_val['id'],$resultingObj->Id);
                    $employees_status =  $this->settings->updateEmployeeQuickbooksStatus($emp_val['id']);
                    log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
                }
            }
            //update employee status
            echo '<pre>';
            print_r($employee_error_array);
            exit;    
        }
    }

    /**
	* 
	* @function name : storeCategory
	* @description   : Store Category data in quickbooks account
	* @return        : Array
	*
	*/
    public function storeCategory(){
       
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $category_error_array = array();
        $category_success = array();

        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));

            //get category data
            $category_data = $this->Category_model->get_all_category($quickbooks_data['user_id']);
            foreach($category_data as $category_key => $category_val){
                $theResourceObj = Item::create([
                    "SubItem" => false, 
                    "Type"    => "Category", 
                    "Name"    => $category_val['category_name']
                ]);
                //save category in quickbooks account
                $resultingObj = $dataService->Add($theResourceObj);
                $error = $dataService->getLastError();
                if ($error) {
                    $category_error_array[$category_key]['status'] = 'error';
                    $category_error_array[$category_key]['status_message'] = $category_val['category_name'].' '.$error->getResponseBody();
                } else {
                    $category_error_array[$category_key]['status'] = 'Success';
                    $category_error_array[$category_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                    //update quickboooks id in category table
                    $category_data = $this->Category_model->updateQuickbooksId($category_val['category_id'],$resultingObj->Id);
                    $category_status =  $this->Category_model->updateQuickbooksStatus($category_val['category_id']);
                    log_message("info","Created Id={$resultingObj->id}. Reconstructed response body");
                }
            }
        }
        echo '<pre>';
        print_r($category_error_array);
        exit;  
    }

  	/**
	* 
	* @function name : storeProduct
	* @description   : Store product data in quickbooks account
	* @return        : Array
	*
	*/
    public function storeProduct(){
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();

        $product_error_array = array();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
                $dataService = DataService::Configure(array(
                    'auth_mode'       => 'oauth2',
                    'ClientID'        => $quickbooks_data['client_id'],
                    'ClientSecret'    => $quickbooks_data['client_secret'],
                    'accessTokenKey'  => $quickbooks_data['access_token_key'],
                    'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                    'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                    'baseUrl'         => "Development"
                ));

                //get all product data 
                $product_data = $this->settings->get_product_data($quickbooks_data['user_id']);
                foreach($product_data as $product_key => $product_val){
                    //get category data
                    $category_data = $this->Category_model->get_all_category_quick_book_id($quickbooks_data['user_id'],$product_val->category_id);
                    if(!empty($category_data)){
                        $item = $dataService->FindbyId('item', $category_data['quick_books_id']);
                        $error = $dataService->getLastError();
                        if($error){
                            $product_error_array[$product_key]['status'] = 'error';
                            $product_error_array[$product_key]['status_message'] = $product_val->product_name.' '.$error->getResponseBody();
                        }
                        else if($item->Id == $category_data['quick_books_id']){
                            $item_id = $item->Id;
                            $item_name = $item->Name;
                            $theResourceObj = Item::create([
                                "Name" => $product_val->product_name,
                                "SubItem" => true,
                                "ParentRef" => [
                                    "value" => $item_id,
                                    "name"  => $item_name
                                ],
                                "IncomeAccountRef" => [
                                    "value" => 79,
                                    "name" => "Sales of Product Income"
                                ],
                                "ExpenseAccountRef" => [
                                    "value" =>"80",
                                    "name" => "Cost of Goods Sold"
                                ],
                                "Type" => "NonInventory",
                            ]);
                            $resultingObj = $dataService->Add($theResourceObj);
                            $error = $dataService->getLastError();
                            if ($error) {
                                $product_error_array[$product_key]['status'] = 'error';
                                $product_error_array[$product_key]['status_message'] = $product_val->product_name.' '.$error->getResponseBody();
                            } else {
                                $product_error_array[$product_key]['status'] = 'Success';
                                $product_error_array[$product_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                                $product_status =  $this->settings->updateproductQuickbooksStatus($product_val->product_id);
                                //update quickboooks id in product table
                                $product_data = $this->settings->updateProductQuickbooksId($product_val->product_id,$resultingObj->Id);
                                log_message("info","Created Id={$resultingObj->id}. Reconstructed response body");
                            }
                        }else{
                            $product_error_array[$product_key]['status'] = 'error';
                            $product_error_array[$product_key]['status_message'] = $dataService->getLastError();
                        }
                    }else{
                        $product_error_array[$product_key]['status'] = 'error';
                        $product_error_array[$product_key]['status_message'] = 'Category data not found!';
                    }
                }
            }
        echo '<pre>';
        print_r($product_error_array);
        die;
    }

    /**
	* 
	* @function name : PurchaseOrder
	* @description   : Store Purchase order data in quickbooks account
	* @return        : Array
	*
    */
    public function PurchaseOrder(){
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();

        $purchase_order_error_array = array();

        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));

            //get purchase deatils from database
            $purchase_details = $this->settings->get_purchase_order_data($quickbooks_data['user_id']);
            foreach($purchase_details as $purchase_key => $purchase_val){
                //get supplier data
                $supplier_data = $this->settings->check_supplier_data($purchase_val['supplier_id'],$quickbooks_data['user_id']);
                //get supplier data from quick books account
                if(!empty($supplier_data)){
                    $vendor = $dataService->FindbyId('vendor', $supplier_data['quick_books_id']);
                    if($vendor->Id == $supplier_data['quick_books_id']){
                        $supplier_id = $vendor->Id;
                        $supplier_name = $vendor->DisplayName;
                    }else{
                        $purchase_order_error_array[$purchase_key]['status'] = 'error';
                        $purchase_order_error_array[$purchase_key]['status_message'] = 'Supplier data not found!';
                    }
                }else{
                    $purchase_order_error_array[$purchase_key]['status'] = 'error';
                    $purchase_order_error_array[$purchase_key]['status_message'] = 'Something went wrong!';
                }
                
                $lineArray = array();
                $i = 0;
                //get purchase deatils data from database
                $puchase_details_data = $this->settings->get_purchase_order_details_data($purchase_val['purchase_id']);
                foreach($puchase_details_data as $purchase_details_key => $purchase_details_val){
                    //getting product data
                    $product_data = $this->settings->get_product_data_by_pattern_id($purchase_details_val['pattern_model_id'],$quickbooks_data['user_id']);
                    if(!empty($product_data)){
                        $quick_books_product = $dataService->FindbyId('item', $product_data['quick_books_id']);
                        if($quick_books_product->Id == $product_data['quick_books_id']){
                            $item_id = $quick_books_product->Id;
                            $item_name = $quick_books_product->Name;
                            $description = $purchase_details_val['pattern_name'].' '.$purchase_details_val['color_name'];
                            $LineObj = Line::create([
                                "LineNum" => ++$i,
                                "Description" => $description,
                                "Amount" => $purchase_val['grand_total'],
                                "DetailType" => "ItemBasedExpenseLineDetail", 
                                "ItemBasedExpenseLineDetail" => [
                                    "BillableStatus" => "NotBillable", 
                                    "ItemRef" => [
                                        "value" => $item_id, 
                                        "name" => $item_name 
                                    ], 
                                    "UnitPrice" => $purchase_details_val['list_price'], 
                                    "Qty" => $purchase_details_val['qty']
                                ] 
                            ]);
                            $lineArray[] = $LineObj;
                        }else{
                            $purchase_order_error_array[$purchase_key]['status'] = 'error';
                            $purchase_order_error_array[$purchase_key]['status_message'] = 'Product data not found!';
                        }
                    }else{
                            $purchase_order_error_array[$purchase_key]['status'] = 'error';
                            $purchase_order_error_array[$purchase_key]['status_message'] = 'Something went wrong!';
                    }
                }
                $theResourceObj = PurchaseOrder::create([
                    "Line" => $lineArray, 
                    "VendorRef" => [
                            "value" => $supplier_id, 
                            "name" => $supplier_name
                        ], 
                    "APAccountRef" => [
                            "value" => "33",
                            "name" => "Accounts Payable (A/P)"
                        ],
                    "TotalAmt" => $purchase_val['grand_total']
                ]);
                $resultingObj = $dataService->Add($theResourceObj);
                $error = $dataService->getLastError();
                if ($error) {
                    $purchase_order_error_array[$purchase_key]['status'] = 'error';
                    $purchase_order_error_array[$purchase_key]['status_message'] = $error->getResponseBody();
                } else {
                    $purchase_order_error_array[$purchase_key]['status'] = 'Success';
                    $purchase_order_error_array[$purchase_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                    //update purchase tbl quick book status
                    $purhase_status = $this->settings->updatePurchaseOrderQuickbooksStatus($purchase_val['id']);
                    //update purchase tbl quick books id
                    $purhase_order_data = $this->settings->updatePurchaseOrderQuickbooksId($purchase_val['id'],$resultingObj->Id);
                    log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
                }
            }
        }
        echo '<pre>';
        print_r($purchase_order_error_array);
        die;
    }

    /**
	* 
	* @function name : ManageCommissionList
	* @description   : Manage Commission data in quickbooks account
	* @return        : Array
	*
    */
    public function ManageCommissionList(){
        $get_all_quickbooks_data = $this->settings->getAllQuickBooksSettingData();
        $purchase_error_array = array();
        $commission_id_array = array();
        foreach($get_all_quickbooks_data as $quickbooks_key => $quickbooks_data){
            $dataService = DataService::Configure(array(
                'auth_mode'       => 'oauth2',
                'ClientID'        => $quickbooks_data['client_id'],
                'ClientSecret'    => $quickbooks_data['client_secret'],
                'accessTokenKey'  => $quickbooks_data['access_token_key'],
                'refreshTokenKey' => $quickbooks_data['refresh_token_key'],
                'QBORealmID'      => $quickbooks_data['qbo_realm_id'],
                'baseUrl'         => "Development"
            ));
            $today_date = date('Y-m-d');
            $last_date_of_month = date("Y-m-t");
            //get user data
            $user_data = $this->settings->get_user_data($quickbooks_data['user_id']);
            if($user_data->user_type == 'c'){
                //get c level commission data 
                $commission_list_data = $this->settings->get_c_level_commision_list($quickbooks_data['user_id']);
            }else{
                //get b level commission data 
                $commission_list_data = $this->settings->get_b_level_commision_list($quickbooks_data['user_id']);
            }

            foreach($commission_list_data as $commission_list_data_key => $commission_list_data_val){
                //get employee data 
                $employee_data = $this->settings->check_employee_data($commission_list_data_val['created_by']);
                if(!empty($employee_data)){
                    //get employee data from quickbooks account
                    $quickbook_employee_data = $dataService->FindbyId('employee', $employee_data['quick_books_id']);
                    //comparing quickbooks employee data with bms employee data
                    if($quickbook_employee_data->Id == $employee_data['quick_books_id']){
                        $employee_id = $quickbook_employee_data->Id;
                        $employee_name = $quickbook_employee_data->DisplayName;
                        if($today_date == $last_date_of_month){
                            if($commission_list_data_val['total_commission'] > 0){
                                $theResourceObj = Purchase::create([
                                    "AccountRef" => [
                                        "value" => "35", 
                                        "name" => "Checking" 
                                    ], 
                                    "PaymentType" => "Check", 
                                    "EntityRef" => [
                                        "value" => $employee_id,
                                        "name" => $employee_name
                                    ],
                                    "Line" => [
                                        [
                                            "Amount" => $commission_list_data_val['total_commission'], 
                                            "DetailType" => "AccountBasedExpenseLineDetail", 
                                            "AccountBasedExpenseLineDetail" => [
                                                "AccountRef" => [
                                                    "name" => "Commissions & fees", 
                                                    "value" => "9" 
                                                ] 
                                            ] 
                                        ] 
                                    ]
                                ]); 
                                
                                $resultingObj = $dataService->Add($theResourceObj);
                                $error = $dataService->getLastError();
                                if ($error) {
                                    $purchase_error_array[$commission_list_data_key]['status'] = 'error';
                                    $purchase_error_array[$commission_list_data_key]['status_message'] = $error->getResponseBody();
                                } else {
                                    $purchase_error_array[$commission_list_data_key]['status'] = 'Success';
                                    $purchase_error_array[$commission_list_data_key]['status_message'] =  "Created Id={$resultingObj->Id}. Reconstructed response body";
                                    if($user_data->user_type == 'c'){
                                        //update commission status in quatation table
                                        $purhase_status = $this->settings->updateclevelCommissionQuickbooksStatus(explode(',',$commission_list_data_val['quatation_id']));
                                    }else{
                                        //update commission status in b_level_quatation_tbl table
                                        $purhase_status = $this->settings->updateblevelCommissionQuickbooksStatus(explode(',',$commission_list_data_val['quatation_id']));
                                    }
                                    log_message("info","Created Id={$resultingObj->Id}. Reconstructed response body");
                                }
                            }else{
                                $purchase_error_array[$commission_list_data_key]['status'] = 'error';
                                $purchase_error_array[$commission_list_data_key]['status_message'] = "Your commission is less then 0.!"; 
                            }
                        }else{
                            $purchase_error_array[$commission_list_data_key]['status'] = 'error';
                            $purchase_error_array[$commission_list_data_key]['status_message'] = "Today is not last day of the month!"; 
                        }
                    }
                }else{
                    $purchase_error_array[$commission_list_data_key]['status'] = 'error';
                    $purchase_error_array[$commission_list_data_key]['status_message'] = "Employee data not found!";
                }
            }
        }
        echo '<pre>';
        print_r($purchase_error_array);
        die;
    }
    
    /**
	* 
	* @function name : refershAccessToken
	* @description   : Refresh token using quick books accont
	* @return        : Array
	*
	*/
    public function refershAccessToken(){
        $current_time = date('Y-m-d H:i:s');
        $refresh_time = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($current_time)));

        //get quickbooks setting data
        $get_quickbooks_setting_data = $this->settings->getQuickBooksRefreshTimeData($current_time,$refresh_time);
        if(!empty($get_quickbooks_setting_data) && count($get_quickbooks_setting_data) > 0){
            $success_array = array();
            foreach($get_quickbooks_setting_data as $key => $val){
                //get new access token and refersh token
                $oauth2LoginHelper = new OAuth2LoginHelper($val['client_id'],$val['client_secret']);
                $accessTokenObj = $oauth2LoginHelper->refreshAccessTokenWithRefreshToken($val['refresh_token']);
                $accessTokenValue = $accessTokenObj->getAccessToken();
                $refreshTokenValue = $accessTokenObj->getRefreshToken();
                 //store the value in database
                $quickbook_setting_data = array(
                    'access_token_key'  =>  $accessTokenValue,
                    'refresh_token_key' =>  $refreshTokenValue,
                    'refresh_time'      => $refresh_time,
                    'updated_at'      => $current_time,
                );
                $this->settings->update_quickbooks_setting_data($quickbook_setting_data,$val['user_id']);
            }
        }
    }

    /**
	* 
	* @function name : revokeToken
	* @description   : Disconnected from quick books account
	* @return        : Array
	*
	*/
    public function revokeToken(){
        $client_id = $this->input->post('client_id');
        $user_id = $this->input->post('hidden_user_id');
        $user_type = $this->input->post('hidden_user_type');
        $client_secret = $this->input->post('client_secret');
        $oauth2LoginHelper = new OAuth2LoginHelper($client_id,$client_secret);
        //get quickbooks data
        $quickbooksdata = $this->settings->getQuickBooksSettingData($user_id);
        //disconnected from quickbooks account
        $revokeResult = $oauth2LoginHelper->revokeToken($quickbooksdata->refresh_token_key);
        //check disconnected result
        if($revokeResult){
            $change_data = array(
                'status'    =>  0,
                'access_token_key'    =>  '',
                'refresh_token_key'    =>  '',
            );
            $this->settings->update_quickbooks_setting_data($change_data,$user_id);
            $data['connection'] = "Connect";
            $data["action"] = base_url().'quickbooks/Authentication_controller/get_authentication';
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Quickbooks Disconnected successfully.!</div>");
        }else{
            $data['connection'] = "Connected";
            $data["action"] = base_url().'quickbooks/Authentication_controller/revokeToken';
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something getting wrong.!Please try again.!</div>");
        }
        $data["quickbooks_setting_data"] = $this->settings->getQuickBooksSettingData($user_id);
        $data['user_id']   =  $user_id;
        $data['user_type'] =  $user_type;
        if($user_type == "b"){
            redirect('wholeseler-store-quickbooks-settings');
        }else{
            redirect('retailer-store-quickbooks-settings');
        }

    }

    /**
	* 
	* @function name : storeRetailerQuickbookSetting
	* @description   : Store retailer Quick Book Settings
	* @return        : Array
	*
	*/
    public function storeRetailerQuickbookSetting(){
        $user_id = $this->session->userdata('user_id');
        //get quickbooks data
        $quickbooksdata = $this->settings->getQuickBooksSettingActiveData($user_id); 
        if(!empty($quickbooksdata)){
            if($quickbooksdata->status == 1){
                $data['connection'] = "Connected";
                $data["action"] = 'quickbooks/Authentication_controller/revokeToken';
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Quickbooks Connected successfully.!</div>");
            }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
            }
        }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something getting wrong.!Please try again.!</div>");
        }
        $data['user_id']   =  $user_id;
        $data['user_type'] =  $this->session->userdata('user_type');
        $data["quickbooks_setting_data"] = $quickbooksdata;
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/quickbooks_setting',$data);
        $this->load->view('c_level/footer'); 
    }

    /**
	* 
	* @function name : storeQuickbookSetting
	* @description   : Store wholeselar Quick Book Settings
	* @return        : Array
	*
	*/
    public function storeQuickbookSetting(){
        $user_id = $this->session->userdata('user_id');
        //get quickbooks data
        $quickbooksdata =$this->settings->getQuickBooksSettingActiveData($user_id);  
        if(!empty($quickbooksdata)){
            if($quickbooksdata->status == 1){
                $data['connection'] = "Connected";
                $data["action"] = 'quickbooks/Authentication_controller/revokeToken';
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Quickbooks Connected successfully.!</div>");
            }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
            }
        }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something getting wrong.!Please try again.!</div>");
        }
        $data['user_id']   =  $user_id;
        $data['user_type'] =  $this->session->userdata('user_type');
        $data["quickbooks_setting_data"] = $quickbooksdata;
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/settings/quickbooks_setting',$data);
        $this->load->view('b_level/footer'); 
    }
}
?>        