<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Switch_controller extends CI_Controller
{
    function b_level_redirection($user_id,$token)
    {
        $this->load->model('b_level/Auth_model');
        $user_detail = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
        
        if($token==$user_detail->login_token) 
        {


            $checkPermission = $this->Auth_model->userPermission($user_id);
            //dd($checkPermission);

            if ($checkPermission != NULL) {

                $permission = array();
                $permission1 = array();

                if (!empty($checkPermission)) {
                    foreach ($checkPermission as $value) {

                        $permission[$value->module] = array(
                            'create' => $value->create,
                            'read' => $value->read,
                            'update' => $value->update,
                            'delete' => $value->delete
                        );
                        //                   dd($permission);
                        $permission1[$value->menu_title] = array(
                            'create' => $value->create,
                            'read' => $value->read,
                            'update' => $value->update,
                            'delete' => $value->delete
                        );
                    }
                }
            } // END PERMISSION

            
                $c = $this->db->select('currency,country_code')
                    ->from('company_profile')
                    ->where('created_by', $user_id)
                    ->get()->row();


                    $user_info = $this->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->row();

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => (($user_info->is_admin == 1) ? true : false),
                    'user_type' => $user_detail->user_type,
                    'user_id' => $user_id,
                    'currency' => $c->currency,
                    'country_code' => $c->country_code,
                    'admin_created_by' => $user_detail->created_by,
                    'name' => $user_detail->first_name . " " . $user_detail->last_name,
                    'email' => $user_detail->email,
                    'permission' => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                );

                // PUT THE USER DATA IN SESSION
                $this->session->set_userdata($session_data);

                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );

                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);

                $get_favorites = $this->db->select('*')->from('b_favorites')->where('user_id', $this->session->userdata('user_id'))->order_by('ordering','ASC')->get()->result_array();
                if (empty($get_favorites)) {
                    $get_favorites = array();
                }
                $this->session->set_userdata('favorite_sidebar', $get_favorites);


                $http_host = explode('.', $_SERVER['HTTP_HOST'])[1];
                $ext = explode('.', $_SERVER['HTTP_HOST'])[2];
                redirect($_SERVER['REQUEST_SCHEME'].'://'.$user_detail->sub_domain.'.'.$http_host.'.'.$ext.'/wholesaler-dashboard');

        }else{
            echo "Invalid Url";die();
        }
    }







    function c_level_redirection($user_id,$token)
    {
        $this->load->model('Auth_model');
        $user_detail = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
        
        if($token==$user_detail->login_token) 
        {


            $checkPermission = $this->Auth_model->userPermission2($user_id);
            //dd($checkPermission);

            if ($checkPermission != NULL) {

                $permission = array();
                $permission1 = array();
                if (!empty($checkPermission)) {
                    foreach ($checkPermission as $value) {
                        $permission[$value->module] = array(
                            'create' => $value->create,
                            'read' => $value->read,
                            'update' => $value->update,
                            'delete' => $value->delete
                        );

                        $permission1[$value->menu_title] = array(
                            'create' => $value->create,
                            'read' => $value->read,
                            'update' => $value->update,
                            'delete' => $value->delete
                        );
                    }
                }
            }


            $main_b_id = "";
            if(empty($user_detail->created_by))
                $main_b_id = $user_detail->id;
            else{
                $b_user = $this->db->where('id',$user_detail->created_by)->get('user_info')->row();
                $main_b_id = $b_user->id;
            }


            $user_info = $this->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->row();


            if ($user_info->is_admin == 1) {

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => (($user_info->is_admin == 1) ? true : false),
                    'user_type' => $user_detail->user_type,
                    'user_id' => $user_id,
                    'admin_created_by' => $user_detail->created_by,
                    'name' => $user_detail->first_name . " " . $user_detail->last_name,
//                'username' => $user->username,
                    'email' => $user_detail->email,
                    'picture' => '',
                    'permission' => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                    'main_b_id' => $main_b_id,
                );

                
                //echo 'Admin -><pre>';                   print_r($session_data); die();
                $this->session->set_userdata($session_data);

                // Get favorites menus
                $get_favorites = $this->db->select('*')->from('c_favorites')->where('user_id', $this->session->userdata('user_id'))->order_by('ordering','ASC')->get()->result_array();
                if (empty($get_favorites)) {
                    $get_favorites = array();
                }
                $this->session->set_userdata('favorite_sidebar', $get_favorites);

                //            ========= its for last login info collect start ============
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );
                //echo '<pre>';            print_r($last_login_info); die();
                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);

               }else if ($user_info->is_admin == 2 || $user_info->is_admin == 3) {

                $session_data = array(
                    'isLogIn'       => true,
                    'isAdmin'       => '', //(($user->is_admin == 2) ? true : false),
                    'user_type'         => $user_detail->user_type,
                    'user_id'           => $user_id,
//                    'user_created_by' => $user->created_by,
                    'admin_created_by' => $user_detail->created_by,
                    'name'              => $user_detail->first_name . " " . $user_detail->last_name,
//                'username' => $user->username,
                    'email'             => $user_detail->email,
                    'picture'           => '',
                    'permission'        => json_encode(@$permission),
                    'label_permission' => json_encode(@$permission1),
                    'logged_in'         => TRUE,
                    'session_id'        => session_id(),
                    'main_b_id' => $main_b_id,
                );
                //  echo 'User -><pre>';                   print_r($session_data); die();
                $this->session->set_userdata($session_data);
                //            ========= its for last login info collect start ============
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );
//            echo '<pre>';            print_r($last_login_info); die();
                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);
//            ========= its for last login info collect close============ 

            }

                $http_host = explode('.', $_SERVER['HTTP_HOST'])[1];
                $ext = explode('.', $_SERVER['HTTP_HOST'])[2];
                redirect($_SERVER['REQUEST_SCHEME'].'://'.$user_detail->sub_domain.'.'.$http_host.'.'.$ext.'/retailer-dashboard');

        }else{
            echo "Invalid Url";die();
        }
    }

}
