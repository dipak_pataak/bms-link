<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Item;
/**
 * Qb_test
 *
 * @author Pavel Espinal
 */
class Qb_tests extends CI_Controller {
    
    /**
     * @var QuickBooksOnline\API\DataService\DataService
     */
    private $dataService = null;
    
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // First of all, take a look at the examples here:
        // [installation path]vendor/quickbooks/v3-php-sdk/src/_Samples/
        // 
        // and from now on, keep an eye here:
        // https://intuit.github.io/QuickBooks-V3-PHP-SDK/overview.html
        
        // This library will make our life easier.
        // On our marks...
      //  $this->load->library('quickbooks');
        
        // Set...
        $this->dataService = $this->quickbooks->getDataService();
        $this->dataService->setLogLocation("/tmp/QB-tests.log");
        $this->data = array();
    }
    
    public function index() {
        // Go...
        $this->list_vendors();
     //   $this->create_invoice();
     //   $this->verify_invoice();
    }
    
    /**
     * List Vendors
     */
    public function list_vendors() {
        
        $data['vendors'] = [];
        
        try {
            $this->data['vendors'] = $this->dataService->query("SELECT * FROM Vendor STARTPOSITION 1 MAXRESULTS 10");
        } catch (Exception $ex) {
            // One of the reasons you might get here is that you don't have the
            // Oauth2 authorization info in your session. print_r($_SESSION) to see what
            // you have in there
            //print "<pre>"; print_r($_SESSION);
            $this->data['error_msg'] = $ex->getMessage();
        }

        //print_r($vendors);die;
       // $this->load->view('qb_tests', $data);
        $this->load->view('b_level/header');
        $this->load->view('b_level/sidebar');
        $this->load->view('b_level/quickbook/qb_tests', $this->data);
        $this->load->view('b_level/footer');
    }
    
    /**
     * Create Invoice
     */

 
           public function create_customer() {
                
            $this->dataService->setLogLocation("/var/www/html/BMS");
            $this->dataService->throwExceptionOnError(true);
            //Add a new Vendor
            $theResourceObj = Customer::create([
                "BillAddr" => [
                    "Line1" => "1234 Main Street",
                    "City" => "Mountaisns Viewr",
                    "Country" => "USA",
                    "CountrySubDivisionCode" => "CA",
                    "PostalCode" => "94042"
                ],
                "Notes" => "Here are other details.",
                "Title" => "Mr",
                "GivenName" => "MOham",
                "MiddleName" => "B",
                "FamilyName" => "Adah",
                "Suffix" => "Jr",
                "FullyQualifiedName" => "Kingere Grocerieress",
                "CompanyName" => "Kingreff Grocerirees12v",
                "DisplayName" => "sagareqrsdf",
                "PrimaryPhone" => [
                    "FreeFormNumber" => "(555) 555-5555"
                ],
                "PrimaryEmailAddr" => [
                    "Address" => "kok3sdfaf9@myemail.com"
                ]
            ]);
            
            $resultingObj = $this->dataService->Add($theResourceObj);
            $error = $this->dataService->getLastError();
            if($error){
                echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
                echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                echo "The Response message is: " . $error->getResponseBody() . "\n";
            }else{
                echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
                $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
                echo $xmlBody . "\n";
            }
  
    }
    public function read_customer(){
      
        $this->dataService->setLogLocation("/var/www/html/BMS");
        $this->dataService->throwExceptionOnError(true);
        $customer = $this->dataService->FindbyId('customer', 63);
        $error = $this->dataService->getLastError();
        if ($error) {
            echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
            echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            echo "The Response message is: " . $error->getResponseBody() . "\n";
        }
        else {
            echo "Created Id={$customer->Id}. Reconstructed response body:\n\n";
            $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($customer , $urlResource);
            echo $xmlBody . "\n";
        }
    } 
    public function create_invoice() {
        
        // This sample follows closely QuickBooks PHP SDK documentation here:
        // https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html
        
        // Uncomment these and play with the parameters
        // 
        //// I picked a random Customer from this list
        // print "<pre>";
        // print_r($this->dataService->FindById('Customer', 11)); die;
        //
        //// I took a look at the internal structure of Invoice objects
        //print "<pre>";
        //print_r($this->dataService->FindAll('Invoice', 1, 100)); die;
        
        /* @var $invoice \QuickBooksOnline\API\Facades\Invoice */
        $invoice = Invoice::create([

                    "Line" => [
                        [
                            "Description" => "Programming services: accounting solution", // :)
                            "Amount" => 6200,
                            "DetailType" => "SalesItemLineDetail",
                            "SalesItemLineDetail" => [
                                "ItemRef" => [
                                    "value" => 1,
                                    "name" => "Services"
                                ]
                            ]
                        ]
                    ],
                    "CustomerRef" => [
                        "value" => "11",
                        "name" => "Gevelber Photography" // This is not required
                    ]
        ]);
        
        // This is the object representation (copy) of what we should have inserted
        // if everything went Ok
        $resultInvoice  = $this->dataService->Add($invoice);
        
        /* @var $lastError QuickBooksOnline\API\Core\HttpClients\FaultHandler */
        $lastError      = $this->dataService->getLastError();
        
        if ($lastError)
        {
            $err .= "The Status code is: " . $lastError->getHttpStatusCode() . "\n";
            $err .=  "The Helper message is: " . $lastError->getOAuthHelperError() . "\n";
            $err .=  "The Response message is: " . $lastError->getResponseBody() . "\n";
            $this->data['error_msg'] .=$err;
        } else {
            // This should look nice...
          //  print "<b>";
            $this->data['resultInvoice'] = $resultInvoice;
        }
    }
    
    public function verify_invoice() {
        
        $invoice = null;
        
        try {
            // While I was testing, this was the generated Id number for the Invoice
            // Note: this is not MySQL, use quotes for field values.
            $invoice = $this->dataService->query("SELECT * FROM Invoice WHERE Id = '145'");
        } catch (Exception $ex) {
            $this->data['error_msg'] .=  $ex->getMessage();

        }
        
      //  print "<b>";
      $this->data['invoice'] =  $invoice;
    }
    public function add_inventory(){
     
        $this->dataService->setLogLocation("/var/www/html/BMS");
        $this->dataService->throwExceptionOnError(true);
        $theResourceObj = Item::create([
          "Name" => "Inventory Supplier Sample",
          "UnitPrice" => 20,
          "IncomeAccountRef" => [
            "value" => "79",
            "name" => "Sales of Product Income"
          ],
          "ExpenseAccountRef" => [
            "value" => "80",
            "name" => "Cost of Goods Sold"
          ],
          "AssetAccountRef" => [
            "value" => "81",
            "name" => "Inventory Asset"
          ],
          "Type" => "Inventory",
          "TrackQtyOnHand" => true,
          "QtyOnHand" => 10,
          "InvStartDate" => "2015-01-01"
        ]);
        $resultingObj = $this->dataService->Add($theResourceObj);
        $error = $this->dataService->getLastError();
        if ($error) {
            echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
            echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            echo "The Response message is: " . $error->getResponseBody() . "\n";
        }
        else {
            echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
            $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
            echo $xmlBody . "\n";
        }
    }
    
}