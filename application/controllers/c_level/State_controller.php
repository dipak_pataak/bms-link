<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class State_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $this->user_id = $this->session->userdata('user_id');

        $this->load->model(array(
            'c_level/state_model'
        ));
    }

    public function index() {
       // $this->permission_c->check_label('state')->create()->redirect();
 if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/state/manage_state');
        $this->load->view('c_level/footer');  
              }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

    function getStateLists(){
        $data = $row = array();
        
        // Fetch member's records
        $states = $this->state_model->getRows($_POST);
        foreach($states as $record){
            $i++;
            $chk = '<input type="checkbox" name="Id_List[]" id="Id_List[]" value="'.$record->id.'" class="checkbox_list">';            
            
            $action = '<a href="javascript:void(0)" class="edit_state btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"  data-data_id="'. $record->id.'"><i class="simple-icon-pencil"></i></a>
        <a href="'.base_url('c_level/state_controller/delete_state/').$record->id.'" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm(\'Do you want to delete it?\')"><i class="glyph-icon simple-icon-trash"></i></a>';
            $data[] = array($chk, $record->state_id, $record->state_name, $record->city, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->state_model->countAll(),
            "recordsFiltered" => $this->state_model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }

    public function save_state() {
       $stateData = array(
            'state_id' => $this->input->post('state_id'),
            'state_name' => $this->input->post('state_name'),
            'city' => $this->input->post('city'),
            'created_by' => $this->session->userdata('user_id'),
        );
        $this->db->insert('city_state_tbl', $stateData);

        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "State information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 State save successfull </div>");
        redirect('retailer-state');
    }

    public function get_state($id) {
        $result = $this->db->where('id', $id)->get('city_state_tbl')->row();
        echo json_encode($result);
    }

    public function update_state() {
        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "State information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $stateData = array(
            'state_id' => $this->input->post('state_id'),
            'state_name' => $this->input->post('state_name'),
            'city' => $this->input->post('city')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id', $id)->update('city_state_tbl', $stateData);
        
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 State update successfull </div>");
        redirect('retailer-state');
    }
    public function delete_state($id) {
            $this->db->where('id', $id)->delete('city_state_tbl');
            //============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "deleted";
            $remarks = "State information deleted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
            //============== close access log info =================
            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 State deleted successfully! </div>");
            redirect('retailer-state');
    }    

    //=========== its for import_state_save ===========
    public function import_state_save() {
        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "color csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    //$insert_csv['customer_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['state_id'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['state_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['city'] = (!empty($csv_line[2]) ? $csv_line[2] : 0);
                }
                $data = array(
                    'state_id' => $insert_csv['state_id'],
                    'state_name' => $insert_csv['state_name'],
                    'city' => $insert_csv['city'],
                    'created_by' => $this->user_id
                );
                if ($count > 0) {
                    $result = $this->db->select('*')
                        ->from('city_state_tbl')
                        ->where('state_id', $data['state_id'])
                        ->where('state_name', $data['state_name'])
                        ->where('city', $data['city'])
                        ->where('created_by', $this->level_id)
                        ->get()
                        ->num_rows();

                    if ($result == 0 && !empty($data['state_name'])) {
                        $this->db->insert('city_state_tbl', $data);
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>State imported successfully!</div>");
        redirect('retailer-state');
    }

    //=========== its for manage_action ==============
    public function manage_action(){
        if($this->input->post('action')=='action_delete')
        {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('city_state_tbl','id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected State has been Deleted successfully.</div>");
        }
        redirect("retailer-state");
    }
}
