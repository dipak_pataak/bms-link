<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_return_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'c') {
            redirect('retailer-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Supplier_model');
        $this->load->model('c_level/User_model');
        // $this->load->model('c_level/purchase_model');
        // $this->load->model('c_level/pattern_model');
        $this->load->model('c_level/C_return_model');
        $this->load->model('common_model');
    }

    public function index() {

//        $this->load->view('b_level/header');
//        $this->load->view('b_level/sidebar');
//        $this->load->view('b_level/customers/customer_edit');
//        $this->load->view('b_level/footer');
    }


//    ================ its for customer_return ===============
    public function customer_return() {


        $config["base_url"] = base_url('c_level/C_return_controller/customer_return');
        //$config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_level_returned_order_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["customer_return"] = $this->C_return_model->customer_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;



      if( $this->session->userdata('package_id')==3  ){
            $this->load->view('c_level/header', $data);
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/c_returns/customer_return');
            $this->load->view('c_level/footer');
        
        }else{
           
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }


    //    ==========its for own user customer count =========
    public function own_level_returned_order_count() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('order_return_tbl');
        return $num_rows;
    }

    //    =========== its for show_return_resend ===========
    public function show_return_resend() {
        $data['return_id'] = $this->input->post('return_id');
        $data['order_id'] = $this->input->post('order_id');
        $data['returned_by'] = $this->input->post('returned_by');
        $data['order_details'] = $this->db->select('*')
                        ->from('order_return_details a')
                        ->join('product_tbl b', 'b.product_id = a.product_id')
                        ->where('a.return_id', $data['return_id'])
//                                           ->where('a.return_id', $data['product_id'])
                        ->where('a.replace_qty', 0)
                        ->get()->result();


        $this->load->view('c_level/c_returns/show_return_resend_form', $data);
    }



    //    ================ its for supplier_return ===============
    public function raw_material_supplier_return() {

        $total_rows = $this->db->where('created_by',$this->level_id)->count_all_results('raw_material_return_tbl');

        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $config["base_url"] = base_url('c_level/C_return_controller/raw_material_supplier_return');
        $config["total_rows"] = $total_rows;

        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["get_raw_material_supplier_return"] = $this->C_return_model->get_raw_material_supplier_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        if($this->session->userdata('package_id')==3 ){
        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/c_returns/supplier_return');
        $this->load->view('c_level/footer');
          }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

    //    ============= its for raw_material_return_approved =========
    public function raw_material_return_purchase_approved() {
     
        $return_id = $this->input->post('return_id');
        $raw_material_id = $this->input->post('raw_material_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $return_qty = $this->input->post('return_qty');

        $approve_data = array(
            'is_approved' => 1,
            'approved_by' => $this->user_id,
        );
        $this->db->where('return_id', $return_id);
        $this->db->update('raw_material_return_tbl', $approve_data);

        for ($i = 0; $i < count($raw_material_id); $i++) {
            $raw_material_return_stock_details = array(
//                'return_id' => $return_id,
                'row_material_id' => $raw_material_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'color_id' => $color_id[$i],
                'out_qty' => $return_qty[$i],
                'from_module' => "From Raw Material Return",
                'stock_date' => date('Y-m-d'),
            );
            $this->db->insert('row_material_stock_tbl', $raw_material_return_stock_details);
        }
//        echo "Returned approved successfully!";
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Approved successfully!</div>");
        redirect("raw-material-supplier-return");
    }


    //    ============== its for supplier_return_filter =============
    public function supplier_return_filter() {
        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $data['invoice_no'] = $this->input->post('invoice_no');
        $data['supplier_name'] = $this->input->post('supplier_name');
//        $material_name = $this->input->post('material_name');
        $data['supplier_return_filter'] = $this->C_return_model->supplier_return_filter($data['invoice_no'], $data['supplier_name']);


        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/c_returns/supplier_return_filter');
        $this->load->view('c_level/footer');
    }

}
