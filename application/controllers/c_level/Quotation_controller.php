<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('b_level/settings');
    }

    public function index() {
        
    }

    public function delete_quotation($quote_id) {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "Quotation information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $this->db->where('qt_id', $quote_id)->delete('c_quatation_table');
        $this->db->where('fk_qt_id', $quote_id)->delete('c_quatation_details');
        $this->session->set_flashdata('success', "<div class='alert alert-success msg'>Data updated successfully!</div>");
        redirect('retailer-manage-quotation');
    }

    public function manage_quotation() {
        $access_retailer=access_role_permission_page(43);
        $acc=access_role_retailer();
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();

        $data['quotation'] = $this->db->select("c_quatation_table.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('c_quatation_table.qt_created_by', $this->level_id)
                        ->order_by('c_quatation_table.qt_id', 'DESC')
                        ->get('c_quatation_table')->result();

        
         if($this->session->userdata('package_id')==1){
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
       
        }else{
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/quotation/manage_quotation', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }
    }

    public function add_new_quotation() {
        $access_retailer=access_role_permission_page(42);
        $acc=access_role_retailer();
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();
        $data['qtjs'] = "c_level/quotation/qt.php";
        $data['rooms'] = $this->db->get('rooms')->result();
        if($this->session->userdata('package_id')==1){
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');      
        }else{
            if ($access_retailer==1 or $acc==1) {    
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/quotation/add_new_quotation', $data);
                    $this->load->view('c_level/footer');
            }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
            }
        }
    }

    public function edit_quotation($quote_id) {
        $data['quote_id'] = $quote_id;


        $data['quotation'] = $this->db->select("c_quatation_table.*,customer_info.*")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('qt_id', $quote_id)
                        ->get('c_quatation_table')->row();
                        
        $data['quotation_details'] = $this->db->select("c_quatation_details.*")
                        ->where('fk_qt_id', $quote_id)
                        ->order_by('qd_id', 'ASC')
                        ->get('c_quatation_details')->result();
                        
        //dd($data['quotation_details']);
        // echo "<pre>";
        // print_r($data['quotation_details']);
        // echo "<hr/>";
        // exit;
        $data['roomWiseArray'] = [];
        $newRoomArray = [];
        $currentRoom = null;
        $totalWindow = sizeOf($data['quotation_details']);

        for ($i = 0; $i < sizeOf($data['quotation_details']); $i++) {
            if (($currentRoom !== null && $data['quotation_details'][$i]->room_name !== $currentRoom)) {
                array_push($data['roomWiseArray'], $newRoomArray);
                $newRoomArray = [];
            }
            array_push($newRoomArray, $data['quotation_details'][$i]);
            $currentRoom = $data['quotation_details'][$i]->room_name;
        }

        if (sizeOf($newRoomArray) > 0) {
            array_push($data['roomWiseArray'], $newRoomArray);
            $newRoomArray = [];
        }

        // echo "<pre>";
        // print_r($data['roomWiseArray']);


        for ($i = 0; $i < sizeOf($data['roomWiseArray']); $i++) {
            for ($j = 0; $j < sizeOf($data['roomWiseArray'][$i]); $j++) {
                $width = $data['roomWiseArray'][$i][$j]->win_width;
                $wfrac = $data['roomWiseArray'][$i][$j]->win_wfraction;
                $height = $data['roomWiseArray'][$i][$j]->win_height;
                $hfrac = $data['roomWiseArray'][$i][$j]->win_htfraction;
                $cats = json_decode($data['roomWiseArray'][$i][$j]->win_categories);
                $updatedCats = [];
                for ($k = 0; $k < sizeof($cats); $k++) {
                    $product = $cats[$k]->win_product;
                    $pattern = $cats[$k]->win_pattern;
                    $cats[$k]->price = $this->price_call($width, $height, $product, $pattern, $wfrac, $hfrac, true);
                    array_push($updatedCats, $cats[$k]);
                }
                $data['roomWiseArray'][$i][$j]->win_categories = json_encode($updatedCats);
            }
        }


        // echo "<pre>";
        // print_r($data['roomWiseArray']);die;
        // foreach ($data['roomWiseArray'] as $key => $value) {
        //     foreach ($value as $keya => $valuea) {
        //         $cats = json_decode($valuea->win_categories);
        //         for ($i=0; $i < sizeof($cats); $i++) { 
        //             //print_r($value);
        //             print_r($cats[$i]);
        //         }
        //         //print_r($cats['win_upcharges_array']);
        //     }
        // }
        // exit;

        $data['get_customer'] = $this->Order_model->get_customer();
        $data['categories'] = $this->Order_model->get_category();
        $data['company_profile'] = $this->settings->company_profile();
        $data['qtjs'] = "c_level/quotation/qt.php";
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->select('id,fraction_value')->get('width_height_fractions')->result();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/edit_quotation', $data);
        $this->load->view('c_level/footer');
    }

    public function get_color_partan_model($product_id) {
        $pp = $this->db->select('*')
                        ->where('product_id', $product_id)
                        ->get('product_tbl')->row();
        $patter_ids = explode(',', @$pp->pattern_models_ids);
        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->order_by('pattern_name', 'asc')->get('pattern_model_tbl')->result();
        $q = '<option value="">-- select pattern --</option>';
        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }
        if ($pp->price_style_type == 3) {
            $main_price = $pp->fixed_price;
        } elseif ($pp->price_style_type == 2) {
            $main_price = $pp->sqft_price;
        } else {
            $main_price = 0;
        }
        $arr = array('price' => $main_price, 'pattern' => $q);
        echo json_encode($arr);
    }

    public function get_product_by_category($category_id) {
        $result = $this->db->select('product_id,category_id,product_name')->where('category_id', $category_id)->where('subcategory_id', 0)->order_by('product_name', 'asc')->get('product_tbl')->result();
        $q = '';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    public function customer_wise_sidemark($customer_id = null) {
        $customer_wise_sidemark = $this->db->select('customer_info.*,c_us_state_tbl.tax_rate')->from('customer_info')->join('c_us_state_tbl', 'c_us_state_tbl.shortcode=customer_info.state', 'left')->where('customer_info.customer_id', $customer_id)->get()->row();
        //  echo $this->db->last_query();
        echo json_encode($customer_wise_sidemark);
    }

    public function price_call($width = NULL, $height = NULL, $product_id = NULL, $patter_id = NULL, $wfraction = NULL, $htfraction = NULL, $return = false) {
        if (isset($product_id) && $height >= 0 && !empty($width)) {
            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

            // If category is Arch then consider height and  htfraction = 0 : START
            $selCategory = $this->db->select('*')->where('category_id', $p->category_id)->get('category_tbl')->row();
            $category_name = $selCategory->category_name;
            if($category_name == 'Arch'){
                $height = 0;
                // $htfraction = 0;
            }
            // echo "==".$height;
            // If category is Arch then consider height and  htfraction = 0 : END

            $discountData = $this->db->select("individual_cost_factor,costfactor_discount")->where('product_id', $product_id)->where('level_id', $this->level_id)->get('c_cost_factor_tbl')->row();
            $q = "";
            $price = "";
            $noPrice = false;
            if ($p->price_style_type == 1) {
                if ($wfraction != NULL || $htfraction != NULL) {
                    $getGreaterCharges = true;
                } else {
                    
                    $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                    ->where('row =', $width)
                                    ->where('col =', $height)
                                    ->get('price_style')->row();
                    if (isset($prices) && $prices->price != 0) {
                        $price = $pc = $prices->price;
                    } else {
                        $getGreaterCharges = true;
                    };
                }

                if (isset($getGreaterCharges)) {
                    $prices = $this->db->where('style_id', $p->price_rowcol_style_id)
                                        ->where('row >=', $width)
                                        ->where('col >=', $height)
                                        ->order_by('row_id', 'asc')
                                        ->limit(1)
                                        ->get('price_style')->row();             
                    if(!$prices) {
                        $noPrice = true;
                    }
                    $pc = ($prices != NULL ? $prices->price : 0);
                    $price = $pc;
                }
            } elseif ($p->price_style_type == 2) {
                $price = $p->sqft_price;

                $new_width = $width + 3;
                $new_height = $height + 1.5;

                $price = round($new_width * $new_height / 144 * $price, 2);
            } elseif ($p->price_style_type == 3) {
                $price = $p->fixed_price;
            } elseif ($p->price_style_type == 4) {
                if ($patter_id != NULL) {
                    $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $patter_id)->get()->row();
                    $price = $this->db->where('style_id', $pg->group_id)
                                    ->where('row >=', $width)
                                    ->where('col >=', $height)
                                    ->order_by('row_id', 'asc')
                                    ->limit(1)
                                    ->get('price_style')->row();

                    $pc = ($price != NULL ? $price->price : 0);
                    $price = $pc;
                } else {
                    $price = 0;
                }
            } else {
                $price = 0;
            }
            $arr = array('price' => $price, 'individual_cost_factor' => $discountData->individual_cost_factor, 'costfactor_discount' => $discountData->costfactor_discount, 'noPrice' => $noPrice);

            if ($return) {
                return $price;
            } else {
                echo json_encode($arr);
            }
        }
    }

    public function upcharge($category) {
        $q = '';
        if ($category == 'Shutters') {
            $q .= '<tr><td><p class="text-danger">Specialty shapes</p>No of Panel</td><td colspan="2"><input type="number" name="no_panel" id="no_panel" class=" text-right"> X $150 each panel = $<b class="plan_cost"></b><input type="hidden" name="plan_price" value="0" id="plan_price" class="up_price"/></td></tr><tr><td>
                    <p class="text-danger">Chair Rail Cut</p>
                    No of window
                </td>
                <td colspan="2"><input type="number" name="no_window" id="no_window" class="text-right"> X $30 each window = $<b class="window_cost"></b> 
                    <input type="hidden" name="window_price" value="0" id="window_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Door handle Cut</p>
                    No of Door
                </td>
                <td colspan="2"><input type="number" name="no_door" id="no_door" class="text-right"> X $150 per Door = $<b class="door_cost"></b>
                    <input type="hidden" name="door_price" value="0" id="door_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">Build out</p>
                    No of Shutters
                </td>
                <td colspan="2"><input type="number" name="no_shutters" id="no_shutters" class="text-right"> X $30 each = $<b class="shutters_cost"></b>
                    <input type="hidden" name="shutters_price" value="0" id="shutters_price" class="up_price"/>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="text-danger">T- Post</p>
                    No of window 
                </td>
                <td colspan="2"><input type="number" name="no_t_window" id="no_t_window" class="text-right"> X $40 each = $<b class="t_window_cost"></b>
                    <input type="hidden" name="t_window_price" value="0" id="t_window_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Blinds') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Upgraded Valance</p>
                    No of Blinds
                </td>
                <td colspan="2"><input type="number" name="no_blinds" id="no_blinds" class=" text-right"> X $4 each = $<b class="blind_cost"></b> 
                <input type="hidden" name="blinds_price" value="0" id="blinds_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Shades') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Fabric Wraped Cassette</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="macro" id="Macro" class=" text-right"> X 10% = $<b class="Macro_cost"></b> 
                    <input type="hidden" name="macro_price" value="0" id="macro_price" class="up_price"/>
                </td>
            </tr>';
        } elseif ($category == 'Arches') {

            $q .= '<tr>
            <td>
                    <p class="text-danger">Custom Paint</p>
                    Macro
                </td>
                <td colspan="2"><input type="number" name="custom_paint" id="custom_paint" class=" text-right"> X 20% = $<b class="custom_paint_cost"></b> 
                <input type="hidden" name="custom_paint_price" value="0" id="custom_paint_price" class="up_price"/>
                </td>
            </tr>';
        }

        echo $q;
    }

    public function add_to_cart1() {

        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $product_id = $this->input->post('product_id');
        $patter_id = $this->input->post('patter_id');
        $price = $this->input->post('price');
        $upcharge = $this->input->post('upcharge');
        $room_sub_price = $this->input->post('room_sub_price');
        $room_name = $this->input->post('room_name');
        $category_id = $this->input->post('category_id');
        $category_name = $this->input->post('category_name');


        if ($category_name == 'Shutters') {

            $upchargeData = array(
                'no_panel' => $this->input->post('no_panel'),
                'panel_price' => $this->input->post('plan_price'),
                'no_window' => $this->input->post('no_window'),
                'window_price' => $this->input->post('window_price'),
                'no_door' => $this->input->post('no_door'),
                'door_price' => $this->input->post('door_price'),
                'no_shutters' => $this->input->post('no_shutters'),
                'shutters_price' => $this->input->post('shutters_price'),
                'no_t_window' => $this->input->post('no_t_window'),
                't_window_price' => $this->input->post('t_window_price')
            );
        } elseif ($category_name == 'Blinds') {

            $upchargeData = array(
                'no_blinds' => $this->input->post('no_blinds'),
                'blinds_price' => $this->input->post('blinds_price')
            );
        } elseif ($category_name == 'Shades') {

            $upchargeData = array(
                'macro' => $this->input->post('macro'),
                'macro_price' => $this->input->post('macro_price')
            );
        } elseif ($category_name == 'Arches') {

            $upchargeData = array(
                'custom_paint' => $this->input->post('custom_paint'),
                'custom_paint_price' => $this->input->post('custom_paint_price')
            );
        }




        $qty = count($product_id);
        $id = str_replace(' ', '_', $room_name) . $qty . $category_id;

        $product_data = [];
        foreach ($product_id as $key => $pid) {
            $product_data[] = array(
                'width' => $width[$key],
                'height' => $height[$key],
                'product_id' => $pid,
                'patter_id' => $patter_id[$key],
                'price' => $price[$key]
            );
        }


        $cartData = array(
            'id' => $id,
            'product_id' => $id,
            'name' => $room_name,
            'category_name' => $category_name,
            'category_id' => $category_id,
            'price' => $room_sub_price + $upcharge,
            'qty' => $qty,
            'product_data' => json_encode($product_data),
            'upchargeData' => json_encode($upchargeData),
            'upcharge' => $upcharge,
            'room_price' => $room_sub_price
        );

        $this->cart->insert($cartData);
        echo 1;
    }

    //-------------------------------------
    // Clear Cart 
    // ------------------------------------
    public function clear_cart() {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        redirect('retailer-new-quotation');
    }

    public function clear_cart_data() {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        //redirect('retailer-new-quotation');
    }

    // END--------------------------------
    //---------------------------------------
    // Delete Cart item
    // --------------------------------------
    public function delete_cart_item($rowid) {
        $this->cart->remove($rowid);
        echo 1;
    }

    public function save_updated_quotation() {
        $this->db->where('qt_id', $this->input->post('quotation_id'));
        $this->db->delete('c_quatation_table');
        $this->db->where('fk_qt_id', $this->input->post('quotation_id'));
        $this->db->delete('c_quatation_details');
        $this->save_quotation();
    }

    public function save_quotation() {
        $data = $this->input->post();
        $cartItems = json_decode($this->session->userdata('cartItems'));
        // echo "<pre>";print_r($cartItems);die;
        foreach ($data['win_discount_amt'] as $key => $value) {
            $cartItems[$key]->win_discount_amt = $value;
        }
        $newCartItems = [];
        $newCartItemsByWindow = [];
        $newCartItemsCategoryArray = [];
        $RoomCount = NULL;
        $WinCount = NULL;

        for ($i = 0; $i < sizeof($cartItems); $i++) {
            if ($RoomCount !== NULL && $RoomCount !== $cartItems[$i]->room_no || $WinCount !== NULL && $RoomCount === $cartItems[$i]->room_no && $WinCount !== $cartItems[$i]->win_no) {
                $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
                array_push($newCartItems, $newCartItemsByWindow);
                $newCartItemsByWindow = [];
                $newCartItemsCategoryArray = [];
            }
            if ($RoomCount === $cartItems[$i]->room_no && $WinCount === $cartItems[$i]->win_no) {
                array_push($newCartItemsCategoryArray, array(
                    'win_category' => $cartItems[$i]->win_cat,
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt,
                    /****** Start: Added to maintain extra discount amount (17-01-2020)*********/
                    //'win_extra_discount_amt' => '',
                    /****** End: Added to maintain extra discount amount *********/
                    'win_comment' => $cartItems[$i]->win_comment,
                ));
            } else {
                $newCartItemsCategoryArray = [];
                $newCartItemsByWindow['room_no'] = $cartItems[$i]->room_no;
                $newCartItemsByWindow['room_name'] = $cartItems[$i]->room_name;
                $newCartItemsByWindow['win_no'] = $cartItems[$i]->win_no;
                $newCartItemsByWindow['win_width'] = $cartItems[$i]->win_width;
                $newCartItemsByWindow['win_wfraction'] = $cartItems[$i]->win_wfraction;
                $newCartItemsByWindow['win_height'] = $cartItems[$i]->win_height;
                $newCartItemsByWindow['win_htfraction'] = $cartItems[$i]->win_htfraction;
                $newCartItemsByWindow['win_comment'] = $cartItems[$i]->win_comment;
                array_push($newCartItemsCategoryArray, array(
                    'win_category' => $cartItems[$i]->win_cat,
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt,
                    /****** Start: Added to maintain extra discount amount (17-01-2020)*********/
                    //'win_extra_discount_amt' => '',
                    /****** End: Added to maintain extra discount amount *********/
                    'win_comment' => $cartItems[$i]->win_comment,
                ));
            }
            $RoomCount = $cartItems[$i]->room_no;
            $WinCount = $cartItems[$i]->win_no;
        }

        if (sizeof($newCartItemsByWindow) > 0) {
            $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
            array_push($newCartItems, $newCartItemsByWindow);
            $newCartItemsByWindow = [];
            $newCartItemsCategoryArray = [];
        }


        $quotTblData = array(
            'qt_cust_id' => $data['customer_id'],
            'qt_cust_type' => $data['customertype'],
            'qt_subtotal' => $data['subtotal'],
            'qt_tax' => $data['tax'],
            'qt_grand_total' => $data['grand_total'],
            'total_discount' => $data['total_discount'],
            'total_upcharge' => $data['total_upcharge'],
            'qt_order_date' => date("Y-m-d", strtotime($data['order_date'])),
            'qt_created_on' => date("Y-m-d H:i:s"),
            'qt_modified_on' => date("Y-m-d H:i:s"),
            'qt_created_by' => $this->session->userdata('user_id'),
                // 'win_comment' => $data['win_comment'],
        );
        $this->db->insert('c_quatation_table', $quotTblData);
        $quote_id = $this->db->insert_id();
        for ($i = 0; $i < sizeof($newCartItems); $i++) {
            $newCartItems[$i]['fk_qt_id'] = $quote_id;
            $newCartItems[$i]['win_created_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_modified_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_created_by'] = $this->session->userdata('user_id');
        }

        $this->db->insert_batch('c_quatation_details', $newCartItems);

        // Send Email and SMS with Quotation details : START
        if($quote_id != ''){
            $this->send_email_quotation_receipt($quote_id);
            $this->send_sms_quotation_receipt($quote_id);
        }
        // Send Email and SMS with Quotation details : END

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order add successfully</div>");
        redirect("retailer-new-quotation");
        exit();
    }

    /** start added by sm */
    public function getModelsByCategory($category_id) {
        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();
        $q = '';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        return $q;
    }

    public function getPatternByModel($model_id) {

        $pp = $this->db->select('*')
                        ->where('product_id', $model_id)
                        ->get('product_tbl')->row();
        $patter_ids = explode(',', @$pp->pattern_models_ids);
        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->get('pattern_model_tbl')->result();
        $q = '<option value="">-- select pattern --</option>';
        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }
        if ($pp->price_style_type == 3) {
            $main_price = $pp->fixed_price;
        } elseif ($pp->price_style_type == 2) {
            $main_price = $pp->sqft_price;
        } else {
            $main_price = 0;
        }

        $arr = array('price' => $main_price, 'pattern' => $q);
        echo json_encode($arr);
    }

    public function getRoomTypeDP() {
        $rooms = $this->db->get('rooms')->result();
        $html = '';
        foreach ($rooms as $room) {
            $html .= "<option value='$room->room_name'>$room->room_name</option>";
        }
        return $html;
    }

    public function getCategoryDP() {
        $categories = $this->Order_model->get_category();
        $html = '<option>--Select Category--</option>';
        foreach ($categories as $category) {
            $html .= "<option value='$category->category_id'>$category->category_name</option>";
        }
        echo $html;
        exit();
    }

    public function getRoomRow() {
        $room_cnt = $this->input->post('room_cnt');
        $room_cnt_ro = $room_cnt + 1;
        $html = '<div class="row room_' . $room_cnt . ' col-md-12">
                                <div class="row col-md-12">
                                    <div class="form-group col-md-6"><h2>Room ' . $room_cnt_ro . ' </h2>';
        $html .= '<select class="form-control select2" name="room_name[' . $room_cnt . ']" id="room_name_' . $room_cnt . '" onchange="reload()" required="" data-placeholder="-- select one --"><option value="">--Select room type--</option>';
        $html .= $this->getRoomTypeDP();
        $html .= '</select>';
        $html .= '</div>
                                    <div class="form-group col-md-6"> <label>&nbsp;</label>
                                        <button type="button" class="btn btn-xs btn-success pull-right right add_window_btn" data-room_cnt="' . $room_cnt . '" >+ Add Window</button>
                                    </div>
                                </div>
                                <div class="no_of_window_div row col-md-12">

                                </div>
                            </div>';
        echo json_encode(array('html' => $html));
        exit();
    }

    public function getWindowRow() {
        $room_cnt = $this->input->post('room_cnt');
        $window_cnt = $this->input->post('window_cnt');
        $window_cnt_ro = $window_cnt + 1;
        $html = '<div class="row detail_row_' . $window_cnt . ' col-md-12">
                            <div class="col-md-12"><h3>Window ' . $window_cnt_ro . '</h3></div>
                            <div class="form-group col-md-4">
                                <label for="room_name" class="col-form-label">Category<span class="text-danger">*</span></label>';
        $html .= '<select class="form-control select2 category_dp" name="category_id[' . $room_cnt . '][' . $window_cnt . ']" onchange="reload()" data-room_no = ' . $room_cnt . ' data-window_no=' . $window_cnt . ' required="" data-placeholder="-- select one --"><option value="">--Select category-</option>';
        $html .= $this->getCategoryDP();
        $html .= '</select>';
        $html .= '</div>
                            <div class="form-group col-md-8 window_details_' . $room_cnt . '_' . $window_cnt . '">

                            </div>
                        </div><div class="col-md-1"><span class="delete_room" data-room_no="' . $room_cnt . '">X</span></div>';
        echo json_encode(array('html' => $html));
        exit();
    }

    public function getMoreRoom() {

        $room_no = $this->input->post('room_no');
        $room_cnt_ro = $room_no + 1;
        $html = '<fieldset class="rooms_section room_' . $room_no . '"> <legend>Room ' . $room_cnt_ro . '</legend><div class="row room_details_section room_section_' . $room_no . '"><div class="form-group col-md-2">';
        $html .= '<select class="form-control select2 twok" name="room_name[' . $room_no . ']"  required="" data-placeholder="-- select Room Type --"><option value="">--Select room type--</option>';
        $html .= $this->getRoomTypeDP();
        $html .= '</select>';

        $html .= '</div><div class="form-group col-md-3"> <label>Windows</label><div class="win_cnt_div"><span class="win_inc_btn" data-action="minus">-</span><input type="number"  name="no_of_windows[' . $room_no . ']" class="form-control twok no_of_windows" value="0" data-room_no="' . $room_no . '" placeholder="No of Windows" min="0" max="100"><span class="win_inc_btn" data-action="plus">+</span></div></div><div class="height_width_sec_' . $room_no . ' height_width"></div><div class="form-group col-md-1 add_category_section_' . $room_no . '"> <label>&nbsp;</label><button type="button" class="btn btn-xs btn-success pull-right right add_category_btn add_category_btn" data-room_no="' . $room_no . '" >+ Add Cat.</button></div></div> <span class="delete_room" data-room_no="' . $room_no . '">X</span></fieldset>';
        echo json_encode(array('html' => $html, 'room_cnt_ro' => $room_cnt_ro));
        exit();
    }

    function manageUpcharges() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $this->global['pageTitle'] = 'Upcharges :  Settings';
            $user_id = $this->session->userdata('user_id');
            $where['created_by'] = $user_id;
            $data['upcharges'] = $this->user_model->getData('c_level_upcharges', $where);

            $table = 'category_tbl';
            $categories = array();
            $where['created_by'] = $this->session->userdata('admin_created_by');
            $res = $this->user_model->getData($table, $where);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $categories[$ro->category_id] = $ro->category_name;
                }
            }

            $data['categories'] = $categories;
            $data['upcharge_table'] = 'c_level_upcharges';

            $data['redirect_key'] = '';
            $access_retailer=access_role_permission_page(65);
            $acc=access_role_retailer();
        if($this->session->userdata('package_id')==1){
               $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
   
        }else{
            if($access_retailer==1 or $acc==1){
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/quotation/manage-upcharges', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
            }
        }
    }

    public function getUpcharge() {
        $this->load->model('user_model');
        $user_id = $this->session->userdata('user_id');
        $category_id = $this->input->post('category_id');
        $category = $this->input->post('category_name');
        $room_no = $this->input->post('room_no');
        $window_no = $this->input->post('window_no');
        $cat_sec_no = $this->input->post('cat_sec_no');
        $cat_ind_no = (int) $cat_sec_no - 1;


        $html = '';

        $table = 'c_level_upcharges';
        $categories = array();
        $where['category_id'] = $category_id;
        $where['created_by'] = $user_id;

        $res = $this->user_model->getData($table, $where);
        if (!empty($res) && $res->num_rows() > 0) {
            foreach ($res->result() as $ro) {
                $html .= '<div class="form-group"><label class="label">' . $ro->name . '</label> &nbsp;&nbsp;';
                if ($ro->type == 'Amount') {
                    $index = $ro->id . '_' . $ro->type;
                    $html .= '<input type="checkbox" class="amount_calc" name="upcharges[' . $room_no . '][' . $window_no . '][' . $cat_ind_no . '][' . $index . ']" value="' . $ro->value . '">  $' . $ro->value;
                } else {
                    $index = $ro->id . '_Percantage';
                    $html .= '<input type="checkbox" class="amount_calc" name="upcharges[' . $room_no . '][' . $window_no . '][' . $cat_ind_no . '][' . $index . ']" value="' . $ro->value . '">  ' . $ro->value . '%';
                }
                $html .= '</div>';
            }
        }
        echo json_encode(array('html' => $html));
        exit();
    }

    function addUpcharges() {
        $this->load->model('user_model');
        $insertInfo['created_by'] = $this->session->userdata('user_id');
        $insertInfo['name'] = $this->input->post('name');
        $insertInfo['type'] = $this->input->post('type');
        $insertInfo['value'] = $this->input->post('value');
        $insertInfo['category_id'] = $this->input->post('category_id');
        $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
        $result = $this->user_model->addData('c_level_upcharges', $insertInfo);
        if ($result > 0) {
            $this->session->set_flashdata('success', 'Upcharge created successfully');
        } else {
            $this->session->set_flashdata('error', 'Upcharge creation failed');
        }
        redirect('retailer-manage-upcharges');
    }

    function updateUpcharges() {
        $this->load->model('user_model');
        $whereInfo['created_by'] = $this->session->userdata('user_id');
        $updateInfo['name'] = $this->input->post('name');
        $updateInfo['type'] = $this->input->post('type');
        $updateInfo['value'] = $this->input->post('value');
        $updateInfo['category_id'] = $this->input->post('category_id');
        $whereInfo['id'] = $this->input->post('upcharge_id');
        $table = 'c_level_upcharges';
        $result = $this->user_model->updateTable($table, $whereInfo, $updateInfo);
        $this->session->set_flashdata('success', 'Upcharge updated successfully');
        // if ($result > 0) {
        //     $this->session->set_flashdata('success', 'Upcharge updated successfully');
        // } else {
        //     $this->session->set_flashdata('error', 'Upcharge updatation failed ');
        // }
        redirect('retailer-manage-upcharges');
    }

    function deleteRow() {
        if (0) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $this->load->model('user_model');
            $del_id = $this->input->post('del_id');
            $del_tbl = $this->input->post('del_tbl');

            $result = $this->user_model->deleteRow($del_id, $del_tbl);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    public function addToList() {
        print_r($this->input->post());
        exit;
        die('here');
        $room_name = $this->input->post('room_name');
        $i = 0;
        foreach ($room_name as $room) {
            // room $i

            $i++;
        }
    }

    public function add_to_cart() {
        $cartItems = [];
        $total_rooms = count($this->input->post('room_name'));
        for ($room = 0; $room < $total_rooms; $room++) {
            $room_name = $this->input->post('room_name[' . $room . ']');
            $total_windows = count($this->input->post('width[' . $room . ']'));
            for ($window = 0; $window < $total_windows; $window++) {
                $categories_no = count($this->input->post('category_id[' . $room . ']'));
                $width = $this->input->post('width[' . $room . '][' . $window . ']');
                $wfraction = $this->input->post('wfraction[' . $room . '][' . $window . ']');
                $height = $this->input->post('height[' . $room . '][' . $window . ']');
                $htfraction = $this->input->post('htfraction[' . $room . '][' . $window . ']');
                $win_comment = $this->input->post('comment[' . $room . '][' . $window . ']');
                if ($categories_no < 1) {
                    array_push($cartItems, array(
                        'room_no' => $room,
                        'room_name' => $room_name,
                        'win_no' => $window,
                        'win_width' => $width,
                        'win_wfraction' => $wfraction,
                        'win_height' => $height,
                        'win_htfraction' => $htfraction,
                        'win_cat' => "",
                        'win_category_name' => "",
                        'win_product' => "",
                        'win_product_name' => "",
                        'win_pattern' => "",
                        'win_pattern_name' => "",
                        'win_price' => 0,
                        'win_upcharges_array' => null,
                        'win_upcharges' => 0,
                        'win_comment' => $win_comment,
                        'list_amount' => 0
                    ));
                } else {
                    for ($category = 0; $category < $categories_no; $category++) {
                        $cat_id = $this->input->post('category_id[' . $room . '][' . $category . ']');
                        $product_id = $this->input->post('product_id[' . $room . '][' . $window . '][' . $category . ']');
                        $pattern_id = $this->input->post('pattern_id[' . $room . '][' . $window . '][' . $category . ']');
                        $price = $this->input->post('price_id[' . $room . '][' . $window . '][' . $category . ']');
                        $upcharges = $this->input->post('upcharges[' . $room . '][' . $window . '][' . $category . ']');
                        $product_discount_id = $this->input->post('product_discount_id[' . $room . '][' . $window . '][' . $category . ']');
                        if ($price == "") {
                            $price = 0;
                        }
                        $win_category_name = '';
                        if ($cat_id != NULL && $cat_id > 0) {
                            $selCategory = $this->db->select('*')->where('category_id', $cat_id)->get('category_tbl')->row();
                            $win_category_name = $selCategory->category_name;
                        } else {
                            $win_category_name = "";
                        }

                        if ($product_id != NULL && $product_id > 0 && $win_category_name != 'Misc') {
                            $selProduct = $this->db->select('*')->where('product_id', $product_id)->get('product_tbl')->row();
                            $win_product_name = $selProduct->product_name;
                        } else {
                            if ($win_category_name == 'Misc') {
                                $win_product_name = $product_id;
                            } else {
                                $win_product_name = "";
                            }
                        }

                        if ($pattern_id != NULL && $pattern_id > 0) {
                            $selPattern = $this->db->select('*')->where('pattern_model_id', $pattern_id)->get('pattern_model_tbl')->row();
                            $win_pattern_name = $selPattern->pattern_name;
                        } else {
                            $win_pattern_name = "";
                        }

                        if ($upcharges != NULL && $upcharges > 0) {
                            $win_upcharges_array = [];
                            $win_upcharges = 0;
                            foreach ($upcharges as $key => $value) {
                                $upchargesArray = explode('_', $key);
                                array_push($win_upcharges_array, array(
                                    'id' => $upchargesArray[0],
                                    'type' => $upchargesArray[1],
                                    'value' => $value
                                ));
                                if ($upchargesArray[1] == 'Amount') {
                                    $this_win_upcharges = $value;
                                } else {
                                    
                                    if($product_discount_id > 0){         
                                        $upcharDiscountPrice = ($price * ($value) / 100);                  
                                        $this_win_upcharges = (($upcharDiscountPrice * ($product_discount_id*100)) / 100);
                                    } else {
                                        $this_win_upcharges = $price * ($value / 100);
                                    }
                                }
                                $win_upcharges = round(($win_upcharges + $this_win_upcharges),2);
                            }
                        } else {
                            $win_upcharges = 0;
                            $win_upcharges_array = null;
                        }

                        if ($win_product_name != "" && $product_id != "" && $price != "") {
                            if ($cat_id == 4) {
                                //arch
                                $height = 0;
                            }
                            array_push($cartItems, array(
                                'room_no' => $room,
                                'room_name' => $room_name,
                                'win_no' => $window,
                                'win_width' => $width,
                                'win_wfraction' => $wfraction,
                                'win_height' => $height,
                                'win_htfraction' => $htfraction,
                                'win_cat' => $cat_id,
                                'win_category_name' => $win_category_name,
                                'win_product' => $product_id,
                                'win_product_name' => $win_product_name,
                                'win_pattern' => $pattern_id,
                                'win_pattern_name' => $win_pattern_name,
                                'win_price' => $price,
                                'win_upcharges_array' => $win_upcharges_array,
                                'win_upcharges' => $win_upcharges,
                                'win_comment' => $win_comment,
                                'list_amount' => $price
                            ));
                        }
                    }
                }
                // $weight = array();
                // foreach ($cartItems as $k => $d) {
                //     $weight[$k] = $d['room_name'];
                // }
                // array_multisort($weight, SORT_ASC, $cartItems);
            }
        }

        $this->session->set_userdata('cartItems', json_encode($cartItems));
        echo $this->generateCartItemsHtml($cartItems);
        exit();
    }

    public function deleteCartItem() {
        $cart_item_index = $this->input->post('cart_item_index');
        $cartItems = json_decode($this->session->userdata('cartItems'));
        $newCartItems = [];
        for ($i = 0; $i < sizeof($cartItems); $i++) {
            if ($cart_item_index != $i) {
                array_push($newCartItems, json_decode(json_encode($cartItems[$i]), True));
            }
        }
        $this->session->set_userdata('cartItems', json_encode($newCartItems));
        echo $this->generateCartItemsHtml($newCartItems);
        exit();
    }

    public function generateCartItemsHtml($cartItems) {
        error_reporting(0);
        $subtotal = 0;
        $upchargetotal = 0;
        $cartItemsHtml = "";
        for ($i = 0; $i < sizeof($cartItems); $i++) {
            $rowTotal = 0;
            //    $cartItemsPrice = $cartItems[$i]['win_price'] + $cartItems[$i]['win_upcharges'];
            $cartItemsPrice = $cartItems[$i]['win_price'];
            $cartItemsHtml .= "<tr>
                <td>" . ($i + 1) . "</td>
                <td>" . $cartItems[$i]['room_name'] . "</td>
                <td>" . $cartItems[$i]['win_width'] . "<sup style='color:red !important;'>" . $cartItems[$i]['win_wfraction'] . "</sup> X " . $cartItems[$i]['win_height'] . "<sup style='color:red !important;'>" . $cartItems[$i]['win_htfraction'] . "</sup></td>
                <td>" . $cartItems[$i]['win_category_name'] . "</td>";


            if ($cartItems[$i]['win_category_name'] == 'Misc') {
                $cartItemsHtml .= "<td>" . $cartItems[$i]['win_product'] . "</td> ";
            } else {
                $cartItemsHtml .= "<td>" . $cartItems[$i]['win_product_name'] . "</td>";
            }
            $rowTotal = $cartItems[$i]['win_price'] + $cartItems[$i]['win_upcharges'];
            $cartItemsHtml .= " <td class='window_price'>$" . $cartItems[$i]['win_price'] . "</td>
              
                <td><input type='text'  name='win_discount_amt[]' class='discount_input' data-ProductId='" . $cartItems[$i]['win_product'] . "' onchange='updatePrice(this)' style='text-align:center;width:79px;' readonly/></td>
                <td class='list-amount'>$" . $cartItems[$i]['list_amount'] . "</td>
      
                     <td class='upcharge'>$" . $cartItems[$i]['win_upcharges'] . "</td>
                <td class='price'>$" . round($rowTotal,2) . "</td>
                <td>" . $cartItems[$i]['win_comment'] . "</td>
                <td><span class='btn btn-danger btn-sm' id='deleteCartItem' onclick='deleteCartItem(" . $i . ")'>Remove</span></td>
            </tr>";
            $subtotal = $subtotal + $cartItems[$i]['win_price'];
            $upchargetotal = $upchargetotal + $cartItems[$i]['win_upcharges'];
        }
        return json_encode(array(
            'cartItemsHtml' => $cartItemsHtml,
            'upcharge' => $upchargetotal,
            'subtotal' => $subtotal
        ));
    }

    public function getProductByCategory() {
        $room_no = $this->input->post('room_no');
        $category_id = $this->input->post('category_id');
        $category_name = $this->input->post('category_name');
        $window_no = $this->input->post('window_no');

        $result = $this->db->select('product_id,category_id,product_name')
                        ->where('category_id', $category_id)->where('subcategory_id', 0)
                        ->get('product_tbl')->result();

        $q = '<select name="product_id[ ' + $room_no + '][' + $window_no + '][' . $category_id . ']" class="form-control select2 threek product_id" id="product_id_' + $room_no + '_' + $window_no + '_' + $category_id + '" onchange="getPattern(' + $room_no + ',' + $window_no + ',' + $category_id + ')" required>';
        $q .= '<option value="">--Select Product--</option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        $q .= '</select>';
        echo json_encode(array('html' => $q));
        exit();
    }

    public function getFractionsOptList() {
        $fractions = $this->db->select('id,fraction_value')->get('width_height_fractions')->result();
        $html = "";
        foreach ($fractions as $fraction) {
            $html .= "<option value='$fraction->fraction_value'>$fraction->fraction_value</option>";
        }
        echo $html;
        exit();
    }

    public function get_height_width_fraction() {
        $hif = $this->input->post('hif');
        if ($hif < 10) {
            $hif = (int) $hif . '0';
        }
        if ($hif > 99) {
            $decimal = $hif / 1000;
        } else {
            $decimal = $hif / 100;
        }
        //  echo $decimal;
        $fractions = $this->db->select('*')->where('decimal_value >=', $decimal)->order_by('decimal_value', 'ASC')->get('width_height_fractions')->row()->fraction_value;
        // echo $this->db->last_query();
        echo $fractions;
        exit();
    }

    public function generate_receipt_preview() {
        $data = $this->input->post();
        $cartItems = json_decode($this->session->userdata('cartItems'));
        foreach ($data['win_discount_amt'] as $key => $value) {
            $cartItems[$key]->win_discount_amt = $value;
        }
        $newCartItems = [];
        $newCartItemsByWindow = [];
        $newCartItemsCategoryArray = [];
        $RoomCount = NULL;
        $WinCount = NULL;

        for ($i = 0; $i < sizeof($cartItems); $i++) {
            if ($RoomCount !== NULL && $RoomCount !== $cartItems[$i]->room_no || $WinCount !== NULL && $RoomCount === $cartItems[$i]->room_no && $WinCount !== $cartItems[$i]->win_no) {
                $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
                array_push($newCartItems, $newCartItemsByWindow);
                $newCartItemsByWindow = [];
                $newCartItemsCategoryArray = [];
            }
            if ($RoomCount === $cartItems[$i]->room_no && $WinCount === $cartItems[$i]->win_no) {
                array_push($newCartItemsCategoryArray, array(
                    'win_category' => $cartItems[$i]->win_cat,
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt,
                    'win_comment' => $cartItems[$i]->win_comment,
                ));
            } else {
                $newCartItemsCategoryArray = [];
                $newCartItemsByWindow['room_no'] = $cartItems[$i]->room_no;
                $newCartItemsByWindow['room_name'] = $cartItems[$i]->room_name;
                $newCartItemsByWindow['win_no'] = $cartItems[$i]->win_no;
                $newCartItemsByWindow['win_width'] = $cartItems[$i]->win_width;
                $newCartItemsByWindow['win_wfraction'] = $cartItems[$i]->win_wfraction;
                $newCartItemsByWindow['win_height'] = $cartItems[$i]->win_height;
                $newCartItemsByWindow['win_htfraction'] = $cartItems[$i]->win_htfraction;
                $newCartItemsByWindow['win_comment'] = $cartItems[$i]->win_comment;
                array_push($newCartItemsCategoryArray, array(
                    'win_category' => $cartItems[$i]->win_cat,
                    'win_category_name' => $cartItems[$i]->win_category_name,
                    'win_product' => $cartItems[$i]->win_product,
                    'win_product_name' => $cartItems[$i]->win_product_name,
                    'win_pattern' => $cartItems[$i]->win_pattern,
                    'win_pattern_name' => $cartItems[$i]->win_pattern_name,
                    'win_price' => $cartItems[$i]->win_price,
                    'win_upcharges_array' => json_encode($cartItems[$i]->win_upcharges_array),
                    'win_upcharges' => $cartItems[$i]->win_upcharges,
                    'win_discount_amt' => $cartItems[$i]->win_discount_amt,
                    'win_comment' => $cartItems[$i]->win_comment
                ));
            }
            $RoomCount = $cartItems[$i]->room_no;
            $WinCount = $cartItems[$i]->win_no;
        }

        if (sizeof($newCartItemsByWindow) > 0) {
            $newCartItemsByWindow['win_categories'] = json_encode($newCartItemsCategoryArray);
            array_push($newCartItems, $newCartItemsByWindow);
            $newCartItemsByWindow = [];
            $newCartItemsCategoryArray = [];
        }

        $quotTblData = array(
            'qt_cust_id' => $data['customer_id'],
            'qt_cust_type' => $data['customertype'],
            'qt_subtotal' => $data['subtotal'],
            'qt_tax' => $data['tax'],
            'qt_grand_total' => $data['grand_total'],
            'total_discount' => $data['total_discount'],
            'total_upcharge' => $data['total_upcharge'],
            'qt_order_date' => date("Y-m-d", strtotime($data['order_date'])),
            'qt_created_on' => date("Y-m-d H:i:s"),
            'qt_modified_on' => date("Y-m-d H:i:s"),
            'qt_created_by' => $this->session->userdata('user_id')
        );

        //$this->db->insert('c_quatation_table', $quotTblData);
        //$quote_id = $this->db->insert_id();
        for ($i = 0; $i < sizeof($newCartItems); $i++) {
            $newCartItems[$i]['fk_qt_id'] = null;
            $newCartItems[$i]['win_created_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_modified_on'] = date("Y-m-d H:i:s");
            $newCartItems[$i]['win_created_by'] = $this->session->userdata('user_id');
        }
        //$this->db->insert_batch('c_quatation_details', $newCartItems);
        //  echo json_encode($quotTblData);
        // echo json_encode($newCartItems);
        //  echo "<hr>";
        echo $this->generateInvoiceHtml($quotTblData, $newCartItems);
        die();
    }

    public function quotation_receipt($quote_id) {

      //  $this->permission_c->check_label('quotation_receipt')->create()->redirect();
        
        // Destroy cart : START
        $this->cart->destroy();
        // Destroy cart : END

        $data['quote_id'] = $quote_id;
        $data['company_profile'] = $this->settings->company_profile();
        $data['quotation'] = $this->db->select("c_quatation_table.*,customer_info.*")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('qt_id', $quote_id)
                        ->get('c_quatation_table')->row();
        $data['quotation_details'] = $this->db->select("c_quatation_details.*")
                        ->where('fk_qt_id', $quote_id)
                        ->order_by('qd_id', 'ASC')
                        ->get('c_quatation_details')->result();
        $data['categories'] = [];

        for ($i = 0; $i < sizeof($data['quotation_details']); $i++) {
            $categories = json_decode($data['quotation_details'][$i]->win_categories);
            if ($i == 0) {
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($data['categories'], $categories[$j]->win_category_name);
                }
            } else {
                $newWindowsCats = [];
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($newWindowsCats, $categories[$j]->win_category_name);
                }
                $newWindowsCatsCount = array_count_values($newWindowsCats);
                $dataCategoriesCount = array_count_values($data['categories']);
                foreach ($newWindowsCatsCount as $key => $value) {
                    if (array_key_exists($key, $dataCategoriesCount)) {
                        if ($dataCategoriesCount[$key] < $value) {
                            while ($dataCategoriesCount[$key] != $value) {
                                array_push($data['categories'], $key);
                                $dataCategoriesCount[$key] ++;
                            }
                        }
                    } else {
                        while ($value != 0) {
                            array_push($data['categories'], $key);
                            $value--;
                        }
                    }
                }
            }
        }

        // dd($data['quotation_details']);

        $roomWiseArray = [];
        $newRoomArray = [];
        $currentRoom = null;
        $totalWindow = sizeOf($data['quotation_details']);

        for ($i = 0; $i < sizeOf($data['quotation_details']); $i++) {
            // if (($currentRoom !== null && $data['quotation_details'][$i]->room_no !== $currentRoom)) {
            if (($currentRoom !== null && $data['quotation_details'][$i]->room_name !== $currentRoom)) {
                array_push($roomWiseArray, $newRoomArray);
                $newRoomArray = [];
            }
            array_push($newRoomArray, $data['quotation_details'][$i]);
            //  $currentRoom = $data['quotation_details'][$i]->room_no;
            $currentRoom = $data['quotation_details'][$i]->room_name;
        }

        if (sizeOf($newRoomArray) > 0) {
            array_push($roomWiseArray, $newRoomArray);
            $newRoomArray = [];
        }

        $data['roomWiseArray'] = $roomWiseArray;
 
        $data['customer_wise_sidemark'] = $this->db->select('customer_info.*,c_us_state_tbl.tax_rate')->from('customer_info')->join('c_us_state_tbl', 'c_us_state_tbl.state_name=customer_info.state', 'left')->where('customer_info.customer_id', $data['quotation']->customer_id)->get()->row();

        // echo "<pre>";
        // print_r($data);
        // exit;
        $data['orderjs'] = "c_level/quotation/order_js.php";


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/quotation/quotation_receipt', $data);
        $this->load->view('c_level/footer');
    }

    public function send_email_quotation_receipt($quote_id) {
        
        $data['quote_id'] = $quote_id;
        $data['company_profile'] = $this->settings->company_profile();
        $data['quotation'] = $this->db->select("c_quatation_table.*,customer_info.*")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('qt_id', $quote_id)
                        ->get('c_quatation_table')->row();
        $data['quotation_details'] = $this->db->select("c_quatation_details.*")
                        ->where('fk_qt_id', $quote_id)
                        ->order_by('qd_id', 'ASC')
                        ->get('c_quatation_details')->result();
        $data['categories'] = [];

        for ($i = 0; $i < sizeof($data['quotation_details']); $i++) {
            $categories = json_decode($data['quotation_details'][$i]->win_categories);
            if ($i == 0) {
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($data['categories'], $categories[$j]->win_category_name);
                }
            } else {
                $newWindowsCats = [];
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($newWindowsCats, $categories[$j]->win_category_name);
                }
                $newWindowsCatsCount = array_count_values($newWindowsCats);
                $dataCategoriesCount = array_count_values($data['categories']);
                foreach ($newWindowsCatsCount as $key => $value) {
                    if (array_key_exists($key, $dataCategoriesCount)) {
                        if ($dataCategoriesCount[$key] < $value) {
                            while ($dataCategoriesCount[$key] != $value) {
                                array_push($data['categories'], $key);
                                $dataCategoriesCount[$key] ++;
                            }
                        }
                    } else {
                        while ($value != 0) {
                            array_push($data['categories'], $key);
                            $value--;
                        }
                    }
                }
            }
        }

        $roomWiseArray = [];
        $newRoomArray = [];
        $currentRoom = null;
        $totalWindow = sizeOf($data['quotation_details']);

        for ($i = 0; $i < sizeOf($data['quotation_details']); $i++) {
            // if (($currentRoom !== null && $data['quotation_details'][$i]->room_no !== $currentRoom)) {
            if (($currentRoom !== null && $data['quotation_details'][$i]->room_name !== $currentRoom)) {
                array_push($roomWiseArray, $newRoomArray);
                $newRoomArray = [];
            }
            array_push($newRoomArray, $data['quotation_details'][$i]);
            //  $currentRoom = $data['quotation_details'][$i]->room_no;
            $currentRoom = $data['quotation_details'][$i]->room_name;
        }

        if (sizeOf($newRoomArray) > 0) {
            array_push($roomWiseArray, $newRoomArray);
            $newRoomArray = [];
        }

        $data['roomWiseArray'] = $roomWiseArray;
 
        $data['customer_wise_sidemark'] = $this->db->select('customer_info.*,c_us_state_tbl.tax_rate')->from('customer_info')->join('c_us_state_tbl', 'c_us_state_tbl.state_name=customer_info.state', 'left')->where('customer_info.customer_id', $data['quotation']->customer_id)->get()->row();

        
        $mailContent = $this->load->view('c_level/quotation/email_html_quotation_receipt', $data, true);

        $to = $data['quotation']->email;
        if($to != "") {
            $subject = 'Quotation Details';
           
            // Send email user configuration wise : START
            $CI = &get_instance();
            $CI->load->model('Common_model');
            $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));

            if(isset($config['smtp_user']) && $config['smtp_user'] != ''){
                $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
                $this->email->set_header('Content-type', 'text/html');
                $this->load->library('email', $config);
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from($config['smtp_user'], $data['company_profile']->company_name);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($mailContent);

                if ($this->email->send()) {
                    // echo 'Quotation sent successfully on customers email address!';
                     return true;
                } else {
                    // echo 'Unable to send email. Please try again.';
                     return false;
                }
            }else{
                // echo 'Please verified email configuration first';
                 return false;
            }    

            // Send email user configuration wise : END

        } else {
            // echo "No email found";
            return false;
        }
        
    }

    public function send_sms_quotation_receipt($quote_id) {

        $data['quotation'] = $this->db->select("c_quatation_table.*,customer_info.*")
                        ->join('customer_info', 'customer_info.customer_id=c_quatation_table.qt_cust_id', 'left')
                        ->where('qt_id', $quote_id)
                        ->get('c_quatation_table')->row();

        if ($data['quotation']->phone != NULL) {

            $this->load->library('twilio');

            if (isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != '') {
                $name = $data['quotation']->first_name . ' ' . $data['quotation']->last_name;

                $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $data['quotation']->phone;
                // $message = 'Hi! ' . $name . ' Order Successfully. OrderId ' . $data['quotation']->qt_id;
                $message = 'Hi! ' . $name . ' Thank you for having us. You will receive an email for quote';
                $response = $this->twilio->sms($from, $to, $message);

                return true;
            } else {
                // $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . SMS_VERIFICATION_ERROR . "</div>");
                return false;
            }
        }
    }

    public function generateInvoiceHtml($quotTblData, $newCartItems) {
        $data['company_profile'] = $this->settings->company_profile();
        $data['quotTblData'] = $quotTblData;
        $data['customerInfo'] = $this->db->select("*")->where('customer_id', $quotTblData['qt_cust_id'])->get('customer_info')->row();
        $data['quotation_details'] = $newCartItems;
        $data['categories'] = [];
        for ($i = 0; $i < sizeof($data['quotation_details']); $i++) {
            $categories = json_decode($data['quotation_details'][$i]['win_categories']);
            if ($i == 0) {
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($data['categories'], $categories[$j]->win_category_name);
                }
            } else {
                $newWindowsCats = [];
                for ($j = 0; $j < sizeof($categories); $j++) {
                    array_push($newWindowsCats, $categories[$j]->win_category_name);
                }
                $newWindowsCatsCount = array_count_values($newWindowsCats);
                $dataCategoriesCount = array_count_values($data['categories']);
                foreach ($newWindowsCatsCount as $key => $value) {
                    if (array_key_exists($key, $dataCategoriesCount)) {
                        if ($dataCategoriesCount[$key] < $value) {
                            while ($dataCategoriesCount[$key] != $value) {
                                array_push($data['categories'], $key);
                                $dataCategoriesCount[$key] ++;
                            }
                        }
                    } else {
                        while ($value != 0) {
                            array_push($data['categories'], $key);
                            $value--;
                        }
                    }
                }
            }
        }
        $roomWiseArray = [];
        $newRoomArray = [];
        $currentRoom = null;
        $totalWindow = sizeOf($data['quotation_details']);
        for ($i = 0; $i < sizeOf($data['quotation_details']); $i++) {
            //     if (($currentRoom !== null && $data['quotation_details'][$i]['room_no'] !== $currentRoom)) {
            if (($currentRoom !== null && $data['quotation_details'][$i]['room_name'] !== $currentRoom)) {
                array_push($roomWiseArray, $newRoomArray);
                $newRoomArray = [];
            }
            array_push($newRoomArray, $data['quotation_details'][$i]);
            //  $currentRoom = $data['quotation_details'][$i]['room_no'];
            $currentRoom = $data['quotation_details'][$i]['room_name'];
        }
        if (sizeOf($newRoomArray) > 0) {
            array_push($roomWiseArray, $newRoomArray);
            $newRoomArray = [];
        }

        $data['roomWiseArray'] = $roomWiseArray;

        // echo "<pre>";print_r($data['roomWiseArray']);die;



        $invoiceHtml = '<div class="row" id="printableArea" style="background-color: #FFF;"><div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card"><div class="card-body"><h2>' . $data['company_profile'][0]->company_name . ',</h2><h4>' . $data['company_profile'][0]->address . ', ' . $data['company_profile'][0]->city . ',</h4><h4>' . $data['company_profile'][0]->state . ', ' . $data['company_profile'][0]->zip_code . ', ' . $data['company_profile'][0]->country_code . '</h4><h4>' . $data['company_profile'][0]->phone . '</h4></div></div></div><div class="col-lg-6 col-md-6 col-sm-6"><div class="card"><div class="card-body"><table><tr><td style="width: 40%;"><strong>Date</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . date_format(date_create($data['quotTblData']['qt_order_date']), 'Y-M-d') . '</td></tr><tr><td colspan="3">&nbsp;<br/></td></tr><tr><td><strong>Customer Name</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . $data['customerInfo']->first_name . ' ' . $data['customerInfo']->last_name . '</td></tr>';

        if ($data['customerInfo']->company != "") {
            $invoiceHtml .= '<tr><td><strong>Comopany Name</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . $data['customerInfo']->company . '</td></tr>';
        }
        $invoiceHtml .= '<tr><td><strong>Address</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . $data['customerInfo']->address . ', ' . $data['customerInfo']->city . ', ' . $data['customerInfo']->state . ', ' . $data['customerInfo']->zip_code . ', ' . $data['customerInfo']->country_code . '</td></tr><tr><td><strong>Phone Number</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . $data['customerInfo']->phone . '</td></tr><tr><td><strong>Email ID</strong></td><td>&nbsp;:&nbsp;&nbsp;</td><td>' . $data['customerInfo']->email . '</td></tr></table></div></div></div>';
        $invoiceDetailsArray = [];
        $invoiceHtml .= '<div class="col-lg-12 mb-4"><div class="card mb-4"><div class="card-body"><h5 class="mb-4">Details</h5><div class="separator mb-5"></div><table class=" table table-bordered table-hover table_print"><thead><tr><th>SL</th><th>Room</th><th colspan="2">Window size</th><th>Comment</th>';
        $catNo = 1;
        foreach ($data['categories'] as $key => $val) {
            if ($val == "Misc") {
                $invoiceHtml .= '<th colspan="2">' . $val . '</th>';
            } else {
                $invoiceHtml .= '<th colspan="3">' . $val . '</th>';
            }
            $invoiceDetailsArray[$val . "-" . $catNo]['subtotal'] = 0;
            $invoiceDetailsArray[$val . "-" . $catNo]['discount'] = 0;
            $invoiceDetailsArray[$val . "-" . $catNo]['upcharge'] = 0;
            $invoiceDetailsArray[$val . "-" . $catNo]['salestax'] = 0;
            $invoiceDetailsArray[$val . "-" . $catNo]['grandtotal'] = 0;
            $catNo++;
        }
        $invoiceHtml .= '</tr><tr><th>SL#</th><th>Room</th><th>Width</th><th>Height</th><th></th>';

        foreach ($data['categories'] as $key => $val) {
            $invoiceHtml .= '<th>Product</th>';
            if ($val != 'Misc') {
                $invoiceHtml .= '<th>Pattern</th>';
            }
            $invoiceHtml .= '<th>Price</th>';
        }

        $invoiceHtml .= '</tr></thead><tbody>';

        foreach ($data['roomWiseArray'] as $key => $room) {
            $invoiceHtml .= '<tr><td rowspan="' . sizeof($room) . '">' . ($key + 1) . '</td><td rowspan="' . sizeof($room) . '">' . $room[0]['room_name'] . '</td>';
            for ($i = 0; $i < sizeof($room); $i++) {
                if ($i == 0) {
                    $invoiceHtml .= '<td>' . $room[$i]['win_width'] . '<sup style="color:red !important;">' . $room[$i]['win_wfraction'] . '</sup></td><td>' . $room[$i]['win_height'] . '<sup style="color:red !important;">' . $room[$i]['win_htfraction'] . '</sup></td>';
                    $invoiceHtml .= '<td>' . $room[$i]['win_comment'] . '</td>';
                    $winCategories = json_decode($room[$i]['win_categories']);
                    $newWinCategories = [];
                    $isPrinted = false;
                    $catNo = 1;
                    foreach ($data['categories'] as $cat => $catVal) {
                        if ($room[$i]['win_categories'] != null || $room[$i]['win_categories'] != "") {
                            for ($j = 0; $j < sizeof($winCategories); $j++) {
                                if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                    $invoiceHtml .= "<td>" . $winCategories[$j]->win_product_name . "</td>";
                                    if ($winCategories[$j]->win_category_name != "Misc") {
                                        $invoiceHtml .= "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                    }
                                    $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);

                                    // Add the upcharge price too : START
                                    $discounted_price += isset($winCategories[$j]->win_upcharges)?$winCategories[$j]->win_upcharges:0;
                                    // Add the upcharge price too : END

                                    $invoiceHtml .= "<td>" . sprintf("%.2f",(round($discounted_price,2))) . "</td>";
                                    $isPrinted = true;

                                    if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {

                                        $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                    } else {
                                        $fSubTotal = 0;
                                    }

                                    if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                        $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                    } else {
                                        $fUpcharges = 0;
                                    }

                                    if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                        $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                    } else {
                                        $fDiscount = 0;
                                    }

                                    $fTotal = $fSubTotal + $fUpcharges;

                                    if ($data['quotTblData']['qt_tax'] != 0 || $data['quotTblData']['qt_tax'] != "") {
                                        $fsalestax = $fTotal * (floatval($data['quotTblData']['qt_tax']) / 100);
                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                    } else {
                                        $fsalestax = 0;
                                    }

                                    if ($fSubTotal != 0) {
                                        $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                    }
                                } else {
                                    array_push($newWinCategories, $winCategories[$j]);
                                }
                            }
                            if ($isPrinted != true) {
                                if ($catVal == 'Misc') {
                                    $invoiceHtml .= "<td colspan='2'>&nbsp;</td>";
                                } else {
                                    $invoiceHtml .= "<td colspan='3'>&nbsp;</td>";
                                }
                            }
                            $winCategories = $newWinCategories;
                            $newWinCategories = [];
                            $isPrinted = false;
                        } else {
                            if ($catVal == "Misc") {
                                $invoiceHtml .= "<td colspan='2'>empty</td>";
                            } else {
                                $invoiceHtml .= "<td colspan='3'>empty</td>";
                            }
                        }
                        $catNo++;
                    }
                }
            }
            $invoiceHtml .= '</tr>';
            if (sizeof($room) > 0) {



                for ($i = 0; $i < sizeof($room); $i++) {

                    if ($i != 0) {
                        $invoiceHtml .= '<tr>';
                        $invoiceHtml .= '<td>' . $room[$i]['win_width'] . '<sup style="color:red !important;">' . $room[$i]['win_wfraction'] . '</sup></td><td>' . $room[$i]['win_height'] . '<sup style="color:red !important;">' . $room[$i]['win_htfraction'] . '</sup></td>';
                        $invoiceHtml .= '<td>' . $room[$i]['win_comment'] . '</td>';
                        $winCategories = json_decode($room[$i]['win_categories']);
                        $newWinCategories = [];
                        $isPrinted = false;
                        $catNo = 1;
                        foreach ($data['categories'] as $cat => $catVal) {
                            if ($room[$i]['win_categories'] != null || $room[$i]['win_categories'] != "") {
                                for ($j = 0; $j < sizeof($winCategories); $j++) {
                                    if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                        $invoiceHtml .= "<td>" . $winCategories[$j]->win_product_name . "</td>";
                                        if ($winCategories[$j]->win_category_name != "Misc") {
                                            $invoiceHtml .= "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                        }
                                        $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);

                                        // Add the upcharge price too : START
                                        $discounted_price += isset($winCategories[$j]->win_upcharges)?$winCategories[$j]->win_upcharges:0;
                                        // Add the upcharge price too : END

                                        $invoiceHtml .= "<td>" . sprintf("%.2f",(round($discounted_price,2))) . "</td>";
                                        $isPrinted = true;

                                        if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                            $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                        } else {
                                            $fSubTotal = 0;
                                        }

                                        if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                            $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                        } else {
                                            $fUpcharges = 0;
                                        }

                                        if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                            $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                        } else {
                                            $fDiscount = 0;
                                        }

                                        $fTotal = $fSubTotal + $fUpcharges;

                                        if ($data['quotTblData']['qt_tax'] != 0 || $data['quotTblData']['qt_tax'] != "") {
                                            $fsalestax = $fTotal * (floatval($data['quotTblData']['qt_tax']) / 100);
                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                        } else {
                                            $fsalestax = 0;
                                        }

                                        if ($fSubTotal != 0) {
                                            $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                        }
                                    } else {
                                        array_push($newWinCategories, $winCategories[$j]);
                                    }
                                }
                                if ($isPrinted != true) {
                                    if ($catVal == 'Misc') {
                                        $invoiceHtml .= "<td colspan='2'>&nbsp;</td>";
                                    } else {
                                        $invoiceHtml .= "<td colspan='3'>&nbsp;</td>";
                                    }
                                }
                                $winCategories = $newWinCategories;
                                $newWinCategories = [];
                                $isPrinted = false;
                            } else {
                                if ($catVal == "Misc") {
                                    $invoiceHtml .= "<td colspan='2'>empty</td>";
                                } else {
                                    $invoiceHtml .= "<td colspan='3'>empty</td>";
                                }
                            }
                            $catNo++;
                        }



                        $invoiceHtml .= '</tr>';
                    }
                }
            }
        }

        /*      $invoiceHtml .= '<tr><td colspan="5">Sub Total (' . $data['company_profile'][0]->currency . ')<br>(After Discount)</td>';
          $catNo = 1;
          foreach ($data['categories'] as $cat => $catVal) {
          if ($catVal == "Misc") {
          $invoiceHtml .= "<td style='border-right:0;'></td>";
          $invoiceHtml .= "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
          } else {
          $invoiceHtml .= "<td colspan='2' style='border-right:0;'></td>";
          $invoiceHtml .= "<td style'border-lrft:0;>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
          }
          $catNo++;
          }
          $invoiceHtml .= '</tr><tr><td colspan="5">Total Amount Saved(' . $data['company_profile'][0]->currency . ')</td>';
          $catNo = 1;
          foreach ($data['categories'] as $cat => $catVal) {
          if ($catVal == "Misc") {
          $invoiceHtml .= "<td style='border-right:0;'></td>";
          $invoiceHtml .= "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
          } else {
          $invoiceHtml .= "<td colspan='2' style='border-right:0;'></td>";
          $invoiceHtml .= "<td style'border-lrft:0;>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
          }
          $catNo++;
          }
          $invoiceHtml .= '</tr><tr><td colspan="5">Upcharge</td>';
          $catNo = 1;
          foreach ($data['categories'] as $cat => $catVal) {
          if ($catVal == "Misc") {
          $invoiceHtml .= "<td style='border-right:0;'></td>";
          $invoiceHtml .= "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
          } else {
          $invoiceHtml .= "<td colspan='2' style='border-right:0;'></td>";
          $invoiceHtml .= "<td style'border-lrft:0;>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
          }
          $catNo++;
          }
          $total = $data['quotTblData']['qt_subtotal'] - $data['quotTblData']['total_discount'] + $data['quotTblData']['total_upcharge'];
          $invoiceHtml .= '</tr><tr><td colspan="5">Sales Tax (' . number_format($data['quotTblData']['qt_tax'], 2, '.', '') . '%)</td>';
          $catNo = 1;
          foreach ($data['categories'] as $cat => $catVal) {
          if ($catVal == "Misc") {
          $invoiceHtml .= "<td style='border-right:0;'></td>";
          $invoiceHtml .= "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
          } else {
          $invoiceHtml .= "<td colspan='2' style='border-right:0;'></td>";
          $invoiceHtml .= "<td style'border-lrft:0;>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
          }
          $catNo++;
          }
          $invoiceHtml .= '</tr><tr><td colspan="5">Grand Total (' . $data['company_profile'][0]->currency . ')</td>';
          $catNo = 1;
          foreach ($data['categories'] as $cat => $catVal) {
          if ($catVal == "Misc") {
          $invoiceHtml .= "<td style='border-right:0;'></td>";
          $invoiceHtml .= "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
          } else {
          $invoiceHtml .= "<td colspan='2' style='border-right:0;'></td>";
          $invoiceHtml .= "<td style'border-lrft:0;>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
          }
          $catNo++;
          }
          $invoiceHtml .= '</tr>'; */

        $invoiceHtml .= '</tbody></table></div></div></div></div><hr/><div class="col-lg-12" style="border-top:1px solid #000; text-align: center;"><p>' . $data['company_profile'][0]->company_name . ', Address: ' . $data['company_profile'][0]->address . ', ' . $data['company_profile'][0]->city . ',' . $data['company_profile'][0]->state . ', ' . $data['company_profile'][0]->zip_code . ',' . $data['company_profile'][0]->country_code . ', Phone: ' . $data['company_profile'][0]->phone . '</p></div></div>';

        return $invoiceHtml;
    }

    public function send_invoice_email() {

        $mailContent = "";

        $mailContent .= '<!DOCTYPE html><html><head><style>
                table { 
                    border-spacing: 0px;
                    border-collapse: separate;
                }
                table.table_print tr th, table.table_print tr td {
                    padding: 5px;
                }
                .title-center{
                    text-align:center;
                    padding:10px;
                    margin-bottom:15px;
                    border-bottom: 1px solid black;
                }
                .left-content{
                    max-width:48%;
                    float:left;
                }
                .right-content{
                    max-width:48%;
                    float:right;
                }
                .custom-div-section{
                    width: 48% !important;
                    float: left;
                }
                .title-center h2 { margin-top:20px !important; text-align: center; } 
            </style></head><body>';

        $mailContent .= $this->input->post('printcontent');

        $mailContent .= '</body></html>';

        if($this->input->post('custemail') != "") {
            $to = $this->input->post('custemail');
            $subject = 'Quotation Details';
        
            // Send email user configuration wise : START
            $CI = &get_instance();
            $CI->load->model('Common_model');
            $config = $CI->Common_model->mailConfig($this->session->userdata('user_id'));

            if(isset($config['smtp_user']) && $config['smtp_user'] != ''){
                $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
                $this->email->set_header('Content-type', 'text/html');
                $this->load->library('email', $config);
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from($config['smtp_user'], $data['company_profile']->company_name);
                $this->email->to($to);
                $this->email->subject($subject);
                $this->email->message($mailContent);

                if ($this->email->send()) {
                    echo 'Quotation sent successfully on customers email address!';
                } else {
                    echo 'Unable to send email. Please try again.';
                }
            }else{
                echo 'Please verified email configuration first';
            }    

            // Send email user configuration wise : END

        } else {
            echo "No email found";
        }
    }

    public function get_product_details($isReturnReq = false) {
        $postData = $this->input->post();
        $selectedRoomNos = [];
        foreach ($postData as $key => $value) {
            if (!in_array($value['roomno'], $selectedRoomNos)) {
                array_push($selectedRoomNos, $value['roomno']);
            }
        }

        $selectedRoomDet = [];
        foreach ($selectedRoomNos as $rno) {
            $windowDetails = $this->db->select('*')->where('qd_id =', $rno)->get('c_quatation_details')->row();
            $selectedRoomDet[$rno] = $windowDetails;
        }

        $returnData = [];
        foreach ($postData as $key => $value) {
            $selectedRoomCats = json_decode($selectedRoomDet[$value['roomno']]->win_categories);
            // print_r($selectedRoomCats);
            $dataRecived = false;
            $currentCatPos = $value['catpos'];
            foreach ($selectedRoomCats as $cat) {
                if ($dataRecived == false) {
                    if ($value['catname'] == $cat->win_category_name) {
                        if ($currentCatPos == 1) {
                            $postData[$key]['selWinDet'] = $selectedRoomDet[$value['roomno']];
                            $postData[$key]['selCatDet'] = $cat;
                            $dataRecived = true;
                        } else {
                            $currentCatPos--;
                        }
                    }
                }
            }
        }

        if ($isReturnReq) {
            return $postData;
        } else {
            echo json_encode($postData);
        }
    }

    public function get_product_discount() {
        if ($this->input->post('productids') == "") {
            $productids = 0;
        } else {
            $productids = array_unique(json_decode($this->input->post('productids')));
        }
        if (sizeof($productids) > 0) {
            $taxRatesData = $this->db->select("product_id,individual_cost_factor,costfactor_discount")->where_in('product_id', $productids)->get('c_cost_factor_tbl')->result();
        } else {
            $taxRatesData = array();
        }
        echo json_encode($taxRatesData);
    }

    public function getIdOfFraction($frac) {
        return $this->db->query("SELECT id FROM `width_height_fractions` where fraction_value='$frac'")->row()->id;
    }

    public function saveColorSessionForOrderPopup() {
        $color_id = $this->input->post('color_id');
        $color_code = $this->input->post('color_code');
        //  echo 'color got '.$color_code;
        $this->session->set_userdata(array('color_code' => $color_code, 'color_id' => $color_id));
        echo $this->session->userdata('color_code') . ' ' . $this->session->userdata('color_id');
    }

    public function get_new_order_form() {
        // echo "<pre>";print_r($this->input->post());die;
        // $data['cart_rowid'] = $cart_rowid = $this->uri->segment(4);
        $data['view_action'] = $view_action = 0;
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->where('created_by', $this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->settings->company_profile();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();


        if ($this->input->post('data') && $this->input->post('data') != "") {
            $selectedData = $this->input->post('data');
            // echo "<pre>";print_r($selectedData);die;
            $data['win_to_edit'] = array(
                'room' => $selectedData['win_room_name'],
                'product_id' => $selectedData['win_product_id'],
                'category_id' => $selectedData['win_category_id'],
                'pattern_model_id' => $selectedData['win_pattern'],
                'width' => $selectedData['win_width'],
                'height' => $selectedData['win_height'],
                'height_fraction_id' => $this->getIdOfFraction($selectedData['win_htfraction']),
                'width_fraction_id' => $this->getIdOfFraction($selectedData['win_wfraction']),
                'win_comment' => $selectedData['win_comment'],
            );
            $data['get_product_order_info'] = (object) $data['win_to_edit'];
            $data['selected_attributes'] = [];
        }

        $data['btn_receipt'] = 'quatation_receipt';
        // print_r($data['get_product_order_info']); die();
        // print_r($selectedData);
        echo $this->load->view('c_level/orders/order_new_form', $data);
        echo $this->load->view('c_level/orders/order_js');
    }

    // public function get_new_order_form() {
    //     $data['get_customer'] = $this->Order_model->get_customer();
    //     $data['get_category'] = $this->Order_model->get_category();
    //     $data['get_patern_model'] = $this->Order_model->get_patern_model();
    //     $data['colors'] = $this->db->get('color_tbl')->result();
    //     $data['orderjs'] = "c_level/quotation/order_js.php";
    //     $data['currencys'] = $currencys = $this->settings->company_profile();
    //     $data['currency'] = $currencys[0]->currency;
    //     $data['fractions'] = $this->db->get('width_height_fractions')->result();
    //     $data['rooms'] = $this->db->get('rooms')->result();
    //     $data['binfo'] = $this->db->where('user_id', 1)->get('company_profile')->row();
    //     $order_form = $this->input->post('order_form');
    //     $this->load->view('c_level/quotation/convert_order', $data);
    // }


    public function get_quotation_edit_form() {
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();

        $selectedKey = "";
        $rowDtls = $this->get_product_details(true);
        foreach ($rowDtls as $key => $value) {
            $rowDtls = $value;
            $selectedKey = $key;
        }

        $quotDetails = array(
            'room' => $rowDtls['selWinDet']->room_name,
            'product_id' => $rowDtls['selCatDet']->win_product,
            'category_id' => $rowDtls['selCatDet']->win_category,
            'pattern_model_id' => $rowDtls['selCatDet']->win_pattern,
            'list_price' => $rowDtls['selCatDet']->win_price,
            'width' => $rowDtls['selWinDet']->win_width,
            'height' => $rowDtls['selWinDet']->win_height,
            'height_fraction_id' => $rowDtls['selWinDet']->win_htfraction,
            'width_fraction_id' => $rowDtls['selWinDet']->win_wfraction,
            'notes' => $rowDtls['selWinDet']->win_comment,
            'selectedKey' => $selectedKey
        );

        $data['quotDetails'] = $quotDetails;

        echo $this->load->view('c_level/quotation/edit_quotation_form', $data);
    }

    public function update_quotation_details() {
        $postData = $this->input->post();

        $RoomWindowTitleArray = explode("_", $postData['update_product']);

        $selectedRoomNo = $RoomWindowTitleArray[0];

        $windowDetails = $this->db->select('*')->where('qd_id =', $selectedRoomNo)->get('c_quatation_details')->row();

        $selectedRoomCats = json_decode($windowDetails->win_categories);

        //print_r($selectedRoomCats);

        $currentCatPos = $RoomWindowTitleArray[2];

        $otherCats = [];

        $catToUpdate = null;
        foreach ($selectedRoomCats as $cat) {
            if ($RoomWindowTitleArray[1] == $cat->win_category_name) {
                if ($currentCatPos == 1) {
                    $catToUpdate = $cat;
                } else {
                    array_push($otherCats, $cat);
                }
                $currentCatPos--;
            } else {
                array_push($otherCats, $cat);
            }
        }

        $catToUpdate->win_category = $postData['category_id'];
        $catToUpdate->win_category_name = $this->db->select('category_name')->from('category_tbl')->where('category_id = ' . $catToUpdate->win_category)->get()->row()->category_name;
        $catToUpdate->win_product = $postData['product_id'];
        $catToUpdate->win_product_name = $this->db->select('product_name')->from('product_tbl')->where('product_id = ' . $catToUpdate->win_product)->get()->row()->product_name;
        $catToUpdate->win_pattern = $postData['pattern_model_id'];
        $catToUpdate->win_pattern_name = $this->db->select('pattern_name')->from('pattern_model_tbl')->where('pattern_model_id = ' . $catToUpdate->win_pattern)->get()->row()->pattern_name;
        $catToUpdate->win_price = $postData['price'];
        $catToUpdate->win_comment = $postData['notes'];

        //print_r($otherCats);

        array_push($otherCats, $catToUpdate);

        //print_r($otherCats);
        //exit;

        $windowDetails->win_width = $postData['width'];

        if (isset($postData['width_fraction_id'])) {
            $windowDetails->win_wfraction = $postData['wfractionText'];
        }

        $windowDetails->win_height = $postData['height'];

        if (isset($postData['height_fraction_id'])) {
            $windowDetails->win_htfraction = $postData['hfractionText'];
        }

        $windowDetails->win_categories = json_encode($otherCats);

        $windowDetails->win_modified_on = date('Y-m-d H:i:s');

        if (is_object($windowDetails)) {
            $windowDetails = get_object_vars($windowDetails);
        }

        if ($this->db->where('qd_id =', $selectedRoomNo)->update('c_quatation_details', $windowDetails)) {
            echo true;
        } else {
            echo false;
        }
    }

    public function get_new_order_form_footer() {
        $data['orderid'] = $this->input->post('orderid');
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['colors'] = $this->db->get('color_tbl')->result();
        $data['orderjs'] = "c_level/quotation/order_js.php";
        $data['currencys'] = $currencys = $this->settings->company_profile();
        $data['currency'] = $currencys[0]->currency;
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['rooms'] = $this->db->get('rooms')->result();
        $user_id = $this->session->userdata('user_id');
        $b_level_user = $this->db->query("SELECT created_by FROM `user_info` where id=$user_id")->row()->created_by;
        $data['binfo'] = $this->db->where('user_id', $b_level_user)->get('company_profile')->row();
        $order_form = $this->input->post('order_form');
        echo $this->load->view('c_level/quotation/convert_order_footer', $data);
    }

    #-----------------------------------
    #   order save
    #----------------------------------

    public function save_order() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "inserted";
        $remarks = "order information inserted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');


        if (!empty($this->input->post('synk_status'))) {
            $synk_status = 1;
        } else {
            $synk_status = 0;
        }

        $barcode_img_path = '';

        if (!empty($orderid)) {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/c/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }


        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("new-order");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');

        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'customer_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $this->level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->user_id,
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => '',
            'synk_status' => $synk_status
        );

        $order_status = $this->input->post('order_status');

        if ($this->db->insert('quatation_tbl', $orderData)) {

            foreach ($products as $key => $product_id) {


                $productData = array(
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'room' => $room[$key],
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key]
                );


                //order details
                $this->db->insert('qutation_details', $productData);

                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                //order attributes
                $this->db->insert('quatation_attributes', $attrData);
            }


            // send email for customer
            $this->Order_model->send_link($orderData);
            //-----------------------
            // clear the cart session
            $this->cart->destroy();

            if ($synk_status == 1) {

                //$this->synk_data_to_b($order_id);
                echo json_encode(array('status' => "synk", 'redirect' => 'retailer-synk-data-wholesaler/' . $orderid));
            }

            echo json_encode(array('status' => "success", 'redirect' => 'retailer-order-view/' . $orderid, 'message' => "Order Placed successfully !"));
        } else {
            echo json_encode(array('status' => "danger", 'redirect' => 'retailer-manage-quotation', 'message' => "Internul error please try again"));
        }
    }

    public function synk_data_to_b($order_id = NULL) {

        $action_page = $this->uri->segment(2);
        $action_done = "synchronize";
        $remarks = "synchronize to b level order";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================

        $query = $this->db->select('*')
                        ->from('customer_info')
                        ->where('level_id', 1)
                        ->where('customer_user_id', $this->user_id)
                        ->order_by('customer_id', 'desc')
                        ->get()->row();



        if (empty($query->customer_id)) {

            return 1;
        }

        if (isset($order_id) && !empty($order_id)) {

            $orderd = $this->Order_model->get_orderd_by_id($order_id);
            $order_details = $this->Order_model->get_orderd_details_by_id($order_id);

            $g_order_id = $this->order_id_generate();
            $productData = [];
            $grand_total = 0;

            $barcode_img_path = '';

            if (!empty($g_order_id)) {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $g_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($g_order_id));
            }



            $attrData = [];
            $productData = [];

            foreach ($order_details as $value) {

                $product = $this->db->select('dealer_price')->where('product_id', $value->product_id)->get('product_tbl')->row();
                $product_cost_f = $this->db->select('dealer_cost_factor')->where('product_id', $value->product_id)->get('b_cost_factor_tbl')->row();
                $proAtt = $this->db->where('fk_od_id', $value->row_id)->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                if (isset($product_cost_f) && !empty($product_cost_f)) {

                    if ($product->dealer_price <= $product_cost_f->dealer_cost_factor) {
                        $cost_f = $product_cost_f->dealer_cost_factor;
                    } else {
                        $cost_f = $product->dealer_price;
                    }
                } else {
                    $cost_f = @$product->dealer_price;
                }

                $discount = ($value->list_price / 100) * @$cost_f;
                $unit_total_price = $value->list_price - @$discount;
                $grand_total += $unit_total_price;

                $productData = array(
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_qty' => $value->product_qty,
                    'list_price' => $value->list_price,
                    'discount' => @$cost_f,
                    'unit_total_price' => $unit_total_price,
                    'category_id' => $value->category_id,
                    'pattern_model_id' => $value->pattern_model_id,
                    'color_id' => $value->color_id,
                    'width' => $value->width,
                    'height' => $value->height,
                    'height_fraction_id' => $value->height_fraction_id,
                    'width_fraction_id' => $value->width_fraction_id,
                    'notes' => $value->notes
                );

                $this->db->insert('b_level_qutation_details', $productData);


                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_attribute' => $proAtt->product_attribute
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }



            $orderData = array(
                'order_id' => $g_order_id,
                'clevel_order_id' => $order_id,
                'order_date' => $orderd->order_date,
                'customer_id' => $query->customer_id,
                'is_different_shipping' => $orderd->is_different_shipping,
                'different_shipping_address' => $orderd->different_shipping_address,
                'level_id' => $this->user_id,
                'side_mark' => $orderd->side_mark,
                'upload_file' => $orderd->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => 0,
                'shipping_charges' => 0,
                'installation_charge' => 0,
                'other_charge' => 0,
                'misc' => 0,
                'invoice_discount' => 0,
                'grand_total' => $grand_total,
                'subtotal' => $grand_total,
                'paid_amount' => 0,
                'due' => $grand_total,
                'order_status' => $orderd->order_status,
                'created_by' => $orderd->created_by,
                'updated_by' => '',
                'created_date' => $orderd->created_date,
                'updated_date' => '',
                'status' => 0
            );


            $this->db->insert('b_level_quatation_tbl', $orderData);

            $this->db->set('synk_status', 1)->where('order_id', $order_id)->update('quatation_tbl');

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order synk successfully! </div>");
            return 1;
        } else {

            return 1;
        }
    }

    #-------------------------------------------
    #   get side mark and sales tax
    #-------------------------------------------

    public function add_customer() {
        $data['get_states'] = $this->Customer_model->get_states();
        $this->load->view('c_level/quotation/add_customer', $data);
    }

    public function delete_quotation_by_id() {
        $this->db->where('qt_id', $this->input->post('quotID'))->delete('c_quatation_table');
        $this->db->where('fk_qt_id', $this->input->post('quotID'))->delete('c_quatation_details');
        echo 'success';
    }

    public function customer_save_new() {

        $this->load->model('c_level/Customer_model');
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $phone_type = $this->input->post('phone_type');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');
        $level_id = 0;
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        dd($phone);
        //
        //       ============ its for accounts coa table ===============
        $coa = $this->Customer_model->headcode($level_id);
        if ($coa->HeadCode != NULL) {
            $hc = explode("-", $coa->HeadCode);
            $nxt = $hc[1] + 1;
            $headcode = $hc[0] . "-" .  $nxt;
        } else {
            $headcode = "1020301-1";
        }
        $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                ->order_by('customer_id', 'desc')
                ->get()
                ->row();
        //        echo $headcode;
        $sl = $lastid->customer_no;
        //        echo $sl;
        if (empty($sl)) {
            $sl = "CUS-0001";
        } else {
            $sl = $sl;
        }
        $supno = explode('-', $sl);
        $nextno = $supno[1] + 1;
        $si_length = strlen((int) $nextno);
        $str = '0000';
        $cutstr = substr($str, $si_length);
        $sino = "CUS" . "-" . $cutstr . $nextno; //$supno[0] . "-" . $cutstr . $nextno;
        //        dd($sino);
        //        $customer_name = $this->input->post('customer_name');
        $customer_no = $sino . '-' . $first_name . " " . $last_name;
        //        ================= close =======================
        //        =============== its for company name with customer id start =============
        $last_c_id = $lastid->customer_id;
        $cn = strtoupper(substr($company, 0, 3)) . "-";
        if (empty($last_c_id)) {
            $last_c_id = $cn . "1";
        } else {
            $last_c_id = $last_c_id;
        }
        $cust_nextid = $last_c_id + 1;
        $company_custid = $cn . $cust_nextid;
        //        =============== its for company name with customer id close=============
        //        ======== its for customer COA data array ============
        $customer_coa = array(
            'HeadCode' => $headcode,
            'HeadName' => $customer_no,
            'PHeadName' => 'Customer Receivable',
            'HeadLevel' => '4',
            'IsActive' => '1',
            'IsTransaction' => '1',
            'IsGL' => '0',
            'HeadType' => 'A',
            'IsBudget' => '0',
            'IsDepreciation' => '0',
            'DepreciationRate' => '0',
            'CreateBy' => $this->user_id,
            'level_id' => $level_id,
            'CreateDate' => $created_date,
        );
        //        dd($customer_coa);
        $this->db->insert('acc_coa', $customer_coa);
        //        ======= close ==============

        $customer_data = array(
            'company_customer_id' => $company_custid,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone[0],
            //            'phone_2' => $phone_2,
            //            'phone_3' => $phone_3,
            'company' => $company,
            'customer_no' => $customer_no,
            'customer_type' => 'personal',
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip_code,
            'country_code' => $country_code,
            'street_no' => $street_no,
            'side_mark' => $side_mark,
            'reference' => $reference,
            'level_id' => $level_id,
            'created_by' => $this->user_id,
            'create_date' => $created_date,
        );


        $this->db->insert('customer_info', $customer_data);
        $customer_inserted_id = $this->db->insert_id();


        //            ================ its for multiple certificate info save ===========

        if (isset($_FILES["file_upload"])) {
            if ($_FILES["file_upload"]['name'][0] != '') {
                $file_uploadCount = count($_FILES['file_uploader']['name']);
                for ($i = 0; $i < $file_uploadCount; $i++) {
                    $_FILES['file_uploader']['name'] = $_FILES['file_upload']['name'][$i];
                    $_FILES['file_uploader']['type'] = $_FILES['file_upload']['type'][$i];
                    $_FILES['file_uploader']['tmp_name'] = $_FILES['file_upload']['tmp_name'][$i];
                    $_FILES['file_uploader']['error'] = $_FILES['file_upload']['error'][$i];
                    $_FILES['file_uploader']['size'] = $_FILES['file_upload']['size'][$i];

                    // configure for upload 
                    $config = array(
                        'upload_path' => "./assets/c_level/uploads/customers/",
                        'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
                        'overwrite' => TRUE,
                        //            'file_name' => "BMSLINK" . time(),
                        'file_size' => '2048',
                    );
                    $image_data = array();

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('file_uploader')) {
                        $image_data = $this->upload->data();
                        //                print_r($image_data); die();
                        $customer_file_name = $image_data['file_name'];

                        $customerFileinfo[$i]['customer_id'] = $customer_inserted_id;
                        $customerFileinfo[$i]['customer_user_id'] = '';
                        $customerFileinfo[$i]['file_upload'] = $customer_file_name;
                        //                    $customerFileinfo[$i]['degree_name'] = $degrees_name[$i];
                    }
                }
                $this->Customer_model->save_customer_file($customerFileinfo);
            }
        }

        //        =============== its for customer phone type info ==============
        for ($i = 0; $i < count($phone); $i++) {
            $phone_types_number = array(
                'phone' => $phone[$i],
                'phone_type' => $phone_type[$i],
                'customer_id' => $customer_inserted_id,
                'customer_user_id' => '',
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
        }

        echo json_encode(array('msg' => 'success', 'id' => $customer_inserted_id));
        exit();
    }

    public function get_customers_opt() {
        $customers = $this->Order_model->get_customer();
        $html = '<option value=""></option>';
        foreach ($customers as $customer) {
            $html .= "<option value='" . $customer->customer_id . "'>" . $customer->first_name . " " . $customer->last_name . "</option>";
        }
        echo $html;
    }

    /** end added by itsea */
    /* Start added by insys */
    public function manage_action() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('c_quatation_table', 'qt_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Quotation has been Deleted successfully.</div>");
        }
        redirect("retailer-manage-quotation");
    }

    /* End added by insys */

    /****** Start: Function to calculate and save extra discount (17-01-2020)******/
    /*public function calculate_and_save_extra_discount() 
    {
        $extra_discount_amount  = htmlentities(strip_tags($this->input->post('extra_discount')));
        $category_grand_total   = htmlentities(strip_tags($this->input->post('category_grand_total')));
        $quotation_id           = htmlentities(strip_tags($this->input->post('quotation_id')));
        $array_position         = htmlentities(strip_tags($this->input->post('array_position')));

        $db_extra_discount      = 0.00;
        $total_extra_discount   = 0.00; 

        if($extra_discount_amount >= 0)
        {*/
            /******************* Start: Get Total Extra Discount ***********/
            /*$this->db->select('total_extra_discount');
            $this->db->from('c_quatation_table');
            $this->db->where('qt_id', $quotation_id);
            $db_extra_discount          = $this->db->get()->row()->total_extra_discount;*/
            /******************* End: Get Total Extra Discount ***********/

            /******************* Start: Get Quotation Grand Total ***********/
            /*$this->db->select('qt_grand_total');
            $this->db->from('c_quatation_table');
            $this->db->where('qt_id',$quotation_id);
            $quotation_grand_total      = $this->db->get()->row()->qt_grand_total;*/
            /******************* End: Get Quotation Grand Total ***********/

            /******************* Start: Get Quotation Details ***********/
            /*$this->db->select('qd_id, win_categories');
            $this->db->from('c_quatation_details');
            $this->db->where('fk_qt_id',$quotation_id);
            $quotation_rows     = $this->db->get()->result_object();*/
            /******************* End: Get Quotation Details ***********/

            /******************* Start: Update Extra Discount in JSON ***********/
            /*$total_quotation_rows       = count($quotation_rows);
            $individual_extra_discount  = bcdiv($extra_discount_amount, $total_quotation_rows, 3);
            if(isset($quotation_rows) && !empty($quotation_rows)) 
            {
                foreach($quotation_rows as $quotation_row) 
                {
                    $quotation_json_string      = $quotation_row->win_categories;
                    $quotation_json_array       = json_decode($quotation_json_string,true);

                    if(isset($quotation_json_array) && !empty($quotation_json_array)) 
                    {
                        if(isset($quotation_json_array[$array_position]['win_extra_discount_amt']) && !empty($quotation_json_array[$array_position]['win_extra_discount_amt'])) 
                        {
                            $category_grand_total           = bcadd($category_grand_total, $quotation_json_array[$array_position]['win_extra_discount_amt'], 3);
                        }
                        $quotation_json_array[$array_position]['win_extra_discount_amt'] = $individual_extra_discount;

                        $grand_total_comparison_result            = bccomp($quotation_grand_total, $extra_discount_amount, 2);
                        $category_grand_total_comparison_result   = bccomp($category_grand_total, $extra_discount_amount, 2);
                        if($grand_total_comparison_result !== -1 && $category_grand_total_comparison_result !== -1) 
                        {
                            $updated_category_grand_total   = bcsub ($category_grand_total, $extra_discount_amount, 3);

                            $this->db->trans_begin();

                            $this->db->set('win_categories', json_encode($quotation_json_array));
                            $this->db->where('qd_id', $quotation_row->qd_id);
                            $this->db->update('c_quatation_details');

                            if ($this->db->trans_status() === FALSE) 
                            {
                                    $this->db->trans_rollback();
                                    echo json_encode(array('status' => 'error', 'message' => 'Unable to save extra discount. Please try again.'));
                                    exit;
                            }
                            else 
                            {
                                    $this->db->trans_commit();
                                    foreach($quotation_json_array as $quotation_json) {
                                        $total_extra_discount  = bcadd($total_extra_discount, $quotation_json['win_extra_discount_amt'], 2);
                                    }
                                    $grand_total_to_add  = ($quotation_grand_total + $db_extra_discount) - $total_extra_discount;

                                    $this->db->set('qt_grand_total', $grand_total_to_add);
                                    $this->db->set('total_extra_discount', $total_extra_discount);
                                    $this->db->where('qt_id', $quotation_id);
                                    $this->db->update('c_quatation_table');
                            }
                        }
                        else 
                        {
                            echo json_encode(array('status' => 'error', 'message' => 'Discount value is greater than grand total.'));
                            exit;
                        }   
                    }  
                    else 
                    {
                        echo json_encode(array('status' => 'error', 'message' => 'Inconsistent item(s) present in the Quotation.'));
                        exit;
                    }                
                }
            }*/
            /******************* End: Update Extra Discount in JSON ***********/
        /*}
        else
        {
            echo json_encode(array('status' => 'error', 'message' => 'Please enter discount amount.'));   
            exit;
        } 
        echo json_encode(array('status' => 'success', 'message' => 'Extra Discount Applied Successfully.', 'grand_total' => number_format($updated_category_grand_total, 2), 'total_extra_discount' => $total_extra_discount));
        exit;
    }*/
    /****** End: Function to calculate and save extra discount ******/
    // Start Code for calculate and save extra discount by DM 01-02-2020
        function UpdateExtraDiscount(){
            $details                = $this->input->post('details');
            $position               = $this->input->post('position');
            $total_extra_discount   = $this->input->post('total_extra_discount');
            $count_product          = count($details);
            $product_extra_discount = $total_extra_discount / $count_product;
            $qt_id                  = $this->input->post('qt_id');
            if ($details) {
                foreach ($details as $details_key => $details_value) {
                    $qd_id = $details_value['qd_id'];
                    //get data of quatation details start
                    $this->db->select('win_categories');
                    $this->db->from('c_quatation_details');
                    $this->db->where('qd_id',$qd_id);
                    $qd_product_details = $this->db->get()->row_array();
                    $decoded_products_data = json_decode($qd_product_details['win_categories']);
                    if ($decoded_products_data) {
                        // start code for add extra discount in particular product in json formate
                        foreach ($decoded_products_data as $product_key => $product_value) {
                            if ($product_value->win_product == $details_value['product_id'] && $product_value->win_pattern == $details_value['pattern_id']) {
                                // check extra discount is already added or not
                                if (array_key_exists("extra_discount",$product_value))
                                  {
                                    $old_extra_discount = $product_value->extra_discount;
                                    $product_value->extra_discount = number_format($product_extra_discount, 2);
                                  }
                                else
                                  {
                                    $product_value->extra_discount = number_format($product_extra_discount, 2);
                                  }
                            }
                        }
                        // update product extra discount in json formate in c_quatation_details table
                        $encode_products_data = json_encode($decoded_products_data);
                        $this->db->set('win_categories', $encode_products_data)->where('qd_id', $qd_id)->update('c_quatation_details');
                        // end code for add extra discount in particular product in json formate
                    }
                }
                if(isset($old_extra_discount)){
                    $total_old_extra_discount = $old_extra_discount * $count_product;
                    //get qt details and manage old total extra discount in c_quatation_table and manage old qt_grand_total in c_quatation_table table in database
                    $this->db->select('qt_grand_total,total_extra_discount');
                    $this->db->from('c_quatation_table');
                    $this->db->where('qt_id',$qt_id);
                    $old_qt_details = $this->db->get()->row_array();


                    // manage grand total and total extra discount
                    $old_grand_extra_discount = $old_qt_details['total_extra_discount'] - number_format($total_old_extra_discount, 2);
                    $old_qt_grand_total       = $old_qt_details['qt_grand_total'] + number_format($total_old_extra_discount, 2);
                    $old_qt_data = array(
                        'qt_grand_total'        => $old_qt_grand_total,
                        'total_extra_discount'  => $old_grand_extra_discount,
                    );
                    $old_qt_details_update = $this->db->set($old_qt_data)->where('qt_id', $qt_id)->update('c_quatation_table');
                }
                if ($qt_id) {
                    //get qt details and add total extra discount in c_quatation_table and manage qt_grand_total in c_quatation_table table in database
                    $this->db->select('qt_grand_total,total_extra_discount');
                    $this->db->from('c_quatation_table');
                    $this->db->where('qt_id',$qt_id);
                    $qt_details = $this->db->get()->row_array();

                    $grand_extra_discount = $qt_details['total_extra_discount'] + number_format($total_extra_discount, 2);
                    $qt_grand_total       = $qt_details['qt_grand_total'] - number_format($total_extra_discount, 2);
                    $qt_data = array(
                        'qt_grand_total'        => $qt_grand_total,
                        'total_extra_discount'  => $grand_extra_discount,
                    );
                    $qt_details_update = $this->db->set($qt_data)->where('qt_id', $qt_id)->update('c_quatation_table');
                    if ($qt_details_update) {
                        echo json_encode(array('status' => 'success', 'message' => 'Extra Discount Applied Successfully.'));
                    } else{
                        echo json_encode(array('status' => 'error', 'message' => 'Extra Discount Applied Successfully!'));
                    }
                }
            }
            exit;
        }
    // End Code for calculate and save extra discount by DM 01-02-2020
}
