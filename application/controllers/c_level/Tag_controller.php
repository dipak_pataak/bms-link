<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_Controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        if ($session_id == '' || $user_type != 'c') {
            redirect('retailer-logout');
        }
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $this->user_id = $this->session->userdata('user_id');
    }

    public function index() {
        
    }

    public function add_tag() {
        $this->permission->check_label(74)->create()->redirect();
        $access_retailer=access_role_permission_page(74);
        $acc=access_role_retailer();
        $config["base_url"] = base_url('c_gallery_tag');
        $config["total_rows"] = $this->db->where('tag_tbl.created_by', $this->level_id)->from('tag_tbl')->count_all_results();
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["tags"] = $this->db->from('tag_tbl')->where('tag_tbl.created_by', $this->level_id)->order_by('tag_tbl.id', 'desc')->limit($config["per_page"], $page)->get()->result();
        $data["links"] = $this->pagination->create_links();
        $data["patterns"] = $this->db->get('pattern_model_tbl')->result();
        $data['pagenum'] = $page;
        if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/tag/add_tag', $data);
                $this->load->view('c_level/footer');
        }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
        }
    }

    //======== its for exists get_tag check ===========
    public function get_tag_check($tag) {
        $query = $this->db->where('tag_name', $tag)
                ->where('tag_tbl.created_by', $this->level_id)
                ->get('tag_tbl')
                ->row();
        if (!empty($query)) {
            echo $query->tag_name;
        } else {
            echo 0;
        }
    }

    public function save_tag() {
        $tagData = array(
            'tag_name' => $this->input->post('tag_name'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d')
        );
        $this->db->insert('tag_tbl', $tagData);

        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Tag information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Tag save successfull </div>");
        redirect('c_gallery_tag');
    }

    public function delete_tag($id) {
        $this->db->where('id', $id)->where('tag_tbl.created_by', $this->level_id)->delete('tag_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "Tag information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //        ============== close access log info =================
        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Tag deleted successfully! </div>");
        redirect('c_gallery_tag');
    }

    public function get_tag($id) {
        $result = $this->db->where('id', $id)->where('tag_tbl.created_by', $this->level_id)->get('tag_tbl')->row();
        echo json_encode($result);
    }

    public function update_tag() {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "Tag information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $tagData = array(
            'tag_name' => $this->input->post('tag_name'),
            'updated_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d')
        );

        $id = $this->input->post('id');

        $this->db->where('id', $id)->where('tag_tbl.created_by', $this->level_id)->update('tag_tbl', $tagData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Tag update successfull </div>");
        redirect('c_gallery_tag');
    }

    // =========== its for import_tag_save ===========
    public function import_tag_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "tag csv information imported done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //  ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");

        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {

            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['tag_name'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                }
                $data = array(
                    'tag_name' => $insert_csv['tag_name'],
                    'created_by' => $this->user_id,
                    'created_date' => date('Y-m-d'),
                );
                if ($count > 0) {
                    $result = $this->db->select('*')
                            ->from('tag_tbl')
                            ->where('tag_name', $data['tag_name'])
                            ->where('tag_tbl.created_by', $this->level_id)
                            ->get()
                            ->num_rows();
                    if ($result == 0 && !empty($data['tag_name'])) {
                        $this->db->insert('tag_tbl', $data);
                    } else {
                        $data = array(
                            'tag_name' => $insert_csv['tag_name'],
                            'updated_by' => $this->user_id,
                            'updated_date' => date('Y-m-d'),
                        );
                        $this->db->where('tag_name', $data['tag_name']);
                        $this->db->where('tag_tbl.created_by', $this->level_id);
                        $this->db->update('tag_tbl', $data);

                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Tag imported successfully!</div>");
        redirect('c_gallery_tag');
    }

    //=============== c_level_pattern_search ============
    public function c_level_tag_search() {
        $keyword = $this->input->post('keyword');
        $data['tags'] = $this->db->select('*')->from('tag_tbl')->like('tag_name', $keyword, 'both')->where('tag_tbl.created_by', $this->level_id)->order_by('id', 'desc')->limit(50)->get()->result();
        $this->load->view('c_level/tag/tag_search', $data);
    }
}
