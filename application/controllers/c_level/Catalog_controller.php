<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';
    private $main_b_id = '';

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        $this->main_b_id = $this->session->userdata('main_b_id');


        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($session_id == '' || $user_type != 'c') {
            redirect('retailer-logout');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Catalog_model');

    }

    /*........... CATEGORY SECTION ..............*/
    function catalog_category_add_edit_view($category_id = 0)
    {
        if(!empty($category_id))
            $data['data'] = $this->db->where('category_id',$category_id)->get('category_tbl')->row();

        $data['parent_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/category_add', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_category_add_update_api()
    {

        if(!isset($_POST['category_name']) || empty($_POST['category_name']))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Category name is required!</div>");
            redirect('customer/catalog/category/add');
        }
        if(!isset($_POST['category_id']) || empty($_POST['category_id']))
        {
            $this->Catalog_model->add_customer_category($this->user_id,$_POST);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Category info saved successfully!</div>");
        }
        else
        {
            $this->Catalog_model->update_customer_category($this->user_id,$_POST);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Category info updated successfully!</div>");
        }
        redirect('customer/catalog/category/list');
    }

    public function catalog_category_list($page = 0) {
        $access_retailer=access_role_permission_page(77);
        $acc=access_role_retailer();
        $search = (object) array(
                    'cat_name' => $this->input->post('cat_name'),
                    'parent_cat' => $this->input->post('parent_cat'),
                    'category_status' => $this->input->post('category_status'),
        );


        $config["base_url"] = base_url('customer/catalog/category/lst');
        $config["total_rows"] = $this->Catalog_model->get_category_count($this->main_b_id,$search);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_category"] = $this->Catalog_model->search_category($config["per_page"], $page,$search);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========
         if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3){
                if ($access_retailer==1 or $acc==1) {
                        $this->load->view('c_level/header');
                        $this->load->view('c_level/sidebar');
                        $this->load->view('c_level/catalog/category_list', $data);
                        $this->load->view('c_level/footer');
                }else{
                        $this->load->view('c_level/header');
                        $this->load->view('c_level/sidebar'); 
                        $this->load->view('c_level/upgrade_error'); 
                        $this->load->view('c_level/footer');
                }
        }else{
          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');

        }
    }

    public function c_category_delete($category_id) {

        $pattern_info = $this->db->select('*')->from('pattern_model_tbl')->where('category_id', $category_id)->get()->result();
        if ($pattern_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some patterns!</div>");
            redirect("customer/catalog/category/list");
        }
        $condition_info = $this->db->select('*')->from('product_conditions_tbl')->where('category_id', $category_id)->get()->result();
        if ($condition_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some conditions!</div>");
            redirect("customer/catalog/category/list");
        }
        $product_cat_info = $this->db->select('*')->from('product_tbl')->where('category_id', $category_id)->get()->result();
        if ($product_cat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some products!</div>");
            redirect("customer/catalog/category/list");
        }
        $product_subcat_info = $this->db->select('*')->from('product_tbl')->where('subcategory_id', $category_id)->get()->result();
        if ($product_subcat_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some sub categories!</div>");
            redirect("customer/catalog/category/list");
        }
        $attributes_info = $this->db->select('*')->from('attribute_tbl')->where('category_id', $category_id)->get()->result();
        if ($attributes_info) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This category depends some attributes!</div>");
            redirect("customer/catalog/category/list");
        }
        $this->db->where('category_id', $category_id)->delete('category_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Category deleted successfully!</div>");
        redirect("customer/catalog/category/list");
    }

    public function multipal_catagory_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('category_tbl', 'category_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/category/list");
    }

    /*......... END CATEGORY SECTION ............*/

    /*......... START COLOR SECTION ............*/
    function catalog_color_add_edit_view($color_id = 0)
    {
        if(!empty($color_id))
            $data['data'] = $this->db->where('id',$color_id)->get('color_tbl')->row();

        $data["patterns"] = $this->db->where('created_by',$this->user_id)->where('created_status',1)->where('status',1)->get('pattern_model_tbl')->result();


        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/color_add', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_color_add_update_api()
    {

        if(!isset($_POST['color_name']) || empty($_POST['color_name']))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Color name is required!</div>");
            redirect('customer/catalog/color/add');
        }
        if(!isset($_POST['color_number']) || empty($_POST['color_number']))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Color number is required!</div>");
            redirect('customer/catalog/color/add');
        }
        if(!isset($_POST['color_id']) || empty($_POST['color_id']))
        {
            if (@$_FILES['color_img']['name']) {
                $config['upload_path'] = './assets/retailer/uploads/colors/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['overwrite'] = false;
                $config['max_size'] = 2048;
                $config['remove_spaces'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('color_img')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                    redirect("customer/catalog/color/list");
                } else {

                    $data = $this->upload->data();
                    $upload_file = $config['upload_path'] . $data['file_name'];
                }
            } else {
                $upload_file = '';
            }

            $this->Catalog_model->add_customer_color($this->user_id,$_POST,$upload_file);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Color info saved successfully!</div>");
        }
        else
        {
            if (@$_FILES['color_img']['name']) {

                $config['upload_path'] = './assets/retailer/uploads/colors/';
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['overwrite'] = false;
                $config['max_size'] = 2048;
                $config['remove_spaces'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('color_img')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                    redirect("customer/catalog/color/list");
                } else {

                    $data = $this->upload->data();
                    $upload_file = $config['upload_path'] . $data['file_name'];
                }
            } else {
                $upload_file = $this->input->post('hid_color_img');
            }

            $this->Catalog_model->update_customer_color($this->user_id,$_POST,$upload_file);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Color info updated successfully!</div>");
        }
        redirect('customer/catalog/color/list');
    }

    function catalog_color_list($page = 0)
    {

        $search = (object) array(
                    'colorname' => $this->input->post('colorname'),
                    'colornumber' => $this->input->post('colornumber'),
                    'pattern_name' => $this->input->post('pattern_name'),
        );

        $config["base_url"] = base_url('customer/catalog/color/list');
        $config["total_rows"] = $this->Catalog_model->get_color_count($this->main_b_id,$search);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_color"] = $this->Catalog_model->search_color($config["per_page"], $page,$search);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $access_retailer=access_role_permission_page(78);
        $acc=access_role_retailer();

        //  ========= its for pagination close===========
          if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
        if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/color_list', $data);
                $this->load->view('c_level/footer');
        }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
    }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

    public function delete_color($id) 
    {
        $this->db->select('colors');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($id, colors)");
        $check_colors = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_colors->num_rows() > 0) {

            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Can't delete it. Color already assigned!  </div>");
            redirect('customer/catalog/color/list');
        } else {
            $this->db->where('id', $id)->delete('color_tbl');
          
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                 Color deleted successfully! </div>");
            redirect('customer/catalog/color/list');
        }
    }

    public function multipal_color_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('color_tbl', 'id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/color/list");
    }

    /********** PRODUCT SECTION START ***********/
    function catalog_product_add_view()
    {
        $data['get_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $data['patern_model'] = $this->Catalog_model->get_patern_model($this->user_id);
        $data['get_product_conditions'] = $this->Catalog_model->get_product_conditions($this->user_id);
            
        $data['style_slist'] = $this->db->get('row_column')->result();
        $data['js'] = "c_level/products/product_addjs";
        $data['colors'] = $this->db->order_by('color_name', 'ASC')->get('color_tbl')->result();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/product_add', $data);
        $this->load->view('c_level/footer');
    }

    function category_wise_condition($category_id) {

        $data['category_wise_condition'] = $this->Catalog_model->category_wise_condition($category_id);

        $this->load->view('c_level/products/category_wise_condition', $data);
    }

    function category_wise_pattern($category_id)
    {
        if ($category_id == 'none') {
            $category_id = ' ';
            $where = "category_id < 0";
        } else {
            $where = "category_id = $category_id";
        }
        $category_wise_pattern = $this->db->select('*')
                        ->where($where)
                        ->where('status', 1)
                        ->where('created_by', $this->session->userdata('user_id'))
                        ->order_by('pattern_name', 'asc')
                        ->order_by('created_status', 1)
                        ->get('pattern_model_tbl')->result();

        $q = "";
        if (!empty($category_wise_pattern)) {

            $q .= '<select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                                ';
            foreach ($category_wise_pattern as $value) {
                $q .= "<option value='$value->pattern_model_id'>$value->pattern_name</option>";
            }
            $q .= '</select>';
        }
        echo $q;
    }

    function get_attr_by_attr_types($type_id)
    {

        $query = $this->db->select("*")
                        ->from('attribute_tbl')
                        ->where('attribute_type_id', $type_id)
                        ->get()->result();

        echo '<table width="80%" class="border-0" border="0">';

        foreach ($query as $key => $value) {

            echo '<tr>
                        <td id="Prod_01" class="border-0"><input type="checkbox" class="checkboxes" name="attribute_id[]" value="' . $value->attribute_id . '" onChange="shuffle_pricebox(' . $value->attribute_id . ');"> </td>
                        <td class="border-0"><b>' . $value->attribute_name . '</b></td>
                        <td class="border-0"><input type="number" name="attribute_price_' . $value->attribute_id . '" id="' . $value->attribute_id . '" placeholder="$0.00" disabled></td>
                        <td class="border-0">
                            <div><input type="radio" name="price_type_' . $value->attribute_id . '" value="1" checked> $ &nbsp;
                            <input type="radio" name="price_type_' . $value->attribute_id . '" value="2"> %</div>
                        </td>
                    </tr>';
        }
        echo '</table>';
    }


    function get_colors_based_on_pattern_ids() 
    {
        $pattern_ids = $this->input->post('pattern_ids');
        $this->db->where_in('pattern_id',$pattern_ids);
        $this->db->order_by('color_name', 'ASC');
        $color_list = $this->db->get('color_tbl')->result_array();
        echo json_encode($color_list);
    }

    function price_model_wise_style() 
    {

        $id = $this->input->post('id');
        $style_slist = $this->db->where('style_type', $id)->where('create_by', $this->session->userdata('user_id'))->order_by('style_name', 'asc')->get('row_column')->result();
        echo json_encode($style_slist);
    }

    function get_price_style($id)
    {
        $data = $this->db->where('style_id', $id)->get('price_chaild_style')->result();
        $table = '';
        $table .= '<table class="table-bordered">';
        $i = 1;

        foreach ($data as $key => $value) {

            $rowData = explode(',', $value->row);
            $table .= '<tr>';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td>' . $val . '</td>';
                } else {
                    $table .= '<td>' . $val . '</td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $i++;
        }

        $table .= '</table>';

        echo $table;
    }


    function save_product_style()
    {
        $product_name = $this->input->post('product_name');
        $category_id = $this->input->post('category_id');
        $subcategory_id = $this->input->post('subcategory_id');
        $pattern_models = $this->input->post('pattern_model_id');
        $price_style_id = $this->input->post('price_style_id');
        $sqft_price = $this->input->post('sqft_price');
        $fixed_price = $this->input->post('fixed_price');
        $dealer_price = $this->input->post('dealer_price');
        $status = $this->input->post('status');
        $attribute_type_id = $this->input->post('attribute_type_id');

        if (@$_FILES['product_img']['name']) {

            $config['upload_path'] = './assets/retailer/uploads/products/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 2048;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('product_img')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("customer/catalog/product/add");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            $upload_file = '';
        }

        //------------
        $colors = $this->input->post('color_id');
        $colors_id = '';
        foreach ($colors as $key => $color_id) {
            $colors_id .= $color_id . ',';
        }
        $colors_ids = rtrim($colors_id, ',');
//        =========== its for multiple id save one field use comma separator ============
        $patterns_id = '';
        foreach ($pattern_models as $key => $pattern_id) {
            $patterns_id .= $pattern_id . ',';
        }
        $patterns_ids = rtrim($patterns_id, ',');

//        $condition_id = $this->input->post('condition_id');
        $shipping = $this->input->post('shipping');
//        $tax = $this->input->post('tax');
//        $init_stock = $this->input->post('init_stock');
//        $individual_price = $this->input->post('individual_price');

        $productData = array(
            'product_name' => $this->input->post('product_name'),
            'category_id' => $this->input->post('category_id'),
            'subcategory_id' => ($this->input->post('subcategory_id') != NULL ? $this->input->post('subcategory_id') : 0),
            'pattern_models_ids' => $patterns_ids,
            'price_style_type' => $this->input->post('price_style_type'),
            'price_rowcol_style_id' => $this->input->post('price_style_id'),
            'sqft_price' => $this->input->post('sqft_price'),
            'fixed_price' => $this->input->post('fixed_price'),
            'dealer_price' => $this->input->post('dealer_price'),
            'individual_price' => $this->input->post('individual_price'),
            'shipping' => $this->input->post('shipping'),
            'colors' => $colors_ids,
            'product_img' => $upload_file,
            'is_taxable' => $this->input->post('is_taxable'),
            'product_description' => $this->input->post('product_description'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
            'active_status' => $this->input->post('status'),
            'created_status'=> 1
        );

        if ($this->db->insert('product_tbl', $productData)) {

            $product_id = $this->db->insert_id();

//              $cost_factor_data = array(
//                'product_id' => $product_id,
//                'individual_cost_factor' => 0,
//                'costfactor_discount' => 0,
//                'level_id' => 0,
//                'created_by' => 0,
//                'create_date' => date('Y-m-d'),
//            );
//            $this->db->insert('c_cost_factor_tbl', $cost_factor_data);
            // if(!empty($attributes) && $product_id!=NULL){
            //     $attribute_mapping = [];
            //     foreach ($attributes as $key => $attribute_id) {
            //         $price = $this->input->post('attribute_price_'.$attribute_id);
            //         $price_type = $this->input->post('price_type_'.$attribute_id);
            //         $attribute_mapping[] = array(
            //             'product_id'        => $product_id,
            //             'attribute_id'      => $attribute_id,
            //             'price_type'        => $price_type,
            //             'price'             => (!empty($price)?$price:0),
            //             'created_by'         => $this->session->userdata('user_id'),
            //             'created_date'      => date('Y-m-d'),
            //         );
            //     }
            //     $this->db->insert_batch('product_attribute_mapping',$attribute_mapping);
            //     $productPriceStyleData = array(
            //         'product_id'        => $product_id,
            //         'style_id'          => $this->input->post('price_style_id'),
            //         'created_by'         => $this->session->userdata('user_id'),
            //         'created_date'      => date('Y-m-d'),
            //     );
            // }


            $this->session->set_flashdata('message', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                " . $product_name . " Add successfull </div>");

////  =========== its for product condition mapping ==============
//            for ($i = 0; $i < count($condition_id); $i++) {
//                $product_condition_mapping = array(
//                    'condition_id' => $condition_id[$i],
//                    'product_id' => $product_id,
//                );
//                $this->db->insert('product_condition_mapping', $product_condition_mapping);
//            }
////            =========== close product condition mapping ============
        }

        redirect('customer/catalog/product/list/');
    }


    function catalog_product_edit_view($product_id)
    {
        $data['get_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $data['patern_model'] = $this->Catalog_model->get_patern_model($this->user_id);

        $data['js'] = "c_level/products/product_addjs";
        $data['category_wise_condition'] = $this->Catalog_model->category_wise_condition($product_id);
        $data['checked_product_wise_condition'] = $this->Catalog_model->checked_product_wise_condition($product_id);
        $data['product'] = $this->Catalog_model->get_product_by_id($product_id);

        $data['style_slist'] = $this->db->where('style_type', $data['product']->price_style_type)->where('create_by', $this->level_id)->order_by('style_name', 'asc')->get('row_column')->result();

        // echo "<pre>";print_r($data['product']->pattern_models_ids);die;

        if($data['product']->pattern_models_ids != ''){
            $pattern_ids = explode(',',$data['product']->pattern_models_ids);
            $this->db->where_in('pattern_id',$pattern_ids);
            $this->db->order_by('color_name', 'ASC');
            $data['colors'] = $this->db->get('color_tbl')->result();
        }else{
            $data['colors'] = [];
        }
//        echo '<pre>';        print_r($data['product']); die();
        //$data['product_attribute_mapping'] = $this->product_model->get_product_attribute_mapping_by_id($product_id);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/edit_product', $data);
        $this->load->view('c_level/footer');
    }

    //------------------------------------------------------
    // Category waise subcategory
    //------------------------------------------------------
     public function category_wise_subcategory($category_id = null)
    {
        $category_wise_subcategory = $this->db->select('*')
            ->where('parent_category', $category_id)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')->result();

        $q = "";
        if (!empty($category_wise_subcategory)) {

            $q .= '<label for="subcategory_id" class="mb-2">Sub Category</label>
                            <select class="form-control select2" name="subcategory_id" id="sub_category_id" data-placeholder="-- select one --"">
                                <option value=""></option>';

            foreach ($category_wise_subcategory as $value) {
                $q .= "<option value='$value->category_id'>$value->category_name</option>";
            }

            $q .= '</select>';

        } else {
            // echo 'no data found';
        }
        echo $q;
    }

    function catalog_product_update_api()
    {
        $product_id = $this->input->post('product_id');

        $product_name = $this->input->post('product_name');
        $category_id = $this->input->post('category_id');
        $subcategory_id = $this->input->post('subcategory_id');
        $pattern_models = $this->input->post('pattern_model_id');
        $price_style_id = $this->input->post('price_style_id');
        $sqft_price = $this->input->post('sqft_price');
        $fixed_price = $this->input->post('fixed_price');
        $dealer_price = $this->input->post('dealer_price');
        $status = $this->input->post('status');
        $attribute_type_id = $this->input->post('attribute_type_id');

        //$attributes = $this->input->post('attribute_id');
//        $condition_id = $this->input->post('condition_id');
        $shipping = $this->input->post('shipping');
//        $tax = $this->input->post('tax');
//        $init_stock = $this->input->post('init_stock');
//        $individual_price = $this->input->post('individual_price');
        //------------
        $colors = $this->input->post('color_id');
        $colors_id = '';

        if(!empty($pattern_models)){
        foreach ($colors as $key => $color_id) {
            $colors_id .= $color_id . ',';
            }
            $colors_ids = rtrim($colors_id, ',');
        }else{
            $colors_ids = '';
        }
        //------------------
        //        =========== its for multiple id save one field use comma separator ============
        
        if(!empty($pattern_models)){

        $patterns_id = '';
            foreach ($pattern_models as $key => $pattern_id) {
                $patterns_id .= $pattern_id . ',';
            }
            $patterns_ids = rtrim($patterns_id, ',');
        }else{
            $patterns_ids = '';
        }

        if (@$_FILES['product_img']['name']) {

            $config['upload_path'] = './assets/retailer/uploads/products/';
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['overwrite'] = false;
            $config['max_size'] = 2048;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('product_img')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("customer/catalog/product/add");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            $upload_file = $this->input->post('hid_product_img');
        }

        $productData = array(
            'product_name' => $this->input->post('product_name'),
            'category_id' => $this->input->post('category_id'),
            'subcategory_id' => ($this->input->post('subcategory_id') != NULL ? $this->input->post('subcategory_id') : 0),
            'pattern_models_ids' => @$patterns_ids,
            'price_style_type' => $this->input->post('price_style_type'),
            'price_rowcol_style_id' => $this->input->post('price_style_id'),
            'sqft_price' => $this->input->post('sqft_price'),
            'fixed_price' => $this->input->post('fixed_price'),
            'dealer_price' => $this->input->post('dealer_price'),
            'individual_price' => $this->input->post('individual_price'),
            'shipping' => $this->input->post('shipping'),
            'colors' => @$colors_ids,
            'product_img' => $upload_file,
            'is_taxable' => $this->input->post('is_taxable'),
            'product_description' => $this->input->post('product_description'),
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => date('Y-m-d'),
            'active_status' => $this->input->post('status')
        );
//        echo '<pre>';        print_r($productData);die();

        $result = $this->db->where('product_id', $product_id)->update('product_tbl', $productData);


////            =========== its for product condition mapping ==============
//        $this->db->where('product_id', $product_id);
//        $this->db->delete('product_condition_mapping');
//
//        for ($i = 0; $i < count($condition_id); $i++) {
//            $product_condition_mapping = array(
//                'condition_id' => $condition_id[$i],
//                'product_id' => $product_id,
//            );
//            $this->db->insert('product_condition_mapping', $product_condition_mapping);
//        }
////            =========== close product condition mapping ============

        if ($result) {
            // if(!empty($attributes) && $product_id!=NULL){
            //     $this->db->where('product_id',$product_id)->delete('product_attribute_mapping');
            //     $attribute_mapping = [];
            //     foreach ($attributes as $key => $attribute_id) {
            //         $price = $this->input->post('attribute_price_'.$attribute_id);
            //         $price_type = $this->input->post('price_type_'.$attribute_id);
            //         $attribute_mapping[] = array(
            //             'product_id'        => $product_id,
            //             'attribute_id'      => $attribute_id,
            //             'price_type'        => $price_type,
            //             'price'             => (!empty($price)?$price:0),
            //             'created_by'         => $this->session->userdata('user_id'),
            //             'created_date'      => date('Y-m-d'),
            //         );
            //     }
            //     $this->db->insert_batch('product_attribute_mapping',$attribute_mapping);
            // }

            $this->session->set_flashdata('success', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                " . $product_name . " Product updated successfull </div>");
        }
        //redirect('b_level/product_controller/maping_product_attribute/' . $product_id);

       redirect('customer/catalog/product/list/');
    }

    function catalog_product_list($page = 0)
    {
        $search = (object) array(
                    'category_id' => $this->input->post('category_id'),
                    'price_style_type' => $this->input->post('price_style_type'),
                    'status' => $this->input->post('status'),
        );

        $config["base_url"] = base_url('customer/catalog/product/list');
        $config["total_rows"] = $this->Catalog_model->get_product_count($this->main_b_id,$search);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_product"] = $this->Catalog_model->search_product($config["per_page"], $page,$search);

        $data["get_category"] = $this->Catalog_model->get_category();

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $access_retailer=access_role_permission_page(79);
        $acc=access_role_retailer();

        //  ========= its for pagination close===========

        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
        if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/product_list', $data);
                $this->load->view('c_level/footer');
         }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
    }else{
          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

      public function delete_product($product_id) {

        $this->db->where('product_id', $product_id)->delete('product_tbl');
//        $this->db->where('product_id', $product_id)->delete('product_attribute_mapping');
//        $this->db->where('product_id', $product_id)->delete('product_condition_mapping');

        $this->session->set_flashdata('success', "<div class='alert alert-success'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Product delete successfull </div>");

        redirect('customer/catalog/product/list');
    }

    public function multipal_product_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('product_tbl', 'product_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/product/list");
    }

    /********** PRODUCT SECTION END ***********/


    /*........... PATTERN SECTION START ..............*/
    function catalog_pattern_add_edit_view($pattern_model_id = 0)
    {
        if(!empty($pattern_model_id))
            $data['data'] = $this->db->where('pattern_model_id',$pattern_model_id)->get('pattern_model_tbl')->row();

        $data['parent_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/pattern_add', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_pattern_add_update_api()
    {

        if(!isset($_POST['pattern_name']) || empty($_POST['pattern_name']))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Pattern name is required!</div>");
            redirect('customer/catalog/pattern/add');
        }
        if(!isset($_POST['category_id']) || empty($_POST['category_id']))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Category is required!</div>");
            redirect('customer/catalog/pattern/add');
        }
        if(!isset($_POST['pattern_model_id']) || empty($_POST['pattern_model_id']))
        {
            $this->Catalog_model->add_customer_pattern($this->user_id,$_POST);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Pattern info saved successfully!</div>");
        }
        else
        {
            $this->Catalog_model->update_customer_pattern($this->user_id,$_POST);
            $this->session->set_flashdata('success', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                Pattern info updated successfully!</div>");
        }
        redirect('customer/catalog/pattern/list');
    }

    function catalog_pattern_list($page = 0)
    {
        $config["base_url"] = base_url('customer/catalog/pattern/list');
        $config["total_rows"] = $this->Catalog_model->get_pattern_count($this->main_b_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_pattern"] = $this->Catalog_model->search_pattern($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
         $access_retailer=access_role_permission_page(80);
        $acc=access_role_retailer();

        //  ========= its for pagination close===========
        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3){
            if ($access_retailer==1 or $acc==1) {
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/catalog/pattern_list', $data);
                    $this->load->view('c_level/footer');
              }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
                }
        }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/upgrade_error');
        $this->load->view('c_level/footer');
         }
    }

     public function pattern_delete($pattern_id) {
        $this->db->select('pattern_models_ids');
        $this->db->from('product_tbl  a');
        $this->db->where("FIND_IN_SET($pattern_id, pattern_models_ids)");
        $check_pattern = $this->db->get();
//        echo '<pre>';        print_r($check_colors);die();
        if ($check_pattern->num_rows() > 0) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Can't delete it. pattern already assigned!</div>");
            redirect("customer/catalog/pattern/list");
        } else {
            $this->db->where('pattern_model_id', $pattern_id)->delete('pattern_model_tbl');

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Pattern deleted successfully!</div>");
            redirect("customer/catalog/pattern/list");
        }
    }

    public function multipal_pattern_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('pattern_model_tbl', 'pattern_model_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/pattern/list");
    }

    /*...........  END PATTERN SECTION ..............*/

    /*...........  START ATTRIBUTE SECTION ..........*/
    function catalog_attribute_add_edit_view($attribute_id = 0)
    {
        if(!empty($attribute_id))
        {
            $data['attribute'] = $this->db->where('attribute_id', $attribute_id)->where('created_by',$this->level_id)->get('attribute_tbl')->row();
            $data['option'] = $this->db->where('attribute_id', $attribute_id)->get('attr_options')->result();
            $data['categorys'] = $this->db->select('category_name,category_id')->order_by('category_name','asc')->get('category_tbl')->result();
        }

        $data['parent_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/attribute_add', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_attribute_add_update_api() 
    {

        if($_POST['attribute_id']==0) 
        {
            $this->Catalog_model->add_attribute($_POST);
        }else{
            $this->Catalog_model->edit_attribute($_POST['attribute_id'],$_POST);
        }


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute save successfully!</div>");
        redirect("customer/catalog/attribute/list");
    }


    function catalog_attribute_list($page = 0)
    {
        $search = (object) array(
                    'attribute_name' => $this->input->post('attribute_name'),
                    'category_id' => $this->input->post('category_id'),
        );

        $config["base_url"] = base_url('customer/catalog/attribute/list');
        $config["total_rows"] = $this->Catalog_model->get_attribute_count($this->main_b_id,$search);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_attribute"] = $this->Catalog_model->search_attribute($config["per_page"], $page,$search);

        $data["get_category"] = $this->Catalog_model->get_category();

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $access_retailer=access_role_permission_page(81);
        $acc=access_role_retailer();
        //  ========= its for pagination close===========
         if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){

        if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/attribute_list', $data);
                $this->load->view('c_level/footer');
          }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
    }else{
          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

    public function attribute_delete($attr_id) {
        $this->db->where('attribute_id', $attr_id)->delete('attribute_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_tbl');
        $this->db->where('attribute_id', $attr_id)->delete('attr_options_option_option_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Attribute delete successfully!</div>");
        redirect('customer/catalog/attribute/list');
    }

    public function multipal_attribute_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('attribute_tbl', 'attribute_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/attribute/list");
    }

    /*...........  END ATTRIBUTE SECTION ..........*/

    /*...........  START PRICE SECTION ..........*/
    function catalog_price_add_view($attribute_id = 0)
    {

        $data['style_slist'] = $this->db->get('row_column')->result();
        //$data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        // if(!empty($attribute_id))
        // {
        //     $data['attribute'] = $this->db->where('attribute_id', $attribute_id)->where('created_by',$this->level_id)->get('attribute_tbl')->row();
        //     $data['option'] = $this->db->where('attribute_id', $attribute_id)->get('attr_options')->result();
        //     $data['categorys'] = $this->db->select('category_name,category_id')->order_by('category_name','asc')->get('category_tbl')->result();
        // }

        // $data['parent_category'] = $this->Catalog_model->get_customer_parent_category($this->user_id);
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/price_add', $data);
        $this->load->view('c_level/footer');
    }


        public function save_price_style() 
        {

        $dataName = array(
            'style_name' => $this->input->post('price_style_name'),
            'style_type' => $this->input->post('style_type'),
            'create_data_time' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('user_id'),
            'active_status' => 1,
            'created_status' => 1,
        );

        $this->db->insert('row_column', $dataName);

        // echo $this->db->last_query();die;

        $getStyle_id = $this->db->insert_id();

        if ($getStyle_id) {

            $getData = $this->input->post('row');
            $price_chaildData = [];
            foreach ($getData as $key => $value) {
                $price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($value, ','));
                $rowData[] = explode(',', $value);
            }


            $j = 0;
            foreach ($rowData as $row => $columns) {

                $j++;
                $i = 0;
                $rss = '';
                foreach ($columns as $row2 => $column2) {
                    $i++;

                    if ($j > 1) {

                        if ($_POST['test1' . $j] != '' && $_POST['test' . $i . "1"] != '') {

                            $priceData[] = array(
                                'style_id' => $getStyle_id,
                                'row' => $_POST['test' . $i . "1"],
                                'col' => $_POST['test1' . $j],
                                'price' => $_POST['test' . $i . $j]
                            );
                        }
                    }
                    $rss .= $_POST['test' . $i . $j] . ',';
                }

                //$price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($rss, ','));
            }

            $this->db->insert_batch('price_style', $priceData);
            $this->db->insert_batch('price_chaild_style', $price_chaildData);

            $style_type = $this->input->post('style_type');
            if ($style_type==1) 
            {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Price save successfully!</div>");
                redirect('customer/catalog/price/list');
            }
            if ($style_type==4) 
            {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Group Price save successfully!</div>");
                redirect('customer/catalog/group-price/list');
            }
            
        } else {
            echo 2;
        }
    }

    function catalog_price_edit_view($style_id)
    {
        $datas = $this->db->where('style_id', $style_id)->get('price_chaild_style')->result();
        $table = '';
        $i = 1;

        foreach ($datas as $key => $value) {

            $rowData = explode(',', $value->row);

            //print_r($rowData);

            $table .= '<tr>';
            $table .= '<input type="hidden" name="row[]" value="' . $value->row . '">';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" class="price_input" id="' . $ids . '" autocomplete="off" /></td>';
                } else {
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" autocomplete="off" /></td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $table .= '<br/>';
            $i++;
        }


        $table .= '</table>';

        $data['style_slist'] = $this->db->where('style_id', $style_id)->get('row_column')->row();
        $data['list'] = $table;
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/edit_price_style', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_price_update()
    {

        $style_id = $this->input->post('style_id');
        $dataName = array(
            'style_name' => $this->input->post('price_style_name'),
            'style_type' => $this->input->post('style_type'),
            'create_data_time' => date('Y-m-d H:i:s'),
            'create_by' => $this->session->userdata('user_id'),
        );

        $this->db->where('style_id', $style_id)->update('row_column', $dataName);
        $getStyle_id = $style_id;

        if (!empty($getStyle_id)) {
            $this->db->where('style_id', $getStyle_id)->delete('price_style');
            $this->db->where('style_id', $getStyle_id)->delete('price_chaild_style');
        }

        if ($getStyle_id) {

            $getData = $this->input->post('row');

            foreach ($getData as $key => $value) {

                $rowData[] = explode(',', $value);
            }


            $j = 0;
            foreach ($rowData as $row => $columns) {
      
                $j++;
                $i = 0;
                $rss = '';
                foreach ($columns as $row2 => $column2) {

                    $i++;

                    if ($j > 1) {

                        if ($_POST['test1' . $j] != '' && $_POST['test' . $i . "1"] != '') {
                            $priceData[] = array(
                                'style_id' => $getStyle_id,
                                'row' => $_POST['test' . $i . "1"],
                                'col' => $_POST['test1' . $j],
                                'price' => $_POST['test' . $i . $j]
                            );
                        }
                    }
                    $rss .= $_POST['test' . $i . $j] . ',';
                }

                $price_chaildData[] = array('style_id' => $getStyle_id, 'row' => rtrim($rss, ','));
            }


           // $this->db->insert_batch('price_style', $priceData);
            $this->db->insert_batch('price_chaild_style', $price_chaildData);


            $style_type = $this->input->post('style_type');
            if ($style_type==1) 
            {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Price Update successfully!</div>");
                redirect('customer/catalog/price/list');
            }
            if ($style_type==4) 
            {
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Group Price Update successfully!</div>");
                redirect('customer/catalog/group-price/list');
            }

        } else {
            echo 2;
        }
    }

    
    function catalog_price_list($page = 0)
    {
        $config["base_url"] = base_url('customer/catalog/price/list');
        $config["total_rows"] = $this->Catalog_model->get_price_count($this->main_b_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_price"] = $this->Catalog_model->search_price($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
         $access_retailer=access_role_permission_page(82);
        $acc=access_role_retailer();

        //  ========= its for pagination close===========
        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3){
        if ($access_retailer==1 or $acc==1) {       
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/price_list', $data);
                $this->load->view('c_level/footer');
         }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
        }
          }else{
           $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
          }
    }

    public function delete_price_style($style_id) {
   
        $this->db->set('price_rowcol_style_id', 0)->where('price_rowcol_style_id', $style_id)->update('product_tbl');
        $this->db->where('style_id', $style_id)->delete('row_column');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Style Delete successfully!</div>");
        redirect("customer/catalog/price/list");
    }

    public function multipal_price_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('row_column', 'style_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/price/list");
    }

    public function delete_group_price_style($style_id) {
   
        $this->db->set('price_rowcol_style_id', 0)->where('price_rowcol_style_id', $style_id)->update('product_tbl');
        $this->db->where('style_id', $style_id)->delete('row_column');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Style Delete successfully!</div>");
        redirect("customer/catalog/group-price/list");
    }

    public function multipal_group_price_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('row_column', 'style_id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/group-price/list");
    }

    /*...........  END PRICE SECTION ..........*/

    /*Sqm Price mapping*/

    function catalog_sqm_price_mapping()
    {
        $this->permission_c->check_label_multi(85)->create()->redirect();
        $data['customet_product_list'] = $this->Catalog_model->get_customer_product_list();

        $config["base_url"] = base_url('customer/catalog/sqm-price-mapping/list');
        $config["total_rows"] = $this->Catalog_model->get_sqm_price_count($this->main_b_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_sqm_price"] = $this->Catalog_model->search_sqm_price($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========

           if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/sqm_price_mapping', $data);
        $this->load->view('c_level/footer');
    }else{
          $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }

    }

    function catalog_sqm_price_delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sqm_price_model_mapping_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping Delete successfully!</div>");
        redirect('customer/catalog/sqm-price-mapping/list');
    }

    public function multipal_sqm_price_mapping_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('sqm_price_model_mapping_tbl', 'id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/sqm-price-mapping/list");
    }


    function save_sqm_maping_price()
    {
        $pattern_ids = $this->input->post('pattern_id');


        foreach ($pattern_ids as $key => $value) {

            $mapData = array(
                'price' => $this->input->post('sqm_price'),
                'product_id' => $this->input->post('product_id'),
                'pattern_id' => $value
            );

            $this->db->insert('sqm_price_model_mapping_tbl', $mapData);
        }


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping successfully!</div>");
        redirect('customer/catalog/sqm-price-mapping/list');
    }

    
    function get_product_pattern($product_id)
    {
        $productData = $this->db->where('product_id', $product_id)->get('product_tbl')->row();

        if (isset($productData->pattern_models_ids) && !empty($productData->pattern_models_ids))
        {
            $pattern_arr = explode(',', $productData->pattern_models_ids);
            $patternData = $this->db->where_in('pattern_model_id', $pattern_arr)->get('pattern_model_tbl')->result();
            echo json_encode($patternData);
            exit();

        } else
        {
            $data = [];
            echo json_encode($data);
            exit();
        }
    }

    

    /*Sqm Price mapping*/
    

    /*Group Price mapping*/
    function catalog_group_price_mapping()
    {

        $data['customet_product_list'] = $this->Catalog_model->get_customer_product_list();
        $data['style_slist'] = $this->Catalog_model->get_customer_row_column_list();

        $config["base_url"] = base_url('customer/catalog/group-price-mapping/list');
        $config["total_rows"] = $this->Catalog_model->get_group_price_mapping_count($this->main_b_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_sqm_price"] = $this->Catalog_model->search_group_price_mapping($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
         $access_retailer=access_role_permission_page(86);
        $acc=access_role_retailer();

        //  ========= its for pagination close===========
         if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/group_price_mapping', $data);
                $this->load->view('c_level/footer');
          }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
    }else{
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }

    }

    function catalog_group_price_delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('price_model_mapping_tbl');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping Delete successfully!</div>");
        redirect('customer/catalog/group-price-mapping/list');
    }

    function save_group_maping_price()
    {
        $pattern_ids = $this->input->post('pattern_id');


        foreach ($pattern_ids as $key => $value) {

            $mapData = array(
                'group_id' => $this->input->post('group_price'),
                'product_id' => $this->input->post('product_id'),
                'pattern_id' => $value
            );

            $this->db->insert('price_model_mapping_tbl', $mapData);
        }


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping successfully!</div>");
        redirect('customer/catalog/group-price-mapping/list');
    }

    public function multipal_group_price_mapping_delete() {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('price_model_mapping_tbl', 'id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Category has been Deleted successfully.</div>");
        }
        redirect("customer/catalog/group-price-mapping/list");
    }

    /*Group Price mapping*/


    /*...........  START GROUP PRICE SECTION ..........*/

     function catalog_group_price_add_view() 
     {
        $data['style_slist'] = $this->db->get('row_column')->result();
        // $data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/group_price_add', $data);
        $this->load->view('c_level/footer');
    }


    function catalog_group_price_edit_view($style_id)
    {
        $datas = $this->db->where('style_id', $style_id)->get('price_chaild_style')->result();
        $table = '';
        $i = 1;


        foreach ($datas as $key => $value) {

            $rowData = explode(',', $value->row);

            //print_r($rowData);

            $table .= '<tr>';
            $table .= '<input type="hidden" name="row[]" value="' . $value->row . '">';
            $j = 1;
            foreach ($rowData as $key => $val) {
                if ($i != 1 && $j != 1) {
                    $ids = $i . '_' . $j;
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" class="price_input" id="' . $ids . '" autocomplete="off" /></td>';
                } else {
                    $table .= '<td><input type="text" size="5" name="test' . $j . $i . '" value="' . $val . '" autocomplete="off" /></td>';
                }
                $j++;
            }
            $table .= '</tr>';
            $table .= '<br/>';
            $i++;
        }


        $table .= '</table>';

        $data['style_slist'] = $this->db->where('style_id', $style_id)->get('row_column')->row();
        $data['list'] = $table;
        $data['price_style_js'] = "b_level/pricemodel/_price_style_js";

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/edit_group_price_style', $data);
        $this->load->view('c_level/footer');
    }

    function catalog_group_price_list($page = 0)
    {
        $config["base_url"] = base_url('customer/catalog/group-price/list');
        $config["total_rows"] = $this->Catalog_model->get_group_price_count($this->main_b_id);
        $config["per_page"] = 10;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["get_price"] = $this->Catalog_model->search_group_price($config["per_page"], $page);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========
        $access_retailer=access_role_permission_page(83);
        $acc=access_role_retailer();
        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3){
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/catalog/group_price_list', $data);
                $this->load->view('c_level/footer');
                 }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }else{
             $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
        }
    }

    /*...........  END GROUP PRICE SECTION ..........*/


        // For delete main attribut option 
    function remove_main_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('op_id',$option_id);
        $this->remove_image_sub_option('option_id',$option_id);
        $this->remove_image_main_option('att_op_id',$option_id);
        // Remove image : END


        $this->db->where('op_id',$option_id)->delete('attr_options_option_option_tbl');
        $this->db->where('option_id',$option_id)->delete('attr_options_option_tbl');
        $this->db->where('att_op_id',$option_id)->delete('attr_options');

        return true;
    }

    function remove_sub_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('att_op_op_id',$option_id);
        $this->remove_image_sub_option('op_op_id',$option_id);
        // Remove image : END

        $this->db->where('att_op_op_id',$option_id)->delete('attr_options_option_option_tbl');
        $this->db->where('op_op_id',$option_id)->delete('attr_options_option_tbl');
        return true;
    }

    function remove_sub_sub_option(){
        $option_id = $this->input->post('option_id');

        // Remove image : START
        $this->remove_image_sub_sub_option('att_op_op_op_id',$option_id);
        // Remove image : END

        $this->db->where('att_op_op_op_id',$option_id)->delete('attr_options_option_option_tbl');
        return true;
    }


    function remove_image_main_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options')->result_array();
        foreach ($data as $key => $value) {
            if($value['attributes_images'] != ''){
                $this->remove_old_images($value['attributes_images']);
            }
        }
    }

    function remove_image_sub_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options_option_tbl')->result_array();
        foreach ($data as $key => $value) {
            if($value['att_op_op_images'] != ''){
                $this->remove_old_images($value['att_op_op_images']);
            }
        }
    }

    function remove_image_sub_sub_option($field,$option_id){
        $data = $this->db->where($field,$option_id)->get('attr_options_option_option_tbl')->result_array();
        foreach ($data as $key => $value) {
            if($value['att_op_op_op_images'] != ''){
                $this->remove_old_images($value['att_op_op_op_images']);
            }
        }
    }

    public function remove_old_images($path){
        if(is_file($path)){
            unlink($path);
        }
    }

    function retailer_upcharge_price()
    {
        $search = (object) array(
                    'attribute_name' => $this->input->post('attribute_name'),
                    'category_id' => $this->input->post('category_id'),
        );

        $data["get_attribute"] = $this->Catalog_model->get_retailer_upcharge_price_attribute($search);
        // $data["get_category"] = $this->Catalog_model->get_category();
        $data["get_category"] = $this->Catalog_model->get_all_category_level_wise();
        $data['search'] = $search;

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/catalog/retailer_upcharge_price_list', $data);
        $this->load->view('c_level/footer');
    }

    public function retailer_upcharge_price_save(){

        $retailer_price_type = $this->input->post('retailer_price_type');
        $retailer_price = $this->input->post('retailer_price');
        $hid_att_op_id = $this->input->post('hid_att_op_id');
        $hid_att_op_id = $this->input->post('hid_att_op_id');
        $hid_retailer_att_op_id = $this->input->post('hid_retailer_att_op_id');

        // For attr option add/edit : START
        $update_data = $insert_data = [];
        $u=$i=0;
        foreach ($hid_retailer_att_op_id as $k => $value) {
            if($hid_retailer_att_op_id[$k] != ''){
                // Update record  
                $update_data[$u]['retailer_price_type'] = $retailer_price_type[$k];
                $update_data[$u]['retailer_price'] = ($retailer_price[$k] != '') ? $retailer_price[$k]:NULL;
                $update_data[$u]['retailer_att_op_id'] = $hid_retailer_att_op_id[$k];
                $u++;
            }else{
                // Insert record
                $insert_data[$i]['att_op_id'] = $hid_att_op_id[$k];
                $insert_data[$i]['retailer_price_type'] = $retailer_price_type[$k];
                $insert_data[$i]['retailer_price'] = ($retailer_price[$k] != '') ? $retailer_price[$k]:NULL;
                $insert_data[$i]['created_by'] = $this->user_id;
                $i++;
            }
        }

        if(count($update_data) > 0){
            $this->db->update_batch('ret_attr_options',$update_data,'retailer_att_op_id');
        }

        if(count($insert_data) > 0){
            $this->db->insert_batch('ret_attr_options',$insert_data);
        }
        // For attr option add/edit : END

        $retailer_att_op_op_price_type = $this->input->post('retailer_att_op_op_price_type');
        $retailer_att_op_op_price = $this->input->post('retailer_att_op_op_price');
        $hid_op_op_id = $this->input->post('hid_op_op_id');
        $hid_retailer_op_op_id = $this->input->post('hid_retailer_op_op_id');

        // For attr option option add/edit : START
        $update_data = $insert_data = [];
        $u=$i=0;
        foreach ($hid_retailer_op_op_id as $k => $value) {
            if($hid_retailer_op_op_id[$k] != ''){
                // Update record  
                $update_data[$u]['retailer_att_op_op_price_type'] = $retailer_att_op_op_price_type[$k];
                $update_data[$u]['retailer_att_op_op_price'] = ($retailer_att_op_op_price[$k] != '') ? $retailer_att_op_op_price[$k]:NULL;
                $update_data[$u]['retailer_op_op_id'] = $hid_retailer_op_op_id[$k];
                $u++;
            }else{
                // Insert record
                $insert_data[$i]['op_op_id'] = $hid_op_op_id[$k];
                $insert_data[$i]['retailer_att_op_op_price_type'] = $retailer_att_op_op_price_type[$k];
                $insert_data[$i]['retailer_att_op_op_price'] = ($retailer_att_op_op_price[$k] != '') ? $retailer_att_op_op_price[$k]:NULL;
                $insert_data[$i]['created_by'] = $this->user_id;
                $i++;
            }
        }

        if(count($update_data) > 0){
            $this->db->update_batch('ret_attr_options_option_tbl',$update_data,'retailer_op_op_id');
        }

        if(count($insert_data) > 0){
            $this->db->insert_batch('ret_attr_options_option_tbl',$insert_data);
        }
        // For attr option option add/edit : END

        $retailer_att_op_op_op_price_type = $this->input->post('retailer_att_op_op_op_price_type');
        $retailer_att_op_op_op_price = $this->input->post('retailer_att_op_op_op_price');
        $hid_att_op_op_op_id = $this->input->post('hid_att_op_op_op_id');
        $hid_retailer_att_op_op_op_id = $this->input->post('hid_retailer_att_op_op_op_id');

        // For attr option option option  add/edit : START
        $update_data = $insert_data = [];
        $u=$i=0;
        foreach ($hid_retailer_att_op_op_op_id as $k => $value) {
            if($hid_retailer_att_op_op_op_id[$k] != ''){
                // Update record  
                $update_data[$u]['retailer_att_op_op_op_price_type'] = $retailer_att_op_op_op_price_type[$k];
                $update_data[$u]['retailer_att_op_op_op_price'] = ($retailer_att_op_op_op_price[$k] != '') ? $retailer_att_op_op_op_price[$k]:NULL;
                $update_data[$u]['retailer_att_op_op_op_id'] = $hid_retailer_att_op_op_op_id[$k];
                $u++;
            }else{
                // Insert record
                $insert_data[$i]['att_op_op_op_id'] = $hid_att_op_op_op_id[$k];
                $insert_data[$i]['retailer_att_op_op_op_price_type'] = $retailer_att_op_op_op_price_type[$k];
                $insert_data[$i]['retailer_att_op_op_op_price'] = ($retailer_att_op_op_op_price[$k] != '') ? $retailer_att_op_op_op_price[$k]:NULL;
                $insert_data[$i]['created_by'] = $this->user_id;
                $i++;
            }
        }

        if(count($update_data) > 0){
            $this->db->update_batch('ret_attr_options_option_option_tbl',$update_data,'retailer_att_op_op_op_id');
        }

        if(count($insert_data) > 0){
            $this->db->insert_batch('ret_attr_options_option_option_tbl',$insert_data);
        }
        // For attr option option option add/edit : END

        $retailer_att_op_op_op_op_price_type = $this->input->post('retailer_att_op_op_op_op_price_type');
        $retailer_att_op_op_op_op_price = $this->input->post('retailer_att_op_op_op_op_price');
        $hid_att_op_op_op_op_id = $this->input->post('hid_att_op_op_op_op_id');
        $hid_retailer_att_op_op_op_op_id = $this->input->post('hid_retailer_att_op_op_op_op_id');

        // For attr option option option  add/edit : START
        $update_data = $insert_data = [];
        $u=$i=0;
        foreach ($hid_retailer_att_op_op_op_op_id as $k => $value) {
            if($hid_retailer_att_op_op_op_op_id[$k] != ''){
                // Update record  
                $update_data[$u]['retailer_att_op_op_op_op_price_type'] = $retailer_att_op_op_op_op_price_type[$k];
                $update_data[$u]['retailer_att_op_op_op_op_price'] = ($retailer_att_op_op_op_op_price[$k] != '') ? $retailer_att_op_op_op_op_price[$k]:NULL;
                $update_data[$u]['retailer_att_op_op_op_op_id'] = $hid_retailer_att_op_op_op_op_id[$k];
                $u++;
            }else{
                // Insert record
                $insert_data[$i]['att_op_op_op_op_id'] = $hid_att_op_op_op_op_id[$k];
                $insert_data[$i]['retailer_att_op_op_op_op_price_type'] = $retailer_att_op_op_op_op_price_type[$k];
                $insert_data[$i]['retailer_att_op_op_op_op_price'] = ($retailer_att_op_op_op_op_price[$k] != '') ? $retailer_att_op_op_op_op_price[$k]:NULL;
                $insert_data[$i]['created_by'] = $this->user_id;
                $i++;
            }
        }

        if(count($update_data) > 0){
            $this->db->update_batch('ret_attr_op_op_op_op_tbl',$update_data,'retailer_att_op_op_op_op_id');
        }

        if(count($insert_data) > 0){
            $this->db->insert_batch('ret_attr_op_op_op_op_tbl',$insert_data);
        }
        // For attr option option option add/edit : END

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Upcharges updated successfully.</div>");
        redirect("retailer-upcharge-price");

    }


}
