<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Row_materials extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'c') {
            redirect('retailer-logout');
        }
        $admin_created_by = $this->session->userdata('admin_created_by');
        $this->user_id = $this->session->userdata('user_id');

        $this->load->model(array(
            'c_level/pattern_model', 'c_level/RowMaterial_model', 'c_level/Setting_model', 'c_level/purchase_model'
        ));
    }

    public function index() {
        
    }

//    ========= its for raw_material_usage ============
    public function raw_material_usage() {
        $data['rmtts'] = $this->purchase_model->get_row_material_list();

        $data['colors'] = $this->db->where('created_by',$this->level_id)->get('color_tbl')->result();
        $data['patern_model'] = $this->pattern_model->get_patern_model();
        $data['get_b_level_quatation'] = $this->db->select('*')->from('b_level_quatation_tbl')->where('created_by',$this->level_id)->where('order_stage', 4)->get()->result();
        $data['get_quatation'] = $this->db->select('*')->from('quatation_tbl')
            ->where('created_by',$this->level_id)
            ->where('order_stage', 4)->get()->result();
   // if($this->session->userdata('package_id')==3 ){

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/row_material/raw_material_usage');
        $this->load->view('c_level/footer');
    //        }else{
    //     $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
    //       $this->load->view('c_level/header');
    //       $this->load->view('c_level/sidebar');
    //       $this->load->view('c_level/upgrade_error');
    //       $this->load->view('c_level/footer');
    // }
    }

    //    =============== its for raw_material_usage_save ===============
    public function raw_material_usage_save() {
    //echo '<pre>';print_r($_POST);exit;
       $item_name_order = $this->input->post('item_name_order');
        $rmtt_id = $this->input->post('rmtt_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $quantity = $this->input->post('quantity');
         $order_id = $this->input->post('order_id');
        $date = $this->input->post('date');
       foreach($item_name_order as $key=>$val)
       {
            foreach($val as $key1=>$val1)
            {
                //print_r($val1);exit;
                foreach($rmtt_id[$key] as $k=>$v){
                $row_id                     =  $val1;
                $rmtt_id1                    =  $rmtt_id[$key][$k];
                $color_id1                  =   $color_id[$key][$k];
                $pattern_model_id1          =$pattern_model_id[$key][$k];
                $quantity1                  =$quantity[$key][$k];
                
                $raw_materials_usage = array(
                'row_material_id' => $rmtt_id1,
                'row_id' => $row_id,
                'color_id' => $color_id1,
                'pattern_model_id' => $pattern_model_id1,
                'out_qty' => $quantity1,
                'from_module' => $order_id,
                'stock_date' => $date,
                );
                //print_r($raw_materials_usage);exit;
                $this->db->insert('row_material_stock_tbl', $raw_materials_usage);
                }
                //print_r($$raw_materials_usage);exit;
            }
            
       }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Raw materials usages information save successfully!</div>");
        redirect('c-raw-material-usage');
    }

    public function get_item_details(){
		$order_id=$this->input->post("item_order_id");
		$get_order_itemwise = $this->db->query("SELECT * FROM b_level_qutation_details WHERE order_id='".$order_id."'")->result();
		$html='<option value="">Sleect Item</option>';
		if(!empty($get_order_itemwise))
		{
			foreach($get_order_itemwise as $row)
			{
				$get_product_name= $this->db->query("SELECT * FROM product_tbl WHERE product_id='".$row->product_id."'")->row();
				$html.='<option value="'.$row->row_id.'">'.$get_product_name->product_name.'</option>';
			}
		}
		echo $html;exit;
	}
	
	public function get_item_dimension(){
		$item_raw_id=$this->input->post("item_raw_id");
		$get_order_itemwise = $this->db->query("SELECT * FROM b_level_qutation_details WHERE row_id=".$item_raw_id)->row();
		if(!empty($get_order_itemwise))
		{
			$get_width=$get_order_itemwise->width;
			$get_height=$get_order_itemwise->height;
			$get_total=$get_order_itemwise->width*$get_order_itemwise->height;
			
		}
			$result=array('get_width'=>$get_width,'get_height'=>$get_height,'get_total'=>$get_total);
			echo json_encode($result);exit;
	}

    //    ============ its for quantity_check ===========
    public function quantity_check() {
        $material_id = $this->input->post('material_id');
        $color_id = $this->input->post('color_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $quantity = $this->input->post('quantity');
        $get_quantity_check = $this->RowMaterial_model->get_quantity_check($material_id, $color_id, $pattern_model_id);
    //        print_r($get_quantity_check);
        if ($quantity > $get_quantity_check->total_in_qty) {
            echo "Quantity is greater than In Quantity";
        }
        if ($get_quantity_check->total_out_qty > $get_quantity_check->total_in_qty) {
            echo "Quantity is not available";
        }
    }


	
}
