<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Make_payment extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $session_id = $this->session->userdata('session_id');
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('email_sender');
        $this->load->model('b_level/settings');
        $this->load->model('c_level/setting_model', 'c_settings');
        $this->load->library('paypal_lib');
        $this->load->model('c_level/Order_model');
    }

    public function payment_to_b($wholesaler_order_id) {

      //  $this->permission_c->check_label('payment_to_b')->create()->redirect();

        // Get c level order id based on wholesaler order id : START
        $b_order = $this->db->where('order_id', $wholesaler_order_id)->where('created_by',$this->session->userdata('user_id'))->limit('1')->get('b_level_quatation_tbl')->row();
        // Get c level order id based on wholesaler order id : END

        if(isset($b_order->clevel_order_id) && $b_order->clevel_order_id != ''){

            $order_id = $b_order->clevel_order_id;

            $data['cmp_info'] = $this->c_settings->company_profile();

            $data['cusstomer'] = $this->db->where('customer_user_id', $data['cmp_info'][0]->user_id)->get('customer_info')->row();

            $data['orderd'] = $this->get_orderd_by_id($order_id);

            $data['order_details'] = $this->get_orderd_details_by_id($data['orderd']->order_id);
        
            $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
                        ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
                        ->where('order_id', $order_id)->get('shipment_data')->row();


            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/orders/payment_to_b', $data);
            $this->load->view('c_level/footer');
        }else{
            // Call 404 error page.
        }    
    }

    public function multiple_payment_to_b() {
        // $this->permission_c->check_label('payment_to_b')->create()->redirect();

        $multiple_order_ids = $_REQUEST['payment_multiple'];

        if (!$multiple_order_ids) {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Payment was not successfull </div>");
            redirect('/my-orders');
        }   

        $data['cmp_info'] = $this->c_settings->company_profile();

        $data['cusstomer'] = $this->db->where('customer_user_id', $data['cmp_info'][0]->user_id)->get('customer_info')->row();

        $orderd = $this->get_multiple_orderd_by_id($multiple_order_ids);
        
        $grand_price = 0;
        $order_id_ary = array();
        foreach ($orderd as $key => $ord) {
            if($ord->paid_amount == 0){
                $g_price = $ord->grand_total / 2;
            }else{
                $g_price = $ord->due;
            }
            $grand_price = $grand_price + $g_price;
            $order_id_ary[] = $ord->order_id;
        }

        $orders_details = (object)array(
                'order_ids' => $_POST['payment_multiple'],
                'paid_amount' => $grand_price,
        );

        $data['orderd'] = $orders_details;

        $data['order_details'] = $orderd;

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/multiple_payment_to_b', $data);
        $this->load->view('c_level/footer');
    }


    public function order_payment_update() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "updated";
        $remarks = "order payment information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $cusstomer_info = $this->db->where('customer_user_id', $this->user_id)->get('customer_info')->row();

        $customer_id = $cusstomer_info->customer_id;


        $payment_method = $this->input->post('payment_method');

        $order_id = $this->input->post('order_id');

        $clevel_order_id = $this->input->post('clevel_order_id');

        $cHead = $this->db->select('HeadCode')->where('HeadName', $cusstomer_info->customer_no)->get('b_acc_coa')->row();

        if (@$cHead->HeadCode != NULL) {

            if ($payment_method == 'paypal') {

                $orderData = array(
                    'order_id' => $clevel_order_id,
                    'customer_id' => $customer_id,
                    'paid_amount' => $this->input->post('paid_amount'),
                    'status' => 1
                );

                $orderDataPaypal = array(
                    'order_id' => $clevel_order_id,
                    'b_order_id' => $order_id,
                    'headcode' => $cHead->HeadCode,
                    'grand_total' => $this->input->post('grand_total'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'payment_method' => $this->input->post('payment_method'),
                    'card_number' => $this->input->post('card_number'),
                    'issuer' => $this->input->post('issuer'),
                    'customer_id' => $this->input->post('customer_id')
                );

                $this->session->set_userdata($orderDataPaypal);

                $pyament = $this->payment_by_paypal($orderData);
            } else {

                $due = $this->input->post('due');

                if ($due > 0) {
                    $order_stage = 3;
                } else {
                    $order_stage = 2;
                }


                $rowData = $this->db->select('due,paid_amount,level_id')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
                
                // Get Wholesaler user info : START
                $b_level_info = $rowData->level_id;
                $wholesaler_user_info = $this->db->select('*')->where('id', $b_level_info)->get('user_info')->row();
                // Get Wholesaler user info : END


                // quatation table update
                $orderData = array(
                    'paid_amount' => @$rowData->paid_amount + $this->input->post('paid_amount'),
                    'due' => $this->input->post('due'),
                    'order_stage' => $order_stage,
                    'status' => 1
                );

                //update to quatation table with pyament due 
                $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
                //-----------------------------------------
                // quatation table update
                $payment_tbl = array(
                    'quotation_id' => $order_id,
                    'payment_method' => $this->input->post('payment_method'),
                    'paid_amount' => $this->input->post('paid_amount'),
                    'description' => "Payment given for Order#" . $order_id,
                    'payment_date' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('user_id'),
                    'create_date' => date('Y-m-d')
                );


                if ($this->db->insert('payment_tbl', $payment_tbl)) {

                    $payment_id = $this->db->insert_id();


                    if ($payment_method == 'check') {


                        if (@$_FILES['check_image']['name']) {

                            $config['upload_path'] = './assets/c_level/uploads/check_img/';
                            $config['allowed_types'] = 'jpeg|jpg';
                            $config['overwrite'] = false;
                            $config['max_size'] = 3000;
                            $config['remove_spaces'] = true;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('check_image')) {
                                $error = $this->upload->display_errors();
                                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                redirect("new-order");
                            } else {

                                $data = $this->upload->data();
                                $check_image = $config['upload_path'] . $data['file_name'];
                            }
                        } else {

                            @$check_image = '';
                        }

                        $payment_check_tbl = array(
                            'payment_id' => $payment_id,
                            'check_number' => $this->input->post('check_number'),
                            'check_image' => $check_image
                        );

                        $this->db->insert('payment_check_tbl', $payment_check_tbl);
                    }
                }


                $voucher_no = $order_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $this->input->post('paid_amount');
                $cAID = $cHead->HeadCode;
                $IsPosted = 1;
                $CreateBy = $this->session->userdata('user_id');
                $createdate = date('Y-m-d H:i:s');


                $payment_method = $this->input->post('payment_method');

                if ($payment_method == 'cash') {
                    $COAID = '1020101';
                }if ($payment_method == 'check') {
                    $COAID = '102010202';
                }if ($payment_method == 'card') {
                    $COAID = '1020103';
                }

                // C level transection start
                //C level credit insert acc_transaction
                $customerCredit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'level_id'=>$this->level_id,
                    'COAID' => $COAID,
                    'Narration' => "Paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);
                //------------------------------------
                //B level debit insert acc_transaction

                $bCOA = '502020101';
                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $bCOA,
                    'level_id'=>$this->level_id,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                ); //b_acc_transaction

                $this->db->insert('acc_transaction', $b_levelDebit);
                // C level transection END
                //. B level transection start
                //customer credit insert b_acc_transaction
                $customerCredit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $cAID,
                    'level_id'=>$this->level_id,
                    'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('b_acc_transaction', $customerCredit);
                //------------------------------------
                //b_level debit insert b_acc_transaction                    

                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'level_id'=>$this->level_id,
                    'Credit' => 0,
                    'COAID' => $COAID,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                ); //b_acc_transaction

                $this->db->insert('b_acc_transaction', $b_levelDebit);
                //- B level transection END----------------------
                // C level notification

                $cNotificationData = array(
                    'notification_text' => 'Payment has been submited for ' . $clevel_order_id,
                    'go_to_url' => 'retailer-invoice-receipt/' . $clevel_order_id,
                    'created_by' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d')
                );

                $this->db->insert('c_notification_tbl', $cNotificationData);

                //-------------------------
                //

                $bNotificationData = array(
                    'b_user_id'         => $this->session->userdata('main_b_id'),
                    'notification_text' => 'Payment has been received for ' . $order_id,
                    // 'go_to_url' => 'wholesaler-retailer-receipt/' . $order_id,
                    'go_to_url' => 'wholesaler-invoice-receipt/' . $order_id,
                    'created_by' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d')
                );
                $this->db->insert('b_notification_tbl', $bNotificationData);

                //--------------------------

                $this->email_sender->send_email(
                        $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject' => 'Order payment'
                        )
                );

                $this->Order_model->smsSend(
                    $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                    'subject' => 'Order payment'
                        )
                );


                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>The deposit paid to ".$wholesaler_user_info->company."</div>");
                // redirect('retailer-make-payment-wholesaler/' . $clevel_order_id);
                redirect('retailer-make-payment-wholesaler/' . $order_id);
                
            }


        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
            // redirect('retailer-make-payment-wholesaler/' . $clevel_order_id);
            redirect('retailer-make-payment-wholesaler/' . $order_id);
        }
    }


    public function multiple_order_payment_update() {


        if($this->input->post())
        {
            $order_ids_string = $this->input->post('order_id');
            $order_ids_url_string = str_replace(",","_",$order_ids_string);
            $order_ids_array = explode(',', $order_ids_string);
            if($order_ids_array)
            {   
                $grand_price = 0;
                $clevel_order_id_array = array();
                foreach ($order_ids_array as $key => $ord_id) 
                {
                    $order_info = $this->get_orderd_by_id($ord_id);

                    $paid_amount = 0;
                    if($order_info->paid_amount == 0){
                        $paid_amount = $order_info->grand_total / 2;
                    }else{
                        $paid_amount = $order_info->due;
                    }

                    if($order_info->paid_amount == 0){
                        $g_price = $order_info->grand_total / 2;
                    }else{
                        $g_price = $order_info->due;
                    }
                    $grand_price = $grand_price + $g_price;


                    $action_page = $this->uri->segment(2);
                    
                    $action_done = "updated";
                    $remarks = "order payment information updated";
                    $accesslog_info = array(
                        'action_page' => $action_page,
                        'action_done' => $action_done,
                        'remarks' => $remarks,
                        'user_name' => $this->user_id,
                        'level_id' => $this->level_id,
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'entry_date' => date("Y-m-d H:i:s"),
                    );

                    $this->db->insert('accesslog', $accesslog_info);
                    
                    $cusstomer_info = $this->db->where('customer_user_id', $this->user_id)->get('customer_info')->row();

                    $customer_id = $order_info->customer_id;


                    $payment_method = $this->input->post('payment_method');

                    $order_id = $order_info->order_id;

                    $clevel_order_id = $ord_id;

                    $cHead = $this->db->select('HeadCode')->where('HeadName', $order_info->customer_no)->get('b_acc_coa')->row();
                   
                    if (@$cHead->HeadCode != NULL) {

                        $due = $order_info->due - $paid_amount;

                        if ($payment_method == 'paypal') {

                            $orderDataPaypal[] = array(
                                'order_id' => $order_id,
                                'b_order_id' => $clevel_order_id,
                                'headcode' => $cHead->HeadCode,
                                'grand_total' => $order_info->grand_total,
                                'paid_amount' => $paid_amount,
                                'due' => $due,
                                'payment_method' => $this->input->post('payment_method'),
                                'card_number' => $this->input->post('card_number'),
                                'issuer' => $this->input->post('issuer'),
                                'customer_id' => $order_info->customer_id
                            );
                        } else {


                            if ($due > 0) {
                                $order_stage = 3;
                            } else {
                                $order_stage = 2;
                            }


                            $rowData = $this->db->select('due,paid_amount')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
                            // quatation table update
                            $orderData = array(
                                'paid_amount' => @$rowData->paid_amount + $paid_amount,
                                'due' => $due,
                                'order_stage' => $order_stage,
                                'status' => 1
                            );

                            //update to quatation table with pyament due 
                            $this->db->where('order_id', $order_id)->update('b_level_quatation_tbl', $orderData);
                            //-----------------------------------------
                            // quatation table update
                            $payment_tbl = array(
                                'quotation_id' => $order_id,
                                'payment_method' => $this->input->post('payment_method'),
                                'paid_amount' => $paid_amount,
                                'description' => "Payment given for Order#" . $order_id,
                                'payment_date' => date('Y-m-d'),
                                'created_by' => $this->session->userdata('user_id'),
                                'create_date' => date('Y-m-d')
                            );

                            if ($this->db->insert('payment_tbl', $payment_tbl)) {

                                $payment_id = $this->db->insert_id();


                                if ($payment_method == 'check') {


                                    if (@$_FILES['check_image']['name']) {

                                        $config['upload_path'] = './assets/c_level/uploads/check_img/';
                                        $config['allowed_types'] = 'jpeg|jpg';
                                        $config['overwrite'] = false;
                                        $config['max_size'] = 3000;
                                        $config['remove_spaces'] = true;

                                        $this->load->library('upload', $config);
                                        $this->upload->initialize($config);

                                        if (!$this->upload->do_upload('check_image')) {
                                            $error = $this->upload->display_errors();
                                            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                                            redirect("new-order");
                                        } else {

                                            $data = $this->upload->data();
                                            $check_image = $config['upload_path'] . $data['file_name'];
                                        }
                                    } else {

                                        @$check_image = '';
                                    }

                                    $payment_check_tbl = array(
                                        'payment_id' => $payment_id,
                                        'check_number' => $this->input->post('check_number'),
                                        'check_image' => $check_image
                                    );

                                    $this->db->insert('payment_check_tbl', $payment_check_tbl);
                                }
                            }


                            $voucher_no = $order_id;
                            $Vtype = "INV";
                            $VDate = date('Y-m-d');
                            $paid_amount = $paid_amount;
                            $cAID = $cHead->HeadCode;
                            $IsPosted = 1;
                            $CreateBy = $this->session->userdata('user_id');
                            $createdate = date('Y-m-d H:i:s');


                            $payment_method = $this->input->post('payment_method');

                            if ($payment_method == 'cash') {
                                $COAID = '1020101';
                            }if ($payment_method == 'check') {
                                $COAID = '102010202';
                            }if ($payment_method == 'card') {
                                $COAID = '1020103';
                            }

                            // C level transection start
                            //C level credit insert acc_transaction
                            $customerCredit = array(
                                'VNo' => $voucher_no,
                                'Vtype' => $Vtype,
                                'VDate' => $VDate,
                                'Debit' => 0,
                                'Credit' => $paid_amount,
                                'level_id'=>$this->level_id,
                                'COAID' => $COAID,
                                'Narration' => "Paid for invoice #" . $voucher_no,
                                'IsPosted' => $IsPosted,
                                'CreateBy' => $CreateBy,
                                'CreateDate' => $createdate,
                                'IsAppove' => 1
                            );

                            $this->db->insert('acc_transaction', $customerCredit);
                            //------------------------------------
                            //B level debit insert acc_transaction

                            $bCOA = '502020101';
                            $b_levelDebit = array(
                                'VNo' => $voucher_no,
                                'Vtype' => $Vtype,
                                'VDate' => $VDate,
                                'Debit' => $paid_amount,
                                'Credit' => 0,
                                'COAID' => $bCOA,
                                'level_id'=>$this->level_id,
                                'Narration' => "Amount received for invoice #" . $voucher_no,
                                'IsPosted' => $IsPosted,
                                'CreateBy' => $CreateBy,
                                'CreateDate' => $createdate,
                                'IsAppove' => 1
                            ); //b_acc_transaction

                            $this->db->insert('acc_transaction', $b_levelDebit);
                            // C level transection END
                            //. B level transection start
                            //customer credit insert b_acc_transaction
                            $customerCredit = array(
                                'VNo' => $voucher_no,
                                'Vtype' => $Vtype,
                                'VDate' => $VDate,
                                'Debit' => 0,
                                'Credit' => $paid_amount,
                                'COAID' => $cAID,
                                'level_id'=>$this->level_id,
                                'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                                'IsPosted' => $IsPosted,
                                'CreateBy' => $CreateBy,
                                'CreateDate' => $createdate,
                                'IsAppove' => 1
                            );

                            $this->db->insert('b_acc_transaction', $customerCredit);
                            //------------------------------------
                            //b_level debit insert b_acc_transaction                    

                            $b_levelDebit = array(
                                'VNo' => $voucher_no,
                                'Vtype' => $Vtype,
                                'VDate' => $VDate,
                                'Debit' => $paid_amount,
                                'level_id'=>$this->level_id,
                                'Credit' => 0,
                                'COAID' => $COAID,
                                'Narration' => "Amount received for invoice #" . $voucher_no,
                                'IsPosted' => $IsPosted,
                                'CreateBy' => $CreateBy,
                                'CreateDate' => $createdate,
                                'IsAppove' => 1
                            ); //b_acc_transaction

                            $this->db->insert('b_acc_transaction', $b_levelDebit);
                            //- B level transection END----------------------
                            // C level notification

                            $cNotificationData = array(
                                'notification_text' => 'Payment has been submited for ' . $clevel_order_id,
                                'go_to_url' => 'retailer-invoice-receipt/' . $clevel_order_id,
                                'created_by' => $this->session->userdata('user_id'),
                                'date' => date('Y-m-d')
                            );

                            $this->db->insert('c_notification_tbl', $cNotificationData);

                            //-------------------------
                            //

                            $bNotificationData = array(
                                'notification_text' => 'Payment has been received for ' . $order_id,
                                'go_to_url' => 'wholesaler-retailer-receipt/' . $order_id,
                                'created_by' => $this->session->userdata('user_id'),
                                'date' => date('Y-m-d')
                            );
                            $this->db->insert('b_notification_tbl', $bNotificationData);

                            //--------------------------

                            $this->email_sender->send_email(
                                    $data = array(
                                'customer_id' => $customer_id,
                                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                                'subject' => 'Order payment'
                                    )
                            );

                            $this->Order_model->smsSend(
                                $data = array(
                                'customer_id' => $customer_id,
                                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount: '.$paid_amount.', Payment method:'.$payment_method,
                                'subject' => 'Order payment'
                                    )
                            );
                        }

                    } else {

                        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer account code not found! </div>");
                        redirect('retailer-multiple-payment-wholesaler/' . $clevel_order_id);
                    }
                }

                    if ($payment_method == 'paypal') {
                        $orderData = array(
                            'order_id' => $order_ids_url_string,
                            'customer_id' => $customer_id,
                            'paid_amount' => $grand_price,
                            'status' => 1
                        );
                        $this->session->set_userdata('bulk_order',$orderDataPaypal);
                        $pyament = $this->bulk_payment_by_paypal($orderData);
                        exit;
                    }
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");
                redirect('retailer-multiple-payment-wholesaler/' . $clevel_order_id);

            }
        }
    }






    // make payment by paypal

    public function payment_by_paypal($orderData) {



        $customer_id = $orderData['customer_id'];
        $order_id = $orderData['order_id'];

        //$discount = 15; //number_format((!empty($history->discount) ? $history->discount : 0), 2);
        //$item_name = "Order :: Test";
        // ---------------------
        //Set variables for paypal form
        $returnURL = base_url("retailer-make-payment-success/$order_id/$customer_id"); //payment success url
        $cancelURL = base_url("retailer-make-payment-cancel/$order_id/$customer_id"); //payment cancel url
        $notifyURL = base_url('retailer-make-payment-ipn'); //ipn url
        //set session token
        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));

        $paypal = $this->db->select('*')
                ->from('gateway_tbl')
                ->where('default_status', 1)
                ->where('level_type', 'b')
                ->get()
                ->row();

        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';


        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);

        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $orderData['paid_amount']);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);
        // additional information 
        $this->paypal_lib->add_field('custom', $order_id);

        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();
    }

    public function success($order_id = null, $customer_id = null) {
        $this->order_save_update();
    }

    public function cancel($order_id = null, $customer_id = null) {

        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('headcode');
        $this->session->unset_userdata('grand_total');
        $this->session->unset_userdata('paid_amount');
        $this->session->unset_userdata('due');
        $this->session->unset_userdata('payment_method');
        $this->session->unset_userdata('card_number');
        $this->session->unset_userdata('issuer');
        $this->session->unset_userdata('b_order_id');

        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Payment was not successfull </div>");

        redirect('retailer-multiple-payment-wholesaler/' . $order_id);
    }

    /*
     * Add this ipn url to your paypal account
     * Profile and Settings > My selling tools > 
     * Instant Payment Notification (IPN) > update 
     * Notification URL: (eg:- http://domain.com/website/paypal/ipn/)
     * Receive IPN messages (Enabled) 
     */

    public function ipn() {

        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;

        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {
            //insert the transaction data into the database
            $this->load->model('Paypal_model');

            $this->Paypal_model->insertTransaction($data);
        }

        return true;
    }

    //Send Customer Email with invoice
    public function setmail($email, $file_path, $id = null, $name = null) {

        $subject = 'ticket Information';
        $message = "Congratulation Mr. " . ' ' . $name . "Your Purchase Order No-  " . '-' . $id;
        
        $config = $this->Common_model->mailConfig($this->session->userdata('user_id'));

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['smtp_user']);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach($file_path);

        $check_email = $this->test_input($email);

        if (filter_var($check_email, FILTER_VALIDATE_EMAIL)) {

            if ($this->email->send()) {
                $this->session->set_flashdata(array('message' => "Email Sent Sucessfully"));
                return true;
            } else {
                $this->session->set_flashdata(array('exception' => "Please configure your mail."));
                return false;
            }
        } else {
            $this->session->set_userdata(array('message' => "Your Data Successfully Saved"));
        }
    }

    public function order_save_update() {
//        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "updated";
        $remarks = "order information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $order_id = $this->session->userdata('order_id');
        $b_order_id = $this->session->userdata('b_order_id');
        $customer_id = $this->session->userdata('customer_id');
        $payment_method = $this->session->userdata('payment_method');

        if (!empty($order_id)) {


            $due = $this->session->userdata('due');
            if ($due > 0) {
                $order_stage = 3;
            } else {
                $order_stage = 2;
            }

            $rowData = $this->db->select('due,paid_amount')->where('order_id', $b_order_id)->get('b_level_quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'paid_amount' => @$rowData->paid_amount + $this->session->userdata('paid_amount'),
                'due' => $this->session->userdata('due'),
                'order_stage' => $order_stage,
                'status' => 1
            );

            //update to quatation table with pyament due 
            $this->db->where('order_id', $b_order_id)->update('b_level_quatation_tbl', $orderData);
            // quatation table update
            $payment_tbl = array(
                'quotation_id' => $b_order_id,
                'payment_method' => $this->session->userdata('payment_method'),
                'paid_amount' => $this->session->userdata('paid_amount'),
                'description' => "Payment given for Order#" . $order_id,
                'payment_date' => date('Y-m-d'),
                'created_by' => $this->session->userdata('user_id'),
                'create_date' => date('Y-m-d')
            );
            $this->db->insert('payment_tbl', $payment_tbl);


            $voucher_no = $b_order_id;
            $Vtype = "INV";
            $VDate = date('Y-m-d');
            $paid_amount = $this->session->userdata('paid_amount');
            $cAID = $this->session->userdata('headcode');
            $IsPosted = 1;
            $CreateBy = $this->session->userdata('user_id');
            $createdate = date('Y-m-d H:i:s');
            $Paypal_COAID = '1020104';
            //C level transaction Start
            $customerCredit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => 0,
                'Credit' => $paid_amount,
                'COAID' => $Paypal_COAID,
                'level_id'=>$this->level_id,
                'Narration' => "Paid for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'level_id'=>$this->level_id,
                'Credit' => 0,
                'COAID' => $bCOA,
                'Narration' => "Amount received for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('acc_transaction', $b_levelDebit);
            // C level transaction END
            // B level transaction start
            // 
            $customerCredit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => 0,
                'Credit' => $paid_amount,
                'COAID' => $cAID,
                'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'level_id'=>$this->level_id,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('b_acc_transaction', $customerCredit);

            //------------------------------------

            $bCOA = '502020101';

            $b_levelDebit = array(
                'VNo' => $voucher_no,
                'Vtype' => $Vtype,
                'VDate' => $VDate,
                'Debit' => $paid_amount,
                'Credit' => 0,
                'COAID' => $Paypal_COAID,
                'level_id'=>$this->level_id,
                'Narration' => "Amount received for invoice #" . $voucher_no,
                'IsPosted' => $IsPosted,
                'CreateBy' => $CreateBy,
                'CreateDate' => $createdate,
                'IsAppove' => 1
            );

            $this->db->insert('b_acc_transaction', $b_levelDebit);
            // B level transaction End 

            $this->session->unset_userdata('order_id');
            $this->session->unset_userdata('headcode');
            $this->session->unset_userdata('grand_total');
            $this->session->unset_userdata('paid_amount');
            $this->session->unset_userdata('due');
            $this->session->unset_userdata('payment_method');
            $this->session->unset_userdata('card_number');
            $this->session->unset_userdata('issuer');
            $this->session->unset_userdata('b_order_id');
            $this->session->unset_userdata('customer_id');

            // C level notification
            $cNotificationData = array(
                'notification_text' => 'Payment has been done for ' . $order_id,
                'go_to_url' => 'retailer-invoice-receipt/' . $order_id,
                'created_by' => $this->session->userdata('user_id'),
                'date' => date('Y-m-d')
            );

            $this->db->insert('c_notification_tbl', $cNotificationData);
            //-------------------------
            //


            $bNotificationData = array(
                'notification_text' => 'Payment has been received for ' . $b_order_id,
                'go_to_url' => 'wholesaler-retailer-receipt/' . $b_order_id,
                'created_by' => $this->session->userdata('user_id'),
                'date' => date('Y-m-d')
            );
            $this->db->insert('b_notification_tbl', $bNotificationData);

            //--------------------------


            $data = array(
                'customer_id' => $customer_id,
                'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount is '.$paid_amount.', Payment method is'.$payment_method,
                'subject' => 'Order payment'
            );


            //--------------------------
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");

            // redirect('retailer-make-payment-wholesaler/' . $order_id);
            redirect('retailer-make-payment-wholesaler/' . $b_order_id);
        }
    }

    public function get_orderd_by_id($order_id) {

        $query = $this->db->select("b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
                        ->from('b_level_quatation_tbl')
                        ->join('customer_info', 'customer_info.customer_id=b_level_quatation_tbl.customer_id', 'left')
                        ->where('b_level_quatation_tbl.clevel_order_id', $order_id)
                        ->get()->row();
        return $query;
    }

    public function get_multiple_orderd_by_id($order_id) {
        $order_id_array = [];
        if($order_id){
            $order_id_array = explode(',',$order_id);
        }
        

        $query = $this->db->select("b_level_quatation_tbl.*,
            CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name,
            customer_info.phone,
            customer_info.address,
            customer_info.city,
            customer_info.state,
            customer_info.zip_code,
            customer_info.country_code,
            customer_info.customer_no,
            customer_info.customer_id")
                        ->from('b_level_quatation_tbl')
                        ->join('customer_info', 'customer_info.customer_id=b_level_quatation_tbl.customer_id', 'left')
                        ->where_in('b_level_quatation_tbl.clevel_order_id', $order_id_array)
                        ->get()->result();
        return $query;
    }

    public function get_orderd_details_by_id($order_id) {

        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
                        ->from('b_level_qutation_details')
                        ->join('product_tbl', 'product_tbl.product_id=b_level_qutation_details.product_id', 'left')
                        ->join('quatation_attributes', 'quatation_attributes.fk_od_id=b_level_qutation_details.row_id', 'left')
                        ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id', 'left')
                        ->join('color_tbl', 'color_tbl.id=b_level_qutation_details.color_id', 'left')
                        ->where('b_level_qutation_details.order_id', $order_id)
                        ->get()->result();


        return $query;
    }

     public function get_multiple_orderd_details_by_id($order_ids) {
        $query = $this->db->select("b_level_qutation_details.*,
            product_tbl.product_name,
            quatation_attributes.product_attribute,
            pattern_model_tbl.pattern_name,
            color_tbl.color_name,
            color_tbl.color_number")
                        ->from('b_level_qutation_details')
                        ->join('product_tbl', 'product_tbl.product_id=b_level_qutation_details.product_id', 'left')
                        ->join('quatation_attributes', 'quatation_attributes.fk_od_id=b_level_qutation_details.row_id', 'left')
                        ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=b_level_qutation_details.pattern_model_id', 'left')
                        ->join('color_tbl', 'color_tbl.id=b_level_qutation_details.color_id', 'left')
                        ->where_in('b_level_qutation_details.order_id', $order_ids)
                        ->get()->result();


        return $query;
    }






      // make payment by paypal

    public function bulk_payment_by_paypal($orderData) {

        $customer_id = $orderData['customer_id'];
        $order_id = $orderData['order_id'];

        //$discount = 15; //number_format((!empty($history->discount) ? $history->discount : 0), 2);
        //$item_name = "Order :: Test";
        // ---------------------
        //Set variables for paypal form
        $returnURL = base_url("retailer-make-payment-bulk-success/$order_id/$customer_id"); //payment success url
        $cancelURL = base_url("retailer-make-payment-bulk-cancel/$order_id/$customer_id"); //payment cancel url
        $notifyURL = base_url('retailer-make-payment-bulk-ipn'); //ipn url
        //set session token
        $this->session->unset_userdata('_tran_token');
        $this->session->set_userdata(array('_tran_token' => $order_id));

        $paypal = $this->db->select('*')
                ->from('gateway_tbl')
                ->where('default_status', 1)
                ->where('level_type', 'b')
                ->get()
                ->row();

        $paypal_lib_currency_code = (!empty($paypal->currency) ? $paypal->currency : 'USD');
        $paypal_lib_ipn_log_file = BASEPATH . 'logs/paypal_ipn.log';
        $paypal_lib_ipn_log = TRUE;
        $paypal_lib_button_path = 'buttons';


        // my customization
        $this->paypal_lib->add_field('mode', $paypal->status);
        $this->paypal_lib->add_field('business', $paypal->payment_mail);
        $this->paypal_lib->add_field('paypal_lib_currency_code', $paypal_lib_currency_code);
        $this->paypal_lib->add_field('paypal_lib_ipn_log_file', $paypal_lib_ipn_log_file);
        $this->paypal_lib->add_field('paypal_lib_ipn_log', $paypal_lib_ipn_log);
        $this->paypal_lib->add_field('paypal_lib_button_path', $paypal_lib_button_path);
        //-----------------------
        // set form auto fill data
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);

        // item information
        $this->paypal_lib->add_field('item_number', $order_id);
        //$this->paypal_lib->add_field('item_name', $item_name);
        $this->paypal_lib->add_field('amount', $orderData['paid_amount']);
        // $this->paypal_lib->add_field('quantity', $quantity);
        // $this->paypal_lib->add_field('discount_amount', $discount);
        // additional information 
        $this->paypal_lib->add_field('custom', $order_id);

        $this->paypal_lib->image('');
        // generates auto form
        $this->paypal_lib->paypal_auto_form();
    }

    public function bulk_success($order_id = null, $customer_id = null) {
        $this->bulk_order_save_update();
    }

    public function bulk_cancel($order_id = null, $customer_id = null) {
        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Payment was not successfull </div>");

        redirect('retailer-multiple-payment-wholesaler/' . $order_id);
    }

    /*
     * Add this ipn url to your paypal account
     * Profile and Settings > My selling tools > 
     * Instant Payment Notification (IPN) > update 
     * Notification URL: (eg:- http://domain.com/website/paypal/ipn/)
     * Receive IPN messages (Enabled) 
     */

    public function bulk_ipn() {

        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;

        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (preg_match("/VERIFIED/i", $result)) {
            //insert the transaction data into the database
            $this->load->model('Paypal_model');

            $this->Paypal_model->insertTransaction($data);
        }

        return true;
    }

     public function bulk_order_save_update() {

        $order_ids_list = $this->session->userdata('_tran_token');
        $bulk_orders_session = $this->session->userdata('bulk_order');

        foreach ($bulk_orders_session as $key => $bulk_order) {
        
            $order_id               = $bulk_order['order_id'];
            $customer_id            = $bulk_order['customer_id'];
            $payment_method         = $bulk_order['payment_method'];
            $b_order_id             = $bulk_order['b_order_id'];
            $paid_amount            = $bulk_order['paid_amount'];
            $headcode               = $bulk_order['headcode'];
            $due                    = $bulk_order['due'];

            $action_page = $this->uri->segment(2);
            $action_done = "updated";
            $remarks = "order information updated";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);


            if (!empty($order_id)) {

                
                if ($due > 0) {
                    $order_stage = 3;
                } else {
                    $order_stage = 2;
                }

                $rowData = $this->db->select('due,paid_amount')->where('order_id', $order_id)->get('b_level_quatation_tbl')->row();
             
                // quatation table update
                $orderData = array(
                    'paid_amount' => @$rowData->paid_amount + $paid_amount,
                    'due' => $due,
                    'order_stage' => $order_stage,
                    'status' => 1
                );
                //update to quatation table with pyament due 
                $this->db->where('order_id', $bulk_order['order_id'])->update('b_level_quatation_tbl', $orderData);
                // quatation table update
                $payment_tbl = array(
                    'quotation_id' => $b_order_id,
                    'payment_method' => $payment_method,
                    'paid_amount' => $paid_amount,
                    'description' => "Payment given for Order#" . $order_id,
                    'payment_date' => date('Y-m-d'),
                    'created_by' => $this->session->userdata('user_id'),
                    'create_date' => date('Y-m-d')
                );
                $this->db->insert('payment_tbl', $payment_tbl);


                $voucher_no = $b_order_id;
                $Vtype = "INV";
                $VDate = date('Y-m-d');
                $paid_amount = $paid_amount;
                $cAID = $headcode;
                $IsPosted = 1;
                $CreateBy = $this->session->userdata('user_id');
                $createdate = date('Y-m-d H:i:s');
                $Paypal_COAID = '1020104';
                //C level transaction Start
                $customerCredit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $Paypal_COAID,
                    'level_id'=>$this->level_id,
                    'Narration' => "Paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $customerCredit);

                //------------------------------------

                $bCOA = '502020101';

                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'level_id'=>$this->level_id,
                    'Credit' => 0,
                    'COAID' => $bCOA,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('acc_transaction', $b_levelDebit);
                // C level transaction END
                // B level transaction start
                // 
                $customerCredit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => 0,
                    'Credit' => $paid_amount,
                    'COAID' => $cAID,
                    'Narration' => "Customer " . $cAID . " paid for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'level_id'=>$this->level_id,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('b_acc_transaction', $customerCredit);

                //------------------------------------

                $bCOA = '502020101';

                $b_levelDebit = array(
                    'VNo' => $voucher_no,
                    'Vtype' => $Vtype,
                    'VDate' => $VDate,
                    'Debit' => $paid_amount,
                    'Credit' => 0,
                    'COAID' => $Paypal_COAID,
                    'level_id'=>$this->level_id,
                    'Narration' => "Amount received for invoice #" . $voucher_no,
                    'IsPosted' => $IsPosted,
                    'CreateBy' => $CreateBy,
                    'CreateDate' => $createdate,
                    'IsAppove' => 1
                );

                $this->db->insert('b_acc_transaction', $b_levelDebit);
                
                $cNotificationData = array(
                    'notification_text' => 'Payment has been done for ' . $order_id,
                    'go_to_url' => 'retailer-invoice-receipt/' . $order_id,
                    'created_by' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d')
                );

                $this->db->insert('c_notification_tbl', $cNotificationData);
                //-------------------------
                //


                $bNotificationData = array(
                    'notification_text' => 'Payment has been received for ' . $b_order_id,
                    'go_to_url' => 'wholesaler-retailer-receipt/' . $b_order_id,
                    'created_by' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d')
                );
                $this->db->insert('b_notification_tbl', $bNotificationData);

                //--------------------------


                $data = array(
                    'customer_id' => $customer_id,
                    'message'     => 'Payment has been done for Order id'.$order_id.', Paid amount is '.$paid_amount.', Payment method is'.$payment_method,
                    'subject' => 'Order payment'
                );


                //--------------------------
                
            }
        }
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Sucessfully </div>");

        redirect('retailer-multiple-payment-wholesaler/' . $order_id);
    }



}
