<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('b_level/Product_model');
        $this->load->library('twilio');
    }

    public function index() {
        
    }

    public function get_card_info($id) {

        $info = $this->db->where('id', $id)->where('created_by', $this->user_id)->get('c_card_info')->row();

        echo json_encode($info);
    }

    //============= its for card info =============== 
    public function card_info() {

        $this->permission_c->check_label(60)->create()->redirect();
        $access_retailer=access_role_permission_page(60);
        $acc=access_role_retailer();
        $data['info'] = $this->db->where('created_by', $this->user_id)->get('c_card_info')->result();
     
       if ($access_retailer==1 or $acc==1) { 
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/settings/my_card_info', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    
    }

    public function getCard() {

        $data = $this->db->where('created_by', $this->user_id)->where('is_active', 1)->get('c_card_info')->row();
        echo json_encode($data);
    }

    public function card_info_update() {
        //dd($this->input->post());
        $id = $this->input->post('id');

        if (isset($id) && !empty($id)) {

            $info = $this->db->where('id', $id)->where('created_by', $this->user_id)->get('c_card_info')->row();

            if ($info != NULL) {

                if ($this->input->post('is_active') == '1') {
                    $this->db->set('is_active', 0)->where('created_by', $this->user_id)->update('c_card_info');
                }
                //        ============ its for access log info collection ===============
                $action_page = $this->uri->segment(1);
                $action_done = "updated";
                $remarks = "my card information updated";
                $accesslog_info = array(
                    'action_page' => $action_page,
                    'action_done' => $action_done,
                    'remarks' => $remarks,
                    'user_name' => $this->user_id,
                    'level_id' => $this->level_id,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'entry_date' => date("Y-m-d H:i:s"),
                );
                $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
                $arrayName = array(
                    'card_number' => $this->input->post('card_number'),
                    'expiry_month' => $this->input->post('expiry_month'),
                    'expiry_year' => $this->input->post('expiry_year'),
                    'card_holder_first_name' => $this->input->post('card_holder_first_name'),
                    'card_holder_last_name' => $this->input->post('card_holder_last_name'),
                    'email' => $this->input->post('email'),
                    'phone_number' => $this->input->post('phone_number'),
                    'address' => $this->input->post('address'),
                    'company' => $this->input->post('company'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'zip_code' => $this->input->post('zip_code'),
                    'country_code' => $this->input->post('country_code'),
                    'created_by' => $this->user_id,
                    'is_active' => $this->input->post('is_active'),
                    'update_by' => $this->user_id,
                    'update_date' => date('Y-m-d')
                );
                $this->db->where('id', $id)->where('created_by', $this->user_id)->update('c_card_info', $arrayName);
                //echo "==>".$this->db->last_query();die;
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card information update successfully!</div>");
            }
        } else {
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "insert";
            $remarks = "my card information inserted";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
            if ($this->input->post('is_active') == '1') {
                $this->db->set('is_active', 0)->where('created_by', $this->user_id)->update('c_card_info');
            }


            $arrayName = array(
                'card_number' => $this->input->post('card_number'),
                'expiry_month' => $this->input->post('expiry_month'),
                'expiry_year' => $this->input->post('expiry_year'),
                'card_holder_first_name' => $this->input->post('card_holder_first_name'),
                'card_holder_last_name' => $this->input->post('card_holder_last_name'),
                'email' => $this->input->post('email'),
                'phone_number' => $this->input->post('phone_number'),
                'address' => $this->input->post('address'),
                'company' => $this->input->post('company'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip_code' => $this->input->post('zip_code'),
                'country_code' => $this->input->post('country_code'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->user_id,
                'create_date' => date('Y-m-d')
            );

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card information setup successfully!</div>");

            $this->db->insert('c_card_info', $arrayName);
        }

                    $user_id = $this->session->userdata('user_id'); 
                    $user_deatils = $this->db->where('id',$user_id)->get('user_info')->row();

                    if($user_deatils->advanced_trail || $user_deatils->pro_trail ){
                   redirect($_POST['package_confirm_page_url']);

                 }else{
                   redirect("package");
         }
    }

    public function card_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "my card information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->db->where('id', $id)->where('created_by', $this->user_id)->delete('c_card_info');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Card delete successfully!</div>");

        redirect("retailer-credit-card");
    }

//    ============= its for my_account =============== 
    public function my_account() {
        $this->permission_c->check_label(54)->create()->redirect();
        $data['my_account'] = $this->Setting_model->my_account();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/my_account', $data);
        $this->load->view('c_level/footer');
    }

//    ========== its for my_account_update ===========
    public function my_account_update() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "my account information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $updatedate = date('Y-m-d');
        $user_id = $this->input->post('user_id');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $language = $this->input->post('language');

        // configure for upload 
        $config = array(
            'upload_path' => "./assets/c_level/uploads/appsettings/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
//            'file_name' => "SPF" . time(),
            'max_size' => '0',
            'encrypt_name' => TRUE,
        );
//
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
            $myaccount_picture_unlink = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
            if ($myaccount_picture_unlink->user_image) {
                $img_path = FCPATH . 'assets/c_level/uploads/appsettings/' . $myaccount_picture_unlink->user_image;
                unlink($img_path);
            }

            $image_data = $this->upload->data();
//                print_r($image_data); die();
            $image_name = $image_data['file_name'];
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path']; //get original image
            $config['maintain_ratio'] = TRUE;
            $config['height'] = '*';
            $config['width'] = '*';
//                $config['quality'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->clear();
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
        } else {
            $image_name = $this->input->post('image_hdn');
        }

        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'company' => $company,
            'address' => $address,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'phone' => $phone,
            'email' => $email,
            'language' => $language,
            'user_image' => $image_name,
            'updated_by' => $this->user_id,
            'update_date' => $updatedate,
        );
//        echo '<pre>';        print_r($user_data);die();
        $this->db->where('id', $user_id);
        $this->db->update('user_info', $user_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>My account updated successfully!</div>");
        redirect("retailer-account");
    }

//    ============= its for company-profile =============== 
    public function company_profile() {
        $this->permission_c->check_label_multi(array(28,53))->create()->redirect();
        $access_retailer=access_role_permission_page(28);
        $acc=access_role_retailer();
        $data['company_profile'] = $this->Setting_model->company_profile();

            if ($access_retailer==1 or $acc==1) {
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/settings/company_profile', $data);
                    $this->load->view('c_level/footer');
            }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
            }
    }

//    ================ its for company_profile_update ===================
    public function company_profile_update() {
        $user_id = $this->input->post('user_id');
        $company_name = $this->input->post('company_name');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip = $this->input->post('zip');
        $country_code = $this->input->post('country_code');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
//        $password = $this->input->post('password');
//        $image = $this->input->post('image');
        $email_notification = $this->input->post('email_notification');
        $sms_notification = $this->input->post('sms_notification');
        $currency = $this->input->post('currency');
//        $logo = $this->input->post('logo');
//        if ($password) {
//            $password_change = array(
//                'password' => md5($password)
//            );
//            $this->db->where('user_id', $user_id);
//            $this->db->update('log_info', $password_change);
//        }
//        ========== its for logo =============
// configure for upload 
        $config = array(
            'upload_path' => "./assets/c_level/uploads/appsettings/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
//            'file_name' => "SPF" . time(),
            'max_size' => '0',
            'encrypt_name' => TRUE,
        );
//
        $image_data = array();

        $this->load->library('upload', $config);

        $this->upload->initialize($config);
//
//        if ($this->upload->do_upload('image')) {
//            $image_data = $this->upload->data();
////                print_r($image_data); die();
//            $image_name = $image_data['file_name'];
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $image_data['full_path']; //get original image
//            $config['maintain_ratio'] = TRUE;
//            $config['height'] = '*';
//            $config['width'] = '*';
////                $config['quality'] = 50;
//            $this->load->library('image_lib', $config);
//            $this->image_lib->clear();
//            $this->image_lib->initialize($config);
//            if (!$this->image_lib->resize()) {
//                echo $this->image_lib->display_errors();
//            }
//        } else {
//            $image_name = $this->input->post('image_hdn');
//        }
//            ========= its for logo upload =========

        if ($this->upload->do_upload('logo')) {
            $company_picture_unlink = $this->db->select('*')->from('company_profile')->where('user_id', $user_id)->get()->row();
            if ($company_picture_unlink->logo) {
                $img_path = FCPATH . 'assets/c_level/uploads/appsettings/' . $company_picture_unlink->logo;
                unlink($img_path);
            }
            $apps_logo = $this->upload->data();
//          echo '<pre>';  print_r($image_dataTwo); die();
            $logo = $apps_logo['file_name'];
        } else {
            $logo = $this->input->post('logo_hdn');
        }
//            ============ close logo upload ===========

        $web_setting = array(
            'company_name' => $company_name,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
            'currency' => $currency,
            'email_notification' => $email_notification,
            'sms_notification' => $sms_notification,
//            'picture' => $image_name,
            'logo' => $logo,
            'updated_by' => $this->user_id,
            'setting_status'=>1
        );
//        dd($web_setting);
        $this->db->where('user_id', $user_id);
        $this->db->update('company_profile', $web_setting);
//        =========== its for company info update by customer table ============
        $customer_data = array(
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
        );
        $this->db->where('customer_user_id ', $user_id);
        $this->db->update('customer_info', $customer_data);
//        ============= its for user info update ============
        $customer_data = array(
            'address' => $address,
            'company' => $company_name,
            'city' => $city,
            'state' => $state,
            'zip_code' => $zip,
            'country_code' => $country_code,
            'email' => $email,
            'phone' => $phone,
        );
        $this->db->where('id ', $user_id);
        $this->db->update('user_info', $customer_data);


        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "company profile information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Company profile updated successfully!</div>");
        redirect('company-profile');
    }

//     =========== its for change_password =============
    public function change_password() {
        $this->permission_c->check_label(55)->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/change_password');
        $this->load->view('c_level/footer');
    }

//    ============== its for update_password ============
    public function update_password() {
        
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "update";
        $remarks = "user password updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $user_id = $this->input->post('user_id');
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $old_password_check = $this->db->select('password')->from('log_info')
                        ->where('password', md5($old_password))
                        ->where('user_id', $user_id)->get()->result();
//        echo '<pre>'; print_r($old_password_check);die();
        if (!empty($old_password_check)) {
            $change_details = array(
                'password' => md5($new_password),
            );
            $this->db->where('user_id', $user_id);
            $this->db->update('log_info', $change_details);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Password changed successfully!</div>");
            redirect("retailer-password-change");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Old password does not match!</div>");
            redirect("retailer-password-change");
        }
    }

//==============its for payment_gateway ============
    public function payment_gateway() {



$this->permission_c->check_label(29)->create()->redirect();
        $access_retailer=access_role_permission_page(61);
        $acc=access_role_retailer();
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $data['gateway_edit'] = $this->db->where('level_id', $level_id)->get('tms_payment_setting')->row();
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header', $data);
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/settings/payment_gateway');
            $this->load->view('c_level/footer');
         }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
            }
    }

//    ========== its for payment_gateway_update =============
    public function payment_gateway_update() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "payment gateway information updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $gatewaydata = array(
            'url' => $this->input->post('url'),
            'user_name' => $this->input->post('user_name'),
            'password' => $this->input->post('password'),
            'mode' => 0,
            'level_id' => $level_id,
        );
        $gateway_check = $this->db->where('level_id', $level_id)->get('tms_payment_setting')->row();
        if ($gateway_check) {
            $this->db->where('level_id', $level_id)->update('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        } else {
            $this->db->insert('tms_payment_setting', $gatewaydata);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        }
        redirect("payment-gateway");
    }

//    ============= its for payment_setting =============== 
    public function payment_setting() {


        $this->permission_c->check_label(29)->create()->redirect();

        $config["base_url"] = base_url('b_level/Setting_controller/gateway');
        $config["total_rows"] = $this->db->count_all('gateway_tbl');
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["gateway_list"] = $this->Setting_model->gateway_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $access_retailer=access_role_permission_page(29);
        $acc=access_role_retailer();
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header', $data);
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/settings/payment_setting');
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
            }
    }

//    ============= its for c_save_gateway =====
    public function c_save_gateway() {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "payment setting information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $createdate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $mode = $this->input->post('mode');
        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'c',
            'created_by' => $level_id,
            'default_status' => '1',
            'status' => $mode,
            'created_date' => $createdate
        );

        $check_gateway = $this->db->where('created_by', $level_id)->where('level_type', 'c')->get('gateway_tbl')->result();
        if ($check_gateway) {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway configuration already done!</div>");
            redirect("payment-setting");
        } else {
            $this->db->insert('gateway_tbl', $gateway_data);
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway save successfully!</div>");
        redirect("payment-setting");
    }

    public function c_gateway_edit($id) {
        $data['gateway_edit'] = $this->Setting_model->gateway_edit($id);
//        dd($data['gateway_edit']);

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/payment_setting_edit');
        $this->load->view('c_level/footer');
    }

//    ============= its for update_payment_gateway =============
    public function update_payment_gateway($id) {
        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $updatedate = date('Y-m-d');
        $payment_gateway = $this->input->post('payment_gateway');
        $payment_mail = $this->input->post('payment_mail');
        $currency = $this->input->post('currency');
        $is_active = $this->input->post('is_active');
        $mode = $this->input->post('mode');
        $gateway_data = array(
            'payment_gateway' => $payment_gateway,
            'payment_mail' => $payment_mail,
            'currency' => $currency,
            'level_type' => 'c',
            'default_status' => $is_active,
            'updated_by' => $this->user_id,
            'status' => $mode,
            'updated_date' => $updatedate
        );
//        echo '<pre>';        print_r($category_data);die();
        $this->db->where('id', $id);
        $this->db->update('gateway_tbl', $gateway_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('id !=', $id);
        $this->db->where('created_by', $level_id);
        $this->db->update('gateway_tbl', $default_status);

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "payment setting information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================


        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway updated successfully!</div>");
        redirect("payment-setting");
    }

//    ============== its for gateway_delete==============
    public function gateway_delete($id) {

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "gateway information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $this->db->where('id', $id)->delete('gateway_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Gateway deleted successfully!</div>");
        redirect("payment-setting");
    }

//    ============= its for cost_factor =============== 
    public function cost_factor() {
        $this->permission_c->check_label_multi(array(30, 112, 120))->create()->redirect();
        $access_retailer=access_role_permission_page(30);
        $acc=access_role_retailer();
//        echo $this->level_id;die();
        $data['get_product'] = $this->Setting_model->get_product();
        $data['get_only_product_c_cost_factor'] = $this->Setting_model->get_only_product_c_cost_factor($this->level_id);
        $data['individual_cost_factor'] = array_column($data['get_only_product_c_cost_factor'], 'individual_cost_factor', 'product_id');
        $data['costfactor_discount']  = array_column($data['get_only_product_c_cost_factor'], 'costfactor_discount', 'product_id');
       // if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){ 
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header', $data);
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/settings/cost_factor');
            $this->load->view('c_level/footer');
         }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
    }
      //    }else{
      //   $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
      //     $this->load->view('c_level/header');
      //     $this->load->view('c_level/sidebar');
      //     $this->load->view('c_level/upgrade_error');
      //     $this->load->view('c_level/footer');
      // }
    
    }

//    ============== its for retailer cost_factor_save =============
    public function cost_factor_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "cost factor information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        if($this->input->post('submit_type') != '' && $this->input->post('submit_type') == 'bulk') {
            $product_id = $this->input->post('Id_List');
            $txt_individual_cost_factor = $this->input->post('txt_individual_cost_factor');

            foreach ($product_id as $key => $p_id) {
                // echo $txt_individual_cost_factor;die;
                if($txt_individual_cost_factor > 0){
                    $costfactor_discount = (100 - ($txt_individual_cost_factor * 100));
                }else{
                    $costfactor_discount = 100;
                }
                $get_cost_product = $this->Setting_model->get_product_c_cost_factor_by_id($this->level_id, $p_id);
                if(!empty($get_cost_product)){
                    $data = [
                        'individual_cost_factor' => $txt_individual_cost_factor,
                        'costfactor_discount' => $costfactor_discount,
                    ];
                    $this->db->where('product_id', $p_id);
                    $this->db->where('level_id', $this->level_id);
                    $this->db->where('created_by', $this->user_id);
                    $this->db->update('c_cost_factor_tbl', $data);
                } else {
                    $cost_factor_data = array(
                        'product_id' => $p_id,
                        'individual_cost_factor' => $txt_individual_cost_factor,
                        'costfactor_discount' => $costfactor_discount,
                        'level_id' => $this->level_id,
                        'created_by' => $this->user_id,
                        'create_date' => date('Y-m-d'),
                    );
                    $this->db->insert('c_cost_factor_tbl', $cost_factor_data);
                }
            }
        } else {
            $product_id = $this->input->post('product_id');
            $individual_cost_factor = $this->input->post('individual_cost_factor');
            $costfactor_discount = $this->input->post('costfactor_discount');

            $this->db->where('level_id', $this->level_id)->delete('c_cost_factor_tbl');

            //=========== its for product condition mapping ==============
            for ($i = 0; $i < count($product_id); $i++) {
                $cost_factor_data = array(
                    'product_id' => $product_id[$i],
                    'individual_cost_factor' => $individual_cost_factor[$i],
                    'costfactor_discount' => $costfactor_discount[$i],
                    'level_id' => $this->level_id,
                    'created_by' => $this->user_id,
                    'create_date' => date('Y-m-d'),
                );
                $this->db->insert('c_cost_factor_tbl', $cost_factor_data);
            }
            //=========== close product condition mapping ============
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Cost factor updated successfully</div>");
        redirect("customer-cost-factor");
    }

//    ============= its for iframe_code =============== 
    public function iframe_code() {

        $this->permission_c->check_label(31)->create()->redirect();
        $access_retailer=access_role_permission_page(31);
        $acc=access_role_retailer();
        $data['check_iframe_code'] = $this->Setting_model->check_iframe_code();
        if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){ 
            if($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/settings/iframe_code', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

//    =============== its for iframe_code_save ==============
    public function iframe_code_save() {
        $data['check_iframe_code'] = $this->Setting_model->check_iframe_code();
//        dd($data['check_iframe_code']);
        $iframe_code = $this->input->post('iframe_code');

        if ($data['check_iframe_code']) {
            $iframe_data = array(
                'iframe_code' => $iframe_code,
                'updated_by' => $this->user_id,
                'level_type' => 'c',
            );
            $this->db->where('created_by', $this->user_id);
            $this->db->update('iframe_code_tbl', $iframe_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Iframe code updated successfully!</div>");
        } else {
            $iframe_data = array(
                'iframe_code' => $iframe_code,
                'created_by' => $this->user_id,
                'level_type' => 'c',
            );
            $this->db->insert('iframe_code_tbl', $iframe_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Iframe code inserted successfully!</div>");
        }
        redirect('iframe-code');
    }

//    ============ its for d_customer_form ============
    public function d_customer_form() {
//         $data['d_level_form'] = $this->db->select('*')->from('iframe_code_tbl')->where('frame_id', 1)->get()->result();
// //        $this->load->view('c_level/header');
// //        $this->load->view('c_level/sidebar');
//         $this->load->view('d_level/customer_form', $data);
//        $this->load->view('c_level/footer');
    }

//    ========= its for d_customer_iframe_form ============ 
    public function d_customer_iframe_form() {

//        $user_id = $this->session->userdata('user_id');
//          $data['d_level_form'] = $this->db->select('*')->from('iframe_code_tbl')->where('created_by', $user_id)->get()->result();

        $this->load->view('d_level/d_customer_iframe_form');
    }

    public function d_customer_iframe_form_custom($user_id, $user_type) {

        $this->load->view('d_level/customer_form');
    }

//================ its for d_customer_info_save =============
    public function d_customer_info_save() {
//        $action_page = $this->uri->segment(1);
//        $action_done = "insert";
//        $remarks = "Customer information save";
        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $company = $this->input->post('company');
        $address = $this->input->post('address');
        $address_explode = explode(",", $address);
        $address = $address_explode[0];
        $street_no = explode(' ', $address);
        $street_no = $street_no[0];
        $side_mark = $first_name . "-" . $street_no;
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $zip_code = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');
        $reference = $this->input->post('reference');
        $user_id = $this->input->post('user_id');
        $level_type = $this->input->post('level_type');
        $level_id = $user_id;
//        if ($this->session->userdata('isAdmin') == 1) {
//            $level_id = $this->session->userdata('user_id');
//        } else {
//            $level_id = $this->session->userdata('admin_created_by');
//        }
//        dd($level_id);
        $email_check = $this->db->select('email')->from('customer_info')->where('email', $email)->where('level_id', $level_id)->get()->row();
        if (isset($email_check->email) && $email_check->email != '') {
            $this->session->set_flashdata('success', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>This email already exists!</div>");
            redirect($_SERVER['HTTP_REFERER']);
        } else {

//        ============ its for accounts coa table ===============
            $coa = $this->Customer_model->headcode($level_id);
                 // echo '<pre>';                print_r($coa); 
                    if ($coa->HeadCode != NULL) {
                        $hc= explode("-", $coa->HeadCode);
                        $nxt = $hc[1]+1;
                        $headcode = $hc[0]."-".  $nxt;                      
                    } else {
                        $headcode = "1020301-1";
                    } 
//                    dd($headcode);

            $lastid = $this->db->select("*")->from('customer_info')
                    ->order_by('customer_id', 'desc')
                    ->get()
                    ->row();
            //dd($lastid);
            $sl = $lastid->customer_no;
            if (empty($sl)) {
                $sl = "CUS-0001";
            } else {
                $sl = $sl;
            }
            $supno = explode('-', $sl);
            $nextno = $supno[1] + 1;
            $si_length = strlen((int) $nextno);

            $str = '0000';
            $cutstr = substr($str, $si_length);
            $sino = "CUS" . "-" . $cutstr . $nextno;

//        $customer_name = $this->input->post('customer_name');
            $customer_no = $sino . '-' . $first_name . " " . $last_name;
//        ================= close =======================
            //        =============== its for company name with customer id start =============
            $last_c_id = $lastid->customer_id;
//            $cn = strtoupper(substr($company, 0, 3)) . "-";
            $cn = $first_name . "-";
            if (empty($last_c_id)) {
                $last_c_id = $cn . "1";
            } else {
                $last_c_id = $last_c_id;
            }
            $cust_nextid = $last_c_id + 1;
            $company_custid = $cn . $cust_nextid;
//            dd($company_custid);
//        =============== its for company name with customer id close=============
//        ======== its for customer COA data array ============
            $customer_coa = array(
                'HeadCode' => $headcode,
                'HeadName' => $customer_no,
                'PHeadName' => 'Customer Receivable',
                'HeadLevel' => '4',
                'IsActive' => '1',
                'IsTransaction' => '1',
                'IsGL' => '0',
                'HeadType' => 'A',
                'IsBudget' => '0',
                'IsDepreciation' => '0',
                'DepreciationRate' => '0',
                'CreateBy' => $user_id,
                'level_id' => $level_id,
                'CreateDate' => $created_date,
            );
//        dd($customer_coa);
            $this->db->insert('acc_coa', $customer_coa);
//        ======= close ==============

            $customer_data = array(
                'company_customer_id' => $company_custid,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone' => $phone,
                'company' => $company,
                'customer_no' => $customer_no,
                'customer_type' => 'personal',
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip_code' => $zip_code,
                'country_code' => $country_code,
                'street_no' => $street_no,
                'side_mark' => $side_mark,
                'reference' => $reference,
                'level_id' => $level_id,
                'created_by' => $user_id,
                'create_date' => $created_date,
            );
            $this->db->insert('customer_info', $customer_data);
            $customer_id = $this->db->insert_id();

            $remarks = $this->input->post('comment');
            $appointmen_status = 'website';
            $appointment_data = array(
                'customer_id' => $customer_id,
                'c_level_id' => $level_id,
                'appointment_date' => $created_date, //$appointment_date,
                'appointment_time' => '', //$appointment_time,
                'c_level_staff_id' => '', //$c_level_staff_id,
                'remarks' => $remarks,
                'create_by' => $customer_id, //$user_id,
                'level_from' => 'd',
                'create_date' => $created_date,
            );
//        echo '<pre>';        print_r($appointment_data);die();
            $this->db->insert('appointment_calendar', $appointment_data);
            $this->db->set('now_status', $appointmen_status)->where('customer_id', $customer_id)->update('customer_info');

            $phone_types_number = array(
                'phone' => $phone,
                'phone_type' => 'Phone',
                'customer_id' => $customer_id,
                'customer_user_id' => $user_id,
            );
            $this->db->insert('customer_phone_type_tbl', $phone_types_number);
//        ============ its for access log info collection ===============
//        $accesslog_info = array(
//            'action_page' => $action_page,
//            'action_done' => $action_done,
//            'remarks' => $remarks,
//            'user_name' => $this->user_id,
//            'entry_date' => date("Y-m-d H:i:s"),
//        );
//        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Customer info save successfully!</div>");
        redirect($_SERVER['HTTP_REFERER']);
    }

//============= its for faq =============
    public function faq() {

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/faq');
        $this->load->view('c_level/footer');
    }

//============= its for profile_setting =============
//    public function profile_setting(){
//        
//         $this->load->view('c_level/header');
//        $this->load->view('c_level/sidebar');
//        $this->load->view('c_level/settings/profile_setting');
//        $this->load->view('c_level/footer');
//    }
    public function own_user_usstate_count($level_id, $user_id) {
        if ($user_id == $level_id) {
            $filter = "level_id=" . $level_id;
        } else {
            $filter = "created_by=" . $user_id;
        }
        $this->db->where($filter);
        $num_rows = $this->db->count_all_results('c_us_state_tbl');
        return $num_rows;
    }

//============= its for us_state =============
    public function us_state() {
        $this->permission_c->check_label(39)->create()->redirect();
        $access_retailer=access_role_permission_page(39);
        $acc=access_role_retailer();
        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->where('created_by', $this->level_id)->group_by('state_name')->get()->result();
        $config["base_url"] = base_url('retailer-us-state');
        $config["total_rows"] = $this->own_user_usstate_count($this->level_id, $this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["us_state_list"] = $this->Setting_model->us_state_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header', $data);
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/settings/us_state');
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }else{
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
              $this->load->view('c_level/header');
              $this->load->view('c_level/sidebar');
              $this->load->view('c_level/upgrade_error');
              $this->load->view('c_level/footer');
    }
    }

//    ============= its for us_state_save ==========
    public function c_us_state_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "us state information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');
        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->insert('c_us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State save successfully!</div>");
        redirect('retailer-us-state');
    }

//    ============= its for us_state_edit ============
    public function c_us_state_edit($id) {
       // $this->permission_c->check_label('c_us_state_edit')->read()->redirect();
        $data['get_city_state'] = $this->db->select('*')->from('city_state_tbl')->group_by('state_id')->get()->result();
        $data['us_state_edit'] = $this->Setting_model->us_state_edit($id);
        $shortcode = $data['us_state_edit'][0]['shortcode'];
        $data['get_statename'] = $this->db->where('state_id', $shortcode)->group_by('state_name')->get('city_state_tbl')->result();

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/us_state_edit');
        $this->load->view('c_level/footer');
    }

//    =========== its for c_us_state_update ===========
    public function c_us_state_update($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "us state information update";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

        $short_code = $this->input->post('short_code');
        $state_name = $this->input->post('state_name');
        $tax_rate = $this->input->post('tax_rate');

        $us_state_data = array(
            'shortcode' => $short_code,
            'state_name' => $state_name,
            'tax_rate' => $tax_rate,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
        );
        $this->db->where('state_id', $id);
        $this->db->update('c_us_state_tbl', $us_state_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State updated successfully!</div>");
        redirect('retailer-us-state');
    }

    //    ============ its for us_state_delete =============
    public function us_state_delete($id) {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "us state information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $this->db->where('state_id', $id);
        $this->db->delete('c_us_state_tbl');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State deleted successfully!</div>");
        redirect('retailer-us-state');
    }

//    =========== its for import_c_us_state_save =========
    public function import_c_us_state_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "us state information import done";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['shortcode'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['state_name'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                    $insert_csv['tax_rate'] = (!empty($csv_line[2]) ? $csv_line[2] : 0);
                }
                $data = array(
                    'shortcode' => $insert_csv['shortcode'],
                    'state_name' => $insert_csv['state_name'],
                    'tax_rate' => $insert_csv['tax_rate'],
                    'level_id' => $this->level_id,
                    'created_by' => $this->user_id
                );
                if ($count > 0) {
                    
                    // Check state name is exist or not : START
                    $state_data = $this->db->select('*')
                            ->from('city_state_tbl')
                            ->where('state_id', $data['shortcode'])
                            ->where('state_name', $data['state_name'])
                            ->where('created_by', $this->user_id)
                            ->get()
                            ->num_rows();
                    // Check state name is exist or not : END

                    $is_price_int = '';
                    if(is_numeric($data['tax_rate'])){
                       $is_price_int = 1;
                    }

                    if($state_data > 0 && $is_price_int != ''){        

                        $result = $this->db->select('*')
                                ->from('c_us_state_tbl')
                                ->where('shortcode', $data['shortcode'])
                                ->or_where('state_name', $data['state_name'])
                                ->get()
                                ->num_rows();
                        if ($result == 0 && !empty($data['shortcode']) && !empty($data['state_name']) && !empty($data['tax_rate'])) {
                            $this->db->insert('c_us_state_tbl', $data);
                            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State imported successfully!</div>");
                        } else {
                            if(!empty($data['shortcode']) && !empty($data['state_name']) && !empty($data['tax_rate'])){
                                
                                $data = array(
                                    'shortcode' => $insert_csv['shortcode'],
                                    'state_name' => $insert_csv['state_name'],
                                    'tax_rate' => $insert_csv['tax_rate']
                                ); 
                                $this->db->where('shortcode', $data['shortcode']);
                                $this->db->update('c_us_state_tbl', $data);
                                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>US State import updated successfully!</div>");
                            }
                        }
                    }        
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        redirect('retailer-us-state');
    }

//    ============== its for city_state_wise_statename ===========
    public function city_statename_wise_stateid() {
        $state_name = $this->input->post('state_name');
        $get_stateid = $this->db->where('state_name', $state_name)->group_by('state_name')->get('city_state_tbl')->result();
        echo json_encode($get_stateid);
    }

//    ==========its for own user customer count =========
    public function own_access_log_count($level_id) {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == $level_id) {
            $filter = "level_id=" . $level_id;
        } else {
            $filter = "user_name=" . $user_id;
        }
        $this->db->where($filter);
        $num_rows = $this->db->count_all_results('accesslog');
        return $num_rows;
    }

//============= its for logs =============
    public function logs() {
        $this->permission_c->check_label(57)->create()->redirect();
        $config["base_url"] = base_url('logs');
//        $config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_access_log_count($this->level_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["access_logs"] = $this->Setting_model->access_logs($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;


        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/logs');
        $this->load->view('c_level/footer');
    }

    // SMS to c level : START
    public function sms() {
        $this->permission->check_label(68)->create()->redirect();
          $access_retailer=access_role_permission_page(68);
        $acc=access_role_retailer();
        $data['customers'] = $this->Customer_model->get_customer();
        if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){
             if ($access_retailer==1 or $acc==1) { 
                    $this->load->view('c_level/header', $data);
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/settings/sms');
                    $this->load->view('c_level/footer');
             }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
             }
           }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }
     // SMS to c level : START

    //  Send SMS : START
    public function sms_send() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "custom sms send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //  ============== close access log info =================
        $receiver = $this->input->post('receiver_id');
        $message = $this->input->post('message');

        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();

            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $receiver;
            $message = $message;

            if ($sms_gateway_info->provider_name == 'Twilio') {

                $response = $this->twilio->sms($from, $to, $message);
                if ($response->IsError) {
                    echo 'Error: ' . $response->ErrorMessage;
                    $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".$response->ErrorMessage."</div>");
                } else {
                    $sms_data = array(
                        'from' => $from,
                        'to' => $to,
                        'message' => $message,
                        'created_by' => $this->user_id,
                    );
                    $this->db->insert('custom_sms_tbl', $sms_data);
                    $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
            }
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }

        redirect("retailer-sms");
    }
    //  Send SMS : END

    //  Send CSV SMS : START
    public function sms_csv_upload() {
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['to'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['message'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                }

                if ($count > 0) {
                    if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
                        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();

                        $from = $sms_gateway_info->phone; //'+12062024567';
                        $to = $this->phone_fax_mobile_no_generate($insert_csv['to']); //$insert_csv['to'];
                        $message = $insert_csv['message'];

                        if ($sms_gateway_info->provider_name == 'Twilio') {
                            $response = $this->twilio->sms($from, $to, $message);
                            if ($response->IsError) {
                                echo 'Error: ' . $response->ErrorMessage;
                            } else {
                                $sms_data = array(
                                    'from' => $from,
                                    'to' => $to,
                                    'message' => $message,
                                    'created_by' => $this->user_id,
                                );
                                $this->db->insert('custom_sms_tbl', $sms_data);
                            }
                        } else {
                            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms not send successfully!</div>");
                        }
                    }else{
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
                    }
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms send successfully!</div>");
        redirect('retailer-sms');
    }
    //  Send CSV SMS : END

    //  Send GROUP SMS : START
    public function group_sms_send() {
        $customer_phone = $this->input->post('customer_phone');
        $message = $this->input->post('message');
        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
            $from = $sms_gateway_info->phone; //'+12062024567';
            for ($i = 0; $i < count($customer_phone); $i++) {
                $to = $customer_phone[$i];
                if($to != 'All'){
                    $message = $message;

                    if ($sms_gateway_info->provider_name == 'Twilio') {
                        $response = $this->twilio->sms($from, $to, $message);
                        if ($response->IsError) {
                            echo 'Error: ' . $response->ErrorMessage;
                        } else {
                            $sms_data = array(
                                'from' => $from,
                                'to' => $to,
                                'message' => $message,
                                'created_by' => $this->user_id,
                            );
                            $this->db->insert('custom_sms_tbl', $sms_data);
                            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                        }
                    } else {
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
                    }
                }    
            }
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }    
        redirect('retailer-sms');
    }
    //  Send GROUP SMS : END

    // =========== its for sms_configure =============
    public function sms_configure() {
        // $this->permission->check_label('sms_configuration')->create()->redirect();
        $access_retailer=access_role_permission_page(69);
        $acc=access_role_retailer();
        $data['get_sms_config'] = $this->Setting_model->get_sms_config();
       if($this->session->userdata('package_id')==3 or $this->session->userdata('package_id')==2  ){
                if ($access_retailer==1 or $acc==1) {
                    $this->load->view('c_level/header', $data);
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/settings/sms_configure');
                    $this->load->view('c_level/footer');
                }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
                }
        }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/upgrade_error');
        $this->load->view('c_level/footer');
    }
    }

    // ============= its for sms_config_save =============
    public function sms_config_save() {

        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');
        $test_sms_number = $this->input->post('test_sms_number');

        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => 0,
            'status' => 1,
            'created_by' => $this->user_id,
            'test_sms_number' => $test_sms_number,
        );
        $this->db->insert('sms_gateway', $config_data);

        //   ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "sms configuration insert";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway save successfully!</div>");
        redirect("retailer-sms-configure");
    }

    // =========== its for sms_config_edit =============
    public function sms_config_edit($gateway_id) {
        $data['sms_config_edit'] = $this->Setting_model->sms_config_edit($gateway_id);
        
        if(!$data['sms_config_edit']){
            redirect("retailer-sms-configure");
        }

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/sms_configure_edit');
        $this->load->view('c_level/footer');
    }

    //  ================ its for sms_config_update ==============
    public function sms_config_update($gateway_id) {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "sms configuration updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //  ============== close access log info =================

        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');
        $is_active = $this->input->post('is_active');
        $test_sms_number = $this->input->post('test_sms_number');

        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => $is_active,
            'status' => 1,
            'is_verify' => 0,
            'test_sms_number' => $test_sms_number,
        );
        $this->db->where('gateway_id', $gateway_id);
        $this->db->update('sms_gateway', $config_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('gateway_id !=', $gateway_id);
        $this->db->where('created_by', $this->user_id);
        $this->db->update('sms_gateway', $default_status);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway updated successfully!</div>");
        redirect("retailer-sms-configure");
    }

    // ============== Verified SMS : START ===========
    public function sms_config_verified($gateway_id) {
        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('gateway_id', $gateway_id)->where('created_by', $this->user_id)->get()->row();
        
        if(!$sms_gateway_info){
            redirect("retailer-sms-configure");
        }

        $this->twilio->initialize_twillio($sms_gateway_info->user, $sms_gateway_info->password,$sms_gateway_info->phone);

        $from = $sms_gateway_info->phone; //'+12062024567';
        $to = $sms_gateway_info->test_sms_number;
        $message = "Thanks for SMS Verified";

        if ($sms_gateway_info->provider_name == 'Twilio') {

            $response = $this->twilio->sms($from, $to, $message);

            if ($response->IsError) {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".$response->ErrorMessage."</div>");
            } else {
                $sms_data = array(
                    'from' => $from,
                    'to' => $to,
                    'message' => $message,
                    'created_by' => $this->user_id,
                );
                $this->db->insert('custom_sms_tbl', $sms_data);

                // For Verified Status : START
                $config_data = array(
                    'is_verify' => 1,
                );
                $this->db->where('gateway_id', $gateway_id);
                $this->db->where('created_by', $this->user_id);
                $this->db->update('sms_gateway', $config_data);
                // For Verified Status : END

                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS Configuration verified successfully!</div>");
            }
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS Configuration not verified successfully!</div>");
        }

        redirect("retailer-sms-configure");
    }
    // ============== Verified SMS : END =============

    // ============== its for sms_config_delete ============
    public function sms_config_delete($gateway_id) {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "deleted";
        $remarks = "sms configuration deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $this->db->where('gateway_id', $gateway_id);
        $this->db->where('created_by',$this->user_id);
        $this->db->delete('sms_gateway');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway deleted successfully!</div>");
        redirect("retailer-sms-configure");
    }
    //=========== its for mail =============
    public function mail_configure() {
        $this->permission_c->check_label(70)->create()->redirect();
        $data['get_mail_config'] = $this->Setting_model->get_mail_config();
        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/mail_configure');
        $this->load->view('c_level/footer');
        }else{
           $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
        }
    }
    //    ==================== its for update_mail_configure ============
    public function mail_config_save() {
        $updated_date = date('Y-m-d');
        $protocol = $this->input->post('protocol');
        $smtp_host = $this->input->post('smtp_host');
        $smtp_port = $this->input->post('smtp_port');
        $smtp_user = $this->input->post('smtp_user');
        $smtp_pass = $this->input->post('smtp_pass');
        $mailtype = $this->input->post('mailtype');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Email configuration save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $emailConfig = $this->db->select('*')->from('mail_config_tbl')->where('created_by', $this->user_id)->get()->row();
        $mail_config_data = array(
            'protocol' => $protocol,
            'smtp_host' => $smtp_host,
            'smtp_port' => $smtp_port,
            'smtp_user' => $smtp_user,
            'smtp_pass' => $smtp_pass,
            'mailtype' => $mailtype,
            'updated_by' => $this->user_id,
            'created_by' => $this->user_id,
            'updated_date' => $updated_date,
            'is_verified' => 0,
        );
        if($emailConfig) {
            $this->db->where('created_by', $this->user_id);
            $this->db->update('mail_config_tbl', $mail_config_data);
        } else {
            $this->db->insert('mail_config_tbl', $mail_config_data);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mail configuration updated successfully!</div>");
        redirect("retailer-mail-configure");
    }
    public function verify()
    {
        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Test email send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $to_email = $this->input->post('email_id');
        $message = "Mail configuration verification";
        if($this->Setting_model->sendCstomEmail($to_email, $message, false)) { 
            $mail_config_data = array(
                'is_verified' => 1,
            );
            $this->db->where('created_by', $this->user_id);
            $this->db->update('mail_config_tbl', $mail_config_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Configuration verified successfully!</div>");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Error in configuration verification. please check and try again!</div>");
        }
        redirect("retailer-mail-configure");
    }

    //     =========== its for sms =============
    public function email() {
        $this->permission->check_label(71)->create()->redirect();
        $access_retailer=access_role_permission_page(71);
        $acc=access_role_retailer();
        $data['customers'] = $this->Customer_model->get_customer();
        if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){ 
            if ($access_retailer==1 or $acc==1) {
                    $this->load->view('c_level/header', $data);
                    $this->load->view('c_level/sidebar');
                    $this->load->view('c_level/settings/email');
                    $this->load->view('c_level/footer');
            }else{
                    $this->load->view('c_level/header');
                    $this->load->view('c_level/sidebar'); 
                    $this->load->view('c_level/upgrade_error'); 
                    $this->load->view('c_level/footer');
            }
            }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

    //    =========== its for sms_send ==============
    public function email_send() {
        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "custom email send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $customer_email = $this->input->post('customer_email');
        $message = $this->input->post('message');
        $custom_email = $this->input->post('custom_email');
        if(!$this->input->post('customer_email')) { 
            $customer_email = array();
        }
        if($custom_email != '') {
            $custom_email = explode(',', $custom_email);
            $customer_email = array_merge($customer_email, $custom_email);
        }
        if(count($customer_email) > 0) {
            for ($i = 0; $i < count($customer_email); $i++) {
                $to_email = $customer_email[$i];
                if ($to_email != '' && $to_email != 'All') {
                    if($this->Setting_model->sendCstomEmail($to_email, $message)) {
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email send successfully!</div>");
                    } else {
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email not send successfully!</div>");
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please select atleast one record!</div>");
        }
        redirect("retailer-email");
    }


    public function blevel_cost_factor() {

        $this->permission_c->check_label(112)->create()->redirect();
        $access_retailer=access_role_permission_page(112);
        $acc=access_role_retailer();
        $data['blevel_cost'] = $this->Setting_model->blevel_cost_data();
        $data['b_company_detail'] = $this->Setting_model->b_company_data();
        $data['c_company_detail'] = $this->Setting_model->c_company_data();
        
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/settings/blevel_cost_factor',$data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    }


    public function customer_uom_list() {
        // $this->permission->check_label('unit_of_measurement')->create()->redirect();
        $access_retailer=access_role_permission_page(110);
        $acc=access_role_retailer();
        $config["base_url"] = base_url('customer-uom-list');
        $config["total_rows"] = $this->db->count_all('unit_of_measurement');
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["uom_list"] = $this->Setting_model->uom_list($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        $data['unit_data'] = $this->Setting_model->get_unit_data();
        $data["get_unit_con"]=$this->db->query("SELECT * FROM unit_of_conversion WHERE user_id=".$this->user_id)->result();
        //print_r($data['get_unit_con']);exit;
      if($this->session->userdata('package_id')==2 or  $this->session->userdata('package_id')==3 ){
        if($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/uom/uom', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
        }else{
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
    }
    }

        public function uom_save() {
            $uom_name = $this->input->post('uom_name');
            $status = $this->input->post('status');
             $unit_types = $this->input->post('unit_types');
            $uom_data = array(
                'uom_name' => $uom_name,
                 'unit_types' => $unit_types,
                'status' => $status,
                'user_id' => $this->user_id
            );
            $this->db->insert('unit_of_measurement', $uom_data);

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement save successfully!</div>");
            redirect('customer-uom-list');
        }

        public function uom_edit($id) 
        {
            $data['uom_edit'] = $this->Setting_model->uom_edit($id);
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/uom/uom_edit', $data);
            $this->load->view('c_level/footer');
        }



    public function uom_update($id) 
    {
        $uom_name = $this->input->post('uom_name');
        $status = $this->input->post('status');
        $unit_types = $this->input->post('unit_types');
        $uom_data = array(
            'uom_name' => $uom_name,
            'status' => $status,
            'unit_types' => $unit_types,
        );
        $this->db->where('uom_id', $id);
        $this->db->update('unit_of_measurement', $uom_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement updated successfully!</div>");
        redirect('customer-uom-list');
    }

    public function uom_delete($id) {

        // Delete Measurement for UOM : START
        $this->db->where('from_measure_id', $id)->delete('mapping_measurement');
        $this->db->where('to_measure_id', $id)->delete('mapping_measurement');
        // Delete Measurement for UOM : END


        $this->db->where('uom_id', $id)->delete('unit_of_measurement');
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Unit of measurement deleted successfully!</div>");
        redirect('customer-uom-list');
    }

    public function save_mapping(){

        $mapping_id = $this->input->post('mapping_id');
        $from_measure_id = $this->input->post('from_measure_id');
        $to_measure_id = $this->input->post('to_measure_id');
        $measure_value = $this->input->post('measure_value');

        foreach ($measure_value as $k => $measure) {
            if($mapping_id[$k] == ''){
                $measure_data = array(
                    'from_measure_id' => $from_measure_id[$k],
                    'to_measure_id' => $to_measure_id[$k],
                    'measure_value' => $measure_value[$k],
                    'user_id' => $this->session->userdata('user_id'),
                ); 
                $this->db->insert('mapping_measurement', $measure_data);
            }else{
                $measure_data = array(
                    'measure_value' => $measure_value[$k],
                ); 
                $this->db->where('id', $mapping_id[$k]);
                $this->db->update('mapping_measurement', $measure_data);
            }    
        }

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mapping updated successfully!</div>");
        redirect("customer-uom-list");
    }

    public function customer_shipping()
    {
        $data['lists'] = $this->db->select('*')->where('created_by',$this->session->userdata('user_id'))->get('shipping_method')->result();
        $access_retailer=access_role_permission_page(111);
        $acc=access_role_retailer();
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/shipping/shipping', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    }

    public function save_shipping_method()
    {
        $methodData = array(
            'method_name' => $this->input->post('method_name'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'account_id' => $this->input->post('account_id'),
            'access_token' => $this->input->post('access_token'),
            'created_by' => $this->session->userdata('user_id'),
            'updated_by' => $this->session->userdata('user_id'),
        );

        $check = $this->db->where('method_name', $this->input->post('method_name'))->where('created_by',$this->level_id)->get('shipping_method')->row();

        if (!empty($check)) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> This shipping method already exists</div>");
            redirect("customer-shipping");
        } else {

            $this->db->insert('shipping_method', $methodData);

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method save successfully</div>");
            redirect("customer-shipping");
        }
    }

    public function edit_shipping_method($id)
    {
        $data['method'] = $this->db->where('id', $id)->where('created_by',$this->level_id)->get('shipping_method')->row();
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/shipping/shipping_edit', $data);
        $this->load->view('c_level/footer');
    }

    public function update_shipping_method()
    {
        $methodData = array(
            'method_name' => $this->input->post('method_name'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'account_id' => $this->input->post('account_id'),
            'access_token' => $this->input->post('access_token'),
            'service_type' => $this->input->post('service_type'),
            'pickup_method' => $this->input->post('pickup_method'),
            'mode' => $this->input->post('mode'),
            'status' => $this->input->post('status')
        );
        $id = $this->input->post('id');

        $this->db->where('id', $id)->update('shipping_method', $methodData);

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method update successfully</div>");
//        redirect("shipping-edit/" . $id);
        redirect("customer-shipping");
    
    }


    public function delete_shipping_method($id) 
    {
        $this->db->where('id', $id)->delete('shipping_method');
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Shipping method delete successfully</div>");
        redirect("customer-shipping");
    }

    public function retailer_quickbooks_setting(){
        $data['user_id'] = $this->user_id;
        $data['user_type'] = $this->session->userdata('user_type');
        $this->session->set_userdata('current_url',base_url('retailer-store-quickbooks-settings'));
        // $data["action"] = base_url().'quickbooks/Authentication_controller/get_authentication';
        $quickbooks_setting_data = $this->Setting_model->getQuickBooksSettingData($this->user_id);
        if(!empty($quickbooks_setting_data)){
            if($quickbooks_setting_data->status == 1){
                $data['connection'] = "Connected";
                $data["action"] = 'quickbooks/Authentication_controller/revokeToken';
                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Quickbooks Connected successfully.!</div>");
            }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
            }
        }else{
                $data['connection'] = "Not Connected";
                $data["action"] = 'quickbooks/Authentication_controller/get_authentication';
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something getting wrong.!Please try again.!</div>");
        }
        $data["quickbooks_setting_data"] = $quickbooks_setting_data;
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/quickbooks_setting',$data);
        $this->load->view('c_level/footer');
    }

    public function change_quickbooks_setting_data(){
        $quickbook_setting_data = array(
            'user_id'           =>  $this->input->post('hidden_user_id'),
            'client_id'         =>  $this->input->post('client_id'),
            'client_secret'     =>  $this->input->post('client_secret'),
            'updated_at'        =>  date('Y-m-d H:i:s'),
        );
        $success = $this->Setting_model->add_quickbooks_setting_data($quickbook_setting_data);
        if($success){
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Quickbooks setting changed successfully.!</div>");
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something getting wrong.!Please try again.!</div>");
        }
        redirect("retailer-quickbooks-setting");
    }

}
