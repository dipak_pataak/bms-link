<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Customer_order_controller extends CI_Controller
{

    private $user_id = '';
    private $level_id = '';

    public function __construct()
    {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {

            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        //if ($session_id == '' || $user_type != 'c') {
        //  redirect('retailer-logout');
        //}

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('email_sender');
    }

    public function index()
    { 

    }

    
    public function new_order()
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->permission_c->check_label(5)->create()->redirect();
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_customer_category();
        // $data['get_patern_model'] = $this->Order_model->get_patern_model();
        // $data['colors'] = $this->db->where('created_by',$main_b_id)->get('color_tbl')->result();
        $data['orderjs'] = "c_level/orders/order_to_wholesaler_js.php";
        $data['currencys'] = $this->Setting_model->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['binfo'] = $this->db->where('user_id', $main_b_id)->get('company_profile')->row();
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/customer_new_order', $data);
        $this->load->view('c_level/footer');
    }


    public function get_order_form()
    {
        $data['cart_rowid'] = $cart_rowid = $this->input->get('row_id');
        $data['view_action'] = $view_action = $this->input->get('action');

        //$data['company_profile'] = $this->Setting_model->company_profile();

        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->where('created_by', $this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->Setting_model->company_profile();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "c_level/orders/order_js.php";

        $win_to_edit = array();
        foreach ($this->cart->contents() as $items) :
            if ($items['rowid'] == $cart_rowid) {
                $win_to_edit = $items;
                break;
            }
        endforeach;
        $data['win_to_edit'] = $win_to_edit;

        $data['get_product_order_info'] = (object) $win_to_edit;
        // echo '<pre>';
        // print_r($win_to_edit);

        $data['selected_attributes'] = $selected_attributes = json_decode($win_to_edit['att_options']);

        // for ($i=0; $i < $data['selected_attributes']; $i++) { 
        //     $options = $this->db->select('attr_options.*')
        //                         ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
        //                         ->where('product_attr_option.id', $data['selected_attributes'][i]->options[0]->option_key_value)
        //                         ->order_by('attr_options.att_op_id', 'ASC')
        //                         ->get('product_attr_option')->row();
        //     $data['selected_attributes'][i]->options[0]->option_type = $options->option_type;
        // }
        // print_r($data['selected_attributes']);
        // exit;
        echo $this->load->view('c_level/orders/order_new_form', $data);
    }


    public function save_order()
    {
        // $this->session->set_userdata(array('order-customer-c' => ''));
        set_cookie('order-customer-c','','3600');
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "inserted";
        $remarks = "order information inserted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');


        if (!empty($this->input->post('synk_status'))) {
            $synk_status = 1;
        } else {
            $synk_status = 0;
        }

        $barcode_img_path = '';

        if (!empty($orderid)) {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/c/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }


        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("new-order");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');


        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'customer_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $this->level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->user_id,
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => '',
            'synk_status' => $synk_status
        );




        $order_status = $this->input->post('order_status');
        $insertData = $this->db->insert('quatation_tbl', $orderData);

        if ($insertData) {

            $order_detail = array();
            foreach ($products as $key => $product_id) {

                // Check created By
                $this->db->select('*');
                $this->db->from('category_tbl');
                $this->db->where('category_id', $category[$key]);
                $q = $this->db->get();
                $cat_detail = $q->row();

                if ($cat_detail->created_status == 1) {
                    $created_status = 1;
                } else {
                    $created_status = 0;
                }
                // Check created By   


                $productData = array(
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'room' => $room[$key],
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key],
                    'created_status' => $created_status
                );


                //order details
                $this->db->insert('qutation_details', $productData);

                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                //order attributes
                $this->db->insert('quatation_attributes', $attrData);
                $order_detail[] = $productData;
            }

            //order transfer email
            $transfer_email = $this->input->post('transfer_email');
            if ($transfer_email == 1) {
                $transfer_email = $this->input->post('order_trns_email');
                $this->Order_model->transfer_order_email($orderData, $order_detail,$transfer_email);
            }
            //order transfer email

            
            // send email for customer
            $this->Order_model->send_link($orderData);
            //-----------------------
            // clear the cart session
            $this->cart->destroy();

            if ($synk_status == 1) {
                //$this->synk_data_to_b($order_id);
                redirect('retailer-synk-data-wholesaler/' . $orderid);
            }

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('retailer-custom-order-view/' . $orderid);

            //}
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
            redirect("order-to-wholesaler");
        }
    }


    public function order_view($order_id)
    {
       // $this->permission_c->check_label('order_view')->create()->redirect();

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('shipment_data')->row();
        $data['cmp_info'] = $this->Setting_model->company_profile();

        $data['card_info'] = $this->db->where('created_by', $this->user_id)->where('is_active', 1)->get('c_card_info')->row();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/order_view', $data);
        $this->load->view('c_level/footer');
    }

    public function saveCustSessionForOrder($cust="")
    {
        // $this->session->set_userdata(array('order-customer' => $cust));
        set_cookie('order-customer-c',$cust,'3600'); 
        echo $cust;
    }


    public function customer_wise_sidemark($customer_id = null)
    {
        $customer_sidemark = $this->db->select('*')
            ->from('customer_info')
            ->where('customer_id', $customer_id)
            ->get()->row();


        $sales_tax = $this->db->select('tax_rate')
            ->from('c_us_state_tbl')
            ->where('shortcode', $customer_sidemark->state)
            ->where('level_id', $this->level_id)
            ->get()->row();


        // Generate New order ID : START 
        $this->load->model('Common_model');
        $passenger_id = $this->Common_model->generate_order_id($customer_id, 'quatation_tbl', 'order_id', 'id');

        $final_passenger_id->order_id = $passenger_id;
        // Generate New order ID : END                

        $n = (object) $new_arr = array_merge((array) $customer_sidemark, (array) $sales_tax, (array) $final_passenger_id);

        echo json_encode($n);
    }

    public function search_sidemark($side_mark,$customer_name='')
    {
         $sidemark = $this->db->select('*')
            ->from('customer_info')
            ->where('side_mark', $side_mark)
            ->get()->row();


            if(!empty($sidemark)) 
            {
    // first ajax 
                // $this->session->set_userdata(array('order-customer-c' => $sidemark->customer_id));
                // $this->session->set_userdata('order-customer-c',$sidemark->customer_id);
                set_cookie('order-customer-c',$sidemark->customer_id,'3600'); 
    // first ajax 

    // customer_wise_sidemark    
                $customer_sidemark = $this->db->select('*')
                                            ->from('customer_info')
                                            ->where('customer_id', $sidemark->customer_id)
                                            ->get()->row();


                $sales_tax = $this->db->select('tax_rate')
                    ->from('c_us_state_tbl')
                    ->where('shortcode', $customer_sidemark->state)
                    ->where('level_id', $this->level_id)
                    ->get()->row();


                // Generate New order ID : START 
                $this->load->model('Common_model');
                $passenger_id = $this->Common_model->generate_order_id($sidemark->customer_id, 'quatation_tbl', 'order_id', 'id');

                $final_passenger_id->order_id = $passenger_id;
                // Generate New order ID : END                

                $n = (object) $new_arr = array_merge((array) $customer_sidemark, (array) $sales_tax, (array) $final_passenger_id);
    // customer_wise_sidemark    

                echo json_encode($n);
            }else{

                if($customer_name=='') 
                {
                   $customer_name = $side_mark;
                }
                $data = array('first_name'=>$customer_name,
                              'side_mark'=>$side_mark,
                              'level_id'=>$this->session->userdata('user_id'),
                              'created_by'=>$this->session->userdata('user_id'),
                              'create_date'=>date('Y-m-d'));
                $this->db->insert('customer_info',$data);
                $insert_id = $this->db->insert_id();

            // first ajax 
                // $this->session->set_userdata(array('order-customer-c' => $insert_id));
                set_cookie('order-customer-c',$insert_id,'3600'); 
            // first ajax 

            // customer_wise_sidemark    
                    $customer_sidemark = $this->db->select('*')
                                                ->from('customer_info')
                                                ->where('customer_id', $insert_id)
                                                ->get()->row();


                    $sales_tax = $this->db->select('tax_rate')
                        ->from('c_us_state_tbl')
                        ->where('shortcode', $customer_sidemark->state)
                        ->where('level_id', $this->level_id)
                        ->get()->row();


                    // Generate New order ID : START 
                    $this->load->model('Common_model');
                    $passenger_id = $this->Common_model->generate_order_id($insert_id, 'quatation_tbl', 'order_id', 'id');

                    $final_passenger_id->order_id = $passenger_id;
                    // Generate New order ID : END                

                    $n = (object) $new_arr = array_merge((array) $customer_sidemark, (array) $sales_tax, (array) $final_passenger_id);
             // customer_wise_sidemark    


                echo json_encode($n);
            }
            exit;
    }



}
