<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_controller extends CI_Controller {

    private $user_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        if ($session_id == NULL) {
            redirect('logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Appointment_model');
    }

    public function index() {
        
    }
//    ============ its for appointment-form =============
    public function appointment_form() {
          if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('user_created_by');
        }
        $data['get_users'] = $this->User_model->get_users();
        $data['get_customer'] = $this->Customer_model->get_customer();
        $data['get_appointment_info'] = $this->Appointment_model->get_appointment_info($level_id);
        
//        dd($data['get_appointment_info']);
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/settings/form_appointment', $data);
        $this->load->view('c_level/footer');
    }
    
//    ============ its for appointment_setup method ================
    public function appointment_setup() {
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $acc_remarks = "Appointment information save";
        $created_date = date('Y-m-d');

        $customer_id = $this->input->post('customer_id');
        if($this->session->userdata('isAdmin') == 1){
            $level_id = $this->session->userdata('user_id');
        }else{
          $level_id = $this->session->userdata('user_created_by'); 
        }
//        dd($c_level_id);
        $appointment_date = $this->input->post('appointment_date');
        $appointment_time = $this->input->post('appointment_time');
        $c_level_staff_id = $this->input->post('c_level_staff_id');
        $remarks = $this->input->post('remarks');

        $appointment_data = array(
            'customer_id' => $customer_id,
            'c_level_id' => $level_id,
            'appointment_date' => $appointment_date,
            'appointment_time' => $appointment_time,
            'c_level_staff_id' => $c_level_staff_id,
            'remarks' => $remarks,
            'create_by' => $this->user_id,
            'create_date' => $created_date,
        );
        $this->db->insert('appointment_calendar', $appointment_data);

        //        ============ its for access log info collection ===============
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $acc_remarks,
            'user_name' => $this->user_id,
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
//        echo '<pre>';        print_r($appointment_data); die();
              $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Appointment setup successfully!</div>");
        redirect('customer-list');
    }

}
