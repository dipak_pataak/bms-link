<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_rma_controller extends CI_Controller {

    private $user_id = '';
    private $level_id = '';

    public function __construct() {
        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        if ($session_id == '' || $user_type != 'c') {
            redirect('retailer-logout');
        }
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Supplier_model');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/C_return_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('common_model');
        $this->load->model('b_level/settings');
        $this->load->library('twilio');
        
    }

    public function index() {
        $data['get_users'] = $this->User_model->get_users();
          $access_retailer=access_role_permission_page(117);
        $acc=access_role_retailer();
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/orders/return_request', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    }

//    ================ its for customer list ===============
    public function getCustomers() {
        //dd('stop');
        $customer_name = $this->input->post("customer_name");
        $results = $this->db->select("customer_id as id, CONCAT((first_name),(' '),(last_name)) as name")
            ->from('customer_info')
            ->where('created_by', $this->session->userdata('user_id'))
            ->having("name LIKE '%$customer_name%' ")
            ->get()->result();
        if(count($results) == 0) {
            $results = $this->db->select("u.customer_id as id, q.side_mark as name")
            ->from('customer_info u')
            ->join('quatation_tbl q', 'q.customer_id = u.customer_id', 'Left')
            ->where('u.created_by', $this->session->userdata('user_id'))
            ->group_by('q.side_mark')
            ->having("q.side_mark LIKE '%$customer_name%' ")
            ->get()->result();
        }

        foreach($results as $row) {
            $json[]=array(
                'value'=> $row->id,
                'label'=>$row->name
            );
        }
        echo json_encode($json);
    }

    //    ================ its for get invoice numbers ===============
    public function getInvoiceNumbers($customer_id='') {
        $order_id = $this->input->post("order_id");
        $this->db->select("id, order_id");
        $this->db->from('quatation_tbl');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->where('order_stage!=', 6);
        
        if($customer_id != '') {
            $this->db->where('customer_id', $customer_id);
        } else {
            $this->db->where("order_id LIKE '%$order_id%' ");
        }
        $results =  $this->db->get()->result();
        $options ='<option value="">Select Invoice</option>';
        foreach($results as $row) {
            $json[]=array(
                'value'=> $row->id,
                'label'=>$row->order_id
            );
            $options .='<option value="'.$row->order_id.'">'.$row->order_id.'</option>';
        }
        if($customer_id != '') { 
            echo $options;
        } else {
            echo json_encode($json);
        }
    }

    public function getOrderDetails($order_id='') {
        $data = array();
        if(isset($order_id) && $order_id != '') {
            $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
            $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);
            $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('shipment_data')->row();
            $data['company_profile'] = $this->settings->company_profile();
            $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();
        }
        echo json_encode($data);
    }

    public function addReturnItem()
    {
        $data['order_id'] = $this->input->post('order_id');
        $data['return_qty'] = $this->input->post('request_return_qty');
        $data['return_type'] = $this->input->post('request_return_type');
        $data['return_comments'] = $this->input->post('request_return_comments');

        $data['product_details'] = $this->Order_model->get_product_details_by_id($this->input->post('order_id'), $_POST['product_id']);
        
        $data['return_img'] ='';
        if ($_FILES["request_return_img"]['name'][0] != '') {
            $file_uploadCount = count($_FILES["request_return_img"]['name']);
            for ($j = 0; $j < $file_uploadCount; $j++) {
                $_FILES['file_uploader']['name'] = $_FILES["request_return_img"]['name'][$j];
                $_FILES['file_uploader']['type'] = $_FILES["request_return_img"]['type'][$j];
                $_FILES['file_uploader']['tmp_name'] = $_FILES["request_return_img"]['tmp_name'][$j];
                $_FILES['file_uploader']['error'] = $_FILES["request_return_img"]['error'][$j];
                $_FILES['file_uploader']['size'] = $_FILES["request_return_img"]['size'][$j];

                // configure for upload 
                $config = array(
                    'upload_path' => "./assets/c_level/uploads/customers/return",
                    'allowed_types' => "jpg|png|jpeg|pdf|doc|docx|xls|xlsx",
                    'file_size' => '2048',
                );
                $image_data = array();

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('file_uploader')) {
                    $image_data = $this->upload->data();
                    $return_img .= $image_data['file_name'].',';
                }
            }
            $data['return_img'] = $return_img;
        }

        $itemRow = $this->load->view('c_level/orders/return_request_item', $data, TRUE);
        echo $itemRow;
    }

    //    ========= its for c_customer_order_return_save ==========
    public function c_customer_order_return_save() {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "customer order return information save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        
        $product_id = $this->input->post('product_id');
        $return_qty = $this->input->post('return_qty');
        $return_type = $this->input->post('return_type');
        $return_img = $this->input->post('return_img');
        $return_comments = $this->input->post('return_comments');
        $customer_id = $this->input->post('customer_id');
        $order_id = $this->input->post('order_id');
        $assigned_to = $this->input->post('responsible_staff');

        $emp_email = $this->input->post('emp_email');
        $emp_phone = $this->input->post('emp_phone');
        $emp_date = $this->input->post('emp_date');
        $emp_time = $this->input->post('emp_time');
        $emp_remarks = $this->input->post('emp_remarks');
        
        $is_approved = 0;
        $approved_by = 0;

        $order_return_data = array(
            'order_id' => $order_id,
            'customer_id' => $customer_id,
            'return_date' => date('Y-m-d'),
            'return_comments' => '',//$return_comments,
            'returned_by' => $this->user_id,
            'is_approved' => $is_approved,
            'approved_by' => $approved_by,
            'level_id' => $this->level_id,
            'created_by' => $this->user_id,
            'assigned_to' => $assigned_to,
            'create_date' => date('Y-m-d h:i:s'),
        );
        $check_order_id = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $order_id)->get()->row();
        if ($check_order_id->order_id) {
            $this->db->where('order_id', $order_id)->update('order_return_tbl', $order_return_data);
            $return_id = $check_order_id->return_id;
        } else {
            $this->db->insert('order_return_tbl', $order_return_data);
            $return_id = $this->db->insert_id();
        }
        for ($i = 0; $i < count($product_id); $i++) {            
            $order_return_details = array(
                'return_id' => $return_id,
                'product_id' => $product_id[$i],
                'return_qty' => $return_qty[$i],
                'return_comments' => $return_comments[$i],
                'return_type' => $return_type[$i],
                'return_img' => rtrim($return_img[$i], ','),
            );
            if ($return_qty[$i] != '') {
                $this->db->insert('order_return_details', $order_return_details);
            }
        }
        // Notification: Start
        if($this->session->userdata('isAdmin') == 1){
            $level_id = $this->session->userdata('user_id');
        }else{
            $level_id = $this->session->userdata('user_created_by'); 
        }
        //        dd($c_level_id);
        $appointment_date = $emp_date;
        $appointment_time = $emp_time;
        $created_date = date('Y-m-d');
        $c_level_staff_id = $this->input->post('c_level_staff_id');
        $remarks = "Return request assigned";

        $appointment_data = array(
            'customer_id' => $customer_id,
            'c_level_id' => $level_id,
            'appointment_date' => $appointment_date,
            'appointment_time' => $appointment_time,
            'c_level_staff_id' => $assigned_to,
            'remarks' => $remarks,
            'create_by' => $this->user_id,
            'create_date' => $created_date,
        );
        $this->db->insert('appointment_calendar', $appointment_data);

        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
            
            $customer = $this->db->select("customer_id as id, phone,email, CONCAT((first_name),(' '),(last_name)) as name")->from('customer_info')->where('customer_id', $customer_id)->get()->row();
            $customer_email = $customer->email;

            if ($sms_gateway_info->provider_name == 'Twilio') {
                //1. SMS To Employee
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $emp_phone;
                $message = 'Return request assigned to you.';
                $response = $this->twilio->sms($from, $to, $message);                

                //1. SMS To Customer
                $from = $sms_gateway_info->phone; //'+12062024567';
                $to = $customer->phone;
                $message = 'Your return request is registered successfully.';
                $response = $this->twilio->sms($from, $to, $message);                
            }
            $message = 'Return request assigned to you.';
            $this->Setting_model->sendCstomEmail($emp_email, $message); //1. Email To Employee
            $message = 'Your return request is registered successfully.';
            $this->Setting_model->sendCstomEmail($customer_email, $message); //1. Email To Customer

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Returned successfully!</div>");
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }
        $this->db->insert('c_notification_tbl', $notifications_data);
        // Notification: End        
        redirect("manage-order");
    }
//    ================ its for customer_return ===============
    public function customer_return() {
        $config["base_url"] = base_url('c_level/C_return_controller/customer_return');
        //$config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $this->own_level_returned_order_count();
        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);


        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["customer_return"] = $this->C_return_model->customer_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/c_returns/customer_return');
        $this->load->view('c_level/footer');
    }


    //    ==========its for own user customer count =========
    public function own_level_returned_order_count() {

        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }
        $this->db->where('level_id', $level_id);
        $num_rows = $this->db->count_all_results('order_return_tbl');
        return $num_rows;
    }

    //    =========== its for show_return_resend ===========
    public function show_return_resend() {
        $data['return_id'] = $this->input->post('return_id');
        $data['order_id'] = $this->input->post('order_id');
        $data['returned_by'] = $this->input->post('returned_by');
        $data['order_details'] = $this->db->select('*')
                        ->from('order_return_details a')
                        ->join('product_tbl b', 'b.product_id = a.product_id')
                        ->where('a.return_id', $data['return_id'])
//                                           ->where('a.return_id', $data['product_id'])
                        ->where('a.replace_qty', 0)
                        ->get()->result();


        $this->load->view('c_level/c_returns/show_return_resend_form', $data);
    }



    //    ================ its for supplier_return ===============
    public function raw_material_supplier_return() {

        $total_rows = $this->db->where('created_by',$this->level_id)->count_all_results('raw_material_return_tbl');

        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $config["base_url"] = base_url('c_level/C_return_controller/raw_material_supplier_return');
        $config["total_rows"] = $total_rows;

        $config["per_page"] = 25;
        $config["uri_segment"] = 4;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["get_raw_material_supplier_return"] = $this->C_return_model->get_raw_material_supplier_return($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;

        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/c_returns/supplier_return');
        $this->load->view('c_level/footer');
    }

    //    ============= its for raw_material_return_approved =========
    public function raw_material_return_purchase_approved() {
     
        $return_id = $this->input->post('return_id');
        $raw_material_id = $this->input->post('raw_material_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $return_qty = $this->input->post('return_qty');

        $approve_data = array(
            'is_approved' => 1,
            'approved_by' => $this->user_id,
        );
        $this->db->where('return_id', $return_id);
        $this->db->update('raw_material_return_tbl', $approve_data);

        for ($i = 0; $i < count($raw_material_id); $i++) {
            $raw_material_return_stock_details = array(
//                'return_id' => $return_id,
                'row_material_id' => $raw_material_id[$i],
                'pattern_model_id' => $pattern_model_id[$i],
                'color_id' => $color_id[$i],
                'out_qty' => $return_qty[$i],
                'from_module' => "From Raw Material Return",
                'stock_date' => date('Y-m-d'),
            );
            $this->db->insert('row_material_stock_tbl', $raw_material_return_stock_details);
        }
//        echo "Returned approved successfully!";
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Approved successfully!</div>");
        redirect("raw-material-supplier-return");
    }


    //    ============== its for supplier_return_filter =============
    public function supplier_return_filter() {
        $data['get_supplier'] = $this->Supplier_model->get_supplier();
        $data['invoice_no'] = $this->input->post('invoice_no');
        $data['supplier_name'] = $this->input->post('supplier_name');
//        $material_name = $this->input->post('material_name');
        $data['supplier_return_filter'] = $this->C_return_model->supplier_return_filter($data['invoice_no'], $data['supplier_name']);


        $this->load->view('c_level/header', $data);
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/c_returns/supplier_return_filter');
        $this->load->view('c_level/footer');
    }


    // ================= RMA Code start ========================


    public function rma_view_list(){
        // $this->permission_c->check_label('customer_list')->create()->redirect();
        #pagination starts
        #
        $access_retailer=access_role_permission_page(114);
        $acc=access_role_retailer();
        $config["base_url"] = base_url('rma-view-list');
        $config["total_rows"] = $this->C_return_model->get_return_items_employee_count($this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["get_items_for_employee"] = $this->C_return_model->get_return_items_employee($config["per_page"], $page,$this->user_id);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========

        $data['filter'] = [];
        $data['filter']['first_name'] = $this->input->post('first_name');
        $data['filter']['sidemark'] = $this->input->post('sidemark');
        $data['filter']['phone'] = $this->input->post('phone');
        $data['filter']['address'] = $this->input->post('address');
        $data['filter']['filter_status'] = $this->input->post('filter_status');
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/rma/view_list', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
    }

    public function manage_rma_data(){

        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            if($this->input->post('radio'))
                $return_wholeseller = 1;
            else
                $return_wholeseller = 0;

            $return_detail_id = $this->input->post('hidden_id');
            $customer_id = $this->input->post('hidden_customer_id');
            $product_id = $this->input->post('hidden_product_id');
            $order_id = $this->input->post('hidden_order_id');
            $user_id = $this->user_id; 

            $get_customer_data = $this->db->where('customer_id',$customer_id)->get('customer_info')->row();
            $get_product_data = $this->db->where('product_id',$product_id)->get('product_tbl')->row();
            $get_order_return_details_data = $this->db->where('id',$this->input->post('hidden_id'))->get('order_return_details')->row();

            $r_id = $get_order_return_details_data->return_id;

            if($this->input->post('item_status') == 1)
                $status_item = "Production";
            if($this->input->post('item_status') == 2)
                $status_item = "Shipping";
            if($this->input->post('item_status') == 3)
                $status_item = "Delivered";

            if($this->input->post('hidden_return_type') == 1)
                $item_return_type = "Replace";
            else
                $item_return_type = "Repair";

            $cust_email = $get_customer_data->email;
            // $cust_phno = $get_customer_data->phone;
            $cust_phno = "+1 478 287 1258";
            $cust_name = ucwords($get_customer_data->first_name.' '.$get_customer_data->last_name);

            $bms_logo = base_url()."assets/b_level/uploads/appsettings/BMS-Logos_Decor-150.png";

            // ---------- SMS Code -------------
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();

            if ($sms_gateway_info->provider_name == 'Twilio') {
                $from = $sms_gateway_info->phone;
                $to = $cust_phno;
                $message = "Hello $cust_name your item $get_product_data->product_name for $item_return_type is in $status_item.";

                $response = $this->twilio->sms($from, $to, $message);   
                if($response)
                    $is_sms = 1;
                else
                    $is_sms = 0;             
            }
            // ---------- SMS Code End -------------

            // ---------- Mail code ------------
            $msg = "
                    <table align='center' style='font-family: Nunito,sans-serif;width: 50%;display: flex;margin: 0 auto;'>
                        <tbody style='width:100%;'>
                        <tr style='width: 100%;display: flex;text-align: center;margin: 0 auto;justify-content: center;padding: 10px;'>
                            <th>
                                <img src='$bms_logo' alt='BMS Logo' />
                            </th>
                        </tr>
                        <tr style='padding: 10px;display: flex;width: 100%;background-color: #304e4f;color: #fff;font-size: 18px;justify-content: center;'>
                            <th>Item Repair/Replace Detail</th>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;background-color:#f1f1f1;'>
                            <td style='width: 50%;font-size: 16px;'><b>Customer Name</b></td>
                            <td style='width: 50%;font-size: 16px;'>$cust_name</td>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;'>
                            <td style='width: 50%;font-size: 16px;'><b>Your Item</b></td>
                            <td style='width: 50%;font-size: 16px;'>$get_product_data->product_name</td>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;background-color:#f1f1f1;'>
                            <td style='width: 50%;font-size: 16px;'><b>Item For</b></td>
                            <td style='width: 50%;font-size: 16px;'>$item_return_type is in $status_item</td>
                        </tr>
                        </tbody>
                    </table>
            ";
            
            $mail_response = $this->Setting_model->sendCstomEmail($cust_email, $msg);
            if($mail_response)
                $is_mail = 1;
            else
                $is_mail = 0;  
            // ---------- Mail code End ------------

            // Add data in history table

            $item_return_history = array(
                'return_detail_id' => $return_detail_id,
                'user_id'          => $user_id,
                'customer_id'      => $customer_id,
                'order_id'         => $order_id,
                'product_id'       => $product_id,
                'status'           => $this->input->post('item_status'),
                'is_mail_received' => $is_mail,
                'is_sms_received'  => $is_sms,
                'created_at'       => date('Y-m-d H:i:s')
            );
            $this->C_return_model->insert_return_item_history($item_return_history);
                       
            // Add data in item_return_status table

            $item_return_status = array(
                'return_detail_id'      => $return_detail_id,
                'user_id'               => $user_id,
                'customer_id'           => $customer_id,
                'order_id'              => $order_id,
                'product_id'            => $product_id,
                'status'                => $this->input->post('item_status'),
                'comment'               => $this->input->post('item_comment'),
                'return_to_wholeseller' => $return_wholeseller,
                'created_at'            => date('Y-m-d H:i:s')
            );
            $status_success = $this->C_return_model->insert_return_item_status($item_return_status);

            // Change item return status in order_return_tbl

            $order_return_tbl_return_status_data = array(
                'return_status'      => $this->input->post('item_status')
            );
            $this->C_return_model->change_item_return_status($order_return_tbl_return_status_data,$order_id);

            if($return_wholeseller == 1)
            {
                $wholeseler_return_data = array(
                    'return_detail_id'      => $return_detail_id,
                    'order_id'              => $order_id,
                    'product_id'            => $product_id,
                    'status'                => 0,
                    'comment'               => $this->input->post('item_comment'),
                    'return_by'             => $user_id,
                    'created_at'            => date('Y-m-d H:i:s')
                );
                $this->C_return_model->insert_wholeseler_return_data($wholeseler_return_data);
            }

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Item Status Changed Successfully!</div>");
            redirect("rma-view-list");
        }
        else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
            redirect("rma-view-list");
        }
    }
    public function rma_in_progress_list(){
        // $this->permission_c->check_label('customer_list')->create()->redirect();
        #pagination starts
        #
        $access_retailer=access_role_permission_page(115);
        $acc=access_role_retailer();
        $config["base_url"] = base_url('rma-in-progress-list');
        $config["total_rows"] = $this->C_return_model->get_return_in_progress_items_count($this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["get_in_progress_items"] = $this->C_return_model->get_return_in_progress_items_employee($config["per_page"], $page,$this->user_id);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========

        $data['filter'] = [];
        $data['filter']['first_name'] = $this->input->post('first_name');
        $data['filter']['sidemark'] = $this->input->post('sidemark');
        $data['filter']['phone'] = $this->input->post('phone');
        $data['filter']['address'] = $this->input->post('address');
        $data['filter']['filter_status'] = $this->input->post('filter_status');
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/rma/in_progress_list', $data);
            $this->load->view('c_level/footer');
         }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    }

    public function manage_rma_in_progress_data(){

        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $id = $this->input->post('hidden_ir_id');
            $order_id = $this->input->post('hidden_order_id');

            $return_id_data = $this->C_return_model->get_return_id_data($id);
            $cust_name = ucwords($return_id_data->cust_name);
            $cust_email = $return_id_data->email;
            // $cust_phno = $return_id_data->phone;
            $cust_phno = "+1 478 287 1258";

            if($this->input->post('item_status') == 1)
                $status_item = "Production";
            if($this->input->post('item_status') == 2)
                $status_item = "Shipping";
            if($this->input->post('item_status') == 3)
                $status_item = "Delivered";

            if($this->input->post('hidden_return_type') == 1)
                $item_return_type = "Replace";
            else
                $item_return_type = "Repair";

            $bms_logo = base_url()."assets/b_level/uploads/appsettings/BMS-Logos_Decor-150.png";

            // ---------- SMS Code -------------
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();

            if ($sms_gateway_info->provider_name == 'Twilio') {
                $from = $sms_gateway_info->phone;
                $to = $cust_phno;
                $message = "Hello $cust_name your item $return_id_data->product_name for $item_return_type is in $status_item.";

                $response = $this->twilio->sms($from, $to, $message);              
            }
            // ---------- SMS Code End -------------

            $msg = "
                    <table align='center' style='font-family: Nunito,sans-serif;width: 50%;display: flex;margin: 0 auto;'>
                        <tbody style='width:100%;'>
                        <tr style='width: 100%;display: flex;text-align: center;margin: 0 auto;justify-content: center;padding: 10px;'>
                            <th>
                                <img src='$bms_logo' alt='BMS Logo' />
                            </th>
                        </tr>
                        <tr style='padding: 10px;display: flex;width: 100%;background-color: #304e4f;color: #fff;font-size: 18px;justify-content: center;'>
                            <th>Item Repair/Replace Detail</th>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;background-color:#f1f1f1;'>
                            <td style='width: 50%;font-size: 16px;'><b>Customer Name</b></td>
                            <td style='width: 50%;font-size: 16px;'>$cust_name</td>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;'>
                            <td style='width: 50%;font-size: 16px;'><b>Your Item</b></td>
                            <td style='width: 50%;font-size: 16px;'>$return_id_data->product_name</td>
                        </tr>
                        <tr style='width: 100%;display: flex;padding: 10px;background-color:#f1f1f1;'>
                            <td style='width: 50%;font-size: 16px;'><b>Item For</b></td>
                            <td style='width: 50%;font-size: 16px;'>$item_return_type is in $status_item</td>
                        </tr>
                        </tbody>
                    </table>
            ";
            
            $mail_response = $this->Setting_model->sendCstomEmail($cust_email, $msg);

            $change_data = array(
                'status'    => $this->input->post('item_status'),
                'comment'   => $this->input->post('item_comment')
            );

            $this->C_return_model->change_item_status($change_data,$id);

            // Change item return status in order_return_tbl

            $order_return_tbl_return_status_data = array(
                'return_status'      => $this->input->post('item_status')
            );
            $this->C_return_model->change_item_return_status($order_return_tbl_return_status_data,$order_id);

            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Item Status Changed Successfully!</div>");
            redirect("rma-in-progress-list");
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
            redirect("rma-view-list");
        }
    }

    public function rma_completed_list(){
        // $this->permission_c->check_label('customer_list')->create()->redirect();
        #pagination starts
        #
        $access_retailer=access_role_permission_page(116);
        $acc=access_role_retailer();
        $config["base_url"] = base_url('rma-completed-list');
        $config["total_rows"] = $this->C_return_model->get_return_completed_items_count($this->user_id);
        $config["per_page"] = 25;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["get_completed_items"] = $this->C_return_model->get_return_completed_items_employee($config["per_page"], $page,$this->user_id);

        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========

        $data['filter'] = [];
        $data['filter']['first_name'] = $this->input->post('first_name');
        $data['filter']['sidemark'] = $this->input->post('sidemark');
        $data['filter']['phone'] = $this->input->post('phone');
        $data['filter']['address'] = $this->input->post('address');
        $data['filter']['filter_status'] = $this->input->post('filter_status');
        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/rma/completed_list', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
        }
    }

}
