<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_controller extends CI_Controller
{

    private $user_id = '';
    private $level_id = '';

    public function __construct()
    {

        parent::__construct();
        $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');

        if ($this->session->userdata('isAdmin') == 1) {

            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }

        //if ($session_id == '' || $user_type != 'c') {
        //  redirect('retailer-logout');
        //}

        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('c_level/Customer_model');
        $this->load->model('c_level/User_model');
        $this->load->model('c_level/Order_model');
        $this->load->model('c_level/Setting_model');
        $this->load->model('email_sender');
    }

    public function index()
    { }

        public function get_color_partan_model($product_id)
    {

        $pp = $this->db->select('colors,pattern_models_ids')
            ->where('product_id', $product_id)
            ->get('product_tbl')->row();

        $colors = $pattern_model = array();

        if ($pp->colors) {
            $color_ids = explode(',', $pp->colors);
            $colors = $this->db->where_in('id', $color_ids)->get('color_tbl')->result();
        }

        $patter_ids = explode(',', @$pp->pattern_models_ids);

        $pattern_model = $this->db->where_in('pattern_model_id', @$patter_ids)->order_by("pattern_name", "asc")->get('pattern_model_tbl')->result();

        $q = '';

        $q .= '<div class="row">
                    <label for="" class="col-sm-3">Pattern</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="pattern_model_id" id="pattern_id" data-placeholder="-- select pattern --" required>
                            <option value="">-- select pattern --</option>';

        foreach ($pattern_model as $pattern) {
            $q .= '<option value="' . $pattern->pattern_model_id . '">' . $pattern->pattern_name . '</option>';
        }

        $q .= '</select>
                    </div>
                </div><br/>';

        echo $q;
    }


    public function get_color_model($product_id, $pattern_id)
    {
        $colorIds = $this->db->select('id')->where('pattern_id', $pattern_id)->get('color_tbl')->result_array();
        $colorIds = array_column($colorIds, 'id');                
        $where = "FIND_IN_SET('" . $pattern_id . "', pattern_models_ids)";
        $product_color_data = $this->db->where('product_id', $product_id)->where($where)->get('product_tbl')->row_array();

        if ($product_color_data['colors'] != '') {
            $product_color_ids = explode(',', $product_color_data['colors']);
            $product_color_ids = array_intersect($colorIds,$product_color_ids);
        } else {
            $product_color_ids = '';
        }
        $colors = array();
        $colors = $this->db->where_in('id', $product_color_ids)->get('color_tbl')->result();

        $q = '';

        $q .= '
                <div class="row">
                    <label for="" class="col-sm-3">Color</label>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="color_id" id="color_id" onchange="getColorCode(this.value)"  data-placeholder="-- select one --" required>
                            <option value="">-- select one --</option>';
        foreach ($colors as $color) {
            $q .= '<option value="' . $color->id . '">' . $color->color_name . '</option>';
        }
        $q .= '</select>
                    </div>
                    <div  class="col-sm-2" style="padding:0 5px 0 0;line-height:30px;">Color code</div>
                    <div  class="col-sm-2"><input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control"></div>
                </div>';

        echo $q;
    }

    public function get_color_code($id)
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $or_where = "(created_by = '".$main_b_id."' OR created_by = '".$this->level_id."')";
        $colors = $this->db->where('id', $id)->where($or_where)->get('color_tbl')->row();

        echo $colors->color_number;
    }

    public function get_color_code_select($keyword,$pattern_id = '')
    {

        $main_b_id = $this->session->userdata('main_b_id');
        $or_where = "(created_by = '".$main_b_id."' OR created_by = '".$this->level_id."')";
        $colors = $this->db->where('color_number', $keyword)->where('pattern_id', $pattern_id)->where($or_where)->get('color_tbl')->row();

        echo @$colors->id;
    }

    //------------------------------------------------------
    // Category waise subcategory
    //------------------------------------------------------
     public function category_wise_subcategory($category_id = null)
    {
        $category_wise_subcategory = $this->db->select('*')
            ->where('parent_category', $category_id)
            ->where('created_by', $this->level_id)
            ->get('category_tbl')->result();

        $q = "";
        if (!empty($category_wise_subcategory)) {

            $q .= '<div class="row">
                        <label for="" class="col-sm-3">Select Sub Category</label>

                        <div class="col-sm-6">
                            <select class="form-control select2" name="subcategory_id" id="sub_category_id" data-placeholder="-- select one --">
                                <option value="0">--Select--</option>';

            foreach ($category_wise_subcategory as $value) {
                $q .= "<option value='$value->category_id'>$value->category_name</option>";
            }

            $q .= '</select>
                        </div>
                    </div>';

        } else {
            // echo 'no data found';
        }
        echo $q;
    }

    public function get_product_by_category($category_id)
    {

        $result = $this->db->select('product_id,category_id,product_name')
            ->where('category_id', $category_id)
            ->where('active_status', 1)
            ->get('product_tbl')->result();
        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    //========== its for get_product_by_subcategory ===========
    public function get_product_by_subcategory($subcategory_id)
    {

        $result = $this->db->select('product_id,category_id,product_name')->where('subcategory_id', $subcategory_id)->where('active_status', 1)->get('product_tbl')->result();
        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product) {
            $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
        }
        echo $q;
    }

    public function get_cat_fraction($category_id = 0)
    {
        if ($category_id > 0) {
            // get same fraction
            $hw1 = $this->db->select('*')
                ->where('category_id', $category_id)
                ->limit(1)
                ->get('category_tbl')->row();
            $fracs1 = $hw1->fractions;
            $fracs = explode(",", $fracs1);
            //print_r($fracs);
            $html = '<option>--Select--</option>';

            $hw2 = $this->db->select('*')->order_by('decimal_value', 'asc')->get('width_height_fractions')->result();
            $all_fractions = array();
            foreach ($hw2 as $row) {
                if (in_array($row->fraction_value, $fracs)) {
                    $html .= '<option value="' . $row->id . '">' . $row->fraction_value . '</option>';
                    //  $all_fractions[$row->fraction_value] = $row->decimal_value;
                }
            }
            echo json_encode(array('status' => 1, 'fractions' => $fracs1, 'html' => $html));
        } else {
            echo json_encode(array('status' => 0));
        }
        exit();
    }

    //------------------------------------------------
    //------------------------------------------------
    //      its for new_order
    //------------------------------------------------
    public function new_order()
    {
        $main_b_id = $this->session->userdata('main_b_id');
        $this->permission_c->check_label(5)->create()->redirect();
        $data['get_customer'] = $this->Order_model->get_customer();

        
            $data['get_category'] = $this->Order_model->get_customer_category();
        $access_retailer=access_role_permission_page(5);
        $acc=access_role_retailer();

        // $data['get_patern_model'] = $this->Order_model->get_patern_model();
        // $data['colors'] = $this->db->where('created_by',$main_b_id)->get('color_tbl')->result();

        $data['orderjs'] = "c_level/orders/order_js.php";
        $data['currencys'] = $this->Setting_model->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['binfo'] = $this->db->where('user_id', $main_b_id)->get('company_profile')->row();


        if($this->session->userdata('package_id')==1){
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/upgrade_error');
            $this->load->view('c_level/footer');
       
         }else{
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/orders/new_order', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
         }
    }

    public function get_order_item_cart()
    {
        $data['company_profile'] = $this->Setting_model->company_profile();
        echo $this->load->view('c_level/orders/new_order_cart', $data);
    }

    public function get_order_form()
    {
        $data['cart_rowid'] = $cart_rowid = $this->input->get('row_id');
        $data['view_action'] = $view_action = $this->input->get('action');

        //$data['company_profile'] = $this->Setting_model->company_profile();

        $data['get_customer'] = $this->Order_model->get_customer();


        $user_id = $this->session->userdata('user_id');
        $user_detail = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row();
        if ($user_detail->wholesaler_connection==1) 
        {
            $data['get_category'] = $this->Order_model->get_category();
        }
        if ($user_detail->wholesaler_connection==0) 
        {
            $data['get_category'] = $this->Order_model->get_category(1);
        }



        // $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->where('created_by', $this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->Setting_model->company_profile();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "c_level/orders/order_js.php";

        $win_to_edit = array();
        foreach ($this->cart->contents() as $items) :
            if ($items['rowid'] == $cart_rowid) {
                $win_to_edit = $items;
                break;
            }
        endforeach;
        $data['win_to_edit'] = $win_to_edit;

        $data['get_product_order_info'] = (object) $win_to_edit;
        // echo '<pre>';
        // print_r($win_to_edit);

        $data['selected_attributes'] = $selected_attributes = json_decode($win_to_edit['att_options']);

        // for ($i=0; $i < $data['selected_attributes']; $i++) { 
        //     $options = $this->db->select('attr_options.*')
        //                         ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
        //                         ->where('product_attr_option.id', $data['selected_attributes'][i]->options[0]->option_key_value)
        //                         ->order_by('attr_options.att_op_id', 'ASC')
        //                         ->get('product_attr_option')->row();
        //     $data['selected_attributes'][i]->options[0]->option_type = $options->option_type;
        // }
        // print_r($data['selected_attributes']);
        // exit;
        echo $this->load->view('c_level/orders/order_new_form', $data);
    }

    public function get_new_order_edit()
    {
        $data['cart_rowid'] = $cart_rowid = $this->uri->segment(4);
        $data['view_action'] = $view_action = $this->uri->segment(5);
        $data['get_customer'] = $this->Order_model->get_customer();
        $data['get_category'] = $this->Order_model->get_category();
        $data['get_patern_model'] = $this->Order_model->get_patern_model();
        $data['get_product'] = $this->Order_model->get_product();
        $data['colors'] = $this->db->where('created_by', $this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->Setting_model->company_profile();
        $data['rooms'] = $this->db->get('rooms')->result();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "c_level/orders/order_js.php";

        $data['get_product_order_info'] = $this->db->select('*')->from('qutation_details a')->where('a.row_id', $data['cart_rowid'])->get()->row();
        $data['win_to_edit'] = $data['get_product_order_info'];
        $data['get_quatation_attributes_info'] = $attributes = $this->db->select('*')->from('quatation_attributes a')->where('a.fk_od_id', $data['cart_rowid'])->get()->row();
        $data['selected_attributes'] = json_decode($data['get_quatation_attributes_info']->product_attribute);

        echo $this->load->view('c_level/orders/order_new_form', $data);
        echo $this->load->view('c_level/orders/order_js');
    }

    public function new_order_edit()
    {

        // for single order edit
        //  die('here');
        // $this->load->model('b_level/Order_model');
        error_reporting(E_ALL);
        //  $this->permission->check_label('order')->create()->redirect();

        $row_id = $this->uri->segment(3);

        $edit = $this->uri->segment(4);

        $data['can_edit'] = $edit;

        $data['get_customer'] = $this->Order_model->get_customer();

        $data['get_category'] = $this->Order_model->get_category();

        $data['get_patern_model'] = $this->Order_model->get_patern_model();

        $data['get_product'] = $this->Order_model->get_product();

        //die('here s');
        $data['colors'] = $this->db->where('created_by', $this->level_id)->get('color_tbl')->result();
        $data['company_profile'] = $this->Setting_model->company_profile();

        $data['rooms'] = $this->db->get('rooms')->result();

        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['customerjs'] = "c_level/orders/order_js.php";



        // for edit order start

        $data['get_product_order_info'] = $this->db->select('*')->from('qutation_details a')->where('a.row_id', $row_id)->get()->result()[0];
        //  echo $this->db->last_query();
        $data['get_quatation_attributes_info'] = $attributes = $this->db->select('*')->from('quatation_attributes a')->where('a.fk_od_id', $row_id)->get()->result();
        $product_attributes = json_decode($data['get_quatation_attributes_info'][0]->product_attribute);
        // print_r($data['get_product_order_info']);
        //              foreach ($product_attributes as $single){
        //            echo $single->attribute_id."<br>";
        //        }
        //   die('here');
        $data['order_id'] = $data['get_product_order_info']->order_id;
        $data['category_id'] = $data['get_product_order_info']->category_id;
        $data['product_id'] = $data['get_product_order_info']->product_id;

        //  $data['get_quatation_info'] = $this->db->select('*')->from('quatation_tbl a')->where('a.order_id', $data['order_id'])->get()->result()[0];
        //        echo $product_attributes[0]->attribute_id;
        //         dd($data['order_id']);
        //        foreach ($product_attributes as $single){
        //            echo $single->attribute_id."<br>";
        //        }
        //        dd($data['get_quatation_attributes_info']);
        $product_id = $data['get_product_order_info']->product_id;

        //        ======== its for product id wise patterns info =============
        $get_products_patterns = $this->db->select('*')->from('product_tbl a')
            ->where('a.product_id', $product_id)->get()->result();
        $patterns = $get_products_patterns[0]->pattern_models_ids;
        if ($patterns) {
            $get_pattern_sql = "SELECT * FROM pattern_model_tbl WHERE pattern_model_id IN($patterns)";
            $data['get_patterns'] = $this->db->query($get_pattern_sql)->result();
        }
        //        ======== close ============
        //        ======== its for product id wise color info =============
        $get_products_colors = $this->db->select('*')->from('product_tbl a')
            ->where('a.product_id', $product_id)->get()->result();
        $colors = $get_products_colors[0]->colors;
        if ($colors) {
            $get_colors_sql = "SELECT * FROM color_tbl WHERE id IN($colors)";
            $data['get_colors'] = $this->db->query($get_colors_sql)->result();
        }
        $data['selected_attributes'] = $selected_attributes = json_decode($attributes[0]->product_attribute);

        //dd($data['selected_attributes']);
        //die();
        //die('here');
        // echo "<pre>";
        // print_r($data['get_product_order_info']);
        // print_r($data['selected_attributes']);
        // exit;

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/new_order_edit', $data);
        $this->load->view('c_level/footer');
    }

    function get_customer_product_by_category($category_id = null, $selected_product = null, $forReturn = null)
    {

        $result = $this->Order_model->get_customer_product_by_category($category_id);
        $q = '';
        $q .= '<option value=""></option>';
        foreach ($result as $key => $product) {
            if ($selected_product != null && $selected_product == $product->product_id) {
                $q .= '<option value="' . $product->product_id . '" selected>' . $product->product_name . '</option>';
            } else {
                $q .= '<option value="' . $product->product_id . '">' . $product->product_name . '</option>';
            }
        }
        if ($forReturn == null) {
            echo $q;
        } else {
            return $q;
        }
    }

    public function contri_price($price_type, $option_price, $main_price, $product_id='0')
    {

        $q = '';
        if ($price_type == 1) {
            $price_total = $main_price + @$option_price;
            $contribution_price = (!empty($option_price) ? $option_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $cost_factor_data = $this->Common_retailer_to_customer_commission($product_id);
            $cost_factor_rate = $cost_factor_data['dealer_price'];
            $price_total = ($main_price * $cost_factor_rate * $option_price) / 100;

            // $price_total = ($main_price * $option_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }
        return $q;
    }

    public function Common_retailer_to_customer_commission($product_id) {
        
        if($product_id != ''){
            $product = $this->db->select('costfactor_discount,individual_cost_factor')
                ->where('product_id', $product_id)
                ->where('created_by', $this->user_id)
                ->get('c_cost_factor_tbl')->row();
      
            $commission = [];

            if (!empty($product)) {
                $commission = array('dealer_price' => $product->individual_cost_factor, 'individual_price' => $product->costfactor_discount);
            } else {
                $commission = array('dealer_price' => 1, 'individual_price' => 0);
            }
        }else{
            $commission = array('dealer_price' => 1, 'individual_price' => 0);
        }    

        return $commission;
    }

    public function get_product_to_attribute($product_id = '')
    {
        $q = '';
        if($product_id == '') {
            echo $q;
        }

        $attributes = $this->db->select('product_attribute.*,attribute_tbl.attribute_name,attribute_tbl.attribute_type')
            ->join('attribute_tbl', 'attribute_tbl.attribute_id=product_attribute.attribute_id')
            ->where('product_attribute.product_id', $product_id)
            ->order_by('attribute_tbl.position', 'ASC')
            ->get('product_attribute')->result();

        // for price style
        $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();


        
        $main_price = 0;
        //Discount cost factor
        $discountData = $this->db->select("individual_cost_factor,costfactor_discount")->where('product_id', $product_id)->where('level_id', $this->level_id)->get('c_cost_factor_tbl')->row();
        $q .= '<input type="hidden" name="individual_cost_factor"  value="' . $discountData->individual_cost_factor . '" id="individual_cost_factor" >';
        if ($p->price_style_type == 3) {

            $q .= '<input type="hidden" name="pricestyle"  value="' . $p->price_style_type . '" id="pricestyle" >';
            // Fix price 
            $q .= '<div class="row hidden">
                    <label class="col-sm-3">Fixed Price</label>
                    <div class="col-sm-6">
                        <input type="text" name="main_price" class="form-control" id="main_price" readonly value="' . $p->fixed_price . '" >
                    </div>
                </div><br>';

            $main_price = $p->fixed_price;
        } elseif ($p->price_style_type == 2) {

            // sqr fit price 
            $q .= '<input type="hidden" name="pricestyle"  value="' . $p->price_style_type . '" id="pricestyle" >';
            $q .= '<input type="hidden" name="sqr_price"  value="' . $p->sqft_price . '" id="sqr_price" >';
            $q .= '<div class="row hidden">
                        <label class="col-sm-3">Sqft Price</label>
                        <div class="col-sm-6">
                            <input type="text" name="main_price" class="form-control" id="main_price"  readonly value="' . $p->sqft_price . '" >
                        </div>
                    </div><br>';

            $main_price = $p->sqft_price;
        }


        foreach ($attributes as $attribute) {

            if ($attribute->attribute_type == 3) {

                $options = $this->db->select('attr_options.*,product_attr_option.id,product_attr_option.product_id')
                    ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                    ->where('product_attr_option.pro_attr_id', $attribute->id)
                    ->order_by('attr_options.att_op_id', 'ASC')
                    ->get('product_attr_option')->result();


                $q .= '<div class="row">
                            
                        <label class="col-sm-3">' . $attribute->attribute_name . '</label>
                        <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                        <input type="hidden" name="attribute_value[]" value="">';

                foreach ($options as $op) {

                    // Check if retailer set the upcharge price : START
                    if(@$op->att_op_id && $op->att_op_id != ''){
                        $ret_attr_query = "SELECT * FROM ret_attr_options WHERE att_op_id = '".$op->att_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                        $ret_attr_result = $this->db->query($ret_attr_query)->row();

                        if(@$ret_attr_result->retailer_att_op_id && $ret_attr_result->retailer_att_op_id != '' && $ret_attr_result->retailer_price != ''){
                            $op->price_type = $ret_attr_result->retailer_price_type;
                            $op->price = $ret_attr_result->retailer_price;
                        }
                    }    
                    // Check if retailer set the upcharge price : END


                    $q .= '<div class="col-sm-3">
                                <label>' . $op->option_name . '</label>
                                <input type="hidden" name="op_id_' . $attribute->attribute_id . '[]" value="' . $op->id . '_' . $op->att_op_id . '">';
                    $q .= '<input type="text" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control" required>';
                    $q .= '</div>';
                    $q .= $this->contri_price($op->price_type, $op->price, $main_price,$op->product_id);
                }

                $q .= '<div class="col-sm-6" style="display:none;"></div>';
                $q .= '<div class="col-sm-12"></div>';

                $q .= '</div><br>';
            } elseif ($attribute->attribute_type == 2) {

                $options = $this->db->select('attr_options.*,product_attr_option.id')
                    ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                    ->where('product_attr_option.pro_attr_id', $attribute->id)
                    ->order_by('attr_options.att_op_id', 'ASC')
                    ->get('product_attr_option')->result();

                $q .= '<div class="row">
                            <label class="col-sm-3">' . $attribute->attribute_name . '</label>
                            <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">
                            <input type="hidden" name="attribute_value[]" value="">';

                $q .= '<input type="hidden" name="op_value_' . $attribute->attribute_id . '[]" min="0"  class="form-control">';

                $q .= '<div class="col-sm-4">
                                
                                <select class="form-control op_op_load select2 options_' . $attribute->attribute_id . '" name="op_id_' . $attribute->attribute_id . '[]"  onChange="OptionOptions(this.value,' . $attribute->attribute_id . ')" data-placeholder="-- select pattern/model --" data-attr-name="'.$attribute->attribute_name.'" required>
                                    
                                    <option value="0_0" >--Select one--</option>';
                $sl1 = 1;
                foreach ($options as $op) {
                    $q .= '<option value="' . $op->id . '_' . $op->att_op_id . '" ' . ($sl1 == 1 ? 'selected' : '') . ' >' . $op->option_name . '</option>';
                    $sl1++;
                }
                $q .= '</select></div>';

                $q .= '<div class="col-sm-6" style="display:none;""></div>';
                $q .= '<div class="col-sm-12"></div>';

                $q .= '</div><br>';
            } elseif ($attribute->attribute_type == 1) {

                $height = $attribute->attribute_name;
                $q .= '<div class="row">
                                <lable class="col-sm-3">' . $attribute->attribute_name . '</lable>
                                <input type="hidden" name="attribute_id[]" value="' . $attribute->attribute_id . '">

                                <div class="col-sm-6">
                                    <input type="text" name="attribute_value[]" id="' . $attribute->attribute_name . '" class="form-control op_input_' . $attribute->attribute_id . '" required>
                                </div>
                                
                        </div><br>';
            } else {

                $q .= '';
            }
        }

        echo $q;
    }

    //--------------------------------------------------
    // get product attributes options price
    public function get_product_attr_option_option_price($att_op_id, $main_price)
    {

        // $option = $this->db->where('att_op_id', $att_op_id)->get('attr_options')->row();

        $option = $this->db->select('attr_options.*,product_attr_option.product_id')
            ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
            ->where('attr_options.att_op_id', $att_op_id)
            ->get('product_attr_option')->row();

        //$products = $this->db->where('product_id',$product_id)->get('product_tbl')->row();
        $main_price = (int) $main_price;
        $contribution_price = 0;

        if (!empty($option)) {

            // Check if retailer set the upcharge price : START
            if(@$option->att_op_id && $option->att_op_id != ''){
                $ret_attr_query = "SELECT * FROM ret_attr_options WHERE att_op_id = '".$option->att_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                $ret_attr_result = $this->db->query($ret_attr_query)->row();

                if(@$ret_attr_result->retailer_att_op_id && $ret_attr_result->retailer_att_op_id != '' && $ret_attr_result->retailer_price != ''){
                    $option->price_type = $ret_attr_result->retailer_price_type;
                    $option->price = $ret_attr_result->retailer_price;
                }
            }    
            // Check if retailer set the upcharge price : END

            if ($option->price_type == 1) {

                $price_total = $main_price + @$option->price;
                $prices = '<p>' . $main_price . '+' . @$option->price . '=' . $price_total . '</p>';
                $contribution_price = (!empty($option->price) ? $option->price : 0);
            } else {

                $cost_factor_data = $this->Common_retailer_to_customer_commission($option->product_id);
                $cost_factor_rate = $cost_factor_data['dealer_price'];

                $price_total = ($main_price * $cost_factor_rate * $option->price) / 100;

                $price_total = ($main_price * $option->price) / 100;
                $prices = '<p>(' . $main_price . '*' . @$option->price . ')/100 = ' . ($main_price + $price_total) . '</p>';
                $contribution_price = (!empty($price_total) ? $price_total : 0);
            }

            $q = '';
            //option price
            $q .= '<input type="text" readonly name="option_price_' . $option->attribute_id . '" value="' . @$option->price . '" class="form-control">' . @$prices;
            //------
            $q .= '<input type="hidden" name="option_id_' . $option->attribute_id . '" value="' . @$option->att_op_id . '" class="form-control">';
            $q .= '<input type="hidden" name="option_value_' . $option->attribute_id . '" value="" class="form-control">';
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            echo $q;
        } else {
            $q = '';
        }
    }

    public function get_product_attr_option_option($pro_att_op_id, $attribute_id, $main_price, $individual_cost_factor = 0)
    {

        $options = $this->db->select('attr_options.*,product_attr_option.product_id')
            ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
            ->where('product_attr_option.id', $pro_att_op_id)
            ->order_by('attr_options.att_op_id', 'ASC')
            ->get('product_attr_option')->row();

        $category_id = '';
        // Get product category base on product id : START 
        if(isset($options->product_id) && $options->product_id != ''){
            $product_data = $this->db->where('product_id',$options->product_id)->limit(1)->get('product_tbl')->row();
            if(isset($product_data->category_id) && $product_data->category_id != ''){
                $category_id = $product_data->category_id;
            }
        }   
        // Get product category base on product id : END 

        $q = '';

        // Check if retailer set the upcharge price : START
        if(@$options->att_op_id && $options->att_op_id != ''){
            $ret_attr_query = "SELECT * FROM ret_attr_options WHERE att_op_id = '".$options->att_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
            $ret_attr_result = $this->db->query($ret_attr_query)->row();

            if(@$ret_attr_result->retailer_att_op_id && $ret_attr_result->retailer_att_op_id != '' && $ret_attr_result->retailer_price != ''){
                $options->price_type = $ret_attr_result->retailer_price_type;
                $options->price = $ret_attr_result->retailer_price;
            }
        }    
        // Check if retailer set the upcharge price : END

        if ($options->price_type == 1) {
            $price_total = $main_price + @$options->price;
            $contribution_price = (!empty($options->price) ? $options->price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {
            if($individual_cost_factor != 0) {
                $disPrice = ($main_price * ($individual_cost_factor*100) / 100);
                $price_total = round((($disPrice * $options->price) / 100), 2);
                $contribution_price = (!empty($price_total) ? $price_total : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            } else {

                $cost_factor_data = $this->Common_retailer_to_customer_commission($options->product_id);
                $cost_factor_rate = $cost_factor_data['dealer_price'];

                $price_total = ($main_price * $cost_factor_rate * $options->price) / 100;

                // $price_total = ($main_price * $options->price) / 100;
                $contribution_price = (!empty($price_total) ? $price_total : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
            }
        }




        if (@$options->option_type == 5) {
            // Text + Fraction

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id,product_attr_option_option.product_id')
                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                ->get('product_attr_option_option')->result();

            // $fractions = $this->db->get('width_height_fractions')->result();

            // Get fraction category wise : START
            $fraction_option = '';
            if($category_id != ''){
                $hw1 = $this->db->select('*')->where('category_id', $category_id)->limit(1)->get('category_tbl')->row();
                $fracs1 = $hw1->fractions;
                $fracs = explode(",", $fracs1);
                $hw2 = $this->db->select('*')->order_by('decimal_value', 'asc')->get('width_height_fractions')->result();
                foreach ($hw2 as $row) {
                    if (in_array($row->fraction_value, $fracs)) {
                        $fraction_option .= '<option value="' . $row->id . '">' . $row->fraction_value . '</option>';
                    }
                }
            }    
            // Get fraction category wise : END

            foreach ($opops as $kk=>$op_op) {

                // Check if retailer set the upcharge price : START
                if(@$op_op->op_op_id && $op_op->op_op_id != ''){
                    $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = '".$op_op->op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                    $ret_attr_result = $this->db->query($ret_attr_query)->row();

                    if(@$ret_attr_result->retailer_op_op_id && $ret_attr_result->retailer_op_op_id != '' && $ret_attr_result->retailer_att_op_op_price != ''){
                        $op_op->att_op_op_price_type = $ret_attr_result->retailer_att_op_op_price_type;
                        $op_op->att_op_op_price = $ret_attr_result->retailer_att_op_op_price;
                    }
                }    
                // Check if retailer set the upcharge price : END

                $q .= '<br><div class="row">

                            <label class="col-sm-3">' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <div class="col-sm-4">
                                <input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control convert_text_fraction" data-op_op_key="'.$kk.'" required>
                            </div>';

                $q .= '<div class="col-sm-2">
                                <select class="form-control select_text_fraction key_text_fraction_'.$kk.'" name="fraction_' . $attribute_id . '[]" id=""  data-placeholder="-- select one --">
                                    <option value="">-- select one --</option>';

                // foreach ($fractions as $f) {
                //     $q .= '<option value=' . $f->fraction_value . '>' . $f->fraction_value . '</option>';
                // }
                $q .= $fraction_option;

                $q .= '</select></div>';
                $q .= '<div class="col-sm-6" style="display:none;"></div>';
                $q .= '<div class="col-sm-12"></div>';
                $q .= '</div>';
                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price,$op_op->product_id);
            }
        } elseif ($options->option_type == 4) {
            // Multi option
            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id,product_attr_option_option.product_id')
                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                ->get('product_attr_option_option')->result();

            foreach ($opops as $op_op) {

                $opopops = $this->db->where('attribute_id', $attribute_id)->where('att_op_op_id', $op_op->op_op_id)->get('attr_options_option_option_tbl')->result();

                $q .= '<input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">';

                if ($op_op->type == 2) {

                    $q .= '<div class="row"><label class="col-sm-3">' . $op_op->op_op_name . '</label>
                                <select class="form-control select2 col-sm-4" id="mul_op_op_id' . $op_op->op_op_id . '" onChange="multiOptionPriceValue(this.value)" data-placeholder="-- select pattern/model --" required>';
                    
                    // If option name is Control Position then remove select option : START
                    $option_vals = '';
                    if($op_op->op_op_name != 'Control Position') {
                        $q .= '<option value="" >--Select one--</option>';
                    }   
                    // If option name is Control Position then remove select option : END

                    foreach ($opopops as $keyss=>$opopop) {
                        
                        $selected = '';
                        // If option name is right then store value in variable : START
                        if($keyss == 0){
                            $option_vals = $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id;
                        }
                        if($op_op->op_op_name == 'Control Position' && $opopop->att_op_op_op_name == 'Right'){
                            $option_vals = $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id;
                            $selected = 'selected';
                        }
                        // If option name is right then store value in variable : END

                        $q .= '<option value="' . $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id . '" '.$selected.'>' . $opopop->att_op_op_op_name . '</option>';
                    }
                    $q .= '</select>';

                    // If option name is Control Position and option_vals is not null then call function using jquery : START
                    if($op_op->op_op_name == 'Control Position' && $option_vals != '') {
                        $q .= '<script>multiOptionPriceValue("'.$option_vals.'")</script>';
                    }  
                    // If option name is Control Position and option_vals is not null then call function using jquery : END

                    $q .= '</div>'; 
                } elseif ($op_op->type == 1) {

                    $q .= '<div class="row"><label class="col-sm-3">' . $op_op->op_op_name . '</label>';
                    $q .= '<input type="text" name="op_op_value_' . $attribute_id . '[]" value="" class="form-control col-sm-4" required>';
                    $q .= '<div class="col-sm-6" style="display:none;"></div>';
                    $q .= '<div class="col-sm-12"></div>';
                    $q .= '</div>';
                }elseif ($op_op->type == 6) {

                    // Check if retailer set the upcharge price : START
                    if(@$op_op->op_op_id && $op_op->op_op_id != ''){
                        $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = '".$op_op->op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                        $ret_attr_result = $this->db->query($ret_attr_query)->row();

                        if(@$ret_attr_result->retailer_op_op_id && $ret_attr_result->retailer_op_op_id != '' && $ret_attr_result->retailer_att_op_op_price != ''){
                            $op_op->att_op_op_price_type = $ret_attr_result->retailer_att_op_op_price_type;
                            $op_op->att_op_op_price = $ret_attr_result->retailer_att_op_op_price;
                        }
                    }    
                    // Check if retailer set the upcharge price : END

                    // For multiselect in multi option
                    $q .= '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';
                    $q .= '<br><div class="row"><label class="col-sm-3">' . $op_op->op_op_name . '</label>
                                <div class="col-sm-4 multi_select_div"><select class="form-control custom_multi_select selectpicker" id="mulselect_op_op_op_id_' . $op_op->op_op_id . '" onChange="MultiOptionOptionsOptionOptions(this,' . $attribute_id . ')" data-placeholder="-- select option --" required name="op_op_op_id_' . $attribute_id . '[]" multiple ></div>';
                    
                    foreach ($opopops as $keyss=>$opopop) {
                        $q .= '<option value="' . $opopop->att_op_op_op_id . '_' . $attribute_id . '_' . $op_op->op_op_id . '">' . $opopop->att_op_op_op_name . '</option>';
                    }
                    $q .= '</select>';
                    $q .= '</div>';

                    $q .= '<div class="col-sm-6" style="display:none;"></div>';
                    $q .= '<div class="col-sm-12"></div>';

                    $q .= '</div><br>';
                }

                $q .= '<div class="col-sm-12" style="display:none;" >
                                <div class="col-sm-12"></div>
                            </div><br>';

                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price,$op_op->product_id);
            }
        } elseif (@$options->option_type == 3) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id,product_attr_option_option.product_id')
                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                ->get('product_attr_option_option')->result();


            foreach ($opops as $op_op) {

                // Check if retailer set the upcharge price : START
                if(@$op_op->op_op_id && $op_op->op_op_id != ''){
                    $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = '".$op_op->op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                    $ret_attr_result = $this->db->query($ret_attr_query)->row();

                    if(@$ret_attr_result->retailer_op_op_id && $ret_attr_result->retailer_op_op_id != '' && $ret_attr_result->retailer_att_op_op_price != ''){
                        $op_op->att_op_op_price_type = $ret_attr_result->retailer_att_op_op_price_type;
                        $op_op->att_op_op_price = $ret_attr_result->retailer_att_op_op_price;
                    }
                }    
                // Check if retailer set the upcharge price : END

                $q .= '<br><div class="row">
                            <label class="col-sm-3">' . $op_op->op_op_name . '</label>
                            <input type="hidden" name="op_op_id_' . $attribute_id . '[]" value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">
                            <div class="col-sm-4">
                            <input type="text" name="op_op_value_' . $attribute_id . '[]"  class="form-control" required></div>
                        ';
                $q .= '<div class="col-sm-6" style="display:none;"></div>';
                $q .= '<div class="col-sm-12"></div>';

                $q .= '</div>';
                $q .= $this->contri_price($op_op->att_op_op_price_type, $op_op->att_op_op_price, $main_price,$op_op->product_id);
            }
        } elseif (@$options->option_type == 2) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                ->get('product_attr_option_option')->result();

            //$q .= '<input type="hidden" name="op_id_'.$attribute_id.'[]" value="' . $options->att_op_id. '" class="form-control">';
            $q .= '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<br><div class="row">
                        <label class="col-sm-3 mobile_hidden"></label>
                        <div class="col-sm-4"><select class="form-control select2" id="op_' . $options->att_op_id . '" name="op_op_id_' . $attribute_id . '[]"  onChange="OptionOptionsOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --" required>
                                    <option value="0">--Select one--</option>';

            foreach ($opops as $op_op) {
                $q .= '<option value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">' . $op_op->op_op_name . '</option>';
            }

            $q .= '</select></div>';

            $q .= '<div class="col-sm-6" style="display:none;"></div>';
            $q .= '<div class="col-sm-12"></div>';

            $q .= '</div><br>';
        } elseif (@$options->option_type == 1) {

            $q .= '<br>
                        <div class="row">
                            <label class="col-sm-3 mobile_hidden"></label>
                            <input type="text" name="op_value_' . $attribute_id . '[]" class="form-control col-sm-4" required>';
            $q .= '<div class="col-sm-6" style="display:none;"></div>';
            $q .= '<div class="col-sm-12"></div>';
            $q .= '</div><br>';
        } elseif (@$options->option_type == 6) {

            $opops = $this->db->select('attr_options_option_tbl.*,product_attr_option_option.id')
                ->join('attr_options_option_tbl', 'attr_options_option_tbl.op_op_id=product_attr_option_option.op_op_id')
                ->where('product_attr_option_option.pro_att_op_id', $pro_att_op_id)
                ->order_by('attr_options_option_tbl.op_op_id', 'ASC')
                ->get('product_attr_option_option')->result();

            $q .= '<input type="hidden" name="op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<br><div class="row">
                        <label class="col-sm-3 mobile_hidden"></label>
                        <div class="col-sm-4 multi_select_div"><select class="form-control custom_multi_select selectpicker " id="op_' . $options->att_op_id . '" name="op_op_id_' . $attribute_id . '[]"  onChange="MultiOptionOptionsOption(this,' . $attribute_id . ')" data-placeholder="-- select pattern/model --" multiple>';

            foreach ($opops as $op_op) {
                $q .= '<option value="' . $op_op->op_op_id . '_' . $op_op->id . '_' . $options->att_op_id . '">' . $op_op->op_op_name . '</option>';
            }

            $q .= '</select></div>';

            $q .= '<div class="col-sm-6" style="display:none;"></div>';
            $q .= '<div class="col-sm-12"></div>';

            $q .= '</div><br>';
        } else {

            $q .= '';
        }

        echo $q;
    }

    public function get_product_attr_op_op_op($op_op_id, $pro_att_op_op_id, $attribute_id, $main_price)
    {

        $opop = $this->db->where('op_op_id', $op_op_id)->get('attr_options_option_tbl')->row();
        
        //$opopop = $this->db->where('att_op_op_op_id',$op_op_op_id)->get('attr_options_option_option_tbl')->row();
        $product_attr_data = $this->db->where('op_op_id', $op_op_id)->limit(1)->get('product_attr_option_option')->row();

        //dd($opop);

        // Check if retailer set the upcharge price : START
        if(@$opop->op_op_id && $opop->op_op_id != ''){
            $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = '".$opop->op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
            $ret_attr_result = $this->db->query($ret_attr_query)->row();

            if(@$ret_attr_result->retailer_op_op_id && $ret_attr_result->retailer_op_op_id != '' && $ret_attr_result->retailer_att_op_op_price != ''){
                $opop->att_op_op_price_type = $ret_attr_result->retailer_att_op_op_price_type;
                $opop->att_op_op_price = $ret_attr_result->retailer_att_op_op_price;
            }
        }    
        // Check if retailer set the upcharge price : END

        $q = '';
        if ($opop->att_op_op_price_type == 1) {

            $price_total = $main_price + @$opop->att_op_op_price;
            $contribution_price = (!empty($opop->att_op_op_price) ? $opop->att_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            if(isset($product_attr_data->product_id)){
                $cost_factor_data = $this->Common_retailer_to_customer_commission($product_attr_data->product_id);
                $cost_factor_rate = $cost_factor_data['dealer_price'];
            }else{
                $cost_factor_rate = 1;
            }    
            $price_total = ($main_price * $cost_factor_rate * @$opop->att_op_op_price) / 100;

            // $price_total = ($main_price * @$opop->att_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        // $q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';

        if ($opop->type == 4) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*,product_attr_option_option_option.product_id')
                ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                ->get('product_attr_option_option_option')->result();
            $opopop = $this->db->query("select * from attr_options_option_option_tbl where att_op_op_id = $op_op_id")->result();
            foreach ($opopop as $op_op_op) {
                //dd($opopop);

                // Check if retailer set the upcharge price : START
                if(@$op_op_op->att_op_op_op_id && $op_op_op->att_op_op_op_id != ''){
                    $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = '".$op_op_op->att_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                    $ret_attr_result = $this->db->query($ret_attr_query)->row();

                    if(@$ret_attr_result->retailer_att_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_price != ''){
                        $op_op_op->att_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_price_type;
                        $op_op_op->att_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_price;
                    }
                }    
                // Check if retailer set the upcharge price : END
                
                $q .= '<input type="hidden" name="op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">';
                
                if ($op_op_op->att_op_op_op_type == 2) {
                    $opopopops = $this->db->where('attribute_id', $attribute_id)->where('op_op_op_id', $op_op_op->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();
                    
                    $q .= '<div class="row"><label class="col-sm-3">' . $op_op_op->att_op_op_op_name . '</label>
                                <div class="col-sm-3"><select class="form-control" id="op_op_op_op_id_' . $op_op_op->att_op_op_op_id . '" name="op_op_op_op_id_' . $attribute_id . '[]" required>
                                    <option value="" >--Select one--</option>';
                    foreach ($opopopops as $opopopop) {
                        $q .= '<option value="' . $opopopop->att_op_op_op_op_id . '_' . $attribute_id . '_' . $op_op_op->att_op_op_op_id . '">' . $opopopop->att_op_op_op_op_name . '</option>';
                    }
                    $q .= '</select></div></div>';
                } elseif ($op_op_op->att_op_op_op_type == 1) {

                    $q .= '<br><div class="row">
                            <label class="col-sm-3">' . $op_op_op->att_op_op_op_name . '</label>
                            <div class="col-sm-3"><input type="text" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control" style="width:100%;" required></div>
                        </div>';
                }
                $q .= $this->contri_price($op_op_op->att_op_op_op_price_type, $op_op_op->att_op_op_op_price, $main_price,$product_attr_data->product_id);
            }
        } else if ($opop->type == 3) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                ->get('product_attr_option_option_option')->result();

            foreach ($opopop as $op_op_op) {

                // Check if retailer set the upcharge price : START
                if(@$op_op_op->att_op_op_op_id && $op_op_op->att_op_op_op_id != ''){
                    $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = '".$op_op_op->att_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                    $ret_attr_result = $this->db->query($ret_attr_query)->row();

                    if(@$ret_attr_result->retailer_att_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_price != ''){
                        $op_op_op->att_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_price_type;
                        $op_op_op->att_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_price;
                    }
                }    
                // Check if retailer set the upcharge price : END

                $q .= '<br><div class="row">
                            <label class="col-sm-3">' . $op_op_op->att_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">
                            <div class="col-sm-3"><input type="text" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control" required></div>
                        </div>';

                $q .= $this->contri_price($op_op_op->att_op_op_op_price_type, $op_op_op->att_op_op_op_price, $main_price,$product_attr_data->product_id);
            }
        } elseif ($opop->type == 2) {

            $opopop = $this->db->select('product_attr_option_option_option.id,attr_options_option_option_tbl.*')
                ->join('attr_options_option_option_tbl', 'attr_options_option_option_tbl.att_op_op_op_id=product_attr_option_option_option.op_op_op_id')
                ->where('product_attr_option_option_option.attribute_id', $attribute_id)
                ->where('product_attr_option_option_option.pro_att_op_op_id', $pro_att_op_op_id)
                ->order_by('attr_options_option_option_tbl.att_op_op_op_id', 'ASC')
                ->get('product_attr_option_option_option')->result();

            $q .= '<br><input type="hidden" name="op_op_op_value_' . $attribute_id . '[]"  class="form-control">
                        <div class="col-sm-4 offset-sm-3"><select class="form-control select2 " id="op_op_' . $op_op_id . '" name="op_op_op_id_' . $attribute_id . '[]" onChange="OptionOptionsOptionOption(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --" required>
                                <option value="0">--Select one--</option>';

            foreach ($opopop as $op_op_op) {
                $q .= '<option value="' . $op_op_op->att_op_op_op_id . '_' . $op_op_id . '">' . $op_op_op->att_op_op_op_name . '</option>';
            }

            $q .= '</select></div><br>';

            $q .= '<div>
                         <div class="col-sm-12" style="margin-top:-7px;"></div>
                    </div><br>';
        } elseif ($opop->type == 1) {

            $q .= '<br>
                        <input type="text" name="op_op_value_' . $attribute_id . '[]" class="form-control" required>
                    
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function get_product_attr_op_op_op_multiselect($attribute_id, $main_price)
    {

        $multi_val = json_decode(stripslashes($_POST['multi_val']));
        $q = '';
        foreach($multi_val as $kk=> $select_mul_val){
            $final_val = explode("_", $select_mul_val);
            $op_op_id = $final_val[0];
            $pro_att_op_op_id = $final_val[1];
            
            $opop = $this->db->where('op_op_id', $op_op_id)->get('attr_options_option_tbl')->row();

            // Check if retailer set the upcharge price : START
            if(@$opop->op_op_id && $opop->op_op_id != ''){
                $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = '".$opop->op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                $ret_attr_result = $this->db->query($ret_attr_query)->row();

                if(@$ret_attr_result->retailer_op_op_id && $ret_attr_result->retailer_op_op_id != '' && $ret_attr_result->retailer_att_op_op_price != ''){
                    $opop->att_op_op_price_type = $ret_attr_result->retailer_att_op_op_price_type;
                    $opop->att_op_op_price = $ret_attr_result->retailer_att_op_op_price;
                }
            }    
            // Check if retailer set the upcharge price : END

            if ($opop->att_op_op_price_type == 1) {

                $price_total = $main_price + @$opop->att_op_op_price;
                $contribution_price = (!empty($opop->att_op_op_price) ? $opop->att_op_op_price : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price cls_multi_select_option" data-select-mul-option="'.$opop->op_op_name.'">';
            } else {

                if(!isset($product_attr_data->product_id)){
                    $product_attr_data = $this->db->where('op_op_id', $op_op_id)->limit(1)->get('product_attr_option_option')->row();

                    if(isset($product_attr_data->product_id)){
                        $cost_factor_data = $this->Common_retailer_to_customer_commission($product_attr_data->product_id);
                        $cost_factor_rate = $cost_factor_data['dealer_price'];
                    }else{
                        $cost_factor_rate = 1;
                    }    
                }
                $price_total = ($main_price * $cost_factor_rate * @$opop->att_op_op_price) / 100;

                // $price_total = ($main_price * @$opop->att_op_op_price) / 100;
                $contribution_price = (!empty($price_total) ? $price_total : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price cls_multi_select_option" data-select-mul-option="'.$opop->op_op_name.'">';
            }
        }    

        echo $q;
    }

    public function get_product_attr_op_op_op_op_multiselect($attribute_id, $main_price)
    {

        $multi_val = json_decode(stripslashes($_POST['multi_val']));
        $q = '';
        foreach($multi_val as $kk=> $select_mul_val){
            $final_val = explode("_", $select_mul_val);
            $att_op_op_op_id = $final_val[0];
            
            $opopop = $this->db->where('att_op_op_op_id', $att_op_op_op_id)->get('attr_options_option_option_tbl')->row();

            // Check if retailer set the upcharge price : START
            if(@$opopop->att_op_op_op_id && $opopop->att_op_op_op_id != ''){
                $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = '".$opopop->att_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                $ret_attr_result = $this->db->query($ret_attr_query)->row();

                if(@$ret_attr_result->retailer_att_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_price != ''){
                    $opopop->att_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_price_type;
                    $opopop->att_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_price;
                }
            }    
            // Check if retailer set the upcharge price : END

            if ($opopop->att_op_op_op_price_type == 1) {

                $price_total = $main_price + @$opopop->att_op_op_op_price;
                $contribution_price = (!empty($opopop->att_op_op_op_price) ? $opopop->att_op_op_op_price : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price cls_multi_select_option" data-select-mul-option="'.$opopop->att_op_op_op_name.'">';
            } else {

                if(!isset($product_attr_data->product_id)){
                    $product_attr_data = $this->db->where('op_op_op_id', $att_op_op_op_id)->limit(1)->get('product_attr_option_option_option')->row();

                    if(isset($product_attr_data->product_id)){
                        $cost_factor_data = $this->Common_retailer_to_customer_commission($product_attr_data->product_id);
                        $cost_factor_rate = $cost_factor_data['dealer_price'];
                    }else{
                        $cost_factor_rate = 1;
                    }    
                }

                $price_total = ($main_price * $cost_factor_rate * @$opopop->att_op_op_op_price) / 100;

                // $price_total = ($main_price * @$opopop->att_op_op_op_price) / 100;
                $contribution_price = (!empty($price_total) ? $price_total : 0);
                $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price cls_multi_select_option" data-select-mul-option="'.$opopop->att_op_op_op_name.'">';
            }
        }    

        echo $q;
    }

    public function get_product_attr_op_op_op_op($op_op_op_id, $attribute_id, $main_price)
    {

        $opopop = $this->db->where('att_op_op_op_id', $op_op_op_id)->where('attribute_id', $attribute_id)->get('attr_options_option_option_tbl')->row();

        $opopop_data = $this->db->where('op_op_op_id', $op_op_op_id)->limit(1)->get('product_attr_option_option_option')->row();

        $q = '';

        // if ($opopop->att_op_op_op_price_type == 1) {
        //     $price_total = $main_price + @$opopop->att_op_op_op_price;
        //     $contribution_price = (!empty($opopop->att_op_op_op_price) ? $opopop->att_op_op_op_price : 0);
        //     $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        // } else {
        //     $price_total = ($main_price * @$opopop->att_op_op_op_price) / 100;
        //     $contribution_price = (!empty($price_total) ? $price_total : 0);
        //     $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        // }

        // Check if retailer set the upcharge price : START
        if(@$opopop->att_op_op_op_id && $opopop->att_op_op_op_id != ''){
            $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = '".$opopop->att_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
            $ret_attr_result = $this->db->query($ret_attr_query)->row();

            if(@$ret_attr_result->retailer_att_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_price != ''){
                $opopop->att_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_price_type;
                $opopop->att_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_price;
            }
        }    
        // Check if retailer set the upcharge price : END

        $q .= $this->contri_price($opopop->att_op_op_op_price_type, $opopop->att_op_op_op_price, $main_price,$opopop_data->product_id);

        //$q .= '<input type="hidden" name="op_op_price_'.$attribute_id.'[]" value="' . $contribution_price . '" class="form-control">';
        if ($opopop->att_op_op_op_type == 3) {

            $opopopop = $this->db->select('*')
                ->where('attribute_id', $attribute_id)
                ->where('op_op_op_id', $op_op_op_id)
                ->order_by('op_op_op_id', 'ASC')
                ->get('attr_op_op_op_op_tbl')->result();

            foreach ($opopopop as $op_op_op_op) {

                // Check if retailer set the upcharge price : START
                if(@$op_op_op_op->att_op_op_op_op_id && $op_op_op_op->att_op_op_op_op_id != ''){
                    $ret_attr_query = "SELECT * FROM ret_attr_op_op_op_op_tbl WHERE att_op_op_op_op_id = '".$op_op_op_op->att_op_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
                    $ret_attr_result = $this->db->query($ret_attr_query)->row();

                    if(@$ret_attr_result->retailer_att_op_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_op_price != ''){
                        $op_op_op_op->att_op_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_op_price_type;
                        $op_op_op_op->att_op_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_op_price;
                    }
                }    
                // Check if retailer set the upcharge price : END

                $q .= '<br><div class="row">
                            <label class="col-sm-3">' . $op_op_op_op->att_op_op_op_op_name . '</label>
                            <input type="hidden" name="op_op_op_op_id_' . $attribute_id . '[]" value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">
                            <div class="col-sm-3"><input type="text" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control" required></div>
                        </div>';

                $q .= $this->contri_price($op_op_op_op->att_op_op_op_op_price_type, $op_op_op_op->att_op_op_op_op_price, $main_price,$opopop_data->product_id);
            }
        } elseif ($opopop->att_op_op_op_type == 2) {

            $opopopop = $this->db->select('*')
                ->where('attribute_id', $attribute_id)
                ->where('op_op_op_id', $op_op_op_id)
                ->order_by('op_op_op_id', 'ASC')
                ->get('attr_op_op_op_op_tbl')->result();

            $q .= '<input type="hidden" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<select class="form-control select2 " id="op_op_op_' . $op_op_op_id . '"  name="op_op_op_op_id_' . $attribute_id . '[]" onChange="OptionFive(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --" required>
                    <option value="0">--Select one--</option>';
            foreach ($opopopop as $op_op_op_op) {
                $q .= '<option value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">' . $op_op_op_op->att_op_op_op_op_name . '</option>';
            }
            $q .= '</select><br>';

            $q .= '<div>
                        <div class="col-sm-12"></div>
                    </div><br>';
        } elseif ($opopop->att_op_op_op_type == 1) {

            $q .= '<div class="col-sm-12">
                        <input type="text" name="op_op_op_value_' . $attribute_id . '[]" class="form-control" required>
                    </div>
                <br>';
        } else {
            $q .= '';
        }

        echo $q;
    }

    public function multioption_price_value($op_op_op_id, $attribute_id, $main_price)
    {

        $opopopop = $this->db->select('*')
            ->where('attribute_id', $attribute_id)
            ->where('att_op_op_op_id', $op_op_op_id)
            ->get('attr_options_option_option_tbl')->row();


        $q = '';

        // Check if retailer set the upcharge price : START
        if(@$opopopop->att_op_op_op_id && $opopopop->att_op_op_op_id != ''){
            $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = '".$opopopop->att_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
            $ret_attr_result = $this->db->query($ret_attr_query)->row();

            if(@$ret_attr_result->retailer_att_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_price != ''){
                $opopopop->att_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_price_type;
                $opopopop->att_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_price;
            }
        }    
        // Check if retailer set the upcharge price : END

        if ($opopopop->att_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_price) ? $opopopop->att_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            $product_attr_data = $this->db->where('op_op_op_id', $op_op_op_id)->limit(1)->get('product_attr_option_option_option')->row();

            if(isset($product_attr_data->product_id)){
                $cost_factor_data = $this->Common_retailer_to_customer_commission($product_attr_data->product_id);
                $cost_factor_rate = $cost_factor_data['dealer_price'];
            }else{
                $cost_factor_rate = 1;
            }  
            $price_total = ($main_price * $cost_factor_rate * @$opopopop->att_op_op_op_price) / 100;

            // $price_total = ($main_price * @$opopopop->att_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        // $q .= '<br><input type="text" name="op_op_value_' . $attribute_id . '[]" value=\'' . $opopopop->att_op_op_op_name . '\'  class="form-control" required>';

        // For new fifth option for revert please uncomment above line  and remove this code : START
        $q .= '<br><input type="hidden" name="op_op_value_' . $attribute_id . '[]" value=\'' . $opopopop->att_op_op_op_name . '\' class="form-control">';

        if ($opopopop->att_op_op_op_type == 2) {

            $opopopop = $this->db->select('*')
                ->where('attribute_id', $attribute_id)
                ->where('op_op_op_id', $op_op_op_id)
                ->order_by('op_op_op_id', 'ASC')
                ->get('attr_op_op_op_op_tbl')->result();

            $q .= '<input type="hidden" name="op_op_op_op_value_' . $attribute_id . '[]"  class="form-control">';

            $q .= '<div class="row fifth_attr_row"><label class="col-sm-3"></label><select class="form-control select2 col-sm-4" id="op_op_op_' . $op_op_op_id . '"  name="op_op_op_op_id_' . $attribute_id . '[]" onChange="OptionFive(this.value,' . $attribute_id . ')" data-placeholder="-- select pattern/model --">
                    <option value="0">--Select one--</option>';
            foreach ($opopopop as $op_op_op_op) {
                $q .= '<option value="' . $op_op_op_op->att_op_op_op_op_id . '_' . $op_op_op_id . '">' . $op_op_op_op->att_op_op_op_op_name . '</option>';
            }
            $q .= '</select>';

            $q .= '<div class="col-sm-6" style="display:none;"></div><div class="col-sm-12"></div><br>';
            $q .= '<div>';
        }

        // For new fifth option : END 


        echo $q;
    }

    public function get_product_attr_op_five($att_op_op_op_op_id, $attribute_id, $main_price)
    {

        $opopopop = $this->db->select('*')
            ->where('attribute_id', $attribute_id)
            ->where('att_op_op_op_op_id', $att_op_op_op_op_id)
            ->get('attr_op_op_op_op_tbl')->row();

        $q = '';

        // Check if retailer set the upcharge price : START
        if(@$opopopop->att_op_op_op_op_id && $opopopop->att_op_op_op_op_id != ''){
            $ret_attr_query = "SELECT * FROM ret_attr_op_op_op_op_tbl WHERE att_op_op_op_op_id = '".$opopopop->att_op_op_op_op_id."' AND created_by = '".$this->user_id."' LIMIT 1";
            $ret_attr_result = $this->db->query($ret_attr_query)->row();

            if(@$ret_attr_result->retailer_att_op_op_op_op_id && $ret_attr_result->retailer_att_op_op_op_op_id != '' && $ret_attr_result->retailer_att_op_op_op_op_price != ''){
                $opopopop->att_op_op_op_op_price_type = $ret_attr_result->retailer_att_op_op_op_op_price_type;
                $opopopop->att_op_op_op_op_price = $ret_attr_result->retailer_att_op_op_op_op_price;
            }
        }    
        // Check if retailer set the upcharge price : END

        if ($opopopop->att_op_op_op_op_price_type == 1) {

            $price_total = $main_price + @$opopopop->att_op_op_op_op_price;
            $contribution_price = (!empty($opopopop->att_op_op_op_op_price) ? $opopopop->att_op_op_op_op_price : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        } else {

            // For Cost Factor : START
            $product_attr_data =  $this->db->where('op_op_op_id', $opopopop->op_op_op_id)->limit(1)->get('product_attr_option_option_option')->row();

            if(isset($product_attr_data->product_id)){
                $cost_factor_data = $this->Common_retailer_to_customer_commission($product_attr_data->product_id);
                $cost_factor_rate = $cost_factor_data['dealer_price'];
            }else{
                $cost_factor_rate = 1;
            } 
            $price_total = ($main_price * $cost_factor_rate * @$opopopop->att_op_op_op_op_price) / 100;  
            // For Cost Factor : END 

            // $price_total = ($main_price * @$opopopop->att_op_op_op_op_price) / 100;
            $contribution_price = (!empty($price_total) ? $price_total : 0);
            $q .= '<input type="hidden" value="' . $contribution_price . '" class="form-control contri_price">';
        }

        if ($opopopop->att_op_op_op_op_type == 1) {

            $q .= '<br><input type="text" name="op_op_op_op_value_' . $attribute_id . '" class="form-control" required>';
        } else {

            $q .= '';
        }

        echo $q;
    }

    //-----------------------------------------------
    //-----------------------------------------------
    //      Get product price
    //-----------------------------------------------

    public function get_product_row_col_price($height = NULL, $width = NULL, $product_id = NULL, $pattern_id = NULL)
    {

        $q = "";
        $st = "";
        $row = "";
        $col = "";
        $prince = "";

        if ($height >= 0 && !empty($width)) {

            $p = $this->db->where('product_id', $product_id)->get('product_tbl')->row();


            if (!empty($p->price_style_type) && $p->price_style_type == 1) {

                $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                    ->where('row', $width)
                    ->where('col', $height)
                    ->get('price_style')->row();

                $pc = ($prince != NULL ? $prince->price : 0);

                if (!empty($prince)) {

                    $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';
                    $st = 1;

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = $pc;
                } else {

                    $prince = $this->db->where('style_id', $p->price_rowcol_style_id)
                        ->where('row >=', $width)
                        ->where('col >=', $height)
                        ->order_by('row_id', 'asc')
                        ->limit(1)
                        ->get('price_style')->row();


                    $pc = ($prince != NULL ? $prince->price : 0);

                    $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                    $row = $prince->row;
                    $col = $prince->col;
                    $prince = $pc;
                    $st = 2;
                }
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 2) {

                $prince = $p->sqft_price;
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 3) {

                $prince = $p->fixed_price;
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 4) {

                $pg = $this->db->select('*')->from('price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();


                $price = $this->db->where('style_id', $pg->group_id)
                    ->where('row >=', $width)
                    ->where('col >=', $height)
                    ->order_by('row_id', 'asc')
                    ->limit(1)
                    ->get('price_style')->row();


                $pc = ($price != NULL ? $price->price : 0);

                $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $pc . '" >
                            </div>
                        </div>';

                $row = $price->row;
                $col = $price->col;

                $prince = $pc;
                $st = 2;
            } elseif (!empty($p->price_style_type) && $p->price_style_type == 5) {

                $pg = $this->db->select('*')->from('sqm_price_model_mapping_tbl')->where('product_id', $product_id)->where('pattern_id', $pattern_id)->get()->row();
                $pc = ($pg->price != NULL ? $pg->price : 0);


                $total_area = $height * $width;
                $sqm = ($total_area / 10000) * ($pc);
                $prince = $sqm;

                $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="' . $prince . '" >
                            </div>
                        </div>';

                $st = 2;
            }

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);
        } else {
            $q .= '<div class="row hidden">
                            <label class="col-sm-3">Row/Col Price</label>
                            <div class="col-sm-6">
                                <input type="number" name="price" class="form-control" id="main_price" readonly value="0" >
                            </div>
                        </div>';
            $st = 1;

            $row = $prince->row;
            $col = $prince->col;
            $prince = 0;

            $arr = array('ht' => $q, 'st' => $st, 'row' => $row, 'col' => $col, 'prince' => $prince);
            echo json_encode($arr);
        }
    }


    //-----------------------------------------------
    //------------------------------------------
    //  Product add to cart
    //-------------------------------------------
    public function add_to_cart()
    {
        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');
        $attrib = [];

        foreach ($attributes as $key => $att) {

            $attributes_type = $this->db->select('attribute_tbl.attribute_type')
                ->where('attribute_tbl.attribute_id', $att)
                ->get('attribute_tbl')
                ->row()->attribute_type;

            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];


            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {
                foreach ($ops as $key => $op) {
                    $option_type = $this->db->select('attr_options.option_type')
                        ->where('attr_options.att_op_id', explode('_', $op)[1])
                        ->get('attr_options')->row()->option_type;

                    if ($option_type == 1) {
                        $opval = $option_value[1];
                    } else {
                        $opval = $option_value[$key];
                    }

                    $options[] = array(
                        'option_type' => $option_type,
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $opval,
                        'option_key_value' => $op, //added by itsea, previously there was no value saving of attributes drop down, so added this
                    );
                }
            }

            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);
            $fraction = $this->input->post('fraction_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key] . ' ' . $fraction[$key],
                        'option_key_value' => $opop,
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key],
                        'option_key_value' => $opopop,
                    );
                }
            }


            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }


            $attrib[] = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'attributes_type' => $attributes_type,
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }

        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $price = $this->input->post('price');
        $h_w_price = $this->input->post('h_w_price');
        $upcharge_price = $this->input->post('upcharge_price');
        $total_price = $this->input->post('total_price');
        $notes = $this->input->post('notes');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $room = $this->input->post('room');
        $cart_row_id = $this->input->post('update_product');

        $product = $this->db->select('product_id,product_name,category_id,subcategory_id,dealer_price,individual_price')
            ->where('product_id', $product_id)
            ->get('product_tbl')->row();


        // $id = $product->product_id .$pattern_model_id.$color_id. $width . $height . $total_price;
   
        // For generate unique id for cart : Insys START 
        $option_cart_data = $this->input->post();
        $id= '';
    
        // For get post data and store in to varibale
        foreach($option_cart_data as $kk=> $cart_option){
            if(!is_array($cart_option)){
                $id .= str_replace(' ', '-', $cart_option);
            }
        }
        
        $attr_arr = [];
        // For get post attribute array data and store in to varibale
        if(isset($option_cart_data['attribute_id'])  && !empty($option_cart_data['attribute_id'])){
            foreach($option_cart_data['attribute_id'] as $ka=> $cart_atr_option){
                $id .= $cart_atr_option;
                $attr_arr[] = $cart_atr_option;
            }
        }    

        // For get post attribute value data and store in to varibale
        if(isset($option_cart_data['attribute_value'])  && !empty($option_cart_data['attribute_value'])){
            foreach($option_cart_data['attribute_value'] as $ka=> $cart_atr_option){
                $id .= $cart_atr_option;
            }
        }    

        // For get post option value and id data and store in to varibale
        if(count($attr_arr) > 0){
            foreach($attr_arr as $kaa=> $attr_arr_option){
                $option_var = "op_value_".$attr_arr_option;
                $option_val_var = "op_id_".$attr_arr_option;

                $option_option_var = "op_op_value_".$attr_arr_option;
                $option_option_val_var = "op_op_id_".$attr_arr_option;
                
                $fraction_var = "fraction_".$attr_arr_option;

                $id .= isset($option_cart_data[$option_var][0])?$option_cart_data[$option_var][0]:'';
                $id .= isset($option_cart_data[$option_val_var][0])?$option_cart_data[$option_val_var][0]:'';

                $id .= isset($option_cart_data[$option_option_var][0])?$option_cart_data[$option_option_var][0]:'';
                $id .= isset($option_cart_data[$option_option_val_var][0])?$option_cart_data[$option_option_val_var][0]:'';
                $id .= isset($option_cart_data[$option_option_var][1])?$option_cart_data[$option_option_var][1]:'';
                $id .= isset($option_cart_data[$option_option_val_var][1])?$option_cart_data[$option_option_val_var][1]:'';

                $id .= isset($option_cart_data[$fraction_var][0])?$option_cart_data[$fraction_var][0]:'';
                $id .= isset($option_cart_data[$fraction_var][1])?$option_cart_data[$fraction_var][1]:'';

            }
        }

        $id = str_replace(' ', '_', $id);
        $id = preg_replace('/[^A-Za-z0-9\-]/', '_', $id);
        // For generate unique id for cart : Insys END 


        $product_name = str_replace('&quot;', '"', str_replace("/", ".", $product->product_name));
        // $product_name = $this->db->escape($product->product_name);
        // if ($this->cart->total_items() > 0) {
        //     foreach ($this->cart->contents() as $items) {
        //         if (explode(' ', $items['room'])[0] == $room) {
        //             $room_cnt++;
        //         }
        //     }
        //     $room_name = $room.' '.$room_cnt;
        // }else{
        //     $room_name = $room.' '.$room_cnt;
        // }

        // echo "<pre>";print_r($attrib);die;

        $cartData = array(
            'id' => $id,
            'product_id' => $product->product_id,
            'room' => $room,
            'name' => $product_name,
            'price' => $total_price,
            'h_w_price' => $h_w_price,
            'upcharge_price' => $upcharge_price,
            'qty' => 1,
            'dealer_price' => $product->dealer_price,
            'individual_price' => $product->individual_price,
            'category_id' => $category_id,
            'pattern_model_id' => $pattern_model_id,
            'color_id' => $color_id,
            'width' => $width,
            'height' => $height,
            'width_fraction_id' => $width_fraction_id,
            'height_fraction_id' => $height_fraction_id,
            'notes' => $notes,
            'row_status' => 1,
            'att_options' => json_encode($attrib)
        );
        if ($cart_row_id != "") {
            $cartData['rowid'] = $cart_row_id;
            $pro = $this->cart->update($cartData);
            echo 2;
        } else {
            $pro = $this->cart->insert($cartData);
            echo 1;
        }
    }

    // END
    // ------------------------------------
    //-------------------------------------
    // Clear Cart 
    // ------------------------------------
    public function clear_cart()
    {
        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Cleare successfully! </div>");
        redirect('new-quotation');
    }

    public function clear_cart_new()
    {

        $this->cart->destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Clear successfully! </div>");
        redirect('new-order');
    }

    // END--------------------------------
    //---------------------------------------
    // Delete Cart item
    // --------------------------------------
    public function delete_cart_item($rowid)
    {
        $this->cart->remove($rowid);
        echo 1;
    }

    //--------------------------------------
    //-------------------------------------

    // public function getproductcomission($product_id, $customer_id) {

    //     // Get retailer cost factor 
    //     $product = $this->db->select('individual_cost_factor,costfactor_discount')
    //         ->where('product_id', $product_id)
    //         ->where('created_by', $this->user_id)
    //         ->get('c_cost_factor_tbl')->row();

    //     if (!empty($product)) {
    //         // If retailer cost factor then assign 
    //         $commission = array(
    //             'individual_price' => $product->costfactor_discount
    //         );
    //     } else {

    //         // Get wholesaler cost factor 
    //         $product = $this->db->select('dealer_cost_factor,individual_cost_factor')
    //         ->join('customer_info c', 'b.customer_id = c.customer_id')
    //         ->where('b.product_id', $product_id)
    //         ->where('c.customer_user_id', $this->user_id)
    //         ->get('b_cost_factor_tbl b')->row();

    //         if (!empty($product)) {
    //             // If wholesaler cost factor then assign 
    //             $individual_price = 100 - ($product->dealer_cost_factor * 100);

    //             $commission = array(
    //                 'individual_price' => $individual_price
    //             );
    //         }else{
    //             // Get product default cost factor 
    //             $product = $this->db->select('dealer_price,individual_price')
    //             ->where('product_id', $product_id)
    //             ->get('product_tbl')->row();

    //             $commission = array(
    //                 'individual_price' => $product->individual_price
    //             );
    //         }
    //     }

    //     echo json_encode($commission);
    // }

    public function getproductcomission($product_id, $customer_id) {

        $product = $this->db->select('individual_cost_factor,costfactor_discount')
            ->where('product_id', $product_id)
            ->where('created_by', $this->user_id)
            ->get('c_cost_factor_tbl')->row();

        if (!empty($product)) {
            $commission = array(
                'individual_price' => $product->costfactor_discount
            );
        } else {
            $commission = array(
                'individual_price' => 0
            );
        }
        echo json_encode($commission);
    }


    public function getproductcomission_new($product_id, $customer_id)
    {

        $product = $this->db->select('dealer_cost_factor,individual_cost_factor')
            ->where('product_id', $product_id)
            ->where('customer_id', $customer_id)
            ->get('b_cost_factor_tbl')->row();
        $commission = [];

        if (!empty($product)) {
            $commission = array('dealer_price' => $product->dealer_cost_factor, 'individual_price' => $product->individual_cost_factor);
        } else {

            $product = $this->db->select('dealer_price,individual_price')
                ->where('product_id', $product_id)
                ->get('product_tbl')->row();

            $commission = array('dealer_price' => $product->dealer_price, 'individual_price' => $product->individual_price);
        }

        echo json_encode($commission);
    }

    #-----------------------------------
    #   order save
    #----------------------------------

    public function save_order()
    {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "inserted";
        $remarks = "order information inserted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $products = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $orderid = $this->input->post('orderid');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');


        if (!empty($this->input->post('synk_status'))) {
            $synk_status = 1;
        } else {
            $synk_status = 0;
        }

        $barcode_img_path = '';

        if (!empty($orderid)) {
            $this->load->library('barcode/br_code');
            $barcode_img_path = 'assets/barcode/c/' . $orderid . '.jpg';
            file_put_contents($barcode_img_path, $this->br_code->gcode($orderid));
        }


        if (@$_FILES['file_upload']['name']) {

            $config['upload_path'] = './assets/b_level/uploads/file/';
            $config['allowed_types'] = 'jpeg|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['overwrite'] = false;
            $config['max_size'] = 4800;
            $config['remove_spaces'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file_upload')) {

                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>" . $error . "</div>");
                redirect("new-order");
            } else {

                $data = $this->upload->data();
                $upload_file = $config['upload_path'] . $data['file_name'];
            }
        } else {
            @$upload_file = '';
        }


        $list_price = $this->input->post('list_price');

        $is_different_shipping = ($this->input->post('different_address') != NULL ? 1 : 0);
        $different_shipping_address = ($is_different_shipping == 1 ? $this->input->post('shippin_address') : '');


        $orderData = array(
            'order_id' => $orderid,
            'order_date' => $this->input->post('order_date'),
            'customer_id' => $this->input->post('customer_id'),
            'is_different_shipping' => $is_different_shipping,
            'different_shipping_address' => $different_shipping_address,
            'level_id' => $this->level_id,
            'side_mark' => $this->input->post('side_mark'),
            'upload_file' => $upload_file,
            'barcode' => @$barcode_img_path,
            'state_tax' => $this->input->post('tax'),
            'shipping_charges' => 0,
            'installation_charge' => $this->input->post('install_charge'),
            'other_charge' => $this->input->post('other_charge'),
            'misc' => $this->input->post('misc'),
            'invoice_discount' => $this->input->post('invoice_discount'),
            'grand_total' => $this->input->post('grand_total'),
            'subtotal' => $this->input->post('subtotal'),
            'paid_amount' => 0,
            'due' => $this->input->post('grand_total'),
            'order_status' => $this->input->post('order_status'),
            'created_by' => $this->user_id,
            'updated_by' => '',
            'created_date' => date('Y-m-d'),
            'updated_date' => '',
            'synk_status' => $synk_status
        );




        $order_status = $this->input->post('order_status');
        $insertData = $this->db->insert('quatation_tbl', $orderData);

        if ($insertData) {

            $order_detail = array();
            foreach ($products as $key => $product_id) {

                // Check created By
                $this->db->select('*');
                $this->db->from('category_tbl');
                $this->db->where('category_id', $category[$key]);
                $q = $this->db->get();
                $cat_detail = $q->row();

                if ($cat_detail->created_status == 1) {
                    $created_status = 1;
                } else {
                    $created_status = 0;
                }
                // Check created By   


                $productData = array(
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'room' => $room[$key],
                    'product_qty' => $qty[$key],
                    'list_price' => $list_price[$key],
                    'discount' => $discount[$key],
                    'unit_total_price' => $utprice[$key],
                    'category_id' => $category[$key],
                    'pattern_model_id' => $pattern_model[$key],
                    'color_id' => $color[$key],
                    'width' => $width[$key],
                    'height' => $height[$key],
                    'height_fraction_id' => $height_fraction_id[$key],
                    'width_fraction_id' => $width_fraction_id[$key],
                    'notes' => $notes[$key],
                    'created_status' => $created_status
                );


                //order details
                $this->db->insert('qutation_details', $productData);

                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $orderid,
                    'product_id' => $product_id,
                    'product_attribute' => $attributes[$key]
                );

                //order attributes
                $this->db->insert('quatation_attributes', $attrData);
                $order_detail[] = $productData;
            }

            //order transfer email
            $transfer_email = $this->input->post('transfer_email');
            if ($transfer_email == 1) {
                $transfer_email = $this->input->post('order_trns_email');
                $this->Order_model->transfer_order_email($orderData, $order_detail,$transfer_email);
            }
            //order transfer email

            
            // send email for customer
            $this->Order_model->send_link($orderData);
            //-----------------------
            // clear the cart session
            $this->cart->destroy();

            if ($synk_status == 1) {
                //$this->synk_data_to_b($order_id);
                redirect('retailer-synk-data-wholesaler/' . $orderid);
            }

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order successfully! </div>");
            redirect('retailer-order-view/' . $orderid);

            //}
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Internul error please try again</div>");
            redirect("new-quotation");
        }
    }

    public function send_confirmation_email($stage_id,$order_id)
    {
        $order_data = $this->db->select('*')->from('quatation_tbl')->where('order_id',$order_id)->get()->row();
        $order_data = (array)$order_data;
        $is_send = $this->Order_model->order_confirm_email($order_data);

        if ($is_send==true) 
        {
            $this->db->where('order_id', $order_data['order_id']);
            $this->db->update('quatation_tbl', array('order_stage'=>$stage_id));
        }
    }

    public function save_order_update()
    {
        // for single order update
        //      $this->permission->check_label('order')->create()->redirect();
        //get all attribiutes data using previous add to cart logic
        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');

        $attrib = [];

        foreach ($attributes as $key => $att) {

            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];

            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {

                foreach ($ops as $key => $op) {

                    $options[] = array(
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $option_value[$key]
                    );
                }
            }

            //$op_op_id = explode('_', $this->input->post('op_op_id_' . $att))[0];
            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);
            $fraction = $this->input->post('fraction_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key] . ' ' . $fraction[$key]
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            //$op_op_op_id = explode('_', $this->input->post('op_op_op_id_' . $att))[0];
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key]
                    );
                }
            }


            //$op_op_op_op_id = explode('_', $this->input->post('op_op_op_op_id_' . $att))[0];
            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }


            $attrib = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }

        // dd($attrib);
        // die('hete');
        // add to cart logic end




        $quotation_details_row_id = $this->input->post('quotation_details_row_id');

        $product_id = $this->input->post('product_id');
        //   $qty = $this->input->post('qty');
        //  $list_price = $this->input->post('list_price');
        $discount = $this->input->post('discount');
        $utprice = $this->input->post('utprice');
        $tprice = $this->input->post('tprice');

        $orderid = $this->input->post('order_id');

        $attributes = $this->input->post('attributes');
        $category = $this->input->post('category_id');
        $pattern_model = $this->input->post('pattern_model_id');
        $color = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $notes = $this->input->post('notes');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $room = $this->input->post('room');


        if ($this->session->userdata('isAdmin') == 1) {
            $level_id = $this->session->userdata('user_id');
        } else {
            $level_id = $this->session->userdata('admin_created_by');
        }


        //   foreach ($products as $key => $product_id) {

        $productData = array(
            //   'order_id' => $orderid,
            'room' => $room,
            'product_id' => $product_id,
            //  'product_qty' => $qty,
            //   'list_price' => $list_price,
            'list_price' => $tprice,
            //   'discount' => $discount,
            'unit_total_price' => $tprice,
            'category_id' => $category,
            'pattern_model_id' => $pattern_model,
            'color_id' => $color,
            'width' => $width,
            'height' => $height,
            'height_fraction_id' => $height_fraction_id,
            'width_fraction_id' => $width_fraction_id,
            'notes' => $notes,
        );

        //  $this->db->insert('b_level_qutation_details', $productData);
        $this->db->where('row_id', $quotation_details_row_id);
        $this->db->update('qutation_details', $productData);
        // $fk_od_id = $this->db->insert_id();
        //        echo $this->db->last_query();
        //   die('here');
        $attrData = array(
            //   'fk_od_id' => $fk_od_id,
            'order_id' => $orderid,
            'product_id' => $product_id,
            'product_attribute' => $attrib
        );

        //  $this->db->insert('b_level_quatation_attributes', $attrData);

        $this->db->where('fk_od_id', $quotation_details_row_id);
        $this->db->update('quatation_attributes', $attrData);
        // }
        //        echo $this->db->last_query();
        //        die('here');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Updated! </div>");
        redirect('retailer-invoice-receipt/' . $orderid);
    }

    public function order_window_delete($row_id)
    {
        // echo 'first';
        $row_id = $this->uri->segment(3);
        // $row_id = $this->uri->segment(3);

        $result = $this->db->select('order_id,unit_total_price')
            ->where('row_id', $row_id)->get('qutation_details')->row();
        $order_id = $result->order_id;
        $unit_total_price = $result->unit_total_price;
        // echo $this->db->last_query();
        $result2 = $this->db->select('subtotal')
            ->where('order_id', $order_id)->get('quatation_tbl')->row();
        $subtotal = $result2->subtotal;
        //  echo $this->db->last_query();
        //  echo '$row_id ' . $row_id;
        //  echo 'order ' . $order_id;
        //  echo '<br> unit price ' . $unit_total_price;
        //  echo '<br> subtotal ' . $subtotal;
        // die('here');
        if ($subtotal == $unit_total_price) {
            $this->db->query("delete from qutation_details where row_id = '$row_id'");
            //      $this->db->query("delete from qutation_details where order_id = '" . $order_id . "'");
            //    echo $this->db->last_query();
            //  die('here');
            redirect("manage-order");
        } else {
            $this->db->query("delete from qutation_details where row_id = '$row_id'");
            $newval = $subtotal - $unit_total_price;
            if ($newval > 0) {
                $this->db->query("update quatation_tbl set subtotal='$newval', grand_total='$newval', due = '$newval'  where order_id = '" . $order_id . "'");
            } else {
                $this->db->query("update quatation_tbl set subtotal='0', grand_total='0', due = '0'  where order_id = '" . $order_id . "'");
            }
            //  echo $this->db->last_query();
            //  die('here');
            redirect("retailer-invoice-receipt/' . $result->order_id.");
        }
    }

    public function synk_data_to_b($order_id = NULL)
    {

        $action_page = $this->uri->segment(2);
        $action_done = "synchronize";
        $remarks = "synchronize to b level order";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $query = $this->db->select('*')
            ->from('customer_info')
            ->where('level_id', 1)
            ->where('customer_user_id', $this->user_id)
            ->order_by('customer_id', 'desc')
            ->get()->row();



        if (empty($query->customer_id)) {

            return 1;
        }

        if (isset($order_id) && !empty($order_id)) {

            $orderd = $this->Order_model->get_orderd_by_id($order_id);
            $order_details = $this->Order_model->get_orderd_details_by_id($order_id);


            $this->load->model('Common_model');
            $passenger_id = $this->Common_model->generate_order_id($query->customer_id, 'b_level_qutation_details', 'order_id', 'id');

            $g_order_id = $passenger_id;
            $productData = [];
            $grand_total = 0;

            $barcode_img_path = '';

            if (!empty($g_order_id)) {
                $this->load->library('barcode/br_code');
                $barcode_img_path = 'assets/barcode/b/' . $g_order_id . '.jpg';
                file_put_contents($barcode_img_path, $this->br_code->gcode($g_order_id));
            }



            $attrData = [];
            $productData = [];

            foreach ($order_details as $value) {

                $product = $this->db->select('dealer_price')->where('product_id', $value->product_id)->get('product_tbl')->row();
                $product_cost_f = $this->db->select('dealer_cost_factor')->where('product_id', $value->product_id)->get('b_cost_factor_tbl')->row();
                $proAtt = $this->db->where('fk_od_id', $value->row_id)->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                if (isset($product_cost_f) && !empty($product_cost_f)) {

                    if ($product->dealer_price <= $product_cost_f->dealer_cost_factor) {
                        $cost_f = $product_cost_f->dealer_cost_factor;
                    } else {
                        $cost_f = $product->dealer_price;
                    }
                } else {
                    $cost_f = @$product->dealer_price;
                }

                $discount = ($value->list_price / 100) * @$cost_f;
                $unit_total_price = $value->list_price - @$discount;
                $grand_total += $unit_total_price;

                $productData = array(
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_qty' => $value->product_qty,
                    'list_price' => $value->list_price,
                    'discount' => @$cost_f,
                    'unit_total_price' => $unit_total_price,
                    'category_id' => $value->category_id,
                    'pattern_model_id' => $value->pattern_model_id,
                    'color_id' => $value->color_id,
                    'width' => $value->width,
                    'height' => $value->height,
                    'height_fraction_id' => $value->height_fraction_id,
                    'width_fraction_id' => $value->width_fraction_id,
                    'notes' => $value->notes
                );

                $this->db->insert('b_level_qutation_details', $productData);


                $fk_od_id = $this->db->insert_id();

                $attrData = array(
                    'fk_od_id' => $fk_od_id,
                    'order_id' => $g_order_id,
                    'product_id' => $value->product_id,
                    'product_attribute' => $proAtt->product_attribute
                );

                $this->db->insert('b_level_quatation_attributes', $attrData);
            }



            $orderData = array(
                'order_id' => $g_order_id,
                'clevel_order_id' => $order_id,
                'order_date' => $orderd->order_date,
                'customer_id' => $query->customer_id,
                'is_different_shipping' => $orderd->is_different_shipping,
                'different_shipping_address' => $orderd->different_shipping_address,
                'level_id' => $this->user_id,
                'side_mark' => $orderd->side_mark,
                'upload_file' => $orderd->upload_file,
                'barcode' => @$barcode_img_path,
                'state_tax' => 0,
                'shipping_charges' => 0,
                'installation_charge' => 0,
                'other_charge' => 0,
                'misc' => 0,
                'invoice_discount' => 0,
                'grand_total' => $grand_total,
                'subtotal' => $grand_total,
                'paid_amount' => 0,
                'due' => $grand_total,
                'order_status' => $orderd->order_status,
                'created_by' => $orderd->created_by,
                'updated_by' => '',
                'created_date' => $orderd->created_date,
                'updated_date' => '',
                'status' => 0
            );


            $this->db->insert('b_level_quatation_tbl', $orderData);

            $this->db->set('synk_status', 1)->where('order_id', $order_id)->update('quatation_tbl');

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order synk successfully! </div>");
            return 1;
        } else {

            return 1;
        }
    }

    #-------------------------------------------
    #   get side mark and sales tax
    #-------------------------------------------

    public function customer_wise_sidemark($customer_id = null)
    {


        $customer_sidemark = $this->db->select('*')
            ->from('customer_info')
            ->where('customer_id', $customer_id)
            ->get()->row();


        $sales_tax = $this->db->select('tax_rate')
            ->from('c_us_state_tbl')
            ->where('shortcode', $customer_sidemark->state)
            ->where('level_id', $this->level_id)
            ->get()->row();


        // Generate New order ID : START 
        $this->load->model('Common_model');
        $passenger_id = $this->Common_model->generate_order_id($customer_id, 'quatation_tbl', 'order_id', 'id');

        $final_passenger_id->order_id = $passenger_id;
        // Generate New order ID : END                

        $n = (object) $new_arr = array_merge((array) $customer_sidemark, (array) $sales_tax, (array) $final_passenger_id);

        echo json_encode($n);
    }

    #--------------------------------------
    #---------------------------------
    #   get sales tax by c state
    #---------------------------------

    public function different_shipping_tax()
    {

        $val = $this->input->post('vs');
        $state = explode(',', $val);
        $different_state_tax = $this->db->select('tax_rate')
            ->from('c_us_state_tbl')
            ->where('state_name', $state[1])
            ->where('level_id', $this->level_id)
            ->get()->row();  
        echo json_encode($different_state_tax);
    }

    public function order_id_generate($customer_id = '')
    {

        $this->load->model('Common_model');
        $passenger_id = $this->Common_model->generate_order_id($customer_id, 'quatation_tbl', 'order_id', 'id');
        echo $passenger_id;
    }

    // ============= its for random key generator ============
    public function random_keygenerator($mode = null, $len = null)
    {
        $result = "";
        if ($mode == 1) :
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 2) :
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        elseif ($mode == 3) :
            $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        elseif ($mode == 4) :
            $chars = "0123456789";
        endif;
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    //    =========== its for quotation_edit ===============
    public function quotation_edit()
    {

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/quotation_edit');
        $this->load->view('c_level/footer');
    }

    //    =========== its for manage_order ===============
    public function manage_order()
    {

        $this->permission_c->check_label_multi(array(6, 48))->create()->redirect();
       // $access_retailer=access_role_permission_page(6);
       //  $acc=access_role_retailer();
        $search = (object) array(
            'order_id' => $this->input->post('order_id'),
            'customer_id' => $this->input->post('customer_id'),
            'search_daterange' => $this->input->post('search_daterange'),
            'order_stage' => $this->input->post('order_stage'),
            'level_id' => $this->level_id,
            'user_id' => ''
        );



        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();

        $data['company_profile'] = $this->Setting_model->company_profile();

        $data['b_level_company_info'] = $this->db->where('id', $this->session->userdata('admin_created_by'))->get('user_info')->row_array();
        //$total_rows = $this->db->where('quatation_tbl.order_stage!=', 6)->get('quatation_tbl')->num_rows();
        // $total_rows = $this->Order_model->order_row_count($search);

        $total_rows = $this->total_rows($search);

        $per_page = 20;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $url = base_url('manage-order');

        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page, $url);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['orderd'] = $this->Order_model->get_all_orderd($search, $per_page, $page);
        $data['search'] = $search;
        if($this->session->userdata('package_id')==1){
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
       
    }else{
           // if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/orders/manage_order', $data);
                $this->load->view('c_level/footer');
         /*   }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            } */
         }
    }

    function pagination($total_rows, $per_page, $page, $url)
    {


        $config["base_url"] = $url;
        //$config["total_rows"] = $this->db->count_all('customer_info');
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = 2;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        /* ends of bootstrap */

        // $config["base_url"] = base_url('manage-order');
        // $config["total_rows"] = $total_rows;
        // $config["per_page"] = $per_page;
        // $config["uri_segment"] = $page;
        // $config["last_link"] = "Last";
        // $config["first_link"] = "First";
        // $config['next_link'] = 'Next';
        // $config['prev_link'] = 'Prev';
        // $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        // $config['full_tag_close'] = '</ul></nav></div>';
        // $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        // $config['num_tag_close'] = '</span></li>';
        // $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        // $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        // $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        // $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        // $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        // $config['prev_tagl_close'] = '</span></li>';
        // $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        // $config['first_tagl_close'] = '</span></li>';
        // $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        // $config['last_tagl_close'] = '</span></li>';

        return $config;
    }

    //    =========== its for invoice_print ===============
    // public function invoice_print() {
    //     $search = (object) array(
    //         'order_id' =>$this->input->post('order_id'),
    //         'product_id' =>$this->input->post('product_id'),
    //         'customer_id' =>$this->input->post('customer_id'),
    //         'order_date' =>$this->input->post('order_date'),
    //         'order_stage' =>$this->input->post('order_stage')
    //     );
    //     $data['customers'] = $this->Order_model->get_customer();
    //     $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
    //     $data['orderd'] = $this->Order_model->get_all_paid_orderd($search);
    //     $data['search'] = $search;
    //     $this->load->view('c_level/header');
    //     $this->load->view('c_level/sidebar');
    //     $this->load->view('c_level/orders/invoice_print');
    //     $this->load->view('c_level/footer');
    // }
    //    =========== its for manage_invoice ===============
    public function manage_invoice()
    {
        $this->permission_c->check_label(7)->create()->redirect();

        $search = (object) array(
            'order_id' => $this->input->post('order_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_stage' => $this->input->post('order_stage'),
            'search_daterange' => $this->input->post('search_daterange'),
        );

        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['cmp_info'] = $this->Setting_model->company_profile();

        $total_rows = $this->total_rows($search);

        $per_page = 20;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $url = base_url('manage-invoice');


        $config = $this->pagination($total_rows, $per_page, $page, $url);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['orderd'] = $this->Order_model->get_all_invoice_orderd($search, $per_page, $page);
        $data['search'] = $search;


        if($this->session->userdata('package_id')==1){
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
        }else{
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/manage_invoice', $data);
        $this->load->view('c_level/footer');
    }
    }

    public function total_rows($search)
    {

        $this->db->select("quatation_tbl.*,CONCAT(customer_info.first_name,' ',customer_info.last_name) as customer_name");
        $this->db->from('quatation_tbl');
        $this->db->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id', 'left');

        if ($search->customer_id != NULL) {
            $this->db->where('quatation_tbl.customer_id', $search->customer_id);
        }
        if ($search->order_stage != NULL) {

            $this->db->where('quatation_tbl.order_stage', $search->order_stage);
        }

        if ($search->order_id != NULL) {

            $this->db->where('quatation_tbl.order_id', $search->order_id);
        }

        if ($search->search_daterange != '') {
            $daterange_date = $search->search_daterange;
            $range_date = explode(" - ", $daterange_date);
            $date1 = $range_date[0];
            $date2 = $range_date[1];

            $date11 = explode("/", $date1);
            $date22 = explode("/", $date2);

            if (count($date11) > 2 && count($date22) > 2) {
                $from_date = $date11[2] . "-" . $date11[0] . "-" . $date11[1];
                $to_date = $date22[2] . "-" . $date22[0] . "-" . $date22[1];

                $this->db->where("quatation_tbl.order_date >=", $from_date);
                $this->db->where("quatation_tbl.order_date <=", $to_date);
            }
        }

        $this->db->where('quatation_tbl.created_by', $this->session->userdata('user_id'));

        // $this->db->where('quatation_tbl.order_stage', 2);
        $this->db->order_by('order_date', 'DESC');

        return $query = $this->db->get()->num_rows();
    }

    //    =========== its for order_cancel ===============
    public function order_cancel()
    {

       // $this->permission_c->check_label('cancelled_order')->create()->redirect();
        $access_retailer=access_role_permission_page(8);
        $acc=access_role_retailer();
        $search = (object) array(
            'order_id' => $this->input->post('order_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => 6
        );


        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();
        $data['orderd'] = $this->Order_model->get_all_cancel_orderd($search);
        $data['company_profile'] = $this->Setting_model->company_profile();
        $data['search'] = $search;


         if($this->session->userdata('package_id')==1){
         $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
          $this->load->view('c_level/header');
          $this->load->view('c_level/sidebar');
          $this->load->view('c_level/upgrade_error');
          $this->load->view('c_level/footer');
       
        }else{
            if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/orders/order_cancel', $data);
                $this->load->view('c_level/footer');
            }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }
    }

    //    =========== its for track_order ===============
    public function track_order()
    {
        $this->permission_c->check_label(10)->create()->redirect();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/track_order');
        $this->load->view('c_level/footer');
    }

    //    =========== its for my_orders ===============
    public function my_orders()
    {

        $this->permission_c->check_label(56)->create()->redirect();
        $access_retailer=access_role_permission_page(56);
        $acc=access_role_retailer();
        $search = (object) array(
            'order_id' => $this->input->post('order_id'),
            'customer_id' => $this->input->post('customer_id'),
            'order_date' => $this->input->post('order_date'),
            'order_stage' => $this->input->post('order_stage'),
            'level_id' => $this->level_id,
            'user_id' => $this->user_id,
            'synk_status' => 1
        );


        //$total_rows = $this->db->where('quatation_tbl.order_stage!=', 6)->get('quatation_tbl')->num_rows();
        $total_rows = $this->Order_model->order_row_count($search);



        $per_page = 20;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $url = base_url('my-orders');
        /* ends of bootstrap */
        $config = $this->pagination($total_rows, $per_page, $page, $url);
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $data['orderd'] = $this->Order_model->get_all_orderd($search, $per_page, $page);
        // $data['binfo'] = $this->db->where('user_id', 1)->get('company_profile')->row();

        $data['customers'] = $this->Order_model->get_customer();
        $data['products'] = $this->db->select('product_id,product_name')->get('product_tbl')->result();

        //$data['orderd'] = $this->Order_model->get_all_orderd($search);
        $data['search'] = $search;
        $data['company_profile'] = $this->Setting_model->company_profile();

        $b_level_info = $this->session->userdata('admin_created_by');
        $data['binfo'] = $this->db->select('*')->where('id', $b_level_info)->get('user_info')->row();

        if ($access_retailer==1 or $acc==1) {
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar');
            $this->load->view('c_level/orders/my_orders', $data);
            $this->load->view('c_level/footer');
        }else{
            $this->load->view('c_level/header');
            $this->load->view('c_level/sidebar'); 
            $this->load->view('c_level/upgrade_error'); 
            $this->load->view('c_level/footer');
            }
    }

    // 
    public function shipment($order_id = NULL)
    {

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['methods'] = $this->db->where('created_by',$this->level_id)->get('shipping_method')->result();
        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/shipment_form', $data);
        $this->load->view('c_level/footer');
    }

    public function shipping()
    {

        $order_id = $this->input->post('orderid');
        $length = $this->input->post('length');
        $weight = $this->input->post('weight');
        $method_id = $this->input->post('ship_method');
        $service_type = $this->input->post('service_type');
        $shipping_address = $this->input->post('shippin_address');

        $shipDataCheck = $this->db->where('order_id', $order_id)->get('shipment_data')->num_rows();

        if ($shipDataCheck > 0) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Shipment alredy done, can't back process</div>");
            redirect("retailer-order-view/" . $order_id);
        }


        $method = $this->db->where('id', $method_id)->where('created_by',$this->level_id)->get('shipping_method')->row();

        $order = $this->db->select('quatation_tbl.*,customer_info.*')
            ->join('customer_info', 'customer_info.customer_id=quatation_tbl.customer_id')
            ->where('order_id', $order_id)
            ->get('quatation_tbl')->row();

        // if($order->is_different_shipping==1){
        //     $add = explode(',', $order->different_shipping_address);
        //     if($order->different_shipping_address!=NULL){
        //         $ShipTo = (object)[
        //             'Name'                  => $order->first_name.' '.$order->last_name,
        //             'AttentionName'         => $order->first_name.' '.$order->last_name,
        //             'Phone'                 =>  $order->phone,
        //             'AddressLine'           =>  trim($add[0]),
        //             'City'                  =>  trim($add[1]),
        //             'StateProvinceCode'     =>  trim($add[2]),
        //             'PostalCode'            =>  trim($add[3]),
        //             'CountryCode'           =>  trim($add[4]),
        //         ];
        //     } else {
        //        $this->session->set_flashdata('message', "<div class='alert alert-danger'>
        //         <p>Errors!</p>
        //         <p>Description : Different shipping address missing!</p>
        //         </div>");
        //         redirect("retailer-order-shipment/".$order_id);
        //     }
        // } else {
        //$st = $this->db->select('shortcode')->where('state_name',@$order->state)->get('us_state_tbl')->row();


        $addd = explode(',', $shipping_address);

        if ($addd[0] != NULL && $addd[1] != NULL && $addd[2] != NULL && $addd[3] != NULL && $addd[4] != NULL) {
            //if($order->address!=NULL && $order->city!=NULL && $order->state!=NULL && $order->zip_code!=NULL ){
            // $ShipTo = (object)[
            //         'Name'                  =>  $order->first_name.' '.$order->last_name,
            //         'AttentionName'         =>  $order->first_name.' '.$order->last_name,
            //         'Phone'                 =>  $order->phone,
            //         'AddressLine'           =>  $order->address,
            //         'City'                  =>  $order->city,
            //         'StateProvinceCode'     =>  $order->state,
            //         'PostalCode'            =>  $order->zip_code,
            //         'CountryCode'           =>  $order->country_code,
            //     ];

            $phone = str_replace('+1', '', $order->phone);

            $ShipTo = (object) [
                'Name' => $order->first_name . ' ' . $order->last_name,
                'AttentionName' => $order->first_name . ' ' . $order->last_name,
                'Phone' => $phone,
                'AddressLine' => $addd[0],
                'City' => $addd[1],
                'StateProvinceCode' => $addd[2],
                'PostalCode' => $addd[3],
                'CountryCode' => $addd[4]
            ];
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : Wrong address format!</p>
                </div>");
            redirect("retailer-order-shipment/" . $order_id);
        }

        //}


        if (strtolower($method->method_name) == 'ups') {

            $shiperInfo = $this->get_shiper_info($method_id, $service_type);

            $this->ups_shipping($ShipTo, $length, $weight, $shiperInfo, $order_id, $method_id);
        }

        if (strtolower($method->method_name) == 'delivery in person' || strtolower($method->method_name) == 'customer pickup') {

            $this->delivery_in_person($ShipTo, $order_id, $method_id);
        }
    }

    public function ups_shipping($ShipTo = null, $length = NULL, $weight = NULL, $shiperInfo = NULL, $order_id = NULL, $method_id = NULL)
    {


        $this->load->library('Upsshipping');

        /* Ship To Address */
        $this->upsshipping->addField('ShipTo_Name', $ShipTo->Name);

        $this->upsshipping->addField('ShipTo_AddressLine', array($ShipTo->AddressLine));

        $this->upsshipping->addField('ShipTo_City', $ShipTo->City);

        $this->upsshipping->addField('ShipTo_StateProvinceCode', $ShipTo->StateProvinceCode);

        $this->upsshipping->addField('ShipTo_PostalCode', $ShipTo->PostalCode);

        $this->upsshipping->addField('ShipTo_CountryCode', $ShipTo->CountryCode);

        $this->upsshipping->addField('ShipTo_Number', $ShipTo->Phone);

        $this->upsshipping->addField('length', $length);

        $this->upsshipping->addField('weight', $weight);

        list($response, $status) = $this->upsshipping->processShipAccept($shiperInfo);

        $ups_response = json_decode($response);

        if (isset($ups_response->ShipmentResponse) && $ups_response->ShipmentResponse->Response->ResponseStatus->Code == 1) {

            $track_number = $ups_response->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;

            $total_charges = $ups_response->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue;

            $graphic_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage;

            $html_image = $ups_response->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->HTMLImage;

            //graphic_image_path
            $data1g = "data:image/jpeg;base64," . $graphic_image;

            $img_name_g = $track_number . '_graphic_image.jpeg';

            $source_g = fopen($data1g, 'r');

            $graphic_image = fopen("assets/b_level/shipping_img/" . $img_name_g, "w");

            stream_copy_to_stream($source_g, $graphic_image);

            fclose($source_g);

            fclose($graphic_image);

            $graphic_image_path = 'assets/b_level/shipping_img/' . $img_name_g;

            // html image
            $data1 = "data:image/jpeg;base64," . $html_image;

            $img_name = uniqid() . $track_number . '_html_image.jpeg';

            $source = fopen($data1, 'r');

            $html_image = fopen("assets/b_level/shipping_img/" . $img_name, "w");

            stream_copy_to_stream($source, $html_image);

            fclose($source);

            fclose($html_image);

            $html_image_path = 'assets/b_level/shipping_img/' . $img_name;

            $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('quatation_tbl')->row();
            // quatation table update
            $orderData = array(
                'grand_total' => @$rowData->grand_total + $total_charges,
                'due' => @$rowData->due + $total_charges,
                'shipping_charges' => $total_charges,
                'order_stage' => 3
            );
            //update to quatation table with pyament due 
            $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);

            $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

            $upsResponseData = array(
                'order_id' => $order_id,
                'track_number' => $track_number,
                'shipping_charges' => $total_charges,
                'graphic_image' => $graphic_image_path,
                'html_image' => $html_image_path,
                'method_id' => $method_id,
                'service_type' => @$shiperInfo['service_type'],
                'shipping_addres' => @$shipping_addres
            );

            $this->db->insert('shipment_data', $upsResponseData);
            redirect('retailer-order-view/' . $order_id);
        } elseif ($ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode != NULL) {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors code : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code . "</p>
                <p>Description : " . $ups_response->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description . "</p>
            </div>");
            redirect("retailer-order-shipment/" . $order_id);
        } else {

            $this->session->set_flashdata('message', "<div class='alert alert-danger'>
                <p>Errors!</p>
                <p>Description : This is internal error!</p>
            </div>");
            redirect("retailer-order-shipment/" . $order_id);
        }
    }

    public function order_view($order_id)
    {
       // $this->permission_c->check_label('order_view')->create()->redirect();

        $data['orderd'] = $this->Order_model->get_orderd_by_id($order_id);
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('shipment_data')->row();
        $data['cmp_info'] = $this->Setting_model->company_profile();

        $data['card_info'] = $this->db->where('created_by', $this->user_id)->where('is_active', 1)->get('c_card_info')->row();

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/orders/order_view', $data);
        $this->load->view('c_level/footer');
    }

    public function delete_order($order_id)
    {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "order information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

        //============== close access log info =================

        $this->db->where('order_id', $order_id)->delete('quatation_tbl');
        $this->db->where('order_id', $order_id)->delete('qutation_details');
        $this->db->where('order_id', $order_id)->delete('quatation_attributes');
        $this->db->where('order_id', $order_id)->delete('shipment_data');

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order Delete successfully!</div>");
        redirect("manage-order");
    }

    public function get_shiper_info($method_id, $service_type)
    {

        $method = $this->db->where('id', $method_id)->where('created_by',$this->level_id)->get('shipping_method')->row();

        $company_profile = $this->Setting_model->company_profile();


        $arrayName = array(
            'username' => $method->username,
            'password' => $method->password,
            'account_id' => $method->account_id,
            'access_token' => $method->access_token,
            'name' => $company_profile[0]->company_name,
            'attentionname' => 'Attention Name',
            'description' => 'This is test deiscription',
            'address' => $company_profile[0]->address,
            'city' => $company_profile[0]->city,
            'state' => $company_profile[0]->state,
            'zip_code' => $company_profile[0]->zip_code,
            'country_code' => $company_profile[0]->country_code,
            'phone' => $company_profile[0]->phone,
            'method_id' => $method->id,
            'mode' => $method->mode,
            'service_type' => $service_type,
            'pickup_method' => $method->pickup_method
        );

        return $arrayName;
    }

    public function delivery_in_person($ShipTo, $order_id, $method_id)
    {

        $delivery_date = $this->input->post('delivery_date');
        $delivery_charge = $this->input->post('delivery_charge');
        $comment = $this->input->post('comment');

        $rowData = $this->db->select('due,paid_amount,grand_total')->where('order_id', $order_id)->get('quatation_tbl')->row();
        // quatation table update
        $orderData = array(
            'grand_total' => @$rowData->grand_total + @$delivery_charge,
            'due' => @$rowData->due + @$delivery_charge,
            'shipping_charges' => @$delivery_charge,
            'order_stage' => 3
        );

        //update to quatation table with pyament due 
        $this->db->where('order_id', $order_id)->update('quatation_tbl', $orderData);

        $shipping_addres = $ShipTo->AddressLine . ',' . $ShipTo->City . ',' . $ShipTo->StateProvinceCode . ',' . $ShipTo->PostalCode . ',' . $ShipTo->CountryCode;

        $deliveryData = array(
            'order_id' => $order_id,
            'track_number' => '',
            'shipping_charges' => $delivery_charge,
            'graphic_image' => '',
            'html_image' => '',
            'method_id' => $method_id,
            'comment' => $comment,
            'delivery_date' => $delivery_date,
            'service_type' => '',
            'shipping_addres' => $shipping_addres
        );

        $this->db->insert('shipment_data', $deliveryData);
        redirect('retailer-order-view/' . $order_id);
    }

    //    =========== its for set_order_stage ==============

    public function set_order_stage($stage, $order_id)
    {

        if (!empty($stage) && !empty($order_id)) {

            if ($stage == 1) {
                $order_stage = 'Quote';
            }
            if ($stage == 2) {
                $order_stage = 'Paid';
            }
            if ($stage == 3) {
                $order_stage = 'Partially Paid';
            }
            if ($stage == 4) {
                $order_stage = 'In Process';
            }
            if ($stage == 5) {
                $order_stage = 'Shipping';
            }
            if ($stage == 6) {
                $order_stage = 'Cancelled';
            }
            if ($stage == 7) {
                $order_stage = 'Delivered';
            }
            if($stage == 11){
                $order_stage = 'Manufacturing';
            }



            $this->db->set('order_stage', $stage)->where('order_id', $order_id)->update('quatation_tbl');

            $order = $this->db->where('order_id', $order_id)->get('quatation_tbl')->row();


            if ($stage == 11) {
                unset($order->id);
                unset($order->manufacture_order_date);
                unset($order->ship_order_date);
                unset($order->is_added_in_quickbooks);
                unset($order->quick_books_id);
                unset($order->is_added_commission_quick_book_status);
                $order = (array) $order;
                $is_insert = $this->db->insert('c_manufactur', $order);

                if ($is_insert) {
                    $order_detail = $this->db->where('order_id', $order_id)->get('qutation_details')->result();



                    foreach ($order_detail as $key => $value) {
                        if ($value->created_status == 0) {
                            print_r($value);
                            unset($value->row_id);
                            $val = (array) $value;
                            $this->db->insert('c_manufactur_details', $val);
                        }
                    }
                }
            }






            $this->email_sender->send_email(
                $data = array(
                    'customer_id' => $order->customer_id,
                    'message' => 'Order Stage has been updated to  ' . $order_stage . ' for ' . $order_id,
                    'subject' => 'Order payment'
                )
            );


            $this->Order_model->smsSend(
                $data = array(
                    'customer_id' => $order->customer_id,
                    'message' => 'Order Stage has been updated to  ' . $order_stage . ' for ' . $order_id,
                    'subject' => 'Order payment'
                )
            );




            echo 1;
        } else {
            echo 2;
        }
    }

    // ============== its for cancel_comment ==============
    public function cancel_comment()
    {
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "cancel";
        $remarks = "order cancel comment";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $order_id = $this->input->post('order_id');
        $stage_id = $this->input->post('stage_id');
        $cancel_comment = $this->input->post('cancel_comment');
        $cancel_data = array(
            'cancel_comment' => $cancel_comment,
            'order_stage' => $stage_id,
        );
        $this->db->where('order_id', $order_id);
        $this->db->update('quatation_tbl', $cancel_data);
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order cancel successfully!</div>");
        redirect("manage-order");
    }

    /** Start added by insys */
    //    =========== its for manage_action ==============
    public function manage_action()
    {
        if ($this->input->post('action') == 'action_delete') {
            $this->load->model('Common_model');
            $res = $this->Common_model->DeleteSelected('quatation_tbl', 'id');
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Selected Order has been Deleted successfully.</div>");
        }
        redirect("manage-order");
    }

    /** End added by insys */
    //------------------------------------------------
    //------------------------------------------------
    //      its for new_order
    //------------------------------------------------
    public function update_order_item()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $data['is_view'] = $this->input->post('is_view_only');
        $data['item_details'] = $this->Order_model->get_order_item_details($this->input->post('row_id'));
        $data['item_attr'] = $this->Order_model->get_order_item_attr(array('order_id' => $data['item_details']->order_id, 'product_id' => $data['item_details']->product_id));
        $item_attr = json_decode($data['item_attr']);

        for ($i = 0; $i < sizeof($item_attr); $i++) {
            $item_attr[$i]->attribute_type = $this->db->select('attribute_type')->where('attribute_id', $item_attr[$i]->attribute_id)->get('attribute_tbl')->row()->attribute_type;
            for ($j = 0; $j < sizeof($item_attr[$i]->opop); $j++) {
                $item_attr[$i]->opop[$j]->option_type = $this->db->select('attr_options.option_type')
                    ->join('attr_options', 'attr_options.att_op_id=product_attr_option.option_id')
                    ->where('product_attr_option.id', $item_attr[$i]->options[0]->option_id)
                    ->get('product_attr_option')->row()->option_type;
            }
        }

        $data['item_attr'] = json_encode($item_attr);

        $data['get_category'] = $this->Order_model->get_customer_category();
        $data['currencys'] = $this->Setting_model->company_profile();
        $data['fractions'] = $this->db->get('width_height_fractions')->result();
        $data['rooms'] = $this->db->get('rooms')->result();

        $data['category_wise_subcategory'] = $this->category_wise_subcategory($data['item_details']->category_id, 1);
        $data['cat_fraction'] = $this->get_cat_fraction($data['item_details']->category_id, 1);
        $data['customer_product_by_category'] = $this->get_customer_product_by_category($data['item_details']->category_id, $data['item_details']->product_id, 1);


        $htmlData = $this->load->view('c_level/orders/order_item_form', $data, true);
        echo $htmlData;

        // $main_b_id = $this->session->userdata('main_b_id');
        // $this->permission_c->check_label('manage_order')->update()->redirect();
        // $data['get_customer'] = $this->Order_model->get_customer();
        // $data['get_category'] = $this->Order_model->get_customer_category();
        // $data['orderjs'] = "c_level/orders/order_js.php";
        // $data['currencys'] = $this->Setting_model->company_profile();
        // $data['fractions'] = $this->db->get('width_height_fractions')->result();
        // $data['rooms'] = $this->db->get('rooms')->result();
        // $data['binfo'] = $this->db->where('user_id', $main_b_id)->get('company_profile')->row();
        // $this->load->view('c_level/header');
        // $this->load->view('c_level/sidebar');
        // $this->load->view('c_level/orders/update_order_item.php', $data);
        // $this->load->view('c_level/footer');
    }

    function c_user_manufactur_list($page = 0)
    {
        $access_retailer=access_role_permission_page(88);
        $acc=access_role_retailer();

        $search = (object) array(
                    'company_id' => $this->input->post('company_id'),
                    'sidemark' => $this->input->post('sidemark'),
                    'order_date' => $this->input->post('order_date'),
                    'order_stage' => $this->input->post('order_stage')
        );


        $data['productid'] = $search->product_id;
        $data['customerid'] = $search->company_id;
        $data['side_mark'] = $search->sidemark;
        $data['order_date'] = $search->order_date;
        $data['order_stage'] = $search->order_stage;


        $config["base_url"] = base_url('retailer-manufacturing/list');
        $config["total_rows"] = $this->Order_model->get_manufactur_count($search);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["last_link"] = "Last";
        $config["first_link"] = "First";
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["get_manufactur"] = $this->Order_model->search_manufactur($search, $config["per_page"], $page);
        // echo "<pre>";
        // print_r($data["get_manufactur"]);
        // die;

        $data['customers'] = $this->Order_model->get_customer();
        $data["links"] = $this->pagination->create_links();
        $data['pagenum'] = $page;
        //  ========= its for pagination close===========
        if($this->session->userdata('package_id')==2 or $this->session->userdata('package_id')==3 ){
       if ($access_retailer==1 or $acc==1) {
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar');
                $this->load->view('c_level/manufactur/manufactur', $data);
                $this->load->view('c_level/footer');
         }else{
                $this->load->view('c_level/header');
                $this->load->view('c_level/sidebar'); 
                $this->load->view('c_level/upgrade_error'); 
                $this->load->view('c_level/footer');
            }
        }else{
       $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>please Upgrade your  package!</div>");
      $this->load->view('c_level/header');
      $this->load->view('c_level/sidebar');
      $this->load->view('c_level/upgrade_error');
      $this->load->view('c_level/footer');
        }
    }

    function chit($orderd, $order_details)
    {

        // $this->load->model('b_level/Manufactur_model');

        $blinds = [];
        $Shades = [];
        if (!empty($order_details)) {

            $i = 1;
            $its = '';
            foreach ($order_details as $key => $value) {
                $fraction_width_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->width_fraction_id)->get()->row();
                $fraction_height_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->height_fraction_id)->get()->row();

                $fraction_width = $fraction_width_data->fraction_value;
                $fraction_height = $fraction_height_data->fraction_value;
                $fraction_width_decimal = $fraction_width_data->decimal_value;
                $fraction_height_decimal = $fraction_height_data->decimal_value;

                if ($value->category_name == 'Blinds') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }

                        $MountData = '';
                        $tile = '';

                        if (!empty($attr)) {
                            foreach ($attr as $key => $att) {
                                // echo "<pre>";
                                // print_r($att);
                                // exit;
                                $pat = $this->db->where('pattern_model_id', $value->pattern_model_id)->get('pattern_model_tbl')->row();
                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                                if ($a->attribute_name == "Mount") {

                                    $MountData = $this->Order_model->get_name($att, $a->attribute_name);
                                }


                                if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {

                                    $tile = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                $room =  $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';

                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.'-'.$i.@$its."#".$value->row_id;
                        
                        $this->br_code->gcode1($barcode_img_path);

                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }
                        $blinds[] = '<div class="table col-sm-4 mt-3">
                                <table class="table table-hover" style="width:76.2mm;height:38.1mm;font-size: 12px;border-spacing: 0px;">
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;text-align:left;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $orderd->company . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">Date :' .date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;text-align:left;">
                                            ' . $value->product_name .' '. $pat->pattern_name.'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border: 0px;text-align:left;">'. $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:left;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $value->room . '</td>
                                            <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="Blinds' . $i . '" class="hidden " style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10.5px;margin-top:-2px;border-spacing: 0px">                                
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 0px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 0px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;border:0px;">' . $orderd->company . '</td>
                                            <td style="padding: 0px;border:0px;">Date :' .date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;">
                                            ' . $value->product_name .' '. $pat->pattern_name.'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;">' . $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;border: 0px;">' . $vroom . '</td>
                                            <td style="padding: 0px;border: 0px;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>
                            <button type="button" class="btn btn-success" onclick="printContent(\'Blinds' . $i . '\')" style="margin-top: 10px;">Print labels</button>
                        </div>
                        ';
                    }
                }



                if ($value->category_name == 'Shades') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }


                        $controlType = '';
                        $controlPosition = '';
                        $controlLength = '';



                        if (!empty($attr)) {

                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();



                                if ($a->attribute_name == 'Control Type' || $a->attribute_name == 'Control type') {

                                    $controlType = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                if ($a->attribute_name == 'Control Position' || $a->attribute_name == 'Control position') {

                                    $controlPosition = $this->Order_model->get_name($att, $a->attribute_name);
                                }
                                if ($a->attribute_name == 'Control Length' || $a->attribute_name == 'Control length') {

                                    $controlLength = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                //$room =  $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';
                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.' '.$i.@$its;

                        $this->br_code->gcode1($barcode_img_path);

                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }

                        $Shades[] = '<div class="table col-sm-4 mt-3">
                            <table class="table table-hover" style="width:76.2mm;height:38.1mm;font-size: 12px;border-spacing: 0px;">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 2px 2px 2px 5px;border:0px;text-align:left;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $orderd->company . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">
                                        ' . $value->product_name . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:left;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . ' <span style="float: right;margin-right:3px;">size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;">' . $value->room . '</td>
                                        <td style="padding: 2px 2px 2px 5px;border:0px;text-align:left;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 2px 2px 2px 5px;border:0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id="Shades' . $i . '" class="hidden" style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10.5px;margin-top:-2px;border-spacing: 0px">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 0px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 0px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border:0px;">' . $orderd->company . '</td>
                                        <td style="padding: 0px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border:0px;">
                                        ' . $value->product_name . '</td>
                                        <td style="padding: 0px;border:0px;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 0px;border:0px;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . ' <span style="float: right;margin-right:3px;">size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 0px;border:0px;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border: 0px;">' . $vroom . '</td>
                                        <td style="padding: 0px;border: 0px;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 0px;border: 0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success" onclick="printContent(\'Shades' . $i . '\')" style="margin-top: 10px;">Print labels</button>
                            </div>
                            ';
                    }
                }


                $i++;
            }
        }

        return $arrayName = array('shades' => @$Shades, 'blinds' => @$blinds);
    }

    function allchit($orderd, $order_details)
    {

        // $this->load->model('b_level/Manufactur_model');

        $blinds = [];
        $Shades = [];
        if (!empty($order_details)) {

            $i = 1;
            $its = '';
            foreach ($order_details as $key => $value) {
                $fraction_width_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->width_fraction_id)->get()->row();
                $fraction_height_data = $this->db->select('*')->from('width_height_fractions')->where('id', $value->height_fraction_id)->get()->row();

                $fraction_width = $fraction_width_data->fraction_value;
                $fraction_height = $fraction_height_data->fraction_value;
                $fraction_width_decimal = $fraction_width_data->decimal_value;
                $fraction_height_decimal = $fraction_height_data->decimal_value;

                if ($value->category_name == 'Blinds') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }

                        $MountData = '';
                        $tile = '';

                        if (!empty($attr)) {
                            foreach ($attr as $key => $att) {
                                // echo "<pre>";
                                // print_r($att);
                                // exit;
                                $pat = $this->db->where('pattern_model_id', $value->pattern_model_id)->get('pattern_model_tbl')->row();
                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();

                                if ($a->attribute_name == "Mount") {

                                    $MountData = $this->Order_model->get_name($att, $a->attribute_name);
                                }


                                if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {

                                    $tile = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                $room =  $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';

                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.'-'.$i.@$its."#".$value->row_id;

                        $this->br_code->gcode1($barcode_img_path);

                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }
                        $blinds[] = '<table style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10.5px;padding-bottom:10px;margin-top:-2px;border-spacing: 0px">                          
                                    <tbody>
                                        <tr>
                                            <td style="width:65%;padding: 0px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                            <td style="padding: 0px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;border:0px;">' . $orderd->company . '</td>
                                            <td style="padding: 0px;border:0px;">Date :' .date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;">' . $value->product_name .' '. $pat->pattern_name.'</td>
                                            </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;">'. $value->width . ' x ' . $value->height . '  ' . $MountData . '   ' . $value->color_name . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;"> Tilt :    ' . $tile . '</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0px;border: 0px;">' . $vroom . '</td>
                                            <td style="padding: 0px;border: 0px;"> Item# ' . $i . $its . '</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding: 0px;border: 0px;text-align:center;"  class="barcode-image">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                        </tr>
                                    </tbody>
                                </table>';
                    }
                }



                if ($value->category_name == 'Shades') {

                    $attr = json_decode($value->product_attribute);

                    for ($sls = 1; $sls <= $value->product_qty; $sls++) {

                        if ($value->product_qty > 1) {

                            if ($sls == 1) {
                                $its = "A";
                            }
                            if ($sls == 2) {
                                $its = "B";
                            }
                            if ($sls == 3) {
                                $its = "C";
                            }
                            if ($sls == 4) {
                                $its = "D";
                            }
                            if ($sls == 5) {
                                $its = "E";
                            }
                        }


                        $controlType = '';
                        $controlPosition = '';
                        $controlLength = '';



                        if (!empty($attr)) {

                            foreach ($attr as $key => $att) {

                                $a = $this->db->select("attribute_name")->where("attribute_id", $att->attribute_id)->get("attribute_tbl")->row();



                                if ($a->attribute_name == 'Control Type' || $a->attribute_name == 'Control type') {

                                    $controlType = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                if ($a->attribute_name == 'Control Position' || $a->attribute_name == 'Control position') {

                                    $controlPosition = $this->Order_model->get_name($att, $a->attribute_name);
                                }
                                if ($a->attribute_name == 'Control Length' || $a->attribute_name == 'Control length') {

                                    $controlLength = $this->Order_model->get_name($att, $a->attribute_name);
                                }

                                //$room =  $value->room;

                                // if($a->attribute_name=='Room'){
                                //      $room =  $att->attribute_value;
                                //  }
                            }
                        }


                        $barcode_img_path = '';
                        $this->load->library('barcode/br_code');
                        $barcode_img_path = $value->row_id;
                        // $barcode_img_path = $orderd->order_id.' '.$i.@$its;

                        $this->br_code->gcode1($barcode_img_path);
                        
                        if (isset($selected_rooms[$value->room])) {
                            $vroom = $value->room . ' ' . $selected_rooms[$value->room];
                            $selected_rooms[$value->room] ++;
                        } else {
                            $vroom = $value->room . ' 1';
                            $selected_rooms[$value->room] = 2;
                        }

                        $Shades[] = '<table style="border: 0px;width:76.2mm;height:38.1mm;font-size: 10.5px;padding-bottom:10px;margin-top:-2px;border-spacing: 0px">
                                <tbody>
                                    <tr>
                                        <td style="width:65%;padding: 0px;border: 0px;">Order No : ' . $orderd->order_id . '</td>
                                        <td style="padding: 0px;border: 0px;">S/M :' . $orderd->side_mark . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border:0px;">' . $orderd->company . '</td>
                                        <td style="padding: 0px;border:0px;">Date :' . date("M dS Y", strtotime($orderd->order_date)) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border:0px;">
                                        ' . $value->product_name . '</td>
                                        <td style="padding: 0px;border:0px;">' . $value->color_name . '</td>
                                    </tr>
                                    <tr>
                                       <td colspan="2" style="padding: 0px;border:0px;"> size (in)-' . ($value->width . ' ' . $fraction_width) . ' x ' . $value->height  . ' ' . $fraction_height . ' <span style="float: right;margin-right:3px;">size (cm)-' . round((($value->width + $fraction_width_decimal) * 2.5),2) . ' x ' . round((($value->height + $fraction_height_decimal) * 2.5),2) . '   </span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding: 0px;border:0px;">' . $controlType . ': ' . $controlPosition . '-' . $controlLength . '</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;border: 0px;">' . $vroom . '</td>
                                        <td style="padding: 0px;border: 0px;"> Item# ' . $i . $its . '</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="barcode-image" style="padding: 0px;border: 0px;text-align:center;">' . $this->br_code->printgcode1($barcode_img_path) . '</td>
                                    </tr>
                                </tbody>
                            </table>
                            ';
                    }
                }


                $i++;
            }
        }

        return $arrayName = array('shades' => @$Shades, 'blinds' => @$blinds);
    }



    public function quotation($order_id)
    {

        $data['orderd']    = $this->Order_model->get_order_and_manufacturing_order_by_id($order_id);

        $data['order_details']  = $this->Order_model->get_manufacture_orderd_details_by_id($order_id);

        $data['chit']  = $this->chit($data['orderd'], $data['order_details']);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->Setting_model->company_profile();


        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();


        $data['for'] = $this->db->where('user_id', $data['orderd']->level_id)->get('company_profile')->row();

        $data['categories'] = array();
        $order_details = $data['order_details'];
        foreach ($order_details as $key => $blind) {
            if (!in_array($blind->category_name, $data['categories'])) {
                $data['categories'][] = $blind->category_name;
            }
        }

        // echo "<pre>";print_r($data['order_details']);die;
        // $attr = json_decode($data['order_details'][0]->product_attribute);
        // foreach ($attr as $key => $att) {
        //     $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
        //     if ($a->attribute_name == 'Mount') {
        //         $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
        //         echo $ops->option_name;
        //     }
        // }
        // exit;

        $data['chit'] = $this->allchit($data['orderd'], $order_details);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/manufactur/new_manufacturing', $data);
        $this->load->view('c_level/footer');
    }

    public function quotation_label($order_id)
    {
        $data['orderd']    = $this->Order_model->get_order_and_manufacturing_order_by_id($order_id);
        $data['order_details']  = $this->Order_model->get_manufacture_orderd_details_by_id($order_id);
        $data['chit']  = $this->chit($data['orderd'], $data['order_details']);

        $data['shipping'] = $this->db->select('shipment_data.*,shipping_method.method_name')
            ->join('shipping_method', 'shipping_method.id=shipment_data.method_id', 'left')
            ->where('order_id', $order_id)->get('shipment_data')->row();

        $data['company_profile'] = $this->Setting_model->company_profile();


        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();


        $data['for'] = $this->db->where('user_id', $data['orderd']->level_id)->get('company_profile')->row();

        $data['categories'] = array();
        $order_details = $data['order_details'];
        foreach ($order_details as $key => $blind) {
            if (!in_array($blind->category_name, $data['categories'])) {
                $data['categories'][] = $blind->category_name;
            }
        }

        // $data['chit'] = $this->allchit($data['orderd'], $order_details);

        $this->load->view('c_level/header');
        $this->load->view('c_level/sidebar');
        $this->load->view('c_level/manufactur/manufacturing', $data);
        $this->load->view('c_level/footer');
    }

    public function save_blind_menufactur() {

        $order_id = $this->input->post('orderid');
        $product_id = $this->input->post('product_id');


        $sc_chk = $this->input->post('sc_chk');
        $val_cut_chk = $this->input->post('val_cut_chk');
        $chk = $this->input->post('chk');


        $hr = $this->input->post('hr');
        $br = $this->input->post('br');
        $tb = $this->input->post('tb');
        $sc = $this->input->post('sc');
        $cl = $this->input->post('cl');


        $blindData = [];
        foreach ($product_id as $key => $id) {

            $opdata = array(
                'sc_chk' => @$sc_chk[$id],
                'val_cut_chk' => @ $val_cut_chk[$id],
                'chk' => @$chk[$id],
                'hr' => @$hr[$id],
                'br' => @$br[$id],
                'tb' => @$tb[$id],
                'sc' => @$sc[$id],
                'cl' => @$cl[$id]
            );

            $blindData[] = array(
                'order_id' => $order_id,
                'product_id' => $id,
                'chk_option' => json_encode($opdata)
            );


            $this->db->where('order_id', $order_id)->where('product_id', $id)->delete('manufactur_data');
        }


        $this->db->insert_batch('manufactur_data', $blindData);

        echo '1';
    }


    public function update_order_item_details()
    {
        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');
        $attrib = [];

        foreach ($attributes as $key => $att) {

            $attributes_type = $this->db->select('attribute_tbl.attribute_type')
                ->where('attribute_tbl.attribute_id', $att)
                ->get('attribute_tbl')
                ->row()->attribute_type;

            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];


            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {
                foreach ($ops as $key => $op) {
                    $option_type = $this->db->select('attr_options.option_type')
                        ->where('attr_options.att_op_id', explode('_', $op)[1])
                        ->get('attr_options')->row()->option_type;

                    if ($option_type == 1) {
                        $opval = $option_value[1];
                    } else {
                        $opval = $option_value[$key];
                    }

                    $options[] = array(
                        'option_type' => $option_type,
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $opval,
                        'option_key_value' => $op, //added by itsea, previously there was no value saving of attributes drop down, so added this
                    );
                }
            }

            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);
            $fraction = $this->input->post('fraction_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key] . ' ' . $fraction[$key],
                        'option_key_value' => $opop,
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key]
                    );
                }
            }


            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }


            $attrib[] = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'attributes_type' => $attributes_type,
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }

        $product_id = $this->input->post('product_id');
        $category_id = $this->input->post('category_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $price = $this->input->post('price');
        $total_price = $this->input->post('total_price');
        $notes = $this->input->post('notes');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $room = $this->input->post('room');

        $cart_row_id = $this->input->post('update_product');

        $new_quat_attr_info = array(
            'product_id' => $product->product_id,
            'product_attribute' => json_encode($attrib)
        );

        $this->db->where('fk_od_id', $cart_row_id);
        $this->db->update('quatation_attributes', $new_quat_attr_info);

        $new_pro_ord_info = array(
            'room' => $room,
            'product_id' => $product_id,
            'category_id' => $category_id,
            'list_price' => $total_price,
            'unit_total_price' => $total_price,
            'pattern_model_id' => $pattern_model_id,
            'color_id' => $color_id,
            'width' => $width,
            'height' => $height,
            'width_fraction_id' => $width_fraction_id,
            'height_fraction_id' => $height_fraction_id,
            'notes' => $notes
        );

        $this->db->where('row_id', $cart_row_id);
        $this->db->update('qutation_details', $new_pro_ord_info);
    }


    public function get_height_width_fraction($g_val, $category_id = 0)
    {
        if (isset($g_val) && !empty($g_val)) {
            error_reporting(E_ALL);
            
            if ($category_id > 0) {
                // Category wise weight and height fraction : START
                $hw = $this->db->select('*')
                    ->where('decimal_value >=', "0." . $g_val)
                    ->order_by('decimal_value', 'asc')
                    ->get('width_height_fractions')->result();

                $hw1 = $this->db->select('*')
                        ->where('category_id', $category_id)
                        ->limit(1)
                        ->get('category_tbl')->row();

                if(isset($hw1->fractions) && $hw1->fractions != '') {
                    $category_fractions = explode(',', $hw1->fractions);
                    $hwf = 0;
                    foreach ($hw as $key => $value) {
                        if (in_array($value->fraction_value, $category_fractions)){
                            $hwf = $value->id;
                            break;
                        }
                    }
                    echo $hwf;
                }else{
                    echo 0;
                }        
                // Category wise weight and height fraction : END
            }else{
                // All weight and height fraction : START
                $hw = $this->db->select('*')
                    ->where('decimal_value >=', "0." . $g_val)
                    ->order_by('decimal_value', 'asc')
                    ->limit(1)
                    ->get('width_height_fractions')->row();
                $hwf = isset($hw->id)?$hw->id:0;  
                echo $hwf;
                // All weight and height fraction : END
            }  
        }else{
            echo 0;    
        }    
    }

    // Get product list data
    public function getOrderData()
    {
        $data['row_id'] = $row_id = $this->input->post('row_id');
        $data['order_id'] = $order_id = $this->input->post('order_id');
        $data['order_details'] = $order_details = $this->Order_model->get_orderd_details_by_id($order_id);
        $data['company_profile'] = $this->Setting_model->single_company_profile($this->session->userdata('main_b_id'));
        echo $this->load->view('c_level/orders/single_order_view_html', $data);
    }

    public function getCartData()
    {
        $data['rowid'] = $rowid = $this->input->post('rowid');
        $data['order_details'] = $order_details = $this->cart->contents($rowid)[$rowid];
        $data['company_profile'] = $this->Setting_model->company_profile();
        echo $this->load->view('b_level/orders/single_cart_view_html', $data);
    }
    
    // Get product list data
    public function getWholeSaleOrderData()
    {
        $data['row_id'] = $row_id = $this->input->post('row_id');
        $data['order_id'] = $order_id = $this->input->post('order_id');
        $data['order_details'] = $order_details = $this->Order_model->get_wholesale_orderd_details_by_id($order_id);
        $data['company_profile'] = $this->Setting_model->single_company_profile($this->session->userdata('main_b_id'));
        echo $this->load->view('c_level/orders/single_order_view_html', $data);
    }

    // Get product list data
    public function getMyOrderData()
    {
        $data['row_id'] = $row_id = $this->input->post('row_id');
        $data['order_id'] = $order_id = $this->input->post('order_id');
        $data['my_order'] = true;
        $data['order_details'] = $order_details = $this->Order_model->get_orderd_details_by_id_for_blevel($order_id);
        $data['company_profile'] = $this->Setting_model->single_company_profile($this->session->userdata('main_b_id'));
        echo $this->load->view('c_level/orders/single_order_view_html', $data);
    }
    public function saveCustSessionForOrder($cust=""){
        // $this->session->set_userdata(array('order-customer-c' => $cust));
        set_cookie('order-customer-c',$cust,'3600'); 
        echo $cust;
    }

    public function go_retailer_stage($order_id)
    {
        if ($this->set_order_stage(11, $order_id)) {
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order status changed successfully.</div>");
        } else {
            // $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Something went wrong please try again. </div>");
             $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Order status changed successfully.</div>");
        }
        redirect('c_level/invoice_receipt/receipt/'.$order_id);
    }
    /**
    * 
    * @function name : get_prev_form_data()
    * @description   : get previous form data when click on next prev button 
    * created by DM
    * 06-02-2020
    *
    */
    public function get_prev_form_data(){
        $attributes = $this->input->post('attribute_id');
        $attribute_value = $this->input->post('attribute_value');
        $attrib = [];
        foreach ($attributes as $key => $att) {

            $attributes_type = $this->db->select('attribute_tbl.attribute_type')
                ->where('attribute_tbl.attribute_id', $att)
                ->get('attribute_tbl')
                ->row()->attribute_type;

            $options = [];
            $op_op_s = [];
            $op_op_op_s = [];
            $op_op_op_op_s = [];


            $ops = $this->input->post('op_id_' . $att);
            $option_value = $this->input->post('op_value_' . $att);

            if (isset($ops) && is_array($ops)) {
                foreach ($ops as $key => $op) {
                    $option_type = $this->db->select('attr_options.option_type')
                        ->where('attr_options.att_op_id', explode('_', $op)[1])
                        ->get('attr_options')->row()->option_type;

                    if ($option_type == 1) {
                        $opval = $option_value[1];
                    } else {
                        $opval = $option_value[$key];
                    }

                    $options[] = array(
                        'option_type' => $option_type,
                        'option_id' => explode('_', $op)[1],
                        'option_value' => $opval,
                        'option_key_value' => $op, //added by itsea, previously there was no value saving of attributes drop down, so added this
                    );
                }
            }

            $opopid = $this->input->post('op_op_id_' . $att);
            $op_op_value = $this->input->post('op_op_value_' . $att);
            $fraction = $this->input->post('fraction_' . $att);

            if (isset($opopid) && is_array($opopid)) {

                foreach ($opopid as $key => $opop) {
                    $op_op_s[] = array(
                        'op_op_id' => explode('_', $opop)[0],
                        'op_op_value' => $op_op_value[$key] . ' ' . $fraction[$key],
                        'option_key_value' => $opop,
                    );
                }
            }

            $opopopid = $this->input->post('op_op_op_id_' . $att);
            $op_op_op_value = $this->input->post('op_op_op_value_' . $att);

            if (isset($opopopid) && is_array($opopopid)) {

                foreach ($opopopid as $key => $opopop) {

                    $op_op_op_s[] = array(
                        'op_op_op_id' => explode('_', $opopop)[0],
                        'op_op_op_value' => $op_op_op_value[$key]
                    );
                }
            }


            $opopopopid = $this->input->post('op_op_op_op_id_' . $att);
            $op_op_op_op_value = $this->input->post('op_op_op_op_value_' . $att);


            if (isset($opopopopid) && is_array($opopopopid)) {

                foreach ($opopopopid as $key => $opopopop) {

                    $op_op_op_op_s[] = array(
                        'op_op_op_op_id' => explode('_', $opopopop)[0],
                        'op_op_op_op_value' => $op_op_op_op_value[$key]
                    );
                }
            }
            $attrib[] = array(
                'attribute_id' => $att,
                'attribute_value' => $attribute_value[@$key],
                'attributes_type' => $attributes_type,
                'options' => @$options,
                'opop' => @$op_op_s,
                'opopop' => @$op_op_op_s,
                'opopopop' => @$op_op_op_op_s
            );
        }
        $category_id = $this->input->post('category_id');
        $product_id = $this->input->post('product_id');
        $pattern_model_id = $this->input->post('pattern_model_id');
        $color_id = $this->input->post('color_id');
        $width = $this->input->post('width');
        $height = $this->input->post('height');
        $price = $this->input->post('price');
        $h_w_price = $this->input->post('h_w_price');
        $upcharge_price = $this->input->post('upcharge_price');
        $total_price = $this->input->post('total_price');
        $notes = $this->input->post('notes');
        $width_fraction_id = $this->input->post('width_fraction_id');
        $height_fraction_id = $this->input->post('height_fraction_id');
        $room = $this->input->post('room');
        $prevData = array(
            'category_id' => $category_id,
            'product_id' => $product_id,
            'room' => $room,
            'name' => $product_name,
            'price' => $total_price,
            'h_w_price' => $h_w_price,
            'upcharge_price' => $upcharge_price,
            'qty' => 1,
            'dealer_price' => $product->dealer_price,
            'individual_price' => $product->individual_price,
            'pattern_model_id' => $pattern_model_id,
            'color_id' => $color_id,
            'width' => $width,
            'height' => $height,
            'width_fraction_id' => $width_fraction_id,
            'height_fraction_id' => $height_fraction_id,
            'notes' => $notes,
            'row_status' => 1,
            'att_options' => json_encode($attrib)
        );
        
       echo json_encode($prevData);
    }
}
