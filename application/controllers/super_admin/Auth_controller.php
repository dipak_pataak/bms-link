<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('super_admin/Auth_model');
        $this->load->helper("cookie");
    }

    public function index()
    {
        $sesstionData = $this->session->userdata('user_type');
        if($sesstionData == 's')
        {
            redirect('super-admin/dashboard');
        }
        

        $subdomain = explode('.', $_SERVER['HTTP_HOST'])[0];
        if ($subdomain!=SAAS_DEFAULT_SUBDOMAIN) 
        {
           redirect('domain-not-found'); 
        }


        $this->load->view('super_admin/super_admin_login');
    }

    function domain_not_found()
    {
        $this->load->view('errors/html/error_404');
    }

    /*
      |-------------------------------------------------------
      |  SUPER ADMIN  LOGIN CHECK
      |-------------------------------------------------------
     */

    public function authentication() {
        $password = $this->input->post('password');
        $email = $this->input->post('email');

        $remember_me = $this->input->post("remember");
        if ($remember_me == 1) {
            $email_cookie = array(
                'name' => 'email',
                'value' => $email,
                'expire' => '86500',
            );
            $this->input->set_cookie($email_cookie);
            $pass_cookie = array(
                'name' => 'password',
                'value' => $password,
                'expire' => '86500',
            );
            $this->input->set_cookie($pass_cookie);
            //            echo '<pre>';            print_r($email_cookie);            echo '<pre>';            print_r($pass_cookie);die();
        }
        //
        $data['user'] = (object) $userdata = array(
            'email' => $email,
            'password' => $password,
        );

        // CHECK USER
        $user = $this->Auth_model->check_user($userdata);

        if(!isset($user->user_id) || empty($user->user_id)){

            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Unauthorized user</div>");
            redirect('super-admin');

        }

        $subdomain = explode('.', $_SERVER['HTTP_HOST'])[0];

        if ($subdomain!=SAAS_DEFAULT_SUBDOMAIN) 
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> You are not authorised for This Sub-domain</div>");
            redirect('super-admin');
        }

                $session_data = array(
                    'isLogIn' => true,
                    'isAdmin' => true,
                    'user_type' => $user->user_type,
                    'user_id' => $user->user_id,
                    'admin_created_by' => $user->created_by,
                    'name' => $user->first_name . " " . $user->last_name,
                    'email' => $user->email,
                    'logged_in' => TRUE,
                    'session_id' => session_id(),
                );

                // PUT THE USER DATA IN SESSION
                $this->session->set_userdata($session_data);

                // LAST LOGIN INFO UPDATE THE TABLE
                $last_login_info = array(
                    'login_datetime' => date("Y-m-d H:i:s"),
                    'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                );

                $this->db->where('user_id', $this->session->userdata('user_id'));
                $this->db->update('log_info', $last_login_info);

                $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Welcome!" . ' ' . $user->first_name . " " . $user->last_name . "</div>");

                redirect('super-admin/dashboard');




        // redirect('wholesaler-dashboard');
    }

    public function super_admin_logout() {

        SuperAdminAuth();

        $last_logout_info = array(
            'logout_datetime' => date("Y-m-d H:i:s"),
        );
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('log_info', $last_logout_info);

        $this->session->sess_destroy();

       /* $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You are not valid user!</div>");*/
        redirect('super-admin');
    }

    function update_customer_menu()
    {
        $all_customer = $this->db->where('customer_user_id!=','')->group_by('customer_user_id')->get('customer_info')->result();

        foreach ($all_customer as $key => $value)
        {


            $get_all_c_default_menu = $this->db->get('menusetup_tbl')->result();
            foreach ($get_all_c_default_menu as $single)
            {
                $check = $this->db->where('menu_id', $single->id)->where('level_id',$value->customer_user_id)->get('c_menusetup_tbl')->row();
                if (empty($check))
                {

                    $transfer_menu = array('menu_id' => $single->id,
                        'menu_title' => $single->menu_title,
                        'korean_name' => $single->korean_name,
                        'page_url' => $single->page_url,
                        'module' => $single->module,
                        'ordering' => $single->ordering,
                        'parent_menu' => $single->parent_menu,
                        'menu_type' => $single->menu_type,
                        'is_report' => $single->is_report,
                        'icon' => $single->icon, 'status' => $single->status,
                        'level_id' => $value->customer_user_id,
                        'created_by' => $single->created_by,
                        'create_date' => $single->create_date);

                    //            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
                    $this->db->insert('c_menusetup_tbl', $transfer_menu);
                    print_r(1);
                }
            }


        }
        //        dd($get_all_c_default_menu);
        echo "success";

    }

    function update_wholesaler_menu()
    {
        $all_wholesalers = $this->db->where('user_type','b')->get('user_info')->result();

        foreach ($all_wholesalers as $key => $wholesaler)
        {
            $parent_info = $this->db->select('*')->from('user_info a')->where('a.id', $wholesaler->created_by)->get()->result();
            if(!empty($parent_info)){
                if($parent_info[0]->user_type == 'b')
                {
                    $wholesaler = $parent_info[0];
                }
            }
            $all_b_default_menu = $this->db->get('b_menusetup_tbl_default')->result();
            foreach ($all_b_default_menu as $single)
            {
                $check = $this->db->where('menu_id', $single->id)->where('level_id',$wholesaler->id)->get('b_menusetup_tbl')->row();
                if (empty($check))
                {
                    $transfer_menu = array('menu_id' => $single->id,
                        'menu_title' => $single->menu_title,
                        'korean_name' => $single->korean_name,
                        'page_url' => $single->page_url,
                        'module' => $single->module,
                        'ordering' => $single->ordering,
                        'parent_menu' => $single->parent_menu,
                        'menu_type' => $single->menu_type,
                        'is_report' => $single->is_report,
                        'icon' => $single->icon, 'status' => $single->status,
                        'level_id' => $wholesaler->id,
                        'created_by' => $single->created_by,
                        'create_date' => $single->create_date);

                    //            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
                    $this->db->insert('b_menusetup_tbl', $transfer_menu);
                    print_r(1);
                }
            }

        }
        //        dd($get_all_c_default_menu);
        echo "success";
    }


    function change_password()
    {
        $this->load->view('super_admin/change_password');
    }

    function update_change_password()
    {
        if ($_POST['password']==$_POST['confirm']) 
        {
            $data = array('password'=>md5($_POST['password']));

            $this->db->update('log_info',$data);
            $this->db->where('user_id',1);

            echo json_encode(['status' => 200, 'msg' => 'Passsword Changed Successfully', 'url' => base_url('change-password')]);

        }else{
            echo json_encode(['status' => 400, 'msg' => 'Passsword And Confirm password Not Match', 'url' => base_url('change-password')]);
        }


    }


}
