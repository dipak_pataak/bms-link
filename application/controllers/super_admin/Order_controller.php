<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();
        $this->load->model('super_admin/Order_model');

    }

    public function index()
    {
        $data = [];
        $this->load->view('super_admin/list/orders_list', $data);
    }

    function search_order_list_api()
    {
        $response = $this->Order_model->search_order_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function single_order_receipt_view($order_id)
    {
        $orderData =$this->Order_model->get_orderd_by_id($order_id);

        $data['orderd'] = $orderData;
        $data['order_details'] = $this->Order_model->get_orderd_details_by_id($order_id);

        // $data['company_profile'] = $this->settings->company_profile();
        $data['company_profile'] =  $this->db->select('*')
            ->from('company_profile')
            ->where('user_id', $orderData->level_id)
            ->get()->row();
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $this->load->view('super_admin/Single/orders_receipt', $data);
    }


    /******************************************/


    function customer_order_list()
    {
        $data = [];
        $this->load->view('super_admin/list/customer_orders_list', $data);
    }

    function customer_search_order_list_api()
    {
        $response = $this->Order_model->search_customer_order_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function single_customer_order_receipt_view($order_id)
    {
        $orderData =$this->Order_model->get_customer_orderd_by_id($order_id);

        $data['orderd'] = $orderData;
        $data['order_details'] = $this->Order_model->get_customer_orderd_details_by_id($order_id);

        // $data['company_profile'] = $this->settings->company_profile();
        $data['company_profile'] =  $this->db->select('*')
            ->from('company_profile')
            ->where('user_id', $orderData->level_id)
            ->get()->row();
        $data['customer'] = $this->db->where('customer_id', $data['orderd']->customer_id)->get('customer_info')->row();

        $this->load->view('super_admin/Single/orders_receipt', $data);
    }

}
