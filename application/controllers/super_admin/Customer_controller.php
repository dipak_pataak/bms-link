<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        SuperAdminAuth();
    }

    function customer_list() {
        $data = [];
        $this->load->view('super_admin/list/all_customer', $data);
    }

    function search_customer_list_api() {
      
        $this->load->model('super_admin/Customer_model');
        $response = $this->Customer_model->b_level_customer_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function add_customer_view($customer_id = 0) {
        $this->load->model('super_admin/User_model');
        $this->load->model('super_admin/Customer_model');
        $this->load->model('super_admin/Package_model');
        $data['b_user_data'] = $this->User_model->get_b_user_list();
        $data['package_list'] = $this->Package_model->get_package_list();
        if (!empty($customer_id)) {
            $data['data'] = $this->Customer_model->get_single_customer_by_id($customer_id);
            $data['user_info_detail'] = $this->db->where('id', $data['data']->customer_user_id)->get('user_info')->row();
        }
        $this->load->view('super_admin/Single/add_edit_customer', $data);
    }

    function add_customer_api() {

        $this->load->model('super_admin/Customer_model');
        $this->form_validation->set_rules('b_user_id', 'B User', 'required');
        $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');

        $this->form_validation->set_rules('company', 'Company', 'required|max_length[1000]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[1000]');
        if ($this->form_validation->run() == true) {

            foreach ($_POST['phone_type'] as $key => $value) {
                if (empty($value)) {
                    echo json_encode(['status' => 400, 'msg' => 'Phone type is required']);
                    exit();
                }
            }

            foreach ($_POST['phone'] as $key => $value) {
                if (empty($value)) {
                    echo json_encode(['status' => 400, 'msg' => 'Phone number is required']);
                    exit();
                }
            }

            $auth_user = GetSuperAdmin();
            $super_admin_id = $auth_user['user_id'];
            $b_user_id = $_POST['b_user_id'];
            $package_id = $_POST['package_id'];
            $remarks = "Customer information save";
            $created_date = date('Y-m-d');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $phone_type = $this->input->post('phone_type');
            $company = $this->input->post('company');
            $customer_type = $this->input->post('customer_type');
            $address = $this->input->post('address');
            $address_explode = explode(",", $address);
            $address = $address_explode[0];
            $street_no = explode(' ', $address);
            $street_no = $street_no[0];
            $side_mark = $first_name . "-" . $street_no;
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $zip_code = $this->input->post('zip_code');
            $country_code = $this->input->post('country_code');
            $password = $this->input->post('password');
            $username = $this->input->post('email');
            $sub_domain = $this->input->post('sub_domain');
            $user_insert_id = "";

            if ($customer_type == 'business') {
                
             if(!empty($sub_domain)){
                    
                  
                /*******************************************/
                    $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();
                    if(!empty($reserve_domain_exist))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                        exit();
                    }


                    if(isset($_POST['customer_id']) && !empty(trim($_POST['customer_id'])))
                    {
                        $customer_Data = $this->db->where('customer_id', $_POST['customer_id'])->get('customer_info')->row();
                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->where('id !=', $customer_Data->customer_user_id)->get()->result();
                        if (!empty($sub_domain_exist)) 
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }

                    }
                    else
                    {
                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
                        if(!empty($sub_domain_exist))
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }
                    }

                    if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                        exit();
                    }
                /*******************************************/
                 }
                $customer_userinfo_data['created_by'] = $b_user_id;
                $customer_userinfo_data['package_id'] = $package_id;
                $customer_userinfo_data['first_name'] = $first_name;
                $customer_userinfo_data['last_name'] = $last_name;
                $customer_userinfo_data['company'] = $company;
                $customer_userinfo_data['address'] = $address;
                $customer_userinfo_data['city'] = $city;
                $customer_userinfo_data['state'] = $state;
                if (isset($zip_code))
                    $customer_userinfo_data['zip_code'] = $zip_code;
                $customer_userinfo_data['country_code'] = $country_code;
                $customer_userinfo_data['phone'] = $phone[0];
                $customer_userinfo_data['language'] = 'English';
                $customer_userinfo_data['user_type'] = 'c';
                $customer_userinfo_data['create_date'] = $created_date;
                $customer_userinfo_data['reference'] = "";
                $customer_userinfo_data['sub_domain'] =$sub_domain;

                $company_profile['company_name'] = $company;
                $company_profile['phone'] = $phone[0];
                $company_profile['address'] = $address;
                $company_profile['city'] = $city;
                $company_profile['state'] = $state;

                $company_profile['zip_code'] = $zip_code;

                $company_profile['country_code'] = $country_code;
                $company_profile['created_by'] = $b_user_id;
                $company_profile['created_at'] = $created_date;


                $customer_userinfo_data['email'] = $email;
                $this->db->insert('user_info', $customer_userinfo_data);
              //  echo $this->db->last_query();
              //  die('here');
                $user_insert_id = $this->db->insert_id(); 

                // Add Default shipping info while create c user : START
                $this->load->model('Common_model');
                $this->Common_model->default_shipping_method($user_insert_id);                
                // Add Default shipping info while create c user : END    

                $this->c_level_menu_transfer($user_insert_id);
                $this->c_level_acc_coa($user_insert_id);
                // =========== its for save customer data in the users table and login info ===============
                $customer_loginfo_data = array('user_id' => $user_insert_id, 'email' => $username, 'password' => md5($password), 'user_type' => 'c', 'is_admin' => '1',);
                $this->db->insert('log_info', $customer_loginfo_data);

                $company_profile['user_id'] = $user_insert_id;
                $company_profile['email'] = $email;
                $this->db->insert('company_profile', $company_profile);
            }


            //        ============ its for accounts coa table ===============
            $coa = $this->Customer_model->headcode();

            if ($coa->HeadCode != NULL) {
                $hc = explode("-", $coa->HeadCode);
                $nxt = $hc[1] + 1;
                $headcode = $hc[0] . "-" . $nxt;
            } else {
                $headcode = "1020301-1";
            }

            $lastid = $this->db->select("*")->from('customer_info')//->where('level_id', $level_id)
                            ->order_by('customer_id', 'desc')->get()->row();

            $sl = $lastid->customer_no;
            if (empty($sl)) {
                $sl = "CUS-0001";
            } else {
                $sl = $sl;
            }
            $supno = explode('-', $sl);
            $nextno = $supno[1] + 1;
            $si_length = strlen((int) $nextno);

            $str = '0000';
            $cutstr = substr($str, $si_length);
            $sino = "CUS" . "-" . $cutstr . $nextno;
            $customer_no = $sino . '-' . $first_name . " " . $last_name;
            //        ================= close =======================
            //        =============== its for company name with customer id start =============
            $last_c_id = $lastid->customer_id;
            $cn = strtoupper(substr($company, 0, 3)) . "-";
            if (empty($last_c_id)) {
                $last_c_id = $cn . "1";
            } else {
                $last_c_id = $last_c_id;
            }
            $cust_nextid = $last_c_id + 1;
            $company_custid = $cn . $cust_nextid;



            $customer_data['package_id'] = $package_id;

                $package_detail = $this->db->where('id', $package_id)->get('hana_package')->row();
//package update

                if($package_id==2 || $package_id==3) 
                {
                    $amount = 0;
                    $package_duration = 0;
                    if($_POST['duration'] =='monthly') 
                    {
                        $amount = $package_detail->monthly_price;
                        $package_duration = 30;
                    }
                    if($_POST['duration'] =='yearly') 
                    {
                        $amount = $package_detail->yearly_price;
                        $package_duration = 365;
                    }

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>2);

                                 $this->db->where('id',$user_insert_id);
                    $is_update = $this->db->update('user_info',$paid_data);  

                    //transaction
                    $transaction_data = array('user_id'=>$user_insert_id,
                                              'package_id'=>$package_id,
                                              'amount'=>$amount,
                                              'transaction_type'=>'credit',
                                              'transaction_id'=>'',
                                              'status'=>1,
                                              'remark'=>'Package Upgrade By Admin'
                                           );
                    $this->db->insert('package_transaction',$transaction_data);
                    //transaction
                }
                if($package_id==1) 
                {
                    $package_duration = 30;

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>1);

                                 $this->db->where('id',$user_insert_id);
                    $is_update = $this->db->update('user_info',$paid_data);  
                }
                $customer_data['package_id'] = $package_id;
            
// package update end             

            $customer_data['first_name'] = $first_name;
            $customer_data['last_name'] = $last_name;
            $customer_data['email'] = $email;
            $customer_data['phone'] = $phone[0];
            $customer_data['company'] = $company;
            $customer_data['customer_type'] = $customer_type;
            $customer_data['address'] = $address;
            $customer_data['city'] = $city;
            $customer_data['state'] = $state;
            $customer_data['zip_code'] = $zip_code;
            $customer_data['country_code'] = $country_code;
            $customer_data['street_no'] = $street_no;
            $customer_data['side_mark'] = $side_mark;
            $customer_data['level_id'] = $b_user_id;
            $customer_data['created_by'] = $b_user_id;
            $customer_data['create_date'] = $created_date;


            $customer_data['customer_user_id'] = $user_insert_id;
            $customer_data['customer_no'] = $customer_no;
            $customer_data['company_customer_id'] = $company_custid;

            $this->db->insert('customer_info', $customer_data);
            $customer_inserted_id = $this->db->insert_id();
            //    $this->c_level_menu_transfer($user_insert_id);
            // $this->c_level_menu_transfer($user_insert_id);
            //transfar acc_coa_table data
            //  $this->c_level_acc_coa($user_insert_id);
            //        ======== its for customer COA data array ============
            $customer_coa = array('HeadCode' => $headcode, 'HeadName' => $customer_no, 'PHeadName' => 'Customer Receivable', 'HeadLevel' => '4', 'IsActive' => '1', 'IsTransaction' => '1', 'IsGL' => '0', 'HeadType' => 'A', 'IsBudget' => '0', 'IsDepreciation' => '0', 'DepreciationRate' => '0', 'CreateBy' => $b_user_id, 'CreateDate' => $created_date,);
            //        dd($customer_coa);
            $this->db->insert('b_acc_coa', $customer_coa);



            for ($i = 0; $i < count($phone); $i++) {
                $phone_types_number = array('phone' => $phone[$i], 'phone_type' => $phone_type[$i], 'customer_id' => $customer_inserted_id, 'customer_user_id' => $user_insert_id,);
                $this->db->insert('customer_phone_type_tbl', $phone_types_number);
            }
            $url = base_url('super-admin/customer/list');
            echo json_encode(['status' => 201, 'msg' => 'Customer info save successfully!', 'url' => $url]);
            exit();
        } else {
            $error_arr = $this->form_validation->error_array();
            foreach ($error_arr as $key => $msg) {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }
    }

    function update_customer_api() {

        $this->load->model('super_admin/Customer_model');
        $this->form_validation->set_rules('b_user_id', 'B User', 'required');
        $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
        $this->form_validation->set_rules('company', 'Company', 'required|max_length[1000]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[1000]');
        $this->form_validation->set_rules('customer_id', 'customer_id', 'required');
        if ($this->form_validation->run() == true) {

            foreach ($_POST['phone_type'] as $key => $value) {
                if (empty($value)) {
                    echo json_encode(['status' => 400, 'msg' => 'Phone type is required']);
                    exit();
                }
            }

            foreach ($_POST['phone'] as $key => $value) {
                if (empty($value)) {
                    echo json_encode(['status' => 400, 'msg' => 'Phone number is required']);
                    exit();
                }
            }

            $auth_user = GetSuperAdmin();
            $super_admin_id = $auth_user['user_id'];
            $b_user_id = $_POST['b_user_id'];
            $package_id = $_POST['package_id'];
            $remarks = "Customer information save";
            $created_date = date('Y-m-d');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $phone_type = $this->input->post('phone_type');
            $company = $this->input->post('company');
            $customer_type = $this->input->post('customer_type');
            $address = $this->input->post('address');
            $address_explode = explode(",", $address);
            $address = $address_explode[0];
            $street_no = explode(' ', $address);
            $street_no = $street_no[0];
            $side_mark = $first_name . "-" . $street_no;
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $zip_code = $this->input->post('zip_code');
            $country_code = $this->input->post('country_code');


            if ($customer_type == 'business') {

                /*******************************************/
                    $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();
                    if(!empty($reserve_domain_exist))
                   {
                        echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                        exit();
                    }


                    if(isset($_POST['customer_id']) && !empty(trim($_POST['customer_id'])))
                    {
                        $customer_Data = $this->db->where('customer_id', $_POST['customer_id'])->get('customer_info')->row();
                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->where('id !=', $customer_Data->customer_user_id)->get()->result();
                        if (!empty($sub_domain_exist)) 
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }

                    }
                    else
                    {
                        $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
                        if(!empty($sub_domain_exist))
                        {
                            echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                            exit();
                        }
                    }

                    if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
                    {
                        echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                        exit();
                    }
                /*******************************************/
                $customer_userinfo_data['created_by'] = $b_user_id;
                $customer_userinfo_data['first_name'] = $first_name;
                $customer_userinfo_data['last_name'] = $last_name;
                $customer_userinfo_data['company'] = $company;
                $customer_userinfo_data['address'] = $address;
                $customer_userinfo_data['city'] = $city;
                $customer_userinfo_data['state'] = $state;
                if (isset($zip_code))
                    $customer_userinfo_data['zip_code'] = $zip_code;
                $customer_userinfo_data['country_code'] = $country_code;
                $customer_userinfo_data['phone'] = $phone[0];
                $customer_userinfo_data['language'] = 'English';
                $customer_userinfo_data['user_type'] = 'c';
                $customer_userinfo_data['create_date'] = $created_date;
                $customer_userinfo_data['sub_domain'] = $_POST['sub_domain'];
                

                $company_profile['company_name'] = $company;
                $company_profile['phone'] = $phone[0];
                $company_profile['address'] = $address;
                $company_profile['city'] = $city;
                $company_profile['state'] = $state;
                if (isset($zip_code))
                    $company_profile['zip_code'] = $zip_code;
                $company_profile['country_code'] = $country_code;
                $company_profile['created_by'] = $b_user_id;
                $company_profile['created_at'] = $created_date;

                $customerData = $this->db->where('customer_id', $_POST['customer_id'])->get('customer_info')->row();

                $user_insert_id = "";
                if (isset($customerData->customer_user_id) && !empty($customerData->customer_user_id)) {

                    //passworde update                
                        if ($_POST['password']!='') 
                        {
                            if($_POST['password']==$_POST['confirm']) 
                            {
                                $pass_data['password'] = md5($_POST['password']);
                                $this->db->where('user_id', $customerData->customer_user_id)->update('log_info',$pass_data);
                            }else{
                                echo json_encode(['status' => 400, 'msg' => 'Password And confirm password not same']);
                                exit();
                            }
                            
                        }
                    //passworde update

                    $this->db->where('id', $customerData->customer_user_id);
                    $this->db->update('user_info', $customer_userinfo_data);
                    $user_insert_id = $customerData->customer_user_id;
                    $this->db->where('company_id', $customerData->customer_user_id);
                    $this->db->update('company_profile', $company_profile);

                }

                 $user_detail = $this->db->where('id', $customerData->customer_user_id)->get('user_info')->row();
                 $package_detail = $this->db->where('id', $package_id)->get('hana_package')->row();


        //package update
            if($package_id!=$user_detail->package_id) 
            {

                if($package_id==2 || $package_id==3) 
                {

                    if ($package_id < $user_detail->package_id) 
                    {
                        $message = 'Package downgrade By Admin';
                    }else{
                        $message = 'Package Upgrade By Admin';
                    }


                    $amount = 0;
                    $package_duration = 0;
                    if($_POST['duration'] =='monthly') 
                    {
                        $package_duration = 30;
                        $amount = $package_detail->monthly_price;
                    }
                    if($_POST['duration'] =='yearly') 
                    {
                        $package_duration = 365;
                        $amount = $package_detail->yearly_price;
                    }

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>2);

                                 $this->db->where('id',$customerData->customer_user_id);
                    $is_update = $this->db->update('user_info',$paid_data);  


                    //transaction
                    $transaction_data = array('user_id'=>$user_detail->id,
                                              'package_id'=>$package_id,
                                              'amount'=>$amount,
                                              'transaction_type'=>'credit',
                                              'transaction_id'=>'',
                                              'status'=>1,
                                              'remark'=>$message
                                           );
                    $this->db->insert('package_transaction',$transaction_data);
                    //transaction


                }
                if($package_id==1) 
                {
                    $message = '';
                    if ($package_id < $user_detail->package_id) 
                    {
                        $message = 'Package downgrade By Admin';
                    }

                    $package_duration = 30;

                    $expire_date = Date('Y-m-d', strtotime("+".$package_duration." days"));


                    $paid_data = array('package_id'=>$package_id,'advanced_trail'=>1,'pro_trail'=>1,'package_expire'=>$expire_date,'package_type'=>1);

                                 $this->db->where('id',$customerData->customer_user_id);
                    $is_update = $this->db->update('user_info',$paid_data);  

                    //transaction
                    $transaction_data = array('user_id'=>$user_detail->id,
                                              'package_id'=>$package_id,
                                              'amount'=>'0',
                                              'transaction_type'=>'credit',
                                              'transaction_id'=>'',
                                              'status'=>1,
                                              'remark'=>$message
                                           );
                    $this->db->insert('package_transaction',$transaction_data);
                    //transaction

                }
                $customer_data['package_id'] = $package_id;
            }

        // package update end  

            }

            
            $customer_data['first_name'] = $first_name;
            $customer_data['last_name'] = $last_name;
            $customer_data['email'] = $email;
            $customer_data['phone'] = $phone[0];
            $customer_data['company'] = $company;
            $customer_data['customer_type'] = $customer_type;
            $customer_data['address'] = $address;
            $customer_data['city'] = $city;
            $customer_data['state'] = $state;
            $customer_data['zip_code'] = $zip_code;
            $customer_data['country_code'] = $country_code;
            $customer_data['street_no'] = $street_no;
            $customer_data['side_mark'] = $side_mark;
            $customer_data['level_id'] = $b_user_id;
            $customer_data['created_by'] = $b_user_id;
            $customer_data['create_date'] = $created_date;


            $this->db->where('customer_id', $_POST['customer_id']);
            $this->db->update('customer_info', $customer_data);

            $customer_phone_types_deleted_sql = $this->db->where('customer_id', $_POST['customer_id'])->delete('customer_phone_type_tbl');
            for ($i = 0; $i < count($phone); $i++) {
                $phone_types_number = array('phone' => $phone[$i], 'phone_type' => $phone_type[$i], 'customer_id' => $_POST['customer_id'], 'customer_user_id' => $user_insert_id,);
                $this->db->insert('customer_phone_type_tbl', $phone_types_number);
            }

            $url = base_url('super-admin/customer/list');
            echo json_encode(['status' => 201, 'msg' => 'Customer info update successfully!', 'url' => $url]);
            exit();
        } else {
            $error_arr = $this->form_validation->error_array();
            foreach ($error_arr as $key => $msg) {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }
    }

    public function c_level_menu_transfer($user_insert_id) {

        $this->db->where('level_id', $user_insert_id)->delete('c_menusetup_tbl');

        $get_all_c_default_menu = $this->db->get('menusetup_tbl')->result();

        //        $menu_title = $get_all_c_default_menu
        foreach ($get_all_c_default_menu as $single) {
            $transfer_menu = array('menu_id' => $single->id,
                'menu_title' => $single->menu_title,
                'korean_name' => $single->korean_name,
                'page_url' => $single->page_url,
                'module' => $single->module,
                'ordering' => $single->ordering,
                'parent_menu' => $single->parent_menu,
                'menu_type' => $single->menu_type,
                'is_report' => $single->is_report,
                'icon' => $single->icon,
                'status' => $single->status,
                'level_id' => $user_insert_id,
                'created_by' => $user_insert_id,
                'create_date' => date('Y-m-d'),);
            //            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
            $this->db->insert('c_menusetup_tbl', $transfer_menu);
        }
        //        dd($get_all_c_default_menu);
        return true;
    }

    public function c_level_acc_coa($user_insert_id) {

        $get_all_acc_coa = $this->db->get('acc_coa_default')->result();
        //        $menu_title = $get_all_acc_coa
        foreach ($get_all_acc_coa as $single) {

            $transfer_acc_coa = array('HeadCode' => $single->HeadCode, 'HeadName' => $single->HeadName, 'PHeadName' => $single->PHeadName, 'HeadLevel' => $single->HeadLevel, 'IsActive' => $single->IsActive, 'IsTransaction' => $single->IsTransaction, 'IsGL' => $single->IsGL, 'HeadType' => $single->HeadType, 'IsBudget' => $single->IsBudget, 'IsDepreciation' => $single->IsDepreciation, 'DepreciationRate' => $single->DepreciationRate, 'level_id' => $user_insert_id, 'CreateBy' => $single->CreateBy, 'CreateDate' => $single->CreateDate, 'UpdateBy' => $single->UpdateBy, 'UpdateDate' => $single->UpdateDate);
            //            echo '<pre>';            print_r($transfer_acc_coa);echo '</pre>';
            $this->db->insert('acc_coa', $transfer_acc_coa);
        }
        //        dd($get_all_c_default_menu);
        return true;
    }

    function user_status_change($user_id)
    {
        $log_info_detail = $this->db->select('*')->from('log_info')->where('user_id',$user_id)->get()->row();

        if($log_info_detail->status==0) 
        {
            $is_update = $this->db->where('user_id', $user_id)->update('log_info', array('status'=>1));
        }
        if($log_info_detail->status==1) 
        {
            $is_update = $this->db->where('user_id', $user_id)->update('log_info', array('status'=>0));
        }

            if ($is_update) 
            {
                echo json_encode(true);
            }
    }

    function user_delete($user_id)
    {
        $is_delete = $this->db->where('user_id', $user_id)->delete('log_info');
        if ($is_delete) 
        {
            $this->db->where('id', $user_id)->delete('user_info');  
            $this->db->where('customer_user_id', $user_id)->delete('customer_info');  
        }
        echo json_encode(true);
    }

    function edit_company_detail($user_id)
    {
        $data['data'] = $this->db->select('*')->from('company_profile')->where('user_id', $user_id)->get()->row();
        $data['user_detail'] = $this->db->where('id', $user_id)->get('user_info')->row();
        $data['user_id'] = $user_id;
        $this->load->view('super_admin/Single/edit_compny_detail', $data);
    }

    function update_company_detail()
    {
        $data['user_id']      = $_POST['user_id'];
        $data['company_name'] = $_POST['company_name'];
        $data['email']        = $_POST['email'];
        $data['phone']        = $_POST['phone'];
        $data['unit']         = $_POST['unit'];
        $data['currency']     = $_POST['currency'];
        $data['address']      = $_POST['address'];
        $data['city']         = $_POST['city'];
        $data['state']        = $_POST['state'];
        $data['zip_code']     = $_POST['zip_code'];
        $data['country_code'] = $_POST['country_code'];

         /* Image upload code */
            if (!empty($_FILES['image']['tmp_name'])) {
            $tempFile = $_FILES['image']['tmp_name']; 
            $filename = $_FILES['image']['name'];
            $file_parts = explode(".", $filename);
            $ext = end($file_parts);
            
            $target_dir = FCPATH . 'assets/c_level/uploads/appsettings/';
            $target_file = $target_dir . $_POST['user_id'] . '.' . $ext;
            $profile_img_path_temp = $_POST['user_id'].'.'.$ext;

                $movefile = move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
                if(!empty($movefile))
                {
                    $data['logo'] = $profile_img_path_temp;
                }
            
            }
            /* image code end */

        if(isset($_POST['company_id']) && !empty($_POST['company_id']))
        {
            $this->db->where('company_id', $_POST['company_id'])->update('company_profile', $data);
        }else{
            $this->db->insert('company_profile',$data);
        }
        
        echo json_encode(['status' => 201, 'msg' => 'Company details has been added successfully']);
    }


    function login_c_user($user_id)
    {
        $check = check_admin_auth();
        if ($check==true) 
        {
            $token = generate_token_for_user_login($user_id);
            $user_detail = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();
            $package=$user_detail->package_id;
            $http_host = explode('.', $_SERVER['HTTP_HOST'])[1];
            $ext = explode('.', $_SERVER['HTTP_HOST'])[2];
            $creat_by=$user_detail->created_by;
            $create_detail = $this->db->select('*')->from('user_info')->where('id',$creat_by)->get()->row();


            if(empty($create_detail)){
            redirect($_SERVER['REQUEST_SCHEME'].'://'.$user_detail->sub_domain.'.'.$http_host.'.'.$ext.'/Switch_controller/c_level_redirection/'.$user_id.'/'.$token);
            }
            else if($package==1){
            redirect($_SERVER['REQUEST_SCHEME'].'://'.$create_detail->sub_domain.'.'.$http_host.'.'.$ext.'/Switch_controller/c_level_redirection/'.$user_id.'/'.$token);
            }else{
            redirect($_SERVER['REQUEST_SCHEME'].'://'.$user_detail->sub_domain.'.'.$http_host.'.'.$ext.'/Switch_controller/c_level_redirection/'.$user_id.'/'.$token);
            }

        }
    }
}
