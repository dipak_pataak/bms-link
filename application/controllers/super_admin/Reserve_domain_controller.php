<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reserve_domain_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        SuperAdminAuth();
    }

    function reserve_domain_list() {
        /*
          $this->load->model('super_admin/User_model');


          $config["base_url"] = base_url('super-admin/wholesaler/list');
          $config["total_rows"] =  $this->User_model->b_user_list_count();
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["last_link"] = "Last";
          $config["first_link"] = "First";
          $config['next_link'] = 'Next';
          $config['prev_link'] = 'Prev';
          $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
          $config['full_tag_close'] = '</ul></nav></div>';
          $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close'] = '</span></li>';
          $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close'] = '</span></li>';
          $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close'] = '</span></li>';

          $this->pagination->initialize($config);


          $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;


          $data["list_data"] = $this->User_model->b_user_list($config["per_page"], $page);
          $data["links"] = $this->pagination->create_links();
          $data['pagenum'] = $page; */

        $data = [];

        $this->load->view('super_admin/list/reserve_domain_list', $data);
    }

    function search_reserve_domain_list_api() {
        $this->load->model('super_admin/Domain_model');
        $response = $this->Domain_model->search_reserve_domain_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => intval($totalFiltered),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $response['data'],
        );
        echo json_encode($json_data);
    }

    function add_edit_reserve_domain_view($reserve_domain_view = 0) {
        $data = [];
        if (!empty($reserve_domain_view)) {
            $userData = $this->db->where('id', $reserve_domain_view)->get('reserve_domain')->row();
            $data['data'] = $userData;
        }

        $this->load->view('super_admin/Single/add_edit_reserve_domain', $data);
    }

    function reserve_domain_save() 
    {

        $this->form_validation->set_rules('subdomain', 'Sub-domain', 'required|max_length[50]');
        if ($this->form_validation->run()) 
        {
            $sub_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain',$_POST['subdomain'])->get()->result();
            if (!empty($sub_domain_exist)) 
            {
                echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                exit();
            }


            $subdomain_data = array('subdomain' => $_POST['subdomain'] );


            if (!isset($_POST['id']) || empty(trim($_POST['id']))) 
            {
                $this->db->insert('reserve_domain', $subdomain_data);
                $user_id = $this->db->insert_id();
                echo json_encode(['status' => 200, 'msg' => 'Sub domain Save  Successfully']);
                exit();
            } else {
                $this->db->where('id', $_POST['id'])->update('reserve_domain', $subdomain_data);
                echo json_encode(['status' => 200, 'msg' => 'Sub domain Update  Successfully']);
                exit();
            }

            
        } else {
            $data = [];
            $this->session->set_flashdata('form_validation_error', $this->form_validation->error_array());
            $this->load->view('super_admin/Single/add_edit_reserve_domain', $data);
        }
    }


}
