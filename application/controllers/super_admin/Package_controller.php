<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();
        $this->load->model('super_admin/Package_model');

    }

    public function index()
    {
        $data['package_list_data'] = $this->Package_model->get_package_list();
        $data['package_option_list_data'] = $this->Package_model->get_package_option_list();
        $this->load->view('super_admin/list/package_list', $data);
    }
      public function wholesaler()
    {    

        $data['package_list_data'] = $this->Package_model->get_package_list_wholesaler();
        $data['package_option_list_data'] = $this->Package_model->get_package_option_list();
        $this->load->view('super_admin/list/wholesaler_package_list', $data);
    }


    function edit_package_view($package_id,$column)
    {

        $packageData = $this->Package_model->get_single_package($package_id);
        if (!empty($packageData))
        {
            $package_package_option = $this->db->select('title,package_key,'.$column)->from('hana_package_option')->get()->result();
            $data['package_option'] = $package_package_option;
            $data['column'] = $column;

            $data['data'] = $packageData;
            $this->load->view('super_admin/Single/edit_package', $data);
        } 
        else
        {
            $this->load->view('errors/html/error_general', ['heading' => 'Not Found', 'message' => 'Package does not found']);
        }
    }
   
     function edit_wholesaler_package_view($package_id,$column)
    {

        $packageData = $this->Package_model->get_wholesaler_single_package($package_id);
        if (!empty($packageData))
        {
            $package_package_option = $this->db->select('title,package_key,'.$column)->from('wholesaler_package_option')->get()->result();
            $data['package_option'] = $package_package_option;
            $data['column'] = $column;

            $data['data'] = $packageData;
            $this->load->view('super_admin/Single/wholesaler_edit_package', $data);
        } 
        else
        {
            $this->load->view('errors/html/error_general', ['heading' => 'Not Found', 'message' => 'Package does not found']);
        }
    }
    function update_package()
    {
        $this->form_validation->set_rules('id', 'id', 'required|numeric');
        $this->form_validation->set_rules('package', 'Package', 'required|max_length[50]');
        $this->form_validation->set_rules('installation_price', 'Installation price', 'required|regex_match[/^[0-9,.]+$/]');
        $this->form_validation->set_rules('monthly_price', 'Monthly price', 'required|regex_match[/^[0-9,.]+$/]');
        $this->form_validation->set_rules('yearly_price', 'Yearly price', 'required|regex_match[/^[0-9,.]+$/]');

        if ($this->form_validation->run() == true)
        {
            $packageData = $this->Package_model->update_package_data($_POST);
            //$packageOption = $this->Package_model->update_package_option($_POST);
            echo json_encode(['status' => 201, 'msg' => 'Package details has been updated successfully','url' => base_url('/super-admin/package/list')]);
            exit();
        } else
        {
            $error_arr = $this->form_validation->error_array();
            foreach ($error_arr as $key => $msg)
            {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }

    }
     
       function update_wholesaler_package()
    {  

        $this->form_validation->set_rules('id', 'id', 'required|numeric');
        $this->form_validation->set_rules('package', 'Package', 'required|max_length[50]');
        $this->form_validation->set_rules('installation_price', 'Installation price', 'required|regex_match[/^[0-9,.]+$/]');
        $this->form_validation->set_rules('monthly_price', 'Monthly price', 'required|regex_match[/^[0-9,.]+$/]');
        $this->form_validation->set_rules('yearly_price', 'Yearly price', 'required|regex_match[/^[0-9,.]+$/]');

        if ($this->form_validation->run() == true)
        {
            $packageData = $this->Package_model->update_wholesaler_package_data($_POST);
            $packageOption = $this->Package_model->update_wholesaler_package_option($_POST);
            echo json_encode(['status' => 201, 'msg' => 'Package details has been updated successfully','url' => base_url('/super-admin/wholesaler/package/list')]);
            exit();
        } else
        {
            $error_arr = $this->form_validation->error_array();
            foreach ($error_arr as $key => $msg)
            {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }

    }
    function tms_payment_receiver_setting() 
    {
        $this->load->model('super_admin/Package_model');

            if(!empty($_POST)) 
            {
                $setting_data = array('user_name'=>$_POST['user_name'],'password'=>$_POST['password'],'url'=>$_POST['url']);
                $this->db->where('id',$_POST['tms_payment_setting_id']);
                $this->db->update('tms_admin_payment_setting',$setting_data);

                echo json_encode(['status' => 200, 'msg' => 'Package details has been updated successfully']);
                exit();
     
            }

        $data['tms_setting'] = $this->db->select('*')->from('tms_admin_payment_setting')->where('id',1)->get()->row();

        $this->load->view('super_admin/tms_admin_payment_setting', $data);
    }

    function list_package_attribute()
    {
         $data['list_data']     = $this->db->select('*')->from('hana_package_option')->get()->result();
        $this->load->view('super_admin/list/list_package_attribute', $data);
    }

     function list_wholesaler_package_attribute()
    {

         $data['list_data']     = $this->db->select('*')->from('wholesaler_package_option')->get()->result();
        $this->load->view('super_admin/list/list_wholesaler_package_attribute', $data);
    }

    function edit_package_attribute_view($package_attribute_id = 0)
    {
        $data['list_data'] = $this->db->select('*')->from('hana_package_option')->where('id',$package_attribute_id)->get()->row();
        $this->load->view('super_admin/Single/edit_package_attribute', $data);
    }
      function edit_wholesaler_package_attribute_view($package_attribute_id = 0)
    {
        $data['list_data'] = $this->db->select('*')->from('wholesaler_package_option')->where('id',$package_attribute_id)->get()->row();
        $this->load->view('super_admin/Single/edit_wholesaler_package_attribute', $data);
    }


    function package_attribute_save()
    {
        $data = array('title'=>$_POST['title']);
        $this->db->where('id', $_POST['id']);
        $this->db->update('hana_package_option',$data);

        echo json_encode(['status' => 200, 'msg' => 'Package Attribute Update']);
        exit();
    }
     function package_wholesaler_attribute_save()
    {

        $data = array('title'=>$_POST['title']);
        $this->db->where('id', $_POST['id']);
        $this->db->update('wholesaler_package_option',$data);

        echo json_encode(['status' => 200, 'msg' => 'Package Attribute Update']);
        exit();
    }


    function change_subscription_date($subscription_id,$user_id)
    {
       $data = [];
       $data['user_detail'] = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row();
       $data['subscription_id'] = $subscription_id;

       $this->load->view('super_admin/change_subscription_date', $data);
    }

    function wholesaler_change_subscription_date($subscription_id,$user_id)
    {
       $data = [];
       $data['user_detail'] = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row();
       $data['subscription_id'] = $subscription_id;

       $this->load->view('super_admin/wholesaler_change_subscription_date', $data);
    }


    function update_subscription_date()
    {
        $user_detail = $this->db->select('*')->from('user_info')->where('id',$_POST['user_id'])->get()->row();
        $package_id = $user_detail->package_id; 
        $ip = $this->input->ip_address();
// for plan id
        $package_detail = $this->db->select('*')->from('hana_package')->where('id',$package_id)->get()->row();

        if($user_detail->package_duration==1) 
        {
           $plan_id = $package_detail->monthly_plan_id;
           $recurring_amount = $package_detail->monthly_price;
        }
        if($user_detail->package_duration==2) 
        {
            $plan_id = $package_detail->yearly_plan_id;
            $recurring_amount = $package_detail->yearly_price;
        }
// for plan id

        $this->load->library('gwapi');
        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();
        $this->gwapi->setLogin($machentinfo);

        $type = 'delete_subscription';
        $subscription_id = $_POST['subscription_id'];

        $r = (object)$this->gwapi->delete_subscription($type, $subscription_id);

        if($r->response=='1') 
        {
           
           $cusstomer_info = $this->db->where('customer_user_id',$_POST['user_id'])->get('customer_info')->row();

            $this->gwapi->setLogin($machentinfo);
            $this->gwapi->setBilling($cusstomer_info);
            $this->gwapi->setShipping($cusstomer_info);
            $this->gwapi->setOrder($package_id, $orderdescription='Package payment', $tax=0,$Shipping=0,$ip);


            $perameter['recurring'] = "add_subscription";
            $perameter['redirect_url'] = "https://www.google.com";
            $perameter['plan_id'] = $plan_id;
            $perameter['account_type'] = "savings";
            $perameter['entity_type'] = "business";
            $perameter['payment'] = "creditcard";
            $perameter['start_date'] = date("Ymd", strtotime($_POST['date']));
            $this->gwapi->sub_perameter($perameter);

            $card_info   = $this->db->where('id',$user_detail->card_id)->get('c_card_info')->row();

            $ccnumber = $card_info->card_number;
            $ccexp = $card_info->expiry_month.'/'.$card_info->expiry_year; 
            $cvv = $card_info->cvv;

            $response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

                if(isset($response->subscription_id) && !empty($response->subscription_id))
                {
                    $new_date = date("d-m-Y", strtotime($_POST['date']));

                    $add_sub = array('subscription_id'=>$response->subscription_id,
                                     'billing_date'=>$new_date);
                    $this->db->where('id',$user_detail->id);
                    $this->db->update('user_info',$add_sub);  

                    $this->session->set_flashdata('success', "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    Date Update succesfully</div>");
                    redirect(base_url().'change-subscription-date/'.$response->subscription_id.'/'.$user_detail->id);
                }
            
        }

        $type = 'update_subscription';
        $subscription_id = $_POST['subscription_id'];
        $start_date = date("Ymd", strtotime($_POST['date']));

        echo $type;
        echo "<br>";
        echo $subscription_id;
        echo "<br>";
        echo $start_date;
        echo "<br>";

        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();
        $this->gwapi->setLogin($machentinfo);

        $response = (object)$this->gwapi->update_subscription($type, $subscription_id, $start_date);

        echo "<pre>"; print_r($response);die();
    }


        function wholesaler_update_subscription_date()
    {   


        $user_detail = $this->db->select('*')->from('user_info')->where('id',$_POST['user_id'])->get()->row();
        print_r($user_detail);
        die();
        $package_id = $user_detail->package_id; 
        $ip = $this->input->ip_address();

// for plan id
        $package_detail = $this->db->select('*')->from('wholesaler_hana_package')->where('id',$package_id)->get()->row();
      
        if($user_detail->package_duration==1) 
        {
           $plan_id = $package_detail->monthly_plan_id;
           $recurring_amount = $package_detail->monthly_price;
        }
        if($user_detail->package_duration==2) 
        {
            $plan_id = $package_detail->yearly_plan_id;
            $recurring_amount = $package_detail->yearly_price;
        }
// for plan id

        $this->load->library('gwapi');
        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();
 
        $this->gwapi->setLogin($machentinfo);

        $type = 'delete_subscription';
        $subscription_id = $_POST['subscription_id'];

        $r = (object)$this->gwapi->delete_subscription($type, $subscription_id);
   
        if($r->response=='1') 
        {
           
           $cusstomer_info = $this->db->where('customer_user_id',$_POST['user_id'])->get('customer_info')->row();

            $this->gwapi->setLogin($machentinfo);
            $this->gwapi->setBilling($cusstomer_info);
            $this->gwapi->setShipping($cusstomer_info);
            $this->gwapi->setOrder($package_id, $orderdescription='Package payment', $tax=0,$Shipping=0,$ip);


            $perameter['recurring'] = "add_subscription";
            $perameter['redirect_url'] = "https://www.google.com";
            $perameter['plan_id'] = $plan_id;
            $perameter['account_type'] = "savings";
            $perameter['entity_type'] = "business";
            $perameter['payment'] = "creditcard";
            $perameter['start_date'] = date("Ymd", strtotime($_POST['date']));
            $this->gwapi->sub_perameter($perameter);

            $card_info   = $this->db->where('id',$user_detail->card_id)->get('wholesaler_card_info')->row();

            $ccnumber = $card_info->card_number;
            $ccexp = $card_info->expiry_month.'/'.$card_info->expiry_year; 
            $cvv = $card_info->cvv;

            $response = (object)$this->gwapi->add_subscription($recurring_amount, $ccnumber, $ccexp, $cvv);

                if(isset($response->subscription_id) && !empty($response->subscription_id))
                {
                    $new_date = date("d-m-Y", strtotime($_POST['date']));

                    $add_sub = array('subscription_id'=>$response->subscription_id,
                                     'billing_date'=>$new_date);
                    $this->db->where('id',$user_detail->id);
                    $this->db->update('user_info',$add_sub);  

                    $this->session->set_flashdata('success', "<div class='alert alert-success'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                    Date Update succesfully</div>");
                    redirect(base_url().'change-wholesaler-subscription-date/'.$response->subscription_id.'/'.$user_detail->id);
                }
            
        }

        $type = 'update_subscription';
        $subscription_id = $_POST['subscription_id'];
        $start_date = date("Ymd", strtotime($_POST['date']));

        echo $type;
        echo "<br>";
        echo $subscription_id;
        echo "<br>";
        echo $start_date;
        echo "<br>";

        $machentinfo   = $this->db->where('id',1)->get('tms_admin_payment_setting')->row();
        $this->gwapi->setLogin($machentinfo);

        $response = (object)$this->gwapi->update_subscription($type, $subscription_id, $start_date);

        echo "<pre>"; print_r($response);die();
    }

     public function package_transaction(){ 
         
       if (!empty($_POST)) {
            if ($_POST['package_type']==2) {
                       $data['transaction']= $this->db->from('package_transaction')
                        ->join('hana_package', 'package_transaction.package_id = hana_package.id')
                        ->join('user_info', 'package_transaction.user_id = user_info.id')
                        ->where('package_transaction.package_id', 2)
                        ->get()->result();
          }else if ($_POST['package_type']==1) {
                     $data['transaction']= $this->db->from('package_transaction')
                        ->join('hana_package', 'package_transaction.package_id = hana_package.id')
                        ->join('user_info', 'package_transaction.user_id = user_info.id')
                        ->where('package_transaction.package_id', 1)
                        ->get()->result();
                       
            }else{
                 $data['transaction']= $this->db->from('package_transaction')
                        ->join('hana_package', 'package_transaction.package_id = hana_package.id')
                        ->join('user_info', 'package_transaction.user_id = user_info.id')
                        ->where('package_transaction.package_id', 3)
                        ->get()->result();
                        
            }
        }else{
                $data['transaction']= $this->db->from('package_transaction')
                        ->join('hana_package', 'package_transaction.package_id = hana_package.id')
                        ->join('user_info', 'package_transaction.user_id = user_info.id')->get()->result();
                        
            
        }
        $this->load->view('super_admin/list/list_package_transaction', $data);
        
    }

    public function wholesaler_package_transaction(){ 

             if (!empty($_POST)) {
                  
                    if ($_POST['package_type']==1) {

                        $data['wholesaler_transaction_list']= $this->db->from('wholesaler_package_transaction')
                        ->join('wholesaler_hana_package', 'wholesaler_package_transaction.package_id = wholesaler_hana_package.id')
                        ->join('user_info', 'wholesaler_package_transaction.user_id = user_info.id')
                        ->where('wholesaler_package_transaction.package_id', 1)
                        ->get()->result();

                    }else{
                        
                        $data['wholesaler_transaction_list']= $this->db->from('wholesaler_package_transaction')
                        ->join('wholesaler_hana_package', 'wholesaler_package_transaction.package_id = wholesaler_hana_package.id')
                        ->join('user_info', 'wholesaler_package_transaction.user_id = user_info.id')
                        ->where('wholesaler_package_transaction.package_id', 2)
                        ->get()->result();
                       
                     }
            }else{
            $data['wholesaler_transaction_list']= $this->db->from('wholesaler_package_transaction')
                        ->join('wholesaler_hana_package', 'wholesaler_package_transaction.package_id = wholesaler_hana_package.id')
                        ->join('user_info', 'wholesaler_package_transaction.user_id = user_info.id')->get()->result();
           

            }
                    
              $this->load->view('super_admin/list/wholesaler_list_package_transaction', $data);
        

           
    
    }

    


}
