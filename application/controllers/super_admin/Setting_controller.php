<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();
        $this->load->model('super_admin/Super_Setting_model');
        $this->user_id=$this->session->userdata('user_id');
         $session_id = $this->session->userdata('session_id');
        $user_type = $this->session->userdata('user_type');
        if ($this->session->userdata('isAdmin') == 1) {
            $this->level_id = $this->session->userdata('user_id');
        } else {
            $this->level_id = $this->session->userdata('admin_created_by');
        }
        $this->load->library('Twilio');
        //$this->load->model('super_admin/Package_model');

    }
  public function sms(){ 
        $this->load->view('super_admin/setting/sms');
     
    }
    public function email(){ 
       //  $this->permission->check_label('custom_sms')->create()->redirect();
        $data['customer_email_c'] = $this->Super_Setting_model->get_customer_c();
        $data['customer_email_b'] = $this->Super_Setting_model->get_customer_b();
        $this->load->view('super_admin/setting/email',$data);    
    }

    public function sms_configure(){ 
      //  $this->permission->check_label('sms_configuration')->create()->redirect();
        $data['get_sms_conf'] = $this->Super_Setting_model->get_sms_conf();
        $this->load->view('super_admin/setting/sms_configure',$data);
   }
    public function sms_config_save() {
        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');
        $test_sms_number = $this->input->post('test_sms_number');
        $user_id=$this->session->userdata('user_id');
        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => 0,
            'status' => 1,
            'created_by' =>$user_id,
            'test_sms_number' => $test_sms_number,
        );
        print_r($config_data);
       // die();
        $this->db->insert('sms_gateway', $config_data);

        //   ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "sms configuration insert";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        print_r($accesslog_info);
       // die();
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway save successfully!</div>");
       // die();
        redirect("super-admin/sms-configure");
    }

    //  public function sms_configure() {
    //     $this->permission->check_label('sms_configuration')->create()->redirect();
    //     $data['get_sms_config'] = $this->settings->get_sms_config();
    //      print_r($data['get_sms_config']);
    //      die();
    //      $this->load->view('super_admin/setting/sms_configure',$data);
       
    // }

    public function sms_config_verified($gateway_id) {
        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('gateway_id', $gateway_id)->where('created_by', $this->session->userdata('user_id'))->get()->row();
        if(!$sms_gateway_info){
            redirect("super-admin/sms-configure");
        }
      
        $this->twilio->initialize_twillio($sms_gateway_info->user, $sms_gateway_info->password,$sms_gateway_info->phone);
             $from = $sms_gateway_info->phone; //'+12062024567';
      
        $to = $sms_gateway_info->test_sms_number;
        $message = "Thanks for SMS Verified";
          
        if ($sms_gateway_info->provider_name == 'Twilio'){

            $response = $this->twilio->sms($from, $to, $message);
            print_r($response->IsError);
            if ($response->IsError) {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".$response->ErrorMessage."</div>");
            } else {
                $sms_data = array(
                    'from' => $from,
                    'to' => $to,
                    'message' => $message,
                    'created_by' =>$this->session->userdata('user_id'),
                );
                 $this->db->insert('custom_sms_tbl', $sms_data);
          
                
                // For Verified Status : START
                $config_data = array(
                    'is_verify' => 1,
                );
                $this->db->where('gateway_id', $gateway_id);
                $this->db->where('created_by', $this->session->userdata('user_id'));
                $this->db->update('sms_gateway', $config_data);
                // For Verified Status : END

                $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS Configuration verified successfully!</div>");
            }
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS Configuration not verified successfully!</div>");
        }

        redirect("super-admin/sms-configure");
    }
    // ============== Verified 


     public function sms_config_edit($gateway_id) {
        $data['sms_config_edit'] = $this->Super_Setting_model->sms_config_edit($gateway_id); 
        if(!$data['sms_config_edit']){
            redirect("super-admin/sms-configure");
        }     
        //echo "string";
        $this->load->view('super_admin/setting/sms_configure_edit',$data);
    }

    public function sms_config_update($gateway_id) {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "updated";
        $remarks = "sms configuration updated";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //  ============== close access log info =================

        $provider_name = $this->input->post('provider_name');
        $user_name = $this->input->post('user_name');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $sender_name = $this->input->post('sender_name');
        $is_active = $this->input->post('is_active');
        $test_sms_number = $this->input->post('test_sms_number');

        $config_data = array(
            'provider_name' => $provider_name,
            'user' => $user_name,
            'password' => $password,
            'phone' => $phone,
            'authentication' => $sender_name,
            'default_status' => $is_active,
            'status' => 1,
            'is_verify' => 0,
            'test_sms_number' => $test_sms_number,
        );
        $this->db->where('gateway_id', $gateway_id);
        $this->db->update('sms_gateway', $config_data);

        $default_status = array(
            'default_status' => 0,
        );
        $this->db->where('gateway_id !=', $gateway_id);
        $this->db->where('created_by', $this->user_id);
        $this->db->update('sms_gateway', $default_status);

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway updated successfully!</div>");
        redirect("super-admin/sms-configure");
    }

    public function sms_config_delete($gateway_id){
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "deleted";
        $remarks = "sms configuration deleted";
        //print_r($action_page);
        //die();
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->session->userdata('user_id'),
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        print_r($accesslog_info);
        //die();
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $this->db->where('gateway_id', $gateway_id);
        $this->db->where('created_by',$this->session->userdata('user_id'));
        $this->db->delete('sms_gateway');

        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS gateway deleted successfully!</div>");
        redirect("super-admin/sms-configure");
    }



    public function mail() {
       //  $this->permission->check_label('email')->create()->redirect();
        $data['get_mail_config'] = $this->Super_Setting_model->get_mail_config();

        $this->load->view('super_admin/setting/mail',$data);
    }
     public function update_mail_configure() {
        $updated_date = date('Y-m-d');
        $protocol = $this->input->post('protocol');
        $smtp_host = $this->input->post('smtp_host');
        $smtp_port = $this->input->post('smtp_port');
        $smtp_user = $this->input->post('smtp_user');
        $smtp_pass = $this->input->post('smtp_pass');
        $mailtype = $this->input->post('mailtype');

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
        $action_done = "insert";
        $remarks = "Email configuration save";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        $emailConfig = $this->db->select('*')->from('mail_config_tbl')->where('created_by', $this->user_id)->get()->row();
        $mail_config_data = array(
            'protocol' => $protocol,
            'smtp_host' => $smtp_host,
            'smtp_port' => $smtp_port,
            'smtp_user' => $smtp_user,
            'smtp_pass' => $smtp_pass,
            'mailtype' => $mailtype,
            'updated_by' => $this->user_id,
            'created_by' => $this->user_id,
            'updated_date' => $updated_date,
            'is_verified' => 0,
        );
        if($emailConfig) {
            $this->db->where('created_by', $this->user_id);
            $this->db->update('mail_config_tbl', $mail_config_data);
        } else {
            $this->db->insert('mail_config_tbl', $mail_config_data);
        }
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Mail configuration updated successfully!</div>");
        redirect("super-admin/mail");
    }

    public function verify()
    {
        //============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "Test email send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        //============== close access log info =================
        $to_email = $this->input->post('email_id');
        $message = "Mail configuration verification";
        if($this->Super_Setting_model->sendCstomEmail($to_email, $message, false)) { 
            $mail_config_data = array(
                'is_verified' => 1,
            );
            $this->db->where('created_by', $this->user_id);
            $this->db->update('mail_config_tbl', $mail_config_data);
            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Configuration verified successfully!</div>");
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Error in configuration verification. please check and try again!</div>");
        }
        redirect("super-admin/mail");
    }
     public function sms_send() {
        // ============ its for access log info collection ===============
        $action_page = $this->uri->segment(1);
        $action_done = "insert";
        $remarks = "custom sms send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
        // ============== close access log info =================
        $receiver = $this->input->post('receiver_id');
        $message = $this->input->post('message');
              if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
            $from = $sms_gateway_info->phone; //'+12062024567';
            $to = $receiver;
            $message = $message;

            if ($sms_gateway_info->provider_name == 'Twilio') {
                $response = $this->twilio->sms($from, $to, $message);
                if ($response->IsError) {
                    echo 'Error: ' . $response->ErrorMessage;
                    $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".$response->ErrorMessage."</div>");
                } else {
                    $sms_data = array(
                        'from' => $from,
                        'to' => $to,
                        'message' => $message,
                        'created_by' => $this->user_id,
                    );
                    $this->db->insert('custom_sms_tbl', $sms_data);
                    $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                }
            } else {
                $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
            }
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }
        
        redirect("super-admin/sms");
    }
    

    //    =========== its for sms_csv_upload ==============
    public function sms_csv_upload(){
        $count = 0;
        $fp = fopen($_FILES['upload_csv_file']['tmp_name'], 'r') or die("can't open file");
        if (($handle = fopen($_FILES['upload_csv_file']['tmp_name'], 'r')) !== FALSE) {
            while ($csv_line = fgetcsv($fp, 1024)) {
                //keep this if condition if you want to remove the first row
                for ($i = 0, $j = count($csv_line); $i < $j; $i++) {
                    $insert_csv = array();
                    $insert_csv['to'] = (!empty($csv_line[0]) ? $csv_line[0] : null);
                    $insert_csv['message'] = (!empty($csv_line[1]) ? $csv_line[1] : null);
                }

                if ($count > 0) {

                    if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
                        $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
                        
                        $from = $sms_gateway_info->phone; //'+12062024567';
                        $to = $this->phone_fax_mobile_no_generate($insert_csv['to']); //$insert_csv['to'];
                        $message = $insert_csv['message'];

                        if ($sms_gateway_info->provider_name == 'Twilio') {
                            $response = $this->twilio->sms($from, $to, $message);
                            if ($response->IsError) {
                                echo 'Error: ' . $response->ErrorMessage;
                            } else {
                                $sms_data = array(
                                    'from' => $from,
                                    'to' => $to,
                                    'message' => $message,
                                    'created_by' => $this->user_id,
                                );
                                $this->db->insert('custom_sms_tbl', $sms_data);
                            }
                        } else {
                            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms not send successfully!</div>");
                        }
                    }else{
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
                    }    
                }
                $count++;
            }
        }
        fclose($fp) or die("can't close file");
        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>sms send successfully!</div>");
        redirect('super-admin/sms');
    }
    
    //    =========== its for group_sms_send ============
    public function group_sms_send() {
        $customer_phone = $this->input->post('customer_phone');
        $message = $this->input->post('message');
        if(isset($this->config->config['twilio']['account_sid']) &&  $this->config->config['twilio']['account_sid'] != ''){
            $sms_gateway_info = $this->db->select('*')->from('sms_gateway')->where('created_by', $this->session->userdata('user_id'))->where('is_verify', '1')->where('default_status', 1)->get()->row();
            $from = $sms_gateway_info->phone; //'+12062024567';
            for ($i = 0; $i < count($customer_phone); $i++) {
                $to = $customer_phone[$i];
                if($to != 'All'){
                    $message = $message;

                    if ($sms_gateway_info->provider_name == 'Twilio') {
                        $response = $this->twilio->sms($from, $to, $message);
                        if ($response->IsError) {
                            echo 'Error: ' . $response->ErrorMessage;
                        } else {
                            $sms_data = array(
                                'from' => $from,
                                'to' => $to,
                                'message' => $message,
                                'created_by' => $this->user_id,
                            );
                            $this->db->insert('custom_sms_tbl', $sms_data);
                            $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS send successfully!</div>");
                        }
                    } else {
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>SMS not send successfully!</div>");
                    }
                }    
            }
        }else{
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>".SMS_VERIFICATION_ERROR."</div>");
        }    
        redirect('super-admin/sms');
    }
    function User_Type(){
            $user_cetogry_type = $this->input->post('val');
            $user_types['user_data'] = $this->Super_Setting_model->user_type_data($user_cetogry_type);
            //print_r($user_types['user_data']);
             $this->load->view('super_admin/setting/sms_usertype',$user_types);
    }


   public function email_send() {
       //============ its for access log info collection ===============
        $action_page = $this->uri->segment(2);
       
        $action_done = "insert";
        $remarks = "custom email send";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);

        //============== close access log info =================
        $customer_email = $this->input->post('customer_email');
        $message = $this->input->post('message');
        $custom_email = $this->input->post('custom_email');
        if(!$this->input->post('customer_email')) { 
            $customer_email = array();
        }
        if($custom_email != '') {
            $custom_email = explode(',', $custom_email);
            $customer_email = array_merge($customer_email, $custom_email);

        }

        if(count($customer_email) > 0) {
            for ($i = 0; $i < count($customer_email); $i++) {
                $to_email = $customer_email[$i];
                if ($to_email != '' && $to_email != 'All') {
                    if($this->Super_Setting_model->sendCstomEmail($to_email, $message)) {
                        die();
                        $this->session->set_flashdata('success', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email send successfully!</div>");
                    } else {
                        $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Email not send successfully!</div>");
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Please select atleast one record!</div>");
        }
        redirect("super-admin/email");
    }

      public function super_admin_users(){

       // $this->permission->check_label('employees')->create()->redirect();  
        $limit = 25;
        @$start = ($this->uri->segment(2) ? $this->uri->segment(2) : 0);
       // $config = $this->pasination($limit, 'user_info', 'super-admin-users');
         // print_r($config);
       // die();
       // $this->pagination->initialize($config);
      // $data["links"] = $this->pagination->create_links();
       $data['users'] = $this->Super_Setting_model->get_userlist($limit, $start);
        $this->load->view('super_admin/setting/super_admin_users',$data);
    }

      


 public function save_user() {      
        $this->form_validation->set_rules('first_name', 'Firstname', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Lastname', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        $this->form_validation->set_rules('address', 'Address', 'max_length[1000]');

        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');
        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');


        // print_r($created_date);echo "<br>";
        // print_r($first_name);echo "<br>";
        // print_r($last_name);echo "<br>";
        // print_r($email);echo "<br>";
        // print_r($password);echo "<br>";
        // print_r($address);echo "<br>";
        // print_r($phone);echo "<br>";
        // print_r($fixed_commission);echo "<br>";
        // print_r($percentage_commission);echo "<br>";

        // if (@$_FILES['user_img']['name']) {
        //     $config['upload_path']   = 'assets/b_level/img/';
        //     $config['allowed_types'] = 'jpg|jpeg|png';
        //     $config['overwrite']     = false;
        //     $config['max_size']      = 6200;
        //     $config['remove_spaces'] = true;
        //     $config['max_filename']   = 10;
        //     $config['file_ext_tolower'] = true;
        //     $this->upload->initialize($config);
        //     $this->load->library('upload', $config);
        //     if (!$this->upload->do_upload('user_img'))
        //     {
        //        $error = $this->upload->display_errors();
        //        $this->session->set_flashdata('exception',$error);
        //        redirect("profile-setting");
        //     } else {
        //      $data = $this->upload->data();
        //      $image = $config['upload_path'].$data['file_name'];
        //     }
        //     } else {
        //         $image = "";
        //     }
        //=========== its for save user info ============
        $user = (object) $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'address' => $address,
            'language' => 'English',
            'phone' => $phone,
            'created_by' => $this->user_id,
            'user_type' => 's',
            'fixed_commission' => $fixed_commission,
            'percentage_commission' => $percentage_commission,
            'create_date' => $created_date
        );
        if ($this->form_validation->run()) {
            $this->db->insert('user_info', $user_data);
            $user_id = $this->db->insert_id();


            // =========== its for save log info ============
            $loginfo = array(
                'user_id' => $user_id,
                'email' => $email,
                'password' => md5($password),
                'user_type' => 's',
                'is_admin' => 2,
            );
            $this->db->insert('log_info', $loginfo);
//        =========== its for customer user info send by email ============  
            if ($email) {
                $data['get_mail_config'] = $this->Super_Setting_model->get_mail_config();

                $this->Super_Setting_model->sendLink_super($user_id, $data, $email, $password);              
            }
            //        ============ its for access log info collection ===============
            $action_page = $this->uri->segment(1);
            $action_done = "insert";
            $remarks = "user information save";
            $accesslog_info = array(
                'action_page' => $action_page,
                'action_done' => $action_done,
                'remarks' => $remarks,
                'user_name' => $this->user_id,
                'level_id' => $this->level_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'entry_date' => date("Y-m-d H:i:s"),
            );
            $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================

            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('super-admin-users');
        } else {

            $data['user'] = $user;
            $this->load->view('super_admin/setting/new_user', $data);
        }
    }
     public function edit_user($user_id) {

        $data['user'] = $this->Super_Setting_model->get_user_by_id($user_id);
        $this->load->view('super_admin/setting/edit_user', $data);
    }


    public function update_user() {
        // $this->form_validation->set_rules('first_name', 'Firstname','required|max_length[50]');
        // $this->form_validation->set_rules('last_name', 'Lastname','required|max_length[50]');
        // $this->form_validation->set_rules('email', 'Email','required|valid_email|is_unique[log_info.email]|max_length[100]');
        // $this->form_validation->set_rules('password', 'Password','required|max_length[32]');
        // $this->form_validation->set_rules('address', 'Address','max_length[1000]');

        $created_date = date('Y-m-d');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
//        if($password == NULL){
//            echo "disi";
//        }else{
//            echo "nai";
//        }die();
         $oldpass = $this->input->post('oldpassword');
        $address = $this->input->post('address');
        $phone = $this->input->post('phone');

        $user_id = $this->input->post('user_id');
        $redirect_url = $this->input->post('redirect_url');

        $fixed_commission = $this->input->post('fixed_commission');
        $percentage_commission = $this->input->post('percentage_commission');
        // $config['upload_path']          = './assets/';
        // $config['allowed_types']        = 'gif|jpg|png';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        // $this->load->library('upload', $config);
        // if ( ! $this->upload->do_upload('userfile'))
        // {
        //        $this->session->set_flashdata('exception', "<div class='alert alert-danger'>
        //     <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
        //     ". $this->upload->display_errors()."!</div>");
        //     redirect($redirect_url);
        // }
        // else
        // {
        //         $data = array('upload_data' => $this->upload->data());
        //         print_r($data); exit;
        // }
        //=========== its for save user info ============
        $user = (object) $user_data = array(
            'id' => $user_id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'user_image' => '', //@$image,
            'email' => $email,
            'address' => $address,
            'phone' => $phone,
            'created_by' => $this->user_id,
            'user_type' => 's',
            'fixed_commission' => $fixed_commission,
            'percentage_commission' => $percentage_commission,
            'create_date' => $created_date
        ); 
        $this->db->where('id', $user_id)->update('user_info', $user_data);
//        (!empty($this->input->post('password'))?$npassword:$oldpass),
        // =========== its for save log info ============(!empty($npassword)?$npassword:$oldpass),
        $loginfo = array(
            'user_id' => $user_id,
            'email' => $email,
//            'password' => md5($password),
            'password' => (!empty($this->input->post('password'))?$password:$oldpass),
            'user_type' => 's',
            'is_admin' => 2,
        );
        $this->db->where('user_id', $user_id)->update('log_info', $loginfo);
     

        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "updated";
        $remarks = "user information updated ";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================
        if ($email) {
            $data['get_mail_config'] = $this->Super_Setting_model->get_mail_config();
            $this->Super_Setting_model->sendLink_super($user_id, $data, $email, $password);
        }

        $this->session->set_flashdata('message', "<div class='alert alert-success'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
            User update successfully!</div>");

        redirect('super-admin-users');
    }

     public function delete_user($id) {
        $this->db->where('user_id', $id)->delete('log_info');
        $this->db->where('id', $id)->delete('user_info');
        $this->db->where('user_id', $id)->delete('b_user_access_tbl');
        //        ============ its for access log info collection ===============
        $action_page = $this->uri->segment(3);
        $action_done = "deleted";
        $remarks = "user information deleted";
        $accesslog_info = array(
            'action_page' => $action_page,
            'action_done' => $action_done,
            'remarks' => $remarks,
            'user_name' => $this->user_id,
            'level_id' => $this->level_id,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'entry_date' => date("Y-m-d H:i:s"),
        );
        $this->db->insert('accesslog', $accesslog_info);
//        ============== close access log info =================       
        
        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User deleted successfully!</div>");
        redirect('super-admin-users');
    }

    //    ============ its for user_filter ===========

    public function user_filter() {
        $data['first_name'] = $this->input->post('first_name');
        $data['email'] = $this->input->post('email');
        $data['type'] = $this->input->post('type');
        $data['get_user_filter_data'] = $this->Super_Setting_model->user_filter($data['first_name'], $data['email'], $data['type']); 
        $this->load->view('super_admin/setting/user_filter', $data);
    }


}
