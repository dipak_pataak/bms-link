<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class B_user_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();
    }

    function b_user_list()
    {
        /*
          $this->load->model('super_admin/User_model');


          $config["base_url"] = base_url('super-admin/wholesaler/list');
          $config["total_rows"] =  $this->User_model->b_user_list_count();
          $config["per_page"] = 10;
          $config["uri_segment"] = 3;
          $config["last_link"] = "Last";
          $config["first_link"] = "First";
          $config['next_link'] = 'Next';
          $config['prev_link'] = 'Prev';
          $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
          $config['full_tag_close'] = '</ul></nav></div>';
          $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['num_tag_close'] = '</span></li>';
          $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
          $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
          $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
          $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['prev_tagl_close'] = '</span></li>';
          $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['first_tagl_close'] = '</span></li>';
          $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
          $config['last_tagl_close'] = '</span></li>';

          $this->pagination->initialize($config);


          $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;


          $data["list_data"] = $this->User_model->b_user_list($config["per_page"], $page);
          $data["links"] = $this->pagination->create_links();
          $data['pagenum'] = $page; */

        $data = [];

        $this->load->view('super_admin/list/b_user_list', $data);
    }

    function search_b_user_list_api()
    {
        $this->load->model('super_admin/User_model');
        $response      = $this->User_model->search_b_user_list($_POST);
        $totalFiltered = $response['total'];
        $json_data     = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function add_edit_b_user_view($b_user_id = 0)
    {


        $data = [];
        if(!empty($b_user_id))
        {
            $userData     = $this->db->where('id', $b_user_id)->get('user_info')->row();
            $data['data'] = $userData;



        }
        $data['b_user_list'] = $this->db->query('SELECT * FROM user_info where id != ' . $b_user_id . ' AND user_type_status = 1 AND user_type = "b" AND ( bridge_b IS NULL OR bridge_b = ' . $b_user_id . ')')->result();

        $this->load->view('super_admin/Single/add_edit_b_user', $data);
    }

    function update_wholesales_bridge_user($user_id,$bridge_id)
    {

        $this->db->where('id', $user_id)->update('user_info',['bridge_b' => $bridge_id]);

        return true;
    }

    function b_user_save()
    {
        $this->load->model('super_admin/User_model');
        $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
        if(!isset($_POST['id']) || empty(trim($_POST['id'])))
        {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        }
        $this->form_validation->set_rules('address', 'Address', 'max_length[1000]');
        if($this->form_validation->run() == true)
        {
            if(isset($_POST['id']) && !empty(trim($_POST['id'])))
            {
                $where['email'] = $_POST['email'];
                $check          = $this->User_model->check_column_exist($where, 'user_id', $_POST['id'], 'log_info');
                if(!empty($check))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Email already exist']);
                    exit();
                }
            }


            $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();
            if(!empty($reserve_domain_exist))
            {
                echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
                exit();
            }


            if(isset($_POST['id']) && !empty(trim($_POST['id'])))
            {
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->where('id !=', $_POST['id'])->get()->result();
            }
            else
            {
                $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
                if(!empty($sub_domain_exist))
                {
                    echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
                    exit();
                }
            }

            if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
            {
                echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
                exit();
            }

            $user_data['first_name'] = $_POST['first_name'];
            $user_data['last_name']  = $_POST['last_name'];
            $user_data['email']      = $_POST['email'];
            $user_data['address']    = $_POST['address'];
            $user_data['phone']      = $_POST['phone'];

            $user_data['company']          = $_POST['company'];
            $user_data['city']             = $_POST['city'];
            $user_data['state']            = $_POST['state'];
            $user_data['zip_code']         = $_POST['zip_code'];
            $user_data['country_code']     = $_POST['country_code'];
            $user_data['user_type_status'] = $_POST['user_type_status'];
            $user_data['sub_domain']       = $_POST['sub_domain'];

            if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
            {
                $user_data['bridge_b'] = $_POST['bridge_b'];
            }

            if(!isset($_POST['id']) || empty(trim($_POST['id'])))
            {

                $user_data['user_type']   = 'b';
                $user_data['create_date'] = date('Y-m-d');
                $user_data['language']    = 'English';
                $this->db->insert('user_info', $user_data);
                $user_id = $this->db->insert_id();

                if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
                {
                    $this->update_wholesales_bridge_user($_POST['bridge_b'],$user_id);
                }


                // Add Default shipping info while create b user : START
                $this->load->model('Common_model');
                $this->Common_model->default_shipping_method($user_id);                
                // Add Default shipping info while create b user : END


                $this->b_level_acc_coa($user_id);
                $this->b_level_menu_transfer($user_id);
                $loginfo = array('user_id' => $user_id, 'email' => $_POST['email'], 'password' => md5($_POST['password']), 'user_type' => 'b', 'is_admin' => 1);

                $this->db->insert('log_info', $loginfo);

                //============ its for access log info collection ===============
                $accesslog_info = array('action_page' => $this->uri->segment(1), 'action_done' => 'insert', 'remarks' => "User information save", 'user_name' => $user_id, 'entry_date' => date("Y-m-d H:i:s"),);
                $this->db->insert('accesslog', $accesslog_info);

                echo json_encode(['status' => 201, 'msg' => 'User details has been added successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
                exit();
            }
            else
            {
                //passworde update
                if($_POST['password'] != '')
                {
                    if($_POST['password'] == $_POST['confirm'])
                    {
                        $pass_data['password'] = md5($_POST['password']);
                        $this->db->where('user_id', $_POST['id'])->update('log_info', $pass_data);
                    }
                    else
                    {
                        echo json_encode(['status' => 400, 'msg' => 'Password And confirm password not same']);
                        exit();
                    }

                }
                //passworde update
                $this->db->where('id', $_POST['id'])->update('user_info', $user_data);

                if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
                {
                    $this->update_wholesales_bridge_user($_POST['bridge_b'],$_POST['id']);
                }

                echo json_encode(['status' => 201, 'msg' => 'User details has been updated successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
                exit();
            }
        }
        else
        {
            $error_arr = $this->form_validation->error_array();
            foreach($error_arr as $key => $msg)
            {
                echo json_encode(['status' => 400, 'msg' => $msg]);
                exit();
                break;
            }
        }


     /*   $created_date = date('Y-m-d');
        $first_name   = $this->input->post('first_name');
        $last_name    = $this->input->post('last_name');
        $email        = $this->input->post('email');
        $password     = $this->input->post('password');
        $address      = $this->input->post('address');
        $phone        = $this->input->post('phone');
        $company      = $this->input->post('company');
        $city         = $this->input->post('city');
        $state        = $this->input->post('state');
        $zip_code     = $this->input->post('zip_code');
        $country_code = $this->input->post('country_code');


        $user_id = $this->input->post('user_id');

        //=========== its for save user info ============
        $user = (object)$user_data =
            array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'user_type' => 'b', 'create_date' => $created_date, 'language' => 'English', 'company' => $company, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'country_code' => $country_code);


        if($this->form_validation->run())
        {

            if(!isset($_POST['id']) || empty(trim($_POST['id'])))
            {

                $this->db->insert('user_info', $user_data);
                $user_id = $this->db->insert_id();
                $this->b_level_acc_coa($user_id);
                $this->b_level_menu_transfer($user_id);
                // =========== its for save log info ============
                $loginfo = array('user_id' => $user_id, 'email' => $email, 'password' => md5($password), 'user_type' => 'b', 'is_admin' => 1);

                $this->db->insert('log_info', $loginfo);

                //============ its for access log info collection ===============
                $accesslog_info = array('action_page' => $this->uri->segment(1), 'action_done' => 'insert', 'remarks' => "User information save", 'user_name' => $user_id, 'entry_date' => date("Y-m-d H:i:s"),);
                $this->db->insert('accesslog', $accesslog_info);
            }
            else
            {
                $user_data = array('first_name' => $first_name, 'last_name' => $last_name, 'address' => $address, 'city' => $city, 'state' => $state, 'zip_code' => $zip_code, 'country_code' => $country_code);

                $this->db->where('id', $_POST['id'])->update('user_info', $user_data);
            }


            $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>User save successfully!</div>");
            redirect('super-admin/wholesaler/list');
        }
        else
        {
            $data = [];
            $this->session->set_flashdata('form_validation_error', $this->form_validation->error_array());
            $this->load->view('super_admin/Single/add_edit_b_user', $data);
        }*/
    }

    public function b_level_acc_coa($user_insert_id)
    {

        $get_all_acc_coa = $this->db->get('acc_coa_default')->result();
        //        $menu_title = $get_all_acc_coa
        foreach($get_all_acc_coa as $single)
        {

            $transfer_acc_coa = array('HeadCode' => $single->HeadCode, 'HeadName' => $single->HeadName, 'PHeadName' => $single->PHeadName, 'HeadLevel' => $single->HeadLevel, 'IsActive' => $single->IsActive, 'IsTransaction' => $single->IsTransaction, 'IsGL' => $single->IsGL, 'HeadType' => $single->HeadType, 'IsBudget' => $single->IsBudget, 'IsDepreciation' => $single->IsDepreciation, 'DepreciationRate' => $single->DepreciationRate, 'level_id' => $user_insert_id, 'CreateBy' => $user_insert_id, 'CreateDate' => date('Y-m-d'), 'UpdateBy' => $this->session->userdata('user_id'), 'UpdateDate' => date('Y-m-d'));
            //            echo '<pre>';            print_r($transfer_acc_coa);echo '</pre>';
            $this->db->insert('b_acc_coa', $transfer_acc_coa);
        }
        //   echo $this->db->last_query();
        //        dd($get_all_c_default_menu);
        return true;
    }

    public function b_level_menu_transfer($user_insert_id)
    {

        $this->db->where('level_id', $user_insert_id)->delete('b_menusetup_tbl');

        $get_all_b_default_menu = $this->db->get('b_menusetup_tbl_default')->result();
        //print_r($get_all_b_default_menu);
        //die('here');
        //        $menu_title = $get_all_c_default_menu
        foreach($get_all_b_default_menu as $single)
        {
            $transfer_menu = array('menu_id' => $single->id, 'menu_title' => $single->menu_title, 'korean_name' => $single->korean_name, 'page_url' => $single->page_url, 'module' => $single->module, 'ordering' => $single->ordering, 'parent_menu' => $single->parent_menu, 'menu_type' => $single->menu_type, 'is_report' => $single->is_report, 'icon' => $single->icon, 'status' => $single->status, 'level_id' => $user_insert_id, 'created_by' => $user_insert_id, 'create_date' => date('Y-m-d'));
            //echo $this->db->last_query();
            //            echo '<pre>';            print_r($transfer_menu);echo '</pre>';
            $this->db->insert('b_menusetup_tbl', $transfer_menu);
            // echo '<br>/n';
        }
        //  dd($get_all_c_default_menu);
        // die('here');
        return true;
    }

    function not_assign_b_user_list()
    {
        $data['b_user_list'] = $this->db->query('SELECT * FROM user_info where user_type_status = 1 AND user_type = "b" AND bridge_b IS NULL  AND id not in (SELECT bridge_b from user_info where bridge_b IS NOT NULL)')->result();
        $this->load->view('super_admin/not_assign_b_user_list', $data);
    }

    function user_status_change($user_id)
    {
        $log_info_detail = $this->db->select('*')->from('log_info')->where('user_id', $user_id)->get()->row();
        if($log_info_detail->status == 0)
        {
            $is_update = $this->db->where('user_id', $user_id)->update('log_info', array('status' => 1));
        }
        if($log_info_detail->status == 1)
        {
            $is_update = $this->db->where('user_id', $user_id)->update('log_info', array('status' => 0));
        }

        if($is_update)
        {
            echo json_encode(true);
        }
    }

    function user_delete($user_id)
    {
        $is_delete = $this->db->where('user_id', $user_id)->delete('log_info');
        if($is_delete)
        {
            $this->db->where('id', $user_id)->delete('user_info');
        }
        echo json_encode(true);
    }

    function edit_company_detail($user_id)
    {
        $data['data'] = $this->db->select('*')->from('company_profile')->where('user_id', $user_id)->get()->row();
        $data['user_detail'] = $this->db->where('id', $user_id)->get('user_info')->row();
        $data['user_id'] = $user_id;
        $this->load->view('super_admin/Single/edit_compny_detail', $data);
    }

    function update_company_detail()
    {
        $data['user_id']      = $_POST['user_id'];
        $data['company_name'] = $_POST['company_name'];
        $data['email']        = $_POST['email'];
        $data['phone']        = $_POST['phone'];
        $data['unit']         = $_POST['unit'];
        $data['currency']     = $_POST['currency'];
        $data['address']      = $_POST['address'];
        $data['city']         = $_POST['city'];
        $data['state']        = $_POST['state'];
        $data['zip_code']     = $_POST['zip_code'];
        $data['country_code'] = $_POST['country_code'];

         /* Image upload code */
            if (!empty($_FILES['image']['tmp_name'])) {
            $tempFile = $_FILES['image']['tmp_name']; 
            $filename = $_FILES['image']['name'];
            $file_parts = explode(".", $filename);
            $ext = end($file_parts);
            
            $target_dir = FCPATH . 'assets/b_level/uploads/appsettings/';
            $target_file = $target_dir . $_POST['user_id'] . '.' . $ext;
            $profile_img_path_temp = $_POST['user_id'].'.'.$ext;

                $movefile = move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
                if(!empty($movefile))
                {
                    $data['logo'] = $profile_img_path_temp;
                }
            
            }
            /* image code end */



        if(isset($_POST['company_id']) && !empty($_POST['company_id']))
        {
            $this->db->where('company_id', $_POST['company_id'])->update('company_profile', $data);
        }else{
            $this->db->insert('company_profile',$data);
        }
        
        echo json_encode(['status' => 201, 'msg' => 'Company details has been added successfully']);
    }


    function login_b_user($user_id)
    {
        $check = check_admin_auth();
        if ($check==true) 
        {
            $token = generate_token_for_user_login($user_id);
            $user_detail = $this->db->select('*')->from('user_info')->where('id', $user_id)->get()->row();

                $http_host = explode('.', $_SERVER['HTTP_HOST'])[1];
                $ext = explode('.', $_SERVER['HTTP_HOST'])[2];

            redirect($_SERVER['REQUEST_SCHEME'].'://'.$user_detail->sub_domain.'.'.$http_host.'.'.$ext.'/Switch_controller/b_level_redirection/'.$user_id.'/'.$token);
        }
    }

    

}
