<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();
    }

    function coupon_list()
    {
        $data = [];
        $this->load->view('super_admin/list/coupon_list', $data);
    }

    function search_coupon_list_api()
    {
        $this->load->model('super_admin/Coupon_model');
        $response      = $this->Coupon_model->search_coupon_list($_POST);
        $totalFiltered = $response['total'];
        $json_data     = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function add_edit_coupon_view($coupon_id = 0)
    {
        $data = [];
        if(!empty($coupon_id))
        {
            $userData     = $this->db->where('id', $coupon_id)->get('coupon')->row();
            $data['data'] = $userData;
        }
        $this->load->view('super_admin/Single/add_edit_coupon', $data);
    }

    function coupon_save() 
    {
        $this->form_validation->set_rules('coupon', 'Coupon', 'required|max_length[50]');
        if ($this->form_validation->run()) 
        {
            $coupon_exist = $this->db->select('*')->from('coupon')->where('coupon',$_POST['subdomain'])->get()->result();
            if (!empty($coupon_exist)) 
            {
                echo json_encode(['status' => 400, 'msg' => 'Coupon already exist']);
                exit();
            }


            $start_date = date("Y-m-d", strtotime($_POST['start_date']));
            $exp_date = date("Y-m-d", strtotime($_POST['exp_date']));


            $coupon_data = array('coupon' => $_POST['coupon'],
                                 'type' => $_POST['type'],
                                 'flat' => $_POST['flat'],
                                 'percent' => $_POST['percent'],
                                 'max_discount' => $_POST['max_discount'],
                                 'min_purchase' => $_POST['min_purchase'],
                                 'status' => $_POST['status'],
                                 'start_date' => $start_date,
                                 'exp_date' => $exp_date,
                                );


            if (!isset($_POST['id']) || empty(trim($_POST['id']))) 
            {
                $this->db->insert('coupon', $coupon_data);
                $user_id = $this->db->insert_id();
                echo json_encode(['status' => 200, 'msg' => 'Coupon Save Successfully']);
                exit();
            } else {
                $this->db->where('id', $_POST['id'])->update('coupon', $coupon_data);
                echo json_encode(['status' => 200, 'msg' => 'Coupon Update Successfully']);
                exit();
            }

            
        } else {
            $data = [];
            $this->session->set_flashdata('form_validation_error', $this->form_validation->error_array());
            $this->load->view('super_admin/Single/add_edit_reserve_domain', $data);
        }
    }

    // function update_wholesales_bridge_user($user_id,$bridge_id)
    // {

    //     $this->db->where('id', $user_id)->update('user_info',['bridge_b' => $bridge_id]);

    //     return true;
    // }

    // function b_user_save()
    // {
    //     $this->load->model('super_admin/User_model');
    //     $this->form_validation->set_rules('first_name', 'First name', 'required|max_length[50]');
    //     $this->form_validation->set_rules('last_name', 'Last name', 'required|max_length[50]');
    //     if(!isset($_POST['id']) || empty(trim($_POST['id'])))
    //     {
    //         $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[log_info.email]|max_length[100]');
    //         $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
    //     }
    //     $this->form_validation->set_rules('address', 'Address', 'max_length[1000]');
    //     if($this->form_validation->run() == true)
    //     {
    //         if(isset($_POST['id']) && !empty(trim($_POST['id'])))
    //         {
    //             $where['email'] = $_POST['email'];
    //             $check          = $this->User_model->check_column_exist($where, 'user_id', $_POST['id'], 'log_info');
    //             if(!empty($check))
    //             {
    //                 echo json_encode(['status' => 400, 'msg' => 'Email already exist']);
    //                 exit();
    //             }
    //         }


    //         $reserve_domain_exist = $this->db->select('*')->from('reserve_domain')->where('subdomain', $_POST['sub_domain'])->get()->result();
    //         if(!empty($reserve_domain_exist))
    //         {
    //             echo json_encode(['status' => 400, 'msg' => 'This Sub domain Reserved']);
    //             exit();
    //         }


    //         if(isset($_POST['id']) && !empty(trim($_POST['id'])))
    //         {
    //             $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->where('id !=', $_POST['id'])->get()->result();
    //         }
    //         else
    //         {
    //             $sub_domain_exist = $this->db->select('*')->from('user_info')->where('sub_domain', $_POST['sub_domain'])->get()->result();
    //             if(!empty($sub_domain_exist))
    //             {
    //                 echo json_encode(['status' => 400, 'msg' => 'Sub domain already exist']);
    //                 exit();
    //             }
    //         }

    //         if(!preg_match('/^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/', $_POST['sub_domain']))
    //         {
    //             echo json_encode(['status' => 400, 'msg' => 'Special characters Not allowed']);
    //             exit();
    //         }

    //         $user_data['first_name'] = $_POST['first_name'];
    //         $user_data['last_name']  = $_POST['last_name'];
    //         $user_data['email']      = $_POST['email'];
    //         $user_data['address']    = $_POST['address'];
    //         $user_data['phone']      = $_POST['phone'];

    //         $user_data['company']          = $_POST['company'];
    //         $user_data['city']             = $_POST['city'];
    //         $user_data['state']            = $_POST['state'];
    //         $user_data['zip_code']         = $_POST['zip_code'];
    //         $user_data['country_code']     = $_POST['country_code'];
    //         $user_data['user_type_status'] = $_POST['user_type_status'];
    //         $user_data['sub_domain']       = $_POST['sub_domain'];

    //         if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
    //         {
    //             $user_data['bridge_b'] = $_POST['bridge_b'];
    //         }

    //         if(!isset($_POST['id']) || empty(trim($_POST['id'])))
    //         {

    //             $user_data['user_type']   = 'b';
    //             $user_data['create_date'] = date('Y-m-d');
    //             $user_data['language']    = 'English';
    //             $this->db->insert('user_info', $user_data);
    //             $user_id = $this->db->insert_id();

    //             if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
    //             {
    //                 $this->update_wholesales_bridge_user($_POST['bridge_b'],$user_id);
    //             }


    //             $this->b_level_acc_coa($user_id);
    //             $this->b_level_menu_transfer($user_id);
    //             $loginfo = array('user_id' => $user_id, 'email' => $_POST['email'], 'password' => md5($_POST['password']), 'user_type' => 'b', 'is_admin' => 1);

    //             $this->db->insert('log_info', $loginfo);

    //             //============ its for access log info collection ===============
    //             $accesslog_info = array('action_page' => $this->uri->segment(1), 'action_done' => 'insert', 'remarks' => "User information save", 'user_name' => $user_id, 'entry_date' => date("Y-m-d H:i:s"),);
    //             $this->db->insert('accesslog', $accesslog_info);

    //             echo json_encode(['status' => 201, 'msg' => 'User details has been added successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
    //             exit();
    //         }
    //         else
    //         {
    //             //passworde update
    //             if($_POST['password'] != '')
    //             {
    //                 if($_POST['password'] == $_POST['confirm'])
    //                 {
    //                     $pass_data['password'] = md5($_POST['password']);
    //                     $this->db->where('user_id', $_POST['id'])->update('log_info', $pass_data);
    //                 }
    //                 else
    //                 {
    //                     echo json_encode(['status' => 400, 'msg' => 'Password And confirm password not same']);
    //                     exit();
    //                 }

    //             }
    //             //passworde update
    //             $this->db->where('id', $_POST['id'])->update('user_info', $user_data);

    //             if(isset($_POST['bridge_b']) && !empty($_POST['bridge_b']))
    //             {
    //                 $this->update_wholesales_bridge_user($_POST['bridge_b'],$_POST['id']);
    //             }

    //             echo json_encode(['status' => 201, 'msg' => 'User details has been updated successfully', 'url' => base_url('/super-admin/wholesaler/list')]);
    //             exit();
    //         }
    //     }
    //     else
    //     {
    //         $error_arr = $this->form_validation->error_array();
    //         foreach($error_arr as $key => $msg)
    //         {
    //             echo json_encode(['status' => 400, 'msg' => $msg]);
    //             exit();
    //             break;
    //         }
    //     }

    // }

    function coupon_status_change($coupon_id)
    {
        $coupon_info_detail = $this->db->select('*')->from('coupon')->where('id', $coupon_id)->get()->row();
        if($coupon_info_detail->status == 0)
        {
            $is_update = $this->db->where('id', $coupon_id)->update('coupon', array('status' => 1));
        }
        if($coupon_info_detail->status == 1)
        {
            $is_update = $this->db->where('id', $coupon_id)->update('coupon', array('status' => 0));
        }

        if($is_update)
        {
            echo json_encode(true);
        }
    }

    function coupon_delete($coupon_id)
    {
        $is_delete = $this->db->where('id', $coupon_id)->delete('coupon');
        if($is_delete)
        {
            $this->db->where('id', $coupon_id)->delete('coupon');
        }
        echo json_encode(true);
    }

}
