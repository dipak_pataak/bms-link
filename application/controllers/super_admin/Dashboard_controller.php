<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();

    }

    public function index()
    {
        $total_b_user = $this->db->select('*')->from('user_info')->where('user_type','b')->get()->result();
        $data['total_b_user'] = count($total_b_user);
        $total_c_user = $this->db->select('*')->from('user_info')->where('user_type','c')->get()->result();
        $data['total_c_user'] = count($total_c_user);

        $current_month_b_user = $this->db->query("SELECT * FROM user_info WHERE MONTH(create_date) = MONTH(CURRENT_DATE()) AND YEAR(create_date) = YEAR(CURRENT_DATE()) AND user_type = 'b'")->result();
        $data['current_month_b_user'] = count($current_month_b_user);

        $current_month_c_user = $this->db->query("SELECT * FROM user_info WHERE MONTH(create_date) = MONTH(CURRENT_DATE()) AND YEAR(create_date) = YEAR(CURRENT_DATE()) AND user_type = 'c'")->result();
        $data['current_month_c_user'] = count($current_month_c_user);
        
        $data['package_list'] = $this->db->select('*')->from('hana_package')->get()->result();

        $total_sales = $this->db->select('SUM(amount) AS total_sales')->from('package_transaction')->where('transaction_type','credit','status',1)->get()->result();
        $data['total_sales'] = $total_sales[0]->total_sales;

        $current_month_sales = $this->db->query("SELECT SUM(amount) AS current_month_sales FROM package_transaction WHERE MONTH(created_at) = MONTH(CURRENT_DATE()) AND YEAR(created_at) = YEAR(CURRENT_DATE()) AND transaction_type = 'credit' AND status = '1'")->result();
        $data['current_month_sales'] = $current_month_sales[0]->current_month_sales;

        $data['transaction_list'] = $this->db->query("SELECT `package_transaction`.*,`user_info`.`first_name`,`user_info`.`last_name`,`user_info`.`email`,`hana_package`.`package` FROM `package_transaction` join user_info ON `package_transaction`.`user_id`= `user_info`.`id` join `hana_package` ON `hana_package`.`id` = `package_transaction`.`package_id` ORDER BY `package_transaction`.`id` desc LIMIT 5")->result();
        $data['wholesaler_transaction_list'] = $this->db->query("SELECT `wholesaler_package_transaction`.*,`user_info`.`first_name`,`user_info`.`last_name`,`user_info`.`email`,`wholesaler_hana_package`.`package` FROM `wholesaler_package_transaction` join user_info ON `wholesaler_package_transaction`.`user_id`= `user_info`.`id` join `wholesaler_hana_package` ON `wholesaler_hana_package`.`id` = `wholesaler_package_transaction`.`package_id` ORDER BY `wholesaler_package_transaction`.`id` desc LIMIT 5")->result();

        $this->load->view('super_admin/dashboard',$data);
    }

    /*
      |-------------------------------------------------------
      |  SUPER ADMIN  LOGIN CHECK
      |-------------------------------------------------------
     */

    public function authentication()
    {
        $password = $this->input->post('password');
        $email = $this->input->post('email');

        $remember_me = $this->input->post("remember");
        if ($remember_me == 1)
        {
            $email_cookie = array('name' => 'email', 'value' => $email, 'expire' => '86500',);
            $this->input->set_cookie($email_cookie);
            $pass_cookie = array('name' => 'password', 'value' => $password, 'expire' => '86500',);
            $this->input->set_cookie($pass_cookie);
        }
        $data['user'] = (object)$userdata = array('email' => $email, 'password' => $password,);

        // CHECK USER
        $user = $this->Auth_model->check_user($userdata);
        if (!isset($user->user_id) || empty($user->user_id))
        {
            $this->session->set_flashdata('error', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Unauthorized user</div>");
            redirect('super-admin');

        }
        $session_data = array('isLogIn' => true, 'isAdmin' => true, 'user_type' => $user->user_type, 'user_id' => $user->user_id, 'admin_created_by' => $user->created_by, 'name' => $user->first_name . " " . $user->last_name, 'email' => $user->email, 'logged_in' => TRUE, 'session_id' => session_id(),);

        // PUT THE USER DATA IN SESSION
        $this->session->set_userdata($session_data);

        // LAST LOGIN INFO UPDATE THE TABLE
        $last_login_info = array('login_datetime' => date("Y-m-d H:i:s"), 'last_login_ip' => $_SERVER['REMOTE_ADDR'],);

        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('log_info', $last_login_info);

        $this->session->set_flashdata('message', "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Welcome!" . ' ' . $user->first_name . " " . $user->last_name . "</div>");
        redirect('super-admin/dashboard');
    }

    public function super_admin_logout()
    {

        $last_logout_info = array('logout_datetime' => date("Y-m-d H:i:s"),);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('log_info', $last_logout_info);
        $this->session->sess_destroy();
        $this->session->set_flashdata('message', "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You are not valid user!</div>");
        redirect('super-admin');
    }
}
