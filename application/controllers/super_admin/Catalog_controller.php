<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        SuperAdminAuth();

    }

    function customer_catalog_list()
    {
        $data = [];
        $this->load->view('super_admin/list/customer_catalog_list', $data);
    }

    function search_customer_catalog_list_api()
    {
        $this->load->model('super_admin/Customer_model');
        $response = $this->Customer_model->customer_catalog_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function catalog_category_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_category_list', $data);
    }

    function search_customer_catalog_category_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Category_model');
        $response = $this->Category_model->customer_catalog_category_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function catalog_color_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_color_list', $data);
    }

    function search_customer_catalog_color_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Color_model');
        $response = $this->Color_model->customer_catalog_color_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_pattern_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_pattern_list', $data);
    }

    function search_customer_catalog_pattern_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;
        
        $this->load->model('super_admin/Pattern_model');
        $response = $this->Pattern_model->customer_catalog_pattern_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function catalog_product_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_product_list', $data);
    }

    function search_customer_catalog_product_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Product_model');
        $response = $this->Product_model->customer_catalog_product_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_attribute_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_attribute_list', $data);
    }

    function search_customer_catalog_attribute_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Attribute_model');
        $response = $this->Attribute_model->customer_catalog_attribute_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_price_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_price_list', $data);
    }

    function search_customer_catalog_price_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Price_model');
        $response = $this->Price_model->customer_catalog_price_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_group_price_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_group_price_list', $data);
    }

    function search_customer_catalog_group_price_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;
        
        $this->load->model('super_admin/Group_price_model');
        $response = $this->Group_price_model->customer_catalog_group_price_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_sqm_price_mapping_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_sqm_price_mapping_list', $data);
    }

    function search_customer_catalog_sqm_price_mapping_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Sqm_price_mapping_model');
        $response = $this->Sqm_price_mapping_model->customer_catalog_sqm_price_mapping_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function catalog_group_price_mapping_list($customer_id)
    {
        $data = [];
        $data['customer_id'] = $customer_id;
        $this->load->view('super_admin/list/customer_catalog_group_price_mapping_list', $data);
    }

    function search_customer_catalog_group_price_mapping_list_api()
    {
        $customer_detail = $this->db->select("customer_info.*")
                 ->from('customer_info')
                 ->where('customer_id', $_POST['customer_id'])->get()->row();
        $level_id = $customer_detail->level_id;
        $_POST['level_id'] = $level_id;

        $customer_user_id = $customer_detail->customer_user_id;
        $_POST['customer_user_id'] = $customer_user_id;

        $this->load->model('super_admin/Group_price_mapping_model');
        $response = $this->Group_price_mapping_model->customer_catalog_group_price_mapping_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }




    function b_user_catalog_category_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_category_list', $data);
    }

    function search_b_user_catalog_category_list_api()
    {
        $this->load->model('super_admin/Category_model');
        $response = $this->Category_model->b_user_catalog_category_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function b_user_catalog_color_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_color_list', $data);
    }

    function search_b_user_catalog_color_list_api()
    {
        $this->load->model('super_admin/Color_model');
        $response = $this->Color_model->b_user_catalog_color_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function b_user_catalog_pattern_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_pattern_list', $data);
    }

    function search_b_user_catalog_pattern_list_api()
    {
        $this->load->model('super_admin/Pattern_model');
        $response = $this->Pattern_model->b_user_catalog_pattern_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function b_user_catalog_product_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_product_list', $data);
    }

    function search_b_user_catalog_product_list_api()
    {
        $this->load->model('super_admin/Product_model');
        $response = $this->Product_model->b_user_catalog_product_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function b_user_catalog_attribute_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_attribute_list', $data);
    }

    function search_b_user_catalog_attribute_list_api()
    {
        $this->load->model('super_admin/Attribute_model');
        $response = $this->Attribute_model->b_user_catalog_attribute_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }


    function b_user_catalog_price_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_price_list', $data);
    }

    function search_b_user_catalog_price_list_api()
    {
        $this->load->model('super_admin/Price_model');
        $response = $this->Price_model->b_user_catalog_price_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

    function b_user_catalog_group_price_list($b_user_id)
    {
        $data = [];
        $data['b_user_id'] = $b_user_id;
        $this->load->view('super_admin/list/b_user_catalog_group_price_list', $data);
    }

    function search_b_user_catalog_group_price_list_api()
    {
        $this->load->model('super_admin/Group_price_model');
        $response = $this->Group_price_model->b_user_catalog_group_price_list($_POST);
        $totalFiltered = $response['total'];
        $json_data = array("draw" => intval($_POST['draw']), "recordsTotal" => intval($totalFiltered), "recordsFiltered" => intval($totalFiltered), "data" => $response['data'],);
        echo json_encode($json_data);
    }

}
