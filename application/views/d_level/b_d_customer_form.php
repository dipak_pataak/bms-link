
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>BMSLink D-Level Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $baseurl = 'http://soft9.bdtask.com/bmslink_apps/'; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/reset.css">


        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/toster/toastr.css" media="screen">


        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/style.css" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/custom_style.css" media="screen">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/colors/blue.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/select2.min.css">
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/css/select2-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap-datepicker3.min.css" />
        <link href="<?php echo base_url(); ?>assets/phone_format/css/intlTelInput.css" rel="stylesheet" type="text/css" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
        <style>
            .pac-container:after{
                content:none !important;
            }
            .custom_p_5{
                padding: 2rem 3rem !important;
            }
            .custom_form_group{
                margin-bottom: 0.5rem;
            }
       
            .note {
                text-align: center;
                background: -webkit-linear-gradient(left, #0072ff, #8811c5);
                color: #fff;
                font-weight: bold;
                font-size:25px;
            }
            .form-content
            {
                padding: 5%;
                border: 1px solid #ced4da;
                margin-bottom: 2%;
            }
            .form-control{
                border-radius:1.5rem;
            }
            .btnSubmit
            {
                border:none;
                border-radius:1.5rem;
                padding: 1%;
                width: 20%;
                cursor: pointer;
                background: #0062cc;
                color: #fff;
            }
            .container{
                max-width:960px;
            }
            .input-group>.form-control{
                border-top-right-radius: 2px;
                border-bottom-right-radius: 2px;
                border-top-left-radius: 2px;
                border-bottom-left-radius: 2px;
            }
        </style>
    </head>
    <script type="text/javascript">
        var mybase_url = '<?php echo $baseurl; //base_url();  ?>';

        //password validation
        function Validate() {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("confirmpassword").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
    </script>
    <body class="background show-spinner">
        <div class="fixed-background"></div>
        <main>
            <div class="container">
                <div class="card form_m_width">
                    <div class="ui small">
                        <div class="">
                            <?php
                            $user_id = $this->uri->segment(2);
                            $company_profile = $this->db->select('*')
                                            ->from('company_profile')
                                            ->where('user_id', $user_id)
                                            ->get()->result();
//                            echo $baseurl."assets/b_level/uploads/appsettings/".$company_profile[0]->logo;
                            ?>
                            <a href="index.html" class="d-block text-center mb-5">
                                <!--<img src="<?php // echo base_url();  ?>assets/c_level/img/logo.png" alt="">-->
                                <!-- <img src="<?php echo $baseurl; //base_url();  ?>assets/b_level/uploads/appsettings/<?php echo @$company_profile[0]->logo; ?>" alt=""  style="width: 10%; margin-top: -15px !important; "> -->
                            </a>
                            <div class="">
                                <?php
                                $error = $this->session->flashdata('error');
                                $success = $this->session->flashdata('success');
                                if ($error != '') {
                                    echo $error;
                                }
                                if ($success != '') {
                                    echo $success;
                                }
                                ?>
                            </div>
                            <?php
// echo '<pre>'; print_r($d_level_form);
//                            echo $d_level_form[0]->iframe_code;
                            ?>
                           <form action="<?php echo base_url();  ?>wholesaler-d-customer-info-save" method="post">
                            <div class="container register-form">
                                <div class="form">
                                    <div class="note">
                                        <p>Register</p>
                                    </div>

                                    <div class="form-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 24px;" class="fa fa-user"></i> </span>
                                                    </div>
                                                    <input class="form-control" id="first_name" name="first_name" placeholder="First Name" type="text" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 24px;"class="fa fa-user"></i> </span>
                                                    </div>
                                                    <input class="form-control" id="last_name" name="last_name" placeholder="Last Name" type="text" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 17px;" class="fa fa-envelope-open"></i> </span>
                                                    </div>
                                                    <input class="form-control" id="email" onkeyup="" name="email" placeholder="Email" type="email">
                                                </div>
                                            </div>



                                           <div class="col-md-6">
                                               <div class="form-group input-group">
                                                   <div class="input-group-prepend">
                                                       <span class="input-group-text"> <i style="font-size: 24px;" class="fa fa-phone"></i></i> </span>
                                                   </div>
                                                   <input class="form-control phone phone-format" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" type="text">
                                               </div>
                                           </div>

                                            <!-- <div class="form-group row">
                                                <label for="phone" class="col-sm-3 col-form-label text-right">Phone </label>
                                                <div class="col-sm-9">
                                                    <input name="phone" class="form-control phone phone-format" type="text" id="phone"  placeholder="+1 (XXX)-XXX-XXXX">
                                                </div>
                                            </div> -->



                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 20px;"class="fa fa-building"></i> </span>
                                                    </div>
                                                    <input class="form-control"  id="company" name="company" placeholder="Your Company Name" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i class="fa fa-address-card"></i> </span>
                                                    </div>
                                                    <input class="form-control" id="address" name="address" placeholder="Address" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i class="fa fa-university"></i> </span>
                                                    </div>
                                                    <input class="form-control" id="city" name="city" placeholder="City" type="text">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group input-group">
                                                                <input class="form-control" id="state" name="state" placeholder="State" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group input-group">
                                                                <input class="form-control" placeholder="Zip" id="zip_code" name="zip_code" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group input-group">
                                                                <input class="form-control" placeholder="Country" id="country_code" name="country_code" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 25px;" class="fa fa-lock"></i> </span>
                                                    </div>
                                                    <input class="form-control" placeholder="Password" type="password" id="password" name="password" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"> <i style="font-size: 25px;" class="fa fa-lock"></i> </span>
                                                    </div>
                                                    <input class="form-control" placeholder="Confirm Password" type="password" id="confirmpassword" name="password" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input is-invalid" required id="invalidCheck" required>
                                                <label class="custom-control-label" for="invalidCheck">Agree to terms and conditions</label>
                                            </div>
                                            </div>
                                        <div class="form-group" style="text-align:center;">
                                        <input type="hidden" name="user_id" value="<?php echo $this->uri->segment(2); ?>">
                                        <input type="hidden" name="level_type" value="<?php echo $this->uri->segment(3); ?>">
                                            <button style="width:180px;" type="submit" class="btn btn-primary" onclick="return Validate()"> Register  </button>
                                        </div>   
                                        <p class="text-center">Already have an account? <a href="/retailer-dashboard">Log In</a> </p> 
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/jquery.flot.min.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/tiny_mce/jquery.tinymce.js"></script>
        <!-- scripts (custom) -->
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.menu.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/smooth.chart.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/scripts/select2.full.js"></script>
        <script src="<?php echo $baseurl; //base_url();  ?>assets/b_level/resources/js/bootstrap-datepicker.js"></script>
        <!--========= time picker ==========-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
        <script src="<?php echo base_url() ?>assets/b_level/resources//js/bootstrap-datetimepicker.js"></script>

        <!--=========== its for add customer phone format .js ================-->
<!--        <script src="--><?php //echo base_url(); ?><!--assets/c_level/js/phone-format.js"></script>-->
        <!--=========== its for add customer phone format .js close ================-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/b_level/toster/toastr.min.js"></script>
        <!--        <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/vendor/jquery-3.3.1.min.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/dore.script.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/scripts.js"></script>
                <script src="<?php echo $baseurl; //base_url();  ?>assets/c_level/js/phone-format.js"></script>-->

        <script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
        <script type="text/javascript">
          var  country = 'US';
//    ======= its for google place address geocomplete =============
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('address'));

            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                //console.log(place);
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var geocoder = new google.maps.Geocoder;
                var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        //console.log(results)
                        if (results[0]) {
                            //document.getElementById('location').innerHTML = results[0].formatted_address;
                            var street = "";
                            var city = "";
                            var state = "";
                            var country = "";
                            var country_code = "";
                            var zipcode = "";
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].types[0] === "locality") {
                                    city = results[i].address_components[0].long_name;
                                    state = results[i].address_components[2].short_name;

                                }
                                if (results[i].types[0] === "postal_code" && zipcode == "") {
                                    zipcode = results[i].address_components[0].long_name;

                                }
                                if (results[i].types[0] === "country") {
                                    country = results[i].address_components[0].long_name;
                                }
                                if (results[i].types[0] === "country") {
                                    country_code = results[i].address_components[0].short_name;
                                }
                                if (results[i].types[0] === "route" && street == "") {
                                    for (var j = 0; j < 4; j++) {
                                        if (j == 0) {
                                            street = results[i].address_components[j].long_name;
                                        } else {
                                            street += ", " + results[i].address_components[j].long_name;
                                        }
                                    }

                                }
                                if (results[i].types[0] === "street_address") {
                                    for (var j = 0; j < 4; j++) {
                                        if (j == 0) {
                                            street = results[i].address_components[j].long_name;
                                        } else {
                                            street += ", " + results[i].address_components[j].long_name;
                                        }
                                    }

                                }
                            }
                            if (zipcode == "") {
                                if (typeof results[0].address_components[8] !== 'undefined') {
                                    zipcode = results[0].address_components[8].long_name;
                                }
                            }
                            if (country == "") {
                                if (typeof results[0].address_components[7] !== 'undefined') {
                                    country = results[0].address_components[7].long_name;
                                }
                                if (typeof results[0].address_components[7] !== 'undefined') {
                                    country_code = results[0].address_components[7].short_name;
                                }
                            }
                            if (state == "") {
                                if (typeof results[0].address_components[5] !== 'undefined') {
                                    state = results[0].address_components[5].short_name;
                                }
                            }
                            if (city == "") {
                                if (typeof results[0].address_components[5] !== 'undefined') {
                                    city = results[0].address_components[5].long_name;
                                }
                            }

                            var address = {
                                "street": street,
                                "city": city,
                                "state": state,
                                "country": country,
                                "country_code": country_code,
                                "zipcode": zipcode,
                            };
                            //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                            $("#city").val(city);
                            $("#state").val(state);
                            $("#zip_code").val(zipcode);
                            $("#country_code").val(country_code);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });

            });


        });
        </script>

        <script src="<?php echo base_url(); ?>assets/c_level/js/phoneformat.js"></script>

        <!-- <script src="<?php echo base_url(); ?>assets/phone_format/js/intlTelInput.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/phone_format/js/cleave.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/phone_format/js/cleave-phone.i18n.js"></script>
        <script src="<?php echo base_url(); ?>assets/phone_format/js/common-phone.js"></script>
    <script>
        // country = 'US';
        //---------phone_format---------
        $(document).ready(function(){
            load_country_dropdown('phone',country);
        })
    </script>

    </body>

</html>
