<style>
    .inactive{
        color: red;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Treeview</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Treeview';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="row m-0">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php echo form_open('wholesaler-account-chart'); ?>
                        <h3><input type="checkbox" name="inactive" value="test" <?php
                            if ($inactive == 0) {
                                echo 'Checked';
                            }
                            ?>>Include Inactive
                            &nbsp; <input type="submit" name="inactive_btn" value="Submit">
                        </h3>

                        <?php echo form_close(); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <ul id="tree1">
                                    <?php
                                    $isActive = 0;
                                    $visit = array();
                                    for ($i = 0; $i < count($userList); $i++) {
                                        $visit[$i] = false;
                                    }
                                    $this->BAccount_model->dfs("COA", "0", $userList, $visit, 0, $isActive);
                                    ?>
                                </ul>
                            </div>
                            <?php // if ($this->permission->method('accounts', 'update')->access() || $this->permission->method('accounts', 'create')->access()): ?>
                            <div class="col-md-6" id="newform"></div>
                            <?php // endif;   ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        "use strict"; // Start of use strict

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'simple-icon-people';
                var closedClass = 'fa-folder-o';

                if (typeof o !== 'undefined') {
                    if (typeof o.openedClass !== 'undefined') {
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass !== 'undefined') {
                        closedClass = o.closedClass;
                    }
                }
                ;

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this === e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    });
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function () {
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        //Initialization of treeviews

        $('#tree1').treed({openedClass: 'fa-folder-open-o', closedClass: 'fa-folder'});

    });

</script>

<script type="text/javascript">
    function loadData(id) {
//         swal(id);
        $.ajax({
            url: "<?php echo site_url('wholesaler-show-selected-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {
                $('#newform').html(data);
                $('#btnSave').hide();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                swal('Error get data from ajax');
            }
        });
    }


</script>
<script type="text/javascript">
    function newdata(id) {
        $.ajax({
            url: "<?php echo site_url('wholesaler-new-account-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {
                // console.log(data.headcode);
                console.log(data.rowdata);
                var headlabel = data.headlabel;
                $('#txtHeadCode').val(data.headcode);
                document.getElementById("txtHeadName").value = '';
                $('#txtPHead').val(data.rowdata.HeadName);
                $('#txtHeadLevel').val(headlabel);
                $('#btnSave').prop("disabled", false);
                $('#btnSave').show();
                $('#btnUpdate').hide();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                swal('Error get data from ajax');
            }
        });
    }

</script>
