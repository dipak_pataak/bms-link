<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <div class="title row">
            <div class="col-sm-6">
                <h5>Voucher Approval</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Voucher Approval';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-wholesaler-customer" class="btn btn-success btn-sm m-1">Add</a>-->
            </div>
        </div>
        <!-- end box / title -->
        <div class="px-3">
			<div class="table-responsive">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Voucher No</th>
                        <th>Remark</th>
                        <th>Debit</th>
                        <th>Credit</th>
                        <?php if (!empty($packageid)){?>
                        <th>Action</th>
                        <?php }  ?>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($aprrove))  ?>
                    <?php $sl = 1; ?>
                    <?php foreach ($aprrove as $approve) { ?>
                        <tr>
                            <td><?php echo $sl++; ?></td>
                            <td><?php echo $approve->VNo; ?></td>
                            <td><?php echo $approve->Narration; ?></td>
                            <td><?php echo $approve->Debit; ?></td>
                            <td><?php echo $approve->Credit; ?></td>
                            <?php if (!empty($packageid)){?>
                            <td class="text-right">
                                <a href="<?php echo base_url("wholesaler-isactive/$approve->VNo/active") ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Inactive">Approve</a>
                                <a href="<?php echo base_url("wholesaler-voucher-edit/$approve->VNo") ?>" class="btn btn-info btn-sm" title="Update" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                            </td>
                             <?php }  ?>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
			</div>
		</div>

    </div>
</div>
<!-- end content / right -->
