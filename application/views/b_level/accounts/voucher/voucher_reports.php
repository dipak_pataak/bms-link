
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="title row">
            <h5>Voucher Filter</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Voucher Filter';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
		<div class="row m-0">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-body">
                    <div class="row m-0" id="">
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="form-group row">
                                <label for="date" class="col-sm-4 col-form-label">Date</label>
                                <div class="col-sm-8">
                                    <input type="text" id="sales_date" placeholder="Data" class="datepicker form-control serach_date">
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success w-md m-b-5" id="btnSerach">Find</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
		</div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Voucher Reports</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">
			<div class="table-responsive">
            <table class="datatable2 table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Voucher No</th>
                        <th>Remarks</th>
                        <th>Amount</th>
                        <th class="text-center">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $date = date('Y-m-d');
                    ?>
                    <tr id="show_vouchar">
                        <td>
                            <a href="<?php echo base_url("b-vouchar-cash/" . $date) ?>">
                                <?php echo "CV-BAC-" . date('M-d-Y'); ?>
                            </a>
                        </td>
                        <td>Aggregated Cash Credit Voucher of <?php echo date('M-d-Y', strtotime($date)); ?></td>
                        <td><?php
                            if ($get_cash->Amount == '') {
                                echo '0.00';
                            } else {
                                echo $get_cash->Amount;
                            }
                            ?></td>
                        <td align="center"><?php echo date('M-d-Y', strtotime($date)); ?></td>
                    </tr>
                    <?php
                    foreach ($get_vouchar as $v_data) {
                        ?>
                        <tr>
                            <td><a href="<?php echo base_url("accounts/accounts/vouchar_view/$v_data->VNo") ?>"><?php
                                    echo $v_data->VNo;
                                    ;
                                    ?></a></td>
                            <td><?php echo $v_data->Narration; ?></td>
                            <td><?php echo number_format($v_data->Amount); ?></td>
                            <td><?php echo $v_data->VDate; ?></td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnSerach').on('click', function () {
            var vouchar = $("#sales_date").val();
            $.ajax({
                url: '<?php echo base_url(); ?>b-voucher-report-serach',
                type: 'POST',
                data: {
                    vouchar: vouchar
                },
                success: function (data) {
//                    swal(data);
                    $("#show_vouchar").html(data);
                }
            });

        });
    });


</script>