
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Cash Flow</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Cash Flow';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <?= form_open_multipart('wholesaler-cash-flow-report-search') ?>
                <div class="row" id="">
                    <div class="col-sm-7 col-md-7 col-lg-5">
     
                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label">From Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="dtpFromDate" value="" placeholder="From Date" class="datepicker form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label">To Date</label>
                            <div class="col-sm-8">
                                <input type="text"  name="dtpToDate" value="" placeholder="To Date" class="datepicker form-control">
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Find</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
        </div>
    </div>
</div>
