<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div id="right">
    <div class="box new_product" style="height: 100%">
        <div class="row">
            <div class="col-md-12">
                <?php
                    $message = $this->session->flashdata('message');
                    if ($message)
                        echo $message;
                    ?> 
            </div>
        </div>
        <!-- box / title -->            
        <div class="title row">
            <h5 class="col-sm-6">Edit Products</h5>
        </div>
        <?php echo form_open('b_level/Product_controller/product_update', array('class' => 'form-row px-3 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1', 'id' => '', 'name' => 'edit_product', 'enctype' => 'multipart/form-data')); ?>
        <div class="col-lg-6 px-4">
            <div class="form-group">
                <label for="product_name" class="mb-2">Product Name (Model Name)</label>
                <input class="form-control" type="text" name="product_name" value='<?= $product->product_name ?>'  id="product_namea" onmouseout="replaceString(this.value)" required >
            </div>
            <input type="hidden" name="product_id" value="<?= $product->product_id ?>">
            <div class="form-group">
                <label for="category_id" class="mb-2">Select Category </label>
                <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>
                    <!-- <option value="none">None</option> -->
                    <?php foreach ($get_category as $category) { ?>
                    <option value='<?= $category->category_id ?>' <?= ($category->category_id == $product->category_id ? 'selected' : ''); ?> >
                        <?= $category->category_name ?>
                    </option>
                    "
                    <?php } ?>
                </select>
            </div>
            <div class="form-group" id="subcategory_id">
                <?php
                    if ($product->subcategory_id) {
                        $this->db->select('*');
                        $this->db->from('category_tbl a');
                        $this->db->where(' a.parent_category', $product->category_id);
                        $this->db->where('a.created_by', $this->session->userdata('user_id'));
                        $subcategory_results = $this->db->get()->result();
                        ?>
                
                    <label for="subcategory_id" class="mb-2">Sub Category</label>
                    <select class="form-control select2" name="subcategory_id" id="sub_category_id" data-placeholder='-- select one --'>
                        <option value=""></option>
                        <?php foreach ($subcategory_results as $subcat) { ?>
                            <option value='<?php echo $subcat->category_id; ?>'>
                                <?php echo $subcat->category_name; ?>
                            </option>
                        <?php } ?>
                    </select>   
                    <script>
                        $("#sub_category_id").val('<?=$product->subcategory_id?>');
                    </script> 
                <?php } ?>
            </div>
            <div class="form-group">
                <?php
                    $category_wise_pattern_list = $this->db->select('*')
                                    ->where('category_id', $product->category_id)
                                    ->where('status', 1)
                                    ->where('created_by', $this->session->userdata('user_id'))
                                    ->get('pattern_model_tbl')->result();
                    ?>
                <label for="pattern_model_id" class="mb-2">Pattern</label>
                <div class="pattern_model_id">
                    <select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true">
                        <?php
                            foreach ($category_wise_pattern_list as $single_pattern) {
                                $array_of_patterns = explode(",", $product->pattern_models_ids);
                                ?>
                        <option value="<?= $single_pattern->pattern_model_id ?>" <?php
                            if (in_array($single_pattern->pattern_model_id, $array_of_patterns)) {
                                echo 'selected';
                            }
                            ?>>
                            <?= ucwords($single_pattern->pattern_name); ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="theSelect" class="mb-2">Price Model/Style</label>
                <select name="price_style_type" class="form-control price_select select2" onchange="price_model_wise_style(this.value)" id="theSelect" data-placeholder="-- select one --">
                    <option value=""></option>
                    <option value="3" <?= ($product->price_style_type == 3 ? 'selected' : '') ?>>Fixed product Price Style</option>
                    <option value="4" <?= ($product->price_style_type == 4 ? 'selected' : '') ?>>Group Price</option>
                    <option value="2" <?= ($product->price_style_type == 2 ? 'selected' : '') ?>>Price by Sq.ft style </option>
                    <option value="1" <?= ($product->price_style_type == 1 ? 'selected' : '') ?>>Row/Column style</option>
                    <option value="5" <?= ($product->price_style_type == 5 ? 'selected' : '') ?>>Sqm Price</option>
                </select>
            </div>
            <div class="hidden isnull_price mb-3"></div>
            <div class="mb-3 is1 hidden">
                <div id="ss_pp">
                    <select name="price_rowcol_style_id" id="price_rowcol_style_id" onchange="setValue(this.value)" class="form-control select2" data-placeholder="-- select style --">
                        <option value=""></option>
                        <?php foreach ($style_slist as $s) { ?>
                        <option value="<?php echo $s->style_id ?>" <?php echo ($s->style_id == $product->price_rowcol_style_id ? 'selected' : '') ?> ><?= $s->style_name ?></option>
                        <?php } ?>
                    </select>
                    <script>
                        $(document).ready(function(){
                            if($("#price_rowcol_style_id").val() != ''){
                                $("#price_rowcol_style_id").change();
                            }
                        })
                    </script>    
                    <input type="hidden" name="price_style_id" id="price_style_id" value="<?= $product->price_rowcol_style_id ?>">
                    <a href="#" data-toggle="modal" data-target="#myModal"  class="btn btn-success btn-xs" style="margin-top: 8px;">add new price style</a>
                </div>
                <br>
                <label for="Row_Column" class="mb-2">Row Column Style</label>
                <div id="priceseet"></div>
            </div>
            <div class="hidden is2 mb-3">
                <label for="sqft" class="mb-2">SqFt. Style</label>
                <input type="text" name="sqft_price" class="form-control" value="<?= @$product->sqft_price ?>">
            </div>
            <div class="hidden is3 mb-3">
                <label for="fixedprice" class="mb-2">Fixed Price Style</label>
                <input type="text" name="fixed_price" class="form-control" value="<?= @$product->fixed_price ?>">
            </div>
        </div>
        <div class="col-lg-6 px-4">
            <div class="form-group product_code_div">
                <label for="product_code" class="mb-2" >Color :</label>
                <div class="">
                    <select class="selectpicker form-control select-all" id="color_id" name="color_id[]" multiple data-live-search="true" required data-selected-text-format="count>2">
                        <option value="[all]" class="select-all">Select All</option>
                        <?php foreach ($colors as $color) {
                            $array_of_values = explode(",", $product->colors);
                            ?>
                        <option value="<?= $color->id ?>" <?php
                            if (in_array($color->id, $array_of_values)) {
                                echo 'selected';
                            }
                            ?>>
                            <?php echo ucwords($color->color_name) . "->( " . $color->color_number . " ) "; ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="product_code" class="mb-2">Status</label>
                <select name="status" class="form-control select2" data-placeholder="-- select one --">
                    <option value="1" <?php
                        if ($product->active_status == 1) {
                            echo 'selected';
                        }
                        ?>>Active</option>
                    <option value="0" <?php
                        if ($product->active_status == 0) {
                            echo 'selected';
                        }
                        ?>>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="cost_factor" class="mb-2">Cost Factor</label>
                        <input type="number" name="dealer_price" id="dealer_price" class="form-control NumbersAndDot" onkeyup="cost_factor_calculation(this.value)" onblur="cost_factor_calculation(this.value)" min="0.1" max="2.0" step="0.001" autocomplete="off" required value="<?=@$product->dealer_price?>">
                    </div>
                    <div class="col-sm-4">
                        <label for="individual_price" class="mb-2">Discount (%)</label>
                        <input type="text" name="individual_price" id="individual_price" class="form-control" readonly value="0.00">
                    </div>
                </div>    
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="product_img" class="mb-2">File Upload</label>
                        <input type="file" class="form-control" name="product_img" id="product_img">
                        <p>Extension: jpg|jpeg|png. <br> File size: 2MB</p>
                        <input type="hidden" name="hid_product_img" value="<?= @$product->product_img ?>">
                    </div>
                    <div class="col-sm-4">
                        <img src="<?php if($product->product_img != '') {   
                            echo base_url().$product->product_img; 
                            } else { 
                            echo base_url('assets/no-image.png');
                            }?> ?>" id="prev_product_img" width="100px" height="100px">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="radio" name="is_taxable" value="1" <?php if($product->is_taxable == '1') { echo "checked"; }?> >   Taxable &nbsp;
                &nbsp;<input type="radio" name="is_taxable" value="0" <?php if($product->is_taxable != '1') { echo "checked"; }?>>  Nontaxable
            </div>
        </div>
       
        <div class="col-sm-12 px-4">
            <div class="form-group">
                <label for="product_description" class="mb-2">Description</label>
                <textarea class="form-control" name="product_description" id="product_description"><?=@$product->product_description?></textarea>
            </div>
        </div>
        <?php // echo '<pre>'; print_r($product); echo '</pre>';       ?>
        <div class="col-lg-12 px-4">
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- end content / right -->

<script src="<?=base_url('assets/ckeditor/ckeditor.js')?>"></script>

<script type="text/javascript">
    document.forms['edit_product'].elements['category_id'].value = "<?php echo $product->category_id ?>";
    document.forms['edit_product'].elements['status'].value = "<?php echo $product->active_status ?>";

    $(document).ready(function(){
        CKEDITOR.replace('product_description');
        $("#dealer_price").blur();
    })

    function cost_factor_calculation(item) {
        var cost_factor = $("#dealer_price").val();
        var discount = 0;
        if(cost_factor != ''){
            cost_factor = (cost_factor*100).toFixed(2); 
            discount = (100 - (cost_factor));
        }
        $("#individual_price").val(discount.toFixed(2));
    }

    $("body").on("keypress keyup blur",".NumbersAndDot",function (event) {
      $(this).val($(this).val().replace(/[^0-9\.]/g,''));
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
    });

    $("body").on("change", "#product_img", function(e) {
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                swal("Please upload file less than 2MB. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
        }    

        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev_product_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#prev_product_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }

    $(document).ready(function () {
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>wholesaler-category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().removeClass("hidden");
                    } else {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().addClass("hidden");
                    }
                }

            });
        
            // ============= its for category-wise-pattern ==============
            $.ajax({
                url: "<?php echo base_url(); ?>category-wise-pattern/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $(".pattern_model_id").html(r);
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $('.selectpicker').selectpicker();
                    } else {
                        $(".pattern_model_id").html("Not Found!");
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $(".pattern_model_id").css({'color': 'red'});
                    }
                }
            });
        });

        // $("#category_id").trigger("change");
    });

    $(document).ready(function () {
        var value = $("#theSelect option:selected").val();


        var theDiv = $(".is" + value);

        theDiv.slideDown().removeClass("hidden");


        theDiv.siblings('[class*=is]').slideUp(function () {
            $(this).addClass("hidden");
        });
    });


    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if(value == '4' || value == '5'){
            $('.is1').slideUp().addClass("hidden");
            $('.is2').slideUp().addClass("hidden");
            $('.is3').slideUp().addClass("hidden");
        }
        theDiv.slideDown().removeClass("hidden");
        theDiv.siblings('[class*=is]').slideUp(function () {
            $(this).addClass("hidden");
        });
    });


    // ============ its for price_model_wise_style =============
    function price_model_wise_style(id) {

        $.ajax({
            url: "<?php echo base_url(); ?>price-model-wise-style",
            type: "post",
            data: {id: id},
            success: function (r) {
                r = JSON.parse(r);
                $("#price_rowcol_style_id").empty();
                $("#price_rowcol_style_id").html("<option value=''>-- select one -- </option>");

                $.each(r, function (ar, typeval) {
                    $('#price_rowcol_style_id').append($('<option>').text(typeval.style_name).attr('value', typeval.style_id));
                });
            }
        });
    }

    $("#attrSelect").change(function () {

        var attr_type_id = $("#attrSelect option:selected").val();
        var url = "<?php echo base_url('b_level/Attribute_controller/get_attr_by_attr_types/'); ?>/" + attr_type_id;

        $.ajax({
            url: url,
            type: 'get',
            success: function (r) {
                $("#iscolor").html(r);
                $('#iscolor').slideDown().removeClass("hidden");
            }
        });
    });


    function shuffle_pricebox(elem) {
        //swal(elem + ' - ' +document.getElementById(elem).disabled);
        if (!document.getElementById(elem).disabled)
            document.getElementById(elem).disabled = true;
        else
            document.getElementById(elem).disabled = false;
    }

</script>

<?php $this->load->view($js) ?>

<script type="text/javascript">
    function replaceString(v) {
        $('#product_name').val((v.replace(/['"]+/g, '&quot;')));
    }


    $("form").on('change','#pattern_model_id',function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length > 0){
            
            $.ajax({
                url: "<?php echo base_url(); ?>get_colors_based_on_pattern_ids",
                type: 'post',
                data: {"pattern_ids":pattern_ids},
                success: function (r) {
                    if (r !== '') {
                        var color_data = JSON.parse(r);
                        var color_option = '';
                        $.each(color_data, function(index, item) {
                            color_option += '<option value="'+item.id+'">'+item.color_name+'->('+item.color_number +')';
                        });
                        var select_all = '';
                        if(color_option != ''){
                            select_all = '<option value="[all]" class="select-all">Select All</option>';
                            $("#color_id").html(select_all + color_option); 
                            $('#color_id').selectpicker('refresh');
                            // $('#color_id option').attr("selected","selected");
                            $('a.select-all').click();
                        }else{
                            $("#color_id").html('');
                            $('#color_id').selectpicker('refresh'); 
                        }
                        $(".product_code_div").show();
                    } else {
                        $("#color_id").html(''); 
                        $('#color_id').selectpicker('refresh');
                        $(".product_code_div").hide();
                    }
                }

            });

        }else{
            $("#color_id").html(''); 
            $('#color_id').selectpicker('refresh');
            $(".product_code_div").hide();
        }
    })

    $(document).ready(function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length == 0){
            $(".product_code_div").hide();
        }
    });
</script>
