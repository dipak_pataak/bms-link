<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    .condition_checkbox{
        font-size: 14px;
        overflow: scroll;
        height: 76px;
    }
</style>
<!-- content / right -->
<div id="right"> 
    <div class="box new_product" style="height: 100%">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <!-- box / title -->            
        <div class="title row">
            <h5 class="col-sm-6">Products New</h5>
        </div>
        <!-- end box / title -->
        <div class="row m-0">

            <?php echo form_open('b_level/product_controller/product_save', array('class' => 'form-row px-3', 'id' => '')); ?>
            
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="product_name" class="mb-2">Product Name (Model Name)</label>
                    <input class="form-control" type="text" name="product_name" placeholder="Product Name" id="product_name" onmouseout="replaceString(this.value)" required>
                </div>

                <div class="form-group">
                    <label for="category_id" class="mb-2">Select Category</label>
                    <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>
                        <option value="none">None</option>
                        <?php
                        foreach ($get_category as $category) {
                            echo "<option value='$category->category_id'>$category->category_name</option>";
                        }
                        ?>
                    </select>
                </div> 

                <div class="form-group" id="subcategory_id">

                </div> 

                <div class="form-group">

                    <label for="pattern_model_id" class="mb-2">Pattern</label> <!--<a href="#">add new</a>-->
                    <div class="pattern_model_id">
                        <select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                            <!--  <?php foreach ($patern_model as $val) { ?>
                                         <option value="<?= $val->pattern_model_id ?>"><?= $val->pattern_name; ?></option>
                            <?php } ?> -->
                        </select>
                    </div>

                </div>

                <div class="form-group">

                    <label for="theSelect" class="mb-2">Price Model/Style</label>
                    <select name="price_style_type" class="form-control price_select select2" id="theSelect" onchange="price_model_wise_style(this.value)" data-placeholder="-- select one --">
                        <option value=""></option>
                        <option value="1" selected>Row/Column style</option>
                        <option value="4">Group style</option>
                        <option value="5">Sqm price style</option>
                        <option value="2">Price by Sq.ft style </option>
                        <option value="3">Fixed product Price Style</option>
                        <!--<option value="grprice">Group Price</option>-->
                    </select>

                </div>

                <div class="hidden isnull_price mb-3"></div>


                <div class="mb-3 is1 hidden">

                    <div id="ss_pp">
                        <select name="price_rowcol_style_id" id="price_rowcol_style_id" onchange="setValue(this.value)" class="form-control select2" data-placeholder="-- select style --">
                            <option value=""></option>
                            <?php foreach ($style_slist as $s) { ?>
                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="price_style_id" id="price_style_id">
                        <a href="#" data-toggle="modal" data-target="#myModal"  class="btn btn-success btn-xs" style="margin-top: 8px;">add new price style</a>

                    </div><br>

                    <label for="Row_Column" class="mb-2">Row Column Style</label>
                    <div id="priceseet"></div>

                </div>
               


                <div class="hidden is2 mb-3">

                    <label for="sqft" class="mb-2">SqFt. Style</label>
                    <input type="text" name="sqft_price" class="form-control" placeholder="$80/sq.ft">

                </div>

                <div class="hidden is3 mb-3">

                    <label for="fixedprice" class="mb-2">Fixed Price Style</label>
                    <input type="text" name="fixed_price" class="form-control" placeholder="$29">

                </div>                                            

            </div>



            <div class="col-sm-6">
                <div class="form-group product_code_div">
                    <label for="color_id" class="mb-2" >Color :</label>
                    <div class="">
                        <select class="selectpicker form-control" id="color_id" name="color_id[]" multiple data-live-search="true" required="">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="product_code" class="mb-2">Status</label>
                    <select name="status" class="form-control select2" data-placeholder="-- select one --">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="product_code" class="mb-2">Default Discount (%)</label>
                    <input class="form-control" min="0" name="dealer_price" type="number" placeholder="20%" value="" required>
                </div>  
            </div>

            <div class="col-lg-12">
				<div class="form-group text-right">
					<button type="submit" class="btn btn-success w-md m-b-5">Add</button>
				</div>
			</div>

            <?php echo form_close(); ?>
        </div>
    </div>

</div>
<!-- end content / right -->

<script type="text/javascript">

    function replaceString(v) {
        $('#product_name').val((v.replace(/['"]+/g, '&quot;')));
    }


//    ============ its for price_model_wise_style =============
    
    function price_model_wise_style(id) {

        $.ajax({
            url: "<?php echo base_url(); ?>price-model-wise-style",
            type: "post",
            data: {id: id},
            success: function (r) {

                    r = JSON.parse(r);
                    $("#price_rowcol_style_id").empty();
                    $("#price_rowcol_style_id").html("<option value=''>-- select one -- </option>");

                    $.each(r, function (ar, typeval) {
                        $('#price_rowcol_style_id').append($('<option>').text(typeval.style_name).attr('value', typeval.style_id));
                    });
                }

        });
    }



    $(document).ready(function () {
        $('select.selectpicker').on('change', function(){
            var selected = $('.selectpicker option:selected').val();
        });
//        $(".group_product").hide();
//        $('body').on('click', '.product_radio', function () {
//            var product_radio = $(this).val();
////            swal(product_radio);
//            if(product_radio == 'New Product'){
//                $(".new_product").show();
//                $(".group_product").hide();
//            }
//            if(product_radio == 'Group Product'){
//                $(".group_product").show();
//                $(".new_product").hide();
//            }
//        });
//        
        //        ================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().removeClass("hidden");
                        $('.select2').select2();
                    } else {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().addClass("hidden");
                    }
                }
            });
//            ========== its for category wise condition ===========
            $.ajax({
                url: "category-wise-condition/" + category_id,
                type: 'get',
                success: function (r) {
//                        console.log(r);
                    if (r !== '') {
                        $(".condition_checkbox").empty();
                        $(".condition_checkbox").html(r);
                    }
//                    else {
//                        $(".condition_checkbox").html("Not Found!");
//                    }
                }
            });
//            ============= its for category-wise-pattern ==============

//            var cat_id = (category_id!==' '?category_id:0);
            $.ajax({
                url: "category-wise-pattern/" + category_id,
                type: 'get',
                success: function (r) {
//                    console.log(r);
                    if (r !== '') {
                        $(".pattern_model_id").html(r);
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $('.selectpicker').selectpicker();
                    } else {
                        $(".pattern_model_id").html("Not Found!");
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $(".pattern_model_id").css({'color': 'red'});
                    }
                }
            });
        });

        $('.is1').removeClass("hidden");

    });



    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        console.log(value);
        if(value==='4' || value==='5'){
            $('.is1').slideUp().addClass("hidden");
            $('.is2').slideUp().addClass("hidden");
            $('.is3').slideUp().addClass("hidden");
        }

        theDiv.slideDown().removeClass("hidden");

        theDiv.siblings('[class*=is]').slideUp(function () {

            $(this).addClass("hidden");

        });

    });

    // $("#attrSelect").change(function () {

    //     var value = $("#attrSelect option:selected").val();
    //     var theDiv = $(".is" + value);

    //     theDiv.slideDown().removeClass("hidden");
    //     theDiv.siblings('[class*=is]').slideUp(function () {
    //         $(this).addClass("hidden");
    //     });
    // });


    $("#attrSelect").change(function () {

        var attr_type_id = $("#attrSelect option:selected").val();

        $.ajax({
            url: "<?php base_url() ?>b_level/Attribute_controller/get_attr_by_attr_types/" + attr_type_id,
            type: 'get',
            success: function (r) {
                $("#iscolor").html(r);
                $('#iscolor').slideDown().removeClass("hidden");
            }
        });


    });


    function shuffle_pricebox(elem) {
        //swal(elem + ' - ' +document.getElementById(elem).disabled);
        if (!document.getElementById(elem).disabled)
            document.getElementById(elem).disabled = true;
        else
            document.getElementById(elem).disabled = false;
    }

</script>

<?php
$this->load->view($js)?>

<script type="text/javascript">

    $("form").on('change','#pattern_model_id',function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length > 0){
            
            $.ajax({
                url: "<?php echo base_url(); ?>get_colors_based_on_pattern_ids",
                type: 'post',
                data: {"pattern_ids":pattern_ids},
                success: function (r) {
                    if (r !== '') {
                        var color_data = JSON.parse(r);
                        var color_option = '';
                        $.each(color_data, function(index, item) {
                            color_option += '<option value="'+item.id+'">'+item.color_name+'->('+item.color_number +')';
                        });
                        $("#color_id").html(color_option); 
                        $('#color_id option').attr("selected","selected");
                        $('#color_id').selectpicker('refresh');
                        $(".product_code_div").show();
                    } else {
                        $("#color_id").html(''); 
                        $('#color_id').selectpicker('refresh');
                        $(".product_code_div").hide();
                    }
                }

            });

        }else{
            $("#color_id").html(''); 
            $('#color_id').selectpicker('refresh');
            $(".product_code_div").hide();
        }
    })

    $(document).ready(function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length == 0){
            $(".product_code_div").hide();
        }
    });
</script>
