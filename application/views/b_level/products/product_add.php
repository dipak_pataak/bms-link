<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    .condition_checkbox{
        font-size: 14px;
        overflow: scroll;
        height: 76px;
    }
	.select2.select2-container.select2-container--default {
		width: 100% !important;
	}
</style>
<!-- content / right -->
<div id="right">
    <div class="box new_product" style="height: 100%">
        <div class="row">
            <div class="col-md-12">
                <?php
                    $message = $this->session->flashdata('message');
                    if ($message)
                        echo $message;
                    ?> 
            </div>
        </div>
        <!-- box / title -->            
        <div class="title row">
            <div class="col-sm-6">
                <h5>Products New</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Products New';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <?php $menu_permission= b_access_role_permission(14); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end box / title -->
        <div class="row m-0">
            <?php echo form_open('b_level/product_controller/product_save', array('class' => 'form-row px-3 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1', 'id' => '', 'enctype' => 'multipart/form-data')); ?>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="product_name" class="mb-2">Product Name (Model Name)</label>
                    <input class="form-control" type="text" name="product_name" placeholder="Product Name" id="product_name" onmouseout="replaceString(this.value)" required>
                </div>
                <div class="form-group">
                    <label for="category_id" class="mb-2">Select Category</label>
                    <select class="form-control select2" name="category_id" id="category_id" data-placeholder='-- select one --'>
                        <!-- <option value="none">None</option> -->
                        <?php
                            foreach ($get_category as $category) {
                                echo "<option value='$category->category_id'>$category->category_name</option>";
                            }
                            ?>
                    </select>
                </div>
                <div class="form-group" id="subcategory_id">
                </div>
                <div class="form-group">
                    <label for="pattern_model_id" class="mb-2">Pattern</label> <!--<a href="#">add new</a>-->
                    <div class="pattern_model_id">
                        <select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                            <!--  <?php foreach ($patern_model as $val) { ?>
                                <option value="<?= $val->pattern_model_id ?>"><?= $val->pattern_name; ?></option>
                                <?php } ?> -->
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="theSelect" class="mb-2">Price Model/Style</label>
                    <select name="price_style_type" class="form-control price_select select2" id="theSelect" onchange="price_model_wise_style(this.value)" data-placeholder="-- select one --">
                        <option value=""></option>
                        <option value="3">Fixed product Price Style</option>
                        <option value="4">Group style</option>
                        <option value="2">Price by Sq.ft style </option>
                        <option value="1" selected>Row/Column style</option>
                        <option value="5">Sqm price style</option>
                        <!--<option value="grprice">Group Price</option>-->
                    </select>
                </div>
                <div class="hidden isnull_price mb-3"></div>
                <div class="mb-3 is1 hidden">
                    <div id="ss_pp">
                        <select name="price_rowcol_style_id" id="price_rowcol_style_id" onchange="setValue(this.value)" class="form-control select2" data-placeholder="-- select style --">
                            <option value=""></option>
                            <?php foreach ($style_slist as $s) { ?>
                            <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="price_style_id" id="price_style_id">
                        <a href="#" data-toggle="modal" data-target="#myModal"  class="btn btn-success btn-xs" style="margin-top: 8px;">add new price style</a>
                    </div>
                    <br>
                    <label for="Row_Column" class="mb-2">Row Column Style</label>
                    <div id="priceseet"></div>
                </div>
                <div class="hidden is2 mb-3">
                    <label for="sqft" class="mb-2">SqFt. Style</label>
                    <input type="text" name="sqft_price" class="form-control" placeholder="$80/sq.ft">
                </div>
                <div class="hidden is3 mb-3">
                    <label for="fixedprice" class="mb-2">Fixed Price Style</label>
                    <input type="text" name="fixed_price" class="form-control" placeholder="$29">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group product_code_div" style="position:relative;">
                    <label for="color_id" class="mb-2">Color</label>
                    <div class="color_select">
                        <select class="selectpicker form-control select-all" id="color_id" name="color_id[]" multiple data-live-search="true" required="" data-selected-text-format="count>2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product_code" class="mb-2">Status</label>
                    <select name="status" class="form-control select2" data-placeholder="-- select one --">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <!-- <div class="form-group">
                    <label for="product_code" class="mb-2">Default Discount (%)</label>
                    <input class="form-control" min="0" name="dealer_price" type="number" placeholder="20%" value="" required>
                    </div>   -->
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label for="cost_factor" class="mb-2">Cost Factor</label>
                            <input type="number" name="dealer_price" id="dealer_price" class="form-control NumbersAndDot" onkeyup="cost_factor_calculation(this.value)" onblur="cost_factor_calculation(this.value)" min="0.1" max="2.0" step="0.001" autocomplete="off" required value="1">
                            
                        </div>
                        <div class="col-sm-4">
                            <label for="individual_price" class="mb-2">Discount (%)</label>
                            <input type="text" name="individual_price" id="individual_price" class="form-control" readonly value="0.00">
                        </div>
                    </div>        
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label for="product_img" class="mb-2">File Upload</label>
                            <input type="file" class="form-control" name="product_img" id="product_img">
                            <p>Extension: jpg|jpeg|png. <br> File size: 2MB</p>
                        </div>
                        <div class="col-sm-4">
                            <img src="<?=base_url('assets/no-image.png')?>" id="prev_product_img" width="100px" height="100px">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="radio" name="is_taxable" value="1">  Taxable &nbsp;
                    &nbsp;<input type="radio" name="is_taxable" value="0" checked>  Nontaxable
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="product_description" class="mb-2">Description</label>
                    <textarea class="form-control" name="product_description" id="product_description"></textarea>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group text-right">
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                    <?php  }?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- end content / right -->
<script src="<?=base_url('assets/ckeditor/ckeditor.js')?>"></script>

<script type="text/javascript">

    $(document).ready(function(){
        CKEDITOR.replace('product_description');
    })

    function cost_factor_calculation(item) {
        var cost_factor = $("#dealer_price").val();
        var discount = 0;
        if(cost_factor != ''){
            cost_factor = (cost_factor*100).toFixed(2); 
            discount = (100 - (cost_factor));
        }
        $("#individual_price").val(discount.toFixed(2));
    }

    $("body").on("keypress keyup blur",".NumbersAndDot",function (event) {
      $(this).val($(this).val().replace(/[^0-9\.]/g,''));
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
    });

    $("body").on("change", "#product_img", function(e) {
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                swal("Please upload file less than 2MB. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
        }    
        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev_product_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#prev_product_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }

    function replaceString(v) {
        $('#product_name').val((v.replace(/['"]+/g, '&quot;')));
    }


//    ============ its for price_model_wise_style =============
    
    function price_model_wise_style(id) {

        $.ajax({
            url: "<?php echo base_url(); ?>price-model-wise-style",
            type: "post",
            data: {id: id},
            success: function (r) {

                    r = JSON.parse(r);
                    $("#price_rowcol_style_id").empty();
                    $("#price_rowcol_style_id").html("<option value=''>-- select one -- </option>");

                    $.each(r, function (ar, typeval) {
                        $('#price_rowcol_style_id').append($('<option>').text(typeval.style_name).attr('value', typeval.style_id));
                    });
                }

        });
    }



    $(document).ready(function () {
        $('select.selectpicker').on('change', function(){
            var selected = $('.selectpicker option:selected').val();
        });
//        $(".group_product").hide();
//        $('body').on('click', '.product_radio', function () {
//            var product_radio = $(this).val();
////            swal(product_radio);
//            if(product_radio == 'New Product'){
//                $(".new_product").show();
//                $(".group_product").hide();
//            }
//            if(product_radio == 'Group Product'){
//                $(".group_product").show();
//                $(".new_product").hide();
//            }
//        });
//        
        // ================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "wholesaler-category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().removeClass("hidden");
                        $('.select2').select2();
                    } else {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().addClass("hidden");
                    }
                }
            });
            // ========== its for category wise condition ===========
            $.ajax({
                url: "category-wise-condition/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $(".condition_checkbox").empty();
                        $(".condition_checkbox").html(r);
                    }
                }
            });
                // ============= its for category-wise-pattern ==============

            $.ajax({
                url: "category-wise-pattern/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $(".pattern_model_id").html(r);
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $('.selectpicker').selectpicker();
                    } else {
                        $(".pattern_model_id").html("Not Found!");
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $(".pattern_model_id").css({'color': 'red'});
                    }
                }
            });
        });
        $('.is1').removeClass("hidden");
        $("#category_id").change();
    });



    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if(value==='4' || value==='5'){
            $('.is1').slideUp().addClass("hidden");
            $('.is2').slideUp().addClass("hidden");
            $('.is3').slideUp().addClass("hidden");
        }

        theDiv.slideDown().removeClass("hidden");

        theDiv.siblings('[class*=is]').slideUp(function () {

            $(this).addClass("hidden");

        });

    });

    // $("#attrSelect").change(function () {

    //     var value = $("#attrSelect option:selected").val();
    //     var theDiv = $(".is" + value);

    //     theDiv.slideDown().removeClass("hidden");
    //     theDiv.siblings('[class*=is]').slideUp(function () {
    //         $(this).addClass("hidden");
    //     });
    // });


    $("#attrSelect").change(function () {

        var attr_type_id = $("#attrSelect option:selected").val();

        $.ajax({
            url: "<?php base_url() ?>b_level/Attribute_controller/get_attr_by_attr_types/" + attr_type_id,
            type: 'get',
            success: function (r) {
                $("#iscolor").html(r);
                $('#iscolor').slideDown().removeClass("hidden");
            }
        });


    });


    function shuffle_pricebox(elem) {
        //swal(elem + ' - ' +document.getElementById(elem).disabled);
        if (!document.getElementById(elem).disabled)
            document.getElementById(elem).disabled = true;
        else
            document.getElementById(elem).disabled = false;
    }

</script>

<?php
$this->load->view($js)?>

<script type="text/javascript">

    $("form").on('change','#pattern_model_id',function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length > 0){
            
            $.ajax({
                url: "<?php echo base_url(); ?>get_colors_based_on_pattern_ids",
                type: 'post',
                data: {"pattern_ids":pattern_ids},
                success: function (r) {
                    if (r !== '') {
                        var color_data = JSON.parse(r);
                        var color_option = '';
                        $.each(color_data, function(index, item) {
                            color_option += '<option value="'+item.id+'">'+item.color_name+'->('+item.color_number +')';
                        });
                        var select_all = '';
                        if(color_option != ''){
                            select_all = '<option value="[all]" class="select-all">Select All</option>';
                            $("#color_id").html(select_all + color_option); 
                            $('#color_id').selectpicker('refresh');
                            // $('#color_id option').attr("selected","selected");
                            $('a.select-all').click();
                        }else{
                            $("#color_id").html('');
                            $('#color_id').selectpicker('refresh'); 
                        }
                        $(".product_code_div").show();
                    } else {
                        $("#color_id").html(''); 
                        $('#color_id').selectpicker('refresh');
                        $(".product_code_div").hide();
                    }
                }

            });

        }else{
            $("#color_id").html(''); 
            $('#color_id').selectpicker('refresh');
            $(".product_code_div").hide();
        }
    })

    $(document).ready(function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length == 0){
            $(".product_code_div").hide();
        }
    });
</script>
