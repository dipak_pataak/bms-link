<?php $packageid=$this->session->userdata('packageid'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">    
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Sqm Price Mapping</h5>
            <?php
                $page_url = $this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Sqm Price Mapping';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <!-- end box / title -->
        <div class="px-3 row m-0">
						
            <?=form_open('b_level/product_controller/save_sqm_price_model_mapping',array('class'=>'col-xs-12 col-sm-8 col-md-7 col-lg-6 p-0'))?>

            <div class="form-group row">
                <label for="group_price" class="col-xs-4 col-form-label"> Sqm price<sup class="text-danger">*</sup></label>
                <div class="col-xs-8">
                   <input type="number"  min="0" oninput="validity.valid||(value='');" class="form-control" placeholder="Enter price here" name="sqm_price">
                </div>
            </div>


            <div class="form-group row">
                <label for="product_id" class="col-xs-4 col-form-label">Product<sup class="text-danger">*</sup></label>
                <div class="col-xs-8">
                    <select name="product_id" class="form-control select2 " onchange="getPatter(this.value)">
                        <option value="">--Select Product--</option>
                        <?php foreach ($product_info as $p) { ?>
                            <option value="<?php echo $p->product_id ?>"><?= $p->product_name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="pattern_idss" class="col-xs-4 col-form-label">Pattern<sup class="text-danger">*</sup></label>
                <div class="col-xs-8">
                    <select class="select2 form-control" id="pattern_idss" name="pattern_id[]" multiple data-live-search="true" required="">

                    </select>
                </div>
            </div>


            <div class="form-group  text-right">
               <?php  if(!empty($packageid)){?>
                <button type="submit" class="btn btn-success w-md m-b-5">Save</button>
                <button type="reset" class="btn btn-primary w-md m-b-5">Reset</button>
              <?php } ?>
            </div>

            <?=form_close();?>
        </div>
    </div>

    <div class="box">
        <div class="title">
            <h5>List Of Sqm Price Mapping</h5>
        </div>
        <div class="action_btn text-right" style="padding: 0px 1rem;">
             <?php  if(!empty($packageid)){?>
            <button type="button" class="btn btn-info btn-sm mb-1" data-toggle="modal" data-target="#importSqmPrice">Bulk Import Upload</button>   
            <a href="<?=base_url('export-wholesaler-sqm-price-mapping')?>" class="btn btn-info btn-sm mb-1">Export</a>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" onClick="return action_delete(document.recordlist)" style="margin-top: -3px !important;" >Delete</a>
               <?php } ?>
        </div>  
        <div class="table-responsive p-3">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/product_controller/manage_sqm_mapping_action') ?>">
                <input type="hidden" name="action">
                <table id="SqmMappingDataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th width="2%"><input type="checkbox" id="SellectAll"/></th>
                            <th width="3%">#.SL</th>
                            <th width="30%">Product Name</th>
                            <th width="30%">Pattern Name</th>
                            <th width="25%">Sqm price</th>
                            <th width="10%" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </form>  
        </div>
		</div>
	</div>

</div>

<!-- Modal -->
<div class="modal fade" id="importSqmPrice" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sqm Price Mapping Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <a href="<?php echo base_url('assets/b_level/csv/sqm_price_mapping_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                <?php echo form_open_multipart('import-sqm-price-mapping', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                </div>

                </form>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
<!-- Modal -->

<!-- end content / right -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    function getPatter(id){
        var url = "<?php echo site_url('b_level/Product_controller/get_product_pattern')?>/"+id;
        $.ajax({
            url : url,
            type: "GET",
            success: function(data) {
                $('#pattern_idss').html(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal('Error get data from ajax');
            }
        });
    }

    $(document).ready(function() {
        var orderable = [0, 1, 5];
        var dt = init_datatable('SqmMappingDataTable','<?php echo base_url('b_level/product_controller/get_sqm_price_mapping_Lists/'); ?>', orderable);
    });
</script>
