<?php $package=$this->session->userdata('packageid'); ?>
<style type="text/css">
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .select2.select2-container.select2-container--default, .select2-container--default .select2-selection--single .select2-selection__rendered{
        width: 100%!important;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6"> 
                <h5>Manage Products</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Products';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <?php $menu_permission= b_access_role_permission(15); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end box / title -->
        <p class="px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <?php if (!empty($package)) { ?>             
            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="add-product" class="btn btn-success">Add New Product</a>
                            
            <a href="javascript:void(0)" class="btn btn-success btn-sm mb-1"  data-toggle="modal" data-target="#productBulk">Bulk Upload</a>
            <a href="<?=base_url('export-wholesaler-products')?>" class="btn btn-info btn-sm mb-1">Export</a>
            <?php } ?>
             <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="javascript:void(0)" class="btn btn-danger action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
             <?php } ?>

            <?php } ?>
        </p>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="productBulk" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!--<h4 class="modal-title">Modal Header</h4>-->
                    </div>
                    <div class="modal-body">
                        <a href="<?php echo base_url('assets/b_level/csv/product_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-primary">
                            The first line in downloaded csv file should remain as it is. Please do not change the order of columns.
                        </span><br><br>
                        <strong>Price Style Type</strong><br>
                        1 For Row/Column style<br>
                        2 For Price by Sq.ft  style<br>
                        3 For Fixed product Price Style <br>
                        4 For Group style <br>
                        5 For sqm price style
                        <br><br><br>
                        <?php echo form_open_multipart('product-bulk-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                        </div>

                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="collapse px-3 mt-3" id="collapseExample">
            <div class="border px-3 pt-3 mb-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>product-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <!-- <div class="col-md-4">
                                <input type="text" class="form-control mb-3 product_name" name="product_name" placeholder="Product Name">
                            </div><span class="or_cls">-- OR --</span> -->
                            <div class="col-sm-3 mb-2">
                                <select class="form-control select2 category_name" name="category_name" data-placeholder="-- Select Category --">
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                    }
                                    ?>
                                </select>
                            </div> 

                             <div class="col-sm-3 mb-2">
                                <select class="form-control select2 price_style_type mb-2" name="price_style_type" data-placeholder="-- Select Price Style --">
                                    <option value=""></option>
                                    <option value="1">Row/Column style</option>
                                    <option value="4">Group style</option>
                                    <option value="5">Sqm price style</option>
                                    <option value="2">Price by Sq.ft style </option>
                                    <option value="3">Fixed product Price Style</option>
                                </select>
                            </div>
                             <div class="col-sm-3 mb-2">
                                <select class="form-control select2 status mb-2" name="status" data-placeholder="-- Select Status --">
                                    <option value=""></option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                    
                                </select>
                            </div>


<!--                            &nbsp; <span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <select class="form-control select2 pattern_model" name="pattern_model" data-placeholder="-- Select Pattern Model --">
                                    <option value=""></option>
                            <?php
                            foreach ($patern_model as $single_pattern) {
                                echo "<option value='$single_pattern->pattern_model_id'>$single_pattern->pattern_name</option>";
                            }
                            ?>
                                </select>
                            </div>-->

                            <div class="col-sm-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right mt-1">
            <div class="form-group row m-0 search_div float-right">
                <label for="keyword" class="col-form-label text-right"></label>
                    <input type="text" class="form-control mb-3" name="keyword" id="keyword" onkeyup="productkeyup_search()" placeholder="Search..." tabindex=""/>
                <!--                <div class="col-sm-1 dropdown" style="margin-left: -22px;">
                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                    </ul>
                                </div>-->
            </div>          
        </div>
        <form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Product_controller/manage_action') ?>">
            <input type="hidden" name="action">
            <?= @$links; ?>
			<div class="table-responsive">
            <table class="table table-bordered mb-3" id="results_product">
                
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th width="5%">#</th>
                        <th width="25%">Product Name</th>
                        <th width="15%">Category</th>
                        <th width="15%">Price Style Type</th>
                        <th width="16%">Default Cost Factor</th>
                        <th width="9%">Status</th>
                         <?php if (!empty($package)){ ?>
                        <th  width="15%" class="text-center">Action</th>
                         <?php } ?>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($products)) {
                        $i = 0+$pagenum;
                        foreach ($products as $key => $p) {
                            $i++;
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $p->product_id; ?>" class="checkbox_list">  
                                </td>
                                <td><?= $i ?></td>
                                <td><?= $p->product_name ?></td>
                                <td>
                                    <?php
                                    if ($p->product_cat_id == 0) {
                                        echo "None";
                                    }
                                    echo $p->category_name;
                                    ?>
                                </td>
                                <!--<td><?= $p->pattern_name ?></td>-->
                                <td>
                                    <?php
                                    if ($p->price_style_type == 1) {
                                        echo "Row/Column style";
                                    } elseif ($p->price_style_type == 2) {
                                        echo "Price by Sq.ft style";
                                    } elseif ($p->price_style_type == 4) {
                                        echo "Group Price";
                                    } elseif($p->price_style_type == 3){
                                        echo "Fixed product Price Style";
                                    }
                                    elseif($p->price_style_type == 5){
                                        echo "Sqm price style";
                                    }
                                    ?>
                                </td>
                                <!--<td><?= $p->init_stock ?></td>-->
                                <td><?= $p->dealer_price ?></td>
                                <!--<td><?= $p->individual_price ?></td>-->
                                <td>
                                    <?php
                                    if ($p->active_status == 1) {
                                        echo 'Active';
                                    } else {
                                        echo 'Inactive';
                                    }
                                    ?>
                                </td>
                                <?php if (!empty($package)) { ?>

                                <td class="text-right">

                                     <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                        <a href="<?php echo base_url(); ?>product-edit/<?= $p->product_id ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    <?php } ?>

                                     <?php if($menu_permission['access_permission'][0]->can_access==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                        <a href="<?php echo base_url('mapping-product-attribute'); ?>/<?= $p->product_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Map product to Attributes" ><i class="fa fa-map"></i></a>
                                    <?php } ?>

                                    <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                        <a href="<?php echo base_url(); ?>delete-product/<?= $p->product_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" ><i class="fa fa-trash"></i></a>
                                    <?php } ?>

                                </td>
                                <?php } ?>

                            </tr>

                            <?php
                        }
                    }
                    ?>

                </tbody>
                <?php if (empty($products)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="10" class="text-center text-danger">No record found!</th>
                        </tr> 
                    </tfoot>
                <?php } ?>
            </table>
            </div>
			<?= @$links; ?>
<!--            <p class="text-right">
                <button type="submit" class="btn btn-success default w-md m-b-5">Save</button>
            </p>-->
        </form>

    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    function productkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>wholesaler-product-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#results_product").html(r);
            }
        });
    }
</script>
