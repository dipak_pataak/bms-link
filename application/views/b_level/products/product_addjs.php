<style type="text/css">
    .modal-dialog {
        max-width: 850px;
    }
</style>

<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Row Column Price Style</h4>
          </div>

            <div class="modal-body">

                <div class="form-row">

                    <div class="alert alert-warning">
                        <strong>Notes! </strong> 1. Do not use any text. 2. Please use (') single quote twice to show inch symbol. ex: 60''
                    </div>

                    <div class="col-md-12 col-lg-10">
                        <div id="exdata">
                            <label class="col-form-label mb-1">Paste excel data here:</label>
                            <textarea name="excel_data" style="width:100%; height:150px;" onblur="javascript:generateTable()"></textarea>
                        </div>
                        <?=form_open('#',array('id'=>'formStylePrice','name'=>'formStylePrice','class'=>'mt-3'))?>
                            <input type="hidden" name="style_type" id="style_type"> 
                            <label class="col-form-label mb-1">Price Style name</label>
                            <input type="text" name="price_style_name" class="form-control mb-3" placeholder="Enter style name" required>
                        
                            <label for="productquantity" class="col-form-label mb-1">Price Sheet</label>
                            <div id="excel_table"></div>
                      

                            <button type="submit" class="btn btn-success">Save</button><hr>
                   
                        <?php echo form_close();?>
                        <!--<p>The .table-bordered class adds borders to a table:</p>--> 
						<div class="table-responsive mt-3">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Edit Price by</th>
                                <th>Apply to</th>
                                <th>Value</th>
                                <th width="100">Action</th>
                              </tr>
                            </thead>

                            <tbody>

                              <tr>
								
								<td>
							<div class="form-check mb-1">
							  <input class="form-check-input" type="radio" name="optradio" id="percentage">
							  <label class="form-check-label" for="percentage">
								Percentage
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="optradio" id="fixed">
							  <label class="form-check-label" for="fixed">
								Fixed
							  </label>
							</div>
                        </td>

                        <td>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="apply" id="whole">
							  <label class="form-check-label" for="fixed">
								Whole table
							  </label>
							</div>
                        </td>

                                <td>
                                    <label class="col-form-label">
                                        Enter value
                                    </label>
                                    <input type="number" name="val" id="setVal" class="form-control">
                                </td>

                                <td class="text-right">
                                    <button class="btn btn-sm btn-success" onclick="plusData()">+</button>
                                    <button class="btn btn-sm btn-danger" onclick="minusData()">-</button>
                                    
                                </td>
                               
                              </tr>
                              
                            </tbody>
                        </table>
						</div>
					</div>                 
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>



<script type="text/javascript">



function generateTable() {

    var data = $('textarea[name=excel_data]').val();
    //console.log(data);
    var exdata = data.trim();

    if(exdata!==''){

        var rows = data.split("\n");

        var table = $('<table class="table-bordered" />');

        var pr=0;

        for(var y in rows) {
           pr++;
            //var cells = rows[y].split("\t");
            var cells = rows[y].replace(/["]+/g, "''").split("\t");
            var row = $('<tr /><br/>');
                row.append('<input type="hidden" name="row[]" value=\"'+cells+'\"/>');
                var n=0;
            for(var x in cells) {
                n++;

                if(pr!=1 && n!=1){
                    var clss = n+"_"+pr;
                    row.append('<input type="text" size="5" name="test'+n+pr+'" value=\"'+cells[x]+'\" class="price_input fr" id="'+clss+'" autocomplete="off"/>');
                }else{
                    var ids = n+pr;
                    var vv = (cells[x].replace(/["]+/g, "''"));

                    row.append('<input type="text" class="fr" id="tfr_'+ids+'" size="5" name="test'+n+pr+'" value="'+vv+'" autocomplete="off"/>');
                }
            }
            table.append(row);
        }

        // Insert into DOM
        $('#excel_table').html(table);
       // $('#exdata').slideUp();

    } else {

        swal('Enter your excel data');

        $('#exdata').show();

    }

}




    $( "#whole" ).on( "click", function() {
        $('.price_input').css('border',' 2px solid #cabdbd');
        $("#whole").val(1);
        $("#selection").val(0);
    });

    $( "#selection" ).on( "click", function() {
        $('.price_input').css('border','');
        $("#whole").val(0);
        $("#selection").val(1);
    });


    $( "#percentage" ).on( "click", function() {
        $("#percentage").val(1);
        $("#fixed").val(0);
    });

    $( "#fixed" ).on( "click", function() {
        $("#percentage").val(0);
        $("#fixed").val(1);
    });




    function plusData(){

        var percentage = $("#percentage").val();

        var setVal = $("#setVal").val();

        var whole = $("#whole").val();
        var selection = $("#selection").val();
        if(whole!=0){

            if(percentage!=0){

                $(".price_input").each(function(){
                   var id = $(this).attr('id');
                   var getVal = $("#"+id).val();
                   var newVal = (getVal/100)*setVal;
                   $("#"+id).val(parseInt(getVal)+parseInt(newVal));
                });
            
            }else{

                $(".price_input").each(function(){
                   var id = $(this).attr('id');
                   var getVal = $("#"+id).val();
                   $("#"+id).val(parseInt(getVal)+parseInt(setVal));
                });

            }
            
        }

    }



        function minusData(){

            var percentage = $("#percentage").val();
            var setVal = $("#setVal").val();
            var whole = $("#whole").val();
            var selection = $("#selection").val();

            if(whole!=0){
                if(percentage!=0){

                    $(".price_input").each(function(){
                       var id = $(this).attr('id');
                       var getVal = $("#"+id).val();
                       var newVal = (getVal/100)*setVal;
                       $("#"+id).val(parseInt(getVal)-parseInt(newVal));
                    });
                
                } else {

                    $(".price_input").each(function(){
                       var id = $(this).attr('id');
                       var getVal = $("#"+id).val();
                       $("#"+id).val(parseInt(getVal)-parseInt(setVal));
                    });

                }
                
            }
        }





    // submit form and add data
    $("#formStylePrice").on('submit',function(e){
        e.preventDefault();
            $("#style_type").val($("#theSelect").val());
            var submit_url = "/save-price-style";
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: $(this).serialize(),
                success: function(res) {
                    
                    if(res==='1'){
                        swal('Save successfully');
                    }
                    if(res==='2'){
                        swal('Internal error plese try again');
                    }
                        
                    $('#myModal').modal('hide'); 
                    $("#ss_pp").load(" #ss_pp > *");
                    
                },error: function() {
                    swal('error');
                }
            });
    });



    function setValue(id){

        $('#price_style_id').val(id);

        var url = "<?php echo site_url('b_level/Product_controller/get_price_style')?>/"+id;
        $.ajax({
            url : url,
            type: "GET",
            success: function(data)
            {
                $('#priceseet').html(data)
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                swal('Error get data from ajax');
            }
        });

    }

</script>


<script type="text/javascript">

    $("#product_name").on('change',function(e){
        $('#product_name').val(($('#product_name').val().replace(/['"]+/g, '&quot;')));
        
    });

    function replaceString(vv,id){
        //$('#tfr_'+id).val((vv.replace(/['"]+/g, "&#34;")));
    }

    $('#product_name').val(($('#product_name').val().replace(/['"]+/g, '&quot;')));

</script>