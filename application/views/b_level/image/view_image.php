<style type="text/css">
    .wholesaler-gallery-image-view .col-form-label {
        font-size: 12px;
        font-weight: 600;
    }
    .wholesaler-gallery-image-view .col-form-data span {
        line-height: 27px;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row m-1">
            <h5 class="col-sm-6">View Image</h5>
        </div>        
        <div id="results_image" class="wholesaler-gallery-image-view">
            <div class="p-3">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                           <img src="<?php echo base_url(); ?><?=$images_tag_data->image?>" class="img-thumbnail" id="prevImg">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        $tags = $this->db->from('tag_tbl')->where('tag_tbl.created_by', $this->session->userdata('user_id'))->get()->result();
                        if (!empty($tags)) {
                            foreach ($tags as $key => $val) {
                                ?>
                                <div class="form-group row">
                                    <label for="<?= $val->tag_name; ?>" class="col-sm-3 col-form-label"><?= $val->tag_name; ?></label>
                                    <div class="col-sm-9 col-form-data">
                                        <?php
                                        $image_id = $images_tag_data->image_id;
                                        if(in_array($val->id, $tag_ids)){
                                        ?>
                                        <span><?=$tag_data[$val->id]?></span>
                                    <?php 
                                        } else { ?>
                                             <span></span>
                                       <?php 
                                    }
                                    ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        //dd($images_tag_data);
                        ?>
                    </div>
                </div>
                <div class="form-group text-left">
                    <a href="<?php echo base_url('edit_wholesaler_gallery_image/'.$images_tag_data->id); ?>" class="btn btn-primary w-md m-b-5" autocomplete="off">Edit</a>
                    <a href="<?= base_url('manage_wholesaler_gallery_image') ?>" class="btn btn-success w-md m-b-5" autocomplete="off">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->