<?php $packageid=$this->session->userdata('packageid'); ?>
<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .gallery-image-wrapper .gallery-image-outer img{
        width: 100%;
        height: 145px;
    }
    .gallery-image-wrapper .gallery-image-outer {
        position: relative;
        margin-bottom: 20px;
        overflow: hidden;
    }
    .gallery-image-wrapper .gallery-manage-btnmain {
        text-align: center;
        position: absolute;
        top: auto;
        bottom: -10px;
        margin: 0 auto;
        left: 0;
        right: 0;
        opacity: 0;        
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        bottom: 10px;
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper {
        text-align: center;
    }
    .gallery-image-wrapper a.load-more-btn {
        font-size: 14px;
        color: #ffffff;
        background-color: #336699;
        vertical-align: middle;
        padding: 0 15px;
        height: 40px;
        line-height: 40px;
        border-radius: 3px;
        margin: 30px auto 0 auto;
        text-decoration: none;
        display: none;
    }
    /* grid & list */
    .grid-list-wrapper button {
        font-size: 14px;
        background-color: #369;
        color: #ffffff;
        text-shadow: none;
        padding: 0;
        height: 30px;
        width: 30px;
        border: none;
    }
    .gallery-image-wrapper .some-list-load.row {
        margin-left: 0;
        margin-right: 0;
    }
    .gallery-image-wrapper .some-list-load.list {
        display: block;
        text-align: left;
    }
    .gallery-image-wrapper .some-list-load.grid .gallery-manage-btnmain-list {
        display: none;
    }
    .gallery-image-wrapper .product-grid .gallery-manage-btnmain a span {
        display: none;
    }
    .gallery-image-wrapper .product-list img {
        width: 25%;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain {
        right: 15px;
        bottom: auto;
        top: 0;
        left: auto;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a {
        display: block;
        margin: 5px 0;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a i {
        margin-right: 5px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer .gallery-manage-btnmain ,
    .gallery-image-wrapper .product-list .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%); 
        -o-transform: translateY(-50%); 
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        bottom: auto;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer {
        border: 1px solid #eeeeee;
        padding: 15px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer:hover:before {
        opacity: 0;
    }
    .gallery-manage-check {
        position: absolute;
        top: 10px;
        right: 10px;
    }
    @media(max-width: 1199px){
        .gallery-image-wrapper .gallery-image-outer img {
            min-height: 175px;
        }
    }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
         <div class="title">
            <h5>Manage Image</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Manage Image';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <?php $menu_permission= b_access_role_permission(120); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <p class="px-4">
            <div class="text-right" style="margin-right: 10px;">
                <?php if (!empty($packageid)) { ?>
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="<?php echo base_url('add_wholesaler_gallery_image'); ?>" class="btn btn-success" id="add_new_tag" style="margin-top: -6px !important;">Add Image</a>
                <?php }?>
                <?php if(count($images) > 0){ ?>
                    <!-- <a href="<?php echo base_url('export_wholesaler_gallery_image'); ?>" class="btn btn-info" style="margin-top: -6px !important;">Export</a> -->
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importGalleryTag">Bulk Import Upload</button>
                    <?php }?>
                <?php } ?>  
                 <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>  
                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete_order(document.recordlist)" style="margin-top: -6px !important;">Delete</a>
                <?php }?>
            <?php  } ?>
            </div>    
        </p>
        <?php if(count($filter_arr) > 0) { ?>
        <div class="px-4 mt-3">
            <div class="border pt-3 px-3">
                <form class="form-horizontal" action="<?php echo base_url('manage_wholesaler_gallery_image'); ?>" method="post" id="galleryFilterFrm">
                    <fieldset>
                        <div class="row">
                            <?php  foreach($filter_arr as $filter) { ?>
                            <div class="col-sm-3 px-1 mb-3">
                                <select class="form-control filter-select-box" name="tag_<?=$filter['tag_id']?>" id="tag_<?=$filter['tag_id']?>" >
                                    <option value="">--Select <?= $filter['tag_name']; ?>--</option>
                                    <?php foreach($filter['value'] as $value) { ?>
                                        <option value="<?= $value['tag_value']?>"><?= $value['tag_value']?></option>
                                    <?php }?>
                                </select>
                                <script>
                                    $('#tag_'+<?=$filter['tag_id']?>).val('<?= $filter['selected_val']; ?>');
                                </script>
                            </div>
                            <?php } ?>
                            <div class="col-sm-3 px-1">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="filter_field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <?php }?>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div id="results_color">
            <form name="recordlist" id="mainform" method="post" action="<?php echo base_url('b_level/Image_controller/manage_action') ?>">
                <input type="hidden" name="action">
                    <div class="gallery-image-wrapper">
                        <div class="some-list-load row grid">
                            <?php
                            $i = 0 + $pagenum;
                            if (!empty($images)) { ?>
                                <div class="col-sm-12 text-left" style="margin: 10px;">
                                    <input type="checkbox" name="" id="master"> 
                                    <label><b>Select All</b></label>                        
                                </div>
                                <?php foreach ($images as $key => $val) {
                                    ?>
                                    <div class="col-xl-3 col-lg-4 col-sm-4 col-md-12 content product-grid">
                                        <div class="gallery-image-outer">
                                            <div class="gallery-manage-check">
                                                <input type="checkbox" name="gallery_img_id[]" value="<?= $val->id; ?>" class="sub_chk">
                                            </div>    
                                            <img src="<?= base_url($val->image); ?>" class='gallery_image'>
                                            <div class="gallery-manage-btnmain">
                                                <a href="<?php echo base_url('view_wholesaler_gallery_image/'.$val->id); ?>" class="btn btn-primary default btn-sm view_image" id="view_image" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye" aria-hidden="true"></i><span>view image</span>
                                                </a>
                                                <a href="<?php echo base_url('edit_wholesaler_gallery_image/'.$val->id); ?>" class="btn btn-warning default btn-sm edit_image" id="edit_image" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i><span>edit image</span></a>
                                                <a href="<?php echo base_url('b_level/Image_controller/delete_image/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm delete_image" data-toggle="tooltip" data-placement="top" data-original-title="Delete" ><i class="fa fa-trash"></i><span>delete image</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <a href="#" class="load-more-btn" id="loadMore">View More <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                    </div>

                    <?php if (empty($images)) { ?>
                        <div class="text-center text-danger">No record found!</div>
                    <?php } ?>
                <?php echo $links; ?>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importGalleryTag" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gallery Tag Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <a href="<?php echo base_url('export_wholesaler_gallery_image'); ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                <?php echo form_open_multipart('import_wholesaler_gallery_image_tag', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                </div>

                </form>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
<!-- Modal -->

<script>
function filter_field_reset() {
    $(".filter-select-box").val('');
    $('#galleryFilterFrm').submit();
}

$(document).on("change", ".sub_chk", function() {
    var all_checked = $("input[type='checkbox'].sub_chk");

    var values = new Array();
    $.each($("input[type='checkbox']:checked"), function() {
        values.push($(this).val());
    });
    $('#check_id').val(values);

    if (all_checked.length == all_checked.filter(":checked").length) {
        $("#master").prop('checked', true);
    } else {
        $("#master").prop('checked', false);
    }
});

$('#master').on('change', function(e) {
    if ($(this).is(':checked', true)) {
        $(".sub_chk").prop('checked', true);
        var values = new Array();
        $.each($("input[class='sub_chk']:checked"), function() {
            values.push($(this).val());
        });
        $('#check_id').val(values);
    } else {
        $(".sub_chk").prop('checked', false);
    }
});

function action_delete_order(frm) {
    with(frm) {
        var flag = false;
        str = '';
        field = document.getElementsByName('gallery_img_id[]');
        for (i = 0; i < field.length; i++) {
            if (field[i].checked == true) {
                flag = true;
                break;
            } else
                field[i].checked = false;
        }
        if (flag == false) {
            swal("Please select atleast one record");
            return false;
        }
    }
    if (confirm("Are you sure to delete selected records ?")) {
        frm.action.value = "action_delete";
        // frm.mul_image_delete.value = $("#hidden_multi_order_id").val();
        frm.submit();
        return true;
    }
}
</script>
<!-- end content / right -->





