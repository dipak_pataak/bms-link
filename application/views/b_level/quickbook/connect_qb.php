<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php 
                    $message = $this->session->flashdata('message');
                    if($message)
                        echo $message;
                ?>
            </div>
        </div>


        <!-- box / title -->
        <div class="title row">
            <h5> Connect To QuickBooks</h5>
        </div>
        <script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere-1.3.3.js">
        </script>
        <script type="text/javascript">
        intuit.ipp.anywhere.setup({
            grantUrl: '<?=site_url('index.php/qb_oauth_endpoint')?>',
            datasources: {
                quickbooks: true,
                payments: true
            },
            paymentOptions: {
                intuitReferred: true
            }
        });
        </script>
        <!-- end box / title -->
        <form class="px-3" name="edit_purchase">

            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group row">
                        <code>
            <ipp:connectToIntuit></ipp:connectToIntuit>
        </code>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group row">
                        <?php if($this->session->userdata('access_token')): ?>
                        <h2 style="color: #006600">Sucessfully connected...</h2>
                        <?php endif; ?>

                    </div>
                </div>
                
<!--                <div class="col-sm-12">
                    <div class="form-group row">
                        <h3>Default things going to create into your quickbook account.</h3>
                        <li>Inventory Categories</li>
                        <li>Misc</li>
                        <li>Cellular</li>
                        <li>Shutters</li>
                        <li>Arch</li>
                        <li>Shades</li>
                        <li>Blinds</li>
                            
                            
                            
                    </div>
                    
            </div>-->



        </form>

    </div>
</div>