<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php 
                    $message = $this->session->flashdata('message');
                    if($message)
                        echo $message;
                ?>
            </div>
        </div>


        <!-- box / title -->
        <div class="title row">
            <h5> Test QuickBooks</h5>
        </div>
        <form>
            <div class="row col-md-12">

                <?php if(!empty($vendors)){ ?>
                <h3>Some vendors from my QB Sandbox</h3>


                <br>
                <?php foreach($vendors as $vendor): ?>

                <strong>Display Name: </strong><span><?=$vendor->DisplayName?></span>
                <br>
                <strong>Currency: </strong><span><?=$vendor->CurrencyRef?></span>
                <br>

                <?php endforeach; ?>
                <?php }else{?>
               <h2> Please connect to QuickBook First! </h2>
                <?php } ?>

            </div>
        </form>

    </div>
</div>