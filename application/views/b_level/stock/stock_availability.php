<?php $get_company=$this->db->query("SELECT * FROM company_profile WHERE user_id".$this->user_id)->row();?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Stock Availablity</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Stock Availablity';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
<!--                    <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Product Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Category Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Stock">
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="reset" class="btn btn-sm btn-danger default">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="px-3">
			<div class="table-responsive">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Raw Material</th>
                        <th>Pattern</th>
                        <th>Color</th>
                        <th>In Qty/(Sq.<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                        <th>Out Qty/(Sq.<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                        <th>Available Qty(<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($get_allrecord as $single) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $single->material_name; ?></td>
                            <td><?php echo ($single->pattern_name!=null?$single->pattern_name:'n/a'); ?></td>
                            <td><?php echo ($single->color_name!=NULL?$single->color_name:'n/a'); ?></td>
                            <td><?php echo $single->inqty; ?></td>
                            <td><?php echo $single->outqty; ?></td>
                            <td>
                                <?php echo $availableqty = $single->inqty - $single->outqty; ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <?php if (empty($get_allrecord)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
			</div>
		</div>
    </div>
</div>
<!-- end content / right -->
