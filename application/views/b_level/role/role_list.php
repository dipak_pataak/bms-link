<?php $packageid=$this->session->userdata('packageid'); ?>
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <?php
                        $error = $this->session->flashdata('error');
                        $success = $this->session->flashdata('success');
                        if ($error != '') {
                            echo $error;
                        }
                        if ($success != '') {
                            echo $success;
                        }
                    ?>
                    <?php $menu_permission= b_access_role_permission(74); ?>

                    <!-- box / title -->
                    <div class="title row">
                        <div class="col-sm-6">
                            <h5>Role List</h5>
                            <?php
                                $page_url = $this->uri->segment(1);
                                $get_favorite = get_b_favorite_detail($page_url);
                                $class = "notfavorite_icon";
                                $fav_title = 'Role List';
                                $onclick = 'add_favorite()';
                                if (!empty($get_favorite)) {
                                    $class = "favorites_icon";
                                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                                }
                            ?>
                            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <?php if (!empty($packageid)) { ?>
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                     <a href="<?php echo base_url(); ?>b_level/role_controller/role_permission" class="btn btn-success ml-3 mb-1">New Role</a>
                      <?php }?>
                       <?php } ?>
                    <!-- end box / title -->
                    <div class="px-3">
						<div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                               <tr>
                                    <th>SL No</th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                      <?php if (!empty($packageid)) { ?>
                                    <th class="text-center">Action</th>
                                        <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($role_list)) {
                                    $sl = 0;
                                    foreach ($role_list as $key => $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->role_name; ?></td>
                                            <td><?php echo $value->description; ?></td>
                                            <?php if (!empty($packageid)) { ?>
                                            <td class="text-right">
                                                 <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                                <a href="<?php echo base_url(); ?>b_level/role_controller/role_edit/<?php echo $value->id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                                 <?php }?>
                                                  <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                                <a href="<?php echo base_url(); ?>b_level/role_controller/role_delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                 <?php }?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if(empty($role_list)){ ?>
                            <tfoot>
                                <tr>
                                    <th class="text-danger text-center" colspan="6">Record not found!</th>
                                </tr>
                            </tfoot>
                            <?php } ?>
                        </table>
						</div>
					</div>
                </div>
            </div>
            <!-- end content / right -->
            
