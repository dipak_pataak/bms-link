
<!-- content / right -->
<div id="right">
    <!-- table -->
<?php $packageid=$this->session->userdata('packageid'); ?>
    <div class="box" style="height: 100%">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title">
            <h5>Assign User Role</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Assign User Role';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>


        <div class="px-3">

            <form action="<?php echo base_url(); ?>b_level/role_controller/assign_user_role_save" method="post" class="form-horizontal">

                <div class="row">
                    <div class="col-sm-7 col-md-7 col-lg-5">
                        <div class="form-group row">
                            <label for="user" class="col-sm-3 col-form-label  text-right">User<sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <select name="user_id" class="form-control select2" id="user_id" onchange="userRole(this.value)" data-placeholder="Select Option" required>
                                    <option value=""></option>
                                    <?php
                                    foreach ($get_users as $user) {
                                        echo '<option value="' . $user->id . '">' . $user->fullname . " ->( " . $user->email . " ) " . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">

                            <label for="role" class="col-sm-3 col-form-label text-right">Role<sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true" required>
                                    <?php foreach ($role_list as $val) { ?>
                                        <option value="<?= $val->id ?>"><?= ucwords($val->role_name); ?></option>
                                    <?php } ?>
                                </select>
                                <?php foreach ($role_list as $val) { ?>
                                    <!--                                    <label class="radio-inline">
                                                                            <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"> <?php echo $val->role_name; ?>
                                                                        </label> -->
                                <?php } ?>
                            </div>
                        </div>
						<div class="form-group text-right">
							<!--<button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>-->
                             <?php $menu_permission= b_access_role_permission(71); ?>
                             <?php if (!empty($packageid)) { ?>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
							<button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                                <?php } ?>
                              <?php } ?>
                            
						</div>
                    </div>
                    <div class="col-md-4">
						<div class="pt-1" style="background: #ddd;">
							<h4>Assigned Role</h4>                                                            
							<div id="existrole">
								<ul style="margin-left: 20px; margin-bottom: 15px;">

								</ul>
							</div>
						</div>
                    </div>
                </div>
            </form>
        </div>        

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function userRole(t) {
        $.ajax({
            url: "<?php echo base_url(); ?>wholesaler-check-user-role",
            type: 'post',
            data: {user_id: t},
            success: function (r) {
                r = JSON.parse(r);
                $("#existrole ul").empty();
                $.each(r, function (ar, typeval) {
                    if (typeval.role_name == 'Not Found') {
                        $("#existrole ul").html("Not Found!");
                        $("#exitrole ul").css({'color': 'red'});
                    } else {
                        $("#existrole ul").append('<li>' + typeval.role_name + '</li>');
                    }
                });
            }
        });
    }
</script>
<!--   if (typeval.role_name == 'Not Found') {
                        $("#existrole ul").html("Not Found!");
                        $("#exitrole ul").css({'color': 'red'});
                    } else {
                        $("#existrole ul").append('<li>' + typeval.role_name + '</li>');
                    }-->
