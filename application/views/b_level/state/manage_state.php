<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<?php $packageid=$this->session->userdata('packageid'); ?>
<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or-filter .col-sm-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 25%;
			max-width: 25%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">    
                <h5>Manage State</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage State';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 float-right text-right">
            </div>
        </div>
        <div class="" style="margin: 10px;">
            <?php if (!empty($packageid)) { ?>
            <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importState">Bulk Import Upload</button>
            <a href="javascript:void(0)" class="btn btn-success" id="add_new_state">Add State</a>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)">Delete</a>
             <?php } ?>

            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div id="results_state">
            <form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/State_controller/manage_action') ?>">
            <input type="hidden" name="action">
				<div class="table-responsive">
                <table id="stateListTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>State Id</th>
                            <th>State Name</th>
                            <th>City Name</th>
                            <?php if (!empty($packageid)) { ?>
                            <th>Action</th>
                           <?php } ?>
                        </tr>
                    </thead>
                </table>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importState" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">State Bulk Upload</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <a href="<?php echo base_url('assets/b_level/csv/state_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                    <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('import-state-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->
<?php
$this->load->view('b_level/state/stateJs');
?>
<!-- end content / right -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_state').on('click', function () {
            $('#add_state')[0].reset();
            $('.modal-title').text('Add new state');
            $('#save').text('Save');
            $("#add_state").attr("action", 'b_level/state_controller/save_state');
            $('#stateModal').modal('show');
        });
        $('#results_state').on('click', '.edit_state', function () { 
            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/state_controller/get_state/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_state')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="state_id"]').val(data.state_id);
                    $('[name="state_name"]').val(data.state_name);
                    $('[name="city"]').val(data.city);

                    $("#add_state").attr("action", 'b_level/state_controller/update_state')

                    $('#stateModal').modal('show');
                    $('.modal-title').text('Update State');
                    $('#save').text('Update State');


                }, error: function () {

                }
            });

        });
        var orderable = [0,4];
        init_datatable('stateListTable','<?php echo base_url('wholesaler-getStateLists/'); ?>',orderable);
    });
</script>





