<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .pattern_select .select2-container {
        width: 100% !important;
    }
</style>
<div id="stateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New State</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array('id' => 'add_state', 'name' => 'add_state', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>
                <div class="col-lg-12 px-4">
                    <div class="form-group">
                        <label for="state_name" class="mb-2">State ID</label>
                        <input class="form-control" type="text" name="state_id" id="state_id" required>
                         <span id="error"></span>
                    </div>
                    <div class="form-group">
                        <label for="state_name" class="mb-2">State Name</label>
                        <input class="form-control" type="text" name="state_name" id="state_name" required>
                         <span id="error"></span>
                    </div>
                    <div class="form-group">
                        <label for="state_number" class="mb-2">City Name</label>
                        <input class="form-control" type="text" name="city" id="city" required>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="">
                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Add state</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>