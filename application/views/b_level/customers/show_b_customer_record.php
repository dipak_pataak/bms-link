
<div class="row">
    <div class="col-lg-5 border-right">
        <h4 class="mb-3 m-0 p-0 border-0">Client Info:</h4>
        <div class="mb-3">
            <p><b>Client Name</b> : <?php echo $show_b_customer_record[0]['full_name']; ?></p>
            <p><b>Address</b> : <?php echo $show_b_customer_record[0]['address']; ?></p>
            <p><b>Email</b>: <?php echo $show_b_customer_record[0]['email']; ?></p>
            <p><b>Mobile</b> : <?php echo $show_b_customer_record[0]['phone']; ?></p>
        </div>
        <div class="d-flex flex-row mb-3 py-3 border-bottom border-top">
            <!-- <a href="#">
                <img alt="Profile Picture" src="<?php echo base_url(); ?>assets/b_level/resources/images/profile11.jpg" class="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall">
            </a> -->

                <div class="pl-3 pr-2">
                    <?php foreach($remarks as $val){?>
                            <a href="#">
                                <p class="font-weight-medium mb-0"><?=$val->remarks;?></p>
                                <p class="text-muted mb-1 text-small"> <?=$val->creater;?> |
                                    <?=$val->appointment_date;?> - <?=$val->appointment_time;?></p>
                            </a>
                    <?php } ?>
                </div>

        </div>
    </div>
    
    <div class="col-lg-7">
        <form action="<?php echo base_url('b_level/Customer_controller/appointment_setup/'); ?>" method="post">
            <div class="form-row">
                <div class="form-group col-md-12 text-left">
                    <label for="b_level_staff_id" class="col-form-label">Responsible Staff</label>
                    <select class="form-control select2" id="b_level_staff_id" name="b_level_staff_id" data-placeholder="-- select one --">
                        <option value="">-- select one --</option>
                         <?php
                        foreach ($get_users as $user) {
                            echo "<option value='$user->id'>$user->full_name</option>";
                        }
                        ?>
                    </select>
                </div>


                <div class="form-group col-md-12 text-left">
                    <label for="appointment_date" class="col-form-label">Set Date</label>
                    <input class="form-control datepicker" placeholder="Date" id="appointment_date" name="appointment_date">
                </div>
                <div class="form-group col-md-12 text-left">
                    <label for="datepicker-left-header" class="col-form-label">Set Time</label>
                    <input type="text" class="form-control" data-header-left="true" id="datepicker-left-header" name="appointment_time" required>
                </div>
                <div class="form-group col-md-12 text-left">
                    <label for="remarks" class="col-form-label">Remarks</label>
                    <input type="text" class="form-control"  id="remarks" name="remarks">
                </div>
                <input type="hidden" name="status" id="status">

                <div class="form-group col-md-12 text-right mb-0">
                    <button type="button" class="btn btn-danger btn-sm default" data-dismiss="modal">Close</button>
                    <input type="hidden" name="customer_id" value="<?php echo $show_b_customer_record[0]['customer_id']; ?>">
                    <button type="submit" class="btn btn-success btn-sm default">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>