<?php $package=$this->session->userdata('packageid'); ?>
<style type="text/css">
    .right_side{float: left; font-weight: bold; }
    .left_side{float: right; font-weight: bold; }
	.or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
    a:hover{
        text-decoration: none;
    }
    .address{
        cursor: pointer;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 18.7%;
			max-width: 18.7%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer List</h5>
            <?php
            $page_url = $this->uri->segment(1);
            $get_favorite = get_b_favorite_detail($page_url);
            $class = "notfavorite_icon";
            $fav_title = 'Customer List';
            $onclick = 'add_favorite()';
            if (!empty($get_favorite)) {
                $class = "favorites_icon";
                $onclick = 'remove_favorite(' . $get_favorite['id'] . ')';
            }
            $menu_permission= b_access_role_permission(2);
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <p class="px-4">
            <button class="btn btn-primary default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
            <?php  if(!empty($package)){ ?>
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="<?php echo base_url(); ?>add-wholesaler-customer" class="btn btn-success">Add Customer</a>
                <a href="javascript:void(0)" class="btn btn-success"  data-toggle="modal" data-target="#custBulkFrm">Bulk Upload</a>
                <?php  }?>
            <?php }  ?>


        </p>

        <!-- Modal -->
        <div class="modal fade" id="custBulkFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!--<h4 class="modal-title">Modal Header</h4>-->
                    </div>
                    <div class="modal-body">
                        <a href="<?php echo base_url('assets/b_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>
                        <?php echo form_open_multipart('customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="collapse px-4 mt-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <fieldset>
                    <form class="form-horizontal" action="<?php echo base_url('wholesaler-customer-filter'); ?>" method="post" id="customerFilterFrm">
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control company_name" placeholder="Company Name" name="company_name" id="company_name">
                            </div>
                            <div class="col-sm-1 or_cls p-0 text-center">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control phone" placeholder="Phone" name="phone" id="phone">
                            </div>
                            <div class="col-sm-1 or_cls p-0 text-center">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control email" placeholder="Email" name="email" id="email">
                            </div>
                            <div class="col-sm-1 or_cls p-0 text-center">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control address" placeholder="Address" name="address" id="address">
                            </div>
                            <div class="col-sm-3 col-md-2">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title m-0 p-0 border-0" id="exampleModalPopoversLabel">Appointment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body" id="customer_info">

                    </div>
                </div>
            </div>
        </div> 


        <div class="col-sm-12">
            <div class="form-group row m-0 mb-3 search_div float-right">
                <label for="keyword" class="mb-2"></label>
                <input type="text" class="form-control mb-2" name="keyword" id="keyword" onkeyup="customerkeyup_search()" placeholder="Search..." tabindex="">
                 <?php  if(!empty($package)){ ?>
                <button class="btn btn-info dropdown-toggle mb-2" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action<span class="caret"></span></button>
                <?php }?>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php echo base_url(); ?>customer-export-csv')" class="dropdown-item">Export to CSV</a></li>
                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php echo base_url(); ?>customer-export-pdf')"  class="dropdown-item">Export to PDF</a></li>
                        <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                    </ul>
            </div>          
        </div>
        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Customer_controller/manage_action') ?>">
                <input type="hidden" name="action">
				<div class="table-responsive">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th width="5%">#</th>
                            <th width="13%">Company Name</th>
                            <th width="15%">Name</th>
                            <th width="15%">Phone</th>
                            <th width="15%">Email</th>
                            <th width="22%">Address</th>
                            <!--<th width="10%">Status</th>-->
                        
                                 <?php  if(!empty($package)){ ?>

                            <th width="15%" class="text-center">Action</th>
                            <?php }?>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $sl = 0 + $pagenum;
                        foreach ($customer_list as $customer) {
                            $sl++;
                            $pre = $this->db->select('remarks')->where('customer_id', $customer->customer_id)->order_by('appointment_id', 'DESC')->get('appointment_calendar')->row();
    //                        ============= its for comments start ==========
                            $customer_user_id = $customer->customer_user_id;
                            $user_id = $this->session->userdata('user_id');
                            $sql = "SELECT comment_from, comment_to, comments, status, created_at FROM customer_commet_tbl 
                            WHERE (comment_from = '$customer_user_id' AND comment_to = '$user_id') 
                            or (comment_from = '$user_id' AND comment_to = '$customer_user_id')  ORDER BY id DESC limit 2";
                            $customer_commet_results = $this->db->query($sql)->result();
    //                        ============= its for comments close==========
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $customer->customer_id; ?>" class="checkbox_list">  
                                </td>
                                <td><?php echo $sl; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>wholesaler-customer-view/<?php echo $customer->customer_id; ?>">
                                        <?php echo $customer->company; ?>
                                    </a>
                                </td>
                                <td><?php echo $customer->full_name; ?></td>
                                <!--<td><?php echo $customer->side_mark; ?></td>-->
                                <td>
                                    <a href="tel:<?php echo $customer->phone; ?>"><?php echo $customer->phone; ?></a>
                                </td>
                                <td>
                                    <a href="mailto:<?php echo $customer->email; ?>"><?php echo $customer->email; ?></a>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                        <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                                    </a>
                                </td>

                                 <?php  
                                 $package=$this->session->userdata('packageid');
                                 ?>
                                 <?php  if(!empty($package)){ ?>

                                <td class="text-right">
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?php echo base_url(); ?>wholesaler-customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye"></i></a>
                                    <?php } ?>
                                    <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?php echo base_url(); ?>wholesaler-customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                    <?php } ?>
                                    <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?php echo base_url('b_level/customer_controller/delete_customer/'); ?><?php echo $customer->customer_id; ?>/<?= @$customer->customer_user_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <?php if (empty($customer_list)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="8" class="text-center  text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
				</div>
			</form>
            <?php echo $links; ?>
        </div>


        <div class="modal fade" id="customer_exp" role="dialog">
            
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Export Customer</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="customer_address_info">
                        <form id="expUrl" action="#" method="post">

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Start from<sup class="text-danger">*</sup></label>
                                <div class="col-xs-6">
                                    <input type="number" min="1" name="ofset" id="ofset" class="form-control" required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Limit<sup class="text-danger">*</sup></label>
                                <div class="col-xs-6">
                                    <input type="number" min="1" name="limit" id="limit" class="form-control" required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label"></label>
                                <div class="col-xs-6">
                                    <button class="btn-sm btn-success" id="closeModal"> Export</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
</div>


<!-- end content / right -->
<script>

    function ExportMethod(url){
        $("#expUrl").attr("action", url);
        $("#customer_exp").modal('show');
    }

    $("body").on('click', '#closeModal', function () {

        $("#customer_exp").modal('hide');
    });


    $("body").on('click', '#customerFilterBtn', function () {
        if ($("#company_name").val() == '' && $("#phone").val() == '' && $("#email").val() == '' && $("#address").val() == '') {
//                $("#company_name").css({'border': '1px solid red'}).focus();
            swal("Please company name or phone or email or address must be required!");
            return false;
        }
    });

//    ========== its for modal show ============
    function show_b_customer_record(t) {

//    $("#appointschedule").modal('show');
        $.post("<?php echo base_url(); ?>show-wholesaler-customer-record/" + t, function (t) {

            $("#customer_info").html(t);
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });

            $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
            $('#datepicker-left-header').datetimepicker({
                'format': "HH:mm", // HH:mm:ss
            });

            var st = $('#sta').val();
            $('#status').val(st);
            $("form :input").attr("autocomplete", "off");
            $("#appointschedule").modal('show');

        });
    }

    $(".inner_table").on("change", function () {
        $modal = $('#appointschedule');
        if ($(this).val() === 'schedule') {
            $modal.modal('show');
        }
    });

//============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)
//        swal(location);
        $.post("<?php echo base_url(); ?>show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }

//    ========== its for customer search ======
    function customerkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>wholesaler-customer-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
//                console.log(r);
                $("#result_search").html(r);
            }
        });
    }


</script>
