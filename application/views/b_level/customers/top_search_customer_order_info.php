<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer List</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="datatable2 table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="13%">Company Name</th>
                        <th width="15%">Name</th>
                        <th width="15%">Phone</th>
                        <th width="15%">Email</th>
                        <th width="22%">Address</th>
                        <!--<th width="10%">Status</th>-->
                        <th width="15%" class="text-center">Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $sl = 0;
                    foreach ($get_top_search_customer_info as $customer) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td>
                                <a href="<?php echo base_url(); ?>wholesaler-customer-view/<?php echo $customer->customer_id; ?>">
                                    <?php echo $customer->company; ?>
                                </a>
                            </td>
                            <td><?php echo $customer->first_name . " " . @$customer->last_name; ?></td>
                            <td>
                                <a href="tel:<?php echo $customer->phone; ?>"><?php echo $customer->phone; ?></a>
                            </td>
                            <td>
                                <a href="mailto:<?php echo $customer->email; ?>"><?php echo $customer->email; ?></a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                    <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                                </a>
                            </td>
                            <td class="text-right">
                                <a href="<?php echo base_url(); ?>wholesaler-customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
                                <a href="<?php echo base_url(); ?>wholesaler-customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url('b_level/customer_controller/delete_customer/'); ?><?php echo $customer->customer_id; ?>/<?= @$customer->customer_user_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($get_top_search_customer_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center  text-danger">No record found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Order List</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>Order/Quote No</th>
                        <th>Client Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($get_top_search_order_info)) {
                        foreach ($get_top_search_order_info as $key => $value) {
                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                            ?>
                            <tr>
                                <td><?= $value->order_id; ?></td>
                                <td><?= $value->customer_name; ?></td>
                                <td><?= $value->side_mark; ?></td>
                                <td><?= (@$value->order_date); ?></td>
                                <td><?= $company_profile[0]->currency ?><?= $value->grand_total ?></td>

                                <td>
                                    <select class="form-control" disabled>
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                    </select>
                                </td>

                                <td class="width_140">
                                    <!-- <a href="<?= base_url('order-view/') . $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a> -->
                                    <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-eye"></i> </a>
                                    <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                    <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>

                    <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($get_top_search_order_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">
                                Record not found!
                            </th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>My Order List</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order/Quote No</th>
                        <th>B User Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    if (!empty($get_top_search_my_order_info)) {
                        foreach ($get_top_search_my_order_info as $key => $value) {
                            $sl++; ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?= $value->order_id; ?></td>
                                <td><?= $value->b_user_name; ?></td>
                                <td><?= $value->side_mark; ?></td>
                                <td><?= (@$value->order_date); ?></td>
                                <td>
                                    <select class="form-control" disabled>
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                    </select>
                                </td>

                                <td class="width_140">
                                    <?php if ($this->permission->check_label('order')->read()->access()) { ?>
                                        <?php if ($value->order_stage == 7) { ?>
                                            <!--<a href="customer-order-return/<?/*= $value->order_id */ ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>-->
                                        <?php }
                                                    if ($value->level_id != 1) { ?>
                                            <a href="<?= base_url('wholesaler-catalog-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url('wholesaler-catalog-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                        <?php } ?>

                                    <?php } ?>

                                    <?php if ($this->permission->check_label('order')->delete()->access()) { ?>
                                        <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                        <a href="<?php echo base_url('b_level/Shared_catalog_controller/delete_order'); ?>/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>

                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($get_top_search_my_order_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">
                                Record not found!
                            </th>
                        </tr>
                    </tfoot>
                <?php   } ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Receive Order List</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order/Quote No</th>
                        <th>B User Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    if (!empty($get_top_search_receive_order_info)) {
                        foreach ($get_top_search_receive_order_info as $key => $value) {
                            $sl++; ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?= $value->order_id; ?></td>
                                <td><?= $value->b_user_name; ?></td>
                                <td><?= $value->side_mark; ?></td>
                                <td><?= (@$value->order_date); ?></td>
                                <td>
                                    <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                    </select>
                                </td>

                                <td class="width_140">
                                    <?php if ($this->permission->check_label('order')->read()->access()) { ?>
                                        <?php if ($value->order_stage == 7) { ?>
                                            <!--<a href="customer-order-return/<?/*= $value->order_id */ ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>-->
                                        <?php }
                                                    if ($value->level_id != 1) { ?>
                                            <a href="<?= base_url('catalog-user-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                        <?php } else { ?>
                                            <a href="<?= base_url('catalog-user-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                        <?php } ?>

                                    <?php } ?>

                                    <?php if ($this->permission->check_label('order')->delete()->access()) { ?>
                                        <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                        <a href="<?php echo base_url('b_level/Shared_catalog_controller/b_user_delete_order'); ?>/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>

                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($get_top_search_receive_order_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">
                                Record not found!
                            </th>
                        </tr>
                    </tfoot>
                <?php   } ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer Return List</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Invoice No</th>
                        <th>Side Mark</th>
                        <th>Company</th>
                        <th>Comments</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    if (!empty($get_top_search_customer_return_info)) {
                        foreach ($get_top_search_customer_return_info as $key => $value) {
                            $sl++;
                            $com = $this->db->where('user_id', $value->c_u_id)->get('company_profile')->row();
                            $blevel_order_id = $this->db->select('order_id')->where('clevel_order_id', $value->order_id)->get('b_level_quatation_tbl')->row();
                            if (!empty($blevel_order_id->order_id)) {
                                ?>
                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $blevel_order_id->order_id; ?></td>
                                    <td><?php echo $value->side_mark; ?></td>
                                    <td><?php echo @$com->company_name; ?></td>
                                    <td><?php echo $value->return_comments; ?></td>
                                    <td><?php echo date('M-d-Y', strtotime($value->return_date)); ?></td>
                                    <td>
                                        <?php if ($value->is_approved == 1) {
                                                        echo "Resend";
                                                    } elseif ($value->is_approved == 0) {
                                                        echo "Pending";
                                                    } elseif ($value->is_approved == 2) {
                                                        echo "Cancel";
                                                    } ?>
                                    </td>
                                    <td class="width_140">
                                        <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" onclick="show_return_resend('<?php echo $value->return_id; ?>', '<?php echo $value->order_id; ?>','<?php echo $value->returned_by; ?>');" data-toggle="tooltip" data-placement="right" title="Resend"> <i class="fa fa-share-square" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                    <?php
                            }
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($get_top_search_customer_return_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="8" class="text-center text-danger">
                                Record not found!
                            </th>
                        </tr>
                    </tfoot>
                <?php   } ?>
            </table>
        </div>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Suppliers Return</h5>
        </div>
        <div class="table-responsive px-3 mb-3">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Purchase Invoice</th>
                        <th>Supplier Name</th>
                        <th>Material</th>
                        <th>Qty</th>
                        <th>Comment</th>
                        <th>Returned By</th>
                        <th>Approved By</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0;
                    if (!empty($get_top_search_supplier_return_order_info)) {
                        foreach ($get_top_search_supplier_return_order_info as $key => $value) {
                            $sl++;
                            $this->db->select('a.*, a.raw_material_id, b.material_name');
                            $this->db->from('raw_material_return_details_tbl a');
                            $this->db->join('row_material_tbl b', 'b.id = a.raw_material_id');
                            $this->db->where('a.return_id', $value->return_id);
                            $material_info_query = $this->db->get()->result();
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->purchase_id; ?></td>
                                <td><?php echo $value->supplier_name; ?></td>
                                <td><?php
                                            $sl = 0;
                                            foreach ($material_info_query as $material_single) {
                                                $sl++;
                                                echo "<p>";
                                                echo $sl . ". " . $material_single->material_name;
                                                echo "</p>";
                                            }
                                            ?></td>
                                <td><?php
                                            $sl = 0;
                                            foreach ($material_info_query as $material_single) {
                                                echo "<p>";
                                                echo $material_single->return_qty;
                                                echo "</p>";
                                            }
                                            ?></td>
                                <td><?php echo $value->return_comments; ?></td>
                                <td><?php echo $value->name; ?></td>
                                <td><?php if ($value->is_approved == 1) {
                                                echo $value->approve_name;
                                            } ?>
                                </td>
                                <td><?php if ($value->is_approved == 1) {
                                                echo "Approved";
                                            } elseif ($value->is_approved == 0) {
                                                echo "Pending";
                                            }
                                            ?>
                                </td>
                                <td class="text-right">
                                    <form action="" method="post" id="return_stock_frm">
                                        <?php foreach ($material_info_query as $single_return_details) { ?>
                                            <input type="hidden" name="raw_material_id[]" value="<?php echo $single_return_details->raw_material_id; ?>" class="form-control">
                                            <input type="hidden" name="pattern_model_id[]" value="<?php echo $single_return_details->pattern_model_id; ?>" class="form-control">
                                            <input type="hidden" name="color_id[]" value="<?php echo $single_return_details->color_id; ?>" class="form-control">
                                            <input type="hidden" name="return_qty[]" value="<?php echo $single_return_details->return_qty; ?>" class="form-control">
                                        <?php } ?>
                                        <input type="hidden" name="return_id" value="<?php echo $value->return_id; ?>">
                                        <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" data-toggle="tooltip" data-placement="right" title="Approve " <?php if ($value->is_approved == 1) {
                                                                                                                                                                                                echo "disabled";
                                                                                                                                                                                            } ?>>
                                            <i class="fa fa-check-square" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($get_top_search_supplier_return_order_info)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="10" class="text-center text-danger">
                                Record not found!
                            </th>
                        </tr>
                    </tfoot>
                <?php   } ?>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="customer_address_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Address Map Show</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="customer_address_info">

            </div>
            <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
        </div>
    </div>
</div>

<script type="text/javascript">
    //============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)
        //        swal(location);
        $.post("<?php echo base_url(); ?>show-address-map/" + id, function(t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
</script>