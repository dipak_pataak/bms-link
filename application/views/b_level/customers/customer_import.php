
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Customer Import</h5>
        </div>
        <div class="" style="margin: 10px;">
            <a href="<?php echo base_url('assets/b_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
            <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <?php echo form_open_multipart('customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
            <div class="form-group text-center">
                <div class="col-sm-12">
                    <input type="file" class="attatch_file" name="upload_csv_file" id="" required> 
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-success w-md m-b-5">Submit</button>
            </div>
            <?php echo form_close() ?>
        </div>

    </div>
</div>
<!-- end content / right -->