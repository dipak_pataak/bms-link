<style>
.select2.select2-container.select2-container--default {
	margin-bottom: 1rem !important;
}
@media only screen and (max-width: 994px) {
.field_wrapper .row{
	margin-left: -7px;
	margin-right: -7px;
}
.field_wrapper .row .col-sm-2, .field_wrapper .row .col-sm-3, .field_wrapper .row .col-sm-1 {
	padding-left: 7px;
	padding-right: 7px;
}
}
</style>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-10">
                <h5>Product Attribute</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Product Attribute';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end box / title -->     
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
             <?php $menu_permission= b_access_role_permission(18); ?>
        </div>

        <form action="<?php echo base_url('b_level/Attribute_controller/save_attribute'); ?>" method="post" class="form-row px-3 frm_save_attributes" enctype="multipart/form-data">
            
            <div class="col-12">
				<div class="row m-0">
				<div class="col-sm-8 col-md-6 col-lg-5">
                <div class="form-group row mb-0">
                    <div class="col-sm-5"><label for="category" class="col-form-label mb-2">Attribute Category</label></div>
                    <div class="col-sm-7">
					<select name="category" class="form-control select2 mb-3"  style="margin-bottom: 1rem !important;" id="category" data-placeholder='-- select one --' required>
                        <option value=" ">None</option>
                        <?php foreach($categorys as $category){?>
                        <option value="<?=$category->category_id?>"><?=$category->category_name?></option>
                        <?php }?>
                    </select>
					</div>
                </div>

               <div class="form-group row mb-0">
                    <div class="col-sm-5"><label class="mb-2">Attribute Name</label></div>
					<div class="col-sm-7">
                    <input class="form-control mb-3" type="text" placeholder="Attribute name" name="attribute" required>
					</div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-sm-5"><label class="mb-2">Attribute Position</label></div>
					<div class="col-sm-7">
                    <input class="form-control mb-3" type="number" placeholder="Position" name="position">
					</div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-sm-5"><label for="attribute_type" class="mb-2">Attribute Type</label></div>
					<div class="col-sm-7">
                    <select name="attribute_type" class="form-control select2 mb-3" id="attribute_type" data-placeholder='-- select one --'>
                        <option value=""></option>
                        <option value="1">Text</option>
                        <option value="2">Option</option>
                        <option value="3">Multi text</option>
                        <option value="4">Multi option</option>
                    </select>
					</div>
                </div>
				</div>

                <div class="att_opt hidden col-12">
                    
                    <a href="javascript:void(0);" class="btn btn-primary add_button mb-3"><span class="glyphicon glyphicon-plus"></span>Add Options</a>
                    <div class="field_wrapper">
                    </div>
                </div>

                <div class="table-responsive hidden" style="margin-top: 10px">
                    <input id="add-invoice-item" class="btn btn-info btn-xs fa fa-plus" name="add-new-item" onclick="addInputField('addRowtem');" value="Add New" type="button">
                </div>

				<div class="col-12">
					<div class="form-group">
                        <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
						<button type="submit" class="btn btn-success w-md m-b-5 add_attribute_btn">Add Attribute</button>
                         <?php } ?>
					</div>
				</div>
				</div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('b_level/attributes/att_add_js') ;?>

<script>
    $('.add_attribute_btn').on("click",function(e){ 
        e.preventDefault();
        rename_option_name();
    });
</script>    

