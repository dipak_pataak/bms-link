
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Product Attribute</h5>
        </div>
        <!-- end box / title -->
        <form class="form-row px-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="product_code" class="mb-2">Name of Attribute</label>
                    <input class="form-control" type="text" placeholder="Name of Attribute"  required>
                </div>
                <div class="form-group">
                    <label for="unit_name" class="mb-2">Type of Attribute</label>
                    <select name="unit_name" class="form-control" id="theSelect">
                        <option value="text" selected="selected">Select Option</option>
                        <option value="checkbox">Checkbox</option>
                        <option value="radio">Radio</option>
                        <option value="dropdown">Dropdown</option>
                        <option value="yesno">Yes/No</option>
                    </select>
                </div>
                <div class="hidden ischeckbox mb-3"> <a href="javascript:" onClick="add_row('chk_tbl', 'chk_tbl_count');" class="float-right">Add New</a>
                    <input type="hidden" id="chk_tbl_count" value="1">
                    <div id="chk_tbl">
                        <table width="80%" border="0">
                            <tr>
                                <th width="34%">Option Name</th>
                                <th width="33%">Value</th>
                                <th width="33%">Price</th>
                            </tr>
                        </table>
                        <table width="80%" border="0">                                    
                            <tr>
                                <td width="34%"><input type="text" class="form-control" placeholder="Option Name" value="" name="opt_1"></td>
                                <td width="33%"><input type="text" class="form-control" placeholder="Option Value" value="" name="val_1"></td>
                                <td width="33%"><input type="number" class="form-control" placeholder="$0.00" value="" name="price_1"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="hidden isradio mb-3">
                    <a href="javascript:" onClick="add_row('radio_tbl', 'radio_tbl_count');" class="float-right">Add New Radio</a>
                    <input type="hidden" id="radio_tbl_count" value="1">
                    <div id="radio_tbl">
                        <table width="80%" border="0">
                            <tr>
                                <th width="34%">Option Name</th>
                                <th width="33%">Value</th>
                                <th width="33%">Price</th>
                            </tr>
                        </table>
                        <table width="80%" border="0">                                    
                            <tr>
                                <td width="34%"><input type="text" class="form-control" placeholder="Option Name" value="" name="opt_1"></td>
                                <td width="33%"><input type="text" class="form-control" placeholder="Option Value" value="" name="val_1"></td>
                                <td width="33%"><input type="number" class="form-control" placeholder="$0.00" value="" name="price_1"></td>
                            </tr>
                        </table>
                    </div>

                </div>
                <div class="hidden isdropdown mb-3">

                    <a href="javascript:" onClick="add_row('drop_tbl', 'drop_tbl_count');" class="float-right">Add New</a>
                    <input type="hidden" id="drop_tbl_count" value="1">
                    <div id="drop_tbl">
                        <table width="80%" border="0">
                            <tr>
                                <th width="34%">Option Name</th>
                                <th width="33%">Value</th>
                                <th width="33%">Price</th>
                            </tr>
                        </table>
                        <table width="80%" border="0">                                    
                            <tr>
                                <td width="34%"><input type="text" class="form-control" placeholder="Option Name" value="" name="opt_1"></td>
                                <td width="33%"><input type="text" class="form-control" placeholder="Option Value" value="" name="val_1"></td>
                                <td width="33%"><input type="number" class="form-control" placeholder="$0.00" value="" name="price_1"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="hidden isyesno mb-3">

                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="yes" name="inlineDefaultRadiosExample" checked>
                        <label class="custom-control-label" for="yes">Yes</label>
                    </div>

                    <!-- Default inline 2-->
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="no" name="inlineDefaultRadiosExample">
                        <label class="custom-control-label" for="no">No</label>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Add/Update Attributes</button>
                </div>
            </div>
            <div class="col-sm-6 gray-back">
                <div class="px-3 mb-3">
                    <h6 class="mx-0">List of products already assigned </h6>
                    <p>
                    <table width="80%" border="0" class="gray-back">
                        <tr>
                            <td id="Prod_01" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_01');"> <b>Honeycomb</b></td>
                            <td id="Prod_02" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_02');"> <b>Roller</b></td>
                        </tr>
                        <tr>
                            <td id="Prod_03" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_03');"> <b>Combi</b> </td>
                            <td id="Prod_04" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_04');"> <b>Wooden blind</b></td>
                        </tr>
                        <tr>
                            <td id="Prod_05" class="border-0 gray-back"><input type="checkbox" class="checkboxes" checked onChange="hideData('Prod_05');"> <b>Fox blind</b></td>

                        </tr>
                    </table>
                    </p>
                </div>
            </div>
        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

    $("#theSelect").change(function () {
        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);

        theDiv.slideDown().removeClass("hidden");
        theDiv.siblings('[class*=is]').slideUp(function () {
            $(this).addClass("hidden");
        });
    });
    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }
    function add_row(loc, tbl_count) {
        var ni = document.getElementById(loc);
        var numi = document.getElementById(tbl_count);
        var num = (document.getElementById(tbl_count).value - 1) + 2;
        numi.value = num;
        var newdiv = document.createElement('div');
        var divIdName = 'my' + num + 'Div';
        newdiv.setAttribute('id', divIdName);

        newdiv.innerHTML = '<table width=80% border=0 cellspacing=3 cellpadding=5><tr><td width="34%"><input type="text" class="form-control" placeholder="Option Name" value="" name="opt_' + num + '"></td><td width="33%"><input type="text" class="form-control" placeholder="Option Value" value="" name="val_' + num + '"></td><td width="33%"><input type="number" class="form-control" placeholder="$0.00" value="" name="price_' + num + '"></td></tr></table>';

        ni.appendChild(newdiv);
    }
</script>