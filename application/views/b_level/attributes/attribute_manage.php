<?php $packageid=$this->session->userdata('packageid'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<style type="text/css">
    .or-filter .col-sm-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
    #att-tbl_wrapper .fa.fa-remove{
        cursor: pointer;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 25%;
			max-width: 25%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Product Attribute</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Product Attribute';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <?php $menu_permission= b_access_role_permission(19); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>


        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="filter-collpase-btn">Filter</button>
            <?php if(!empty($packageid)){ ?>
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="<?php echo base_url(); ?>add-attribute" class="btn btn-success">Add Attribute</a>
                <?php } ?>
                 <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
             <?php } ?>
            <?php } ?>
        </p>

        <div class="collapse px-3 mb-3" id="collapseExample">
			<div class="border p-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>attribute-filter" id="AttributeFilterFrm" method="post">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control attribute_name" name="attribute_name" placeholder="Attribute Name" value="<?=$this->session->userdata('search_attribute_name')?>">
                            </div>
                            <div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <select name="category_id" class="form-control select2 category_id" data-placeholder="-- select category --" id="category_id">
                                    <option value=""></option>
                                    <?php foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>".$category->category_name."</option>";
                                    }?>
                                </select>
                                <script>
                                    $("#category_id").val(<?=$this->session->userdata('search_category_id')?>);
                                </script>    
                            </div>
                            <div class="col-sm-3 text-left">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </form>
            </div>
        </div>
        <div class="px-3" id="results_attribute">

            <div class="p-1">
                <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?>
            </div>
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Attribute_controller/manage_action') ?>">
                <input type="hidden" name="action">
    			<div class="table-responsive">
                    <table id="att-tbl" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="SellectAll"/></th>
                                <th>Position No.</th>
                                <th>Category Name</th>
                                <th>Attribute Name</th>
                                <th>Option</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
    			</div>	
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->

<?php $this->load->view($js) ?>
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // For Open filter Box : START
        var attribute_name = $("#AttributeFilterFrm input[name=attribute_name]").val();
        var category_id = $("#AttributeFilterFrm select[name=category_id]").val();

        if(attribute_name != '' || category_id != '') {
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
        var orderable = [0,4,5];
        init_datatable('att-tbl','<?php echo base_url('b_level/Attribute_controller/getAttributeLists/'); ?>',orderable);
    });
</script>
