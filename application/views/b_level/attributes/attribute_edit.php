<style>
.select2.select2-container.select2-container--default {
	margin-bottom: 1rem !important;
}
@media only screen and (max-width: 994px) {
.field_wrapper .row{
	margin-left: -7px;
	margin-right: -7px;
}
.field_wrapper .row .col-sm-2, .field_wrapper .row .col-sm-3, .field_wrapper .row .col-sm-1 {
	padding-left: 7px;
	padding-right: 7px;
}
|
</style>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Edit Product Attribute</h5>
        </div>


        <div class="p-1">
            <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
            ?>
        </div>

        <!-- end box / title -->
        <form action="<?php echo base_url('b_level/Attribute_controller/attribute_update' ); ?>" method="post" class="form-row px-3 frm_update_attributes" enctype="multipart/form-data">
            
            <div class="col-12">
				<div class="row m-0">
    				<div class="col-sm-8 col-md-6 col-lg-5">
                        <div class="form-group row mb-0">
                           <div class="col-sm-5"> <label for="category" class="col-form-label mb-2">Attribute Category</label></div>
                            <div class="col-sm-7">
                            <select name="category" class="form-control select2 mb-3"  style="margin-bottom: 1rem !important;"  id="category" data-placeholder='-- select one --'>
                                <option value=" ">None</option> 
                                <?php foreach($categorys as $category){?>
                                <option value="<?=$category->category_id?>" <?=($category->category_id==$attribute->category_id?'selected':'');?>><?=$category->category_name?></option>
                                <?php }?>
                            </select>
                        </div>
                        </div>
                        <div class="form-group row mb-0">
                           <div class="col-sm-5"> <label class="col-form-label mb-2">Attribute Name</label></div>
                		   <div class="col-sm-7">
                                    <input class="form-control mb-3" type="text" value='<?=$attribute->attribute_name;?>' name="attribute" required>
                		    </div>
                        </div>

                        <div class="form-group row mb-0">
                           <div class="col-sm-5"> <label class="col-form-label mb-2">Attribute Position</label></div>
                           <div class="col-sm-7"> <input class="form-control mb-3" type="number" value="<?=$attribute->position;?>" name="position"></div>
                        </div>

                        <div class="form-group row mb-0">
                           <div class="col-sm-5"> <label for="attribute_type" class="col-form-label mb-2">Attribute Type</label></div>
                           <div class="col-sm-7">  
                                <select name="attribute_type" class="form-control select2 mb-3" id="attribute_type" data-placeholder='-- select one --'>
                                    <option value=""></option>
                                    <option value="1" <?=($attribute->attribute_type==1?'selected':'');?>>Text</option>
                                    <option value="2" <?=($attribute->attribute_type==2?'selected':'');?>>Option</option>
                                    <option value="3" <?=($attribute->attribute_type==3?'selected':'');?>>Multi text</option>
                                    <option value="4" <?=($attribute->attribute_type==4?'selected':'');?>>Multi option</option>
                                </select>
        		            </div>
                        </div>
        			</div>
                    <input type="hidden" name="attribute_id" class="form-control" value="<?=$attribute->attribute_id?>">


            <?php 
                if($attribute->attribute_type==2 || $attribute->attribute_type==3 || $attribute->attribute_type== 4){
            ?>
                <div class="att_opt" style="margin-top: 10px">
                    <!-- <a style="margin-bottom: 10px" href="javascript:void(0);" class="btn btn-primary add_button "><span class="glyphicon glyphicon-plus"></span>Add Options</a> -->
                    <div class="field_wrapper">

            <?php 
                $i=0;
                foreach ($option as $ko => $op) {
                    
                    $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                    $option_option_result = $this->db->query($option_option)->result();
            ?>
                        <div class='row main_option' id='item_<?=$i?>'>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Attribute Option</label>
                                <input class='form-control' type='text' value='<?=$op->option_name?>' name='option_name[]' required>
                            </div>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Option Type</label>
                                <select class='form-control option_type' name='option_type[]' onchange='addOption("<?=$i?>")'>
                                    <option value='0'>--Select--</option>
                                    <option value='1' <?=($op->option_type==1?'selected':'')?>>Text</option>
                                    <option value='2' <?=($op->option_type==2?'selected':'')?>>Option</option>
                                    <option value='3' <?=($op->option_type==3?'selected':'')?>>Multi text</option>
                                    <option value='4' <?=($op->option_type==4?'selected':'')?>>Multi option</option>
                                    <option value='5' <?=($op->option_type==5?'selected':'')?>>Text+Fraction</option>
                                    <option value='6' <?=($op->option_type==6?'selected':'')?>>Multi Selected</option>
                                </select>
                            </div>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Price Type</label>
                                <select class='form-control'  name='price_type[]'  >
                                    <option value='1' <?=($op->price_type==1?'selected':'')?>>$</option>
                                    <option value='2' <?=($op->price_type==2?'selected':'')?>>%</option>
                                </select>
                            </div>
                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Price </label>
                                <input class='form-control' type='text' value='<?=$op->price;?>' name='price[]' >
                            </div>
                            <div class='form-group col-sm-3'>
                                <label for='attr_option' class='mb-2'> Option condition </label>
                                <input class='form-control' type='text' value='<?=$op->op_condition;?>' name='condition[]' >
                            </div> 
                            <input type="hidden" name="option_id[]" value="<?=$op->att_op_id?>" class="form-control">
                            <div class='form-group col-sm-1'>
                                <label for='attr_option' class='mb-2'> </label>
                                <button  class='btn btn-danger delete_old_option' style='margin-top:20px;' data-record_id='<?=$op->att_op_id?>'><i class='fa fa-trash'></i></button>
                            </div>

                            <!-- File upload : START -->
                            <div class='form-group col-sm-4 img_div'>
                                <label for='product_img' class='mb-2'>File Upload</label>
                                <input type='file' class='form-control attr_img' name='attr_img[]'>
                                <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                <input type="hidden" class="form-control" name="hid_attr_img[]" value="<?= @$op->attributes_images; ?>">
                            </div>
                            <div class='form-group col-sm-2 prev_img_div'>
                                <img src="<?php if($op->attributes_images != '') {   
                                    echo base_url().$op->attributes_images; 
                                } else { 
                                    echo base_url('assets/no-image.png');
                                }?>"  class='cls_prev_img' width="100px" height="100px">
                            </div>
                            <!-- File upload : END -->

                            <div class='col-md-12'>

                            <?php 
                            $k =0;
                            foreach ($option_option_result as $kk => $opop) {

                                $op_op_op = "SELECT * FROM attr_options_option_option_tbl WHERE att_op_op_id = $opop->op_op_id";
                                $op_op_op_result = $this->db->query($op_op_op)->result();

                            ?>
                                <div class='row sub_option' id="<?=$ko."_".$kk?>">
                                    
                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Attribute Sub Option</label>
                                        <input class='form-control' type='text' value='<?=$opop->op_op_name?>' name='option_option_name[<?=$i?>][]' required>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub option Type</label>
                                        <select class='form-control' name='option_option_type[<?=$i?>][]' >
                                            <option value='0'>--Select--</option>
                                            <option value='1' <?=($opop->type==1?'selected':'')?> > Text</option>
                                            <option value='2' <?=($opop->type==2?'selected':'')?> > Option</option>
                                            <option value='3' <?=($opop->type==3?'selected':'')?> > Multi text</option>
                                            <option value='4' <?=($opop->type==4?'selected':'')?> > Multi option</option>
                                            <option value='5' <?=($opop->type==5?'selected':'')?> > Checkbox</option>
                                            <option value='6' <?=($opop->type==6?'selected':'')?> > Multi Selected</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub Price Type</label>
                                        <select class='form-control' name='op_op_price_type[<?=$i?>][]'  >
                                            <option value='1' <?=($opop->att_op_op_price_type==1?'selected':'')?>>$</option>
                                            <option value='2' <?=($opop->att_op_op_price_type==2?'selected':'')?>>%</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub Option Price </label>
                                        <input class='form-control' type='text' value='<?=$opop->att_op_op_price;?>' name='op_op_price[<?=$i?>][]' >
                                    </div>

                                    <div class='form-group col-sm-3'>
                                        <label for='attr_option' class='mb-2'>Sub Option condition </label>
                                        <input class='form-control' type='text' value='<?=$opop->att_op_op_condition;?>' name='op_op_condition[<?=$i?>][]' >
                                    </div> 
                                    <input type="hidden" name="op_op_id[<?=$i?>][]" value="<?=$opop->op_op_id?>" class="form-control">
                                    <div class='form-group col-sm-1'>
                                        <label for='attr_option' class='mb-2'> </label>
                                        <button  class='btn btn-danger delete_old_option_option' style='margin-top:20px;' data-record_id='<?=$opop->op_op_id?>'><i class='fa fa-trash'></i></button>
                                    </div>

                                    <!-- File upload : START -->
                                    <div class='form-group col-sm-4 img_div'>
                                        <label for='product_img' class='mb-2'>File Upload</label>
                                        <input type='file' class='form-control attr_img' name='op_op_attr_img[<?=$i?>][]'>
                                        <input type="hidden" class="form-control" name="hid_op_op_attr_img[<?=$i?>][]" value="<?= @$opop->att_op_op_images; ?>">
                                        <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                    </div>
                                    <div class='form-group col-sm-2 prev_img_div'>
                                        <img src="<?php if($opop->att_op_op_images != '') {   
                                            echo base_url().$opop->att_op_op_images; 
                                            } else { 
                                            echo base_url('assets/no-image.png');
                                            }?>"  class='cls_prev_img' width="100px" height="100px">
                                    </div>
                                    <!-- File upload : END -->

                                    <div class='col-md-12'>
                                        <?php 
                                            $j =0;
                                            foreach ($op_op_op_result as $kkk => $opopop) {
                                        ?>

                                                <div class='row sub_sub_option' id="<?=$ko."_".$kk."_".$kkk?>">
                                                    
                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Attribute Sub-Sub Option</label>
                                                        <input class='form-control' type='text' value='<?=$opopop->att_op_op_op_name?>' name='op_op_op_name[<?=$i?>][<?=$k?>][]' required>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub option Type</label>
                                                        <select class='form-control' name='op_op_op_type[<?=$i?>][<?=$k?>][]' >
                                                            <option value='0'>--Select--</option>
                                                            <option value='1' <?=($opopop->att_op_op_op_type==1?'selected':'')?>> Text </option>
                                                            <option value='2' <?=($opopop->att_op_op_op_type==2?'selected':'')?> > Option </option>
                                                            <option value='3' <?=($opopop->att_op_op_op_type==3?'selected':'')?> > Checkbox </option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Price Type</label>
                                                        <select class='form-control'  name='op_op_op_price_type[<?=$i?>][<?=$k?>][]'  >
                                                            <option value='1' <?=($opopop->att_op_op_op_price_type==1?'selected':'')?>>$</option>
                                                            <option value='2' <?=($opopop->att_op_op_op_price_type==2?'selected':'')?>>%</option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Option Price </label>
                                                        <input class='form-control' type='text' value='<?=$opopop->att_op_op_op_price;?>' name='op_op_op_price[<?=$i?>][<?=$k?>][]' >
                                                    </div>

                                                    <div class='form-group col-sm-3'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub condition </label>
                                                        <input class='form-control' type='text' value='<?=$opopop->att_op_op_op_condition;?>' name='op_op_op_condition[<?=$i?>][<?=$k?>][]' >
                                                    </div> 
                                                    <input type="hidden" name="op_op_op_id[<?=$i?>][<?=$k?>][]" value="<?=$opopop->att_op_op_op_id?>" class="form-control">
                                                    <div class='form-group col-sm-1'>
                                                        <label for='attr_option' class='mb-2'> </label>
                                                        <button  class='btn btn-danger delete_old_sub_option_option' style='margin-top:20px;' data-record_id='<?=$opopop->att_op_op_op_id?>'><i class='fa fa-trash'></i></button>
                                                    </div>
                                                    <!-- File upload : START -->
                                                    <div class='form-group col-sm-4 img_div'>
                                                        <label for='product_img' class='mb-2'>File Upload</label>
                                                        <input type='file' class='form-control attr_img' name='op_op_op_attr_img[<?=$i?>][<?=$k?>][]'>
                                                        <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                                        <input type="hidden" class="form-control" name="hid_op_op_op_attr_img[<?=$i?>][<?=$k?>][]" value="<?= @$opopop->att_op_op_op_images; ?>">
                                                    </div>
                                                    <div class='form-group col-sm-2 prev_img_div'>
                                                        <img src="<?php if($opopop->att_op_op_op_images != '') {   
                                                            echo base_url().$opopop->att_op_op_op_images; 
                                                        } else { 
                                                            echo base_url('assets/no-image.png');
                                                        }?>"  class='cls_prev_img' width="100px" height="100px">
                                                    </div>
                                                    <!-- File upload : END -->
                                                </div>

                                        <?php $j++; } ?>

                                    </div>
                                    
                                </div>

                                <?php $k++; } ?>

                            </div>

                        </div>

                    <?php $i++; } ?>

                    </div>
                </div>
            <?php  } ?>

                <div class="table-responsive hidden" style="margin-top: 10px">
                    <input id="add-invoice-item" class="btn btn-info btn-xs fa fa-plus" name="add-new-item" onclick="addInputField('addRowtem');" value="Add New" type="button">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success w-md m-b-5 update_attr_btn">Update Attribute</button>
                </div>
            </div>
			</div>
        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

        $(document).ready(function(){

            var wrapper = $('.field_wrapper');

            // For Delete Stored database data : START
            $(wrapper).on('click', '.delete_old_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('b_level/Attribute_controller/remove_main_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });

            $(wrapper).on('click', '.delete_old_option_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('b_level/Attribute_controller/remove_sub_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });

            $(wrapper).on('click', '.delete_old_sub_option_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('b_level/Attribute_controller/remove_sub_sub_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });
            // For Delete Stored database data : END

        });

        $("body").on("change", ".img_div .attr_img", function(e) {
       
            if ($(this).val() != '') {
                var file = (this.files[0].name);
                var size = (this.files[0].size);
                var ext = file.substr((file.lastIndexOf('.') + 1));
                // check extention
                if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                    swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                        icon: "error",
                    });
                    $(this).val('');
                }
                // chec size
                if (size > 2000000) {
                    swal("Please upload file less than 2MB. Thanks!!", {
                        icon: "error",
                    });
                    $(this).val('');
                }
            }    

            filePreview(this);
        });

        function filePreview(input) {
            if ($(input).val() != '' && input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src',e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }else{
                $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src','<?=base_url('assets/no-image.png')?>');
            }
        }


        function rename_option_name(){
            $(".main_option").each(function(key_main_op,main_option_html) {
                // Rename main option name
                var main_option_id = $(main_option_html).attr('id');

                $("#"+ main_option_id + " .sub_option").each(function(key_sub_op,sub_option_html) {
                    var sub_option_id = $(sub_option_html).attr('id');
                    // Rename sub option name : START
                    $("#"+ sub_option_id +" .form-control").each(function(key_sub_op_k,sub_option_html_k) {
                        if(key_sub_op_k == 0){
                            var namee = 'option_option_name[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 1){
                            var namee = 'option_option_type[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 2){
                            var namee = 'op_op_price_type[' + key_main_op +']'+'['+key_sub_op+']';    
                        }else if(key_sub_op_k == 3){
                            var namee = 'op_op_price[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 4){
                            var namee = 'op_op_condition[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 5){
                            var namee = 'op_op_id[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 6){
                            var namee = 'op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 7){
                            var namee = 'hid_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']';
                        }else{
                            return false; 
                        }

                        $(this).attr('name',namee);
                    });
                    // Rename sub option name : END

                    $("#"+ main_option_id + " #"+ sub_option_id + " .sub_sub_option").each(function(key_sub_sub_op,sub_sub_option_html) {
                        var sub_sub_option_id = $(sub_sub_option_html).attr('id');
                        // Rename sub sub option name : START                   
                        $("#"+ sub_sub_option_id +" .form-control").each(function(key_sub_sub_op_k,sub_sub_option_html_k) {
                            if(key_sub_sub_op_k == 0){
                                var namee = 'op_op_op_name[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 1){
                                var namee = 'op_op_op_type[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 2){
                                var namee = 'op_op_op_price_type[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';    
                            }else if(key_sub_sub_op_k == 3){
                                var namee = 'op_op_op_price[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 4){
                                var namee = 'op_op_op_condition[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 5){
                                var namee = 'op_op_op_id[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 6){
                                var namee = 'op_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 7){
                                var namee = 'hid_op_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }
                            $(this).attr('name',namee);
                        });
                        // Rename sub sub option name : END
                        
                    });

                });
            });

            $(".frm_update_attributes").submit();
        }

        $('.update_attr_btn').on("click",function(e){ 
            e.preventDefault();
            rename_option_name();
        });



</script>