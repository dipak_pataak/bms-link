<style type="text/css">
    .modal-dialog {
        max-width: 850px;
    }
</style>

<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Add attribute 4th-Sub Option</h4>
            </div>

            <div class="modal-body">

                <div class="form-row">

                    <div class="col-md-12">
                        <?=form_open('#',array('id'=>'SaveOp','name'=>'formStylePrice','enctype'=>'multipart/form-data'))?>

                        <div class="field_wrapper">

                            <div class="row">

                                <div class=" form-group col-md-2">
                                    <label class="">4th-Sub Option Name</label>
                                    <input type="text" name="option_name[]" class="form-control" required> 
                                </div>

                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Type</label>
                                    <select class='form-control ' name='op_op_op_op_type[]' id='option_option_type'><option value=''>--Select--</option><option value='1'>Text</option></select>
                                </div>

                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Price Type</label>
                                    <select class='form-control'  name='op_op_op_op_price_type[]'  ><option value='1'>$</option><option value='2'>%</option></select>
                                </div>
                                <div class='form-group col-md-2'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Price</label>
                                    <input class='form-control' type='text' name='op_op_op_op_price[]'>
                                </div>
                                <div class='form-group col-md-3'>
                                    <label for='attr_option' class='mb-2'>4th-Sub Option Condition</label>
                                    <input class='form-control' type='text' name='op_op_op_op_condition[]'>
                                </div>



                                <div class="form-group col-md-1">
                                    <label class="col-form-label"></label>
                                    <a class="btn btn-xs btn-success add_opp" style="margin-top: 15px;"><i class="fa fa-plus"></i></a>
                                </div>

                                <div class='form-group col-sm-4 img_div'>
                                    <label for='product_img' class='mb-2'>File Upload</label>
                                    <input type='file' class='form-control attr_img' name='op_op_op_op_attr_img[]'>
                                    <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                </div>
                                <div class='form-group col-sm-2 prev_img_div'>
                                    <img src='<?=base_url('assets/no-image.png')?>' class='cls_prev_img' width='100px' height='100px'>
                                </div>

                            </div>

                        </div>

                        <input type="hidden" name="op_op_op_id" id="op_op_op_id" value="">
                          
                        <button type="submit" class="btn btn-success">Save</button><hr>
                       
                        <?php echo form_close();?>
                        
                    </div>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>



<script type="text/javascript">



    function addOpOpOpOp(att_op_op_op_op_id){

        var submit_url = "<?=base_url();?>b_level/Attribute_controller/option_option_option_option_delete/"+att_op_op_op_op_id;
     
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function(res) {

            // $("#att-tbl").load(" #att-tbl > *");
            //$("#cartItems").load(location.href+" #cartItems>*",""); 
            $('#att-tbl').DataTable().ajax.reload( null, false );        
            },error: function() {
                swal('error');
            }
        });
    }


    function addOpOpOp(op_op_op_id){
        $('#op_op_op_id').val(op_op_op_id);
        $('#myModal').modal('show');
    }


    // submit form and add data
    $("#SaveOp").on('submit',function(e){
        e.preventDefault();
            var formData = new FormData(this);
            var submit_url = "b_level/Attribute_controller/option_option_option_option_save";
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res) {
                    if(res==='1'){
                        swal('Save successfully');
                    }
                    if(res==='2'){
                        swal('Internal error plese try again');
                    }
                        
                    $('#myModal').modal('hide'); 
                    // $("#att-tbl").load(" #att-tbl > *");
                    $('#att-tbl').DataTable().ajax.reload( null, false );  
                },error: function() {
                    swal('error');
                }
            });
    });

    $(document).ready(function(){

        var maxField = 10; 
        var addButton = $('.add_opp'); 
        var wrapper = $('.field_wrapper');
        var count = 0;

        $(addButton).on('click',function(){ 

                var rowS="<div class='row'>\n\
                            <div class='form-group col-md-2'>\n\
                                <label class='label'>4th-Sub Option Name</label>\n\
                                <input type='text' name='option_name[]' class='form-control' required>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Type</label>\n\
                                <select class='form-control option_option_type' name='op_op_op_op_type[]' id='option_option_type'><option value=''>--Select--</option><option value='1'>Text</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Price Type</label>\n\
                                <select class='form-control'  name='op_op_op_op_price_type[]'  ><option value='1'>$</option><option value='2'>%</option></select>\n\
                            </div>\n\
                            <div class='form-group col-md-2'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Price</label>\n\
                                <input class='form-control' type='text' name='op_op_op_op_price[]'>\n\
                            </div>\n\
                            <div class='form-group col-md-3'>\n\
                                <label for='attr_option' class='mb-2'>4th-Sub Option Condition</label>\n\
                                <input class='form-control' type='text' name='op_op_op_op_condition[]'>\n\
                            </div>\n\
                            <div class='form-group col-md-1'>\n\
                                <label class='col-form-label'></label>\n\
                                <a class='btn btn-xs btn-danger remove_opp' style='margin-top: 15px;'><i class='fa fa-trash'></i></a>\n\
                            </div>\n\
                            <div class='form-group col-sm-4 img_div'>\n\
                                <label for='product_img' class='mb-2'>File Upload</label>\n\
                                <input type='file' class='form-control attr_img' name='op_op_op_op_attr_img[]'>\n\
                                <p>Extension: jpg|jpeg|png. File size: 2MB</p>\n\
                            </div>\n\
                            <div class='form-group col-sm-2 prev_img_div'>\n\
                                <img src='<?=base_url('assets/no-image.png')?>' class='cls_prev_img' width='100px' height='100px'>\n\
                            </div>\n\
                        </div>";  
                $(wrapper).append(rowS); 
           count++;
        });


        $(wrapper).on('click', '.remove_opp', function(e){ 
            e.preventDefault();
            $(this).parent().parent().remove(); 
            count--; 
        });


    })


    $("body").on("change", ".img_div .attr_img", function(e) {
       
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                swal("Please upload file less than 2MB. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
        }    

        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }




</script>