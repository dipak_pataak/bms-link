<style type="text/css">
    .modal-tr-height{
        height: 45px;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Retailer Completed Return</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Customer Return';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>


        
<!--             <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
<div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Invoice No.">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Supplier Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Product Name">
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-sm btn-success default mt-3">Filter</button>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="px-3">
			<div class="table-responsive">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order Id</th>
                        <th>Item Name</th>
                        <th>Customer Name</th>
                        <th>Return Qty</th>
                        <th>Replace Qty</th>
                        <th>Replace/Repair</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($wholeseler_return_data as $value) { $sl++; ?>
						<tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->order_id; ?></td>
                            <td><?php echo $value->product_name; ?></td>
                            <td><?php echo $value->cust_name; ?></td>
                            <td><?php echo $value->return_qty; ?></td>
                            <td><?php echo $value->replace_qty; ?></td>
                            <td>
                                <?php 
                                    if($value->return_type == 1)
                                        echo "Replace";
                                    else
                                        echo "Repair";
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($value->status == 0)
                                        echo "Pending";
                                    if($value->status == 1)
                                        echo "In Progress";
                                    if($value->status == 3)
                                        echo "Completed";
                                ?>
                            </td>
                            <td class="text-right">
                                <a href="javascript:void(0)" id="address_1" class="btn btn-success">
                                    Resend
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($wholeseler_return_data)) { ?>
                    <tfoot>
                        <tr>
                            <th class="text-danger text-center" colspan="7">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
            </div>
			<?php echo $links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function(){
        $(':input[type="submit"]').prop('disabled', true);
        $('select').on('change', function() {
            $status_val = $(this).val();

            if($status_val == ""){
                $('.error-msg').show();
                $(':input[type="submit"]').prop('disabled', true);
            }else{
                $('.error-msg').hide();
                $(':input[type="submit"]').prop('disabled', false);
            }
        });
    });
</script>