<style type="text/css">
    .modal-tr-height{
        height: 45px;
    }
    .resend-wrapper label {
        border-bottom: 2px solid #dee2e6 !important;
        width: 100%;
        padding: 10px;
        font-weight: bold;
        border: 1px solid #dee2e6;
    }
    .resend-wrapper span {
        border: 1px solid #dee2e6;
        width: 100%;
        display: flex;
        padding: 10px;
        border-top: none;
        min-height: 55px;
        align-items: center;
    }
    @media screen and (max-width:600px){
        .overflowY-hidden{
            overflow-y:hidden !important;
        }
        .resend-wrapper{
            width:500px;
        }
    }
</style>
<!-- content / right -->
<?php $packageid=$this->session->userdata('packageid');?>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Retailer Return</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Customer Return';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
    <?php $menu_permission= b_access_role_permission(133); ?>

        
<!--             <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Filter
            </button>
        </p>
<div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Invoice No.">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Supplier Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control" placeholder="Product Name">
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-sm btn-success default mt-3">Filter</button>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>-->

        <div class="px-3">
			<div class="table-responsive">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order Id</th>
                        <th>Item Name</th>
                        <th>Retailer Name</th>
                        <th>Return Qty</th>
                        <th>Replace Qty</th>
                        <th>Replace/Repair</th>
                        <th>Status</th>
                        <th>Comments</th>
                        <?php if (!empty($packageid)) { ?>
                        <th>Action</th>
                         <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($wholeseler_return_data as $value) { 
                        $sl++; 
                        $get_return_data = $this->db->where(['id'=>$value->return_detail_id])->get("order_return_details")->row();
                    ?>
						<tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->order_id; ?></td>
                            <td><?php echo $value->product_name; ?></td>
                            <td><?php echo $value->cust_name; ?></td>
                            <td><?php echo $value->return_qty; ?></td>
                            <td><?php echo $value->replace_qty; ?></td>
                            <td>
                                <?php 
                                    if($value->return_type == 1)
                                        echo "Replace";
                                    else
                                        echo "Repair";
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($value->status == 0)
                                        echo "Pending";
                                    if($value->status == 1)
                                        echo "In Progress";
                                    if($value->status == 2)
                                        echo "Completed";
                                ?>
                            </td>
                            <td><?php echo $value->comment; ?></td>
                             <?php if (!empty($packageid)) { ?>
                            <td class="text-right">
                            <?php if($value->status == 2){ ?>
                                <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" onclick="show_return_resend('<?php echo $get_return_data->return_id; ?>', '<?php echo $get_return_data->product_id; ?>','<?php echo $value->return_by; ?>');" data-toggle="tooltip" data-placement="right" title="Resend">
                                    <i class="fa fa-share-square" aria-hidden="true"></i>
                                </button>
                            <?php }else{ ?>
                                <a href="javascript:void(0)" id="address_1" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter_<?php echo $value->id; ?>">
                                    Manage Status
                                </a>
                            <?php } ?>
                            </td>
                             <?php } ?>
                            <!-- RMA Modal start -->

                            <div class="modal fade" id="exampleModalCenter_<?php echo $value->id; ?>" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-sm modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title m-0 p-0 border-0">Retailer Return Resend Information</h5>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <form method="post" action="<?php echo base_url('b_level/Return_controller/change_item_status') ?>">
                                        <input type="hidden" name="hidden_wh_re_id" value="<?php echo $value->id; ?>">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <span style="font-size:15px;">Item For: <?php 
                                                                if($value->return_type == 1)
                                                                    echo "Replace";
                                                                else
                                                                    echo "Repair";
                                                            ?>
                                                        </span>
                                                        <select class="form-control mt-3" id="item_status" name="item_status">
                                                            <option value="">Select Status</option>
                                                            <?php if($value->status == 0){ ?>
                                                                <option value="1">In Progress</option>
                                                                <option value="2">Completed</option>
                                                            <?php }if($value->status == 1){ ?>
                                                                <option value="2">Completed</option>
                                                            <?php } ?>
                                                        </select>
                                                        <span class="error-msg mt-1" style="color:red;position: absolute;">Please select status</span>
                                                        <textarea class="form-control mt-4" placeholder="Enter comments.." name="comments"></textarea>
                                                        <input type="submit" onClick="this.form.submit(); this.disabled=true; this.value='Submiting'; " class="btn btn-info mt-3" id="btn-save-changes" value="Save Changes"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="modal-footer">
                                            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <!-- RMA Modal End -->
                        </tr>
                    <?php } ?>
                </tbody>
                <?php if (empty($wholeseler_return_data)) { ?>
                    <tfoot>
                        <tr>
                            <th class="text-danger text-center" colspan="10">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
            </div>
			<?php echo $links; ?>
        </div>
    </div>
</div>
<div class="modal fade" id="resend_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order Return Retailer Resend Information</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="resend_info">

            </div>
            <div class="modal-footer">
<!--                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function(){
        $(':input[type="submit"]').prop('disabled', true);
        $('select').on('change', function() {
            $status_val = $(this).val();

            if($status_val == ""){
                $('.error-msg').show();
                $(':input[type="submit"]').prop('disabled', true);
            }else{
                $('.error-msg').hide();
                $(':input[type="submit"]').prop('disabled', false);
            }
        });
    });
</script>
<script type="text/javascript">
    function show_return_resend(return_id, order_id, returned_by) {
        $.ajax({
            url: "<?php echo base_url('show-retailer-return-resend'); ?>",
            type: 'POST',
            data: {'return_id': return_id, 'order_id': order_id, 'returned_by' : returned_by},
            success: function (data) {
                $("#resend_info").html(data);
                $('#resend_modal_info').modal('show');
            }
        });
    }
</script>
