<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
        .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <div class="box" id="showuser">
        <!-- box / title -->
        <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>
         <?php $menu_permission= b_access_role_permission(8); ?>

        <div class="title">
            <h5>Add Employee</h5>
        </div>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                  <?php if (!empty($packageid)) { ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <?php } ?>

                <?php echo validation_errors() ?>
            </div>
        <?php } ?>

        <!-- end box / title -->
        <?= form_open('b_level/setting_controller/save_user', array('class' => 'p-3', 'name' => 'userFrm')); ?>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="first_name" class="mb-2">First Name<sup class="text-danger">*</sup></label>
                <input type="text" class="form-control" name="first_name" id="first_name" value="<?= @$user->first_name ?>" placeholder="John" onkeyup="required_validation()" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="last_name" class="mb-2">Last Name<sup class="text-danger">*</sup></label>
                <input type="text" class="form-control" name="last_name" id="last_name" value="<?= @$user->last_name ?>" onkeyup="required_validation()" placeholder="Doe" required>
                <div class="valid-tooltip">
                    Looks good!
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="email" class="mb-2">Email<sup class="text-danger">*</sup></label>
                <input type="email" class="form-control" name="email" id="email" onkeyup="check_email_keyup()" value="<?= @$user->email ?>"  placeholder="johndoe@yahoo.com" required>
                <span id="error"></span>
            </div>
            <div class="form-group col-md-6">
                <label for="phone" class="mb-2">Contact<sup class="text-danger">*</sup></label>
                <input type="text" class="form-control phone phone-format" name="phone" id="phone" value="<?= @$user->phone ?>" onkeyup="special_character(1)" placeholder="+1 (XXX)-XXX-XXXX" required>
            </div>
            <div class="form-group col-md-6">
                <label for="address" class="mb-2">Address</label>
                <input type="text" class="form-control" name="address" id="address" value="<?= @$user->address ?>" placeholder="1234 Main St">
            </div>

            <div class="form-group col-md-6">
                <label for="password" class="mb-2">Password<sup class="text-danger">*</sup></label>
                <input type="password" class="form-control col-md-8" name="password" id="password" placeholder="Password" required>
                <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                <input type="button" class="button col-md-2 password_generate_btn btn" value="Generate" onClick="generate();" >
            </div>
            
            <div class="form-group col-md-6">
                <label for="fixed_commission" class="mb-2">Fixed Commission</label>
                <input type="text" class="form-control NumbersAndDot" name="fixed_commission" id="fixed_commission"  placeholder="Fixed Commission">
            </div>

            <div class="form-group col-md-6">
                <label for="percentage_commission" class="mb-2">Commission (%)</label>
                <input type="text" class="form-control NumbersAndDot percentage_valid" name="percentage_commission" id="percentage_commission"  placeholder="Commission (%)">
            </div>

        </div>
         <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
        <input type="submit" class="btn btn-sm d-block float-right btn-success" value="Save">
            <?php }?>
        <?= form_close(); ?>
    </div>
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Employee List</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Employee List';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 float-right text-right">
<!--                <a href="<?php echo base_url(); ?>create-user" class="btn btn-success mt-1">New User</a>-->
            </div>
        </div>
        <?php
        $message = $this->session->flashdata('message');
        $exception = $this->session->flashdata('exception');
        if ($message)
            echo $message;
        if ($exception)
            echo $exception;
        ?>
        <!-- end box / title -->
        <p class="px-4">
            <button class="btn btn-primary default mb-1" type="button" id="collapseExample_btn">
                Filter
            </button>
              <!--<a href="<?php echo base_url(); ?>create-user" class="btn btn-success mt-1" style="margin-top: -5px !important;">New User</a>-->
               <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <button type="button" class="btn btn-success  mb-1" id="addUser_btn" data-toggle="modal">Add Employee</button>
             <?php }?>
        </p>
        <div class="collapse px-4 mt-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>user-filter" method="post">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" class="form-control mb-3 name" placeholder="Enter Name" name="first_name">
                            </div>
                            <span class="or_cls">-- OR --</span>
                            <div class="col-md-3">
                                <input type="email" class="form-control mb-3 email" placeholder="Enter Email" name="email">
                            </div>
                            <span class="or_cls">-- OR --</span>
                            <div class="col-md-2">
                                <input type="text" class="form-control mb-3 type" placeholder="Ener Type" name="type">
                            </div>
                            <div class="col-md-2">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </form>
            </div>
        </div>

        <div class="mt-2 px-3">
			<div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <!--<th>User Type</th>-->
                        <th>Fixed Commission</th>
                        <th>Percentage Commission</th>
                        <th>Status</th>
                        <?php if (!empty($packageid)) { ?>
                        <th class="text-center">Action</th>
                         <?php } ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($users))
                        $i = 1;
                    foreach ($users as $key => $user) {
                        ?>

                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $user->fullname ?></td>
                            <td><?= $user->email ?></td>
                            <td><?= $user->fixed_commission ?></td>
                            <td><?= $user->percentage_commission ?></td>
                            <!--<td><?= $user->user_type ?></td>-->
                            <td>Active </td>
                            <?php if (!empty($packageid)) { ?>
                            <td class="text-right">
                                 <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url() ?>b_level/setting_controller/edit_user/<?= $user->id ?>" class="btn btn-success" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                  <?php }?>
                                  <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url() ?>b_level/setting_controller/delete_user/<?= $user->id ?>" class="btn btn-danger" onclick="return confirm('Do you want to delete it?')" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                <?php }?>
                            </td>
                            <?php } ?>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
            </div>
			<?php echo @$links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">
    $("#showuser").hide();
    $("body").on("click", "#addUser_btn", function () {
        $("#showuser").slideToggle("slow");
        $("#collapseExample").hide();
    });
       $("#collapseExample").hide();
        $("body").on("click", "#collapseExample_btn", function () {
            $("#collapseExample").slideToggle("slow");
            $("#showuser").hide();
        });
//    ============ its for show password ===============
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
//    ============ its for generate password ============
    function randomPassword(length = 6) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    function generate() {
        userFrm.password.value = randomPassword(userFrm.length.value);
    }
//    ============ close generate password =============

//        ========== some field validation ============   
    // $('input[type=submit]').prop('disabled', true);
    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '') {
//            $("#first_name").css({'border': '1px solid red'}).focus();
            $('input[type=submit]').prop('disabled', false);
            return false;
        }
    }
//    =============== its for check_email_keyup ==========
    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
//        console.log(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "get-check-user-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
//                console.log(data);
                if (data != 0) {
//                    $('button[type=submit]').prop('disabled', true);
                    $("#error").html("This email already exists!");
                    $("#error").css({'color': 'red', 'font-weight': 'bold', 'display': 'block', 'margin-top': '5px'});
                    $("#email").css({'border': '2px solid red'}).focus();
                    return false;
                } else {
                    $("#error").hide();
//                    $('button[type=submit]').prop('disabled', false);
                    $("#email").css({'border': '2px solid green'}).focus();
                }
            }
        });
    }

    //=========== its for get special character =========
    function special_character(t) {
//        swal(t);
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone').val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            swal(specialChars + " these special character are not allows");
            $("#phone").focus();
            $("#phone").val('');
        }
    }

//    =============== its for google place address geocomplete ===============
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        swal('No results found');
                    }
                } else {
                    swal('Geocoder failed due to: ' + status);
                }
            });

        });
    });

    $('.NumbersAndDot').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

    $('.percentage_valid').keyup(function(){
      if ($(this).val() > 100){
        $(this).val('100');
      }
    });

    //---------phone_format---------
    $(document).ready(function(){
        load_country_dropdown('phone',country);
    })

</script>
