<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Mail</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Mail';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <?php $menu_permission= b_access_role_permission(58); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3"><?php // echo '<pre>';            print_r($get_mail_config); ?>
            <?php 
                if($get_mail_config[0]->is_verified != 1) {
                    echo "<div class='alert alert-danger'>Mail configuration is not verified. Please verify it.</div>";
                }
            ?>
            <form action="<?php echo base_url(); ?>update-mail-configure" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="form-group row">
                    <label for="protocol" class="col-sm-2 col-md-2 col-lg-2 col-form-label">Protocol<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="protocol" id="protocol" type="text" value="<?php echo $get_mail_config[0]->protocol; ?>">
                    </div>
                    <div class="col-sm-3">
                        <p> smtp </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_host" class="col-sm-2 col-md-2 col-lg-2 col-form-label">SMTP Host<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_host" id="smtp_host" type="text" value="<?php echo $get_mail_config[0]->smtp_host; ?>">
                    </div>
                    <div class="col-sm-3">
                        <p> OR /usr/sbin/sendmail </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_port" class="col-sm-2 col-md-2 col-lg-2 col-form-label">SMTP Port<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_port" id="smtp_port" type="text" value="<?php echo $get_mail_config[0]->smtp_port; ?>">
                    </div>

                    <div class="col-sm-3">
                        <p> 465 </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_user" class="col-sm-2 col-md-2 col-lg-2 col-form-label">Sender Email<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_user" id="smtp_user" type="email" value="<?php echo $get_mail_config[0]->smtp_user; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="smtp_pass" class="col-sm-2 col-md-2 col-lg-2 col-form-label">Password<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <input class="form-control" name="smtp_pass" id="smtp_pass" type="password" value="<?php echo $get_mail_config[0]->smtp_pass; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mailtype" class="col-sm-2 col-md-2 col-lg-2 col-form-label">Mail Type<sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                        <select class="form-control" name="mailtype" id="mailtype">
                            <option value="">Select One</option>
                            <option value="html" <?php if($get_mail_config[0]->mailtype == 'html'){ echo 'selected'; } ?>>Html</option>
                            <option value="text" <?php if($get_mail_config[0]->mailtype == 'text'){ echo 'selected'; } ?>>Text</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <p> html </p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-md-2 col-lg-2 col-form-label"></label>
                    <div class="col-sm-6 text-right">
                        <?php if (!empty($packageid)) { ?>
                        <?php if($get_mail_config[0]->is_verified != 1) { ?>
                             <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <a href="javascript:void(0)" id="verify_configuration_btn" class="btn btn-success">Verify</a>
                             <?php } ?>
                        <?php } ?>
                          <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                        <?php } ?>
                         <?php } ?>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<div id="verify_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send test email</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array('id' => 'verify_configuration', 'name' => 'verify_configuration', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>
                <div class="col-lg-12 px-4">
                    <div class="form-group">
                        <label for="email_id" class="mb-2">EmailId</label>
                        <input class="form-control" type="text" name="email_id" id="email_id" required>
                         <span id="error"></span>
                    </div>
                </div>
                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Send mail</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
$('#verify_configuration_btn').on('click', function () {
    $("#verify_configuration").attr("action", 'b_level/Setting_controller/verify');
    $('#verify_modal').modal('show');
});
</script>
<!-- end content / right -->
