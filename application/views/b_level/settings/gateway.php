<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Paypal Information</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Paypal Information';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <?php $menu_permission= b_access_role_permission(60); ?>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php echo base_url(); ?>save-gateway" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
				<div class="row">
				<div class="col-sm-7 col-md-7 col-lg-5">
                <div class="form-group row">
                    <label for="payment_gateway" class="col-sm-4 col-form-label">Payment Gateway<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <select class="form-control select2" name="payment_gateway" id="payment_gateway" data-placeholder='-- select one --'>
                            <option value=""></option>
                            <option value="paypal" selected>Paypal</option>
                            <!--<option value="sandbox">Sandbox</option>-->
                        </select>
                    </div>
                </div>
                
				<div class="form-group row">
                    <label for="payment_mail" class="col-sm-4 col-form-label">Paypal Mail<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" name="payment_mail" id="payment_mail" class="form-control" placeholder="Payment Mail" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="currency" class="col-sm-4 col-form-label">Currency<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <select class="form-control select2" name="currency" id="currency" data-placeholder="-- select one --" required>
                            <option value=""></option>
                            <option value="USD">(USD) U.S. Dollar</option>
                            <option value="EUR">(EUR) Euro</option>
                            <option value="AUD">(AUD) Australian Dollar</option>
                            <option value="CAD">(CAD) Canadian Dollar</option>
                            <option value="CZK">(CZK) Czech Koruna</option>
                            <option value="DKK">(DKK) Danish Krone</option>
                            <option value="HKD">(HKD) Hong Kong Dollar</option>
                            <option value="Yen">(YEN) Japanese</option>
                            <option value="MXN">(MXN) Mexican Peso</option>
                            <option value="NOK">(NOK) Norwegian Krone</option>
                            <option value="NZD">(NZD) New Zealand Dollar</option>
                            <option value="PHP">(PHP) Philippine Peso</option>
                            <option value="PLN">(PLN) Polish Zloty</option>
                            <option value="SGD">(SGD) Singapore Dollar</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="mode" class="col-sm-4 col-form-label">Mode<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                            <option value="0">Development</option>
                            <option value="1">Production</option>
                        </select>
                        <!--<input type="text" name="is_active" id="is_active" class="form-control" value="<?php echo $gateway_edit[0]['default_status']; ?>">-->
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8 text-right">
                          <?php if (!empty($packageid)) { ?>
                         <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                          <?php } ?>
                           <?php } ?>
                    </div>
                </div>
				</div>
				</div>
			</form>
        </div>
    </div>


    <div class="box">
        <div class="title row">
            <h5>Paypal Payment Gateway</h5>
        </div>
		<div class="px-3">
		<div class="table-responsive">
        <table class="datatable2 table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="8%">SL No.</th>
                    <th width="15%">Name</th>
                    <th width="10%">Email</th>
                    <th width="10%">Currency</th>
                    <th width="10%">Mode</th>
                    <th width="10%">Is Active</th>
                      <?php if (!empty($packageid)) { ?>
                    <th width="12%" class="text-center">Action</th>
                     <?php } ?>

                </tr>
            </thead>
            <tbody>
                <?php
                $sl = 0 + $pagenum;
                foreach ($gateway_list as $gateway) {
                    $sl++;
                    ?>
                    <tr>
                        <td><?php echo $sl; ?></td>
                        <td><?php echo $gateway->payment_gateway; ?></td>
                        <td><?php echo $gateway->payment_mail; ?></td>
                        <td><?php echo $gateway->currency; ?></td>
                        <td><?php
                            if ($gateway->status == 1) {
                                echo 'Production';
                            } else {
                                echo "Development";
                            }
                            ?></td>
                        <td><?php
                            if ($gateway->default_status == 1) {
                                echo 'Active';
                            } else {
                                echo "Inactive";
                            }
                            ?></td>
                               <?php if (!empty($packageid)) { ?>
                        <td class="text-right">
                             <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                            <a href="<?php echo base_url(); ?>gateway-edit/<?php echo $gateway->id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                             <?php } ?>
                            <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                            <a href="<?php echo base_url(); ?>gateway-delete/<?php echo $gateway->id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                             <?php } ?>
                        </td>
                          <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
            <?php if(empty($gateway_list)){ ?>
            <tfoot>
                <tr>
                    <th class="text-danger text-center" colspan="7">Record not found!</th> 
                </tr>
            </tfoot>
            <?php } ?>
        </table>
        </div>
		</div>
		<?php echo $links; ?>
    </div>



</div>
<!-- end content / right -->