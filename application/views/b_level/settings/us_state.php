<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Tax Rate Information</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Tax Rate Information';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="m-2"> 
            <?php if (!empty($packageid)) { ?>
            <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importColor">Import Tax Rate</button>
             <?php } ?>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
             <?php $menu_permission= b_access_role_permission(69); ?>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="importColor" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!--<h4 class="modal-title">Modal Header</h4>-->
                    </div>
                    <div class="modal-body">
                        <a href="<?php echo base_url('assets/b_level/csv/us_state_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>
                        <?php echo form_open_multipart('import-us-state-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                        </div>

                        </form>
                    </div>
                   <!--  <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url('us-state-save'); ?>" method="post">
				<div class="row">
					<div class="col-sm-7 col-md-7 col-lg-5">
						<div class="form-group row">
							<label for="state_name" class="col-sm-4 col-form-label">State Name<sup class="text-danger">*</sup></label>
							<div class="col-sm-8">
								<select name="state_name" class="form-control select2" id="state_name" onchange="city_statename_wise_stateid(this.value)" data-placeholder="-- select one --" required>
									<option value=""></option>
									<?php
									foreach ($get_city_state as $city_state) {
										echo "<option value='$city_state->state_name'>$city_state->state_name</option>";
									}
									?>
								</select>
					<!--<input class="form-control" name="state_name" id="state_name" type="text" placeholder="Enter State Name!" required>-->
							</div>
						</div>
						<!--                <div class="form-group row">
											<label for="short_code" class="col-sm-3 col-form-label">Short Code<sup class="text-danger">*</sup></label>
											<div class="col-sm-6">-->
						<input type="hidden" name="short_code" class="form-control short_code " id="short_code">
					<!--<input type="text" name="short_code" class="form-control short_code" id="short_code" placeholder="Enter Short Code" required>-->
						<!--                    </div>
										</div>-->
						<div class="form-group row">
							<label for="tax_rate" class="col-sm-4 col-form-label">Tax Rate <i class="text-danger"></i></label>
							<div class="col-sm-8">
								<input class="form-control" name="tax_rate" id="tax_rate" type="number" placeholder="Enter Tax Rate!" required min="0.1" max="100.0" step="0.001"> 
                            </div>
						</div>
						<div class="form-group row">
							<div class="col-12 text-right">
                                <?php if (!empty($packageid)) { ?>
                                  <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
								<input type="submit" id="add-shipping_method" class="btn btn-success btn-large" name="" value="Save">
                                 <?php } ?>
                                 <?php } ?>
							</div>
						</div>
					</div>
				</div>
			</form>
        </div>
    </div>

    <div class="box">
        <div class="title">
            <h5>Tax Rate List</h5>
        </div>
		<div class="px-3">
		<div class="table-responsive">
        <table class="table table-bordered mb-3" id="result_search">
            <thead>
                <tr>
                    <th>Sl No.</th>
                    <th>State Name</th>
                    <th>Short Code</th>
                    <th>Tax Rate</th>
                     <?php if (!empty($packageid)) { ?>
                    <th>Action</th>
                     <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $sl = 0 + $pagenum;
                if (!empty($us_state_list)) {
                    foreach ($us_state_list as $key => $val) {

                        $sl++;
                        ?>
                        <tr>
                            <td><?= $sl; ?></td>
                            <td><?= $val->state_name; ?></td>
                            <td><?= $val->shortcode; ?></td>
                            <td><?= $val->tax_rate; ?></td>
                             <?php if (!empty($packageid)) { ?>
                            <td width="100" class="text-right">
                                 <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url(); ?>us-state-edit/<?php echo $val->state_id; ?>" class="btn btn-success  btn-sm " data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                 <?php } ?>
                                  <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url(''); ?>us-state-delete/<?= $val->state_id ?>" onclick="return confirm('Are you sure want to delete it?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </td>
                             <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
            <?php if (empty($us_state_list)) { ?>
                <tfoot>
                    <tr>
                        <th colspan="9" class="text-center text-danger">No record found!</th>
                    </tr> 
                </tfoot>
            <?php } ?>
        </table>
		</div>
		</div>
		<?php echo $links; ?>
    </div>
</div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function city_statename_wise_stateid(t) {
        $.ajax({
            url: "<?php echo base_url('b_level/Setting_controller/city_statename_wise_stateid'); ?>",
            type: 'post',
            data: {state_name: t},
            success: function (r) {
                r = JSON.parse(r);
//                    swal(r);
                $("#short_code").empty();
//                $("#short_code").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $("#short_code").val(typeval.state_id);
//                    $('#short_code').append($('<option>').text(typeval.state_id).attr('value', typeval.state_id));
                });
            }
        });
    }
</script>
