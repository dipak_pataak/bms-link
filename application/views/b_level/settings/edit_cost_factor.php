
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Update Cost Factor</h5>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-wholesaler-customer" class="btn btn-success btn-sm mt-1">Add Customer</a>-->
            </div>
        </div>


        <div class="table-responsive p-3">


            <form action="<?php echo base_url('b_level/setting_controller/update_cost_factor'); ?>" method="post">
                
                <input type="hidden" name="id" value="<?=$data->id?>">
                <div class="form-group col-md-6">
                    <label for="inputEmail4" class="mb-2">Customer<sup class="text-danger">*</sup></label>
                    
                    <select class="form-control select2" name="customer_id" id="customer_id" required="" data-placeholder="--select one --">
                            <option value=""></option>
                            <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'".($data->customer_id==$customer->customer_id?'selected':'')." >$customer->company</option>";
                            }
                            ?>
                    </select>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Product<sup class="text-danger">*</sup></label>
                     <select class="form-control select2" name="product_id" id="product_id" required="" data-placeholder="--select one --">
                            <option value=""></option>
                            <?php
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id' ".($data->product_id==$product->product_id?'selected':'').">$product->product_name</option>";
                            }
                            ?>
                    </select>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Cost Factor<sup class="text-danger">*</sup></label>
                    <input type="number" name="dealer_cost_factor" class="form-control" value="<?=@$data->dealer_cost_factor?>" required="" min="0.1" max="2.0" step="0.001">
                </div>

                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2"></label>
                    <button type="submit" class="btn-success btn-sm "> Update</button>
                </div>

            </form>


        </div>


    </div>
</div>
<!-- end content / right -->