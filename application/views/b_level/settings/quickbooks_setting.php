<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
</style>
<!-- content / right -->
<div id="right">
    <div class="box" id="showuser">
        <!-- box / title -->
        <div class="title">
            <h5>Manage QuickBooks Settings</h5>
        </div>

        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
            <?php $menu_permission= b_access_role_permission(131); ?>
        </div>

        <?php if (validation_errors()) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo validation_errors() ?>
            </div>
        <?php } ?>

        <!-- end box / title -->
        <?= form_open($action, array('class' => 'p-3', 'name' => 'userFrm')); ?>
        <input type="hidden" name="hidden_user_id" value="<?php echo $user_id; ?>">
        <input type="hidden" name="hidden_user_type" value="<?php echo $user_type; ?>">
        <div class="form-group row">
                <label for="client_id" class="col-sm-3 col-form-label text-right">Client Id<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input name="client_id" class="form-control" type="text" placeholder="Client Id" id="client_id" value="<?php echo $quickbooks_setting_data->client_id; ?>" required <?php echo ($connection == "Connected") ? 'readonly' : ''; ?>>
                </div>
        </div>
        
        <div class="form-group row">
            <label for="client_secret" class="col-sm-3 col-form-label text-right">Client Secret<sup class="text-danger">*</sup></label>
            <div class="col-sm-9">
                <input name="client_secret" class="form-control" type="text" placeholder="Client Secrect" id="client_secret" value="<?php echo $quickbooks_setting_data->client_secret; ?>" required <?php echo ($connection == "Connected") ? 'readonly' : ''; ?>>
            </div>
        </div>
        <div class="w-100 text-center d-flex justify-content-center">
            <input type="submit" class="btn btn-sm d-block btn-success mr-3" <?php echo ($connection == "Connected") ? 'disabled' : '';?>  value="<?php echo ($connection == "Connected") ? 'Connected' : 'Connect';?>">
          <?php if($connection == 'Connected') { ?>
            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <input type="submit" class="btn btn-sm d-block btn-danger" value="Disconnected">
             <?php }?>

          <?php } ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>
<!-- end content / right -->