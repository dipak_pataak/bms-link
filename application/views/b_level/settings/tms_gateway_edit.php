<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Gateway Settings</h5>
            <?php
                $page_url = $this->uri->segment(1) . "/" . $this->uri->segment(2) ."/" . $this->uri->segment(3);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Gateway Settings';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">

            <form action="<?php echo base_url(); ?>b_level/setting_controller/update_tms_payment_setting" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
				<div class="row">
				<div class="col-sm-7 col-md-7 col-lg-5">
                <div class="form-group row">
                    <label for="url" class="col-sm-4 col-form-label text-right">URL<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" name="url" id="url" class="form-control" value="<?= $gateway_edit->url; ?>"  required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user_name" class="col-sm-4 col-form-label text-right">User Name<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" name="user_name" id="user_name" class="form-control" value="<?= $gateway_edit->user_name ?>" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-4 col-form-label text-right">Password<sup class="text-danger">*</sup></label>
                    <div class="col-sm-8">
                        <input type="password" name="password" id="password" class="form-control" value="<?= @$gateway_edit->password ?>" placeholder="Payment Mail">
                    </div>
                </div>
				<div class="form-group row">
                    <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8 text-right">
                        <?php if(empty($gateway_list)){ ?>
                        <input type="submit" class="btn btn-success btn-large" value="Update">
                          <?php } ?>
                    </div>
                </div>
				</div>
				</div>
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->
