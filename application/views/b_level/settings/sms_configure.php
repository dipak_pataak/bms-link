<!-- content / right -->
<?php $packageid=$this->session->userdata('packageid'); ?>
<div id="right">
    <!-- table -->
    <div class="box box-main">
        <!-- box / title -->
        <div class="title row">
            <h5>SMS Configuration</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'SMS Configuration';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
         <?php $menu_permission= b_access_role_permission(76); ?>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php base_url(); ?>sms-config-save" method="post">
				<div class="row">
				<div class="col-sm-7 col-md-7 col-lg-5">
                <div class="form-group row">
                    <label for="provider_name" class="col-xs-4 col-form-label">Provider Name<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="user_name" class="col-xs-4 col-form-label">User Name<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-4 col-form-label">Password<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-xs-4 col-form-label">Phone<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="phone" class="form-control phone-format" id="phone" placeholder="+12062024567 This is the valid format" tabindex="4" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sender_name" class="col-xs-4 col-form-label">Sender Name<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="test_sms_number" class="col-xs-4 col-form-label">Test SMS Number<sup class="text-danger">*</sup></label>
                    <div class="col-xs-8">
                        <input type="text" name="test_sms_number" class="form-control" id="test_sms_number" placeholder="+12062024567 This is the valid format" tabindex="5" required>
                    </div>
                </div>

                <div class="form-group  text-right">
                    <?php if (!empty($packageid)) { ?>
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <button type="submit" class="btn btn-success w-md m-b-5"  tabindex="6">Save</button>
                    <button type="reset" class="btn btn-primary w-md m-b-5" tabindex="7">Reset</button>
                    <?php }?>
                    <?php } ?>
                </div>
				</div>
				</div>
            </form>
        </div>
    </div>
    <div class="box">
        <div class="title">
            <h5>List Of SMS Configuration</h5>
        </div>
        <div class="table-responsive p-3">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#.</th>
                        <th width="10%">Name</th>
                        <th width="15%">User Name</th>
                        <th width="15%">Password</th>
                        <th width="10%">Phone</th>
                        <th width="10%">Sender</th>
                        <th width="4%">Status</th>
                        <th width="4%">Verified</th>
                           <?php if (!empty($packageid)) { ?>
                        <th width="30%" class="text-center">Action</th>
                         <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0; // + $pagenum;
                    foreach ($get_sms_config as $value) {
                        $sl++;
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $value->provider_name; ?></td>
                            <td><?php echo $value->user; ?></td>
                            <td><?php echo $value->password; ?></td>
                            <td><?php echo $value->phone; ?></td>
                            <td><?php echo $value->authentication; ?></td>

                            <td>
                                <?php
                                if ($value->default_status == 1) {
                                    echo 'Active';
                                } else {
                                    echo "Inactive";
                                }
                                ?></td>
                            <td>
                                <?php
                                if ($value->is_verify == 1) {
                                    echo 'Yes';
                                } else {
                                    echo "No";
                                }
                                ?></td>
                            <?php if (!empty($packageid)) { ?>
                            <td class="text-right">
                                <?php if ($value->is_verify == 0) { ?>
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?php echo base_url(); ?>sms-config-verified/<?php echo $value->gateway_id; ?>" class="btn btn-info default btn-sm" title="Verified Configuration" data-toggle="tooltip" data-placement="top" data-original-title="Verified Configuration"
                                        ><i class="fa fa-check"></i></a>
                                         <?php } ?>
                                <?php } ?>
                                 <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url(); ?>sms-config-edit/<?php echo $value->gateway_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                 <?php } ?>
                                  <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url(); ?>sms-config-delete/<?php echo $value->gateway_id; ?>" class="btn btn-danger danger btn-sm" onclick="return confirm('Do you want to delete it?')" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                 <?php } ?>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>

                </tbody>
                <?php if (empty($get_sms_config)) { ?>
                    <tfoot>
                    <th colspan="9" class="text-center">
                        No result found!
                    </th>
                    </tfoot>
                <?php } ?>
            </table>

        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    //---------phone_format---------
    $(document).ready(function(){
        load_country_dropdown('phone',country);
    })
</script>
