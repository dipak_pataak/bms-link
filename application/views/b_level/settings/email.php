<link rel="stylesheet" href="<?php echo base_url(); ?>assets/b_level/resources/scripts/summernote/summernote.css" />
<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .note-group-image-url {
        display: none;
    }
    .note-editor .dropdown-menu > li > a {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 1.42857143;
        color: #333;
        white-space: nowrap;
        text-decoration: none;
    }
    .note-editor .dropdown-menu > li > a:hover, .note-editor .dropdown-menu > li > a:focus {
        color: #262626;
        text-decoration: none !important;
        background-color: #f5f5f5;
    }

    .note-editor .dropdown-menu{
        max-height: 200px;
        overflow-y: scroll;
    }
</style>
<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Custom EMAIL</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Custom EMAIL';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php echo base_url('email-send'); ?>" method="post" name="customerFrm" class="p-3" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="mb-2">Receiver Email</label>
                        <select class="selectpicker form-control select-all" id="customer_email" name="customer_email[]" multiple data-live-search="true" data-selected-text-format="count>2">
                            <option value="[all]" class="select-all">Select All</option>
                            <?php foreach ($customers as $val) { ?>
                                <option value="<?= $val->email ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="custom_email" class="mb-2">Custom Email</label>
                        <input type="text" name="custom_email" class="form-control" id="custom_email" placeholder="test@gmail.com, test1@gmail.com" tabindex="1" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="message" class="mb-2">Message<sup class="text-danger">*</sup></label>
                        <textarea name="message"  id="message"class="form-control tinymce" placeholder="Message" rows="7"  tabindex="2"></textarea>
                    </div>
                </div>

                <div class="form-group  text-right">
                     <?php if (!empty($packageid)) { ?>
                 <?php $menu_permission= b_access_role_permission(121); ?>
                     <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
                    <button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="4">Reset</button>
                     <?php }?>
                     <?php } ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/summernote/summernote.js"></script>
<script>
    $(document).ready(function() {
        $('#message').summernote({
            height: 150,
        });
        $(".note-icon-trash").trigger('click');
    });
</script>
<!-- end content / right -->
