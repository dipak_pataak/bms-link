<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Unit of Measurement</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Unit of Measurement';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
             <?php $menu_permission= b_access_role_permission(62); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Setting_controller/uom_save" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="panel">
                    <div class="panel-body">
						<div class="row">
							<div class="col-sm-7 col-md-7 col-lg-5">
							<div class="form-group row">
								<label for="uom_name" class="col-sm-4 control-label">Unit of Measurement</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" name="uom_name" id="uom_name" placeholder="Unit of measurement" required tabindex="1">
								</div>
							</div>
							<div class="form-group row">
								<label for="uom_name" class="col-sm-4 control-label">Unit Type</label>
								<div class="col-sm-8">
									<select name="unit_types" class="form-control select2" id="unit_types">
										<option value="Unit" selected>Unit</option>
										<option value="Quantity">Quantity</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="status" class="col-sm-4 control-label">Status</label>
								<div class="col-sm-8">
									<select name="status" class="form-control select2" id="status" data-placeholder="-- select one --" tabindex="2">
										<option value=""></option>
										<option value="1" selected>Active</option>
										<option value="0">Inactive</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="example-text-input" class="col-sm-4 col-form-label"></label>
								<div class="col-sm-8">
                                    <?php if (!empty($packageid)) { ?>
                                         <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
									<input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Save" tabindex="3" />
                                     <?php } ?>
                                     <?php } ?>
								</div>
							</div>
							</div>
						</div>
					</div>
                </div>
            </form>            

			<div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No</th>
                        <th>Unit of Measurement</th>
                        <th class="">Status</th>
                        <?php if (!empty($packageid)) { ?>
                        <th class="text-center">Action</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($uom_list)) {
                        $sl = 0 + $pagenum;
                        foreach ($uom_list as $key => $value) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->uom_name; ?></td>
                                <td>
                                    <?php if ($value->status == 1) {
                                        echo 'Active';
                                    } else {
                                        echo 'Inactive';
                                    } ?>
                                    </td>
                                    <?php if(!empty($packageid)){?>
                                <td class="text-right">
                                    <?php if($value->uom_name != 'cm' && $value->uom_name != 'inches') { ?>

                                        <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>

                                        <a href="<?php echo base_url(); ?>b_level/Setting_controller/uom_edit/<?php echo $value->uom_id; ?>" title="" class="btn btn-info btn-xs simple-icon-note" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                         <?php } ?>
                                          <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                        <a href="<?php echo base_url(); ?>b_level/Setting_controller/uom_delete/<?php echo $value->uom_id ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-xs simple-icon-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>

                                    <?php } ?>

                                         <?php } ?>
                                    
                                </td>
                                 <?php } ?>
                            </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
<?php if (empty($uom_list)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="3" class="text-center">No record found!</th>
                        </tr> 
                    </tfoot>
<?php } ?>
            </table> 
			</div>
			
			<?php echo $links; ?>
			
			<br />
			<h2>Unit Conversion</h2>
			<form action="<?php echo base_url(); ?>b_level/Setting_controller/save_mapping" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Unit</th>
                                <?php  foreach ($unit_data as $key => $unit) { ?>
                                    <th><?=$unit->uom_name?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $temp_unit_data = $unit_data; ?>
                            <?php foreach ($unit_data as $row_key => $row_unit) { ?>
                                <tr>
                                    <td><?=$row_unit->uom_name?></td>
                                    <?php foreach ($temp_unit_data as $col_key => $col_unit) { 
                                        $measure_data = $this->mapping->get_mapping_data($row_unit->uom_id,$col_unit->uom_id);
                                        // echo "<pre>";print_r($measure_data);die;
                                        ?>
                                       <td>
                                            <input type="hidden" name="mapping_id[]" value="<?=isset($measure_data->id)?$measure_data->id:''?>">
                                            <input type="hidden" name="from_measure_id[]" value="<?=$row_unit->uom_id?>">
                                            <input type="hidden" name="to_measure_id[]" value="<?=$col_unit->uom_id?>">
                                            <input type="text" name="measure_value[]" class="form-control" value="<?=isset($measure_data->measure_value)?$measure_data->measure_value:''?>" required>
                                       </td> 
                                    <?php } ?>
                                </tr>
                            <?php } ?>    
                        </tbody>    
                    </table>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-sm-12">
                         <?php if (!empty($packageid)) { ?>
                            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <input type="submit" class="btn btn-primary btn-large" name="save" value="Save" />
                         <?php } ?>
                         <?php } ?>
                    </div>
                </div>        
            </form>
			
			

        </div>        
    </div>
</div>
<!-- end content / right -->
