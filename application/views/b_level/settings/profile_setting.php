<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<!-- content / right -->
<?php $packageid=$this->session->userdata('packageid'); ?>
<div id="right">
    <!-- table -->
    <div class="box">
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>


        <!-- box / title -->
        <div class="title row">
            <h5>Company Profile</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Company Profile';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <?php $menu_permission= b_access_role_permission(55); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <div class="px-3">

            <?= form_open_multipart('b_level/setting_controller/company_profile_update'); ?>

            <input type="hidden" name="user_id" value="<?= $company_profile[0]->company_id ?>" />
            <input type="hidden" name="redirect_url" value="profile-setting" />
			<div class="col-sm-10 col-md-10 col-lg-8">
            <div class="form-group row">
                <label for="company_name" class="col-sm-3 col-form-label text-right">Company Name<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input name="company_name" class="form-control" type="text" placeholder="Company Name" id="company_name" value="<?= @$company_profile[0]->company_name ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-3 col-form-label text-right">Email Address<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input name="email" class="form-control" type="email" placeholder="Email Address" id="email" value="<?= $company_profile[0]->email ?>" required>
                </div>
            </div>
            <!--            <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label">Change Password<sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <input name="password" class="form-control" type="password" placeholder="Password" id="password" value="">
                            </div>
                        </div>-->
            <div class="form-group row">
                <label for="phone" class="col-sm-3 col-form-label text-right">Phone </label>
                <div class="col-sm-9">
                    <input name="phone" class="form-control phone phone-format" type="text" id="phone" value="<?= $company_profile[0]->phone ?>" placeholder="+1 (XXX)-XXX-XXXX">
                </div>
            </div>

            <div class="form-group row">
                <label for="unit" class="col-sm-3 col-form-label text-right">Choose Unit</label>
                <div class="col-sm-9">
                    <select class="form-control" name="unit" id="unit" tabindex="5">
                        <option value="inches" <?php
                        if ($company_profile[0]->unit == 'inches') {
                            echo "selected";
                        }
                        ?>>inches</option>
                        <option value="cm" <?php
                        if ($company_profile[0]->unit == 'cm') {
                            echo "selected";
                        }
                        ?>>cm</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label for="address" class="col-sm-3 col-form-label text-right">Choose Currency</label>
                <div class="col-sm-9">
                    <select class="form-control" name="currency" id="currency" tabindex="5">
                        <option value="">-- select one --</option>
                        <option value="$" <?php
                        if ($company_profile[0]->currency == '$') {
                            echo "selected";
                        }
                        ?>>$ USD</option>
                        <option value="AU$" <?php
                        if ($company_profile[0]->currency == 'AU$') {
                            echo "selected";
                        }
                        ?>>$ AUD</option>
                        <option value="ƒ" <?php
                        if ($company_profile[0]->currency == 'ƒ') {
                            echo "selected";
                        }
                        ?>>ƒ AWD</option>
                        <option value="R$" <?php
                        if ($company_profile[0]->currency == 'R$') {
                            echo "selected";
                        }
                        ?>>R$ BRL</option>
                        <option value="¥" <?php
                        if ($company_profile[0]->currency == '¥') {
                            echo "selected";
                        }
                        ?>>¥ CNY</option>
                        <option value="₡" <?php
                        if ($company_profile[0]->currency == '₡') {
                            echo "selected";
                        }
                        ?>>₡ CRC</option>
                        <option value="kn" <?php
                        if ($company_profile[0]->currency == 'kn') {
                            echo "selected";
                        }
                        ?>>kn HRK</option>
                        <option value="£" <?php
                        if ($company_profile[0]->currency == '£') {
                            echo "selected";
                        }
                        ?>>£ EGP</option>
                        <option value="€" <?php
                        if ($company_profile[0]->currency == '€') {
                            echo "selected";
                        }
                        ?>>€ EUR</option>
                        <option value="Rs" <?php
                        if ($company_profile[0]->currency == 'Rs') {
                            echo "selected";
                        }
                        ?>>Rs INR</option>
                        <option value="R" <?php
                        if ($company_profile[0]->currency == 'R') {
                            echo "selected";
                        }
                        ?>>R ZAR</option>
                        <option value="₩" <?php
                        if ($company_profile[0]->currency == '₩') {
                            echo "selected";
                        }
                        ?>>₩ KRW</option>
                        <option value="৳" <?php
                        if ($company_profile[0]->currency == '৳') {
                            echo "selected";
                        }
                        ?>>৳ BDT</option>
                        <option value="₨" <?php
                        if ($company_profile[0]->currency == '₨') {
                            echo "selected";
                        }
                        ?>> PKR</option>
                        <option value="₣" <?php
                        if ($company_profile[0]->currency == '₣') {
                            echo "selected";
                        }
                        ?>>Swiss Franc ₣</option>
                        <option value="ر.س" <?php
                        if ($company_profile[0]->currency == 'ر.س') {
                            echo "selected";
                        }
                        ?>>Saudi Riyal ر.س</option>
                        <option value="₣" <?php
                        if ($company_profile[0]->currency == '₣') {
                            echo "selected";
                        }
                        ?>>₣</option>
                        <option value="د.إ" <?php
                        if ($company_profile[0]->currency == 'د.إ') {
                            echo "selected";
                        }
                        ?>>UAE Dirham د.إ</option>د.إ
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="image" class="col-sm-3 col-form-label text-right">Choose Logo</label>
                <div class="col-sm-9">
                    <input type="file" name="logo" id="logo">
                    <input type="hidden" name="logo_hdn" id="logo_hdn" value="<?php echo $company_profile[0]->logo; ?>">
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group row">
                <label for="preview" class="col-sm-3 col-form-label text-right">Preview</label>
                <div class="col-sm-5">
                    <img src="<?php echo base_url(); ?>assets/b_level/uploads/appsettings/<?php echo $company_profile[0]->logo; ?>" class="img-thumbnail <?=($company_profile[0]->logo == '')?'hidden': ''?>" width="125" height="100" id="prevImg">
                </div>
            </div>
            <div class="form-group row">
                <label for="address" class="col-sm-3 col-form-label text-right">Address<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="address" id="address" class="form-control"  value="<?php echo $company_profile[0]->address; ?>" required>
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group row">
                <label for="city" class="col-sm-3 col-form-label text-right">City<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="city" id="city" class="form-control"  value="<?php echo $company_profile[0]->city; ?>">
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group row">
                <label for="state" class="col-sm-3 col-form-label text-right">State<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="state" id="state" class="form-control"  value="<?php echo $company_profile[0]->state; ?>">
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group row">
                <label for="zip" class="col-sm-3 col-form-label text-right">Zip Code<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="zip" id="zip" class="form-control"  value="<?php echo $company_profile[0]->zip_code; ?>">
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>
            <div class="form-group row">
                <label for="country_code" class="col-sm-3 col-form-label text-right">Country Code<sup class="text-danger">*</sup></label>
                <div class="col-sm-9">
                    <input type="text" name="country_code" id="country_code" class="form-control"  value="<?php echo $company_profile[0]->country_code; ?>" readonly>
                    <small id="fileHelp" class="text-muted"></small>
                </div>
            </div>

            <div class="form-group text-right">
               <?php if (!empty($packageid)) { ?>
                 <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
                <?php }?>
               <?php }?>
                <?php if (!empty($packageid)) { ?>
                <a href="<?php echo base_url('profile-setting'); ?>" class="btn btn-danger btn-sm">Cancel</a>
                   <?php }?>

            </div>
			
			</div>
            <?= form_close(); ?>
        </div>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
        $('#logo_hdn').val('');
    }
    $(document).ready(function () {
// -------- Show Image Preview once File selected ----
//        $("#logo").change(function (e) {
        $("body").on("change", "#logo", function (e) {

            if(this.files[0].size > 2000000) {
               swal("Please upload file less than 2MB. Thanks!!");
               $(this).val('');
            }


            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

                var file = e.originalEvent.srcElement.files[i];
                var img = document.getElementById('prevImg');

               
                var reader = new FileReader();
                reader.onloadend = function () {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("logo").after(img);
                
                $("#prevImg").show();
            }
        });
// -------- Image Preview Ends --------------
//========== its for file reset starts =======
        var file_input_index = 0;
        $('input[type=file]').each(function () {
            file_input_index++;
            $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
            $(this).after('<input type="button" value="Clear" class="btn btn-danger" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" />');

        });
//========== its for file reset close=======

    });
//        ----------------- google place api start  -------------
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);

                    } else {
                        swal('No results found');
                    }
                } else {
                    swal('Geocoder failed due to: ' + status);
                }
            });

        });


    });
//        ----------------- google place api close  -------------

//---------phone_format---------
$(document).ready(function(){
    load_country_dropdown('phone',country);
})

</script>
