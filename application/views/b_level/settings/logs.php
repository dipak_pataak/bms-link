
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <div class="title row">
            <h3>List of Access Logs</h3>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'List of Access Logs';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div id="results_menu">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL no.</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>IP Address</th>
                        <th>Done</th>
                        <th>Remarks</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    foreach ($access_logs as $logs) {
                        $sl++;
                        $dateTimes = explode(" ", $logs->entry_date);
                        $date = $dateTimes[0];
                        $time = $dateTimes[1];
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo date('M-d-Y', strtotime($date)); ?></td>
                            <td><?php echo date('H:i:s', strtotime($time)); ?></td>
                            <td><?php echo $logs->ip_address; ?></td>
                            <td><?php echo $logs->action_done; ?></td>
                            <td><?php echo $logs->remarks; ?></td>
                                            <td><?php echo $logs->name; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $links; ?>
        </div>
    </div>  
</div>
<!-- end content / right -->

