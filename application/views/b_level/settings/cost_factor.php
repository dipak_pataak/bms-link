<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<style>
    #result_search_filter {
        display:none;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Cost Factor</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Cost Factor';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
                <!--<a href="<?php echo base_url(); ?>add-wholesaler-customer" class="btn btn-success btn-sm mt-1">Add Customer</a>-->
            </div>
        </div>

        
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->

        <div class="table-responsive p-3">


            <form action="<?php echo base_url(); ?>wholesaler-cost-factor-save" method="post">



<!--                 <div class="form-group col-md-6">
                    
                    <label for="inputEmail4" class="mb-2">Customer<sup class="text-danger">*</sup></label>
                    <select class="form-control selectpicker" name="customer_id[]" id="customer_id" required="" multiple data-actions-box="true"  data-placeholder="--select one --">
                        <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->first_name $customer->last_name</option>";
                            }
                        ?>

                    </select>
                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Product<sup class="text-danger">*</sup></label>
                    <select class="form-control selectpicker" name="product_id[]" id="product_id" multiple data-actions-box="true"  required=""   autocomplete="off" data-placeholder="--select one --">
                        <?php
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id'>$product->product_name</option>";
                            }
                        ?>
                    </select>
                </div>
 -->


                <div class="form-group col-md-6">
                    
                    <label for="inputEmail4" class="mb-2">Customer<sup class="text-danger">*</sup></label>
                    
                    <select class="form-control " name="customer_id[]" id="customer_id" required="" multiple=""  data-placeholder="--select one --">
                    
                        <?php
                            foreach ($get_customer as $customer) {
                                echo "<option value='$customer->customer_id'>$customer->company</option>";
                            }
                        ?>

                    </select>

                    <input type="button" id="select_all_c" name="select_all" value="Select All">
                    <input type="button" id="dselect_all_c" name="select_all" value="DeSelect All">

                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Product<sup class="text-danger">*</sup></label>
                    <!-- <select class="form-control " name="product_id[]" id="product_id" multiple=""  required=""   autocomplete="off" data-placeholder="--select one --">
                        <?php
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id'>$product->product_name</option>";
                            }
                        ?>
                    </select> -->
                    <select class="selectpicker form-control select-all" id="product_id" name="product_id[]" multiple data-live-search="true" data-selected-text-format="count>2" required>
                        <option value="[all]" class="select-all">Select All</option>
                        <?php 
                            foreach ($get_only_product as $product) {
                                echo "<option value='$product->product_id'>$product->product_name</option>";
                            
                        } ?>
                    </select>
                    <!-- <input type="button" id="select_all_p" name="select_all" value="Select All">
                    <input type="button" id="dselect_all_p" name="select_all" value="DeSelect All"> -->
                </div>


                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Cost Factor<sup class="text-danger">*</sup></label>
                    <input type="number" name="dealer_cost_factor" class="form-control" required min="0.1" max="2.0" step="0.001">
                </div>

               <!--  <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2">Individual Cost Factor<sup class="text-danger">*</sup></label>
                    <input type="text" name="individual_cost_factor" class="form-control" required="">
                </div> -->

                <div class="form-group col-md-6">
                    <label for="inputcontact" class="mb-2"></label>
                    <button type="submit" class="btn-success btn-sm "> Save</button>
                </div>

            </form>

<!--             <form action="<?php echo base_url(); ?>wholesaler-cost-factor-save" method="post">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Product List</th>
                            <th class="text-right">Dealer Cost Factor</th>
                            <th class="text-right">Individual Cost Factor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($get_only_product as $single_product) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $single_product->product_name; ?>
                                    <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                </td>
                                <td>
                                    <input type="text" name="dealer_cost_factor[]" class="form-control text-right" value="<?php echo $single_product->dealer_cost_factor; ?>">
                                </td>
                                <td>
                                    <input type="text" name="individual_cost_factor[]" class="form-control text-right" value="<?php echo $single_product->individual_cost_factor; ?>">
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3" class="text-right">
                                <input type="submit" class="btn btn-success" value="Save Change">
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </form> -->
        </div>


        <div class="title row">
            <h5 class="col-sm-6">Cost Factor List</h5>
            <div class="col-sm-6 text-right">
            </div>
        </div>
        <div class="table-responsive p-3">
            <form name="recordlist" id="mainform"  method="post" action="">
                <input type="hidden" name="action">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="txt_dealer_cost_factor" class="mb-2">Modify Cost Factor<sup class="text-danger">*</sup></label>
                        <input type="number" name="txt_dealer_cost_factor" class="form-control col-md-8" required="" min="0.1" max="2.0" step="0.001">
                        <input type="submit" class="button col-md-2 password_generate_btn btn btn-success action-save" value="Save">
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onclick="return action_delete(document.recordlist)">Delete</a>
                    </div>
                    <label for="keyword" class="col-sm-2 col-form-label offset-2 text-right"></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control pull-right" name="keyword" id="keyword" placeholder="Search..." tabindex="">
                        <button type="button" class="btn btn-info btn-sm mb-1 mt-2" data-toggle="modal" data-target="#importcostfactor">Bulk Import Upload</button>   
                    </div>
                </div>
                <table class="datatable2 table table-bordered table-hover" id="result_search">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Company Name</th>
                            <th>Product Name</th>
                            <th class="text-right"> Cost Factor</th>
                             <th class="text-right">Discount</th> 
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($costfactor as $val) {
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->id; ?>" class="checkbox_list">  
                                </td>
                                <td>
                                    <?php echo $val->company; ?>
                                </td>

                                <td>
                                    <?php echo $val->product_name; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $val->dealer_cost_factor; ?>
                                </td>
                                 <td class="text-right">
                                   <?php echo 100 - (100 * $val->dealer_cost_factor) ; ?>
                                </td> 
                                <td class="text-right">
                                   <a href="<?= base_url('wholesaler-edit-cost-factor/').$val->id; ?>" class="btn btn-success default btn-sm" ><i class="fa fa-edit"></i></a>
                                   <a href="<?= base_url('b_level/setting_controller/delete_cust_factor/').$val->id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" ><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->

<!-- Import Bulk Upload Cost factor : Modal -->
<div class="modal fade" id="importcostfactor" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cost factor Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <a href="<?php echo base_url('assets/b_level/csv/costfactor_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                <?php echo form_open_multipart('import-costfactor-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                </div>

                </form>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#select_all_p').click( function() {
        $('select#product_id > option').prop('selected', 'selected');
    });
    $('#dselect_all_p').click( function() {
        $('select#product_id > option').prop('selected', '');
    });

    $('#select_all_c').click( function() {
        $('select#customer_id > option').prop('selected', 'selected');
    });
    $('#dselect_all_c').click( function() {
        $('select#customer_id > option').prop('selected', '');
    });
</script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    oTable = $('#result_search').DataTable( {
        "paging": false,
        "bInfo": false,
    });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
    $('#keyword').keyup(function(){
        oTable.search($(this).val()).draw() ;
    })
    $('.action-save').on('click', function(e) {
        e.preventDefault();
        $('#mainform').attr('action', '<?php echo base_url('b_level/Setting_controller/save_cost_factor') ?>');
        $('#mainform').submit();
    });
    $('.action-delete').on('click', function(e) {
        e.preventDefault();
        $('#mainform').attr('action', '<?php echo base_url('b_level/Setting_controller/manage_action') ?>');
        $('#mainform').submit();
    });
} );
</script>
