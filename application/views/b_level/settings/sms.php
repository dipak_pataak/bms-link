<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Custom SMS</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Custom SMS';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
             <?php $menu_permission= b_access_role_permission(59); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
           <?php if (!empty($packageid)) { ?>
             <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
        <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2"  data-toggle="modal" data-target="#bulkSms" style="font-size: 12px; ">Bulk SMS</a>
        <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2"  data-toggle="modal" data-target="#groupSms" style="font-size: 12px; ">Multi User SMS</a>
         <?php } ?>
         <?php } ?>

        <!-- Modal -->
        <div class="modal fade" id="bulkSms" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!--<h4 class="modal-title">Modal Header</h4>-->
                    </div>
                    <div class="modal-body">
                        <a href="<?php echo base_url('assets/b_level/csv/sms_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>
                        <?php echo form_open_multipart('sms-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                        </div>

                        </form>
                    </div>
                   <!--  <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="groupSms" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Multi User SMS</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open('group-sms-send', array('class' => 'form-row px-3', 'id' => '')); ?>
                        <div class="col-md-12 px-4"> 
                            <div class="form-group row">
                                <label for="customer_id" class="col-md-3 control-label text-right</label>">Customer's Name<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                    <select class="selectpicker form-control select-all" id="customer_phone" name="customer_phone[]" multiple data-live-search="true" data-selected-text-format="count>2" data-all="false"required>
                                        <option value="[all]" class="select-all">Select All</option>
                                        <?php foreach ($customers as $val) { ?>
                                            <option value="<?= $val->phone ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-xs-3 col-form-label text-right">Message<sup class="text-danger">*</sup></label>
                                <div class="col-xs-9">
                                    <textarea name="message"  id="message"class="form-control" placeholder="Message" rows="7"  tabindex="2" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 px-4">
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                            </div>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form action="<?php base_url(); ?>sms-send" method="post">
				<div class="row">
					<div class="col-sm-7 col-md-7 col-lg-5">
						<div class="form-group row">
							<label for="receiver_id" class="col-sm-4 col-form-label text-right">Receiver Number<sup class="text-danger">*</sup></label>
							<div class="col-sm-8">
								<input type="text" name="receiver_id" class="form-control phone" id="receiver_id" placeholder="+1 (XXX)-XXX-XXXX" tabindex="1" required>
								<!--<input type="text" name="receiver_id" class="form-control" id="receiver_id" placeholder="Please follow this format +8801703136868" tabindex="1" required>-->
							</div>
						</div>

						<div class="form-group row">
							<label for="message" class="col-sm-4 col-form-label text-right">Message<sup class="text-danger">*</sup></label>
							<div class="col-sm-8">
								<textarea name="message"  id="message"class="form-control tinymce" placeholder="Message" rows="7"  tabindex="2"></textarea>
							</div>
						</div>

						<div class="form-group  text-right">
                             <?php if (!empty($packageid)) { ?>
                                 <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
							<button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
							<button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="4">Reset</button>
                             <?php } ?>
                             <?php } ?>
						</div>
					</div>
				</div>
            </form>
        </div>
    </div>
</div>
<!-- end content / right -->
  
