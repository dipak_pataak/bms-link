<?php $packageid=$this->session->userdata('packageid'); ?>
            <!-- content / right -->
            <div id="right">
                <!-- table -->
                <div class="box">
                    <?php if($this->session->flashdata('message')){
                        echo  $this->session->flashdata('message');
                    }?>

                    <!-- box / title -->
                    <div class="title row">
                        <h5>Shipping Method</h5>
                        <?php
                            $page_url = $this->uri->segment(1);
                            $get_favorite = get_b_favorite_detail($page_url);
                            $class = "notfavorite_icon";
                            $fav_title = 'Shipping Method';
                            $onclick = 'add_favorite()';
                            if (!empty($get_favorite)) {
                                $class = "favorites_icon";
                                $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                            }
                        ?>
                          <?php $menu_permission= b_access_role_permission(57); ?>
                        <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                        <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                        <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                    </div>
                    <!-- end box / title -->
                    <div class="px-3">
                        <?php echo form_open('b_level/setting_controller/save_shipping_method');?>
                            <div class="row">
								<div class="col-sm-8 col-md-8 col-lg-6">
									<div class="form-group row">
										<label for="method_name" class="col-sm-4 col-form-label">Shipping method name<sup class="text-danger">*</sup></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="method_name" id="method_name" placeholder="Shipping method name" required="">
										</div>
									</div>

									<div class="form-group row">
										<label for="username" class="col-sm-4 col-form-label">User name<sup class="text-danger">*</sup></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="username" id="username" placeholder="Username" required="">
										</div>
									</div>

									<div class="form-group row">
										<label for="password" class="col-sm-4 col-form-label">Password<sup class="text-danger">*</sup></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="password" id="password" placeholder="Password" required="">
										</div>
									</div>

								   <div class="form-group row">
										<label for="account_id" class="col-sm-4 col-form-label">Account id<sup class="text-danger">*</sup></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="account_id" id="account_id" placeholder="Account ID" required="">
										</div>
									</div>

									<div class="form-group row">
										<label for="access_token" class="col-sm-4 col-form-label">Access Token<sup class="text-danger">*</sup></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="access_token" id="access_token" placeholder="Access Token" required="">
										</div>
									</div>

									<div class="form-group row">
										<div class="col-12 text-right">
                                            <?php if (!empty($packageid)) { ?>
                                                 <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
											<input type="submit"  class="btn btn-success btn-large" value="Submit">
                                            <?php }?>
                                              <?php }?>

										</div>
									</div>
								</div>
							</div>
                            <?php echo form_close();?>

                    </div>


   <hr><br>
        <h3>Manage Shipping Method</h3>
        <form class="p-3">
			<div class="table-responsive">
            <table class="table table-bordered mb-3">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Shipping method</th>
                        <th>User Name</th>
                        <th>Password</th>
                        <th>Account Id</th>
                        <th>Access token</th>
                         <?php if (!empty($packageid)) { ?>
                        <th>Action</th>
                          <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($lists as $key => $val) {
                    ?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><?=$val->method_name?></td>
                        <td><?=$val->username?></td>
                        <td><?=$val->password?></td>
                        <td><?=$val->account_id?></td>
                        <td><?=$val->access_token?></td>
                        <?php if (!empty($packageid)) { ?>                                
                        <td class="text-right">
                            <?php if($menu_permission['access_permission'][0]->can_edit==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                            <a href="<?php echo base_url(); ?>shipping-edit/<?=$val->id?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                             <?php } ?>

                            <?php if($val->is_default != '1'){ ?>
                                 <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?php echo base_url(); ?>shipping-method-delete/<?=$val->id?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            <?php } ?>    
                        </td>
                          <?php }?>
                    </tr>
                <?php } ?>


                </tbody>
            </table>
            </div><!--<p class="text-right">
                <button type="submit" class="btn btn-success default w-md m-b-5">Save</button>
            </p>-->
        </form>

                </div>
            </div>
            <!-- end content / right -->
