<?php $currency = @$company_profile[0]->currency; ?>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<style type="text/css">
    .clr_btn {
        position: relative;
    }

    #tprice {
        color: #fff !important;
    }

    .select2-container {
        margin-bottom: 10px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }

    .form-control {
        padding: 6px;
    }

    /*--- for wholesaler --*/
    .form-group {
        margin-bottom: 0;
    }

    br {
        display: none;
    }

    .form-control {
        margin-bottom: 10px;
    }

    .col-sm-9 label {
        padding-top: 10px;
        padding-right: 0 !important;
    }

    .col-sm-6+.col-sm-4 {
        flex: 0 0 100%;
        max-width: 100%;
        clear: both;
        float: none;
    }

    .col-sm-4 .col-sm-12 .form-control:nth-child(3) {
        max-width: 47.5%;
        flex: 0 0 47.5%;
    }

    .col-sm-9 .row .col-sm-3:nth-child(2),
    .col-sm-9 .row .col-sm-4,
    .col-sm-9 .row .col-sm-3:not(:first-child) {
        max-width: 50%;
        flex: 0 0 50%;
    }

    #pattern_color_model .col-sm-2:nth-child(3) {
        padding-top: 10px;
    }

    .row .col-sm-2:nth-child(3) {
        max-width: 13%;
        flex: 0 0 13%;
        padding: 0;
    }

    .row .col-sm-2:nth-child(4) {
        max-width: 12%;
        flex: 0 0 12%;
        padding: 0;
    }

    .col-sm-9 .col-sm-6+.col-sm-4 {
        max-width: 100%;
        flex: 0 0 100%;
        margin: 0 !important;
    }

    .btn-sm,
    .btn-group-sm>.btn {
        font-size: 11px;
        padding: .375rem .75rem;
    }

    @media only screen and (max-width: 570px) {
        .col-sm-9 label {
            margin-bottom: 5px !important;
        }

        .col-sm-9 .row .col-sm-3:nth-child(2),
        .col-sm-9 .row .col-sm-4,
        .col-sm-9 .row .col-sm-4 .row .col-sm-3:not(:last-child) {
            max-width: 70%;
            flex: 0 0 70%;
        }

        .row .col-sm-2:nth-child(3),
        .row .col-sm-2:nth-child(4) {
            max-width: 30%;
            flex: 0 0 30%;
            padding-right: 15px;
        }

        #pattern_color_model .col-sm-2:nth-child(3) {
            margin-bottom: 5px;
        }

        #pattern_color_model .col-sm-2 {
            padding: 0 15px;
        }

        #pattern_color_model .col-sm-3,
        #pattern_color_model .col-sm-2,
        .col-sm-9 .row .col-sm-3:not(:first-child),
        .col-sm-9 .row .col-sm-4:last-child {
            max-width: 100%;
            flex: 0 0 100%;
        }

        .mobile_hidden {
            display: none;
        }
    }
</style>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>

        <div class="title row">
            <div class="col-sm-6">
                <h5>Order New</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Order New';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                   $menu_permission= b_access_role_permission(5)
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>    
        </div>

        <!-- Customer Info : START -->
        <h5>Customer info</h5>
        <div class="separator mb-3"></div>
        <div class="form-row" style="margin-bottom: 30px;">
            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                    <div class="col-sm-8">
                        <p><input type="text" name="orderid" id="orderid" value="" class="form-control" readonly></p>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="order_date" class="col-form-label col-sm-4">Date</label>
                    <div class="col-sm-8">
                        <input type="text" name="order_date" id="order_date" class="form-control datepicker"
                               value="<?php echo date('Y-m-d'); ?>">
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="customer_id" class="col-form-label col-sm-4">Select /Add Customer</label>
                    <div class="col-sm-8" id="custo">
                        <select class="form-control select2" name="customer_id" id="customer_id" required=""
                                data-placeholder="--select one --">
                            <option value=""></option>
                            <?php
                            foreach ($get_customer as $customer) {
                                if (get_cookie('order-customer') == $customer->customer_id) {
                                    echo "<option value='$customer->customer_id' selected>$customer->company</option>";
                                } else {
                                    echo "<option value='$customer->customer_id'>$customer->company</option>";
                                }
                            }
                            ?>
                        </select>
                        <p class="text-danger customer-error d-none">Please select customer</p>
                        <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <a href="javascript:void(0)" onclick="new_customer_modal()" style="white-space:nowrap;">Add
                            New</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0">
                    <label for="side_mark" class="col-form-label col-sm-4">Side Mark</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="side_mark" name="side_mark">
                        <p class="text-danger sitemark-error d-none">Please enter atleast 4 characters</p>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <div class="row m-0 ship_addr" style="display: none;">
                    <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                    <div class="col-sm-8">
                        <input type="text" id="mapLocation" name="shippin_address" class="form-control">
                        <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,US</p>
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <div class="row m-0">
                    <div class="col-sm-8 offset-sm-4 mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="different_address" value="1"
                                   id="shipaddress">
                            <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Customer Info : END -->


        <h5>Add Product to cart</h5>
        <div class="separator mb-3"></div>

        <div class="row  m-0 sticky_container" id="add_order_form">

        </div>
    </div>




    <div class="box">
        <!-- box / title -->
        <!--  <div class="title row">
             <h5 class="col-sm-6">Order New</h5>
         </div> -->
        <!-- end box / title -->
        <!--        <h5>Customer info</h5>-->
        <div class="separator mb-3"></div>

        <form action="<?php echo base_url('b_level/order_controller/save_order'); ?>" id="save_order"
              enctype="multipart/form-data" method="post" accept-charset="utf-8">
            <input type="hidden" name="orderid" id="hid_orderid">
            <input type="hidden" name="order_date" id="hid_order_date">
            <input type="hidden" name="customer_id" id="hid_customer_id">
            <input type="hidden" name="side_mark" id="hid_side_mark">
            <input type="hidden" name="shippin_address" id="hid_shippin_address">
            <input type="hidden" name="different_address" id="hid_different_address">
            <input type="hidden" name="customertype" id="customertype" value="">
            <input type="hidden" name="order_no" id="order_no" value="0">
            <input type="hidden" name="company_name" id="company_name" value="0">
            <input type="hidden" name="cur_company_name" id="cur_company_name" value="0">
            <input type="hidden" name="my_side_mark" id="my_side_mark" value="0">

            <!-- <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="row m-0">
                        <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control" name="file_upload" id="file_upload">
                            <p>Extension:JPG/PNG PDF & DOC. File size: 2MB</p>
                            <img src="<?php //echo base_url(); 
                            ?>assets/profile-pic.png" class="img-thumbnail" width="100" height="50" id="prevImg">
                        </div>
                    </div>
                </div>
            </div> -->



            <h5>Order Details</h5>
            <div class="separator mb-3"></div>

            <div class="px-3" id="cartItems">

                <!--For Item Cart Div : START  -->
                <div class="order_item_cart table-responsive">
                </div>
                <!--For Item Cart Div : END  -->

                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <table class="table table-bordered mb-4">
                            <tr>
                                <td>Sub Total (<?= $currency ?>)</td>
                                <td><input type="number" name="subtotal" id="subtotal" readonly=""
                                           class="form-control text-right"></td>
                            </tr>
                            <tr>
                                <td>Discount (<?= $currency ?>)</td>
                                <td><input type="number" name="invoice_discount" onchange="calculetsPrice()"
                                           onclick="calculetsPrice()" value="0" min="0" step="0.05" id="invoice_discount"
                                           class="form-control text-right"></td>
                            </tr>
                            <tr>
                                <td>Grand Total (<?= $currency ?>)</td>
                                <td><input type="number" name="grand_total" id="grand_total"
                                           class="form-control text-right" readonly=""></td>
                            </tr>
                        </table>
                    </div>
                    <input type="hidden" name="order_status" id="order_status">
                    <div class="col-lg-6 offset-lg-6 text-right">
                        <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <button type="button" class="btn btn-success submit_btn" id="gq">Submit</button>
                        <?php } ?>

                        <a href="<?= base_url(); ?>b_level/order_controller/clear_cart" class="btn btn-danger btn-sm"
                           id="clearCart">Clear All</a>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


<!-- Customer Popup  : START-->
<div class="modal fade" id="new_customer_modal_info" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Information</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="new_customer_info"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Customer Popup : END-->

<!-- end content / right -->
<script type="text/javascript">
    var var_currency = '<?= $currency; ?>';

    function load_add_order_form(rowid, act) {
        // rowid id of producst in cart, 0 = new blank form
        // act view = 0 , edit = 1
        var row_id = 0;
        var action = 0;
        if (rowid != '') {
            row_id = rowid;
        }
        if (act != '') {
            action = act;
        }
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/get_order_form') ?>",
            type: 'get',
            data: {
                row_id: row_id,
                action: action
            },
            success: function (r) {
                $('#add_order_form').empty();
                $('#add_order_form').html(r);
                $("#AddToCart").on('submit', function (e) {
                    e.preventDefault();
                    var submit_url = "<?php echo base_url('b_level/order_controller/add_to_cart') ?>";
                    $.ajax({
                        type: 'POST',
                        url: submit_url,
                        data: $(this).serialize(),
                        success: function (res) {
                            // window.location.reload();
                            toastr.success('Success! - Product add to cart ');
                            $("#AddToCart").load(location.href + " #AddToCart>*", "");
                            order_cart_item();
                            load_add_order_form(0);
                            // $("#customer_id").change();
                        },
                        error: function () {
                            swal('error');
                        }
                    });
                });
            }
        });
    }
//load_add_order_form();

    function order_cart_item() {
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/get_order_item_cart') ?>",
            type: 'get',
            success: function (r) {
                $('.order_item_cart').html(r);
                $("#customer_id").change();

                // load_add_order_form(); // Commented after Code Conflict
                calculetsPrice();
            }
        });
    }

    $(document).ready(function () {

        order_cart_item();
        load_add_order_form();


        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });

        //---------------------------
        // Get customer-wise-sidemark
        // --------------------------
        $("body").on('change', '#customer_id', function () {
            var customer_id = $(this).val();
            $(".customer-error").addClass('d-none');

            $.get("<?php echo base_url(); ?>b_level/order_controller/saveCustSessionForOrder/" + customer_id, function (response) {
                console.log("===="+response);
            });

            $.ajax({
                url: 'customer-wise-sidemark/' + customer_id,
                type: 'get',
                success: function (data) {
                    if (data == 0) {
                        $("#side_mark").val("None");
                    } else {
                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                        var my_side_mark = $('#my_side_mark').val();
                        if (my_side_mark == 0) {
                            $('#side_mark').val(obj.side_mark);
                        }
                        $('#my_side_mark').val(0);
                        $('#orderid').val(obj.order_id);
                        $('#tax').val(tax);
                        if (obj.side_mark) {
                            //$('#side_mark').val(obj.side_mark);
                        }
                        $('#customertype').val(obj.customer_type);
                        $('#order_no').val(obj.order_no);
                        $('#company_name').val(obj.company);
                        $('#cur_company_name').val(obj.comp_name);
                        generate_sidemark();
                        customerWiseComission();
                    }
                }
            });
        });
        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);
        });
        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);
        });
    });


//---------------------------
// submit form and add data
// --------------------------
// submit form and add data
    $("#AddToCart").on('submit', function (e) {
        e.preventDefault();
        var submit_url = "<?php echo base_url('b_level/order_controller/add_to_cart') ?>";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {
                // window.location.reload();
                toastr.success('Success! - Product add to cart ');
                $("#AddToCart").load(location.href + " #AddToCart>*", "");
                order_cart_item();
                load_add_order_form(0);
                // $("#customer_id").change();
            },
            error: function () {
                swal('error');
            }
        });
    });
//---------------------------
// submit to cleare cart 
// --------------------------
// submit form and add data
    $("#clearCart").on('click', function (e) {
        e.preventDefault();
        var submit_url = "<?= base_url(); ?>b_level/order_controller/clear_cart";

        swal({
            title: "Are you sure ??",
            // text: "You will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'POST',
                    url: submit_url,
                    data: $(this).serialize(),
                    success: function (res) {
                        order_cart_item();
                        // $("#customer_id").change();
                        swal("Poof! data cleared!", {
                            icon: "success",
                        });
                    },
                    error: function () {
                        swal("Unknown Error", "Error in ajax call!", "error");
                    }
                });
                // window.location.href = href;
            } else {
                swal("Your data is safe!");
            }
        });
    });


//---------------------------
// Delete cart item
// --------------------------
    function deleteCartItem(id) {
        var submit_url = "<?= base_url(); ?>b_level/order_controller/delete_cart_item/" + id;


        swal({
            title: "Are you sure ??",
            // text: "You will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function (res) {

                        swal("Poof! window deleted!", {
                            icon: "success",
                        }).then((value) => {
                            order_cart_item();
                        });
                        ;

                    },
                    error: function () {
                        swal('error');
                    }
                });
                // window.location.href = href;
            } else {
                swal("Your data is safe!");
            }
        });
    }
//---------------------------
// customer Wise Comission
// --------------------------
    function customerWiseComission() {
        //  console.log('in customerWiseComission');
        var customertype = $('#customertype').val();
        if (customertype == '') {
            // console.log('in customertype null');
            swal('Please select customer!');
            $("#customer_id").focus();
        } else {
            //  console.log('in customertype not null');
            var i = 1;
            //   console.log($(document).find(".product_id"));
            $(document).find(".product_id").each(function () {
                //      console.log('in product_id loop');
                var productid = (this.value);
                var customer_id = $('#customer_id').val();
                var submit_url = "<?= base_url(); ?>b_level/order_controller/getproductcomission/" + productid +
                        '/' + customer_id;
                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    async: false,
                    success: function (res) {
                        var obj = jQuery.parseJSON(res);
                        var qty = $('#qty_' + i).val();
                        var list_price = parseFloat($('#list_price_' + i).val());
                        var h_w_price = parseFloat($('#h_w_price_' + i).val());
                        var upcharge_price = parseFloat($('#upcharge_price_' + i).val());
                        // var total_list_price = (list_price * qty);
                        var total_h_w_price = (h_w_price * qty);
                        var total_upcharge_price = (upcharge_price * qty);
                        //if (customertype === 'business') {
                        var discount_rate = 0;
                        if (obj.dealer_price > 0) {
                            discount_rate = 100 - (obj.dealer_price * 100);
                        } else {
                            discount_rate = 0;
                        }
                        $('#discount_' + i).val(discount_rate);
                        // var discount = (total_list_price * discount_rate) / 100;
                        // $('#utprice_' + i).val((total_list_price - discount).toFixed(2));

                        var discount = (total_h_w_price * discount_rate) / 100;
                        var amtt = (total_h_w_price - discount);
                        $('#final_list_price_'+ i).val(amtt.toFixed(2));
                        var final_ut_price = (amtt + total_upcharge_price).toFixed(2);
                        $('#utprice_' + i).val(final_ut_price);

                        calculetsPrice();
                        i++;
                    },
                    error: function () {
                        swal('error');
                    }
                });
            });
        }
    }

    function calculetsPrice() {
        var subtotal = 0;
        $(".utprice").each(function () {
            isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
        });
        $('#subtotal').val(subtotal.toFixed(2));
        var install_charge = parseFloat($('#install_charge').val());
        var other_charge = parseFloat($('#other_charge').val());
        var invoice_discount = parseFloat($('#invoice_discount').val());
        var misc = parseFloat($('#misc').val());
        var grandtotal = (subtotal);
        var gtotal = grandtotal - invoice_discount;
        $('#grand_total').val(gtotal.toFixed(2));
        calDuePaid();
    }

    function calDuePaid() {
        var grand_total = parseFloat($('#grand_total').val());
        var paid_amount = parseFloat($('#paid_amount').val());
        var due = (grand_total - paid_amount);
        $('#due').val(due.toFixed(2));
    }
    $('#card_area').hide();
    $('#card_area2').hide();

    function setCard(value) {
        if (value === 'card') {
            $('#card_area').slideDown();
            $('#card_area2').slideDown();
        } else if (value === 'cash') {
            $('#card_number').val('');
            $('#issuer').val('');
            $('#card_area').slideUp();
            $('#card_area2').slideUp();
        }
    }

// submit form and add data
    $(".submit_btn").on('click', function (e) {
        // swal('you want to submit ?');
        //e.preventDefault();
        swal({
            title: "Are you sure Submit?",
            text: "Once submit cart data will be destroyed!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {


                var customer_id = $("#customer_id").val();
                var side_mark = $("#side_mark").val();
                if (customer_id == '') {
                    $(".customer-error").removeClass('d-none');
                    $('html, body').animate({
                        scrollTop: "0px"
                    }, 800);
                    e.preventDefault();
                } else if (side_mark.length < 4) {
                    $(".sitemark-error").removeClass('d-none');
                    $('html, body').animate({
                        scrollTop: "0px"
                    }, 800);
                    e.preventDefault();
                } else {
                    $(".customer-error").addClass('d-none');
                    $(".sitemark-error").addClass('d-none');
                    var order_status = $(this).val();
                    var order_status = $('#order_status').val(order_status);

                    // Assign value to hidden for customer info : START
                    var orderid = $("#orderid").val();
                    var order_date = $("#order_date").val();
                    var side_mark = $("#side_mark").val();
                    var mapLocation = $("#mapLocation").val();
                    // var shipaddress = $("#shipaddress").val();

                    if ($("#shipaddress").prop("checked") == true) {
                        var shipaddress = $("#shipaddress").val();
                    } else {
                        var shipaddress = '';
                    }
                    $('#hid_orderid').val(orderid);
                    $('#hid_order_date').val(order_date);
                    $('#hid_customer_id').val(customer_id);
                    $('#hid_side_mark').val(side_mark);
                    $('#hid_shippin_address').val(mapLocation);
                    $('#hid_different_address').val(shipaddress);
                    // Assign value to hidden for customer info : END

                    $('#save_order').submit();
                }
                // window.location.href = href;
            }
        });

        // var submit_url = "<?= base_url(); ?>b_level/order_controller/save_order";
    });

// -------- Show Image Preview once File selected ----
    $("body").on("change", "#file_upload", function (e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.getElementById('prevImg');
            var reader = new FileReader();
            reader.onloadend = function () {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("logo").after(img);
            $("#prevImg").show();
        }
    });

// -------- Image Preview Ends --------------
    $("#openDialog").click(function () {
        $.post("<?php echo base_url(); ?>show-customer-new-window-popup", function (t) {
            var w = window.open("", "popupWindow", "width=600, height=500, scrollbars=yes");
            var $w = $(w.document.body);
            $w.html(t);
        });
    });

//========== its for file reset starts =======
    var file_input_index = 0;
    $('input[type=file]').each(function () {
        file_input_index++;
        $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
        $(this).after(
                '<input type="button" value="Clear" class="btn btn-danger clr_btn" onclick="reset_html(\'file_input_container_' +
                file_input_index + '\')" />');
    });

    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
    }


//========== its for file reset close=======
//========= its for new_customer_modal ==========
    function new_customer_modal() {
        $.post("<?php echo base_url(); ?>new-customer-modal", function (t) {
            $("#new_customer_info").html(t);
            load_autocompelete();
            $('#new_customer_modal_info').modal('show');
        });
    }

    function load_autocompelete() {
        ////============ its for google place api ================
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);

                    } else {
                        swal('No results found');
                    }
                } else {
                    swal('Geocoder failed due to: ' + status);
                }
            });
        });
    }

    function customerWiseComission_Inc_Dic(qty, item) {
        var customer_id = $("#customer_id").val();
        if (customer_id === '') {
            swal('Please select customer');
            $("#customer_id").focus();
        } else {
            var qty = $('#qty_' + item).val();
            var list_price = parseFloat($('#list_price_' + item).val());

            var h_w_price = parseFloat($('#h_w_price_' + item).val());
            var upcharge_price = parseFloat($('#upcharge_price_' + item).val());
            var total_h_w_price = (h_w_price * qty);
            var total_upcharge_price = (upcharge_price * qty);

            // var total_list_price = list_price * qty;
            var dealer_price = $('#discount_' + item).val();
            // var discount = (total_list_price * dealer_price) / 100;
            // $('#utprice_' + item).val((total_list_price - discount).toFixed(2));

            var discount = (total_h_w_price * dealer_price) / 100;
            var amtt = (total_h_w_price - discount);
            var final_ut_price = (amtt + total_upcharge_price).toFixed(2);
            $('#utprice_' + item).val(final_ut_price);
            calculetsPrice();
        }
    }

    $("body").on('click', '.add', function () {
        if ($(this).prev().val() < 1000) {
            $(this).prev().val(+$(this).prev().val() + 1);
            var qty = $(this).prev().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);
        }
    });

    $("body").on('click', '.sub', function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);
            var qty = $(this).next().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);
        }
    });

// update order according to user input on sidemark
    $(document).on('blur', '#side_mark', function () {
        if (side_mark.length < 4) {
            $(".sitemark-error").removeClass('d-none');
        } else {
            generate_sidemark();
        }
    });

    function generate_sidemark() {
        var order_id = $('#orderid').val();
        var side_mark = $('#side_mark').val();
        $(".sitemark-error").addClass('d-none');
        var order_no = $('#order_no').val();
        var company_name = $('#company_name').val();
        var cur_company_name = $('#cur_company_name').val();
        company_name = company_name.replace(/\s+/g, '');
        side_mark = side_mark.replace(/\s+/g, '');
        $('#orderid').val(cur_company_name.substring(0, 4)+'-' + company_name.substring(0, 4) + '-' + side_mark.substring(0, 4) + '-' + order_no);
    }

    $(document).on('click', '#cartbtn', function () {
        $('#my_side_mark').val(1);
    });

    var inFormOrLink;
    $('a').on('click', function () {
        inFormOrLink = true;
    });
    $('form').on('submit', function () {
        inFormOrLink = false;
    });

    $(window).on("beforeunload", function () {
        if (inFormOrLink) {
            return "Do you really want to close?";
        }
    })
    $('body').on('click', '#cpyPrevOrder', function() {
        $('#cartItems .order_item_cart tbody tr:last').find('.copy-btn').click();
    });
</script>

<?php $this->load->view($customerjs) ?>