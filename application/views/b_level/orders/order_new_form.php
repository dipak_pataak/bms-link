<style>
    .price-details {
        width: 80%;
        margin: auto;
    }

    /*.price-details tr td:last-child {
        height: 30px;
        vertical-align: middle;
        width: 25%;
    }*/

    .price-details tr td {
        background-color: #145388 !important;
    }

    /*.price-details tr td:last-child {
        text-align: right;
    }

    .price-details tr:last-child td {
        height: 40px;
        border-top: 1px solid #fff;
    }*/

    .fixed_item {
        background: #145388;
        padding: 15px !important;
    }

    #tprice {
        color: #fff;
    }
    .custom-select-css{
        margin-left: 15px;
        max-width: 46% !important;
    }
</style>

<div class="col-sm-9">
    <?= form_open('b_level/order_controller/add_to_cart', array('id' => 'AddToCart')) ?>
    <div class="form-row">
        <div class="form-group col-md-12">
            <div class="row">
                <label for="" class="col-sm-2">Select Category</label>
                <div class="col-sm-4">
                    <select class="form-control select2-single" name="category_id" id="category_id" required=""
                            data-placeholder="--select one --">
                        <option value=""></option>
                        <?php
                        foreach ($get_category as $category) {
                            echo "<option value='$category->category_id'>$category->category_name</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12" id="subcategory_id">
        </div>
        <div class="form-group col-md-12">
            <div class="row">
                <label for="" class="col-sm-2">Select Product</label>
                <div class="col-sm-4">
                    <select class="form-control select2-single" name="product_id" id="product_id"
                            onchange="getAttribute(this.value)" required data-placeholder="-- select one --">
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12" id="color_model">
            <div class="row">
                <label for="" class="col-sm-2">Pattern</label>
                <div class="col-sm-4">
                    <select class="form-control select2" name="pattern_model_id" id="pattern_id"
                            data-placeholder="-- select pattern --" required="">
                        <!-- <option value="">-- select pattern --</option> -->
                        <!-- <option value="1">Smooth</option> -->
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12" id="pattern_color_model">
            <div class="row">
                <label for="" class="col-sm-2">Color</label>
                <div class="col-sm-4">
                    <select class="form-control select2" name="color_id" id="color_id"
                            onchange="getColorCode(this.value)" required="" data-placeholder="-- select one --">
                       <!--  <option value="">-- select one --</option> -->
                        <!-- <option value="11">Alabaster</option>
                        <option value="10">Antique White</option>
                        <option value="9">White White</option> -->
                    </select>
                </div>
                <!-- <div class="col-sm-2">Color code :</div> -->
                <div class="col-sm-2" style="padding: 0;">
                    <input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control" placeholder='Color Code' >
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="row">
                <label for="" class="col-sm-2">Width (<?=$company_profile[0]->unit?>)</label>
                <div class="col-sm-4">
                    <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()"
                           onKeyup="masked_two_decimal(this);loadPStyle()" min="0" required>
                </div>
                <div class="col-sm-2">
                    <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()"
                            onChange="loadPStyle()" data-placeholder='-- select one --' >
                        <option value="">--Select one--</option>
                        <?php
                        foreach ($fractions as $f) {
                            echo "<option value='$f->id'>$f->fraction_value</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="row">
                <label class="col-sm-2">Height (<?=$company_profile[0]->unit?>)</label>
                <div class="col-sm-4">
                    <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()"
                           onKeyup="masked_two_decimal(this);loadPStyle()" min="0" required>
                </div>
                <div class="col-sm-2">
                    <select class="form-control " name="height_fraction_id" id="height_fraction_id"
                            onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' >
                        <option value="">--Select one--</option>
                        <?php
                        foreach ($fractions as $f) {
                            echo "<option value='$f->id'>$f->fraction_value</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12" id="ssssttt14"></div>
        <div class="form-group col-md-12">
        </div>
        <!-- atributs area -->
        <div class="form-group col-md-12" id="attr">
        </div>
        <!-- End atributs area -->
        <div class="form-group col-md-12">
            <div class="row">
                <label for="" class="col-sm-2">Room</label>
                <div class="col-sm-4">
                    <select class="form-control select2-single" name="room" id="room" required
                            data-placeholder="--select one --">
                        <option value=""></option>
                        <?php
                        foreach ($rooms as $r) {
                            echo "<option value='$r->room_name'>$r->room_name</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            <div class="row">
                <label class="col-sm-2">Special Instruction
</label>
                <div class="col-sm-4">
                    <textarea class="form-control" name="notes" rows="6" id="notes"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group col-md-11 mb-0 btns-box">
            <?php
            if ($view_action == 0) {
                //view, user can also add same product to list
                ?>
                <button type="button" class="btn btn-warning ml-2 float-right" id="cpyPrevOrder">Copy Preovious Order</button>&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn">Add product to list</button>
                <?php
            } else {
                //edit , user can update 
                ?>
                <input type="hidden" name="update_product" value="<?php echo $cart_rowid; ?>">
                <button type="submit" class="btn btn-primary mb-0 float-right" id="cartbtn">Update Product</button>
            <?php } ?>
            <button type="button" class="btn btn-info mb-0 float-right" id="clrbtn"
                    onclick="load_add_order_form('0', '0')">Clear</button>
        </div>
    </div>
    <input type="hidden" name="total_price" id="total_price">
    <input type="hidden" name="h_w_price" id="h_w_price">
    <input type="hidden" name="upcharge_price" id="upcharge_price">
    <?= form_close(); ?>
</div>
<div class="col-sm-3">
    <div class="fixed_item">
        <div id="tprice"></p>
        </div>
    </div>
</div>


<script>
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////-------category product pattern change events starts | Height and width keyup / change events starts----------->
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('body').on('change', '#category_id', function () {
        var category_id = $(this).val();
        onCategoryChange(category_id);
    });
    $('body').on('change', '#product_id', function () {
        var product_id = $(this).val();
        onProductChange(product_id);
    });
    $('body').on('change', '#pattern_id', function () {
        var pattern_id = $(this).val();
        OnPatternChange(pattern_id);
    });
    $('body').on('keyup', '#width', function () {
        var hif = $(this).val().split(".")[1];
        var category_id = $('#category_id').val();
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>b_level/order_controller/get_height_width_fraction/" + hif +
                        "/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#width_fraction_id ").val(r);
                }
            });
        } else {
            $("#width_fraction_id ").val('');
        }
    });
    $('body').on('keyup', '#height', function () {
        var hif = $(this).val().split(".")[1];
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>b_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id ").val(r);
                }
            });
        } else {
            $("#height_fraction_id ").val('');
        }
    });
    $('body').on('change', function () {
        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);
    });


    // Get fractiob value based on enter value in text box for text+fraction type : START
    $('body').on('keyup', '.convert_text_fraction', function () {
        masked_two_decimal(this);
        var text_val = (isNaN($(this).val()) ? '' : $(this).val());
        var attr_option_key = $(this).attr('data-op_op_key');
        var _this = $(this);
        if(text_val){
            var hif = text_val.split(".")[1];
            var category_id = $('#category_id').val();
            if (hif) {
                $.ajax({
                    url: "<?php echo base_url(); ?>b_level/order_controller/get_height_width_fraction/" + hif +
                            "/" + category_id,
                    type: 'get',
                    success: function (r) {
                        // $(".key_text_fraction_"+attr_option_key).val(r);
                        _this.parent().parent('.row').find('.select_text_fraction').val(r);
                    }
                });
            } else {
                // $(".key_text_fraction_"+attr_option_key).val('');
                _this.parent().parent('.row').find('.select_text_fraction').val('');
            }
        }else{
            $(this).val(text_val);
            // $(".key_text_fraction_"+attr_option_key).val('');
            _this.parent().parent('.row').find('.select_text_fraction').val('');
        }    
    });

    $('body').on('blur', '.convert_text_fraction', function () {
        $(this).val($(this).val().split(".")[0]);
    });
    // Get fractiob value based on enter value in text box for text+fraction type : END

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////-------category product pattern change events starts | Height and width keyup / change events End----------->
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    function getColorCode(id) {
        var submit_url = "<?= base_url(); ?>b_level/order_controller/get_color_code/" + id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {
                $('#colorcode').val(res);
            }
        });
    }

    function getColorCode_select(keyword) {
        var pattern_id = $("#pattern_id").val();
        if (keyword !== '') {
            var submit_url = "<?= base_url(); ?>b_level/order_controller/get_color_code_select/" + keyword + '/'+ pattern_id;
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $('#color_id').val(res);
                }
            });
        }
    }

    $("body").on('keyup',"#width",function(){
        // If width >=72 and tube attribute available then select 38mm default : START
        var width = $("#width").val();
        if(width >= 72 && $("select[data-attr-name='Tube']").length > 0){
            $("select[data-attr-name='Tube'] option").each(function() {
                var tube_text = $(this).text();
                var match_text = tube_text.substring(0, 4);
                if(match_text == '38mm'){
                    var tube_val = $(this).val();
                    $("select[data-attr-name='Tube']").val(tube_val);
                }
            });
        }
        // If width >=72 and tube attribute available then select 38mm default : END
    })

    function loadPStyle(callback = false) {
        var product_id = $('#product_id').val();
        var pattern_id = $('#pattern_id').val();
        var pricestyle = $('#pricestyle').val();
        var price = $("#height").parent().parent().parent().next();
        var hif = ($("#height_fraction_id :selected").text().split("/")[0] / $("#height_fraction_id :selected").text()
                .split("/")[1]);
        var wif = ($("#width_fraction_id :selected").text().split("/")[0] / $("#width_fraction_id :selected").text().split(
                "/")[1]);
        var width = parseFloat($('#width').val()) + (isNaN(wif) ? 0 : wif);
        var height = parseFloat($('#height').val()) + (isNaN(hif) ? 0 : hif);
        var width_w = (isNaN(width) ? '' : width);
        var height_w = (isNaN(height) ? '' : height);
        if (pricestyle !== '2') {
            
            // IF category Arch then assign height 0 : START
            var category_name = $("#category_id :selected").text();
            if(height_w == '' && category_name == 'Arch'){
                height_w = 0;
                $('#height').val(0);
            }
            // IF category Arch then assign height 0 : END

            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_row_col_price/') ?>" + height_w +
                        "/" + width_w + "/" + product_id + "/" + pattern_id,
                type: 'get',
                success: function (r) {
                    var obj = jQuery.parseJSON(r);
                    $(price).html(obj.ht);
                    if (isNaN(parseFloat(obj.prince))) {
                        // $("#tprice").text("Total Price = $0");
                        $("#tprice").text("");
                    } else {
                        // $("#tprice").text("Total Price = $" + parseFloat(obj.prince));
                        $("#tprice").text("");
                    }
                    // $("#height").val(obj.col);
                    if (obj.st === 1) {
                        $('#cartbtn').removeAttr('disabled');
                    } else if (obj.st === 2) {
                        //$('#cartbtn').prop('disabled', true);
                    }
                    if (callback != false) {
                        callback();
                    }
                    cal();
                }
            });
        } else if (pricestyle === '2') {
            var main_p = parseFloat($('#sqr_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var new_width = parseFloat(width + 3);
            var new_height = parseFloat(height + 1.5);

            var sum = (new_width * new_height) / 144;
            
            // var sum = (width * height) / 144;
            var price = main_price * sum;
            $('#main_price').val(price.toFixed(2));
            if (callback != false) {
                callback();
            }
            cal();
        } else {
            if (callback != false) {
                callback();
            }
            cal();
    }
    //console.log('loadPStyle calculated');
    }


    function multiOptionPriceValue(att_op_op_op_op_id) {
        var op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var att = att_op_op_op_op_id.split("_")[1];
        var op_op_id = att_op_op_op_op_id.split("_")[2];
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if (op_op_op_id) {
            var wrapper = $("#mul_op_op_id" + op_op_id).parent().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/multioption_price_value/') ?>" + op_op_op_id +
                        "/" + att + "/" + main_price,
                type: 'get',
                success: function (r) {

                    // For shades product fifth attributes : START
                    $(wrapper).show();
                    // For shades product fifth attributes : END


                    $(wrapper).html(r);
                    if (op_op_op_id == '4' && att == '18' && op_op_id == '13') {
                        //  swal(att_op_op_op_op_id);
                        $('body #mul_op_op_id14').val('12_18_14');

                    } else if (op_op_op_id == '5' && att == '18' && op_op_id == '13') {
                        //swal(att_op_op_op_op_id);
                        $('body #mul_op_op_id14').val('22_18_14');

                    } else {
                        $('body #mul_op_op_id14').val('');
                    }
                    cal();
                }
            });
        }
    }

    function OptionFive(att_op_op_op_op_id, attribute_id) {
        var op_op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id = att_op_op_op_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if (op_op_op_op_id) {
            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_five/') ?>" +
                        op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                    loadPStyle();
                }
            });
        } else {
            loadPStyle();
        }
    }

    function OptionOptionsOptionOption(pro_att_op_id, attribute_id) {
        var op_op_op_id = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if (op_op_id) {
            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op_op/') ?>" +
                        op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                    loadPStyle();
                }
            });
        } else {
            loadPStyle();
        }
    }

    function OptionOptionsOption(pro_att_op_id, attribute_id, callback = false) {
        
        var op_op_id = pro_att_op_id.split("_")[0];
        var id = pro_att_op_id.split("_")[1];
        var op_id = pro_att_op_id.split("_")[2];
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if (op_op_id) {
            var wrapper = $("#op_" + op_id).parent().next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op/') ?>" + op_op_id +
                        "/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                    loadPStyle();
                    if (callback != false) {
                        callback();
                    }
                }
            });
        } else {
            if (callback != false) {
                callback();
            }
            loadPStyle();
        }
    }


    function MultiOptionOptionsOption(pro_att_op_id, attribute_id, callback = false) {
        var multi_values = $(pro_att_op_id).val();
        var get_cur_id = $(pro_att_op_id).attr('id');
        
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if(multi_values){
            var wrapper = $("#" + get_cur_id).parent().parent().next().next();
            var multi_val = JSON.stringify(multi_values); 
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op_multiselect/') ?>" + attribute_id + "/" + main_price,
                type: 'post',
                data : { multi_val : multi_val},
                success: function (r) {
                    $(wrapper).html(r);
                    loadPStyle();
                    if (callback != false) {
                        callback();
                    }
                }
            });
        }else{
            if (callback != false) {
                callback();
            }
            loadPStyle();
        }
    }

    function MultiOptionOptionsOptionOptions(pro_att_op_id, attribute_id, callback = false) {
        var multi_values = $(pro_att_op_id).val();
        var get_cur_id = $(pro_att_op_id).attr('id');
        
        var main_p = parseFloat($('#main_price').val());
        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if(multi_values){
            var wrapper = $("#" + get_cur_id).parent().parent().next().next();
            var multi_val = JSON.stringify(multi_values); 
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_op_op_op_op_multiselect/') ?>" + attribute_id + "/" + main_price,
                type: 'post',
                data : { multi_val : multi_val},
                success: function (r) {
                    $(wrapper).html(r);
                    loadPStyle();
                    if (callback != false) {
                        callback();
                    }
                }
            });
        }else{
            if (callback != false) {
                callback();
            }
            loadPStyle();
        }
    }

    function OptionOptions(pro_att_op_id, attribute_id, callback = false) {
        if (pro_att_op_id) {
            //id
            var id = pro_att_op_id.split("_")[0];
            //option_id
            var optin_id = pro_att_op_id.split("_")[1];
            //mainprice
            var main_p = parseFloat($('#main_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }
            
            var attr_name = $('.options_'+attribute_id).attr('data-attr-name');
            if(attr_name == 'Side Channel'){
                // if side channel yes then add upcharge (height(inch) * 0.5)
                var get_selected_value = $(".options_"+attribute_id+ " option:selected").text();
                if(get_selected_value.toLowerCase() == 'yes'){

                    var side_upch = (parseInt($('#height').val()) * 0.3333333333).toFixed(2);
                    // var side_upch = (parseInt($('#height').val()) * 0.5).toFixed(2);

                    $('body .options_'+attribute_id).parent().next().html('<input type="hidden" value="' + side_upch +
                            '" class="form-control side_chan_upcharge contri_price">');
                }else{
                    $('body .options_'+attribute_id).parent().parent().find('.side_chan_upcharge').remove();
                }
            }

            // If Model is speciallity shutter then display Specialty Type section otherwise hide : START
            
            if(attr_name == 'Model'){
                var get_selected_value = $(".options_"+attribute_id+ " option:selected").text();
                var sepcial_type_section = $('select[data-attr-name="Speciality Type"]');
                var option_val = '';
                if(get_selected_value.toLowerCase() == 'specialty shutters'){
                    // Display
                    option_val = $(sepcial_type_section).children('option:eq(1)').val();
                    $(sepcial_type_section).parent().parent('.row').show();
                    $(sepcial_type_section).parent().parent('.row').show();
                }else{
                    //hide
                    option_val = $(sepcial_type_section).children('option:eq(0)').val();
                    $(sepcial_type_section).parent().parent('.row').hide();
                    $(sepcial_type_section).parent().parent('.row').hide();
                }
                $(sepcial_type_section).val(option_val);
                $(sepcial_type_section).change();
            }
            // If Model is speciallity shutter then display Specialty Type section otherwise hide : END

            // If Contorl type is motorized and tube attribute available then select 38mm for tube : START
            if(attr_name == 'Control Type'){
                var get_selected_value = $(".options_"+attribute_id+ " option:selected").text();
                if(get_selected_value == 'Motorized' && $("select[data-attr-name='Tube']").length > 0){
                    $("select[data-attr-name='Tube'] option").each(function() {
                        var tube_text = $(this).text();
                        var match_text = tube_text.substring(0, 4);
                        if(match_text == '38mm'){
                            var tube_val = $(this).val();
                            $("select[data-attr-name='Tube']").val(tube_val);
                        }
                    });
                }
            }
            // If Contorl type is motorized and tube attribute available then select 38mm for tube : END
             
            var wrapper = $(".options_" + attribute_id).parent().next().next();
            $.ajax({
                url: "<?php echo base_url('b_level/order_controller/get_product_attr_option_option/') ?>" + id +
                        "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                    
                    $(".custom_multi_select.selectpicker").selectpicker();
                        
                    loadPStyle();
                    if (callback != false) {
                        callback();

                    }
                }
            });
    }
    }

    function callTrigger() {
        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }

    function cal() {
        var contribut_prices_array = {};

        var w = $('body #width').val();
        var h = $('body #height').val();
        if (w !== '' && h !== '') {
            var contribut_price = 0;
            $("body .contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contribut_price += parseFloat(this.value));
                var contri_price_for = $(this).parent().parent().children('label').text();

                if($(this).hasClass('cls_multi_select_option')){
                    // If multi select then consider this block 
                    contri_price_for = $(this).attr('data-select-mul-option');
                }else{
                    // If not multi select then consider this block 
                    
                    var select_2_length = $(this).parent().parent().children('div').children('.select2').length;
                    // For multi option multiple select data get : START
                    if(select_2_length > 1){
                        var select_op_name = '';
                        $(this).parent().parent().children('div').children('.select2').each(function( index ) {
                          if($( this ).val() != ''){
                            select_op_name += $( this ).find('option:selected').text() + ", ";
                          }
                        });
                        select_op_name = select_op_name.replace(/,\s*$/, "");
                    }else{

                        if($(this).parent().parent().hasClass('fifth_attr_row')){
                            // For fiifth attr name : START
                            var select_op_name = $(this).parent().parent().children('.select2').children("option:selected").text();
                             // For fiifth attr name : END
                        }else{
                            var select_op_name = $(this).parent().parent().children('div').children('.select2').children("option:selected").text();
                        }                        
                    }
                    // For multi option multiple select data get : END
                    
                    if (typeof select_op_name !== 'undefined') {
                        contri_price_for = contri_price_for+" ("+ select_op_name +")"; 
                    }    
                
                    // For blank key issue for Control Type type option : START
                    if(contri_price_for == ''){
                        contri_price_for = $(this).parent('div').prev('div').children('select').children("option:selected").text();
                        if (typeof contri_price_for === 'undefined') {
                            contri_price_for = '';
                        } 
                    }
                    // For blank key issue for Control Type type option : END
                }

                // For cart item order issue : START
                contri_price_for = " "+contri_price_for;
                // For cart item order issue : END
                
                if (contribut_prices_array[contri_price_for] == undefined) {
                    contribut_prices_array[contri_price_for] = parseFloat(this.value);
                } else {
                    contribut_prices_array[contri_price_for] += parseFloat(this.value);
                }
            });

            var main_price = parseFloat($('#main_price').val());

            //console.log(contribut_prices_array);
            var priceListHtml = '<table class="price-details">';
            priceListHtml += "<tr><td>W*H:</td><td>" + var_currency + main_price.toFixed(2); + "</td></tr>";
            for (var key in contribut_prices_array) {
                if (contribut_prices_array.hasOwnProperty(key)) {
                    if (contribut_prices_array[key] > 0) {
                        priceListHtml += "<tr><td>" + key + ":</td><td>" + var_currency + (contribut_prices_array[key]).toFixed(2) +
                                "</td></tr>";
                    }
                }
            }
            if (isNaN(main_price)) {
                var prc = 0;
            } else {
                var prc = main_price;
            }
            var total_price = (contribut_price + prc);
            var t = (isNaN(total_price) ? 0 : total_price);
            $("body #total_price").val(t.toFixed(2));
            $("body #upcharge_price").val(contribut_price.toFixed(2));
            $("body #h_w_price").val(prc.toFixed(2));
            //$("body #tprice").text("Total Price = " + var_currency + t);
            // priceListHtml += "<tr><td>Total Price = </td><td>" + var_currency + (t.toFixed(2)) + "</td></table>";
            $("body #tprice").html(priceListHtml);
        }     
    }





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////-------function for change trigger with callback functionality starts here--------------------->
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function onCategoryChange(selectedCat, callback = false) {
        if ($('#category_id').find(":selected").text() == 'Arch') {
            $('#height').val(0).attr('disabled', 'true');
            $('#height_fraction_id').val(0).attr('disabled', 'true');
            $('#height').parent().parent('.row').parent('.form-group').hide();
        } else {
            $('#height').removeAttr('disabled');
            $('#height_fraction_id').removeAttr('disabled');
            $('#height').parent().parent('.row').parent('.form-group').show();
        }
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/category_wise_subcategory/') ?>" + selectedCat,
            type: 'get',
            success: function (r) {
                $("#subcategory_id").html(r);
                $.ajax({
                    url: "<?php echo base_url('b_level/order_controller/get_product_by_category/') ?>" +
                            selectedCat,
                    type: 'get',
                    success: function (r) {
                        $("#product_id").html(r);
                        $.ajax({
                            url: "<?php echo base_url('b_level/order_controller/get_cat_fraction/') ?>" +
                                    selectedCat,
                            type: 'get',
                            dataType: 'JSON',
                            success: function (r) {
                                $("#width_fraction_id").html('');
                                $("#width_fraction_id").html(r.html);
                                $("#height_fraction_id").html('');
                                $("#height_fraction_id").html(r.html);
                                if (callback != false) {
                                    callback();
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    function onProductChange(selectedProduct, callback = false) {
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/get_product_to_attribute/') ?>" + selectedProduct,
            type: 'get',
            success: function (r) {
                $("#attr").html(r);
                $('#cartbtn').removeAttr('disabled');
                $('#pattern_color_model').html('');
                $.ajax({
                    url: "<?php echo base_url('b_level/order_controller/get_color_partan_model/') ?>" +
                            selectedProduct,
                    type: 'get',
                    success: function (r) {
                        $('#color_model').html(r);
                        if (callback != false) {
                            callback();
                        }
                        callTrigger();
                    }
                });
            }
        });
    }

    function OnPatternChange(selectedPattern, callback = false) {
        var product_id = $('#product_id').val();
        $.ajax({
            url: "<?php echo base_url('b_level/order_controller/get_color_model/') ?>" + product_id + "/" +
                    selectedPattern,
            type: 'get',
            success: function (r) {
                $('body #pattern_color_model').html('');
                $('body  #pattern_color_model').html(r);
                $('body #color_id').parent().removeClass('col-sm-3').addClass('col-sm-6');
                // $('body #color_id').parent().next().removeClass('col-sm-2').addClass('col-sm-1');
                if (callback != false) {
                    callback();
                }
            }
        });
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////-------function for change trigger with callback functionality ends here--------------------->
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $(document).ready(function () {
        if($('.sticky_container .sticky_item').length > 0){
            $('.sticky_container .sticky_item').theiaStickySidebar({
                additionalMarginTop: 110
            });
        }   
    });
</script>

<?php if (isset($win_to_edit) && !empty($win_to_edit)) { ?>
<script type="text/javascript">
        
    $('body #category_id').val("<?php echo $get_product_order_info->category_id; ?>");
    $('body #room').val("<?php echo $get_product_order_info->room; ?>");
    $('body #notes').val("<?php echo $get_product_order_info->notes; ?>");
        onCategoryChange("<?php echo $get_product_order_info->category_id; ?>", function () {
            $('body #product_id').val("<?php echo $get_product_order_info->product_id; ?>");
            onProductChange("<?php echo $get_product_order_info->product_id; ?>", function () {
                $('body #pattern_id').val("<?php echo $get_product_order_info->pattern_model_id; ?>");
                OnPatternChange("<?php echo $get_product_order_info->pattern_model_id; ?>", function () {
                    $('body #color_id').val("<?php echo $get_product_order_info->color_id; ?>");
                    getColorCode("<?php echo $get_product_order_info->color_id; ?>");
                    $('body #width').val("<?php echo $get_product_order_info->width; ?>");
                    $('body #width_fraction_id').val("<?php echo $get_product_order_info->width_fraction_id; ?>");
                    $('body #height').val("<?php echo $get_product_order_info->height; ?>");
                    $('body #height_fraction_id').val("<?php echo $get_product_order_info->height_fraction_id; ?>");
                    loadPStyle(function () {
                        var i = 0;
                        <?php foreach ($selected_attributes as $records) { ?>
                            <?php 
                            $attribute_id = $records->attribute_id;
                            if ($records->attributes_type == 2) {
                                $options_val = $records->options[0]->option_key_value; ?>

                                if ($('select[name="op_id_<?php echo $attribute_id; ?>[]"]').val() != '<?php echo $options_val; ?>' || '<?=$records->options[0]->option_type?>' == 6 || '<?=$records->options[0]->option_type?>' == 4 || '<?=$records->options[0]->option_type?>' == 3) {
                                    $('select[name="op_id_<?php echo $attribute_id; ?>[]"]').val('<?php echo $options_val; ?>');
                                    OptionOptions("<?php echo $options_val; ?>", "<?php echo $attribute_id; ?>", function () {
                                        <?php if ($records->options[0]->option_type == 5) {
                                            if (isset($records->opop[0]) && $records->opop[0]->op_op_value != '') {
                                                $opop_val1 = explode(" ", $records->opop[0]->op_op_value);
                                            }
                                            if (isset($records->opop[1]) && $records->opop[1]->op_op_value != '') {
                                                $opop_val2 = explode(" ", $records->opop[1]->op_op_value);
                                            }
                                            if (isset($records->opop[2]) && $records->opop[2]->op_op_value != '') {
                                                $opop_val3 = explode(" ", $records->opop[2]->op_op_value);
                                            }
                                            ?>
                                            var i = 0;
                                            $('input[name="op_op_value_<?php echo $attribute_id; ?>[]"]').each(function () {
                                                if (i == 0) {
                                                    $(this).val("<?php echo $opop_val1[0]; ?>");
                                                    $('select[name="fraction_<?php echo $attribute_id; ?>[]"]').val("<?php echo $opop_val1[0]; ?>");
                                                }
                                                if (i == 1) {
                                                    $(this).val("<?php echo $opop_val2[0]; ?>");
                                                }
                                                if (i == 2) {
                                                    $(this).val("<?php echo $opop_val3[0]; ?>");
                                                }
                                                i++;
                                            });
                                            var i = 0;
                                            $('select[name="fraction_<?php echo $attribute_id; ?>[]"]').each(function () {
                                                if (i == 0) {
                                                    $(this).val("<?php echo $opop_val1[1]; ?>");
                                                }
                                                if (i == 1) {
                                                    $(this).val("<?php echo $opop_val2[1]; ?>");
                                                }
                                                if (i == 2) {
                                                    $(this).val("<?php echo $opop_val3[1]; ?>");
                                                }
                                                i++;
                                            });
                                        <?php } else if ($records->options[0]->option_type == 1) { ?>
                                            $('input[name="op_value_<?php echo $attribute_id; ?>[]"]').val('<?php echo $records->options[0]->option_value; ?>');
                                        <?php } else if ($records->options[0]->option_type == 3) { ?>
                                            var i = 0;
                                            $('input[name="op_op_value_<?php echo $attribute_id; ?>[]"]').each(function () {
                                                if (i == 0) {
                                                    $(this).val("<?php echo $records->opop[0]->op_op_value; ?>");
                                                }
                                                if (i == 1) {
                                                    $(this).val("<?php echo $records->opop[1]->op_op_value; ?>");
                                                }
                                                if (i == 2) {
                                                    $(this).val("<?php echo $records->opop[2]->op_op_value; ?>");
                                                }
                                                i++;
                                            });
                                        <?php } else if ($records->options[0]->option_type == 2) { ?>
                                            $('select[name="op_op_id_<?php echo $attribute_id; ?>[]"]').val('<?php echo $records->opop[0]->option_key_value; ?>');
                                                OptionOptionsOption('<?php echo $records->opop[0]->option_key_value; ?>', "<?php echo $attribute_id; ?>", function () {
                                                    <?php for ($i = 0; $i < sizeof($records->opopop); $i++) { ?>
                                                        var updatedNumber = 0;
                                                        $('input[name="op_op_op_value_<?php echo $attribute_id; ?>[]"]').each(function () {
                                                            if (updatedNumber == <?php echo $i; ?>) {
                                                                $(this).val('<?php echo $records->opopop[$i]->op_op_op_value; ?>');
                                                            }
                                                            updatedNumber = updatedNumber + 1;
                                                        });
                                                    <?php } ?>
                                                });
                                        <?php } else if ($records->options[0]->option_type == 4) { ?>
                                            var mul_op_arr = new Array();
                                            <?php for ($i = 0; $i < sizeof($records->opop); $i++) { ?>
                                                var option_text = '<?=$records->opop[$i]->op_op_value?>';
                                                
                                                // For multipleselect arr : START
                                                if(option_text.trim() == ''){
                                                    mul_op_arr.push('<?=$records->opop[$i]->op_op_id?>');
                                                }
                                                // For multipleselect arr : END

                                                $("#mul_op_op_id"+<?=$records->opop[$i]->op_op_id?>).val();
                                                $('#mul_op_op_id'+<?=$records->opop[$i]->op_op_id?>+' > option').each(function() {
                                                    if(option_text.trim() == $(this).text().trim()){
                                                        $("#mul_op_op_id"+<?=$records->opop[$i]->op_op_id?>).val($(this).val());
                                                        $("#mul_op_op_id"+<?=$records->opop[$i]->op_op_id?>).change();
                                                    }
                                                });
                                            <?php } ?>

                                            // For Multi option multi select : START
                                            $(mul_op_arr).each(function( index,val_id ) {
                                                var select_mul_arr = new Array();
                                                <?php for ($i = 0; $i < sizeof($records->opopop); $i++) { ?>
                                                    var option_key_val = '<?=$records->opopop[$i]->option_key_value?>';
                                                    if(option_key_val != ''){
                                                        strFine = option_key_val.substring(option_key_val.lastIndexOf('_'));
                                                        if(strFine == "_"+val_id){
                                                            select_mul_arr.push(option_key_val);
                                                        }
                                                    } 
                                                <?php } ?>
                                                $('#mulselect_op_op_op_id_'+val_id).selectpicker('val', select_mul_arr);
                                            });    
                                            // For Multi option multi select : END
                                        <?php } else if ($records->options[0]->option_type == 6) { ?>

                                            var select_mul_arr = new Array();
                                            <?php for ($i = 0; $i < sizeof($records->opop); $i++) { ?> 
                                                select_mul_arr.push('<?=$records->opop[$i]->option_key_value?>');
                                            <?php } ?>
                                            $('#op_<?=$records->options[0]->option_id?>').selectpicker('val', select_mul_arr);
                                        <?php } ?>    
                                    });
                                }
                            <?php } else if ($records->attributes_type == 1) { ?>
                                $('input.op_input_<?php echo $attribute_id; ?>').val('<?php echo $records->attribute_value; ?>');
                            <?php }
                        } ?>
                    });
                });
            });
        });

        function new_test_function(option_id){
            console.log("hi",option_id);
        }
    </script>
<?php } ?>