<style type="text/css">
    .customer-return-table td a.btn-danger {
        color: #fff !important;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
        /*top: 0px !important;*/
        top: 290px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
    .show-invoicedetails{
        margin-right: 0px;
        margin-left: 0px;
    }
</style>
<!-- content / right -->
<?php $packageid=$this->session->userdata('packageid');?>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Return Order</h5>
        </div>
        <!-- end box / title -->
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <div class="col-sm-12">
            <form class="form-horizontal" action="<?php echo base_url('customer-list-filter'); ?>" method="post" id="filter-form">
                <fieldset>
                    <div class="row or-filter">
                        <div class="col-sm-3 offset-md-3">
                            <input type="text" class="form-control mb-3 customer_name" name="customer_name"
                            id="customer_name" placeholder="Retailer Name / Side Mark">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control mb-3 invoice_number" id="invoice_number" name="invoice_number" placeholder="Invoice Number">
                            <select class="form-control" style="display:none"  name="filter_invoice_number" id="filter_invoice_number">
                            <option value="">Select Invoice</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>                    
        </div>

        <div class="row">
            <div class="table-responsive col-sm-6" id="invoice_details"></div>
            <div class="col-sm-6 show-invoicedetails" style="display:none;">
                <form action="" method="post" enctype= multipart/form-data name="frm-add-return-item" id="frm-add-return-item">
                    <input type="hidden" name="order_id" id="return_order_id">
                    <div class="form-row">                            
                        <div class="form-group col-md-6">
                            <label for="product_id">Item<span class="text-danger"> * </span></label>
                            <select name="product_id" id="product_id" class="item-list form-control" required></select>
                        </div>  
                        <div class="form-group col-md-6">
                            <label for="request_return_qty">Quantity to return<span class="text-danger"> * </span></label>
                            <input type="number" min="1" name="request_return_qty"  id="request_return_qty" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="request_return_img">Product Photo</label>
                            <input type="file" multiple name="request_return_img[]" id="request_return_img" class="form-control" onChange="javascript:limitFileUpload();">
                        </div>    
                        <div class="form-group col-md-6">
                            <label for="request_return_type">Resolution<span class="text-danger"> * </span></label>
                            <select name="request_return_type" id="request_return_type" class="form-control" required>
                                <option value="1">Replace</option>
                                <option value="2">Repair</option>
                            </select>
                        </div>    
                        <div class="form-group col-md-6">
                            <label for="request_return_type">Item condition<span class="text-danger"> * </span></label>
                            <select name="request_return_type" id="request_return_type" class="form-control" required>
                                <option value="1">Damaged</option>
                                <option value="2">Not as shown</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="request_return_comments">Reason to return<span class="text-danger"> * </span></label>
                            <input type="text" name="request_return_comments" id="request_return_comments" class="form-control" required>
                        </div>                 
                        <div class="form-group col-md-12 text-right">
                            <input type="submit" name="add_item_to_return" id="add_item_to_return" class="btn btn-primary pull-right" value="Add item to return"/>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
        <!-- <form action="<?php echo base_url(); ?>c-customer-order-return-save" method="post"> -->
        <form action="<?php echo base_url('retailer-order-return-save'); ?>" id="frm-customer-return-save" method="post">
            <div class="row show-invoicedetails" style="display:none;">
                <div class="table-responsive col-sm-12" style="margin-top: 10px">
                    <table class="table table-bordered table-hover customer-return-table" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center">Product Name</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Return Qty </th>
                                <th class="text-center">Comment </th>
                                <th class="text-center">WantTo </th>
                                <th class="text-center">Image </th>
                                <th class="text-center">Action </th>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            
                        </tbody>
                    </table> 
                </div>
            </div>
            <div class="row show-invoicedetails" style="display:none;">
                <div class="col-sm-12">
                    <fieldset>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="productcolor" class="col-form-label">Responsible Staff<span class="text-danger"> * </span></label>
                                <select class="form-control responsible_staff" name="responsible_staff" id="responsible_staff" required>
                                    <option value="">Select employee</option>
                                    <?php
                                    foreach ($get_users as $user) {
                                        echo "<option data-email='$user->email' data-phone='$user->phone' value='$user->id'>$user->first_name $user->last_name</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="setdata" class="col-form-label">Email<span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" name="emp_email" id="emp_email" required>
                            </div>
                            
                            <div class="form-group col-md-4">
                                <label for="setdata" class="col-form-label">Phone<span class="text-danger"> * </span></label>
                                <input class="form-control" name="emp_phone" id="emp_phone" required>
                            </div>
                            
                            <div class="form-group col-md-4">
                                <label for="setdata" class="col-form-label">Set Date<span class="text-danger"> * </span></label>
                                <input class="form-control datepicker emp_date" placeholder="Date" name="emp_date" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="setime" class="col-form-label">Set Time<span class="text-danger"> * </span></label>
                                <input type="text" class="form-control emp_time" data-header-left="true" id="emp_time" name="emp_time" required>
                            </div>
                        
                            <div class="form-group col-md-4 text-left">
                                <label for="remarks" class="col-form-label bfh-timepicker">Remarks</label>
                                <input type="text" class="form-control emp_remarks" name="emp_remarks" id="remarks" placeholder="Remarks">
                            </div>
                            <input type="hidden" name="customer_id" id="customer_id" value="">
                            <input type="hidden" name="order_id" id="order_id" value="">
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success" style="float: right; "><strong>Confirm</strong></button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="Modal-Manage-Order" class="modal fade modal-place-order" role="dialog">
    <div class="modal-dialog modal-xl" style="margin: auto; max-width: 900px; ">
        <div class="modal-content" style="width: 1024px;">
            <div class="modal-header">
                <h4 class="modal-title"><span class="action_type"></span> Order</h4>
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="padding-top:20px!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    function resetAll() {
        $('#addinvoiceItem').html('');
        $('.show-invoicedetails').hide();
        displayData('');
    }

    function return_calculation(item) {
        var stock_quantity = parseInt($("#stock_quantity_" + item).val());
        var quantity = parseInt($("#quantity_" + item).val());
        var already_quantity = parseInt($("#already_return_" + item).val());
        var available_quantity = parseInt(quantity)-parseInt(already_quantity);
        var return_quantity = $("#return_quantity_" + item).val();
        if(return_quantity != '') {
            return_quantity = parseInt($("#return_quantity_" + item).val());
        }
        if (available_quantity < return_quantity) {
            Swal.fire("Return Quantity is not greater than quantity");
            $("#quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
            $('button[type=submit]').prop('disabled', true);
        } else if (return_quantity == '0') {
            Swal.fire("0 is not allowed");
            $("#quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).css({'border': '2px solid red'});
            $("#return_quantity_" + item).val('').focus();
            $('button[type=submit]').prop('disabled', true);
        } else {
            console.log('return_quantity', return_quantity);
            if(return_quantity == '') {
                $('#return_comments_'+ item).prop('required', false);
                $('.return_type_'+ item).prop('required', false); 
            } else {
                $('#return_comments_'+ item).prop('required', true);
                $('.return_type_'+ item).prop('required', true);
            }
            $("#quantity_" + item).css({'border': '2px solid green'});
            $("#return_quantity_" + item).css({'border': '2px solid green'});
            $('button[type=submit]').prop('disabled', false);
        }
    }

    function viewOrderHtml(order_id, row_id) {
        var orderid = order_id;
        var rowid = row_id;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Order_controller/getOrderData') ?>",
            data: {
                order_id: orderid,
                row_id: rowid

            },
            success: function(data) {

                $('#Modal-Manage-Order .action_type').text('View');
                $('#Modal-Manage-Order .modal-body').html(data);
                $('#Modal-Manage-Order').modal('show');

            },
            error: function() {
                Swal.fire('error');
            }
        });
    }

    // File upload limit upto 4 files
    function limitFileUpload() {
        var fileUpload = $("#request_return_img");
        if (parseInt(fileUpload.get(0).files.length) > 4){
            $("#request_return_img").val('');
            swal("You can only upload a maximum of 4 files");
        }
    }

    $(document).ready(function(){
        //Timepicket initialization
        $('#emp_time').datetimepicker({
            //'format': "HH:mm", // HH:mm:ss its for 24 hours format
            format: 'LT', /// its for 12 hours format
        });
        // Customer number auto typing
        $("#customer_name").bind('keyup paste', function() {
            if($(this).val() == '') {
                $("#invoice_number").show();
                $("#filter_invoice_number").html('<option value="">Select Invoice</option>');
                $("#filter_invoice_number").trigger('change');
                $("#filter_invoice_number").hide();
            } else {
                $("#invoice_number").hide();
                $("#filter_invoice_number").show();
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('b_level/B_rma_controller/getCustomers') ?>",
                data: {
                    customer_name: $(this).val()
                },
                success: function(response) {
                    var source = JSON.parse(response);
                    if(source != null) {
                        $("#customer_name").autocomplete({
                            source: source,
                            minLength:1,
                            select: function (event, ui) {
                                event.preventDefault();
                                $("#customer_name").val(ui.item.label);
                                var customer_id = ui.item.value;
                                $('#customer_id').val(customer_id);
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url('b_level/B_rma_controller/getInvoiceNumbers') ?>/" + customer_id,
                                    success: function(data) {
                                        if(data != '') {
                                            $('#filter_invoice_number').html(data);
                                        }
                                    },
                                    error: function() {
                                        Swal.fire('error');
                                    }
                                });
                            }
                        });
                    }
                },
                error: function() {
                    Swal.fire('error');
                }
            });
        });

        // Invoice number auto typing
        $("#invoice_number").bind('keyup paste', function(){
            if($(this).val() == '') {
                resetAll();
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('b_level/B_rma_controller/getInvoiceNumbers') ?>",
                data: {
                    order_id: $(this).val()
                },
                success: function(response) {
                    var source = JSON.parse(response);
                    if(source != null) {
                        $("#invoice_number").autocomplete({
                            source: source,
                            minLength:1,
                            select: function (event, ui) {
                                event.preventDefault();
                                $("#invoice_number").val(ui.item.label);
                                var invoice_number = ui.item.value;
                                displayData(ui.item.label);
                            }
                        });
                    }
                },
                error: function() {
                    Swal.fire('error');
                }
            });
        });

        // Fill data basead on invoice number selected
        $('#filter_invoice_number').on('change', function(){
            if($(this).val() !== '') {
                $('#addinvoiceItem').html('');
                displayData($(this).val());
            } else {
                resetAll();
            }
        });

        // Add item to return list
        $("form#frm-add-return-item").submit(function(e) {
            var product_id = $('#product_id').val();
            e.preventDefault();    
            var formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('b_level/B_rma_controller/addReturnItem') ?>",
                data: formData,
                contentType: false,
                processData: false,
                success: function(response) {
                    $("form#frm-add-return-item")[0].reset();
                    $("#product_id option[value='"+ product_id + "']").attr('disabled', true); 
                    $('#addinvoiceItem').append(response);
                },
                error: function() {
                    Swal.fire('error');
                }
            });
        });
        
    })
    
    // Display order customer details (Left table)
    function displayData(invoice_number) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('b_level/B_rma_controller/getOrderDetails') ?>/" + invoice_number,
            success: function(data) {
                data = JSON.parse(data);
                if(data != '') {
                    console.log('data', data);
                    $('#customer_id').val(data.orderd.customer_id);
                    $('#return_order_id').val(data.orderd.order_id);
                    $('#order_id').val(data.orderd.order_id);
                    var address = '<span>' + data.company_profile[0].address + '</span><br /><span>' + data.company_profile[0].city + ',<br />' + data.company_profile[0].state + ', ' + data.company_profile[0].zip_code + ',' + data.company_profile[0].country_code + '</span><br />';

                    var invoice_details = '<table class="table table-bordered table-hover"><tr><td class="text-left">Customer Name</td><td class="text-left" id="inv_customer_name">' + data.customer.first_name + ' ' + data.customer.last_name + '</td></tr><tr><td class="text-left">Phone</td><td class="text-left" id="inv_phone">' + data.customer.phone + '</td></tr><tr><td class="text-left">Email</td><td class="text-left" id="inv_email">' + data.customer.email + '</td></tr><td class="text-left">Address</td><td class="text-left" id="inv_address">' + address + '</td><tr></tr></tr></table>'
                    
                    $('#invoice_details').html(invoice_details);

                    // Item dropdownlist
                    var itemList = '<option value="">Select item</option>';
                    $.each( data.order_details, function( i, val ) {
                        itemList += '<option data-qty="' + val.product_qty + '" value="' + val.product_id + '">' + val.product_name + '</option>'
                    });
                    $('.item-list').html(itemList);
                    $('.show-invoicedetails').show();
                } else {
                    $('#invoice_details').html('');
                }
            },
            error: function() {
                Swal.fire('error');
            }
        });
    }
    
    // Delete items from return item list
    $("#normalinvoice").on('click', '.delete-product', function (e) {
        e.preventDefault();
        var _this = $(this);
        swal({
            title: "Are you sure ??",
            text: "You want to delete this product",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                var product_id = _this.attr('data-product-id');
                $("#product_id option[value='"+ product_id + "']").attr('disabled', false); 
                $('#product_' + product_id).remove();
            }
        });
    });

    //Assign max return quantity
    $('#product_id').on('change', function() {
        var order_qty = $(this).find(':selected').attr('data-qty');
        $('#request_return_qty').attr('max', order_qty);
    });

    //Assign Employee email and phone
    $('#responsible_staff').on('change', function() {
        var emp_email = $(this).find(':selected').attr('data-email');
        var emp_phone = $(this).find(':selected').attr('data-phone');
        $('#emp_email').val(emp_email);
        $('#emp_phone').val(emp_phone);
    });
</script>