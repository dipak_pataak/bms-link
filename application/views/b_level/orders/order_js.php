<script>
    var var_currency = '$';
</script>
<?php if (isset($currencys[0]->currency) && !empty($currencys[0]->currency)) { ?>
    <script>
        var var_currency = '<?= $currencys[0]->currency; ?>';
    </script>
<?php } ?>

<script type="text/javascript">

    $('#install_charge').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];

        if (att.length > 2) {

            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#other_charge').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#invoice_discount').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#misc').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $(function () {
        $('#personal').click(function () {
            $(".form-template").css("display", "none");
        });
        $('#business').click(function () {
            $(".form-template").css("display", "flex");
        });
    });

    //============ its for username and password field not empty check ==============
    $('body').on('click', '.customer_btn', function () {

        if ($('#business').is(":checked"))
        {
            if ($('#username').val() == '') {
                $('#username').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#username').css({'border': '2px; solid green'});
            }
            if ($('#password').val() == '') {
                $('#password').css({'border': '2px; solid red'}).focus();
                return false;
            } else {
                $('#password').css({'border': '2px; solid green'});
            }
        }
    });


    // submit form and add data
    $('body').on('submit', "#AddToCart", function (e) {
        console.log('call goes here');
        e.preventDefault();
        var submit_url = "<?php echo base_url(); ?>b_level/order_controller/add_to_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {
                if (res == 1) {
                    toastr.success('Success! - Add to cart Successfully');
                } else if (res == 2) {
                    toastr.success('Success! - cart updated Successfully');
                }
                load_add_order_form();
                order_cart_item();
            }
        });
    });

    // submit form and add data
    $('body').on('click', "#clearCart", function (e) {
        e.preventDefault();
        var submit_url = "<?= base_url(); ?>b_level/order_controller/clear_cart";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {
                toastr.success('Success! - Clear cart Successfully');
                order_cart_item();
            }
        });
    });

    // submit form and add data
    $('body').on('submit', "#save_order", function (e) {
        var customer_id = $("#customer_id").val();
        if (customer_id == '') {
            $(".customer-error").removeClass('d-none');
            $('html, body').animate({
                scrollTop: "0px"
            }, 800);
            e.preventDefault();
        } else {
            $(".customer-error").addClass('d-none');
            var order_status = $(this).val();
            var order_status = $('#order_status').val(order_status);
            // Assign value to hidden for customer info : START
            var orderid = $("#orderid").val();
            var order_date = $("#order_date").val();
            var side_mark = $("#side_mark").val();
            var mapLocation = $("#mapLocation").val();
            if ($("#shipaddress").prop("checked") == true) {
                var shipaddress = $("#shipaddress").val();
            } else {
                var shipaddress = '';
            }
            if ($("#synk_status").prop("checked") == true) {
                var synk_status = $("#synk_status").val();
            } else {
                var synk_status = '';
            }
            $('#hid_orderid').val(orderid);
            $('#hid_order_date').val(order_date);
            $('#hid_customer_id').val(customer_id);
            $('#hid_side_mark').val(side_mark);
            $('#hid_shippin_address').val(mapLocation);
            $('#hid_different_address').val(shipaddress);
            $('#hid_synk_status').val(synk_status);
            // Assign value to hidden for customer info : END
            return true;
        }
    });


    $("body").on('click', '.add', function () {
        if ($(this).prev().val() < 1000) {
            $(this).prev().val(+$(this).prev().val() + 1);
            var qty = $(this).prev().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);
        }
    });

    $("body").on('click', '.sub', function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);

            var qty = $(this).next().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);

        }
    });

    $("body").on('change', '#customer_id', function () {
        var customer_id = $(this).val();
        $(".customer-error").addClass('d-none');
        if (customer_id != '') {
            $.ajax({
                url: '<?= base_url() ?>b_level/order_controller/customer_wise_sidemark_new/' + customer_id,
                type: 'get',
                success: function (data) {
                    if (data == 0) {
                        $("#side_mark").val("None");
                    } else {
                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                        $('#customertype').val(obj.level_id);
                        $('#side_mark').val(obj.side_mark);
                        $('#orderid').val(obj.order_id);
                        $('#tax').val(tax);
                        customerWiseComission();
                    }
                }
            });
        } else {
            $('#customertype').val('');
            $('#side_mark').val('');
            $('#orderid').val('');
            $('#tax').val(0);
            customerWiseComission();
        }
    });


    $("body").on('click', '#gq', function () {
        $('#order_status').val(1);
    });

    $("body").on('click', '#gqi', function () {
        $('#order_status').val(2);
    });


    $(document).ready(function () {
        $('#card_area').hide();
        $('#card_area2').hide();


        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });

        $('.sticky_container .sticky_item').theiaStickySidebar({
            additionalMarginTop: 110
        });

        order_cart_item();
    });


    function getSalesTax(v) {
        var formData = {
            'vs': v
        };
        $.ajax({
            url: '<?= base_url() ?>b_level/order_controller/different_shipping_tax',
            type: 'post',
            data: formData,
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                var tax = 0;
                if(obj != null){
                    var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                }
                $('#tax').val(tax);
                customerWiseComission();
            }
        });
    }


    function order_cart_item() {
        $.ajax({
            type: 'GET',
            url: "<?php echo base_url('b_level/order_controller/get_order_item_cart'); ?>",
            success: function (res) {
                $("#cartItems").html(res);
                $('#customer_id').trigger('change');
            }
        });
    }




    function deleteCartItem(id) {
        var submit_url = "<?= base_url(); ?>b_level/order_controller/delete_cart_item/" + id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {
                toastr.success('Success! - Item deleted Successfully');
                order_cart_item();
            }
        });
    }

    function customerWiseComission() {
        var customertype = $('#customertype').val();
        if (customertype == '') {
            // swal('Please select customer');
            $("#customer_id").focus();
        } else {
            var i = 1;
            $(".product_id").each(function () {
                var productid = (this.value);
                var customer_id = $('#customer_id').val();
                var submit_url = "b_level/order_controller/getproductcomission_new/" + productid + '/' + customer_id;
                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function (res) {
                        var obj = jQuery.parseJSON(res);
                        var qty = $('#qty_' + i).val();
                        var list_price = parseFloat($('#list_price_' + i).val());
                        var total_list_price = (list_price * qty);
                        $('#discount_' + i).val(obj.individual_price);
                        var discount = (total_list_price * obj.individual_price) / 100;
                        var utprice = total_list_price - discount;
                        $('#utprice_' + i).val(utprice.toFixed(2));
                        calculetsPrice();
                        i++;
                    }
                });
            });
        }
    }

    function calculetsPrice() {
        var install_charge = parseFloat($('#install_charge').val());
        var other_charge = parseFloat($('#other_charge').val());
        var invoice_discount = parseFloat($('#invoice_discount').val());
        var misc = parseFloat($('#misc').val());
        var subtotal = 0;
        $(".utprice").each(function () {
            isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
        });
        $('#subtotal').val(subtotal.toFixed(2));
        var taxs = parseFloat($('#tax').val());
        var tax = (subtotal * taxs) / 100;
        $('#tax_text').text('Sales tax ' + taxs + '%');
        $('#tax_val').val(tax.toFixed(2));
        var grandtotal = (subtotal + tax + install_charge + other_charge + misc) - invoice_discount;
        $('#grand_total').val(grandtotal.toFixed(2));
        calDuePaid();
    }

    function calDuePaid() {
        var grand_total = parseFloat($('#grand_total').val());
        var paid_amount = parseFloat($('#paid_amount').val());
        var due = (grand_total - paid_amount);
        $('#due').val(due);
    }


    function setCard(value) {
        if (value === 'card') {
            $('#card_area').slideDown();
            $('#card_area2').slideDown();
        } else if (value === 'cash') {
            $('#card_number').val('');
            $('#issuer').val('');
            $('#card_area').slideUp();
            $('#card_area2').slideUp();
        }
    }



    function customerWiseComission_Inc_Dic(qty, item) {
        var customer_id = $("#customer_id").val();
        if (customer_id === '') {
            swal('Please select customer');
            $("#customer_id").focus();
        } else {
            var qty = $('#qty_' + item).val();
            var list_price = parseFloat($('#list_price_' + item).val());
            var total_list_price = (list_price * qty);
            var dealer_price = $('#discount_' + item).val();
            var discount = (total_list_price * dealer_price) / 100;
            $('#utprice_' + item).val((total_list_price - discount.toFixed(2)).toFixed(2));
            calculetsPrice();
        }
    }

</script>
