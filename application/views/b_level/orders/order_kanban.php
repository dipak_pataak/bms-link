<?php $package=$this->session->userdata('packageid'); ?>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Order Manage (Kanban View)</h5>
                <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Order Kanban View';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
                ?>
                <?php  $menu_permission= b_access_role_permission(7);?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
                <?php if(!empty($package)){?>
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <a href="<?php echo base_url(); ?>new-order" class="btn btn-success btn-sm mt-1">Add</a>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

        <style type="text/css">
            .tables_ui {
                min-height: 200px;
                min-width: 200px;
            }

            .tables_ui tbody {
                min-height: 200px;
                display: block;
                width: 100%;
            }

            .fbox tr {
                cursor: move;
            }
        </style>


        <input type="hidden" name="" id="status">
        <input type="hidden" name="" id="orderids">


        <div id="wrapper" class="px-2 d-flex table-responsive">

            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Quote</th>
                    </tr>
                </thead>
                <tbody id="1" class="fbox">
                    <?php foreach ($quote_orderd as $key => $val) { ?>
                        <tr data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>" class="draggable">
                            <td>
                                <p>Order Date:
                                    <?php echo date('M-d-Y', strtotime($val->order_date)); ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Paid</th>
                    </tr>
                </thead>

                <tbody id="T_2" class="fbox">

                    <?php foreach ($paid_orderd as $key => $val) { ?>

                        <tr data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>" class="draggable">
                            <td>
                                <p>Order Date: <?= $val->order_date ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Partially Paid</th>
                    </tr>
                </thead>
                <tbody id="T_3" class="fbox">
                    <?php foreach ($partially_paid_orderd as $key => $val) { ?>
                        <tr data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>" class="draggable">
                            <td>
                                <p>Order Date: <?= $val->order_date ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                 <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Manufacturing</th>
                    </tr>
                </thead>
                <tbody id="T_4" class="fbox">
                    <?php foreach ($manufactur_orderd as $key => $val) { ?>
                        <tr class="draggable" data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>">
                            <td>
                                <p>Order Date: <?= date_format(date_create($val->order_date), 'M-d-Y') ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Shipping</th>
                    </tr>
                </thead>
                <tbody id="T_5" class="fbox">
                    <?php foreach ($shipping_orderd as $key => $val) { ?>
                        <tr data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>" class="draggable">
                            <td>
                                <p>Order Date: <?= date_format(date_create($val->order_date), 'M-d-Y') ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


            <table class="tables_ui" id="t_draggable5">
                <thead>
                    <tr>
                        <th>Cancelled</th>
                    </tr>
                </thead>
                <tbody id="T_6" class="fbox">
                    <?php foreach ($cancelled_orderd as $key => $val) { ?>
                        <tr class="draggable" data-data_id="<?= $val->order_id ?>" id="<?= $val->order_id ?>">
                            <td>
                                <p>Order Date: <?= date_format(date_create($val->order_date), 'M-d-Y') ?></p>
                                <p>Order: <?= $val->order_id ?></p>
                                <p>Client Sidemark: <?= $val->side_mark ?></p>
                                <p>Username: <?= $val->customer_name ?></p>
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $val->order_id; ?>">More Details</a>
                                 <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>


        </div>

    </div>
</div>
<!-- end content / right -->


<script>
    $("#T_6").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            var id = idt.split("_")[1];
            $('#status').val(id);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }

    });


    $("#T_5").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            var id = idt.split("_")[1];
            $('#status').val(id);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }

    });


    $("#T_4").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            var id = idt.split("_")[1];
            $('#status').val(id);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }

    });



    $("#T_3").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            var id = idt.split("_")[1];
            $('#status').val(id);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }

    });



    $("#T_2").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            var id = idt.split("_")[1];
            $('#status').val(id);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }

    });




    $("#1").droppable({

        cursor: "move",
        accept: ".draggable",

        drop: function(event, ui) {

            var idt = event.target.id;
            $('#status').val(idt);
            $(this).removeClass("border").removeClass("over");
            cursor: "move";
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({
                top: 0,
                left: 0
            }).appendTo(droppedOn);
        }


    });


    $(".draggable").draggable({

        stop: function(event, ui) {
            var id = event.target.id;
            $('#orderids').val(id);
            setStatus();
        },

    });



    function setStatus() {
        var stage_id = $('#status').val();

        if (stage_id !== '') {

            var order_id = $('#orderids').val();

            if (stage_id === '2' || stage_id === '3') {
                window.location.href = 'b_level/order_controller/order_view/' + order_id;
            }

            if (stage_id === '5') {
                window.location.href = 'b_level/order_controller/shipment/' + order_id;
            }


            if (stage_id === '6' || stage_id === '4' || stage_id === '1') {

                //swal(stage_id);

                $.ajax({
                    url: "b_level/order_controller/set_order_stage/" + stage_id + "/" + order_id,
                    type: 'GET',

                    success: function(r) {
                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function() {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });
            }

        }

    }
</script>
