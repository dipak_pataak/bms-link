<html>
   <body>
      <div id="SubContainer" style="font-size: 14px;">
         <div id="BodyContainer">
            <div id="SubBodyContainer">
               <table width="100%">
                  <thead style="display:table-header-group;">
                     <tr style="font-weight:bold;">
                        <td style="text-align:right;width:8%;">Item</td>
                        <td style="text-align:center;width:8%;">Qty</td>
                        <td colspan="2" style="text-align:center;width:60%;">Description</td>
                        <td style="text-align:right;width:8%;">List</td>
                        <td style="text-align:right;width:8%;">Discount</td>
                        <td style="text-align:right;width:8%;">Price</td>
                     </tr>
                  </thead>
                  <?php
                     foreach ($order_details as $items) {
                         if ($items->row_id == $row_id) {
                             $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                             $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                             $cat_name = $this->db->where('category_id', $items->category_id)->get('category_tbl')->row();
                             $attributes = $this->db->select('*')->from('b_level_quatation_attributes a')->where('a.fk_od_id', $row_id)->get()->result();
                             $selected_attributes = json_decode($attributes[0]->product_attribute);
                             ?>
                  <tbody>
                     <tr>
                        <td colspan="11" style="border-top:1px solid black;">&nbsp;</td>
                     </tr>
                     <tr>
                        <td style="vertical-align:top;text-align:right;">1</td>
                        <td style="vertical-align:top;text-align:center;"><?php echo $items->product_qty; ?></td>
                        <td colspan="2"><?php
                           echo $items->product_name . ', ';
                           if ($items->pattern_name) {
                               echo $items->pattern_name . '';
                           }
                           ?></td>
                        <td style="vertical-align:top;text-align:right;"><?= $company_profile[0]->currency; ?><?= number_format($items->list_price, 2) ?></td>
                        <td style="vertical-align:top;text-align:right;"><?= ($items->discount) ?></td>
                        <td style="vertical-align:top;text-align:right;"><?= $company_profile[0]->currency; ?> <?= @number_format($items->unit_total_price, 2) ?></td>
                     </tr>
                     <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2"> Category:  <?= $cat_name->category_name; ?> 
                     <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2"> Color:  <?= $items->color_number; ?> 
                           <?php echo '(' . $items->color_name . ')'; ?>
                        </td>
                     </tr>
                     
                     <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>Room Location</td>
                        <td><?php echo $items->room; ?></td>
                     </tr>
                     <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>Width</td>
                        <td><?= $items->width; ?> <?= @$width_fraction->fraction_value ?></td>
                     </tr>
                     <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>Height</td>
                        <td><?= $items->height; ?> <?= @$height_fraction->fraction_value ?> </td>
                     </tr>
                     <?php
                        foreach ($selected_attributes as $atributes) { ?>
                           
                           <!-- Old View : START -->
                            <!-- <tr>
                              <td colspan="2">&nbsp;</td>
                              <td><?php
                                 $at_id = $atributes->attribute_id;
                                 $att_name = $this->db->where('attribute_id', $at_id)->get('attribute_tbl')->row();
                                 echo $att_name->attribute_name;
                                 ?></td>
                              <td><?php
                                 $at_op_id = $atributes->options[0]->option_id;
                                 $att_op_name = $this->db->where('att_op_id', $at_op_id)->get('attr_options')->row();
                                 echo $att_op_name->option_name;
                                 ?></td>
                            </tr> -->
                            <!-- Old View : END -->

                            <!-- New View : START -->

                            <tr>
                                <td colspan="2">&nbsp;</td>
                                <td>
                                    <?php
                                        $at_id = $atributes->attribute_id;
                                        $att_name = $this->db->where('attribute_id', $at_id)->get('attribute_tbl')->row();
                                        echo $att_name->attribute_name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(isset($atributes->options[0]->option_id) && $atributes->options[0]->option_id != '' && $atributes->attributes_type != 1){
                                            
                                            $at_op_id = $atributes->options[0]->option_id;
                                            $att_op_name = $this->db->where('att_op_id', $at_op_id)->get('attr_options')->row();
                                            echo $att_op_name->option_name;

                                        }else{
                                            echo $atributes->attribute_value;
                                        }    
                                    ?>
                                </td>
                            </tr>

                            <?php
                            if ($atributes->options[0]->option_type == 3 || $atributes->options[0]->option_type == 5 || $atributes->options[0]->option_type == 2 || $atributes->options[0]->option_type == 4 || $atributes->options[0]->option_type == 6){
                                if (sizeof($atributes->opop) > 0) {
                                    foreach ($atributes->opop as $secondLevelOpts) {
                                        $secondLevelOpt = $this->db->where('op_op_id', $secondLevelOpts->op_op_id)->get('attr_options_option_tbl')->row();
                                        $secondLevelOptName = $secondLevelOpt->op_op_name;

                                        $secondLevelOptValue = "";
                                        if ($secondLevelOpt->type == 1 || $secondLevelOpt->type == 0 || $secondLevelOpt->type == 2) {
                                            $secondLevelOptValue = $secondLevelOpts->op_op_value;   
                                        }

                                        // For multioption with multiselect : START
                                        if ($atributes->options[0]->option_type == 4 && $secondLevelOpt->type == 6) {
                                            $secondLevelOptValue = "";
                                            foreach ($atributes->opopop as $thirdLevelOpts) {
                                                if(isset($thirdLevelOpts->op_op_op_id) && $thirdLevelOpts->op_op_op_id != ''){
                                                $thirdLevelOpt = $this->db->where('att_op_op_op_id', $thirdLevelOpts->op_op_op_id)->get('attr_options_option_option_tbl')->row();
                                                $secondLevelOptValue .= $thirdLevelOpt->att_op_op_op_name.",";
                                                }
                                            }
                                            $secondLevelOptValue = rtrim($secondLevelOptValue, ',');
                                        }
                                        // For multioption with multiselect : END

                                        // For fraction value type : START
                                        if ($atributes->options[0]->option_type == 5) {
                                            $get_fraction_id = explode(" ", $secondLevelOptValue);
                                            if(isset($get_fraction_id[1]) && $get_fraction_id[1] != ''){
                                                $sub_fraction = $this->db->where('id', $get_fraction_id[1])->get('width_height_fractions')->row();
                                                $fraction_option = isset($sub_fraction->fraction_value) ? $sub_fraction->fraction_value : ''; 
                                                $secondLevelOptValue =  $get_fraction_id[0]." ".$fraction_option;
                                            }    
                                        }
                                        // For fraction value type : END

                                        if ($atributes->options[0]->option_type == 2 || $atributes->options[0]->option_type == 6) {
                                            ?>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>   
                                                <td><?php echo $secondLevelOptName; ?></td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>   
                                                <td><?php echo $secondLevelOptName; ?></td>
                                                <td><?php echo $secondLevelOptValue; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                }
                            } else if ($atributes->options[0]->option_type == 1) {
                                ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td><?php echo $atributes->options[0]->option_value ; ?></td>
                                </tr>
                                <?php
                            } ?>
                            <!-- New View : END -->
                     <?php } ?>
                  </tbody>
                  <tfoot>
                     <tr>
                        <td colspan="2">Notes:</td>
                        <td><b><?= $items->notes ?></b></td>
                     </tr>
                  </tfoot>
                  <?php
                     }
                     }
                     ?>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>