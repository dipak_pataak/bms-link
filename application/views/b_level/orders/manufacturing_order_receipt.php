<?php $currency = $company_profile[0]->currency; ?>
<style type="text/css">
.rotate90 {
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
    margin-top: 50px;
}

@media print {

    .noprint {
        content: "" !important;
        display: none !important;
        visibility: hidden !important;
    }

}


@media (min-width: 768px) {
    .modal-xl {
        width: 90%;
        max-width: 1200px;
    }
}

.modal .modal-header {
    padding: 1rem !important;
}

.modal .modal-body,
.modal .modal-footer {
    padding: 0.5rem !important;
}

div,
p {
    font-size: 11px !important;
}

table tr th {
    font-size: 12px !important;
}

.print_header {
    padding-left: 20px;
    padding-right: 20px;
}
</style>


<div id="right">
    <div class="box" id="">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>

<?php $split_data_detail = $this->db->select('*')->from('b_to_b_level_quatation_tbl')->where('clevel_order_id',$orderd->order_id)->get()->row(); 
$manufacture_split_data_detail = $this->db->select('order_id')->from('manufacturing_b_level_quatation_tbl')->where('clevel_order_id',$orderd->order_id)->get()->row(); 
?>
        <h5>
            <span class="right_buttons">Order info</span>
            <div class="pull-right">
                <?php if ($orderd->order_stage != 4) { ?>
                    <a href="<?= base_url('wholeseler_go_manufacturing/'); ?><?= $orderd->order_id ?>"
                    class="btn btn-success">Go Manufacturing</a>
                <?php } ?>
                <?php if (isset($check_re_order_status) && $check_re_order_status != 1) { ?>
                    <a onclick="open_re_order_modal()" href="#" class="btn btn-success">Split order</a>
                <?php }else{ ?> 
                    <a href="<?php echo base_url(); ?>manufacturing-receipt/<?php echo $manufacture_split_data_detail->order_id; ?>" class="btn btn-success"><?= $company_profile[0]->company_name." Manufacture" ; ?></a>
                    <a href="<?php echo base_url(); ?>wholesaler-catalog-receipt/<?php echo $split_data_detail->order_id; ?>" class="btn btn-success">Send Order To <?php echo $other_company_profile[0]->company_name; ?></a>
                    <a href="<?php echo base_url(); ?>catalog-shipment/<?php echo $split_data_detail->order_id; ?>" class="btn btn-success">Go Shipment</a>
                    
                <?php } ?>

                <?php if ($shipping == '' && $orderd->order_stage == 4 && $check_re_order_status != 1) { ?>
                <a href="<?= base_url('b_level/order_controller/shipment/'); ?><?= $orderd->order_id ?>"
                    class="btn btn-success">Go Shipment</a>
                <?php } ?>

                <?php if ($orderd->order_stage != 2) { ?>
                <a href="<?= base_url('b_level/order_controller/order_view/'); ?><?= $orderd->order_id ?>"
                    class="btn btn-success">Receive Payment</a>
                <?php } ?>

                <button type="button" class="btn btn-success" onclick="printContent('printableArea')">Print</button>
            </div>
        </h5>

        <div class="separator mb-3"></div>

        <div id="printableArea">
            <div class="row print_header">
                <div class="form-group col-md-6">
                    <div class="">
                        <img
                            src="<?php echo base_url('assets/b_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">
                    </div>
                    <p><?= $company_profile[0]->company_name; ?></p>
                    <p><?= $company_profile[0]->address; ?></p>
                    <p><?= $company_profile[0]->city; ?>,
                        <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                        <?= $company_profile[0]->country_code; ?></p>
                    <p><?= $company_profile[0]->phone; ?></p>
                    <p><?= $company_profile[0]->email; ?></p>
                </div>


                <div class="form-group col-md-6">

                    <div class="">
                        <table class="table-bordered">

                            <tr class="text-center">
                                <td>Order Date</td>
                                <td class="text-right"><?= date_format(date_create($orderd->order_date), 'M-d-Y') ?>
                                </td>
                            </tr>

                            <tr class="text-center">
                                <td>Order Id</td>
                                <td class="text-right"><?= $orderd->order_id ?></td>
                            </tr>

                            <tr class="text-center">
                                <td>Sidemark</td>
                                <td class="text-right"><?= ($orderd->side_mark != '') ? $orderd->side_mark : $customer->side_mark; ?></td>
                            </tr>



                            <?php if ($shipping != NULL) { ?>
                            <tr class="text-center">
                                <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                                <td class="text-right"> <?= $shipping->track_number ?> </td>
                            </tr>

                            <?php } ?>

                            <tr class="text-center">
                                <td>Barcode</td>
                                <td class="text-right"> <?php
                                    if ($orderd->barcode != NULL) {
                                        echo '<img src="' . base_url() . $orderd->barcode . '" width="400px;" height="50px;"/>';
                                    }
                                    ?></td>


                            </tr>



                        </table>
                    </div>
                </div>
            </div>

            <h5 style="margin-left: 15px;">Order Details</h5>
            <div class="separator mb-3"></div>
            <div class="px-3" id="cartItems123">
                <div class="table-responsive">
                    <table class="datatable2 table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <?php if (isset($check_re_order_status) && $check_re_order_status != 1): ?>
                                <th>Select</th>
                                <?php endif;?>
                                <th>#</th>
                                <th>Description</th>
                                <th>Qty</th>

                                <!-- <th>List</th>
                                <th>Discount(%)</th> -->
                                <th>Price</th>
                                <th>Notes</th>
                                <th class="noprint">Action</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php $i = 1; ?>

                            <?php
                            foreach ($order_details as $items):

                                if ($items->manufactured_scan_date && $items->shipping_scan_date) {
                                    $background_class = "color-orange";
                                } else if ($items->manufactured_scan_date) {
                                    $background_class = "color-green";
                                } else {
                                    $background_class = '';
                                }


                                $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                                $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                                ?>

                    <?php 
                        $user_id = $this->session->userdata('user_id');
                        $link_check = $this->db->select("*")
                                 ->from('b_user_catalog_products')
                                 ->join('b_user_catalog_request', 'b_user_catalog_products.request_id = b_user_catalog_request.request_id')
                                 ->where('b_user_catalog_products.product_link', $items->product_id)
                                 ->where('b_user_catalog_products.requested_by', $user_id)
                        ->get()->result();
                    ?>

                            <tr class="<?php echo $background_class; ?>">
                                <?php if (isset($check_re_order_status) && $check_re_order_status != 1): ?>
                                <td>
                                    <?php if (!empty($link_check)) { ?>
                                        <?php //if (isset($items->link_status) && $items->link_status == 1) { ?>
                                        <input type="checkbox" name="Id_List[]" id="Id_List[]"
                                            value="<?= $items->row_id; ?>" class="checkbox_list">
                                        <?php //} ?>
                                    <?php } ?>
                                </td>
                                <?php endif;?>
                                <td><?= $i ?></td>

                                <td width="400px;">
                                    <strong><?= $items->product_name; ?></strong><br />
                                    <?= $items->pattern_name; ?><br />
                                        W <?= $items->width; ?> <?= @$width_fraction->fraction_value ?> <?= strtoupper($company_unit); ?>,
                                        H <?= $items->height; ?> <?= @$height_fraction->fraction_value ?> <?= strtoupper($company_unit); ?>,
                                    <?= $items->color_number; ?>
                                    <?= $items->color_name . ', '; ?>

                                    <?php
                                        if (isset($selected_rooms[$items->room])) {
                                            echo $items->room . ' ' . $selected_rooms[$items->room];
                                            $selected_rooms[$items->room] ++;
                                        } else {
                                            echo $items->room . ' 1';
                                            $selected_rooms[$items->room] = 2;
                                        }
                                        ?>

                                </td>
                                <td><?= $items->product_qty; ?></td>
                               <!--  <td><?= $currency; ?><?= $items->list_price ?> </td>
                                <td><?= $items->discount ?> %</td> -->
                                <td><?= $currency; ?><?= $items->unit_total_price ?> </td>
                                <td><?= $items->notes ?></td>

                                <td class="noprint text-right">
                                    <!--<a href="<?php // echo base_url();    ?>order-edit/<?php // echo $items->row_id;    ?>/0" class="btn btn-success btn-sm" title="View"><i class="fa fa-eye"></i></a>-->
                                    <a href="#" data-row_id="<?php echo $items->row_id; ?>"
                                        data-order_id="<?php echo $items->order_id; ?>"
                                        class="btn btn-success btn-sm view_order_btn" title="View"><i
                                            class="fa fa-eye"></i></a>

                                    <?php if ($this->permission->check_label('manage_order')->update()->access() && $orderd->order_stage == 1 || 1) { ?>
                                    <!-- <a href="<?php echo base_url(); ?>order-edit/<?php echo $items->row_id; ?>/1" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-edit"></i></a> -->

                                   <!--  <a class="btn btn-info btn-sm"
                                        onclick="showEditModal(<?php echo $items->row_id ?>);"><i
                                            class="fa fa-edit"></i></a>

                                    <a href="<?php echo base_url(); ?>order-delete/<?php echo $items->row_id; ?>"
                                        class="btn btn-danger  btn-sm" title="Delete"><i class="fa fa-trash"></i></a> -->
                                    <?php } ?>

                                </td>

                            </tr>

                            <?php $i++; ?>

                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <!-- <table class="datatable2 table table-bordered table-hover mb-4">

                        <thead>
                            <tr>
                                <?php if ($shipping != NULL) { ?>
                                <th>Shipping cost</th>
                                <?php } ?>

                                <th>Sub-Total</th>
                                <th>Installation Charge</th>
                                <th>Other Charge</th>
                                <th>Misc</th>
                                <th>Discount</th>
                                <th>Grand Total</th>
                                <th>Deposit</th>
                                <th>Due </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if ($shipping != NULL) { ?>
                            <td class="text-right"> <?= $currency; ?> <?= $shipping->shipping_charges ?> </td>
                            <?php } ?>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->subtotal; ?> </td>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->installation_charge ?> </td>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->other_charge ?></td>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->misc ?> </td>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->invoice_discount ?></td>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->grand_total ?>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->paid_amount ?>
                            <td class="text-right"> <?= $currency; ?><?= $orderd->due ?>
                        </tbody>

                    </table> -->

                    <?php
                    if ($shipping != NULL) {
                        if ($shipping->method_name == 'UPS') {
                            ?>

                    <div class="col-lg-6 offset-lg-4 noprint">
                        <!--<a href="#"  data-toggle="modal" data-target="#order_logo">-->
                        <a href="<?php echo base_url(); ?>order-logo-print/<?php echo $this->uri->segment(4); ?>"
                            target="_new">
                            <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="" height='250'>
                        </a>
                    </div>


                    <!-- Modal -->
                    <div class="modal fade" id="order_logo" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">

                                <div class="modal-body" style="height: 455px;">
                                    <div class="col-md-5" id="printableImgArea">
                                        <img src="<?php echo base_url() . $shipping->graphic_image ?>" class="rotate90"
                                            style="-webkit-transform: rotate(90deg); margin-top: 90px; height: 250px;">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-warning " href="#"
                                        onclick="printDiv('printableImgArea')">Print</a>
                                    <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                    <a href="" class="btn btn-danger">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>

                </div>
            </div>

            <div class="col-lg-6 offset-lg-6 text-right">
                <button type="button" class="btn btn-success" onclick="printContent('printableArea')">Print</button>
            </div>

        </div>
    </div>
</div>



<div class="modal fade" id="re_order_modal" role="dialog">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Split order</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="split_order_form" action="<?php echo base_url('/wholesaler-re-order'); ?>" method="post">
                    <div class="form-group row">
                        <label class="col-xs-2 control-label">Select b user</label>
                        <div class="col-xs-6">
                            <!--  <select name="b_user_id" class="form-control">
                            <?php /* if(isset($b_user_data) && !empty($b_user_data)) { */ ?>
                            <?php /* /*foreach ($b_user_data as $key => $value) { */ ?>
                                        <option value="<?php /* /*echo $value->id; */ ?>"><?php /* echo $value->first_name . ' ' . $value->last_name; */ ?></option>
                            <?php /* /*}

                              } */ ?>
                            </select>-->
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xs-2 control-label">Select Product</label>
                        <div class="col-xs-6">
                            <select name="re_order_product_data[]" id="re_order_product_data"
                                class="form-control selectpicker" multiple>
                                <?php /* if(isset($re_order_product_data) && !empty($re_order_product_data)) { */ ?>
                                <?php /* foreach ($re_order_product_data as $key => $value) { */ ?>
                                <option value="<?php /* echo $value->product_id; */ ?>">
                                    <?php /* echo $value->product_name; */ ?></option>
                                <?php /* }
                                  }
                                 */ ?>
                            </select>
                        </div>
                    </div>

                    <!--  <input type="hidden" name="clevel_order_id" value="<?php /* if(isset($clevel_order_id)) { echo $clevel_order_id; } */ ?>">-->
                    <input type="hidden" name="order_id" value="<?php
                    if (isset($orderd->order_id)) {
                        echo $orderd->order_id;
                    }
                    ?>">



                    <div class="form-group row">
                        <label class="col-xs-2 control-label"></label>
                        <div class="col-xs-6">
                            <button class="btn-sm btn-success" id="closeModal">Split order</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<!-- Modal -->
<div id="Modal-Manage-Order" class="modal fade modal-place-order" role="dialog">
    <div class="modal-dialog modal-xl" style="margin: auto; max-width: 900px; ">
        <div class="modal-content" style="width: 1024px;">
            <div class="modal-header">
                <h4 class="modal-title"><span class="action_type"></span> Order</h4>
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer" style="padding-top:20px!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="editOrderModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="min-width: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row form-area">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="submitUpdateRequest" class="btn btn-default"
                    data-dismiss="modal">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function showEditModal(rowId) {
    $.ajax({
        url: "<?php echo base_url(); ?>b_level/Order_controller/get_new_order_edit/" + rowId + "/1",
        success: function(result) {
            $('#editOrderModal').modal('show');
            $('#editOrderModal .modal-body .form-area').html('');
            $('#editOrderModal .modal-body .form-area').html(result);
            $('#cartbtn').remove();
            $('#clrbtn').remove();
            $("#AddToCart").attr('action',
                '<?php echo base_url(); ?>b_level/Order_controller/update_order_item_details_frommanage'
            );
            $('#AddToCart').attr('id', 'UpdateOrderItem');
            
            if($('.sticky_container .sticky_item').length > 0){
                $('.sticky_container .sticky_item').theiaStickySidebar({
                    additionalMarginTop: 110
                });
            }
                
            console.log(rowId);
        }
    });
}


$('#submitUpdateRequest').click(function() {
    $("#UpdateOrderItem").submit();
});


// submit form and add data
$('body').on('submit', "#UpdateOrderItem", function(e) {
    e.preventDefault();
    var submit_url = "<?php echo base_url(); ?>b_level/Order_controller/update_order_item_details_frommanage";
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $(this).serialize(),
        success: function(res) {
            location.reload();
        }
    });
});








//print a div
function printContent(el) {

    $('.noprint').hide();

    $('body').css({
        "background-color": "#fff"
    });
    var restorepage = $('body').html();
    var printcontent = $('#' + el).clone();
    $('body').empty().html(printcontent);
    window.print();
    $('body').html(restorepage);
    location.reload();

}

function printDiv(divName) {

    $('body').css({
        "background-color": "#fff"
    });
    $('.noprint').hide();
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    // document.body.style.marginTop="-45px";
    window.print();
    document.body.innerHTML = originalContents;
}

function open_re_order_modal() {
    $('#re_order_product_data').empty();
    var row_id_arr = [];
    $.each($("input[name='Id_List[]']:checked"), function() {
        $('#re_order_product_data').append(`<option value="` + $(this).val() + `" selected>` + $(this).val() +
            `</option>`);
        row_id_arr.push($(this).val());
    });
    if (row_id_arr.length == 0) {
        swal('Select Item');
    } else {


        $('#split_order_form').submit();

        /* $.ajax({
         url: "b_level/order_controller/get_b_split_order_view",
         type: 'POST',
         data: {
         row_id_arr: row_id_arr
         },
         success: function (r) {
         $('#split_order_html').html(r);
         $('#re_order_modal').modal('show');
         }
         });*/
    }
    /* $('#re_order_modal').modal('show');*/
}


function viewOrderHtml(order_id, row_id) {
    var orderid = order_id;
    var rowid = row_id;
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('b_level/Order_controller/getOrderData') ?>",
        data: {
            order_id: orderid,
            row_id: rowid

        },
        success: function(data) {

            $('#Modal-Manage-Order .action_type').text('View');
            $('#Modal-Manage-Order .modal-body').html(data);
            //                $('#Modal-Manage-Order #customer_id').val(quote_cutomer_id);
            //                $('#Modal-Manage-Order #customer_id').change();
            $('#Modal-Manage-Order').modal('show');

        },
        error: function() {
            swal('error');
        }
    });
}
$(".view_order_btn").click(function() {
    var row_id = $(this).data('row_id');
    var order_id = $(this).data('order_id');
    viewOrderHtml(order_id, row_id);
});
</script>
