<?php $package=$this->session->userdata('packageid'); ?>
<?php $currency = $company_profile[0]->currency; ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5 >Order List</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Order List';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
                <!--<a href="new-order" class="btn btn-success btn-sm mt-1">Add</a>-->
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
       <?php $menu_permission= b_access_role_permission(8); ?>
        <!-- end box / title -->

        <p class="px-3">

            <div class="col-md-4 pull-right">
                <form action="<?= base_url('b_level/order_controller/multiple_order_view'); ?>" id="make_payment_form" method="post">
                    <input type="hidden" name="payment_multiple" id="hidden_multi_order_id">
                    <?php if(!empty($package)){ ?>
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                    <input type="button" name="" class="btn btn-primary pull-right" id="make_multi_payment" value="Receive Payment">
                    <?php }?>
                    <?php }?>
                </form>
            </div>

            <div class="col-md-8">
                <button class="btn btn-primary default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Filter
                </button>
                <?php if(!empty($package)){ ?>
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="new-order" class="btn btn-success btn-sm">Add</a>
                 <?php } ?>
                 <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete_order(document.recordlist)">Delete</a>
                <?php } ?>

                <?php } ?>

            </div>
        </p>

        <div class="collapse px-3 mb-3" id="collapseExample">

            <div class="border px-3 pt-3">

                <form class="form-horizontal" action="<?= base_url('wholesaler-order-list') ?>" method="post">

                    <fieldset>

                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" name="customer_id">
                                    <option value="">--Select Company--</option>
                                    <?php foreach ($customers as $c) { ?>
                                        <option value="<?= $c->customer_id ?>" <?php if ($customerid == $c->customer_id) { echo 'selected'; } ?> >
                                            <?php if($c->company != '') { 
                                                echo $c->company; 
                                            }else{
                                                echo $c->first_name; ?> <?= $c->last_name;
                                            } ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <input type="text" name="side_mark" id="side_mark" class="form-control mb-3" value="<?php echo $side_mark; ?>" placeholder="Side Mark">
                            </div>

                            <div class="col-md-3">
                                <input type="text" name="order_date" id="order_date" class="form-control datepicker mb-3" value="<?php echo $order_date; ?>" placeholder="Order Date">
                            </div>


                            <div class="col-md-3">
                                <select class="form-control" name="order_stage">
                                    <option value="">--Select Status--</option>

                                    <option value="1" <?php
                                                        if ($order_stage == 1) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Quote</option>

                                    <option value="2" <?php
                                                        if ($order_stage == 2) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Paid</option>

                                    <option value="3" <?php
                                                        if ($order_stage == 3) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Partially Paid</option>

                                    <option value="4" <?php
                                                        if ($order_stage == 4) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Manufacturing</option>


                                    <option value="5" <?php
                                                        if ($order_stage == 5) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Shipping</option>

                                    <option value="6" <?php
                                                        if ($order_stage == 6) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Cancelled</option>


                                    <option value="7" <?php
                                                        if ($order_stage == 7) {
                                                            echo 'selected';
                                                        }
                                                        ?>>Delivered</option>

                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>

            </div>

        </div>


        <!-- end box / title -->
        <div class="px-3">
            <?= @$links; ?>
            <form name="recordlist" id="mainform" method="post" action="<?php echo base_url('b_level/Order_controller/manage_action') ?>">
                <input type="hidden" name="action">
                <input type="hidden" name="mul_order_delete" id="mul_order_delete">

                <div class="table-responsive">
                    <table class="table table-bordered table-hover text-center">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id="master"></th>
                                <th>Order/Quote No</th>
                                <th>Company Name </th>
                                <th>Side mark</th>
                                <th>Order date</th>
                                <th>Order Due</th>
                                <th>Status</th>
                                <?php if(!empty($package)){ ?>
                                <th>Action</th>
                                <?php }?>

                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            if (!empty($orderd)) {
                                foreach ($orderd as $key => $value) {

                                    $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                                    $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();

                                    if ($value->order_stage == 8) {
                                        $background_class = "color-green";
                                    } else if ($value->order_stage == 9) {
                                        $background_class = "color-orange";
                                    } else {
                                        $background_class = '';
                                    }
                                    $customer_user_id = $this->db->where('customer_id', $value->customer_id)->get('customer_info')->row()->customer_user_id;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php if ($value->order_stage != 2) { ?>
                                                <input type="checkbox" name="order_List" value="<?= $value->order_id; ?>" data-customer_id="<?= $value->customer_id; ?>" class="sub_chk">
                                            <?php } ?>
                                        </td>

                                        <td><?= $value->order_id; ?></td>
                                        <td>
                                            <?php
                                                    if ($value->level_id != 1) {
                                                        $sql = "SELECT * FROM company_profile WHERE user_id = '$customer_user_id'";
                                                        $sql_result = $this->db->query($sql)->result();
                                                        echo $sql_result[0]->company_name;
                                                    } else {
                                                        echo $value->customer_name;
                                                    }
                                                    ?>
                                        </td>
                                        <td><?= $value->side_mark; ?></td>

                                        <td>
                                            <?= date_format(date_create($value->order_date), 'M-d-Y'); ?>
                                        </td>

                                        <td><?= $currency ?><?= $value->due ?></td>

                                        <td>
                                            <?php if ($this->permission->check_label('order')->update()->access()) { ?>
                                                <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')" <?= ($value->order_stage == 11 ? 'disabled="true"' : '') ?> >
                                                    <?php if(!empty($package)){ ?>
                                                    <option value="">--Select--</option>
                                                    <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                                    <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                                    <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                    <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Manufacturing</option>
                                                    <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                                    <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                    <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                                    <option value="8" <?= ($value->order_stage == 8 ? 'selected' : '') ?>>Ready to be Shipped</option>
                                                    <option value="9" <?= ($value->order_stage == 9 ? 'selected' : '') ?>>In Transit</option>
                                                    <option value="10" <?= ($value->order_stage == 10 ? 'selected' : '') ?>>Confirmation</option>
                                                    <option value="11" <?= ($value->order_stage == 11 ? 'selected' : '') ?>>Manufacturing / Split</option>
                                                    <?php } ?>
                                                </select>
                                            <?php } ?>
                                        </td>
                                        <?php if(!empty($package)){ ?>

                                        <td class="width_140 text-right">
                                            <?php if ($this->permission->check_label('order')->read()->access()) { ?>
                                                <?php if($menu_permission['access_permission'][0]->can_access==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                                <?php if ($value->order_stage == 4) { ?>
                                                    <a href="<?= base_url('wholesaler-manufacture-quotation/') . $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="Manufacturing"> <i class="fa fa-cogs"></i> </a>
                                                <?php }
                                                            if ($value->order_stage == 7) { ?>
                                                    <a href="customer-order-return/<?= $value->order_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>
                                                <?php }
                                                            if ($value->level_id != 1 && 0) { ?>
                                                    <a href="<?= base_url('wholesaler-retailer-receipt/') ?><?= $value->order_id . '/' . $value->level_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                                <?php } else { ?>
                                                    <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="fa fa-eye"></i> </a>
                                                <?php } ?>
                                                <?php } ?>


                                            <?php } ?>

                                            <?php if ($this->permission->check_label('order')->delete()->access()) { ?>
                                                <!-- <a href="order-edit/<?= $value->order_id; ?>" class="btn btn-warning default btn-sm" ><i class="fa fa-pencil"></i></a> -->
                                            <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                                <a href="delete-order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                            <?php } ?>
                                            <?php } ?>
                                        </td>
                                        <?php }?>

                                    </tr>

                                <?php
                                    }
                                } else {
                                    ?>

                                <div class="alert alert-danger"> There have no order found..</div>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </form>
            <?= @$links; ?>
        </div>
    </div>
</div>
<!-- end content / right -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#make_multi_payment").attr("disabled", true);

        $('.sub_chk, #make_multi_payment').on('click', function() {
            $("#make_multi_payment").attr("disabled", false);
            var order_ids = '';
            var customer_ids = [];
            var checked_customer_ids = [];

            $('[name="order_List"]').each(function(i, e) {
                if ($(e).is(':checked')) {
                    var comma = order_ids.length === 0 ? '' : ',';
                    order_ids += (comma + e.value);
                    customer_id = $(this).data('customer_id');

                    if (jQuery.inArray(customer_id, customer_ids) || customer_ids.length == 0) {
                        customer_ids.push(customer_id);
                    }

                    checked_customer_ids.push(customer_id);
                }
                $("#hidden_multi_order_id").val(order_ids);
            });

            if (customer_ids.length == 1) {
                // $("#make_payment_form").submit(); 
            } else {
                swal({
                    title: "Alert!",
                    text: "Please select same Customer..!",
                });
                $("#make_multi_payment").attr("disabled", true);
            }
        });

        $('#master').on('click', function() {
            if ($(this).prop("checked") == true) {
                var order_ids = '';
                $(".sub_chk").trigger('click');
                $('[name="order_List"]').each(function(i, e) {
                    if ($(e).is(':checked')) {
                        var comma = order_ids.length === 0 ? '' : ',';
                        order_ids += (comma + e.value);
                    }
                });

                $("#hidden_multi_order_id").val(order_ids);

            } else {
                $("#hidden_multi_order_id").val("");
            }
        });
    });

    $('#make_multi_payment').on('click', function() {
        var order_ids = '';
        var customer_ids = [];
        var checked_customer_ids = [];
        $('[name="order_List"]').each(function(i, e) {
            if ($(e).is(':checked')) {
                var comma = order_ids.length === 0 ? '' : ',';
                order_ids += (comma + e.value);
                customer_id = $(this).data('customer_id');

                if (jQuery.inArray(customer_id, customer_ids) || customer_ids.length == 0) {
                    customer_ids.push(customer_id);
                }

                checked_customer_ids.push(customer_id);
            }
        });

        if (customer_ids.length == 1) {
            $("#make_payment_form").submit();
        } else {
            swal({
                title: "Alert!",
                text: "Please select same Customer..!",
            });
        }
    });

    $(document).on("change", ".sub_chk", function() {
        var all_checked = $("input[type='checkbox'].sub_chk");

        var values = new Array();
        $.each($("input[type='checkbox']:checked"), function() {
            values.push($(this).val());
        });
        $('#check_id').val(values);

        if (all_checked.length == all_checked.filter(":checked").length) {
            $("#master").prop('checked', true);
        } else {
            $("#master").prop('checked', false);
        }
    });

    $('#master').on('change', function(e) {
        if ($(this).is(':checked', true)) {
            $(".sub_chk").prop('checked', true);
            var values = new Array();
            $.each($("input[class='sub_chk']:checked"), function() {
                values.push($(this).val());
            });
            $('#check_id').val(values);
        } else {
            $(".sub_chk").prop('checked', false);
        }
    });


    function setOrderStage(stage_id, order_id) {


        if (stage_id !== '') {


            if (stage_id === '2' || stage_id === '3') {
                window.location.href = 'b_level/order_controller/order_view/' + order_id;
            }

            if (stage_id === '5') {
                window.location.href = 'b_level/order_controller/shipment/' + order_id;
            }


            if (stage_id === '7' || stage_id === '6' || stage_id === '4' || stage_id === '1') {

                //swal(stage_id);

                $.ajax({
                    url: "b_level/order_controller/set_order_stage/" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function(r) {

                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function() {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });
            }

            // For send confirmation email
            if (stage_id === '10') {
                $.ajax({
                    url: "b_level/order_controller/send_confirmation_email/" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function(r) {
                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function() {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });
            }
        }
    }

    function action_delete_order(frm) {
        with(frm) {
            var flag = false;
            str = '';
            field = document.getElementsByName('order_List');
            for (i = 0; i < field.length; i++) {
                if (field[i].checked == true) {
                    flag = true;
                    break;
                } else
                    field[i].checked = false;
            }
            if (flag == false) {
                swal("Please select atleast one record");
                return false;
            }
        }
        if (confirm("Are you sure to delete selected records ?")) {
            frm.action.value = "action_delete";
            frm.mul_order_delete.value = $("#hidden_multi_order_id").val();
            frm.submit();
            return true;
        }
    }
</script>
