<?php $currency = $company_profile[0]->currency; ?>


<style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }
</style>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/b_level/card/card_style.css">

<div id="right">

    <div class="box">

        <div class="row">
            
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
            
        </div>

        <h5>Order Payment Info</h5>

        <div class="separator mb-3"></div>

        <div class="">
            <div class="form-group col-md-6 pull-right">

                <?php
                //if ($orderd->due > 0) {
                ?>

                    <?php echo form_open('b_level/invoice_receipt/multiple_order_payment_update', array('id'=>'MyForm','target'=>'_blank','onSubmit'=>'return PymentValidation()') ) ?>

                    <table class="table table-bordered ">

                        <?php 
                           $pval = sprintf ("%.2f", $orderd->paid_amount);
                        ?>
                        
                        <!-- <input type="hidden" value="<?= @$orderd->paid_amount; ?>" name="paid_amount" id="paid_amount"> -->
                        <!-- <input type="hidden" value="<?= ($orderd->grand_total) ?>" name="grand_total" id="grand_total">
                        <input type="hidden" value="<?= $orderd->customer_no ?>" name="customer_no" > -->
                        <input type="hidden" value="<?= $orderd->customer_id ?>" name="customer_id" id="customer_id"> 
                        <input type="hidden" value="<?= $orderd->order_ids ?>" name="order_id">
                        <!-- <input type="hidden" value="<?= $orderd->due; ?>" id="due_amm">
                        <input type="hidden" value="<?= @$orderd->clevel_order_id; ?>" id="c_level_order_id"> -->

                        <tr>
                            <td>Paid Amount (<?= $currency ?>)</td>
                            <td><input type="text" name="paid_amount" onkeyup="calDuePaid(); isNumeric(this.id);" onchange="calDuePaid()" value="<?=$pval?>" id="paid_amount" placeholder="0" class="form-control text-right" required ></td>
                        </tr>

                        <!-- <tr>
                            <td>Due (<?= $currency ?>)</td>
                            <td><input type="text" name="due" id="due" class="form-control text-right" value="0" readonly=""></td>
                        </tr> -->


                        <tr>
                            <td>Payment method</td>
                            <td>
                                <select name="payment_method" class="form-control" onchange="setCard(this.value)">
                                    <option value="cash">Cash</option>
                                    <option value="card">Card</option>
                                    <option value="check">Check</option>
                                    <option value="paypal">Paypal</option>
                                </select>
                            </td>
                        </tr>

                    </table>

                    <div class="card_method" style="display: none;">

                        <div id="paymentForm">

                            <ul>
                                <li>
                                    <input type="text" name="card_number" placeholder="xxxx xxxx xxxx xxxx" id="card_number">
                                </li>

                                <li class="vertical">

                                    <ul>
                                        <li>
                                            <label for="expiry_month">Month</label>
                                            <input type="text" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month">
                                        </li>
                                        
                                        <li style="margin-left: 5px;">
                                            <label for="expiry_year">Year</label>
                                            <input type="text" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year">
                                        </li>

                                        <li>
                                            <label for="cvv">CVV</label>
                                            <input type="text" name="cvv"  placeholder="xxx" maxlength="4" id="cvv">
                                        </li>

                                    </ul>

                                </li>

                                <li>
                                    <label for="name_on_card">Name on card</label>
                                    <input type="text" name="card_holder_name" placeholder="Name on Card" id="name_on_card">
                                </li>
                            </ul>

                        </div>


                        <table class="table table-bordered ">

                        </table>

                    </div>


                    <div class="check_method" style="display: none;">

                        <table class="table table-bordered ">

                            <tr id="check_area">
                                <td>Check Number</td>
                                <td><input type="text" name="check_number" id="check_number" class="form-control "></td>
                            </tr>

                            <tr id="check_area2">
                                <td>Check Image</td>
                                <td><input type="file" name="check_image" id="check_image" class="form-control "></td>
                            </tr>
                        </table>
                    </div>



                    <div class="col-lg-6 offset-lg-6 text-right" style="margin-top: 10px">
                        <button type="submit" class="btn btn-success" >Submit</button>
                    </div>
                    <?php echo form_close(); ?> 

                <?php // } ?>
            </div>
        </div>


        <h5>Order Payment Details</h5>

        <div class="separator mb-3"></div>

        <div class="px-3" id="cartItems">

            <table class="datatable2 table table-bordered table-hover mb-4">

                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>SideMark/CustomerId</th>
                        <th>Invoice No</th>
                        <th>Amount</th>
                        <th>Due</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($order_details as $items): ?>
                        <tr>
                            <td><?= $items->customer_name; ?></td>
                            <td><?= $items->side_mark.'/'.$items->customer_id; ?></td>
                            <td><?= $items->order_id; ?></td>
                            <td><?= $items->grand_total; ?></td>
                            <td><?= $items->due; ?></td>
                            <td>
                                
                                <form action="<?= base_url('b_level/order_controller/multiple_order_view'); ?>" method="post" >
                                    <input type="hidden" name="payment_multiple" class="hidden_multi_order_id">
                                    <button type="button" class="btn btn-danger btn-sm make_multi_payment text" data-ord_id="<?php echo $items->order_id; ?>"><i class=" fa fa-trash"></i></button>
                                </form>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php if (isset($shipping->graphic_image) && $shipping->graphic_image != NULL) { ?>

                <div class="row">

                    <div class="col-lg-6 offset-lg-4">
                        <a href="<?php echo base_url(); ?>order-logo-print/<?php echo $this->uri->segment(2); ?>" target='_new'>
                            <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="" height='250' >
                        </a>
                    </div>


                    <!-- Modal -->
                    <div class="modal fade" id="order_logo" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-body" style="">
                                    <div class="col-md-5" id="printableImgArea">
                                        <img src="<?php echo base_url() . $shipping->graphic_image ?>" class="rotate90" style="-webkit-transform: rotate(90deg); margin-top: 90px; height: 250px;"> 
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a  class="btn btn-warning " href="#" onclick="printDiv('printableImgArea')">Print</a>
                                    <a href="" class="btn btn-danger">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>

        </div>

    </div>
</div>

<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>




<script type="text/javascript">


    $(document).ready(function () {
        var grand_total = parseFloat($('#grand_total').val());
        $('#due').val(grand_total);
        calDuePaid();
    });

    $(document).ready(function () {
        var order_ids = [];
        $('.make_multi_payment').click(function(){
            var clicked_order = $(this).data('ord_id');
            $('.make_multi_payment').each(function(){
                if(clicked_order != $(this).data('ord_id')){
                    order_ids.push($(this).data('ord_id'));
                }
            });
            var string_order_ids = order_ids.toString();
            var prev = $(this).prev().val(string_order_ids);
            $(this).closest("form").submit();
        });
    });



    // function calDuePaid() {

    //     var due = parseFloat($('#due').val());
    //     var due_amm = parseFloat($('#due_amm').val());
    //     var paid_amount = parseFloat($('#paid_amount').val());

    //     var pval = '<?=$pval?>';
    //     if(paid_amount<pval){
    //         swal("Can't pay less than "+pval);
    //         $('#paid_amount').val(pval);
    //     }
        
    //     var pval = '<?=$pval?>';
    //     if(due<pval){
    //         swal("Can't pay more than "+pval);
    //         $('#paid_amount').val(pval);
    //     }

    //     if (isNaN(paid_amount)) {
    //         var paid_amount = 0;
    //     } else {
    //         var paid_amount = parseFloat($('#paid_amount').val());
    //     }

    //     var due = (due_amm - paid_amount);
    //     $('#due').val(due.toFixed(2));

    // }


function PymentValidation(){

        var grand_total = parseFloat($('#grand_total').val());
        var due_amm = parseFloat($('#due_amm').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var pval = '<?=$pval?>';

        if(grand_total == due_amm){


            if(paid_amount<pval){

                swal("Can't pay less than "+pval);
                $('#paid_amount').val(pval);
                
                calDuePaid();
                return false;

            }
            if(paid_amount>grand_total){
                swal("Can't pay more than "+grand_total);
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }


        } else {

            if(1>paid_amount){
                swal("Can't pay less than 1");
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }

            if(due_amm<paid_amount){
                swal("Can't pay more than "+due_amm);
                $('#paid_amount').val(pval);
                calDuePaid();
                return false;
            }

        }

}

// $('#MyForm').on('submit', function() {



   
// });




    function calDuePaid(){

        var grand_total = parseFloat($('#grand_total').val());
        var due_amm = parseFloat($('#due_amm').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var pval = '<?=$pval?>';




        if(isNaN(paid_amount)) {
            var paid_amount = 0;
        }else{
            var paid_amount = parseFloat($('#paid_amount').val());
        }

        var due = (due_amm-paid_amount);
        $('#due').val(due.toFixed(2));

    }



    $('#card_number').keyup(function ()
    {
        $(this).val(function (i, v)
        {
            if (v.length > 19) {
                var str = v.substring(0, 19);
                var str = str.replace(/[^\d]/g, '').match(/.{1,4}/g);
                return str ? str.join('-') : '';
            } else {
                var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
                return v ? v.join('-') : '';
            }
        });
    });




    function setCard(value) {

        if (value === 'card') {

            var customer_id = $('#customer_id').val();

                var submit_url = "<?php echo base_url(); ?>"+"b_level/setting_controller/getCardInfo/"+customer_id;

                $.ajax({
                    url: submit_url,
                    type: 'post',
                    success: function (data) {

                        var obj = jQuery.parseJSON(data);

                        $("#card_number").val(obj.card_number);
                        $("#expiry_month").val(obj.expiry_month);
                        $("#expiry_year").val(obj.expiry_year);
                        $("#name_on_card").val(obj.card_holder);

                        $('#card_number').trigger('keyup');
                       
                    },error: function() {

                    }
                });



            $(".card_method").slideToggle();

            $('#check_number').val('');
            $('#check_image').val('');
            $('.check_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/receive_payment/multiple_tms_payment');
            


        } else if (value === 'cash') {

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('#check_number').val('');
            $('#check_image').val('');

            $('.card_method').slideUp();
            $('.check_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/invoice_receipt/multiple_order_payment_update');

        } else if (value === 'check') {

            $(".check_method").slideToggle();

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('.card_method').slideUp();

            $("#MyForm").attr("action", '<?=base_url()?>b_level/invoice_receipt/multiple_order_payment_update');

        } else if (value === 'paypal') {

            $('#card_number').val('');
            $('#card_holder_name').val('');
            $('#expiry_date').val('');
            $('#cvv').val('');

            $('#check_number').val('');
            $('#check_image').val('');

            $('.card_method').slideUp();
            $('.check_method').slideUp();
            $("#MyForm").attr("action", '<?=base_url()?>b_level/invoice_receipt/multiple_order_payment_update');

        }

    }

</script>


<script src="<?php echo base_url(); ?>assets/b_level/card/creditCardValidator.js"></script>

<script>

    function cardFormValidate() {
        var cardValid = 0;

        //card number validation
        $('#card_number').validateCreditCard(function (result) {
            console.log(result);

            var cardType = (result.card_type == null) ? '' : result.card_type.name;
            if (cardType == 'visa') {
                var backPosition = result.valid ? '2px -163px, 260px -87px' : '2px -163px, 260px -61px';
            } else if (cardType == 'visa_electron') {
                var backPosition = result.valid ? '2px -205px, 260px -87px' : '2px -163px, 260px -61px';
            } else if (cardType == 'masterCard') {
                var backPosition = result.valid ? '2px -247px, 260px -87px' : '2px -247px, 260px -61px';
            } else if (cardType == 'maestro') {
                var backPosition = result.valid ? '2px -289px, 260px -87px' : '2px -289px, 260px -61px';
            } else if (cardType == 'discover') {
                var backPosition = result.valid ? '2px -331px, 260px -87px' : '2px -331px, 260px -61px';
            } else if (cardType == 'amex') {
                var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
            } else {
                var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
            }

            $('#card_number').css("background-position", backPosition);
            if (result.valid) {
                $("#card_type").val(cardType);
                $("#card_number").removeClass('required');
                cardValid = 1;
            } else {
                $("#card_type").val('');
                $("#card_number").addClass('required');
                cardValid = 0;
            }
        });

        //card details validation
        var cardName = $("#name_on_card").val();
        var expMonth = $("#expiry_month").val();
        var expYear = $("#expiry_year").val();
        var cvv = $("#cvv").val();
        var regName = /^[a-z ,.'-]+$/i;
        var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
        var regYear = /^19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45$/;
        var regCVV = /^[0-9]{3,4}$/;
        if (cardValid == 0) {
            $("#card_number").addClass('required');
            $("#card_number").focus();
            return false;
        } else if (!regMonth.test(expMonth)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").addClass('required');
            $("#expiry_month").focus();
            return false;
        } else if (!regYear.test(expYear)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").addClass('required');
            $("#expiry_year").focus();
            return false;
        } else if (!regCVV.test(cvv)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").addClass('required');
            $("#cvv").focus();
            return false;
        } else if (!regName.test(cardName)) {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").removeClass('required');
            $("#name_on_card").addClass('required');
            $("#name_on_card").focus();
            return false;
        } else {
            $("#card_number").removeClass('required');
            $("#expiry_month").removeClass('required');
            $("#expiry_year").removeClass('required');
            $("#cvv").removeClass('required');
            $("#name_on_card").removeClass('required');
            $("#cardSubmitBtn").removeAttr('disabled');
            return true;
        }
    }


    $(document).ready(function () {



        
        //Demo card numbers
        $('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function (e) {
            e.preventDefault();
            $('.card-payment .numbers').slideUp(100);
            cardFormValidate();
            return $('#card_number').val($(this).text()).trigger('input');
        });

        $('body').click(function () {
            return $('.card-payment .numbers').slideUp(100);
        });

        $('#sample-numbers-trigger').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            return $('.card-payment .numbers').slideDown(100);
        });

        //Card form validation on input fields
        $('#paymentForm input[type=text]').on('keyup', function () {
            cardFormValidate();
        });

        //Submit card form
        $("#cardSubmitBtn").on('click', function () {
            if (cardFormValidate()) {
                var card_number = $('#card_number').val();
                var valid_thru = $('#expiry_month').val() + '/' + $('#expiry_year').val();
                var cvv = $('#cvv').val();
                var card_name = $('#name_on_card').val();
                var cardInfo = '<p>Card Number: <span>' + card_number + '</span></p><p>Valid Thru: <span>' + valid_thru + '</span></p><p>CVV: <span>' + cvv + '</span></p><p>Name on Card: <span>' + card_name + '</span></p><p>Status: <span>VALID</span></p>';
                $('.cardInfo').slideDown('slow');
                $('.cardInfo').html(cardInfo);
            } else {
                $('.cardInfo').slideDown('slow');
                $('.cardInfo').html('<p>Wrong card details given, please try again.</p>');
            }
        });
    });

    function isNumeric(n) {
//swal(n);
        if (isNaN(document.getElementById(n).value)) {
            swal("Please enter numeric values!");
            document.getElementById(n).value = 0;
        }
    }
</script>