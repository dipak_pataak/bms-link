<html>

<body>
    <div id="SubContainer" style="font-size: 14px;">
        <div id="BodyContainer">
            <div id="SubBodyContainer">
                <table width="100%">
                    <thead style="display:table-header-group;">
                        <tr style="font-weight:bold;">
                            <td style="text-align:right;width:8%;">Item</td>
                            <td style="text-align:center;width:8%;">Qty</td>
                            <td colspan="2" style="text-align:center;width:66%;">Description</td>
                           <!-- <td style="text-align:right;width:8%;">List</td>
                            <td style="text-align:right;width:8%;">Discount</td>-->
                            <td style="text-align:center;width:18%;">Price</td>
                        </tr>
                    </thead>
                    <?php
                        $items = (Object)$order_details;
                        $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                        $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                        $cat_name = $this->db->where('category_id', $items->category_id)->get('category_tbl')->row();
                        $prod = $this->db->where('product_id', $items->product_id)->get('product_tbl')->row();
                        $color = $this->db->where('id', $items->color_id)->get('color_tbl')->row();
                        $pattern_data = $this->db->where('pattern_model_id', $items->pattern_model_id)->get('pattern_model_tbl')->row();
                        $selected_attributes = json_decode($items->att_options);
                        // dd($selected_attributes);
                    ?>
                    <tbody>
                       
                        <tr>
                            <td style="vertical-align:top;text-align:right;">1</td>
                            <td style="vertical-align:top;text-align:center;"><?php echo $items->qty; ?></td>
                            <td colspan="2"><?php if ($items->name) { echo $items->name . ''; } ?></td>
                            <!--<td style="vertical-align:top;text-align:right;">
                                <?= $company_profile[0]->currency; ?><?= number_format($items->list_price, 2) ?></td>
                            <td style="vertical-align:top;text-align:right;"><?= ($items->discount) ?></td>-->
                            <!-- <td style="vertical-align:top;text-align:right;"><?= $company_profile[0]->currency; ?>
                                <?= @number_format($items->price, 2) ?></td> -->
                            <td>
                                <div class="fixed_item">
                                    <div id="tprice">
                                        <table class="price-details">
                                            <tbody>
                                                <?php if(isset($items->h_w_price) && $items->h_w_price != ''){ ?>
                                                    <tr>
                                                        <td>Window Price :</td>
                                                        <td><?= $company_profile[0]->currency; ?><?=number_format($items->h_w_price, 2)?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if(isset($items->upcharge_price) && $items->upcharge_price > 0){ ?>
                                                    <tr>
                                                        <td>Upcharge :</td>
                                                        <td><?= $company_profile[0]->currency; ?><?=number_format($items->upcharge_price, 2)?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if(isset($items->price) && $items->price != ''){ ?>
                                                <!-- <tr>
                                                    <td>Total Price = </td>
                                                    <td><?= $company_profile[0]->currency; ?><?=number_format($items->price, 2)?></td>
                                                </tr> -->
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>  
                            </td>    
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2"> Pattern: <?php echo $pattern_data->pattern_name; ?>
                            <td colspan="3" rowspan="25"></td>
                        <tr>
                            <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2"> Category: <?php echo $cat_name->category_name; ?>
                            <td colspan="3" rowspan="25"></td>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td colspan="2"> Color: <?= $color->color_number; ?>
                                <?php echo '(' . $color->color_name . ')'; ?>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>Room Location</td>
                            <td><?php echo $items->room; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>Width</td>
                            <td><?= $items->width; ?> <?= @$width_fraction->fraction_value ?></td>
                        </tr>
                        <?php if($cat_name->category_name != 'Arch') { ?>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                                <td>Height</td>
                                <td><?= $items->height; ?> <?= @$height_fraction->fraction_value ?> </td>
                            </tr>
                        <?php } ?>
                            
                        <?php  foreach ($selected_attributes as $atributes) { ?>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                                <td>
                                    <?php
                                        $at_id = $atributes->attribute_id;
                                        $att_name = $this->db->where('attribute_id', $at_id)->get('attribute_tbl')->row();
                                        echo $att_name->attribute_name;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(isset($atributes->options[0]->option_id) && $atributes->options[0]->option_id != '' && $atributes->attributes_type != 1){
                                            
                                            $at_op_id = $atributes->options[0]->option_id;
                                            $att_op_name = $this->db->where('att_op_id', $at_op_id)->get('attr_options')->row();
                                            echo $att_op_name->option_name;

                                        }else{
                                            echo $atributes->attribute_value;
                                        }    
                                    ?>
                                </td>
                            </tr>

                            <?php
                            if ($atributes->options[0]->option_type == 3 || $atributes->options[0]->option_type == 5 || $atributes->options[0]->option_type == 2 || $atributes->options[0]->option_type == 4 || $atributes->options[0]->option_type == 6){
                                if (sizeof($atributes->opop) > 0) {
                                    foreach ($atributes->opop as $secondLevelOpts) {
                                        $secondLevelOpt = $this->db->where('op_op_id', $secondLevelOpts->op_op_id)->get('attr_options_option_tbl')->row();
                                        $secondLevelOptName = $secondLevelOpt->op_op_name;

                                        $secondLevelOptValue = "";
                                        if ($secondLevelOpt->type == 1 || $secondLevelOpt->type == 0 || $secondLevelOpt->type == 2) {
                                            $secondLevelOptValue = $secondLevelOpts->op_op_value;   
                                        }

                                        // For multioption with multiselect : START
                                        if ($atributes->options[0]->option_type == 4 && $secondLevelOpt->type == 6) {
                                            $secondLevelOptValue = "";
                                            foreach ($atributes->opopop as $thirdLevelOpts) {
                                                if(isset($thirdLevelOpts->op_op_op_id) && $thirdLevelOpts->op_op_op_id != ''){
                                                $thirdLevelOpt = $this->db->where('att_op_op_op_id', $thirdLevelOpts->op_op_op_id)->get('attr_options_option_option_tbl')->row();
                                                $secondLevelOptValue .= $thirdLevelOpt->att_op_op_op_name.",";
                                                }
                                            }
                                            $secondLevelOptValue = rtrim($secondLevelOptValue, ',');
                                        }
                                        // For multioption with multiselect : END

                                        // For fraction value type : START
                                        if ($atributes->options[0]->option_type == 5) {
                                            $get_fraction_id = explode(" ", $secondLevelOptValue);
                                            if(isset($get_fraction_id[1]) && $get_fraction_id[1] != ''){
                                                $sub_fraction = $this->db->where('id', $get_fraction_id[1])->get('width_height_fractions')->row();
                                                $fraction_option = isset($sub_fraction->fraction_value) ? $sub_fraction->fraction_value : ''; 
                                                $secondLevelOptValue =  $get_fraction_id[0]." ".$fraction_option;
                                            }    
                                        }
                                        // For fraction value type : END

                                        if ($atributes->options[0]->option_type == 2 || $atributes->options[0]->option_type == 6) {
                                            ?>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>   
                                                <td><?php echo $secondLevelOptName; ?></td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>   
                                                <td><?php echo $secondLevelOptName; ?></td>
                                                <td><?php echo $secondLevelOptValue; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                }
                            } else if ($atributes->options[0]->option_type == 1) {
                                ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td><?php echo $atributes->options[0]->option_value ; ?></td>
                                </tr>
                                <?php
                            } 
                        } 
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">Notes:</td>
                            <td colspan="5"><b><?= $items->notes ?></b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>

</html>