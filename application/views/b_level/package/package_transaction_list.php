


<main  id="right" >
    <div class="box p-4">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1 class="pl-0 ml-0">Payment Transaction List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment Transaction </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>


        <!-- 
                <div class="card mb-4 " >
                    <div class="card-body">
                        <p class="mb-0">
                            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Filter
                            </button>
                        </p>
                        <div class="collapse" id="collapseExample">
                            <div class="p-4 border mt-4">
        
                                <form class="form-horizontal" action="<?php echo base_url('retailer-payment-log'); ?>" method="post">
        
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="text" class="form-control datepicker mb-3 " name="fromdate" placeholder="From date">
                                            </div>
        
                                            <div class="col-md-3">
                                                <input type="text" class="form-control datepicker mb-3 " name="todate" placeholder="To date">
                                            </div>
                                            
                                            <div class="col-md-2 text-right">
                                                <div>
                                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>   --> 


        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <!-- end box / title -->
                        <div class="table-responsive px-3">
                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>name</th>
                                        <th>transaction id</th>
                                         <th>Card details</th>
                                        <th>Paid amount</th>
                                        <th>Date</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($package_data_list)) {
                                        $i = 1;
                                        foreach ($package_data_list as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?= $i++; ?></td>
                                                <td><?= $value->package; ?></td>
                                    
                                                 <td><?= $value->transaction_id; ?></td>
                                                  <td><?= $value->card_no; ?></td>
                                                <td> <?= $cmp_info[0]->currency; ?><?= $value->amount; ?></td>
                                                <td><?= date_format(date_create($value->create_date), 'M-d-Y'); ?></td>
                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>

                                        <!--<div class="alert alert-danger"> There have no notification found..</div>-->

                                    <?php } ?>
                                </tbody>
                                <?php if (empty($package_data_list)) { ?>
                                    <tr>
                                        <th class="text-center text-danger" colspan="6">Record not found!</th>
                                    </tr>
                                <?php } ?>
                            </table>

                            <?= @$links ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
