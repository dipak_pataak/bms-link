<script type="text/javascript">

//    var count = 2;
//    limits = 500;
// ========== its for row add dynamically =============
    // function addInputField(t) {
    //     var row = $("#addinvoiceItem tr").length;
    //     var count = row + 1;
    //     var limits = 500;
    //     if (count == limits) {
    //         swal("You have reached the limit of adding" + count + "inputs");
    //     } else {
    //         var a = "rmtt_id_" + count, e = document.createElement("tr");

    //         e.innerHTML = "<td>\n\
    //                         <select  id='rmtt_id_" + count + "' onchange='service_cals(" + count + ")' name='rmtt_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
    //                             <option value=''>-- select one --</option>\n\
    // <?php foreach ($rmtts as $val) { ?> <option value='<?php echo  $val->id ?>'> <?php echo  $val->material_name; ?></option><?php } ?>  </select>\n\
    //                         </td>\n\
    //                         <td>\n\
    //                         <select  id='color_id_" + count + "'  onchange='color_cals(" + count + ")' name='color_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
    //                             <option value=''>-- select one --</option>\n\
    //                         </select>\n\
    //                         </td>\n\
    //                         <td>\n\
    //                         <select  id='pattern_model_id_" + count + "' name='pattern_model_id[]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>>\n\
    //                             <option value=''>-- select one --</option>\n\
    //                         </select>\n\
    //                         </td>\n\
    //             <td><input type='text' class='form-control' name='available_qnt[]' id='available_qnt_" + count + "' onkeyup='' onchange='' value='' readonly style='text-align:right'></td>\n\
    //             <td><input type='number' class='form-control' name='product_quantity[]' id='product_quantity_" + count + "' onkeyup='quantity_calculate(" + count + ")' onchange='quantity_calculate(" + count + ")' placeholder='' value='' style='text-align:center'></td>\n\
    //             <td><input type='text' class='form-control' name='product_rate[]' id='product_rate_" + count + "' onkeyup='quantity_calculate(" + count + ")' onchange='quantity_calculate(" + count + ")' placeholder='0.00' value='' style='text-align:right'></td>\n\
    //             <td><input type='text' class='form-control product_discount' name='product_discount[]' id='product_discount_" + count + "' onkeyup='quantity_calculate(" + count + ")' onchange='quantity_calculate(" + count + ")' placeholder='0.00' value='' style='text-align:right'></td>\n\
    //             <td><input type='text' class='form-control total_price' name='total_price[]' id='total_price_" + count + "' onkeyup='quantity_calculate(" + count + ")' onchange='quantity_calculate(" + count + ")' placeholder='0.00' style='text-align:right' readonly></td> \n\
    //             <td>\n\
    //             <input type='hidden' class='form-control all_discount' id='all_discount_" + count + "' name='discount_amount[]'>\n\
    //             <button style='text-align: right;' class='btn btn-danger' type='button' value='Delete' onclick='deleteRow(this)'><i class='fa fa-trash-o'></i></button></td>\n\
    //             ",
    //                 document.getElementById(t).appendChild(e), document.getElementById(a).focus(), count++
    //         count++;
    //     }
    // }


    // ========== its for row add dynamically : START =============
    function addInputField() {
        var material_option = $(".main_invoice_item #rmtt_id").html();
        var material_html = '<tr class="child_invoice_item">'+
                                '<td>'+
                                    '<select onchange="service_cals(this)" name="rmtt_id[]" class="form-control select2 rmtt_id" tabindex="3" data-placeholder="-- select one --" required>'+
                                        material_option+
                                    '</select>'+
                                '</td>'+

                                '<td>'+
                                    '<select onchange="color_cals(this)" name="color_id[]" class="form-control select2 color_id" tabindex="4" data-placeholder="-- select one --" required>'+
                                        '<option value="">-- select one --</option>'+
                                    '</select>'+
                                '</td>'+

                                '<td>'+
                                    '<select onchange="color_cals(this)" name="pattern_model_id[]" class="form-control select2 pattern_model_id" tabindex="5" data-placeholder="-- select one --" required>'+
                                        '<option value=""></option>'+
                                    '</select>'+
                                '</td>'+
                                '<td class="text-right">'+
                                    '<input type="text" name="available_qnt[]" class="form-control text-right available_qnt" readonly>'+
                                '</td>'+
                                '<td class="text-right">'+
                                    '<select name="measurement_unit[]" class="form-control select2" tabindex="5" data-placeholder="-- select one --" required>'+
                                        '<option value="">-- select one --</option>'+
                                        <?php foreach ($get_measurement_unit as $key => $value) { ?>
                                            '<option value="<?=$value->uom_id?>"><?=$value->uom_name?></option>'+
                                        <?php } ?>
                                '</td>'+
                                '<td class="text-right">'+
                                    '<input type="number" name="product_quantity[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control text-center product_quantity" tabindex="6">'+
                                '</td>'+
                                '<td class="test">'+
                                    '<input type="text" name="product_rate[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control product_rate text-right" placeholder="0.00" value="" min="0" required="" tabindex="7"/>'+
                                '</td>'+
                                '<td class="test">'+
                                    '<input type="text" name="product_discount[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control product_discount text-right" placeholder="0.00" value="" min="0" tabindex="8"/>'+
                                '</td>'+
                                '<td class="text-right">'+
                                    '<input class="form-control total_price text-right" type="text" name="unit_total_price[]" value="0.00" readonly="readonly" />'+
                                '</td>'+
                                '<td>'+
                                    '<button style="text-align: right;" class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)"><i class="fa fa-trash-o"></i></button>'+
                                '</td>'+
                            '</tr>';
        $("#addinvoiceItem").append(material_html);
        $("#addinvoiceItem .select2").select2();
    }
    // ========== its for row add dynamically : END =============

    // ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalinvoice > tbody > tr").length;
        if (1 == a) {
            swal("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
        }
        calculateSum()
    }

    //Calcucate Invoice Add Items
    function quantity_calculate(_this) {
        var parent_html = $(_this).parent('td').parent('tr');

        var available_qnt = parent_html.find('.available_qnt').val();
        var product_quantity = parent_html.find('.product_quantity').val();
        var product_rate = parent_html.find('.product_rate').val();
        var product_discount = parent_html.find('.product_discount').val();
        var total_price = parent_html.find('.total_price').val();
        var invoice_discount = $("#invoice_discount").val();
        if (product_quantity > 0 && product_rate > 0) {
            var total_amount = product_quantity * product_rate;
            parent_html.find(".total_price").val(total_amount);
        }
        if (product_discount > 0) {
             parent_html.find(".total_price").val(total_amount - product_discount);
        }
        calculateSum();
    }


    function calculateSum() {
        var t = 0,
                a = 0,
                e = 0,
                o = 0,
                p = 0;
        $(".total_price").each(function () {
            isNaN(this.value) || 0 == this.value.length || (e += parseFloat(this.value))
        }),
                $(".product_discount").each(function () {
            isNaN(this.value) || 0 == this.value.length || (p += parseFloat(this.value))
        }),
                $("#grandTotal").val(e.toFixed(2));
        var gt = $("#grandTotal").val();
        var invoiceDiscount = $("#invoice_discount").val();
        var ttl_discount = +invoiceDiscount + +p.toFixed(2, 2);
        $("#total_discount").val(ttl_discount);
        var grandTotals = e.toFixed(2) - invoiceDiscount;
        $("#grandTotal").val(grandTotals);
        invoice_paidamount();
    }

    //Invoice Paid Amount
    function invoice_paidamount() {
        var t = $("#grandTotal").val(),
                a = $("#paidAmount").val(),
                e = t - a;
        $("#dueAmmount").val(e.toFixed(2, 2))
    }

    function service_cals(_this) {
        var raw_material_id = $(_this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>get-material-info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
                var color_d = $(_this).parent('td').parent('tr').find('.color_id');
                color_d.empty();
                color_d.html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
                    color_d.append($('<option>').text(typevar.color_name).attr('value', typevar.id));
                });
            },
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>get-material-wise-pattern-info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
                var pattern_d = $(_this).parent('td').parent('tr').find('.pattern_model_id');
                pattern_d.empty();
                pattern_d.html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
                    pattern_d.append($('<option>').text(typevar.pattern_name).attr('value', typevar.pattern_model_id));
                });
            },
        });
    }
    //==== its for material, color and pattern wise stock summation when change color_cals function ====
    function color_cals(_this) {
        var parent_html = $(_this).parent('td').parent('tr');

        var raw_material_id = parent_html.find('.rmtt_id').val();
        var color_id = parent_html.find('.color_id').val();
        var pattern_model_id = parent_html.find('.pattern_model_id').val();

        if(raw_material_id != '' && color_id != '' && pattern_model_id != ''){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>get-material-color-pattern-wise-stock-qnt/" + raw_material_id + "/" + color_id + "/" + pattern_model_id,
                success: function (s) {
                    var obj = jQuery.parseJSON(s);
                    parent_html.find('.available_qnt').val(obj.available_quantity);
                },
            });
        }else{
            parent_html.find('.available_qnt').val('');
        }    
    }

    var count = 2,
    limits = 500;
</script>