
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Purchase view</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <div class="row" id="supplier_info">
                <div class="col-sm-6">
                    <table class="table-bordered">
                        <tr>
                            <td class="text-center">Purchase ID</td>
                            <td class="text-center"><?= ($purchase->purchase_id) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Purchase Date</td>
                            <td class="text-center"><?php echo date('M-d-Y', strtotime($purchase->date)) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Supplier</td>
                            <td class="text-center"><?= $purchase->supplier_name ?></td>
                        </tr>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="table-responsive" style="margin-top: 10px">
                <table class="table table-bordered table-hover" id="normalinvoice">
                    <thead>
                        <tr>
                            <th class="text-center">Raw material</th>
                            <th class="text-center">Color</th>
                            <th class="text-center">Pattern/Model</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Discount </th>
                            <th class="text-center">Total </th>
                        </tr>
                    </thead>
                    <tbody id="addinvoiceItem">
                        <?php
                        if (!empty($purchase_details)) {
                            $i = 1;
                            foreach ($purchase_details as $key => $p) {
                                ?>    
                                <tr>
                                    <td> <?= $p->material_name; ?></td>
                                    <td><?= $p->color_name; ?></td>
                                    <td><?= $p->pattern_name; ?></td>
                                    <td class="text-right"><?= $p->quantity; ?> </td>
                                    <td class="text-right"> <?= $p->purchase_price ?></td>
                                    <td class="text-right"><?= $p->discount ?></td>
                                    <td class="text-right"> <?= ($p->purchase_price * $p->quantity) - $p->discount ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" rowspan="1">
                                <?= $purchase->purchase_description ?>
                            </td>
                            <td style="text-align:right;" colspan="2"><b>Invoice Discount</b>:</td>
                            <td class="text-right"><?= $purchase->invoice_discount ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;" colspan="6"><b>Total Discount</b>:</td>
                            <td class="text-right"><?= $purchase->total_discount ?></td>
                        </tr>

                        <tr>
                            <td colspan="6"  style="text-align:right;"><b>Grand Total:</b></td>
                            <td class="text-right"><?= $purchase->grand_total ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;" colspan="6"><b>Paid Amount:</b></td>
                            <td class="text-right"><?= $purchase->paid_amount ?></td>
                        </tr>
                        <tr>
                            <td style="text-align:right;" colspan="6"><b>Due:</b></td>
                            <td class="text-right"><?= $purchase->due_amount ?></td>
                        </tr>
                    </tfoot>
                </table> 
            </div><br/><br/>
            <div class="row" id="supplier_info">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                    <table class="table-bordered">
                        <tr>
                            <td class="text-center">Payment type</td>
                            <td class="text-center"><?= ($purchase->payment_type == 1 ? 'Cash' : 'Check') ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Chack No</td>
                            <td class="text-center"><?= ($purchase->check_no) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Bank Branch name</td>
                            <td class="text-center"><?= ($purchase->bank_branch_name) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Account No</td>
                            <td class="text-center"><?= ($purchase->account_no) ?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>


        </div>

    </div>
</div>


