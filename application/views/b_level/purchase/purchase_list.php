<?php $packageid=$this->session->userdata('packageid'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
                ?> 
            </div>
        </div>

        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Purchase List</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Purchase List';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                 <?php $menu_permission= b_access_role_permission(66); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
<!--                <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success mt-1">Add</a>-->
            </div>
        </div>

        <!-- end box / title -->
        <div class="px-3">
              <?php $packageid=$this->session->userdata('packageid'); ?>
                <?php if (!empty($packageid)){?>
                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                        <a href="<?php echo base_url(); ?>purchase-entry" class="btn btn-success mb-2">Add New</a>
                    <?php }?>
             <?php }  ?>
            <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL No.</th>
                        <th>Supplier Name</th>
                        <th>Purchase id</th>
                        <th>Date</th>
                        <th>Total</th>
                        <th>Payment Type</th>
                         <?php if (!empty($packageid)){?>
                        <th class="text-center">Action</th>
                           <?php }  ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($purchases != NULL) {
                        $i = 1;
                        foreach ($purchases as $key => $val) {
                            ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $val->supplier_name; ?></td>
                                <td><?= $val->purchase_id; ?></td>
                                <td><?= date('M-d-Y', strtotime($val->date)); ?></td>
                                <td><?= $val->grand_total; ?></td>
                                <td><?= ($val->payment_type == 1 ? 'Cash' : 'Check'); ?></td>
                                <?php if (!empty($packageid)){?>
                                <td class="text-right">
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="view-purchase/<?= $val->purchase_id ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                   
                                    <!--<a href="edit-purchase/<?= $val->purchase_id ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="edit"><i class="fa fa-edit" aria-hidden="true"></i></a>-->
                                      <?php } ?>
                                    <a href="raw-material-return-purchase/<?= $val->purchase_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>
                                    
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="delete-purchase/<?= $val->purchase_id ?>" onclick="return confirm('Are you sure want to delete it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    <?php } ?>

                                </td>
                              <?php } ?>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($purchases)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="text-center text-danger">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
			</div>
        </div>
    </div>
</div>
<!-- end content / right -->
