<?php $packageid=$this->session->userdata('packageid'); ?>
<?php $this->load->view('b_level/purchase/purchase_invoicejs'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">


        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>


        <!-- box / title -->
        <div class="title row">
            <h5>Purchase Entry</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Purchase Entry';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <?php $menu_permission= b_access_role_permission(65); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <form action="<?php echo base_url('purchase-save'); ?>" method="post" class="px-3" id="purchase_frm">
            <div class="row" id="supplier_info">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="form-group row">
                        <label for="supplier_sss" class="col-sm-4 col-form-label">Supplier</label>
                        <div class="col-sm-8">
                            <select name="supplier_id" id="supplier_id" class="form-control select2" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php
                                foreach ($get_supplier as $supplier) {
                                    echo "<option value='$supplier->supplier_id'>$supplier->supplier_name</option>";
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">Date </label>
                        <div class="col-sm-8">
                            <input type="text" name="date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" tabindex="2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="table-responsive col-sm-12" style="margin-top: 10px;">
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center" width="15%">Material</th>
                                <th class="text-center" width="10%">Color</th>
                                <th class="text-center" width="10%">Pattern</th>
                                <th class="text-center" width="10%">Stock Qnt</th>
                                <th class="text-center" width="10%">Unit</th>
                                <th class="text-center" width="10%">Qty/Unit</th>
                                <th class="text-center" width="10%">Price</th>
                                <th class="text-center" width="10%">Discount </th>
                                <th class="text-center" width="10%">Total </th>
                                <?php $packageid=$this->session->userdata('packageid'); ?>
                                 <?php if (!empty($packageid)){?>
                                <th class="text-center" width="5%">Action </th>
                                 <?php } ?>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <tr class="main_invoice_item">
                                <td>
                                    <select onchange='service_cals(this)' id="rmtt_id" name='rmtt_id[]' class='form-control select2 rmtt_id' tabindex='3' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                    </select>
                                </td>

                                <td>
                                    <select onchange="color_cals(this)" name='color_id[]' class='form-control select2 color_id' tabindex='4' data-placeholder='-- select one --' required>
                                        <option value=''>-- select one --</option>
                                    </select>
                                </td>

                                <td>
                                    <select onchange="color_cals(this)" name='pattern_model_id[]' class='form-control select2 pattern_model_id' tabindex='5' data-placeholder='-- select one --' required>
                                        <option value=''>-- select one --</option>
                                    </select>
                                </td>
                                <td class="text-right">
                                    <input type="text" name="available_qnt[]" class="form-control text-right available_qnt" readonly>
                                </td>
                                <td class="text-right">
                                    <select name='measurement_unit[]' class='form-control select2' tabindex='5' data-placeholder='-- select one --' required>
                                    <option value=''>-- select one --</option>
                                    <?php foreach ($get_measurement_unit as $key => $value) { ?>
                                        <option value='<?=$value->uom_id?>'><?=$value->uom_name?></option>
                                    <?php } ?>
                                </td>
                                <td class="text-right">
                                    <input type="number" name="product_quantity[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control text-center product_quantity" tabindex="6">
                                </td>
                                <td class="test">
                                    <input type="text" name="product_rate[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control product_rate text-right" placeholder="0.00" value="" min="0" required="" tabindex="7"/>
                                </td>
                                <td class="test">
                                    <input type="text" name="product_discount[]" onkeyup="quantity_calculate(this);" onchange="quantity_calculate(this);" class="form-control product_discount text-right" placeholder="0.00" value="" min="0" tabindex="8"/>
                                </td>
                                <td class="text-right">
                                    <input class="form-control total_price text-right" type="text" name="unit_total_price[]" value="0.00" readonly="readonly" />
                                </td>
                                <td class="text-right">
                                    <button style="text-align: right;" class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>    
                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="6" rowspan="1">
                                    <input id="add-invoice-item" class="btn btn-info d-none" name="add-new-item" onclick="addInputField();" value="Add New" type="button" style="margin: 0px 15px 15px;">
                                    <br>
                                    <label style="" for="purchase_description" class="  col-form-label">Purchase Details</label>
                                    <textarea name="purchase_description" class="form-control" placeholder="Purchase Details"></textarea>
                                </td>

                                <td style="text-align:right;" colspan="2"><b>Invoice Discount</b>:</td>
                                <td class="text-right">
                                    <input type="text" onkeyup="quantity_calculate(this);"  onchange="quantity_calculate(this);" id="invoice_discount" class="form-control text-right" name="invoice_discount" placeholder="0.00" tabindex="10" />
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:right;" colspan="8"><b>Total Discount</b>:</td>
                                <td class="text-right">
                                    <input type="text"  id="total_discount" class="form-control text-right" name="total_discount" placeholder="0.00" readonly />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="8"  style="text-align:right;"><b>Grand Total:</b></td>
                                <td class="text-right">
                                    <input type="text" id="grandTotal" class="form-control text-right" name="grand_total_price" value="0.00" readonly="readonly" />
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:right;" colspan="8"><b>Paid Amount:</b></td>
                                <td class="text-right">
                                    <input type="text" id="paidAmount" 
                                           onkeyup="invoice_paidamount();" class="form-control text-right" name="paid_amount" placeholder="0.00" tabindex="13"/>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align:right;" colspan="8"><b>Due:</b></td>
                                <td class="text-right">
                                    <input type="text" id="dueAmmount" class="form-control text-right" name="due_amount" value="0.00" readonly="readonly"/>
                                </td>
                            </tr>
                        </tfoot>

                    </table>          
                </div>               
            </div>
            <br/>

            <div class="row" id="supplier_info">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="col-sm-3 col-form-label">Payment type
                        </label>
                        <div class="col-sm-9">
                            <select name="payment_type" class="form-control select2" id="theSelect" tabindex="" data-placeholder="-- select one --" required>

                                <option value=""></option>
                                <option value="1">Cash</option>
                                <option value="2">Check</option>

                            </select>
                        </div>
                    </div>
                </div>



                <div class="col-sm-6 hidden is2" >

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Check No
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="check_no" class="form-control" id="check_no">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">Bank and Branch
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="branch_name" class="form-control" id="branch_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-12 col-form-label">A/C No
                        </label>
                        <div class="col-sm-12">
                            <input type="text" name="ac_no" class="form-control" id="ac_no">
                        </div>
                    </div>

                </div>

            </div>



            <div class="form-group text-right mt-2">
                <?php if (!empty($packageid)){?>
                 <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>   
                <input type="submit" id="add_purchase" class="btn btn-primary default btn-sm" name="add-purchase" value="Submit " />
                <input type="submit" value="Submit and Another" name="add-purchase-another" class="btn btn-sm default btn-success" id="add_purchase_another">
                <?php }?>
                <?php }  ?>
            </div>
        </form>

    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">

    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if (value == 1) {

            $('#check_no').prop("required", false);
            $('#branch_name').prop("required", false);
            $('#ac_no').prop("required", false);

            $('.is2').slideUp().addClass("hidden");

        } else {

            theDiv.slideDown().removeClass("hidden");
            $('#check_no').prop("required", true);
            $('#branch_name').prop("required", true);
            $('#ac_no').prop("required", true);
        }

    });


</script>


<script>
    $("#supplier_id").change(function(){
        var supplier_id = $(this).val();
        $(".child_invoice_item").remove();
        if(supplier_id != ''){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>get_row_material_supplierwise/" + supplier_id,
                success: function (s) {
                    var obj = jQuery.parseJSON(s);
                    $(".rmtt_id").empty();
                    $(".rmtt_id").append($('<option>').text('--select one--').attr('value', ''));
                    $.each(obj, function (r, typevar) {
                        $(".rmtt_id").append($('<option>').text(typevar.material_name).attr('value', typevar.id));
                    });
                },
            });
            $("#add-invoice-item").removeClass('d-none');
        }else{
            $("#add-invoice-item").addClass('d-none');
        } 
    });
</script>    
