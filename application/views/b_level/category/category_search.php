<form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Category_controller/manage_action') ?>">
<input type="hidden" name="action">
	<div class="table-responsive">
    <table class="table table-bordered table-hover text-center">
        <thead>
            <tr>
                <th><input type="checkbox" id="SellectAll"/></th>
                <th>Serial No.</th>
                <th>Category Name</th>
                <th>Parent Category</th>
                <th>Description</th>
                <th>Assigned Products</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sl = 0;
            foreach ($category_list as $category) {
                $parent_category = $this->db->select('*')->where('category_id', $category->parent_category)->get('category_tbl')->row();
                $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $category->category_id)
                                ->get()->result();
                $sl++
                ?>
                <tr>
                    <td>
                        <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $category->category_id; ?>" class="checkbox_list">  
                    </td>
                    <td><?php echo $sl; ?></td>
                    <td><?php echo $category->category_name; ?></td>
                    <td>
                        <?php
                        if (!empty($parent_category->category_name)) {
                            echo $parent_category->category_name;
                        } else {
                            echo "None";
                        }
                        ?>
                    </td>
                    <td><?php echo $category->description; ?></td>
                    <td class="text-left">
                        <?php
                        foreach ($assigned_products as $product) {
                            echo "<ul>";
                            echo "<li> => " . $product->product_name . "</li>";
                            echo "</ul";
                        }
                        ?>
                    </td>
                    <td><?php
                        if ($category->status == '1') {
                            echo "Active";
                        } else {
                            echo 'Inactive';
                        }
                        ?></td>
                    <td class="width_140 text-right">
                        <!--<a href="<?php echo base_url(); ?>category-edit/<?php echo $category->category_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>-->
                        <a href="javascript:void(0)" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit" onclick="show_category_edit(<?php echo $category->category_id; ?>);"><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url(); ?>wholesaler-category-delete/<?php echo $category->category_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
        <?php if (empty($category_list)) { ?>
            <tfoot>
                <tr>
                    <th colspan="8" class="text-center text-danger">No record found!</th>
                </tr> 
            </tfoot>
        <?php } ?>
    </table> 
	</div>
</form>       
