<?php $packageid=$this->session->userdata('packageid'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<style type="text/css">
	.select2.select2-container {
			width: 100% !important;
	}
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or-filter .col-sm-3.col-md-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 18.7%;
			max-width: 18.7%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6"><h5>Manage Category</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Category';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                     $menu_permission= b_access_role_permission(10); 
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="filter-collpase-btn">
                Filter
            </button>
           <?php if(!empty($packageid)){?>
            <button type="button" class="btn btn-info btn-sm mb-1" data-toggle="modal" data-target="#importCategory">Bulk Import Upload</button>   
            <a href="<?=base_url('export-wholesaler-category')?>" class="btn btn-info btn-sm mb-1">Export</a> 
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#categoryFrm">Add Category</button>
                <?php } ?>
                <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
                <?php } ?>           
       </p>
            <?php } ?>
        <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>category-filter" id="categoryFilterFrm">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control cat_name" placeholder="Enter Name" name="cat_name" value="<?=$this->session->userdata('search_categoryname')?>">
                            </div>
                            <div class="or_cls"> -- OR -- </div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="parent_cat" class="form-control parent_cat select2" id="parent_cat" data-placeholder="-- select category --">
                                        <option value=""></option>
                                        <?php foreach ($parent_category as $category) { ?>
                                            <option value='<?php echo $category->category_id; ?>'>
                                                <?php echo $category->category_name; ?>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                                    <script>
                                        $("#parent_cat").val('<?=$this->session->userdata('search_parent_category')?>');
                                    </script>    
                                </div>
                            </div>
                            <div class="or_cls"> -- OR -- </div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="category_status" class="form-control category_status select2" id="category_status" data-placeholder="-- select status --">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <script>
                                        $("#category_status").val('<?=$this->session->userdata('search_status')?>');
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </form>
            </div>
        </div>
        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="categoryFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Category Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url('b_level/Category_controller/save_category'); ?>" method="post" class="filter_frm">
                            <div class="form-group row">
                                <label for="category_name" class="col-md-3 control-label mb-1 mb-1">Category Name<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="category_name" id="category_name" onkeyup="special_character()" placeholder="Category Name"  required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="parent_category" class="col-md-3 control-label mb-1">Parent Category</label>
                                <div class="col-md-6">
                                    <select name="parent_category" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=" ">None</option>
                                        <?php
                                        foreach ($parent_category as $category) {
                                            echo "<option value='$category->category_id'>$category->category_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-3 control-label mb-1">Description</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control" type="text" placeholder="Description" rows="1" id="description"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="description" class="col-md-3 control-label">Fractions</label>
                                <div class="col-md-6">
                                    <input type="text" name="fractions" class="form-control" value="" placeholder="1/8, 1/4, 1/2, etc." >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-md-3 control-label mb-1">Status</label>
                                <div class="col-md-6">
                                    <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=""></option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="offset-3 col-md-2">
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <button type="submit" class="btn btn-success w-md m-b-5 float-right">Save</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="table-responsive px-3" id="results_category">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Category_controller/manage_action') ?>">
                <input type="hidden" name="action">
				<div class="table-responsive">
                    <table id="CategoryDataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Serial No.</th>
                            <th>Category Name</th>
                            <th>Parent Category</th>
                            <th>Description</th>
                            <th>Fractions</th>
                            <th>Assigned Products</th>
                            <th>Status</th>
                            <th>Action</th>
                           
                        </tr>
                    </thead>
                </table>
				</div>
			</form>
        </div>

        <div class="modal fade" id="category_modal_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Category Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="category_info">

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


 <!-- Modal -->
<div class="modal fade" id="importCategory" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Category Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <a href="<?php echo base_url('assets/b_level/csv/category_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                <?php echo form_open_multipart('category-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                </div>

                </form>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>


<!-- end content / right -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    function show_category_edit(id) {
        $.post("<?php echo base_url(); ?>category-edit/" + id, function (t) {
            $("#category_info").html(t);
            $('#category_modal_info').modal('show');
        });
    }
    //=========== its for get special character =========
    function special_character() {
        var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\/~`-="
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#category_name').val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            swal(specialChars + " these special character are not allows");
            $("#category_name").focus();
        }
    }

    $(document).ready(function(){

        // For Open filter Box : START
        var cat_name = $("#categoryFilterFrm input[name=cat_name]").val();
        var parent_cat = $("#categoryFilterFrm #parent_cat").val();
        var category_status = $("#categoryFilterFrm #category_status").val();

        if(cat_name != '' || parent_cat != '' || category_status != '') {
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
        var orderable = [0,1,5,6,7,8];
        init_datatable('CategoryDataTable','<?php echo base_url('wholesaler-getCategoryLists/'); ?>',orderable);
    });
</script>
