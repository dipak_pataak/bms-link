<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
    .color_checkbox{
        font-size: 15px;
        overflow: scroll;
        height: 100px;
    }
</style>
<!-- content / right -->
<div id="right">

    <div class="box new_product" style="height: 100%;">
        <div class="row">
            <div class="col-md-12 m-2">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>


        <!-- box / title -->            
        <div class="title row">
            <h5 class="col-sm-6">Add New raw material</h5>
            <div class="col-sm-6 float-right text-right">
                <a href="row-material-list" class="btn btn-success mt-1 " >Manage Raw Material</a>
            </div>
        </div>
        <!-- end box / title -->

        <?php echo form_open('b_level/row_materials/save', array('class' => 'form-row px-3', 'id' => '')); ?>
        <div class="offset-3 col-lg-6 px-4">
            <div class="form-group">
                <label for="product_code" class="mb-2">Material Name</label>
                <input class="form-control" type="text" name="material_name" placeholder="Row material name" required>
            </div>

            <div class="form-group">
                <label class="mb-2">Pattern/Model</label>
                <select name="pattern_model_id" id="Pattern" class="form-control select2" data-placeholder="-- select one --">
                    <option value="">-- select one --</option>
                    <?php foreach ($patern_model as $val) { ?>
                        <option value="<?= $val->pattern_model_id ?>"><?= $val->pattern_name; ?></option>
                    <?php } ?>
                </select>
            </div>  
            <div class="form-group">
                <label class="mb-2">Color</label>
                <select class="selectpicker form-control" name="color_id[]" multiple data-live-search="true">
                    <?php foreach ($colors as $val) { ?>
                        <option value="<?= $val->id ?>"><?= ucwords($val->color_name); ?></option>
                    <?php } ?>
                </select>
            </div>
            <!--            <div class="form-group">
                            <label for="color_id" class="mb-3">Color</label>
                            <select name="color_id" id="color_id" class="form-control select2" data-placeholder="-- select one --">
                                <option value=""></option>
            <?php foreach ($colors as $val) { ?>
                                                <option value="<?= $val->id ?>"><?= $val->color_name; ?></option>
            <?php } ?>
                            </select>
                            <div class="color_checkbox">
            <?php foreach ($colors as $val) { ?>
                                            <label for="color_id_<?php echo $val->id; ?>">
                                                <input type="checkbox" name="color_id[]" id="color_id_<?php echo $val->id; ?>" value="<?php echo $val->id; ?>"> <?php echo ucwords($val->color_name); ?>
                                            </label>
            <?php } ?>
                            </div>
                        </div>  -->

            <div class="form-group">
                <label class="mb-2" for="uom">Unit of Measurement</label>
                <select name="uom" id="uom" class="form-control select2" data-placeholder="-- select one --">
                    <option value="">-- select one --</option>
                    <?php foreach ($get_uom_list as $val) { ?>
                        <option value="<?= $val->uom_id ?>"><?= $val->uom_name; ?></option>
                    <?php } ?>
                </select>
            </div>  
        </div>


        <div class="col-lg-8 px-4">
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success w-md m-b-5">Add material</button>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>

</div>
<!-- end content / right -->
<script type="text/javascript">

    $(document).ready(function () {
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    r = JSON.parse(r);
                    $("#subcategory_id").empty();
                    $("#subcategory_id").html("<option value=''>-- select one -- </option>");
                    $.each(r, function (ar, typeval) {
                        $('#subcategory_id').append($('<option>').text(typeval.category_name).attr('value', typeval.category_id));
                    });
                }
            });
        });


//        $('.selectpicker').selectpicker();
    });



</script>
