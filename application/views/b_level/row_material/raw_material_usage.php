<?php $packageid=$this->session->userdata('packageid'); ?>
<?php $this->load->view('b_level/row_material/raw_materialusagejs.php'); ?>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- box / title -->
        <div class="title row">
            <h5>Raw Material Usage</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Raw Material Usage';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
             <?php $menu_permission= b_access_role_permission(114); ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->
        <form action="<?php echo base_url('raw-material-usage-save'); ?>" method="post" class="px-3" id="purchase_frm">
            <div class="row" id="supplier_info">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="form-group row">
                        <label for="order_id" class="col-sm-4 col-form-label">Order ID</label>
                        <div class="col-sm-8">
                            <select name="order_id" id="get_item_data" class="form-control select2" tabindex="1" data-placeholder="-- select one --" required>
                                <option value=""></option>
                                <?php
                                foreach ($get_b_level_quatation as $b_order) {
                                    echo "<option value='$b_order->order_id'>$b_order->order_id</option>";
                                }
                                foreach ($get_quatation as $order) {
                                    echo "<option value='$order->order_id'>$order->order_id</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="form-group row">
                        <label for="date" class="col-sm-4 col-form-label">Date </label>
                        <div class="col-sm-8">
                            <input type="text" name="date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" tabindex="2">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Item details : START -->
            <div id="item_list_info">
                <div class="row">
                    
                </div>
            </div>    
            <!-- Item details : END -->
 <div class="row">
                <div class="table-responsive col-sm-12" style="margin-top: 10px;">
				
				</div>
				</div>

            <div class="row">
                <div class="table-responsive col-sm-12" style="margin-top: 10px;" id="append_row">
                    <table class="table table-bordered table-hover" id="normalinvoice">
					 <thead>
						
                            <tr>
                                <th class="text-center" width="15%">Item Name</th>
                                <th class="text-center" width="15%">Height</th>
                                <th class="text-center" width="15%">Width</th>
                                <th class="text-center" width="10%">Height*Width</th>
                                <th class="text-center" width="10%" ></th>
								<th class="text-center" width="10%"></th>
                            </tr>
                        </thead>
						<tbody id="addinvoiceItem_test">
						
						 <tr>
                                <td>
                                    <select  id='item_name_order' name='item_name_order[0][]' class='form-control select2' tabindex='3' data-placeholder='-- select one --' required>
                                      
                                    </select>
                                </td>
								<td><input type="text" class='form-control' id="i_width" value="" readonly="" /></td>
								 <td><input type="text" class='form-control' id="i_height" value="" readonly="" /></td>
                                <td class="text-right"><input type="text" class='form-control' id="i_total" value="" readonly="" /></td>
                                <td class="text-right"></td>
								<td class="text-right"></td>
                            </tr>
						</tbody>
                        <thead>
						
                            <tr>
                                <th class="text-center" width="15%">Material</th>
                                <th class="text-center" width="15%">Color</th>
                                <th class="text-center" width="15%">Pattern</th>
								<th class="text-center" width="10%">Available Quantity/Unit</th>
                                <th class="text-center" width="10%">Quantity/Unit</th>
                                <?php if(!empty($packageid)){?>
                                <th class="text-center" width="10%">Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <tr>
                                <td>
                                    <select  id='rmtt_id_1' onchange='service_cals(1)' name='rmtt_id[0][]' class='form-control select2' tabindex='3' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php foreach ($rmtts as $val) { ?>
                                            <option value='<?= $val->id ?>'> <?= $val->material_name; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='color_id_1' onchange="color_cals(1)" name='color_id[0][]' class='form-control select2' tabindex='4' data-placeholder='-- select one --' required>
                                        <option value=''>-- select one --</option>
                                        <?php // foreach ($colors as $val) { ?>
                                            <!--<option value='<?= $val->id ?>'> <?= $val->color_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>

                                <td>
                                    <select  id='pattern_model_id_1' name='pattern_model_id[0][]' class='form-control select2' tabindex='5' data-placeholder='-- select one --' required>
                                        <option value=''></option>
                                        <?php // foreach ($patern_model as $val) { ?>
                                            <!--<option value='<?= $val->pattern_model_id ?>'> <?= $val->pattern_name; ?></option>-->
                                        <?php // } ?>
                                    </select>
                                </td>
								 <td class="text-right">
                                    <input type="text" name="available_qnt[0][]"  class="form-control text-center"  id="available_qnt_1" tabindex="6" min="0">
                                </td>
                                <td class="text-right">
                                    <input type="text" name="quantity[0][]"  class="form-control text-center" onkeyup="quantity_check('1')" id="quantity_1" tabindex="6" min="0">
                                </td>
                                <td class="text-right">
                                    <?php if(!empty($packageid)){?>
                                        <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <button class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                    <?php }?>
                                    <?php } ?>
                                </td>
                            </tr>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="5" rowspan="1">
                                     <?php if(!empty($packageid)){?>
                                         <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addinvoiceItem',0);" value="Add Material" type="button" style="margin: 0px 15px 15px;">
									  <?php } ?>   
                                      <?php } ?>   
                                </td>
                            </tr>
                        </tfoot>


                    </table>   
					 <?php if(!empty($packageid)){?>
                        <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
					<a href="javascript:void(0)" class="btn btn-info add_new_data" id="add_new_data_id" data-cnt="1" style="float:right;">Add Item</a>   
                    <?php } ?>    
                     <?php } ?>  
                </div>

            </div>
            <br/>
            <div class="form-group text-right mt-2">
                 <?php if(!empty($packageid)){?>
                     <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <input type="submit" id="add_purchase" class="btn btn-primary default btn-sm" name="add-usage" value="Save "/>
                <?php } ?>
                 <?php } ?>
            </div>
        </form>
    </div>
</div>
<!-- end content / right -->
<script type="text/javascript">
$(document).ready(function(){
$("#get_item_data").change(function(){
var item_order_id=$("#get_item_data").val();
			$.ajax({
			url: "b_level/Row_materials/get_item_details/",
			type: "POST",
			data: {item_order_id: item_order_id},
			success: function (r) {
				$("#item_name_order").html(r);
			}
			});
});

$("#item_name_order").change(function(){
var item_raw_id=$("#item_name_order").val();
			$.ajax({
			url: "b_level/Row_materials/get_item_dimension/",
			type: "POST",
			data: {item_raw_id: item_raw_id},
			success: function (result) {
				obj = jQuery.parseJSON(result);
				
				$("#i_width").val(obj.get_width);
				$("#i_height").val(obj.get_height);
				$("#i_total").val(obj.get_total);
			}
			});
});

});
</script>
