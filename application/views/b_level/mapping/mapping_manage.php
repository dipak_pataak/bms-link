
<!-- content / right -->
<div id="right">
    <!-- table -->

    <div class="box">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>
        <div class="title">
            <h5>Mapping of Measurement</h5>
        </div>
        <div class="px-3">
            <form action="<?php echo base_url(); ?>b_level/Mapping_controller/save_mapping" id="menusetupFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Unit</th>
                                <?php foreach ($unit_data as $key => $unit) { ?>
                                    <th><?=$unit->uom_name?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $temp_unit_data = $unit_data; ?>
                            <?php foreach ($unit_data as $row_key => $row_unit) { ?>
                                <tr>
                                    <td><?=$row_unit->uom_name?></td>
                                    <?php foreach ($temp_unit_data as $col_key => $col_unit) { 
                                        $measure_data = $this->mapping->get_mapping_data($row_unit->uom_id,$col_unit->uom_id);
                                        // echo "<pre>";print_r($measure_data);die;
                                        ?>
                                       <td>
                                            <input type="hidden" name="mapping_id[]" value="<?=isset($measure_data->id)?$measure_data->id:''?>">
                                            <input type="hidden" name="from_measure_id[]" value="<?=$row_unit->uom_id?>">
                                            <input type="hidden" name="to_measure_id[]" value="<?=$col_unit->uom_id?>">
                                            <input type="text" name="measure_value[]" class="form-control" value="<?=isset($measure_data->measure_value)?$measure_data->measure_value:''?>" required>
                                       </td> 
                                    <?php } ?>
                                </tr>
                            <?php } ?>    
                        </tbody>    
                    </table>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary btn-large" name="save" value="Save" />
                    </div>
                </div>        
            </form>            
        </div>        
    </div>
</div>
<!-- end content / right -->
