<form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Color_controller/manage_action') ?>">
    <input type="hidden" name="action">
	<div class="table-responsive">
    <table class="table table-bordered mb-3">
        <thead>
            <tr>
                <th><input type="checkbox" id="SellectAll"/></th>
                <th>Sl No.</th>
                <th>Color Name</th>
                <th>Color Number</th>
                <th>Pattern Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            if (!empty($colors)) {
                foreach ($colors as $key => $val) {
                    $i++;
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->id; ?>" class="checkbox_list">  
                        </td>
                        <td><?= $i ?></td>
                        <td><?= $val->color_name; ?></td>
                        <td><?= $val->color_number; ?></td>
                        <td><?= $val->pattern_name; ?></td>
                        <td width="100" class="text-right">
                            <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" ><i class="fa fa-pencil"></i></a>
                            <a href="<?php echo base_url('b_level/color_controller/delete_color/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>
                        </td>

                    </tr>

                    <?php
                }
            }
            ?>


        </tbody>
        <?php if (empty($colors)) { ?>
            <tfoot>
                <tr>
                    <th colspan="6" class="text-center text-danger">No record found!</th>
                </tr> 
            </tfoot>
        <?php } ?>
    </table>
	</div>
</form>