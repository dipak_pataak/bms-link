<?php $packageid=$this->session->userdata('packageid'); ?>
 <?php $menu_permission= b_access_role_permission(63); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or-filter .col-sm-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
	#colorListTable td:last-child {
		text-align: right;
	}
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 25%;
			max-width: 25%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Manage Color</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Color';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>

                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 float-right text-right">
                <!--                <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_color">Add New Color</a>-->
            </div>
        </div>
        <div class="" style="margin: 10px;">
			<button class="btn btn-primary default" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="filter-collpase-btn">
                Filter
            </button>
            <?php if(!empty($packageid)){?>
            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#importColor">Bulk Import Upload</button>
            <a href="<?=base_url('export-wholesaler-color')?>" class="btn btn-info btn-sm mb-1">Export</a>
            <a href="javascript:void" class="btn btn-success" id="add_new_color">Add Color</a>
            <?php } ?>
             <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)">Delete</a>
            <?php } ?>
            <?php } ?>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div class="collapse px-4 mt-3" id="collapseExample">
            <div class="border px-3 pt-3 mb-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>color-list-filter" method="post" id="colorFilterFrm">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control colorname" placeholder="Colors Name" name="colorname" id="colorname" value="<?=$this->session->userdata('search_colorname')?>">
                            </div><div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control colornumber" placeholder="Colors Number" name="colornumber" id="colornumber" value="<?=$this->session->userdata('search_colornumber')?>">
                            </div><div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" placeholder="Pattern Name" name="pattern_name" id="pattern_name" value="<?=$this->session->userdata('search_pattern_name')?>">
                            </div>
                            <div class="col-sm-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>

        <!--<div class="col-sm-12 mb-3 text-right">
            <div class="form-group row m-0 search_div float-right">
                <label for="keyword" class="col-form-label text-right"></label>
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="colorkeyup_search()" placeholder="Search..." tabindex="">
            </div>          
        </div>-->

        <div id="results_color">
            <form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Color_controller/manage_action') ?>">
            <input type="hidden" name="action">
				<div class="table-responsive">
                <table id="colorListTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Color Name</th>
                            <th>Color Number</th>
                            <th>Pattern Name</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importColor" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Colors Bulk Upload</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <a href="<?php echo base_url('assets/b_level/csv/color_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                    <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('import-color-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->


<!-- end content / right -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_color').on('click', function () {
            $(".edit_color_box").hide();
            $(".add_color_box").show();
            $('#add_color')[0].reset();
            $("#add_color").attr("action", 'b_level/color_controller/save_color');
            $('#pattern_id').val('').trigger('change');
            $('#myColor').modal('show');

            $('.modal-title').text('Add New Color');
            $('#save').text('Add Color');
            $("#prev_color_img").attr('src','<?=base_url('assets/no-image.png')?>');
            $('[name="hid_color_img"]').val('');
            
//            $('.modal-title').text('Add New Color');
        });


        $('#results_color').on('click', '.edit_color', function () { 
                   
            //$("#pattern_id").removeAttr('multiple');
            $(".edit_color_box").show();
            $(".add_color_box").hide();
            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/color_controller/get_color/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_color')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="color_name"]').val(data.color_name);
                    $('[name="color_number"]').val(data.color_number);
                    if(data.color_img != ''){
                        $('[name="hid_color_img"]').val(data.color_img);
                        $("#prev_color_img").attr('src',data.color_img);
                    }
                    $('#edit_pattern_id').val(data.pattern_id).trigger('change');

                    $("#add_color").attr("action", 'b_level/color_controller/update_color')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });
    });
   function colorkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            //url: "<?php echo base_url(); ?>wholesaler-color-search",
            url: "<?php echo base_url(); ?>color-list-filter",
            type: 'post',
            data: {action:'SearchBox', keyword: keyword},
            success: function (r) {
                $("#results_color").html(r);
            }
        });
    }
    $(document).ready(function(){
        // For Open filter Box : START
        var colorname = $("#colorFilterFrm input[name=colorname]").val();
        var colornumber = $("#colorFilterFrm input[name=colornumber]").val();

        if(colorname != '' || colornumber != '') {
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
        var orderable = [0,4];
        var lengthMenu = [[10, 25, 50, 1000], [10, 25, 50, 1000]]
        init_datatable('colorListTable','<?php echo base_url('wholesaler-getColorLists/'); ?>',orderable, lengthMenu);
    });
</script>


<?php
$this->load->view('b_level/color/colorJs');
?>




