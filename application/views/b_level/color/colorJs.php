<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .pattern_select .select2-container {
        width: 100% !important;
    }
</style>
<div id="myColor" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Color</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">



                <?php
                $attributes = array('id' => 'add_color', 'name' => 'add_color', 'class' => 'form-row px-3', 'enctype' => 'multipart/form-data');
                echo form_open('#', $attributes);
                ?>


                <div class="col-lg-12 px-4">

                    <div class="form-group">
                        <label for="color_name" class="mb-2">Color Name</label>
                        <input class="form-control" type="text" name="color_name" id="color_name" required>
                         <span id="error"></span>
                    </div>

                    <div class="form-group">
                        <label for="color_number" class="mb-2">Color Number</label>
                        <input class="form-control" type="text" name="color_number" id="color_number">
                    </div>

                    <div class="form-group pattern_select add_color_box">
                        <label for="pattern_id" class="mb-2">Pattern</label>
                        <select class="selectpicker form-control" id="pattern_id" name="pattern_id[]" multiple data-live-search="true">
                        <?php
                                foreach ($patterns as $pattern) { ?>
                                    <option value="<?=$pattern->pattern_model_id?>"><?=$pattern->pattern_name ?></option>
                            <?php } ?>
                        </select>
                        <span id="pattern_error"></span>
                    </div>

                    <div class="form-group pattern_select edit_color_box" style="display:none">
                        <label for="pattern_id" class="mb-2">Pattern</label>
                        <select class="selectpicker form-control" id="edit_pattern_id" name="edit_pattern_id[]" data-live-search="true">
                        <?php
                                foreach ($patterns as $pattern) { ?>
                                    <option value="<?=$pattern->pattern_model_id?>"><?=$pattern->pattern_name ?></option>
                            <?php } ?>
                        </select>
                        <span id="edit_pattern_error"></span>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-8">
                                <label for="color_img" class="mb-2">File Upload</label>
                                <input type="file" class="form-control" name="color_img" id="color_img">
                                <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                <input type="hidden" name="hid_color_img">
                            </div>
                            <div class="col-sm-4">
                                <img src="<?=base_url('assets/no-image.png')?>" id="prev_color_img" width="100px" height="100px">
                            </div>
                        </div>
                    </div>

                </div>
                <input type="hidden" name="id" id="id" value="">


                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Add Color</button>
                    </div>
                </div>

                <?php echo form_close(); ?>



            </div>

            

        </div>

    </div>
</div>

<script>
    $("body").on("change", "#color_img", function(e) {
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                 swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                swal("Please upload file less than 2MB. Thanks!!", {
                    icon: "error",
                });
                $(this).val('');
            }
        }    

        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev_color_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#prev_color_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }
</script>    