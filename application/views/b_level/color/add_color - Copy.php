<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or-filter .col-sm-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 25%;
			max-width: 25%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Manage Color</h5>
            <div class="col-sm-6 float-right text-right">
                <!--                <a href="javascript:void" class="btn btn-success mt-1 " id="add_new_color">Add New Color</a>-->
            </div>
        </div>
        <div class="" style="margin: 10px;">
            <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="filter-collpase-btn">
                Filter
            </button>
            <button type="button" class="btn btn-info btn-sm mb-2" data-toggle="modal" data-target="#importColor">Bulk Import Upload</button>
            <a href="javascript:void" class="btn btn-success" id="add_new_color">Add Color</a>
            <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)">Delete</a>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            $message = $this->session->flashdata('message');
            if ($message)
                echo $message;
            ?>
        </div>
        <div class="collapse px-4 mt-3" id="collapseExample">
            <div class="border px-3 pt-3 mb-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>color-list-filter" method="post" id="colorFilterFrm">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control colorname" placeholder="Colors Name" name="colorname" id="colorname" value="<?=$this->session->userdata('search_colorname')?>">
                            </div><div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control colornumber" placeholder="Colors Number" name="colornumber" id="colornumber" value="<?=$this->session->userdata('search_colornumber')?>">
                            </div>
                            <div class="col-sm-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
        <div class="col-sm-12 text-right">
            <div class="form-group row m-0 mb-3 search_div float-right">
                <label for="keyword" class="col-form-label text-right"></label>
                    <form class="form-horizontal" action="<?php echo base_url(); ?>color-list-filter" method="post" id="colorFilterFrm">
                        <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Clear</button>
                        <button type="submit" class="btn btn-sm btn-success default" name="action" value="Search" >Go</button>
                        <input type="text" class="form-control" name="keyword" id="keyword" value="<?=$this->session->userdata('search_keyword')?>" placeholder="Search..." tabindex="">
                    </form>
            </div>          
        </div>
        <div id="results_color">
            <form class="p-3" name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Color_controller/manage_action') ?>">
            <input type="hidden" name="action">
				<div class="table-responsive">
                <table class="table table-bordered mb-3">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>Sl no.</th>
                            <th>Color Name</th>
                            <th>Color Number</th>
                            <th>Pattern Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $i = 0 + $pagenum;
                        if (!empty($colors)) {
                            foreach ($colors as $key => $val) {
                                $i++;
                                ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $val->id; ?>" class="checkbox_list">  
                                    </td>
                                    <td><?= $i ?></td>
                                    <td><?= $val->color_name; ?></td>
                                    <td><?= $val->color_number; ?></td>
                                    <td><?= $val->pattern_name; ?></td>

                                    <td width="100">
                                        <a href="javascript:void(0)" class="btn btn-warning default btn-sm edit_color" id="edit_color" data-data_id="<?= $val->id ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="<?php echo base_url('b_level/color_controller/delete_color/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>

                                <?php
                            }
                        }
                        ?>


                    </tbody>
                    <?php if (empty($colors)) { ?>
                        <tfoot>
                            <tr>
                                <th colspan="6" class="text-center text-danger">No record found!</th>
                            </tr> 
                        </tfoot>
                    <?php } ?>
                </table>
                </div>
				<?php echo $links; ?>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importColor" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Colors Bulk Upload</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <a href="<?php echo base_url('assets/b_level/csv/color_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                    <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('import-color-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end content / right -->


<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_color').on('click', function () {
            $(".edit_color_box").hide();
            $(".add_color_box").show();
            $('#add_color')[0].reset();
            $("#add_color").attr("action", 'b_level/color_controller/save_color');
            $('#pattern_id').val('').trigger('change');
            $('#myColor').modal('show');
            
//            $('.modal-title').text('Add New Color');
        });


        $('#results_color').on('click', '.edit_color', function () { 
                   
            //$("#pattern_id").removeAttr('multiple');
            $(".edit_color_box").show();
            $(".add_color_box").hide();
            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "b_level/color_controller/get_color/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_color')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="color_name"]').val(data.color_name);
                    $('[name="color_number"]').val(data.color_number);
                    $('#edit_pattern_id').val(data.pattern_id).trigger('change');

                    $("#add_color").attr("action", 'b_level/color_controller/update_color')

                    $('#myColor').modal('show');
                    $('.modal-title').text('Update Color');
                    $('#save').text('Update Color');


                }, error: function () {

                }
            });

        });
    });
   function colorkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            //url: "<?php echo base_url(); ?>wholesaler-color-search",
            url: "<?php echo base_url(); ?>color-list-filter",
            type: 'post',
            data: {action:'SearchBox', keyword: keyword},
            success: function (r) {
                $("#results_color").html(r);
            }
        });
    }
    $(document).ready(function(){
        // For Open filter Box : START
        var colorname = $("#colorFilterFrm input[name=colorname]").val();
        var colornumber = $("#colorFilterFrm input[name=colornumber]").val();

        if(colorname != '' || colornumber != '') {
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
    })
</script>


<?php
$this->load->view('b_level/color/colorJs');
?>




