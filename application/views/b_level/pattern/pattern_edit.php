<form action="<?php echo base_url('b_level/Pattern_controller/pattern_update/' . $pattern_edit[0]['pattern_model_id']); ?>" method="post" class="form-row px-3">
    <div class="col-sm-12">        
        <div class="form-group">
            <label for="category_id" class="mb-2">Category Name<sup class="text-danger">*</sup></label>
            <select class="form-control select2" id="category_id" name="category_id" data-placeholder='-- select one --' required>
                <!-- <option value=" ">None</option> -->
                <?php foreach ($parent_category as $category) { ?>
                    <option value="<?php echo $category->category_id; ?>" <?php
                    if ($category->category_id == $pattern_edit[0]['category_id']) {
                        echo 'selected';
                    }
                    ?>>
                        <?php echo $category->category_name; ?></option>
                <?php }
                ?>
            </select>
        </div>
        <!--        <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" style="width: 75px;">
                            <input type="radio" class="form-check-input"id="pattern" value="Pattern" <?php
        if ($pattern_edit[0]['pattern_type'] == 'Pattern') {
            echo 'checked';
        }
        ?> name="pattern_type">Pattern
                        </label>
                        <label class="form-check-label" style="width: 75px;">
                            <input type="radio" class="form-check-input"  id="model" value="Model" <?php
        if ($pattern_edit[0]['pattern_type'] == 'Model') {
            echo 'checked';
        }
        ?> name="pattern_type"> Model
                        </label>
                    </div>
                </div>-->

        <div class="form-group">
            <label for="product_code" class="mb-2">Name<sup class="text-danger">*</sup></label>
            <input class="form-control" type="text" name="pattern_name" value="<?php echo $pattern_edit[0]['pattern_name']; ?>" id="pattern_name" required>
            <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
        </div>
        <div class="form-group">
            <label for="status" class="mb-2">Status</label>
            <select class="form-control select2" id="status" name="status" data-placeholder='-- select one --'>
                <!--<option value=""></option>-->
                <option value="1" <?php
                if ($pattern_edit[0]['status'] == 1) {
                    echo "selected";
                }
                ?>>Active</option>
                <option value="0"  <?php
                if ($pattern_edit[0]['status'] == 0) {
                    echo "selected";
                }
                ?>>Inactive</option>
            </select>
        </div>
        <div class="form-group text-left">
            <input type="hidden" name="pattern_model_id" id="pattern_model_id" value="<?php echo $pattern_edit[0]['pattern_model_id']; ?>">
            <button type="submit" class="btn btn-success w-md m-b-5">Update Pattern</button>
        </div>
    </div>

</form>
<script type="text/javascript">
    $(document).ready(function () {
        style_path = "resources/css/colors";
    });
    function hideData(loc) {
        document.getElementById(loc).innerHTML = '&nbsp;';
    }
//========== its for assigned product delete ===========
    function assingned_product_delete(product_id) {
        var pattern_model_id = $("#pattern_model_id").val();
        $.ajax({
            url: "<?php echo base_url(); ?>wholesaler-assinged-product-delete",
            type: 'post',
            data: {pattern_model_id: pattern_model_id, product_id: product_id},
            success: function (r) {
//                $("#results_category").html(r);
                swal("assigned remove successfully!");
            }
        });
    }
    $("#pattern_name").on('mouseout', function (e) {

        $('#pattern_name').val(($('#pattern_name').val().replace(/['"]+/g, '&quot;')));

    });

</script>
