<?php $package=$this->session->userdata('packageid'); ?>
<!--============ its for multiselects ============-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />-->
<!-- content / right -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<style type="text/css">
	#PatternDataTable td:last-child {
		text-align: left;
	}
	.select2.select2-container {
			width: 100% !important;
	}
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
	.or-filter .col-sm-3, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
    }
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 18.7%;
			max-width: 18.7%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6"> 
                <h5>Manage Pattern</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Pattern';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <!-- end box / title -->
        <div>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
        <?php $menu_permission= b_access_role_permission(16); ?>
        <p class="px-3">
            <?php if (!empty($package)){?>
             <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
             <button type="button" class="btn btn-info btn-sm mb-1" data-toggle="modal" data-target="#importPattern">Bulk Import Upload</button>   
             <a href="<?=base_url('export-wholesaler-pattern')?>" class="btn btn-info btn-sm mb-1">Export</a>
             <?php }?>            
            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <button type="button" class="btn btn-success  mb-1" data-toggle="modal" data-target="#patternFrm">Add Pattern</button>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onClick="return action_delete(document.recordlist)">Delete</a>
            <?php } ?>
            <?php }?> 


        </p>
        <div class="collapse px-3 mt-3" id="collapseExample">
            <div class="border px-3 pt-3 mb-3">
                <form class="form-horizontal" action="<?php echo base_url(); ?>pattern-model-filter" method="post">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control pattern_name" name="pattern_name" placeholder="Enter Pattern Name">
                            </div>
                            <div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="parent_cat" class="form-control select2 parent_cat" id="parent_cat" data-placeholder="-- select category --">
                                        <option value=""></option>
                                        <?php foreach ($parent_category as $category) { ?>
                                            <option value='<?php echo $category->category_id; ?>' <?php
                                            ?>><?php echo $category->category_name; ?></option>";
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="pattern_status" class="form-control pattern_status select2" id="pattern_status" data-placeholder="-- select status --">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="patternFrm" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pattern Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url('b_level/Pattern_controller/pattern_save'); ?>" method="post" class="">
                            <div class="form-group row">
                                <label for="category_id" class="col-sm-3 mb-1 control-label">Category Name<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <select name="category_id" class="form-control select2" id="category_id" data-placeholder="-- select one --" required>
                                        <!-- <option value=" ">None</option> -->
                                        <?php foreach ($parent_category as $val) { ?>
                                            <option value="<?= $val->category_id ?>"><?= ucwords($val->category_name); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pattern_name" class="col-sm-3 mb-1 control-label">Name<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" name="pattern_name" id="pattern_name" required>
                                    <input type="radio" class="custom-control-input" id="pattern" value="Pattern" name="pattern_type" checked>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="status" class="col-sm-3 mb-1 control-label">Status</label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control select2" id="parent_category" data-placeholder="-- select one --">
                                        <option value=""></option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="offset-3 col-md-2">
                                    <button type="submit" class="btn btn-success w-md m-b-5 float-right">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive px-3" id="results_pattern">
        <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Pattern_controller/manage_action') ?>">
            <input type="hidden" name="action">
			<div class="table-responsive">
            <table id="PatternDataTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="SellectAll"/></th>
                        <th>SL No.</th>
                        <th>Pattern Name</th>
                        <th>Category Name</th>
                        <th>Assigned Product</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
			</div>
		</form>
        </div>
        <div class="modal fade" id="pattern_model_info" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Pattern Information</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="pattern_info">

                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                    </div>
                </div>
            </div>
        </div>

         <!-- Modal -->
        <div class="modal fade" id="importPattern" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pattern Bulk Upload</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <a href="<?php echo base_url('assets/b_level/csv/pattern_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                        <?php echo form_open_multipart('import-pattern-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                        </div>

                        </form>
                    </div>
                   <!--  <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>


    </div>
</div>
<!-- end content / right -->
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    function pattern_edit_form(id) {
        $.post("<?php echo base_url(); ?>pattern-edit/" + id, function (t) {
            $("#pattern_info").html(t);
            $('#pattern_model_info').modal('show');
        });
    }

    $("#pattern_name").on('mouseout', function (e) {

        $('#pattern_name').val(($('#pattern_name').val().replace(/['"]+/g, '&quot;')));

    });
    $(document).ready(function() {
        var orderable = [0, 1, 4, 5, 6];
        var dt = init_datatable('PatternDataTable','<?php echo base_url('wholesaler-getPatternLists/'); ?>', orderable);
    });
</script>
