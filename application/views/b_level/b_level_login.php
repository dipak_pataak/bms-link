<!DOCTYPE html>
<html lang="en">
    <head>
        <title>BMS Link</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/style.css" media="screen" />
        <link id="color" rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/colors/blue.css" />

    </head>
    <body>
        <div id="login">

             <div class="p-1">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>

            <!-- login -->
            <div class="title">
                <h5>Sign In to BMSLink</h5>
                <div class="corner tl"></div>
                <div class="corner tr"></div>
            </div>



            <div class="inner">
               
                <form action="<?php echo base_url(); ?>b_level/auth_controller/authentication" method="post">
                    <div class="form">
                        <!-- fields -->
                        <div class="fields">

                            <div class="field">
                                <div class="label">
                                    <label for="username">Username:</label>
                                </div>
                                <div class="input">
                                    <input type="text" id="username" name="email" size="40" autocomplete="off" class="focus" value="<?php echo get_cookie("email"); ?>" required />
                                </div>
                            </div>

                            <div class="field">
                                <div class="label">
                                    <label for="password">Password:</label>
                                </div>
                                <div class="input">
                                    <input type="password" id="password" name="password" size="40" class="focus" value="<?php echo get_cookie("password"); ?>" required />
                                    <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password" style="position: relative; left: -20px; top: 8px;"></span>
                                </div>
                            </div>

                            <div class="field">
                                <div class="checkbox">
                                    <input type="checkbox" id="remember" name="remember" value="1" />
                                    <label for="remember">Remember me</label>
                                </div>
                            </div>

                            <div class="buttons">
                                 <a class="text-left" href="<?php echo base_url(); ?>wholesaler-forgot-password-form">Forgot your password?</a>
                                <input type="submit" class="btn btn-success text-right"  value="Login">
                            </div>
                        </div>
                        <!-- end fields -->
                    </div>
                </form>
            </div>

        </div>

        <!-- scripts (jquery) -->
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/b_level/resources/scripts/smooth.js"></script>
        <script>
            $(document).ready(function () {
                //    ============ its for show password ===============
                $(".toggle-password").click(function () {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                });
            });
        </script>
    </body>
</html>