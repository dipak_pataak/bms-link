<style type="text/css">
    .right_side{float: left; font-weight: bold; }
    .left_side{float: right; font-weight: bold; }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    a:hover{
        text-decoration: none;
    }
    .address{
        cursor: pointer;
    }
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>
        <!-- box / title -->
        <div class="title">
            <h5>Catalog request</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Catalog request';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <!-- end box / title -->

        <div>
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>

        </div>
 <?php $menu_permission= b_access_role_permission(124); ?>
        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Customer_controller/manage_action') ?>">
                <input type="hidden" name="action">
				<div class="table-responsive">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="13%">User Name</th>
                        <th width="15%">Company</th>
                        <th width="15%">Phone</th>
                        <th width="15%">Email</th>
                        <th width="22%">Address</th>
                        <th width="10%">Request remark</th>
                        <th width="10%">Status</th>
                        <th width="15%" class="text-center">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    if(isset($catalog_request_list))
                    {
                        foreach ($catalog_request_list as $value)
                        {
                            $sl++;

                            ?>
                            <tr>

                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->full_name; ?></td>
                                <td>
                                    <a href="javascript:void(0)">
                                        <?php echo $value->company; ?>
                                    </a>
                                </td>


                                <td>
                                    <a href="tel:<?php echo $value->phone; ?>"><?php echo $value->phone; ?></a>
                                </td>
                                <td>
                                    <a href="mailto:<?php echo $value->email; ?>"><?php echo $value->email; ?></a>
                                </td>
                                <td>
                                    <a href="javascript:void(0)"
                                       class="address"
                                    >
                                        <?php echo $value->address . '<br>' . @$value->city . ', ' . @$value->state . ', ' . @$value->zip_code . ', ' . @$value->country_code; ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo $value->remark;?>
                                </td>
                                <td>
                                    <a href="javascript:void(0)"
                                       class=""
                                    >
                                        <?php

                                        if($value->status == 0)
                                        {
                                            echo "Not approved";
                                        }
                                        else{
                                            echo "Approved";
                                        }
                                        ?>
                                    </a>
                                </td>

                                <td class="text-right">
                                     <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?php echo base_url().'catalog-request-product/'.$value->request_id;?>"
                                       class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Approve request">
                                        <i class="fa fa-check"></i>
                                    </a>
                                     <?php }?>
                                   <!-- <?php /*if($value->status == 0) { */?>
                                    <a href="#"
                                       onclick="show_request_approve_modal('<?php /*echo $value->request_id;*/?>')"
                                       class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                       data-original-title="Approve request">
                                        <i class="fa fa-check"></i>
                                    </a>
                                    <?php /*}else { */?>
                                        <a href="#"
                                           onclick="show_request_approve_modal('<?php /*echo $value->request_id;*/?>')"
                                           class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top"
                                           data-original-title="Approve request">
                                            <i class="fa fa-check"></i>
                                        </a>

                                    --><?php /*} */?>

                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                    </tbody>

                    <?php if (!isset($catalog_request_list) || empty($catalog_request_list)) { ?>
                        <tfoot>
                        <tr>
                            <th colspan="8" class="text-center  text-danger">No record found!</th>
                        </tr>
                        </tfoot>
                    <?php } ?>
                </table>
				</div>
			</form>
            <?php echo $links; ?>
        </div>


   <!--     <div class="modal fade" id="request_catalog_modal" role="dialog">

            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Approve catalog request</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >
                        <form  action="<?php /*echo base_url('/approve-catalog-request');*/?>" method="post">

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Select product *</label>
                                <div class="col-sm-6">
                                    <select class="form-control selectpicker show-tick select-all" data-selected-text-format="count > 2"  required name="approve_products[]" id="approve_products" multiple data-live-search="true">
                                        <option value="[all]" class="select-all">All products</option>
                                        <option value="" data-divider="true"></option>

                                        <?php /*if(isset($product_list)){
                                            foreach ($product_list as $value) { */?>

                                            <option <?php /*if(isset($value->checked_status) && $value->checked_status == 1) { echo "selected"; }*/?> value='<?php /*echo $value->product_id; */?>' <?php
/*                                            */?>>
                                                <?php /*echo $value->product_name; */?>
                                            </option>
                                        <?php /*} } */?>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="request_id" id="request_id">


                            <div class="form-group row">
                                <label  class="col-xs-2 control-label"></label>
                                <div class="col-xs-6">
                                    <button class="btn-sm btn-success" id="closeModal">Approve</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>-->

    </div>
</div>


<!-- end content / right -->
<script>

 /*   function show_request_approve_modal(id)
    {
        $('#request_id').val(id);
        $('#request_catalog_modal').modal('show');
    }
*/



</script>
