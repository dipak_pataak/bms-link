<?php $currency = "";
if(isset($user_company_profile[0]->currency) && !empty($user_company_profile[0]->currency))
{
    $currency = $user_company_profile[0]->currency;
}
?>
<style type="text/css">
    .rotate90 {
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
        margin-top: 50px;
    }

    @media print {

        .noprint{display:none !important;visibility:hidden !important;}
    }

</style>


<div id="right">
    <div class="box" id="">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>


        <h5>
            <div class="pull-right">
                <?php if ($shipping == '') { ?>
                    <a href="<?= base_url('catalog-shipment/'); ?><?= $orderd->order_id ?>" class="btn btn-success" >Go Shipment</a>
                <?php } ?>

                <?php if ($orderd->order_stage != 2) { ?>
                    <a href="<?= base_url('payment-wholesaler-to-wholesaler/'); ?><?= $orderd->order_id ?>" class="btn btn-success" >Make Payment</a><!--
                    <a href="<?/*= base_url('catalog-order-view/'); */?><?/*= $orderd->order_id */?>" class="btn btn-success" >Receive Payment</a>-->
                   <!-- <a href="<?/*= base_url('payment-wholesaler-to-wholesaler/'); */?><?/*= $orderd->order_id */?>" class="btn btn-success" >Make Payment</a>-->
                <?php } ?>

                <button type="button" class="btn btn-success" onclick="printContent('printableArea')" >Print</button>
            </div>
            Order info
        </h5>

        <div class="separator mb-3"></div>

        <div id="printableArea">

            <style type="text/css">
                div, p{
                    font-size: 11px !important;
                }
                table tr th{font-size: 12px !important; }
            </style>

            <div class="row ">

                <div class="form-group col-md-6" style="margin-left: 20px;">
                    <div class="">
                        <img src="<?php echo base_url('assets/b_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">
                    </div>
                    <p><?= $company_profile[0]->company_name; ?></p>
                    <p><?= $company_profile[0]->address; ?></p>
                    <p><?= $company_profile[0]->city; ?>,
                        <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>, <?= $company_profile[0]->country_code; ?></p>
                    <p><?= $company_profile[0]->phone; ?></p>
                    <p><?= $company_profile[0]->email; ?></p>
                </div>

                <div class="form-group col-md-5 " style="margin-left: 20px">


                    <table class="table table-bordered mb-4">

                        <tr class="text-center">
                            <td>Order Date</td>
                            <td class="text-right"><?= date_format(date_create($orderd->order_date),'M-d-Y') ?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Order Id</td>
                            <td class="text-right"><?= $orderd->order_id ?></td>
                        </tr>

                        <tr class="text-center">
                            <td>Sidemark</td>
                            <td class="text-right"><?= $orderd->side_mark; ?></td>
                        </tr>



                        <?php if ($shipping != NULL) { ?>
                            <tr class="text-center">
                                <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                                <td class="text-right"> <?= $shipping->track_number ?> </td>
                            </tr>

                        <?php } ?>

                        <tr class="text-center">
                            <td>Barcode</td>
                            <td class="text-right"> <?php if($orderd->barcode!=NULL){
                                    echo '<img src="'.base_url().$orderd->barcode.'" width="250px;"/>';
                                }
                                ?></td>
                        </tr>



                    </table>
                </div>

            </div>

            <div class="row" style="margin-top: 20px;">

                <div class="form-group col-md-6" style="margin-left: 20px;">
                    <strong>Sold To: </strong>
                    <?php //print_r($created_by_data); ?>
                    <?= $created_by_data->company; ?> 
                    <p><?= $created_by_data->address; ?></p>
                    <p><?= $created_by_data->city; ?>,
                        <?= $created_by_data->state; ?>, <?= $created_by_data->zip_code; ?>,
                        <?= $created_by_data->country_code; ?></p>
                    <p><?= $created_by_data->phone; ?></p>
                    <p><?= $created_by_data->email; ?></p>
                </div>

                <div class="form-group col-md-5">
                    <strong>Ship To: </strong><?= $created_by_data->company; ?> 
                    <?php if ($orderd->is_different_shipping == 1) { ?>
                        <p><?= $orderd->different_shipping_address ?></p>

                    <?php } else { ?>

                        <p><?= $created_by_data->address; ?></p>
                        <p><?= $created_by_data->city; ?>, <?= $created_by_data->state; ?>, <?= $created_by_data->zip_code; ?>,
                            <?= $created_by_data->country_code; ?></p>
                        <p><?= $created_by_data->phone; ?></p>
                        <p><?= $created_by_data->email; ?></p>

                    <?php } ?>
                </div>
               <!-- <a href="<?php /*echo base_url(); */?>order-customer-info-edit/<?php /*echo $orderd->order_id; */?>" class="btn btn-info btn-sm noprint" style="height: 30px; margin-right: 10px;"><i class="fa fa-edit"></i></a>-->

            </div>

            <h5 style="margin-left: 15px;">Order Details</h5>
            <div class="separator mb-3"></div>
            <div class="px-3" id="cartItems">
                <table class="datatable2 table table-bordered table-hover mb-4">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Description</th>
                        <th>List</th>
                        <th>Discount(%)</th>
                        <th>Price</th>
                        <th>Notes</th>
                        <!--<th class="noprint">Action</th>-->
                    </tr>
                    </thead>

                    <tbody>

                    <?php $i = 1; ?>

                    <?php

                    $convert_price = 1;
                     if(isset($order_details[0]->b_user_id) && !empty($order_details[0]->b_user_id) && isset($order_details[0]->created_by) && !empty($order_details[0]->created_by))
                         $convert_price = convert_price($order_details[0]->b_user_id,$order_details[0]->created_by);



                    foreach ($order_details as $items):
                        $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                        $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                        ?>

                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $items->product_qty; ?></td>
                            <td width="400px;">
                                <strong><?= $items->product_name; ?></strong><br/>
                                <?= $items->pattern_name; ?><br/>


                                <?php  
                                    $converted_width = convert_unit($items->width,$items->created_by,$items->b_user_id); 
                                    $convert_width = explode(".",$converted_width);

                                    $converted_height = convert_unit($items->height,$items->created_by,$items->b_user_id); 
                                    $convert_height = explode(".",$converted_height);
                                ?>

                                W <?= $convert_width[0]; ?> <?= @$width_fraction->fraction_value ?> <?php echo $user_company_profile[0]->unit; ?>,
                                H <?= $convert_height[0]; ?> <?= @$height_fraction->fraction_value ?> <?php echo $user_company_profile[0]->unit; ?>,


                                <br>
                                ( W <?= $items->width ?> <?php echo $company_profile[0]->unit; ?>,
                                H <?= $items->height ?> <?php echo $company_profile[0]->unit; ?> ),


                                <?= $items->color_number; ?>
                                <?= $items->color_name; ?>

                            </td>

                            <td>
                                <?= $currency; ?><?php echo  number_format($items->list_price*$convert_price, 2); ?> 
                                <br>
                                (<?php echo $company_profile[0]->currency; ?> <?php echo $items->list_price; ?>)
                            </td>
                            <td><?= $items->discount ?> %</td>
                            <td><?= $currency; ?><?php echo  number_format($items->unit_total_price*$convert_price, 2); ?>
                                <br>(<?php echo $company_profile[0]->currency; ?> <?php echo $items->list_price; ?>)
                            </td>
                            <td><?= $items->notes ?></td>
                            <!--                                <td class="noprint">
                                    <a href="<?php //echo base_url(); ?>order-single-product-edit/<?php //echo $items->row_id;  //echo $orderd->order_id ."/". $items->product_id; ?>" class="btn btn-info btn-sm" style="height: 30px; margin-right: 10px;"><i class="fa fa-edit"></i></a>
                                </td>-->

                        </tr>

                        <?php $i++; ?>

                    <?php endforeach; ?>

                    </tbody>
                </table>



                <table class="datatable2 table table-bordered table-hover mb-4">

                    <thead>
                    <tr>
                        <?php if ($shipping != NULL) { ?>
                            <th>Shipping cost</th>
                        <?php } ?>

                        <th>Sub-Total</th>
                        <th>Installation Charge</th>
                        <th>Other Charge</th>
                        <th>Misc</th>
                        <th>Discount</th>
                        <th>Grand Total</th>
                        <th>Deposit</th>
                        <th>Due </th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if ($shipping != NULL) { ?>
                        <td class="text-right"> <?= $currency; ?> <?php echo  number_format($shipping->shipping_charges, 2); ?> </td>
                    <?php } ?>
                    <!-- <td class="text-right"> <?= $currency; ?><?= $orderd->state_tax ?></td> -->

                    <td class="text-right"> <?= $currency; ?><?php echo  number_format($orderd->subtotal*$convert_price, 2); ?> 
                    <br>
                    (<?php echo $company_profile[0]->currency; ?> <?php echo $orderd->subtotal; ?>)
                    </td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->installation_charge ?> </td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->other_charge ?></td>
                    <td class="text-right"> <?= $currency; ?><?= $orderd->misc ?> </td>
                    <td class="text-right"> <?= $currency; ?><?php echo  number_format($orderd->invoice_discount,2); ?></td>
                    <td class="text-right"> <?= $currency; ?><?php echo  number_format($orderd->grand_total*$convert_price, 2); ?>
                                                             <br>
                                                             (<?php echo $company_profile[0]->currency; ?> <?php echo $orderd->grand_total; ?>)</td>
                    <td class="text-right"> <?= $currency; ?><?php echo  number_format($orderd->paid_amount*$convert_price, 2); ?></td>
                    <td class="text-right"> <?= $currency; ?><?php echo  number_format($orderd->due*$convert_price, 2); ?>
                                                             <br>(<?php echo $company_profile[0]->currency; ?> <?php echo $orderd->due; ?>)</td>
                    </tbody>

                </table>

                <?php
                if ($shipping != NULL) {
                    if ($shipping->method_name == 'UPS') {
                        ?>

                        <div class="col-lg-6 offset-lg-4 noprint">
                            <!--<a href="#"  data-toggle="modal" data-target="#order_logo">-->
                            <a href="<?php echo base_url(); ?>order-logo-print/<?php echo $this->uri->segment(4); ?>" target="_new">
                                <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="" height='250' >
                            </a>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="order_logo" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">

                                    <div class="modal-body" style="height: 455px;">
                                        <div class="col-md-5" id="printableImgArea">
                                            <img src="<?php echo base_url() . $shipping->graphic_image ?>" class="rotate90" style="-webkit-transform: rotate(90deg); margin-top: 90px; height: 250px;">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a  class="btn btn-warning " href="#" onclick="printDiv('printableImgArea')">Print</a>
                                        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>-->
                                        <a href="" class="btn btn-danger">Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

            </div>
        </div>

        <div class="col-lg-6 offset-lg-6 text-right">
            <button type="button" class="btn btn-success" onclick="printContent('printableArea')" >Print</button>
        </div>

    </div>

</div>

<script type="text/javascript">
    //print a div
    function printContent(el) {

        $('.noprint').hide();

        $('body').css({"background-color": "#fff"});
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();

    }

</script>


<script type="text/javascript">
    function printDiv(divName) {

        $('body').css({"background-color": "#fff"});
        $('.noprint').hide();
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        // document.body.style.marginTop="-45px";
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>


