<style type="text/css">
    .right_side {
        float: left;
        font-weight: bold;
    }

    .left_side {
        float: right;
        font-weight: bold;
    }

    .or_cls {
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }

    a:hover {
        text-decoration: none;
    }

    .address {
        cursor: pointer;
    }

    #content div.box h5 {
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }

    #content div.box div.table {
        margin: 0;
        padding: 0 20px 10px 20px;
        clear: both;
        overflow: visible;
    }

    #content div.box {
        overflow: visible;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?>
            </div>
        </div>
        <!-- box / title -->
        <div class="title row">
            <h5 class="col-sm-6">Request Product Catalog</h5>

        </div>

        <div class="p-1">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '')
            {
                echo $error;
            }
            if ($success != '')
            {
                echo $success;
            }
            ?>
        </div>
        <p class="px-3">
            <a href="javascript:void(0)" class="btn btn-primary btn-sm mt-1 action-update"
               style="margin-top: -3px !important;" onClick="return action_approved(document.recordlist)">
                Approve product
            </a>

        </p>
        <div class="table mt-3" id="result_search">
            <form name="recordlist" id="mainform" method="post"
                  action="<?php echo base_url('approve-catalog-request') ?>">
                <input type="hidden" name="action">
                <input type="hidden" name="requested_by" value="<?= $requested_by; ?>">
                <input type="hidden" name="request_id" value="<?= $request_id; ?>">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th width="1%"><input type="checkbox" id="SellectAll"/></th>
                        <th width="5%">#</th>
                        <th width="13%">Product name</th>
                        <th width="15%">Pattern</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $sl = 0 + $pagenum;
                    if (isset($product_data))
                    {

                        foreach ($product_data as $value)
                        {

                            $array_of_patterns = explode(",", $value->pattern_models_ids);
                            foreach ($array_of_patterns as $pattern_value)
                            {

                                $sl++;
                                $check_status = 0;

                                $checkData = $this->db->where('pattern_model_id',$pattern_value)->where('product_id',$value->product_id)->get('b_user_catalog_products')->row();
                                if(!empty($checkData))
                                {
                                    $check_status = 1;
                                }

                                ?>

                                <tr>
                                    <td>

                                        <input type="hidden" name="product_link[<?= $sl; ?>]" value="<?php echo $checkData->product_link;?>">
                                        <input type="hidden" name="pattern_link[<?= $sl; ?>]" value="<?php echo $checkData->pattern_link;?>">


                                        <input type="hidden" name="product_arr[<?= $sl; ?>]" value="<?php echo $value->product_id;?>">
                                        <input type="hidden" name="pattern_arr[<?= $sl; ?>]" value="<?php echo $pattern_value;?>">
                                        <input type="checkbox"
                                               name="Id_List[]"
                                               id="Id_List[]"
                                            <?php if (!empty($checkData))
                                            {
                                                echo "checked";
                                            }; ?>
                                               value="<?= $sl; ?>" class="checkbox_list">
                                    </td>
                                    <td><?php echo $sl; ?></td>
                                    <td><?php echo $value->product_name; ?></td>
                                    <td>
                                        <?php
                                        if (isset($pattern_data))
                                        {
                                            foreach ($pattern_data as $single_pattern)
                                            {
                                                if ($single_pattern->pattern_model_id == $pattern_value)
                                                {
                                                    echo ucwords($single_pattern->pattern_name);
                                                    break;
                                                }
                                            }
                                        }
                                        ?>
                                    </td>

                                </tr>
                            <?php }
                        }
                    }
                    ?>
                    </tbody>

                    <?php if (!isset($product_data) || empty($product_data)) { ?>
                        <tfoot>
                        <tr>
                            <th colspan="8" class="text-center  text-danger">No record found!</th>
                        </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </form>

        </div>


    </div>
</div>

