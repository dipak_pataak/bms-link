<style type="text/css">
    .nav-pills .nav-link {
        border: 1px groove;
    }
    .custom-checkbox .custom-control-input {
        right: 0;
        z-index: 10;
        top: 5px;
        left: 2px;
    }
    .table-part .table-responsive .table thead tr th:nth-child(3){
        width: 170px !important;
        max-width: 170px !important;
        min-width: 170px !important;
        text-align: center;
    }
    .table-part .table-responsive .table thead tr th:nth-child(4),
    .table-part .table-responsive .table thead tr th:nth-child(5){
        width: 70px !important;
        max-width: 70px !important;
        min-width: 70px !important;
        text-align: center;
    }
    .table-part .table-responsive .table thead tr th:nth-child(14) {
        width: 270px !important;
        max-width: 270px !important;
        min-width: 270px !important;
        text-align: center;
    }
    .table-part .table-responsive .table tbody tr td:nth-child(3),
    .table-part .table-responsive .table tbody tr td:nth-child(4),
    .table-part .table-responsive .table tbody tr td:nth-child(5){
        text-align: center;
    }
    .table-part .table-responsive .table tbody tr td:nth-child(14),
    .table-part .table-responsive .table tbody tr td:nth-child(15){
        text-align: left;
    }
    .table-part .table-responsive .table tbody tr td:nth-child(14) span,
    .table-part .table-responsive .table tbody tr td:nth-child(15) span{
        display: inline-block;
        width: 100%;
    }
    .table-part .table-responsive .table thead tr th:nth-child(9){
        width: 150px !important;
        max-width: 150px !important;
        min-width: 150px !important;
        text-align: center;
    }
    .table-part .table-responsive .table thead tr th:nth-child(16){
        width: 200px !important;
        max-width: 200px !important;
        min-width: 200px !important;
        text-align: center;
    }
    /*Shades css*/

    .table-part-shades .table-responsive tbody tr td:nth-child(3){
        width: 180px !important;
        max-width: 180px !important;
        min-width: 180px !important;
    }
    .table-part-shades .table-responsive tbody tr td:nth-child(4),
    .table-part-shades .table-responsive tbody tr td:nth-child(5){
        width: 70px !important;
        max-width: 70px !important;
        min-width: 70px !important;
    }
    .table-part-shades .table-responsive tbody tr td:nth-child(18){
        width: 150px !important;
        max-width: 150px !important;
        min-width: 150px !important;
    }
</style>
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title">
            <h5>Work Order</h5>
        </div>
        <!-- end box / title -->
        <div class="bs_tab">
            <ul class="nav nav-pills px-4" role="tablist">
                <?php
                foreach ($categories as $key => $value) {
                    // if($blind->category_name=='Blinds'){
                    //     $active = "";
                    // }else{
                    //     $active = "active";
                    // }

                    if ($value == 'Blinds' && count($categories) == 1) {
                        $blind_active = "active";
                    } else if ($value == 'Shades' && count($categories) == 1) {
                        $shades_active = "active";
                    } else {
                        $blind_active = "active";
                    }
                    if ($value == 'Blinds') {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#blind" role="tab">Blind Work</a>
                        </li>
                    <?php } if ($value == 'Shades') { ?>
                        <li class="nav-item">
                            <a class="nav-link <?= @$shades_active ?>" data-toggle="tab" href="#shade" role="tab">Shade Work</a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane p-2 <?= @$blind_active ?>" id="blind" role="tabpanel">
                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printBlind')" >Print Blind Work</button>
                    <div id="printBlind">
                        <div class="form-row px-2">
                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Order #</label>
                                    <div class="col-sm-8">
                                        <p><?= $orderd->order_id ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">S/M :</label>
                                    <div class="col-sm-6">
                                        <p><?= $orderd->side_mark ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Order Date</label>
                                    <div class="col-sm-8">
                                        <p><?= date_format(date_create($orderd->order_date), 'M-d-Y'); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">For:</label>
                                    <div class="col-sm-8">
                                        <p><?php echo $customer->first_name . " " . $customer->last_name; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6 offset-md-3">
                                <div class="row m-0">
                                    <label for="orderid" class="col-form-label col-sm-4">Bar code</label>
                                    <div class="col-sm-8">
                                        <img src="<?php echo base_url() . $orderd->barcode; ?>" alt="" style="max-width: 100%;" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table table-part mt-3">
                            <form action="<?= base_url('b_level/manufacturer_controller/save_blind_menufactur') ?>" id="blind_manufactur" method="post">
                                <input type="hidden" name="orderid" value="<?= $orderd->order_id ?>">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Qty</th>
                                                <th>Product</th>
                                                <th>W</th>
                                                <th>H</th>
                                                <th>Sc</th>
                                                <th>Sc Chk</th>
                                                <th>M</th>
                                                <th >Multi Blind</th>
                                                <th>HD</th>
                                                <th>Tilt</th>
                                                <th>Lift</th>
                                                <th>Routless</th>
                                                <th colspan="2">Valance</th>
                                                <th>Val cut chk</th>
                                                <th colspan="2">Tile cut Outs</th>
                                                <th>Cord Length</th>
                                                <th>Rooms</th>
                                                <th>Comment</th>
                                                <th>Chk</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($order_details)) {
                                                $CI = & get_instance();

                                                $CI->load->model('b_level/Manufactur_model');

                                                $i = 1;
                                                $bld = 1;
                                                foreach ($order_details as $key => $blind) {
                                                    // print_r($blind->width_fraction_id);
                                                    $pat = $this->db->where('pattern_model_id', $blind->pattern_model_id)->get('pattern_model_tbl')->row();

                                                    $width_fraction = $this->db->where('id', $blind->width_fraction_id)->get('width_height_fractions')->row();

                                                    $height_fraction = $this->db->where('id', $blind->height_fraction_id)->get('width_height_fractions')->row();

                                                    if ($blind->category_name == 'Blinds') {

                                                        $attr = json_decode($blind->product_attribute);

                                                        $ch = $this->db->where('product_id', $blind->product_id)->where('order_id', $orderd->order_id)->get('manufactur_data')->row();
                                                        $chk_op = (json_decode(@$ch->chk_option));

                                                        ?>  
                                                    <input type="hidden" name="product_id[]" value="<?= $blind->product_id ?>">  
                                                    <tr class="<?php if ($blind->manufactured_scan_date != NULL) echo "color"; ?>">
                                                        <td><?= $i++; ?></td>
                                                        <td><?= $blind->product_qty ?></td>
                                                        <td>
                                                            <!-- Product name -->
                                                            <?= $blind->product_name ?><br/>
                                                            <!-- Pattern name -->
                                                            <?php
                                                            if (isset($pat->pattern_name)) {
                                                                echo $pat->pattern_name;
                                                            }
                                                            ?><br/>
                                                            <!-- Color name -->
                                                            <?php
                                                            $col = $this->db->where('id', $blind->color_id)->get('color_tbl')->row();
                                                            ?>
                                                            <?php
                                                            if (isset($col->color_name)) {
                                                                echo $col->color_name . '';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Mount') {
                                                                        $MountData = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        if($MountData == "Inside Mount(IB)"){
                                                                            $val_size = $blind->width;
                                                                            $final_val_size = ($val_size + $width_fraction->decimal_value) - DEFAULT_WIDTH_BLINDS;
                                                                            echo ShadesdecToFraction($final_val_size);
                                                                        }
                                                                        if($MountData == "Outside Mount(OB)"){
                                                                            
                                                                            echo $blind->width.' '.$width_fraction->fraction_value;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td><?= $blind->height.' '.$height_fraction->fraction_value ?></td>
                                                        <!-- SC -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                $blind_count_data = $this->db->get('tbl_blinds_shades_count')->result_array();
                                                                
                                                                $pname = $blind->product_name;
                                                                $pro_inch = explode('&quot;',trim($pname));
                                                                $pro_inch_val = $pro_inch[0];
                                                                $inch_array = array();

                                                                foreach ($blind_count_data as $value) {
                                                                    $inch_array[] = $value['blind_length_inch'];
                                                                }

                                                                $j = true;

                                                                foreach ($blind_count_data as $key => $value) {
                                                                    $final_height_value = ceil($blind->height + $height_fraction->decimal_value);
                                                                    if(in_array($final_height_value, $inch_array) && $j){
                                                                        $j = false;
                                                                        $inch_final_value = $blind_count_data[array_search($final_height_value,$inch_array)];

                                                                        if($pro_inch_val == '2')
                                                                            echo $inch_final_value['2_inch_slats'];
                                                                        if($pro_inch_val == '2 1/2')
                                                                             echo $inch_final_value['2_5_inch_slats'];
                                                                        if($pro_inch_val == '3')
                                                                            echo $inch_final_value['3_inch_slats'];
                                                                    }else if($j){
                                                                        $j = false;
                                                                        if($pro_inch_val == '2')
                                                                            echo $value['2_inch_slats'];
                                                                        if($pro_inch_val == '2 1/2')
                                                                             echo $value['2_5_inch_slats'];
                                                                        if($pro_inch_val == '3')
                                                                            echo $value['3_inch_slats'];
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Sc Chk   -->
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="sc_chk[<?= $blind->product_id ?>]" value="1" <?= (@$chk_op->sc_chk == '1' ? 'checked' : '') ?> class="custom-control-input" id="checkB<?= $bld ?>" >
                                                                <label class="custom-control-label" for="checkB<?= $bld ?>"></label>
                                                            </div>
                                                        </td>
                                                        <!-- Mount -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {

                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Mount') {

                                                                        $MountData = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        if($MountData == "Inside Mount(IB)"){
                                                                            echo "IB";
                                                                        }
                                                                        if($MountData == "Outside Mount(OB)"){
                                                                            echo "OB";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Multi Blind -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Multi Blinds' || $a->attribute_name == 'Multi blinds') {

                                                                        // $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                        //echo @$ops->option_name.'<br/>';
                                                                        echo '<table>
                                                                        <tr>'.@$ops->option_name.'</tr>';
                                                                        foreach ($att->opop as $key => $opops) {
                                                                            $opop = $this->db->select('op_op_name')->where('op_op_id', $opops->op_op_id)->get('attr_options_option_tbl')->row();
                                                                            $mblind_val = explode(" ",$opops->op_op_value);
                                                                            $get_frac_val = $this->db->select('decimal_value')->where('id', $mblind_val[1])->get('width_height_fractions')->row();
                                                                            $final_calculation_val = $mblind_val[0] + $get_frac_val->decimal_value;
                                                                            if($blind->product_name == "3&quot; WiVi Blinds"){
                                                                                if($ops->option_name == "3 on One"){
                                                                                    if($opop->op_op_name == "Left"){
                                                                                        $final_val = $final_calculation_val - 0.375;
                                                                                    }
                                                                                    if($opop->op_op_name == "Center"){
                                                                                        $final_val = $final_calculation_val - 0.25;
                                                                                    }
                                                                                    if($opop->op_op_name == "Right"){
                                                                                        $final_val = $final_calculation_val - 0.375;
                                                                                    }
                                                                                }
                                                                                if($ops->option_name == "2 on One"){
                                                                                    $final_val = $final_calculation_val - 0.375;
                                                                                }
                                                                            }else{
                                                                                if($ops->option_name == "3 on One"){
                                                                                    $final_val = $final_calculation_val - 0.25;
                                                                                }
                                                                                if($ops->option_name == "2 on One"){
                                                                                    if($opop->op_op_name == "Left"){
                                                                                        $final_val = $final_calculation_val - 0.375;
                                                                                    }
                                                                                    if($opop->op_op_name == "Right"){
                                                                                        $final_val = $final_calculation_val - 0.25;
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (!empty($opop)) {
                                                                                echo '<tr>
                                                                                        <td>'.@$opop->op_op_name.'</td>
                                                                                        <td>'.@ShadesdecToFraction($final_val).'</td>
                                                                                    </tr>';
                                                                            }
                                                                        }
                                                                        echo ' </table>';
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- HD -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Hold down' || $a->attribute_name == 'Hold Down') {
                                                                        $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Tilt -->
                                                        <td><?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Tilt Type' || $a->attribute_name == 'Tilt type') {
                                                                        $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                    if ($a->attribute_name == 'Tilt Position' || $a->attribute_name == 'Tilt position') {
                                                                        $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?></td>
                                                        <!-- Lift -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Lift Type' || $a->attribute_name == 'Lift type') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                    if ($a->attribute_name == 'Lift Position' || $a->attribute_name == 'Lift position') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Routless -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Routless') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Valance -->
                                                       <td class="valance-part">
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                    if ($a->attribute_name == 'Valance Type' || $a->attribute_name == 'Valance type') {
                                                                        echo "<span>Type</span>";
                                                                    }

                                                                    if ($a->attribute_name == 'Common Valance' || $a->attribute_name == 'Common valance') {
                                                                        echo "<span>Common</span>";
                                                                    }

                                                                    if ($a->attribute_name == 'Valance Clips' || $a->attribute_name == 'Valance clips') {
                                                                        echo "<span>Clips</span>";
                                                                    }

                                                                    if ($a->attribute_name == 'Custom Valance Returned' || $a->attribute_name == 'Custom Valance returned') {
                                                                        echo "<span>Returned</span>";
                                                                    }
                                                                }
                                                                echo "<span>Size</span>";
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                    if ($a->attribute_name == 'Mount') {

                                                                        $MountData = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }

                                                                    if ($a->attribute_name == 'Valance Type' || $a->attribute_name == 'Valance type') {
                                                                        $type = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }

                                                                    if ($a->attribute_name == 'Common Valance' || $a->attribute_name == 'Common valance') {
                                                                        $common = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        // echo $multiblinds."<br/>";
                                                                        if($common == "No"){
                                                                            echo "<span>".$common."</span>";
                                                                        }else{
                                                                            $com_val = explode(" ",$common);
                                                                            $get_frac_val = $this->db->select('fraction_value')->where('id', $com_val[1])->get('width_height_fractions')->row();
                                                                            echo "<span>".$com_val[0].' '.$get_frac_val->fraction_value."</span>";
                                                                            // echo "<span>".ShadesdecToFraction($common)."</span>";
                                                                        }
                                                                    }

                                                                    if ($a->attribute_name == 'Valance Clips' || $a->attribute_name == 'Valance clips') {
                                                                        $clips = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        // echo $multiblinds."<br/>";
                                                                        echo "<span>".$clips."</span>";
                                                                    }

                                                                    if ($a->attribute_name == 'Custom Valance Returned' || $a->attribute_name == 'Custom Valance returned') {
                                                                        $returned = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        // echo $multiblinds."<br/>";
                                                                        if($returned == "No Returns-IB"){
                                                                            echo "<span>".$returned."</span>";
                                                                        }else if($returned == '1/2" Returns'){
                                                                            echo "<span>".$returned."</span>";
                                                                        }else{
                                                                            $returned_val = explode(" ",$returned);
                                                                            $get_frac_val = $this->db->select('fraction_value')->where('id', $returned_val[1])->get('width_height_fractions')->row();
                                                                            echo "<span>".$returned_val[0].' '.$get_frac_val->fraction_value."</span>";
                                                                        }
                                                                    }
                                                                }
                                                                if($MountData == "Inside Mount(IB)"){
                                                                    $val_size = $blind->width;
                                                                    $final_val_size = ($val_size + $width_fraction->decimal_value) - DEFAULT_VAL_SIZE_IB;
                                                                    echo ShadesdecToFraction($final_val_size);
                                                                }
                                                                if($MountData == "Outside Mount(OB)"){
                                                                    $val_size = $blind->width;
                                                                    $final_val_size = ($val_size + $width_fraction->decimal_value) + DEFAULT_VAL_SIZE_OB;
                                                                    echo ShadesdecToFraction($final_val_size);
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Valance Type' || $a->attribute_name == 'Valance type') {
                                                                        if (!empty($att->opop)) {
                                                                            $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                            echo $opop->op_op_name;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td> -->
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" <?= (@$chk_op->val_cut_chk == '1' ? 'checked' : '') ?> name="val_cut_chk[<?= $blind->product_id ?>]" value="1" id="checkA<?= $bld ?>" >
                                                                <label class="custom-control-label" for="checkA<?= $bld ?>"></label>
                                                            </div>
                                                        </td>
                                                        <!-- Tile cut Outs -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Tile Cut Outs' || $a->attribute_name == 'Tile cut outs') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Tile Cut Outs' || $a->attribute_name == 'Tile cut outs') {

                                                                        foreach ($att->opop as $key => $opops) {

                                                                            $opop = $this->db->select('op_op_name')->where('op_op_id', $opops->op_op_id)->get('attr_options_option_tbl')->row();

                                                                            $tile_cut_val = explode(" ",$opops->op_op_value);
                                                                            $get_frac_val = $this->db->select('fraction_value')->where('id', $tile_cut_val[1])->get('width_height_fractions')->row();
                                                                            $final_val = $tile_cut_val[0].' '.$get_frac_val->fraction_value;

                                                                            if (!empty($opop)) {
                                                                                echo @$opop->op_op_name;
                                                                                if (!empty($opops->op_op_value)) {
                                                                                    echo ' : ' . @$final_val;
                                                                                }
                                                                                echo '<br>';
                                                                            }
                                                                        }


                                                                        // if(!empty($att->opop)){
                                                                        //     $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                        //     echo $opop->op_op_name;
                                                                        // }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Cord Length -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Custom Cord Length' || $a->attribute_name == 'Custom Cord Length') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                        if($multiblinds == "No"){
                                                                            echo "<span>".$multiblinds."</span>";
                                                                        }else{
                                                                            $mlt_blind_val = explode(" ",$multiblinds);
                                                                            $get_frac_val = $this->db->select('fraction_value')->where('id', $mlt_blind_val[1])->get('width_height_fractions')->row();
                                                                            echo "<span>".$mlt_blind_val[0].' '.$get_frac_val->fraction_value."</span>";
                                                                            // echo "<span>".ShadesdecToFraction($common)."</span>";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <!-- Rooms -->
                                                        <td>
                                                            <?= @$blind->room; ?>
                                                            <!-- <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Room') {
                                                                        echo $att->attribute_value;
                                                                    }
                                                                }
                                                            }
                                                            ?> -->
                                                        </td>
                                                        <!-- Comment -->
                                                        <td>
                                                            <?php
                                                            if (!empty($attr)) {
                                                                foreach ($attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Comment') {
                                                                        $multiblinds = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="chk[<?= $blind->product_id ?>]" value="1" <?= (@$chk_op->chk == '1' ? 'checked' : '') ?> class="custom-control-input" id="checkC<?= $bld ?>">
                                                                <label class="custom-control-label" for="checkC<?= $bld ?>"></label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }

                                            $bld++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="submit" class="btn btn-success btn-sm"> Save</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane p-2 <?= @$shades_active ?>" id="shade"  role="tabpanel">
                    <button type="button" class="btn btn-success pull-right" onclick="printContent('printShade')" >Print Shade Work</button>

                    <form action="<?= base_url('b_level/manufacturer_controller/save_blind_menufactur') ?>" id="shads_manufactur" method="post">
                        <div id="printShade">
                            <input type="hidden" name="orderid" value="<?= $orderd->order_id ?>">
                            <div class="form-row px-2">
                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Order #</label>
                                        <div class="col-sm-8">
                                            <p><?= $orderd->order_id ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">S/M :</label>
                                        <div class="col-sm-6">
                                            <p><?= $orderd->side_mark ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Order Date</label>
                                        <div class="col-sm-8">
                                            <p><?= date_format(date_create($orderd->order_date), 'M-d-Y'); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">For:</label>
                                        <div class="col-sm-8">
                                            <p><?php echo $customer->first_name . " " . $customer->last_name; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 offset-md-3">
                                    <div class="row m-0">
                                        <label for="orderid" class="col-form-label col-sm-4">Bar code</label>
                                        <div class="col-sm-8">
                                            <img src="<?php echo base_url() . $orderd->barcode; ?>" alt="" style="max-width: 100%;" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table table-part-shades mt-3">
                                <table class=" table table-bordered table-hover table-responsive text-center">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">#</th>
                                            <th rowspan="2">Qty</th>
                                            <th rowspan="2">Product</th>
                                            <th colspan="2">Shades Size</th>
                                            <th colspan="2">Fabric Size</th>
                                            <th rowspan="2">Chk</th>
                                            <th rowspan="2">M</th>
                                            <th colspan="6">Control</th>
                                            <th colspan="6">Hardware</th>
                                            <th rowspan="2">chk</th>
                                            <th rowspan="2">Side by side</th>
                                            <th rowspan="2">Rooms</th>
                                            <th rowspan="2">Image Pattern</th>
                                            <th rowspan="2">Privacy Height</th>
                                            <th rowspan="2">Comment</th>
                                            <th rowspan="2">Chk</th>
                                        </tr>
                                        <tr>
                                            <th>W</th>
                                            <th>H</th>
                                            <th>W</th>
                                            <th>H</th>
                                            <th>T</th>
                                            <th>P</th>
                                            <th>L</th>
                                            <th>Mo</th>
                                            <th>RCc</th>
                                            <th>RCh</th>
                                            <th colspan="2">Type</th>
                                            <th colspan="2">Color</th>
                                            <th colspan="2">Size</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($order_details)) {

                                            $sed = 1;
                                            $sr_no = 1;
                                            foreach ($order_details as $key => $shads) {

                                                if ($shads->category_name == 'Shades') {

                                                    $shades_attr = json_decode($blind->product_attribute);

                                                    // echo "<pre>"; print_r($shades_attr); die;

                                                    $chc = $this->db->where('product_id', $shads->product_id)->where('order_id', $orderd->order_id)->get('manufactur_data')->row();
                                                    $width_fraction = $this->db->where('id', $shads->width_fraction_id)->get('width_height_fractions')->row();

                                                    $height_fraction = $this->db->where('id', $shads->height_fraction_id)->get('width_height_fractions')->row();

                                                    $chk_op = (json_decode(@$chc->chk_option));
                                                    ?> 
                                                <input type="hidden" name="product_id[]" value="<?= $shads->product_id ?>">
                                                <tr>
                                                    <td><?= $sr_no++; ?></td>
                                                    <td><?= $shads->product_qty ?></td>
                                                    <td>
                                                        <!-- Product name -->
                                                        <?= $shads->product_name ?><br/>
                                                        <!-- Pattern name -->
                                                        <?php
                                                        if (isset($shads->pattern_name)) {
                                                            echo $shads->pattern_name;
                                                        }
                                                        ?><br/>
                                                        <!-- Color name -->
                                                        <?php
                                                        if (isset($shads->color_name)) {
                                                            echo $shads->color_name . '';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            if (!empty($shades_attr)) {
                                                                foreach ($shades_attr as $key => $att) {
                                                                    $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                    if ($a->attribute_name == 'Mount') {
                                                                        $MountData = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);

                                                                        if($MountData == "Inside Mount (IB)"){
                                                                            $val_size = $shads->width;
                                                                            $final_val_size = ($val_size + $width_fraction->decimal_value) - DEFAULT_WIDTH_IB_SHADES;
                                                                            // echo $final_val_size;
                                                                            echo ShadesdecToFraction($final_val_size);
                                                                        }
                                                                        if($MountData == "Outside Mount(OB)"){
                                                                            
                                                                            echo $shads->width.' '.$width_fraction->fraction_value;
                                                                        }

                                                                        if($MountData == "End Mount(EB)"){
                                                                            $val_size = $shads->width;
                                                                            $final_val_size = ($val_size + $width_fraction->decimal_value) - DEFAULT_WIDTH_EB_SHADES;
                                                                            // echo $final_val_size;
                                                                            echo ShadesdecToFraction($final_val_size);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    </td>
                                                    <td><?= $shads->height.' '.$height_fraction->fraction_value ?></td>
                                                    <!-- Fabric size -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Type') {

                                                                    $as = $this->db->select('option_condition')
                                                                                    ->where('attribute_id', $att->attribute_id)
                                                                                    ->where('option_id', $att->options[0]->option_id)
                                                                                    ->where('product_id', $shads->product_id)
                                                                                    ->get('product_attr_option')->row();

                                                                    echo @$shads->width + @($as->option_condition ? $as->option_condition : 0);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?= $shads->height ?></td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="fracchk[<?= $shads->product_id ?>]"  <?= (@$chk_op->chk == 'on' ? 'checked' : '') ?> class="custom-control-input" id="fracchkB<?= $sed ?>">
                                                            <label class="custom-control-label" for="fracchkB<?= $sed ?>"></label>
                                                        </div>
                                                    </td>
                                                    <!-- ==== -->
                                                    <!-- Mount -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {

                                                            foreach ($shades_attr as $key => $mount) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $mount->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Mount') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($mount, $a->attribute_name);
                                                                    if($data == "Inside Mount (IB)"){
                                                                        echo "IB";
                                                                    }
                                                                    if($data == "Outside Mount(OB)"){
                                                                        echo "OB";
                                                                    }
                                                                    if($data == "End Mount(EB)"){
                                                                        echo "EB";
                                                                    }
                                                                    // if()
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- T -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {

                                                            foreach ($shades_attr as $key => $att) {

                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                if ($a->attribute_name == 'Control Type') {

                                                                    $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                    echo @$ops->option_name;

                                                                    //$data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- P -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Position') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- L -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Length') {

                                                                    if ($shads->height < 55) {
                                                                        echo '<input type="text" name="cl[' . trim($shads->product_id) . ']" value="' . @$chk_op->cl . '" class="form-control" style="width:40px;">';
                                                                    }

                                                                    if ($shads->height > 95) {
                                                                        echo '<input type="text" name="cl[' . trim($shads->product_id) . ']" value="' . @$chk_op->cl . '" class="form-control" style="width:40px;">';
                                                                    }

                                                                    if ($shads->height >= 55 && $shads->height <= 65) {
                                                                        echo '36"';
                                                                    }
                                                                    if ($shads->height > 65 && $shads->height <= 75) {
                                                                        echo '48"';
                                                                    }

                                                                    if ($shads->height > 75 && $shads->height <= 85) {
                                                                        echo '54"';
                                                                    }

                                                                    if ($shads->height > 85 && $shads->height <= 95) {
                                                                        echo '64"';
                                                                    }

                                                                    //$data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Mo -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();

                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo(@$att->opop[0]->op_op_value);
                                                                }

                                                                // if($a->attribute_name=='Control Motor'){
                                                                //     $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Rcc -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo (@$att->opop[1]->op_op_value);
                                                                }
                                                                // if($a->attribute_name=='Remote Controller'){
                                                                //     $data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Rch -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Control Type') {
                                                                    echo(@$att->opop[2]->op_op_value);
                                                                }
                                                                // if($a->attribute_name=='Remote Channel Programming'){
                                                                //     $data = $CI->Manufactur_model->get_attr_options($att,$a->attribute_name);
                                                                // }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- HR -->
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Headrail') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Bottom rail' || $a->attribute_name == 'Bottom Rail') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>TB</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Tube') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Side Channel') {
                                                                                $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                                echo @$ops->option_name;
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Headrail /Cord Color' || $a->attribute_name == 'Headrail Color') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Bottom Rail Color') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>CD</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Cord Color' || $a->attribute_name == 'Cord color') {
                                                                                $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Side Channel') {
                                                                                if (!empty($att->opop)) {
                                                                                    $opop = $this->db->select('op_op_name')->where('op_op_id', $att->opop[0]->op_op_id)->get('attr_options_option_tbl')->row();
                                                                                    echo $opop->op_op_name;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>HR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Headrail') {

                                                                                $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();

                                                                                if (strpos($ops->option_name, 'Cassette') !== false) {
                                                                                    $final_val = ($shads->width + $width_fraction->decimal_value) - 0.25;
                                                                                    echo ShadesdecToFraction($final_val);
                                                                                }
                                                                                if (strpos($ops->option_name, 'Facia') !== false) {
                                                                                    $final_val = ($shads->width + $width_fraction->decimal_value) - 0.25;
                                                                                    echo ShadesdecToFraction($final_val);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!-- <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="hr[<?= $shads->product_id ?>]" <?= (@$chk_op->hr == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkHR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkHR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                            <tr>
                                                                <td>BR</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Bottom rail' || $a->attribute_name == 'Bottom Rail') {
                                                                                $final_val = ($shads->width + $width_fraction->decimal_value) - 0.875;
                                                                                echo ShadesdecToFraction($final_val);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!-- <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="br[<?= $shads->product_id ?>]"  <?= (@$chk_op->br == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkBR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkBR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                            <tr>
                                                                <td>TB</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {
                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Tube') {
                                                                                $final_val = ($shads->width + $width_fraction->decimal_value) - 0.875;
                                                                                echo ShadesdecToFraction($final_val);
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!-- <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="tb[<?= $shads->product_id ?>]"  <?= (@$chk_op->tb == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkTB<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkTB<?= $sed ?>"></label>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                            <tr>
                                                                <td>SC</td>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($shades_attr)) {
                                                                        foreach ($shades_attr as $key => $att) {

                                                                            $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                            if ($a->attribute_name == 'Side Channel') {
                                                                                $ops = $this->db->select('option_name')->where('att_op_id', $att->options[0]->option_id)->get('attr_options')->row();
                                                                                if ($ops->option_name == 'Yes') {
                                                                                    $final_val = ($shads->width + $width_fraction->decimal_value) - 2.25;
                                                                                    echo ShadesdecToFraction($final_val);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!-- <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="sc[<?= $shads->product_id ?>]"  <?= (@$chk_op->sc == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checksc<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checksc<?= $sed ?>"></label>
                                                                    </div>
                                                                </td> -->
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="hr[<?= $shads->product_id ?>]" <?= (@$chk_op->hr == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkHR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkHR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="br[<?= $shads->product_id ?>]"  <?= (@$chk_op->br == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkBR<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkBR<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="tb[<?= $shads->product_id ?>]"  <?= (@$chk_op->tb == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkTB<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checkTB<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" name="sc[<?= $shads->product_id ?>]"  <?= (@$chk_op->sc == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checksc<?= $sed ?>">
                                                                        <label class="custom-control-label" for="checksc<?= $sed ?>"></label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td> 
                                                    <!-- <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="checkB">
                                                                <label class="custom-control-label" for="checkB"></label>
                                                            </div>
                                                            </td> -->
                                                    <!-- Side by side -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Side By Side') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Rooms -->
                                                    <td>
                                                        <?= $shads->room ?>
                                                        <!-- <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Room') {
                                                                    echo $att->attribute_value;
                                                                }
                                                            }
                                                        }
                                                        ?> -->
                                                    </td>
                                                    <!-- Image Pattern -->
                                                    <td>
                                                        <?= $shads->pattern_name ?>
                                                        <!-- <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Image Pattern') {
                                                                    echo $att->attribute_value;
                                                                }
                                                            }
                                                        }
                                                        ?> -->
                                                    </td>
                                                    <!-- Privacy Height   -->
                                                    <td>
                                                        <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Privacy Height') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <!-- Comment -->
                                                    <td>
                                                        <?= $shads->notes ?>
                                                        <!-- <?php
                                                        if (!empty($shades_attr)) {
                                                            foreach ($shades_attr as $key => $att) {
                                                                $a = $this->db->select('attribute_name')->where('attribute_id', $att->attribute_id)->get('attribute_tbl')->row();
                                                                if ($a->attribute_name == 'Comment') {
                                                                    $data = $CI->Manufactur_model->get_attr_options($att, $a->attribute_name);
                                                                }
                                                            }
                                                        }
                                                        ?> -->
                                                    </td>
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="chk[<?= $shads->product_id ?>]"  <?= (@$chk_op->chk == 'on' ? 'checked' : '') ?> class="custom-control-input" id="checkB<?= $sed ?>">
                                                            <label class="custom-control-label" for="checkB<?= $sed ?>"></label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $sed++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-success btn-sm"> Save</button>
                            </div>
                        </div>
                    </form>

                </div>
                <a href="<?= base_url('b_level/Manufacturer_controller/manufacturing_level/') . $orderd->order_id; ?>" class="btn btn-success"  style="margin-left: 25px;">Go to Manufacture Label</a>

                <button onclick="printAllContent();" class="btn btn-success"  style="margin-left: 25px;">Print all label</button>
            </div>
        </div>
    </div>
    <div class="box" style="display:none">
        <!-- <div class="box" > -->
        <div  id="print-all-label">
            <?php
            if (!empty($chit['blinds'])) {
                foreach ($chit['blinds'] as $key => $value) {
                    echo @$value;
                }
            }
            if (!empty($chit['shades'])) {
                foreach ($chit['shades'] as $key => $value) {
                    echo @$value;
                }
            }
            ?>
        </div>
    </div>
</div>
<?php 

    function ShadesdecToFraction($float) {
        // 1/2, 1/4, 1/8, 1/16, 1/3 ,2/3, 3/4, 3/8, 5/8, 7/8, 3/16, 5/16, 7/16,
        // 9/16, 11/16, 13/16, 15/16
        $whole = floor ( $float );
        $decimal = $float - $whole;
        $leastCommonDenom = 48; // 16 * 3;
        $denominators = array (2, 3, 4, 8, 16, 24, 48 );
        $roundedDecimal = round ( $decimal * $leastCommonDenom ) / $leastCommonDenom;
        if ($roundedDecimal == 0)
            return $whole;
        if ($roundedDecimal == 1)
            return $whole + 1;
        foreach ( $denominators as $d ) {
            if ($roundedDecimal * $d == floor ( $roundedDecimal * $d )) {
                $denom = $d;
                break;
            }
        }
        return ($whole == 0 ? '' : $whole) . " " . ($roundedDecimal * $denom) . "/" . $denom;
    }
    

    ?>
<!-- end content / right -->
<style type="text/css">

    .rotated { 
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $("#print-all-label table:last-child").attr('style', 'width:76.2mm;height:38.1mm;font-size: 10px;border-spacing: 0px;');
    });
    //print a div
    function printContent(el) {
        //$('#' + el).toggleClass('rotated');
        var css = '@page { size: landscape; } body{ font-size: 10px !important; } .table th, .table td{ padding: 1px !important;} .custom-control{ min-height: 5px !important; padding-left: 5px !important;} .custom-control-label::before { width: 10px !important; height: 10px !important; top: 0px !important;} .table-responsive{ width: 1024px !important; overflow: hidden  !important; margin: auto  !important; }',
                head = document.head || document.getElementsByTagName('head')[0],
                style = document.createElement('style');

        style.type = 'text/css';
        style.media = 'print';

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }


    // submit form and add data
    $("#blind_manufactur").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "<?= base_url() ?>b_level/manufacturer_controller/save_blind_menufactur";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                if (res === '1') {
                    toastr.success('Success! - Save successfully');
                }

            }, error: function () {
                swal('error');
            }
        });
    });


    // submit form and add data
    $("#shads_manufactur").on('submit', function (e) {
        e.preventDefault();

        var submit_url = "<?= base_url() ?>b_level/manufacturer_controller/save_blind_menufactur";

        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                if (res === '1') {
                    toastr.success('Success! - Save successfully');
                }

            }, error: function () {
                swal('error');
            }
        });
    });
    //print a div
    function printAllContent() {
        var restorepage = $('body').html();
        var printAllContent = $('#print-all-label').html();
        $('body').empty().html(printAllContent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }
</script>