
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <h5>Manufacturer Formula</h5>
        </div>
        <!-- end box / title -->
        <div class="px-3">
            <form>
                <div class="form-group row">
                    <label for="vo_no" class="col-sm-2 col-form-label">Variable Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ac" class="col-sm-2 col-form-label">Product Name</label>
                    <div class="col-sm-4">
                        <select name="cmbDebit" id="cmbDebit" class="form-control">
                            <option value='1020101'>Blind</option>
                            <option value="1020102">Shutters</option>
                            <option value="1020103">Shades</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="txtRemarks" class="col-sm-2 col-form-label">Formula</label>
                    <div class="col-sm-8">
                        <textarea name="txtRemarks" rows="5" id="txtRemarks" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group row">

                    <div class="col-sm-10 text-right">

                        <input type="submit" class="btn btn-success default btn-large" name="save" value="Update" tabindex="9"/>

                    </div>
                </div>
            </form> 
        </div>
    </div>
</div>
<!-- end content / right -->
