<style type="text/css">
    .nav-pills .nav-link {
        border: 1px groove;
    }
    .tab-pane .table table{
        border-left: 1px solid #cdcdcd;
    }
    .table table td {
        border: 1px solid #2f2f2f;
    }
    .tab-pane .table table {
        border-left: 0px;
    }
</style>

			<!-- content / right -->
			<div id="right">
				<!-- table -->
				<div class="box">
					<!-- box / title -->
					<div class="title">
						<h5>Work order label</h5>
					</div>
					<!-- end box / title -->
                    
					
					<div class="bs_tab">
                    <ul class="nav nav-pills px-4" role="tablist">
                        <?php
                            foreach ($categories as $key => $value) {
                                if ($value == 'Blinds' && count($categories) == 1) { 
                                    $blind_active = "active";
                                } else if($value == 'Shades' && count($categories) == 1) {
                                    $shades_active = "active";
                                } else {
                                    $blind_active = "active";
                                }
                            
                                if ($value == 'Blinds') {
                                    ?>
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#blind" role="tab">Blind Work</a>
                        </li>
                        <?php } if ($value == 'Shades') { ?>
                        <li class="nav-item">
                            <a class="nav-link <?= @$shades_active ?>" data-toggle="tab" href="#shade" role="tab">Shade Work</a>
                        </li>
                        <?php }
                            } ?>
                    </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div class="tab-pane p-2 <?=@$blind_active?>" id="blind" role="tabpanel">

                                <div class="row" >

                                    <?php 

                                        if(!empty($chit['blinds'])){
                                            foreach ($chit['blinds'] as $key => $value) {
                                               echo @$value;
                                            }
                                        }

                                    ?>

                                </div>
              
                            </div>


                            <div class="tab-pane p-2 <?=@$shades_active?>" id="shade"  role="tabpanel">
                                
                                <div class="row" >

                                    <?php 

                                        if(!empty($chit['shades'])){
                                            foreach ($chit['shades'] as $key => $value) {
                                               echo @$value;
                                            }
                                        }

                                    ?>

                                </div>

                                <!-- <button type="button" class="btn btn-success" onclick="printContent('printableArea2')" style="margin-left: 25px;">Print labels</button> -->

                            </div>
                        </div>
                    </div>
				</div>
			</div>
			<!-- end content / right -->

<style type="text/css">
    .rotated { 
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
</style>
         
<script type="text/javascript">
    //print a div
    function printContent(el) {

       

        //$('#' + el).toggleClass('rotated');
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }


</script>

