<?php $packageid=$this->session->userdata('packageid'); ?>
<?php $currency = $company_profile[0]->currency; ?>
<!-- content / right  -->
<div id="right">
    <!-- table -->
    <div class="box">

        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Manufaturing</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manufaturing';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                 <?php $menu_permission= b_access_role_permission(26); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
            <div class="col-sm-6 text-right">
                <!--<a href="new-order" class="btn btn-success btn-sm mt-1">Add</a>-->
            </div>
        </div>

        <!-- end box / title -->
        <p class="mb-3 px-3">
            <button class="btn btn-primary default" type="button" id="add_manufacturer_filter">
                Filter
            </button>
            <?php if(!empty($packageid)){?>
            <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="<?=base_url('new-order')?>" class="btn btn-success btn-sm">Add</a>
             <?php }?>
        <?php } ?>
        </p>

        <div class="collapse show px-3 mb-3" id="collapseExample">

            <div class="border px-3 pt-3">

                <form class="form-horizontal" action="<?= base_url('add-manufacturer') ?>" method="post">

                    <fieldset>

                        <div class="row">
                            <div class="col-sm-3">
                                <select class="form-control mb-3" name="company_id" >
                                    <option value="">--Select Company--</option>
                                    <?php foreach ($customers as $c) { ?>
                                        <option value="<?= $c->customer_id ?>" <?php
                                        if ($customerid == $c->customer_id) {
                                            echo 'selected';
                                        }
                                        ?>>
                                        <?= $c->company; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <!-- <div class="col-sm-3">
                                <select class="form-control mb-3" name="sidemark" >
                                    <option value="">--Select Side Mark--</option>

                                    <?php
                                    
                                     foreach ($customers as $c) { ?>
                                        <option value="<?= $c->side_mark ?>" <?php
                                        if ($side_mark == $c->side_mark) {
                                            echo 'selected';
                                        }
                                        ?>>
                                        <?= $c->side_mark; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div> -->

                            <div class="col-md-3">
                                <input type="text" name="sidemark" id="sidemark" class="form-control mb-3" value="<?php echo $side_mark; ?>" placeholder="Side Mark">
                            </div>

                            <div class="col-sm-3">
                                <input type="text"  name="order_date" id="order_date" class="form-control datepicker mb-3" value="<?php echo $order_date; ?>" placeholder="Order Date">
                            </div>                          

                            <div class="col-sm-2">
                               
                                <select class="form-control mb-3" name="order_stage" >
                                    <option value="">--Select Status--</option>
                                    <option value="1" <?php
                                    if ($order_stage == 1) {
                                        echo 'selected';
                                    }
                                    ?>>Quote</option>
                                    <option value="2" <?php
                                    if ($order_stage == 2) {
                                        echo 'selected';
                                    }
                                    ?>>Paid</option>
                                    <option value="3" <?php
                                    if ($order_stage == 3) {
                                        echo 'selected';
                                    }
                                    ?>>Partially Paid</option>
                                    <option value="4" <?php
                                    if ($order_stage == 4) {
                                        echo 'selected';
                                    }
                                    ?>>Shipping</option>
                                    <option value="5" <?php
                                    if ($order_stage == 5) {
                                        echo 'selected';
                                    }
                                    ?>>Cancelled</option>
                                </select>
                              
                            </div>

                            <div class="col-sm-1">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>

            </div>

        </div>


        <!-- end box / title -->
        <div class="px-3">
            <?= @$links; ?>
			<div class="table-responsive">
            <table class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                        <th>Order/Quote No</th>
                        <th>Client Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <!-- <th>Status</th> -->
                         <?php if(!empty($packageid)) {?>
                        <th>Action</th>
                          <?php } ?>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($orderd)) {
                        foreach ($orderd as $key => $value) {

                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                            ?>
                            <tr>
                                <td><?= $value->order_id; ?></td>
                                <td>
                                    <?php
                                    if ($value->level_id != 1) {
                                        //$sql = "SELECT * FROM company_profile WHERE user_id = $value->level_id";
                                        $sql = "SELECT * FROM company_profile WHERE user_id = $value->customer_user_id";
                                        $sql_result = $this->db->query($sql)->result();
//                                      dd($sql_result);
                                        echo @$sql_result[0]->company_name;
                                    } else {
                                        echo $value->customer_name;
                                    }
                                    ?></td>
                                <td><?= $value->side_mark; ?></td>

                                <td>
                                    <?=date_format(date_create($value->order_date),'M-d-Y');?>
                                </td>
                                <!-- <td><?= $currency ?><?= $value->grand_total ?></td> -->

<!--                                 <td>
                                    <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                        <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Manufacturing</option>
                                    </select>
                                </td> -->
                                  <?php if(!empty($packageid)) {?>
                                <td class="width_140">
                                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?= base_url('wholesaler-manufacture-quotation/') . $value->order_id; ?>" class="btn btn-success btn-sm default" title="Manufacturing"> <i class="fa fa-cogs"></i> </a>


                                        <!-- <a href="<?= base_url('wholesaler-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" title="View"> <i class="fa fa-eye"></i> </a> -->
                                        <a href="<?= base_url('manufacturing-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-sm default" title="View"> <i class="fa fa-eye"></i> </a>
                                    <?php } ?>
                                </td>
                            <?php } ?>

                            </tr>

                            <?php
                        }
                    } else {
                ?>

                    <div class="alert alert-danger"> There have no order found..</div>
                
                <?php } ?>

                </tbody>
            </table>
            </div>
			<?= @$links; ?>
        </div>
        <?php if(isset($b_user_orderd) && !empty($b_user_orderd)){ ?>
            <div class="table-responsive px-3">
                <h4>Order from b user</h4>

                <table class="table table-bordered table-hover text-center">
                    <thead>
                    <tr>
                        <th>Order/Quote No</th>
                        <th>Client Name </th>
                        <th>Side mark</th>
                        <th>Order date</th>
                        <!-- <th>Status</th> -->
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (!empty($b_user_orderd)) {
                        foreach ($b_user_orderd as $key => $value) {

                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                            ?>
                            <tr>
                                <td><?= $value->order_id; ?></td>
                                <td>
                                    <?php

                                        echo $value->user_name;

                                    ?></td>
                                <td><?= $value->side_mark; ?></td>

                                <td>
                                    <?=date_format(date_create($value->order_date),'M-d-Y');?>
                                </td>
                                <!-- <td><?= $currency ?><?= $value->grand_total ?></td> -->

                                <!--                                 <td>
                                    <select class="form-control" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')">
                                        <option value="">--Select--</option>
                                        <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                        <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                        <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                        <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Shipping</option>
                                        <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Cancelled</option>
                                        <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Manufacturing</option>
                                    </select>
                                </td> -->

                                <td class="width_140" class="text-right">
                                    <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                                    <a href="<?= base_url('wholesaler-manufacture-quotation/') . $value->order_id; ?>" class="btn btn-success btn-sm default"> <i class="fa fa-cogs"></i> </a>
                                    <?php } ?>
                                </td>

                            </tr>

                            <?php
                        }
                    } else {
                        ?>

                        <div class="alert alert-danger"> There have no order found..</div>

                    <?php } ?>

                    </tbody>
                </table>
                <?= @$links; ?>
            </div>
        <?php } ?>
    </div>
</div>
<!-- end content / right -->

<script type="text/javascript">


    function setOrderStage(stage_id, order_id) {

        $.ajax({
            url: "b_level/order_controller/set_order_stage/" + stage_id + "/" + order_id,
            type: 'GET',
            success: function (r) {
                toastr.success('Success! - Order Stage Set Successfully');
                setTimeout(function () {
                    window.location.href = window.location.href;
                }, 2000);
            }
        });
    }
    $("#collapseExample").hide();
    $(document).ready(function () {
        $("body").on("click", "#add_manufacturer_filter", function () {
            $("#collapseExample").slideToggle("slow");
        });
    });

</script>
