<?php $package=$this->session->userdata('packageid'); ?>
 <?php $menu_permission= b_access_role_permission(24); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/b_level/resources/css/jquery.dataTables.min.css">
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">

        <div class="row">
            <div class="col-md-12">
                <?php
                $message = $this->session->flashdata('message');
                if ($message)
                    echo $message;
                ?> 
            </div>
        </div>

        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Manage Row-column Price</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Manage Row-column Price';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- end box / title -->
        <p class="px-3">
            <?php if (!empty($package)){?>
            <?php if($menu_permission['access_permission'][0]->can_delete==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
            <a href="javascript:void(0)" class="btn btn-danger action-delete" onClick="return action_delete(document.recordlist)">Delete</a>
            <?php } ?>
            <?php } ?>
        </p>

        <div class="table-responsive p-3" id="price_results">
            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('b_level/Pricemodel_controller/manage_action') ?>">
                <input type="hidden" name="action">
                <input type="hidden" name="action_type" value="price_manage">
    			<div class="table-responsive">
                <table id="RowColumnDataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="SellectAll"/></th>
                            <th>SL No.</th>
                            <th>Price Sheet Name</th>
                            <th>Assigned Product</th>
                            <th>Is Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
    			</div>
    		</form>
        </div>

    </div>
</div>
<script src="<?php echo base_url(); ?>assets/b_level/resources/js/jquery.dataTables.min.js"></script>
<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function() {
        var orderable = [0, 1, 3, 4, 5];
        var dt = init_datatable('RowColumnDataTable','<?php echo base_url('b_level/Pricemodel_controller/get_row_column_Lists/'); ?>', orderable);
    });
</script>
