
<!-- content / right -->
<div id="right">
    <!-- table -->
    <div class="box">
        <!-- box / title -->
        <div class="title row">
            <div class="col-sm-6">
                <h5>Add Row-column Price</h5>
                <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_b_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Add Row-column Price';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                ?>
                 <?php $menu_permission= b_access_role_permission(8); ?>
                <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
            </div>
        </div>

        <div class="col-md-12">

            <div class="alert alert-info">
                <strong>Notes! </strong> 1. Do not use any text. 2. Please use (') single quote twice to show inch symbol. ex: 60''
            </div>

            <!--<div class="">
                    <select class="form-control select2" name="" id="style_id" onchange="editTable()" data-placeholder="-- select one --">
                        <option value=""></option>
            <?php foreach ($style_slist as $s) { ?>
                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
            <?php } ?>
                    </select>
                </div> -->

            <!--            <div class="btn-group">
            <?php foreach ($style_slist as $s) { ?>
                                                <a  href="javascript:void" class="btn btn-primary btn-sm" onclick="editTable('<?php echo $s->style_id ?>')"><?= $s->style_name ?></a>
            <?php } ?>
                        </div>-->
        </div>

        <div class="col-sm-12">
            <div id="exdata" class="mb-3">
                <p>Paste excel data here:</p>
                <textarea name="excel_data" style="width:100%;height:150px;" onblur="javascript:generateTable()"></textarea>
            </div>
        <!-- end box / title -->
        <?= form_open('#', array('id' => 'formStylePrice', 'name' => 'formStylePrice')) ?>

        <div class="form-row row">

            <div class="form-group mb-3 col-md-12">
                <label class="col-form-label">Price Style name</label>
                <input type="text" name="price_style_name" class="form-control" placeholder="Enter style name" required>
                <input type="hidden" name="style_type" class="form-control style_type" value="1">
            </div>


            <div class="form-group mb-3 col-md-12">
                <label for="productquantity" class="col-form-label">Price Sheet</label>
                <div id="excel_table"></div>
            </div>


            <div class="form-group mb-0 col-md-12">
                <?php if($menu_permission['access_permission'][0]->can_create==1  || $menu_permission['is_admin'][0]->is_admin==1){?>
                <button type="submit" class="btn btn-primary my-4 float-right">Add</button>
                 <?php }?>
            </div>
        </div>
			</div>
		<div class="col-sm-12 col-md-12 col-lg-12">
        <?php echo form_close(); ?>
		
            <!-- <p>The .table-bordered class adds borders to a table:</p>             -->
			<div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Edit Price by</th>
                        <th>Apply to</th>
                        <th>Value</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>

                <tbody>

                    <tr>

                        <td>
							<div class="form-check mb-1">
							  <input class="form-check-input" type="radio" name="optradio" id="percentage">
							  <label class="form-check-label" for="percentage">
								Percentage
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="optradio" id="fixed">
							  <label class="form-check-label" for="fixed">
								Fixed
							  </label>
							</div>
                        </td>

                        <td>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="apply" id="whole">
							  <label class="form-check-label" for="fixed">
								Whole table
							  </label>
							</div>
                        </td>

                        <td>
                            <label class="mb-1">
                                Enter value
                            </label>

                            <input type="number" name="val" id="setVal" class="form-control" min="0">

                        </td>

                        <td class="text-right">
                            
                            <button class="btn btn-sm btn-success" onclick="plusData()" data-toggle="tooltip" data-placement="top" data-original-title="Add">+</button>
                            <button class="btn btn-sm btn-danger" onclick="minusData()" data-toggle="tooltip" data-placement="top" data-original-title="Remove">-</button>

                        </td>

                    </tr>

                </tbody>
            </table>
			</div>
		</div>
    </div>
</div>
<!-- end content / right -->

<style type="text/css">
    .fr {
        padding: 2px;
        font-size: 10px;
        color: #495057;
        background-color: #fff;
        border: 1px solid #ced4da;
        border-radius: 0;
    }

</style>

<?php
$this->load->view($price_style_js);
?>

