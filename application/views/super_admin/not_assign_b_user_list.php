 <label class="col-xl-3 col-lg-3 col-form-label">B user Bridge</label>
    <div class="col-lg-9 col-xl-6">
        <select class="form-control" name="bridge_b">
            <option>Select B User</option>
            <?php
            if (isset($b_user_list) && count($b_user_list) != 0) :
                foreach ($b_user_list AS $key => $value): ?>
                    <option value="<?php echo $value->id; ?>">
                        <?php echo $value->first_name; ?>
                    </option>
                <?php endforeach;
            endif; ?>
        </select>
    </div>