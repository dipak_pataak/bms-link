<?php echo load_view('super_admin/layouts/header');?>
<?php $assets_url =  base_url().'super_admin_assets/';?>
<!-- begin::Body -->
<body  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

<!-- begin::Page loader -->

<!-- end::Page Loader -->
<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <!--begin::Aside-->
            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(<?php echo $assets_url;?>media//bg/bg-4.jpg);">
                <div class="kt-grid__item">
                    <a href="#" class="kt-login__logo">
                        <img src="<?php echo $assets_url;?>logo/bmslink-logo.png">
                    </a>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--middle">
                        <h3 class="kt-login__title">Welcome to BMS LINK</h3>
                        <h4 class="kt-login__subtitle"></h4>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright">
                            &copy 2019 BMS LINK
                        </div>
                        <!-- <div class="kt-login__menu">
                            <a href="#" class="kt-link">Privacy</a>
                            <a href="#" class="kt-link">Legal</a>
                            <a href="#" class="kt-link">Contact</a>
                        </div> -->
                    </div>
                </div>
            </div>
            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
                <!--begin::Head-->
                <div class="kt-login__head">
                   <!-- <span class="kt-login__signup-label">Don't have an account yet?</span>&nbsp;&nbsp;
                    <a href="#" class="kt-link kt-login__signup-link">Sign Up!</a>-->
                </div>
                <!--end::Head-->

                <!--begin::Body-->
                <div class="kt-login__body">

                    <!--begin::Signin-->
                    <div class="kt-login__form">
                        <div class="kt-login__title">
                            <h3>Sign In</h3>
                        </div>



                        <!--begin::Form-->
                        <form class="kt-form" method="post"  action="<?php echo base_url(); ?>super_admin/auth_controller/authentication" novalidate="novalidate" id="kt_login_form">
                            <div class="p-1">
                                <?php
                                $error = $this->session->flashdata('error');
                                $success = $this->session->flashdata('success');
                                if ($error != '') {
                                    echo $error;
                                }
                                if ($success != '') {
                                    echo $success;
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" placeholder="Password" name="password"  autocomplete="off">
                            </div>
                            <!--begin::Action-->
                            <div class="kt-login__actions">
                                <a href="#" class="kt-link kt-login__link-forgot">
                                   <!-- Forgot Password ?-->
                                </a>
                                <button  class="btn btn-primary btn-elevate kt-login__btn-primary">Sign In</button>
                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->

                        <!--begin::Divider-->
                       <!-- <div class="kt-login__divider">
                            <div class="kt-divider">
                                <span></span>
                                <span>OR</span>
                                <span></span>
                            </div>
                        </div>-->
                        <!--end::Divider-->

                        <!--begin::Options-->
                      <!--  <div class="kt-login__options">
                            <a href="#" class="btn btn-primary kt-btn">
                                <i class="fab fa-facebook-f"></i>
                                Facebook
                            </a>

                            <a href="#" class="btn btn-info kt-btn">
                                <i class="fab fa-twitter"></i>
                                Twitter
                            </a>

                            <a href="#" class="btn btn-danger kt-btn">
                                <i class="fab fa-google"></i>
                                Google
                            </a>
                        </div>-->
                        <!--end::Options-->
                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>

<?php echo load_view('super_admin/layouts/footer');?>

<!--

<!DOCTYPE html>
<html lang="en">
<head>
    <title>BMS Link</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php /*echo base_url(); */?>assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php /*echo base_url(); */?>assets/b_level/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php /*echo base_url(); */?>assets/b_level/resources/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?php /*echo base_url(); */?>assets/b_level/resources/css/style.css" media="screen" />
    <link id="color" rel="stylesheet" type="text/css" href="<?php /*echo base_url(); */?>assets/b_level/resources/css/colors/blue.css" />

</head>
<body>
<div id="login">

    <div class="p-1">
        <?php
/*        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        */?>
    </div>


    <div class="title">
        <h5>Sign In to BMSLink</h5>
        <div class="corner tl"></div>
        <div class="corner tr"></div>
    </div>



    <div class="inner">

        <form action="<?php /*echo base_url(); */?>super_admin/auth_controller/authentication" method="post">
            <div class="form">

                <div class="fields">

                    <div class="field">
                        <div class="label">
                            <label for="username">Username:</label>
                        </div>
                        <div class="input">
                            <input type="text" id="username" name="email" size="40" autocomplete="off" class="focus" value="<?php /*echo get_cookie("email"); */?>" required />
                        </div>
                    </div>

                    <div class="field">
                        <div class="label">
                            <label for="password">Password:</label>
                        </div>
                        <div class="input">
                            <input type="password" id="password" name="password" size="40" class="focus" value="<?php /*echo get_cookie("password"); */?>" required />
                        </div>
                    </div>

                    <div class="field">
                        <div class="checkbox">
                            <input type="checkbox" id="remember" name="remember" value="1" />
                            <label for="remember">Remember me</label>
                        </div>
                    </div>

                    <div class="buttons">

                        <input type="submit" class="btn btn-success text-right"  value="Login">
                    </div>
                </div>

            </div>
        </form>
    </div>

</div>


<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/jquery-3.3.1.min.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/smooth.js"></script>

</body>
</html>-->
