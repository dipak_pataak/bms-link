
<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
       <br>
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                          Package info:
                        </h3>
                    </div>
                </div>



            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Package</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text"
                                                           class="form-control"
                                                           id="package"
                                                           name="package"
                                                           value="<?php if(isset($data->package) && !empty($data->package)): echo $data->package; else: echo set_value('package'); endif; ?>"
                                                           required>

                                                    <!-- <input class="form-control" type="text" value="Nick">-->
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Trial days</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Days</span>
                                                        </div>
                                                        <input type="number"
                                                               class="form-control"
                                                               id="trial_days"
                                                               min="0" oninput="validity.valid||(value='');"
                                                               name="trial_days"
                                                               value="<?php if(isset($data->trial_days)): echo $data->trial_days; else: echo set_value('trial_days'); endif; ?>"
                                                                />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Installation price</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                        <input type="number"
                                                               class="form-control"
                                                               id="installation_price"
                                                               name="installation_price"
                                                               value="<?php if(isset($data->installation_price)): echo $data->installation_price; else: echo set_value('installation_price'); endif; ?>"
                                                                />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Monthly price</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                        <input type="number"
                                                               class="form-control"
                                                               id="monthly_price"
                                                               name="monthly_price"
                                                               value="<?php if(isset($data->monthly_price)): echo $data->monthly_price; else: echo set_value('monthly_price'); endif; ?>"
                                                                />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Yearly price</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                        <input type="number"
                                                               class="form-control"
                                                               id="yearly_price"
                                                               name="yearly_price"
                                                               value="<?php if(isset($data->yearly_price)): echo $data->yearly_price; else: echo set_value('yearly_price'); endif; ?>"
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <!-- <i class="kt-font-brand flaticon2-line-chart"></i> -->
                        </span>
                        <h3 class="kt-portlet__head-title">
                          Package Access
                        </h3>
                    </div>
                </div>
                                          
                                            <input type="hidden" name="id" value="<?php if(isset($data->id) && !empty($data->id)): echo $data->id; else: 0; endif ?>">
    <!-- Package Option -->

    <input type="hidden" name="column" value="<?php echo $column; ?>">



<div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">  
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded" id="kt_datatable_latest_orders" style="">
                    <table class="kt-datatable__table" style="display: block; max-height: 2500px;">
                    <tbody class="kt-datatable__body ps ps--active-y" style="max-height: 2448px;">

                        <?php foreach ($package_option as $key => $value) { ?>

                                     <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td class="kt-datatable__cell kt-datatable__toggle-detail">
                                            <a class="kt-datatable__toggle-detail" href="">
                                            </a>
                                        </td>
                                        <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check" data-field="RecordID"><span style="width: 50px;"><label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        </span>
                                    </td>
                                    <td data-field="ShipName" data-autohide-disabled="false" class="kt-datatable__cell">
                                        <span style="width: 100px;">                        
                                            <div class="kt-user-card-v2">                            
                                                     <?php echo $value->title; ?>
                                                            
                                            </div>
                                        </span>
                                    </td>
                                    <input type="hidden" class="form-control" id="package_key" name="package_key[]" value="<?php if(isset($value->package_key)): echo $value->package_key; else: ''; endif; ?>" />
                                    <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check" data-field="RecordID"><span style="width: 350px;"><label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                        <input type="checkbox" id="package_value" name="package_value[<?php echo $value->package_key; ?>]" <?php if(isset($value->$column) && $value->$column==1): echo 'checked'; else: ''; endif; ?> 

                                        <?php if($value->package_key=='setting_basic' || $value->package_key=='setting_advanced' || $value->package_key=='setting_pro'){ echo "disabled='disabled'"; } ?>
                                            
                                    

                                         /><span></span></label>
                                        
                                        </span>
                                    </td>
                                </tr>  

                            <!-- <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label"><?php echo $value->title; ?></label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">

                                        <input type="hidden" class="form-control" id="package_key" name="package_key[]" value="<?php if(isset($value->package_key)): echo $value->package_key; else: ''; endif; ?>" />
                                        
                                        <input type="checkbox" class="kt-checkbox kt-checkbox--single kt-checkbox--solid" id="package_value" name="package_value[<?php echo $value->package_key; ?>]" <?php if(isset($value->$column) && $value->$column==1): echo 'checked'; else: ''; endif; ?>  />

                                    </div>
                                </div>
                            </div> -->
                        <?php } ?>
                    </tbody>
                </table>  
            </div>
            </div>

    <!-- Package Option -->

                                            <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <button type="button" onclick="add_update_details('add_update_form','<?php echo base_url('super_admin/Package_controller/update_wholesaler_package'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>	</div>
    <!-- end:: Content -->						</div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>

