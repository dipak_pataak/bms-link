<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>
<style>
    .phone-format-div .input-group {
        flex-wrap: unset !important;
    }
    .phone-format-div .iti.iti--allow-dropdown{
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">
                        <?php if (isset($data->id) && !empty($data->id)) {
                            echo "Edit User";
                        } else {
                            echo "Add User";
                        } ?>

                    </h3>

                    <!--    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                    <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                                            Alex Stone                                    </span>-->

                </div>

            </div>
            <div class="kt-subheader__toolbar">

                <!-- <a href="#" class="btn btn-default btn-bold">

                        Back                </a>

                    <div class="btn-group">
                        <button type="button" class="btn btn-brand btn-bold">

                            Save Changes                        </button>
                        <button type="button" class="btn btn-brand btn-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="kt-nav">
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-writing"></i>
                                        <span class="kt-nav__link-text">Save &amp; continue</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-medical-records"></i>
                                        <span class="kt-nav__link-text">Save &amp; add new</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon flaticon2-hourglass-1"></i>
                                        <span class="kt-nav__link-text">Save &amp; exit</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
-->
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">User Info:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name here" value="<?php if (isset($data->first_name) && !empty($data->first_name)) : echo $data->first_name;  endif; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name here" value="<?php if (isset($data->last_name)) : echo $data->last_name; endif; ?>" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">B User Type</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" id="user_type_status" name="user_type_status" onchange="not_assign_b_user_list()">
                                                       <?php foreach (get_b_user_type_status() as  $value) { ?>
                                                       <option <?php if(isset($data->user_type_status) && $data->user_type_status == $value['id']){ echo "selected"; } ?>  value="<?= $value['id'] ?>">
                                                           <?= $value['status'] ?>
                                                       </option>
                                                       <?php } ?>
                                                    </select>
                                                    <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                                                </div>
                                            </div>

                                            <!-- ********* b user ******* -->
                                            <div class="form-group row" id="unassign_b_user" >
                                                <label class="col-xl-3 col-lg-3 col-form-label">B user Bridge</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" name="bridge_b">

                                                        <?php
                                                        if (isset($b_user_list) && count($b_user_list) != 0) :
                                                            foreach ($b_user_list AS $key => $value): ?>
                                                            <?php if(isset($data->bridge_b) && !empty($data->bridge_b)):?>
                                                                <?php if($data->bridge_b == $value->id) : ?>
                                                                    <option selected  value="<?php echo $value->id; ?>">
                                                                        <?php echo $value->first_name.' '.$value->last_name; ?>
                                                                    </option>
                                                                <?php endif; ?>
                                                            <?php else : ?>
                                                                <?php if($key == 0): ?>
                                                                    <option value="">Select B User</option>
                                                                <?php endif;?>
                                                                <option  value="<?php echo $value->id; ?>">
                                                                    <?php echo $value->first_name.' '.$value->last_name; ?>
                                                                </option>
                                                            <?php endif;?>
                                                            <?php endforeach;
                                                        endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- ********* B user ******** -->

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                        <input type="email" class="form-control" id="email" placeholder="Enter email here" name="email" value="<?php if (isset($data->email)) : echo $data->email; endif; ?>" />

                                                    </div>
                                                </div>
                                            </div>

                                            <?php if (!isset($data->id) || empty($data->id)) :  ?>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input type="password" class="form-control" id="password" placeholder="Enter password here" name="password" required>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($data->id) || !empty($data->id)) :  ?>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input type="password" class="form-control" id="password" placeholder="*****" name="password" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Confirm Password</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input type="password" class="form-control" id="confirm" placeholder="*****" name="confirm" required>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <div class="form-group row phone-format-div">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                        <input id="phone_1" class="form-control phone phone-format" type="text" name="phone" value="<?php if (isset($data->phone)) : echo $data->phone; endif; ?>" required placeholder="+1 (XXX) XXX-XXXX">
                                                    </div>
                                                    <!--  <span class="form-text text-muted">We'll never share your email with anyone else.</span>-->
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Company</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="company" name="company" value="<?php if (isset($data->company)) : echo $data->company; else : echo set_value('company'); endif; ?>" placeholder="Enter company name here" required>
                                                    <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="address" name="address" value="<?php if (isset($data->address)) : echo $data->address; else : echo set_value('address'); endif; ?>" placeholder="Enter address here" autocomplete="off" required>
                                                    <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="city" name="city" value="<?php if (isset($data->city)) : echo $data->city; else : echo set_value('city'); endif; ?>" placeholder="Enter city here">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="state" name="state" value="<?php if (isset($data->state)) : echo $data->state; else : echo set_value('state'); endif; ?>" placeholder="Enter status here">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Zip</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="zip_code" placeholder="Enter zip code here" value="<?php if (isset($data->zip_code)) : echo $data->zip_code; endif; ?>" name="zip_code">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Country code</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="country_code" placeholder="Enter country code here" value="<?php if (isset($data->country_code)) : echo $data->country_code; endif; ?>" name="country_code">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Sub Domain</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="sub_domain" placeholder="Sub domain" onkeyup="sub_domain_validate()" value="<?php if (isset($data->sub_domain)) : echo $data->sub_domain; endif; ?>" name="sub_domain">
                                                    <span id="subdomain_validate" style="color: green;"></span>
                                                    <span id="subdomain_not_validate" style="color: red;"></span>

                                                </div>
                                            </div>


                                            <input type="hidden" name="id" value="<?php if (isset($data->id) && !empty($data->id)) : echo $data->id;  else : 0;  endif ?>">

                                            <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <button type="button" onclick="add_update_details('add_update_form','<?php echo base_url('super_admin/B_user_controller/b_user_save'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<script>

//    ============= its for  google geocomplete =====================
load_country_dropdown('phone_1', country_code_phone_format); // For phone format

google.maps.event.addDomListener(window, 'load', function() {
    var places = new google.maps.places.Autocomplete(document.getElementById('address'));

    google.maps.event.addListener(places, 'place_changed', function() {
        var place = places.getPlace();
        console.log(place);
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: parseFloat(latitude),
            lng: parseFloat(longitude)
        };
        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            if (status === 'OK') {
                //console.log(results)
                if (results[0]) {
                    //document.getElementById('location').innerHTML = results[0].formatted_address;
                    var street = "";
                    var city = "";
                    var state = "";
                    var country = "";
                    var country_code = "";
                    var zipcode = "";
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].types[0] === "locality") {
                            city = results[i].address_components[0].long_name;
                            state = results[i].address_components[2].short_name;

                        }
                        if (results[i].types[0] === "postal_code" && zipcode == "") {
                            zipcode = results[i].address_components[0].long_name;

                        }
                        if (results[i].types[0] === "country") {
                            country = results[i].address_components[0].long_name;
                        }
                        if (results[i].types[0] === "country") {
                            country_code = results[i].address_components[0].short_name;
                        }
                        if (results[i].types[0] === "route" && street == "") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }

                        }
                        if (results[i].types[0] === "street_address") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }

                        }
                    }
                    if (zipcode == "") {
                        if (typeof results[0].address_components[8] !== 'undefined') {
                            zipcode = results[0].address_components[8].long_name;
                        }
                    }
                    if (country == "") {
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country = results[0].address_components[7].long_name;
                        }
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country_code = results[0].address_components[7].short_name;
                        }
                    }
                    if (state == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            state = results[0].address_components[5].short_name;
                        }
                    }
                    if (city == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            city = results[0].address_components[5].long_name;
                        }
                    }

                    var address = {
                        "street": street,
                        "city": city,
                        "state": state,
                        "country": country,
                        "country_code": country_code,
                        "zipcode": zipcode,
                    };
                    //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                    //                        console.log(zipcode);
                    $("#city").val(city);
                    $("#state").val(state);
                    $("#zip_code").val(zipcode);
                    $("#country_code").val(country_code);

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    });
});


function sub_domain_validate() {
    var sub_domain = $('#sub_domain').val();
    var mailformat = /^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/;
    if (sub_domain.match(mailformat)) {
        $('#subdomain_not_validate').text('');
        $('#subdomain_validate').text('');
        return true;
    } else {
        $('#subdomain_validate').text('');
        $('#subdomain_not_validate').text("Special characters Not allowed");
        return false;
    }
}


// A $( document ).ready() block.
$( document ).ready(function() {
    not_assign_b_user_list();
});

function not_assign_b_user_list()
{
    var type = $('#user_type_status option:selected').val();
    if(type == 1)
    {
        $('#unassign_b_user').show();
    }
    else {
        $('#unassign_b_user').hide();
    }


}

</script>
