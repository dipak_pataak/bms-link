<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>
<style>
    .phone-format-div .input-group {
        flex-wrap: unset !important;
    }
    .phone-format-div .iti.iti--allow-dropdown{
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">
                        <?php if (isset($data->id) && !empty($data->id)) {
                            echo "Company Setting";
                        } else {
                            echo "Company Setting";
                        } ?>

                    </h3>

                </div>

            </div>
           
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Company Info:</h3>
                                                </div>
                                            </div>

                                            <input type="hidden" name="company_id" value="<?php echo $data->company_id; ?>">
                                            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Company Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter first name here" value="<?php if (isset($data->company_name) && !empty($data->company_name)) : echo $data->company_name;  endif; ?>" required>
                                                </div>
                                            </div>
                                           

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                     <input type="email" class="form-control" id="email" placeholder="Enter email here" name="email" value="<?php if (isset($data->email)) : echo $data->email; endif; ?>" />

                                                    </div>
                                                </div>
                                            </div>

                                          

                                            <div class="form-group row phone-format-div">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                        <input id="phone_1" class="form-control phone phone-format" type="text" name="phone" value="<?php if (isset($data->phone)) : echo $data->phone; endif; ?>" required placeholder="+1 (XXX) XXX-XXXX">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Choose Unit</label>
                                                <div class="col-lg-9 col-xl-6">
                                                   <select class="form-control" name="unit" id="unit" tabindex="5">
                                                        <option value="inches" <?php
                                                        if ($data->unit == 'inches') {
                                                            echo "selected";
                                                        }
                                                        ?>>inches</option>
                                                        <option value="cm" <?php
                                                        if ($data->unit == 'cm') {
                                                            echo "selected";
                                                        }
                                                        ?>>cm</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Choose Currency</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" name="currency" id="currency" tabindex="5">
                                                        <option value="">-- select one --</option>
                                                        <option value="$" <?php
                                                        if ($data->currency == '$') {
                                                            echo "selected";
                                                        }
                                                        ?>>$ USD</option>
                                                        <option value="AU$" <?php
                                                        if ($data->currency == 'AU$') {
                                                            echo "selected";
                                                        }
                                                        ?>>$ AUD</option>
                                                        <option value="ƒ" <?php
                                                        if ($data->currency == 'ƒ') {
                                                            echo "selected";
                                                        }
                                                        ?>>ƒ AWD</option>
                                                        <option value="R$" <?php
                                                        if ($data->currency == 'R$') {
                                                            echo "selected";
                                                        }
                                                        ?>>R$ BRL</option>
                                                        <option value="¥" <?php
                                                        if ($data->currency == '¥') {
                                                            echo "selected";
                                                        }
                                                        ?>>¥ CNY</option>
                                                        <option value="₡" <?php
                                                        if ($data->currency == '₡') {
                                                            echo "selected";
                                                        }
                                                        ?>>₡ CRC</option>
                                                        <option value="kn" <?php
                                                        if ($data->currency == 'kn') {
                                                            echo "selected";
                                                        }
                                                        ?>>kn HRK</option>
                                                        <option value="£" <?php
                                                        if ($data->currency == '£') {
                                                            echo "selected";
                                                        }
                                                        ?>>£ EGP</option>
                                                        <option value="€" <?php
                                                        if ($data->currency == '€') {
                                                            echo "selected";
                                                        }
                                                        ?>>€ EUR</option>
                                                        <option value="Rs" <?php
                                                        if ($data->currency == 'Rs') {
                                                            echo "selected";
                                                        }
                                                        ?>>Rs INR</option>
                                                        <option value="R" <?php
                                                        if ($data->currency == 'R') {
                                                            echo "selected";
                                                        }
                                                        ?>>R ZAR</option>
                                                        <option value="₩" <?php
                                                        if ($data->currency == '₩') {
                                                            echo "selected";
                                                        }
                                                        ?>>₩ KRW</option>
                                                        <option value="৳" <?php
                                                        if ($data->currency == '৳') {
                                                            echo "selected";
                                                        }
                                                        ?>>৳ BDT</option>
                                                        <option value="₨" <?php
                                                        if ($data->currency == '₨') {
                                                            echo "selected";
                                                        }
                                                        ?>> PKR</option>
                                                        <option value="₣" <?php
                                                        if ($data->currency == '₣') {
                                                            echo "selected";
                                                        }
                                                        ?>>Swiss Franc ₣</option>
                                                        <option value="ر.س" <?php
                                                        if ($data->currency == 'ر.س') {
                                                            echo "selected";
                                                        }
                                                        ?>>Saudi Riyal ر.س</option>
                                                        <option value="₣" <?php
                                                        if ($data->currency == '₣') {
                                                            echo "selected";
                                                        }
                                                        ?>>₣</option>
                                                        <option value="د.إ" <?php
                                                        if ($data->currency == 'د.إ') {
                                                            echo "selected";
                                                        }
                                                        ?>>UAE Dirham د.إ</option>د.إ
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Choose File</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="file" class="form-control" id="image" name="image" >
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Preview</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <?php if ($user_detail->user_type=='b') { ?>
                                                        <img src="<?php echo base_url(); ?>assets/b_level/uploads/appsettings/<?php echo $data->logo; ?>" height="100px">
                                                    <?php } ?>
                                                    <?php if ($user_detail->user_type=='c') { ?>
                                                        <img src="<?php echo base_url(); ?>assets/c_level/uploads/appsettings/<?php echo $data->logo; ?>" height="100px">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="address" name="address" value="<?php if (isset($data->address)) : echo $data->address; else : echo set_value('address'); endif; ?>" placeholder="Enter address here" autocomplete="off" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="city" name="city" value="<?php if (isset($data->city)) : echo $data->city; else : echo set_value('city'); endif; ?>" placeholder="Enter city here">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="state" name="state" value="<?php if (isset($data->state)) : echo $data->state; else : echo set_value('state'); endif; ?>" placeholder="Enter status here">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Zip</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="zip_code" placeholder="Enter zip code here" value="<?php if (isset($data->zip_code)) : echo $data->zip_code; endif; ?>" name="zip_code">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Country code</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="country_code" placeholder="Enter country code here" value="<?php if (isset($data->country_code)) : echo $data->country_code; endif; ?>" name="country_code">
                                                </div>
                                            </div>

                                        

                                            <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <?php if ($user_detail->user_type=='b') { ?>
                                                            <button type="button" onclick="add_update_image_details('add_update_form','<?php echo base_url('super_admin/B_user_controller/update_company_detail'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>
                                                        <?php } ?>
                                                        <?php if ($user_detail->user_type=='c') { ?>
                                                            <button type="button" onclick="add_update_image_details('add_update_form','<?php echo base_url('super_admin/Customer_controller/update_company_detail'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>
                                                        <?php } ?>


                                                        
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<script>

//    ============= its for  google geocomplete =====================
load_country_dropdown('phone_1', country_code_phone_format); // For phone format

google.maps.event.addDomListener(window, 'load', function() {
    var places = new google.maps.places.Autocomplete(document.getElementById('address'));

    google.maps.event.addListener(places, 'place_changed', function() {
        var place = places.getPlace();
        console.log(place);
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: parseFloat(latitude),
            lng: parseFloat(longitude)
        };
        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            if (status === 'OK') {
                //console.log(results)
                if (results[0]) {
                    //document.getElementById('location').innerHTML = results[0].formatted_address;
                    var street = "";
                    var city = "";
                    var state = "";
                    var country = "";
                    var country_code = "";
                    var zipcode = "";
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].types[0] === "locality") {
                            city = results[i].address_components[0].long_name;
                            state = results[i].address_components[2].short_name;

                        }
                        if (results[i].types[0] === "postal_code" && zipcode == "") {
                            zipcode = results[i].address_components[0].long_name;

                        }
                        if (results[i].types[0] === "country") {
                            country = results[i].address_components[0].long_name;
                        }
                        if (results[i].types[0] === "country") {
                            country_code = results[i].address_components[0].short_name;
                        }
                        if (results[i].types[0] === "route" && street == "") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }

                        }
                        if (results[i].types[0] === "street_address") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }

                        }
                    }
                    if (zipcode == "") {
                        if (typeof results[0].address_components[8] !== 'undefined') {
                            zipcode = results[0].address_components[8].long_name;
                        }
                    }
                    if (country == "") {
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country = results[0].address_components[7].long_name;
                        }
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country_code = results[0].address_components[7].short_name;
                        }
                    }
                    if (state == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            state = results[0].address_components[5].short_name;
                        }
                    }
                    if (city == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            city = results[0].address_components[5].long_name;
                        }
                    }

                    var address = {
                        "street": street,
                        "city": city,
                        "state": state,
                        "country": country,
                        "country_code": country_code,
                        "zipcode": zipcode,
                    };
                    //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                    //                        console.log(zipcode);
                    $("#city").val(city);
                    $("#state").val(state);
                    $("#zip_code").val(zipcode);
                    $("#country_code").val(country_code);

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    });
});


</script>
