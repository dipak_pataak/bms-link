<?php echo load_view('super_admin/layouts/header'); ?>

<?php echo load_view('super_admin/layouts/top_header'); ?>
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Javascript -->
<script>
    $(function() {
        $( ".datepicker-13" ).datepicker();
    });
</script>



<style>
    .phone-format-div .input-group {
        flex-wrap: unset !important;
    }
    .phone-format-div .iti.iti--allow-dropdown{
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">
                        <?php if (isset($data->id) && !empty($data->id)) {
                            echo "Edit Coupon";
                        } else {
                            echo "Add Coupon";
                        } ?>

                    </h3>

                </div>

            </div>
          
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Edit Coupon :</h3>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Coupon</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="coupon" name="coupon" placeholder="Enter Coupon" value="<?php if (isset($data->coupon) && !empty($data->coupon)) : echo $data->coupon;  endif; ?>" required onkeyup="coupon_validate();">
                                                    <span id="coupon_validate" style="color: red;"></span>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Discount Type</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" id="type" name="type" onchange="descount_type(this.value)">
                                                       <option <?php if(isset($data->type) && $data->type == 1){ echo "selected"; } ?>  value="1">
                                                           Flat
                                                       </option>
                                                       <option <?php if(isset($data->type) && $data->type == 2){ echo "selected"; } ?>  value="2">
                                                           Percent
                                                       </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div id="coupon_type">
                                                
                                            </div>


                                             <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Min Purchase</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="number" class="form-control" id="min_purchase" name="min_purchase" placeholder="Min Purchase" value="<?php if (isset($data->min_purchase) && !empty($data->min_purchase)) : echo $data->min_purchase;  endif; ?>" required>
                                                </div>
                                            </div>


                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <select class="form-control" id="status" name="status" >
                                                <option <?php if(isset($data->status) && $data->status == 1){ echo "selected"; } ?>  value="1">
                                                   Active
                                                </option>
                                                <option <?php if(isset($data->status) && $data->status == 0){ echo "selected"; } ?>  value="0">
                                                   Inactive
                                                </option>
                                            </select>
                                        </div>
                                    </div>
<?php $start_date=date_create($data->start_date); ?>  
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Start date</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control datepicker-13" id="start_date" name="start_date" placeholder="Enter Start Date" value="<?php if (isset($data->start_date) && !empty($data->start_date)) : echo date_format($start_date,"m/d/Y");  endif; ?>" required >
                                        </div>
                                    </div>
<?php $exp_date=date_create($data->exp_date); ?>  
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">End date</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="text" class="form-control datepicker-13" id="exp_date" name="exp_date" placeholder="Enter Exp Date" value="<?php if (isset($data->exp_date) && !empty($data->exp_date)) : echo date_format($exp_date,"m/d/Y");  endif; ?>" required >
                                        </div>
                                    </div>

                                            <input type="hidden" name="id" value="<?php if (isset($data->id) && !empty($data->id)) : echo $data->id;  else : 0;  endif ?>">

                                            <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <button type="button" onclick="add_update_details('add_update_form','<?php echo base_url('super_admin/Coupon_controller/Coupon_save'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>

<script>

    function descount_type(value)
    {
        if(value==1) 
        {
            $('#coupon_type').empty();
            $('#coupon_type').append(`<input type="hidden" name="max_discount" value="0">
                                      <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Flat</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="number" class="form-control" id="flat" name="flat" placeholder="Flat Discount" value="<?php if (isset($data->flat) && !empty($data->flat)) : echo $data->flat;  endif; ?>" required>
                                            </div>
                                      </div>`);
        }
        if(value==2) 
        {
            $('#coupon_type').empty();
            $('#coupon_type').append(`<input type="hidden" name="flat" value="0">
                                      <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">percent</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="number" class="form-control" id="percent" name="percent" placeholder="Percent" value="<?php if (isset($data->percent) && !empty($data->percent)) : echo $data->percent;  endif; ?>" required>
                                            </div>
                                      </div>
                                      <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Max Discount</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="number" class="form-control" id="max_discount" name="max_discount" placeholder="Max Discount" value="<?php if (isset($data->max_discount) && !empty($data->max_discount)) : echo $data->max_discount;  endif; ?>" required>
                                            </div>
                                      </div>`);

        }
    }

    $(document).ready(function(){
        var value = $('#type').val();
        descount_type(value);
    })

    function coupon_validate() {
    var coupon = $('#coupon').val();
    var format = /^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/;
    if (coupon.match(format)) {
        $('#coupon_validate').text('');
        return true;
    } else {
        $('#coupon_validate').text('');
        $('#coupon_validate').text("Special characters Not allowed");
        return false;
    }
}
</script>
