<?php $assets_url = base_url() . 'super_admin_assets/'; ?>
<?php $currency = '$';?>
<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>
<link href="<?= $assets_url; ?>css/demo2/pages/invoices/invoice-2.css" rel="stylesheet" type="text/css"/>


<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Order info</h3>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-2">
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <!--<h1 class="kt-invoice__title">INVOICE</h1>-->
                                    <div href="#" class="kt-invoice__logo">
                                        <a href="#">
                                            <?php if (isset($company_profile->logo) && !empty($company_profile->logo)) : ?>
                                                <img src="<?php echo base_url('assets/b_level/uploads/appsettings/') . $company_profile->logo; ?>">
                                            <?php endif; ?>
                                        </a>
                                        <span class="kt-invoice__desc">
                                            <?php if (isset($company_profile->company_name) && !empty($company_profile->company_name)) : ?>
                                                <span><?= $company_profile->company_name; ?></span>
                                            <?php endif; ?>
                                            <?php if (isset($company_profile->address) && !empty($company_profile->address)) : ?>
                                                <span><?= $company_profile->address; ?></span>
                                            <?php endif; ?>
                                            <span>
                                                 <?php if (isset($company_profile->city) && !empty($company_profile->city)) : ?>
                                                     <?= $company_profile->city; ?>,
                                                 <?php endif; ?>
                                                <?php if (isset($company_profile->state) && !empty($company_profile->state)) : ?>
                                                    <?= $company_profile->state; ?>,
                                                <?php endif; ?>
                                                <?php if (isset($company_profile->zip_code) && !empty($company_profile->zip_code)) : ?>
                                                    <?= $company_profile->zip_code; ?>,
                                                <?php endif; ?>
                                                <?php if (isset($company_profile->country_code) && !empty($company_profile->country_code)) : ?>
                                                    <?= $company_profile->country_code; ?>
                                                <?php endif; ?>
                                            </span>
                                            <?php if(isset($company_profile->phone) && !empty($company_profile->phone)) : ?>
                                                <span><?= $company_profile->phone; ?></span>
                                            <?php endif;?>
                                            <?php if(isset($company_profile->email) && !empty($company_profile->email)) : ?>
                                                <span><?= $company_profile->email; ?></span>
                                            <?php endif;?>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <div class="table-responsive">
                                            <table class="table table-bordered mb-4">
                                                <tr class="text-center">
                                                    <td>Order Date</td>
                                                    <td class="text-right"><?= date_format(date_create($orderd->order_date), 'M-d-Y') ?></td>
                                                </tr>
                                                <tr class="text-center">
                                                    <td>Order Id</td>
                                                    <td class="text-right"><?= $orderd->order_id ?></td>
                                                </tr>
                                                <?php if ($shipping != NULL) { ?>
                                                    <tr class="text-center">
                                                        <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                                                        <td class="text-right"> <?= $shipping->track_number ?> </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr class="text-center">
                                                    <td>Barcode</td>
                                                    <td class="text-right"> <?php
                                                        if ($orderd->barcode != NULL) {
                                                            echo '<img src="' . base_url() . $orderd->barcode . '" width="250px;"/>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Sold To:</span>
                                        <span class="kt-invoice__text">
                                            <?php if(isset($customer->first_name) && !empty($customer->first_name)) :
                                                echo $customer->first_name.' '.$customer->last_name;
                                            endif; ?>
                                        </span>

                                        <?php if(isset($customer->address) && !empty($customer->address)): ?>
                                            <span class="kt-invoice__text">
                                                <?= $customer->address;?>
                                            </span>
                                            <span class="kt-invoice__text">
                                                <?= $customer->city;?>,<?= $customer->state; ?>, <?= $customer->zip_code; ?>,<?= $customer->country_code; ?>
                                            </span>
                                        <?php endif;?>

                                        <?php if(isset($customer->phone) && !empty($customer->phone)) : ?>
                                            <span class="kt-invoice__text">
                                              <?= $customer->phone;?>
                                            </span>
                                        <?php endif;?>
                                    </div>

                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Ship To:</span>
                                        <span class="kt-invoice__text">
                                            <?php if(isset($customer->first_name) && !empty($customer->first_name)) :
                                                echo $customer->first_name.' '.$customer->last_name;
                                            endif; ?>
                                        </span>
                                        <?php if(isset($orderd->is_different_shipping) && $orderd->is_different_shipping == 1) : ?>
                                            <span class="kt-invoice__text">
                                                <?= $customer->different_shipping_address;?>
                                            </span>
                                        <?php
                                        else:
                                        if(isset($customer->address) && !empty($customer->address)): ?>
                                            <span class="kt-invoice__text">
                                                <?= $customer->address;?>
                                            </span>
                                            <span class="kt-invoice__text">
                                                <?= $customer->city;?>,<?= $customer->state; ?>, <?= $customer->zip_code; ?>,<?= $customer->country_code; ?>
                                            </span>
                                        <?php endif;?>

                                        <?php if(isset($customer->phone) && !empty($customer->phone)) : ?>
                                            <span class="kt-invoice__text">
                                              <?= $customer->phone;?>
                                            </span>
                                        <?php endif;?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Description</th>
                                            <th>List</th>
                                            <th>Discount(%)</th>
                                            <th>Price</th>
                                            <th>Notes</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1; ?>
                                        <?php
                                        if(isset($order_details) && count($order_details) != 0):
                                            foreach ($order_details as $items): ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <td><?= $items->product_qty; ?></td>
                                                    <td>
                                                        <?php if(!empty($items->product_name)) : ?>
                                                            <strong><?= $items->product_name; ?></strong>
                                                            <br/>
                                                        <?php endif;?>
                                                        <?php if(!empty($items->pattern_name)) : ?>
                                                        <?= $items->pattern_name; ?><br/>
                                                        <?php endif;?>
                                                        W <?= $items->width; ?> <?= @$width_fraction->fraction_value ?>,
                                                        H <?= $items->height; ?> <?= @$height_fraction->fraction_value ?>,
                                                        <?= $items->color_number; ?>
                                                        <?= $items->color_name . ', '; ?>

                                                        <?php
                                                        if (isset($selected_rooms[$items->room])) {
                                                            echo $items->room . ' ' . $selected_rooms[$items->room];
                                                            $selected_rooms[$items->room] ++;
                                                        } else {
                                                            echo $items->room . ' 1';
                                                            $selected_rooms[$items->room] = 2;
                                                        }
                                                        ?>
                                                    </td>
                                                  <!--  <td class="kt-font-danger kt-font-lg">$3200.00</td>-->
                                                    <td><?= $currency; ?><?= $items->list_price ?> </td>
                                                    <td><?= $items->discount ?> %</td>
                                                    <td><?= $currency; ?><?= $items->unit_total_price ?> </td>
                                                    <td><?= $items->notes ?></td>

                                                </tr>
                                                <?php $i++; ?>
                                        <?php endforeach;
                                        endif; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="kt-invoice__footer">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <?php if ($shipping != NULL) { ?>
                                                <th>Shipping cost</th>
                                            <?php } ?>
                                            <th>Sub-Total</th>
                                            <th>Installation Charge</th>
                                            <th>Other Charge</th>
                                            <th>Misc</th>
                                            <th>Discount</th>
                                            <th>Grand Total</th>
                                            <th>Deposit</th>
                                            <th>Due </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <?php if ($shipping != NULL) { ?>
                                                <td class="text-right"> <?= $currency; ?> <?= $shipping->shipping_charges ?> </td>
                                            <?php } ?>
                                            <!-- <td class="text-right"> <?= $currency; ?><?= $orderd->state_tax ?></td> -->

                                            <td> <?= $currency; ?><?= $orderd->subtotal; ?> </td>
                                            <td> <?= $currency; ?><?= $orderd->installation_charge ?> </td>
                                            <td> <?= $currency; ?><?= $orderd->other_charge ?></td>
                                            <td> <?= $currency; ?><?= $orderd->misc ?> </td>
                                            <td> <?= $currency; ?><?= $orderd->invoice_discount ?></td>
                                            <td> <?= $currency; ?><?= $orderd->grand_total ?>
                                            <td> <?= $currency; ?><?= $orderd->paid_amount ?>
                                            <td> <?= $currency; ?><?= $orderd->due ?>
                                           <!-- <td class="kt-font-danger kt-font-xl kt-font-boldest">20,600.00</td>-->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="kt-invoice__actions">
                            <div class="kt-invoice__container">
                               <!-- <button type="button" class="btn btn-label-brand btn-bold" onclick="window.print();">
                                    Download Invoice
                                </button>-->
                                <button type="button" class="btn btn-brand btn-bold" onclick="window.print();">Print
                                    Invoice
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->                        </div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>

