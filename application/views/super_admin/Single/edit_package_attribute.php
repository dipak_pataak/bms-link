<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>
<style>
    .phone-format-div .input-group {
        flex-wrap: unset !important;
    }
    .phone-format-div .iti.iti--allow-dropdown{
        width: 100%;
    }
</style>
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">
                        <?php if (isset($list_data->id) && !empty($list_data->id)) {
                            echo "Edit Package Attribute";
                        } else {
                            echo "Add User";
                        } ?>

                    </h3>

                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                   <h3 class="kt-section__title kt-section__title-sm">Edit Attribute:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Attribute</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="first_name" name="title" placeholder="Enter first name here" value="<?php if (isset($list_data->title) && !empty($list_data->title)) : echo $list_data->title;  endif; ?>" required>
                                                </div>
                                            </div>
                                            

                                            <input type="hidden" name="id" value="<?php if (isset($list_data->id) && !empty($list_data->id)) : echo $list_data->id;  else : 0;  endif ?>">

                                            <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <button type="button" onclick="add_update_details('add_update_form','<?php echo base_url('super_admin/Package_controller/package_attribute_save'); ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save</button>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>
