<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title">
                        <?php if (isset($data->id) && !empty($data->id)) {
                            echo "Edit Customer";
                        } else {
                            echo "Add Customer";
                        } ?>

                    </h3>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="row">
                                                <label class="col-xl-3"></label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <h3 class="kt-section__title kt-section__title-sm">Customer
                                                        Info:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Wholeselar</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" name="b_user_id">
                                                        <option value="0">Select Wholeselar</option>
                                                        <?php
                                                        if (isset($b_user_data) && count($b_user_data) != 0) :
                                                            foreach ($b_user_data as $key => $value) : ?>
                                                             
                                                                  <?php   $company_name=$this->db->where('user_id',$value->id)->get('company_profile')->result(); ?>
                                                                <option <?php if (isset($data->level_id) && $data->level_id == $value->id) {
                                                                                    echo "selected";
                                                                                }; ?> value="<?= $value->id; ?>">
                                                                    <?= $company_name[0]->company_name; ?>
                                                                </option>
                                                        <?php endforeach;
                                                        endif; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Update Package</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="checkbox" id="update_package" onclick="is_update_package(this.value)">
                                                </div>
                                            </div> -->

                                            <input type="hidden" name="is_package_update" id="is_package_update" value="0">


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Select Package</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" name="package_id">
                                                        <?php
                                                        if (isset($package_list) && count($package_list) != 0) :
                                                            foreach ($package_list as $key => $value) : ?>
                                                                <option <?php if (isset($user_info_detail->package_id) && $user_info_detail->package_id == $value->id) {
                                                                                    echo "selected";
                                                                                }; ?> value="<?= $value->id; ?>">
                                                                    <?= $value->package; ?>
                                                                </option>
                                                        <?php endforeach;
                                                        endif; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Select Duration</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <select class="form-control" name="duration">
                                                        <option value="monthly">Monthly</option>
                                                        <option value="yearly">Yearly</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name here" value="<?php if (isset($data->first_name) && !empty($data->first_name)) : echo $data->first_name;
                                                                                                                                                                            else : echo set_value('first_name');
                                                                                                                                                                            endif; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name here" value="<?php if (isset($data->last_name)) : echo $data->last_name;
                                                                                                                                                                        else : echo set_value('last_name');
                                                                                                                                                                        endif; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                        <input type="email" class="form-control" id="email" placeholder="Enter email here" name="email" <?php if (isset($data->email)) : echo 'readonly';
                                                                                                                                                        endif; ?> value="<?php if (isset($data->email)) : echo $data->email;
                                                                                                                                                                            else : echo set_value('email');
                                                                                                                                                                            endif; ?>" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Phone number</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <table class="" id="normalItem" style="margin-top: 10px;">
                                                        <thead>
                                                        </thead>
                                                        <tbody id="addItem">
                                                            <?php

                                                            if (isset($data->customer_id) && !empty($data->customer_id)) :
                                                                $this->db->select('*');
                                                                $this->db->from('customer_phone_type_tbl');
                                                                $this->db->where('customer_id', $data->customer_id);
                                                                $customer_phone_types = $this->db->get()->result();
                                                                $j = 0;
                                                                foreach ($customer_phone_types as $phone_type) :
                                                                    $j++; ?>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <select class="form-control phone_type_select" id="phone_type_<?php echo $j; ?>" name="phone_type[]" required>
                                                                                        <option value="Phone" <?php if ($phone_type->phone_type == 'Phone') : echo 'selected';
                                                                                                                        endif; ?>>
                                                                                            Phone
                                                                                        </option>
                                                                                        <option value="Fax" <?php if ($phone_type->phone_type == 'Fax') : echo 'selected';
                                                                                                                    endif; ?>>
                                                                                            Fax
                                                                                        </option>
                                                                                        <option value="Mobile" <?php if ($phone_type->phone_type == 'Mobile') : echo 'selected';
                                                                                                                        endif; ?>>
                                                                                            Mobile
                                                                                        </option>
                                                                                    </select>
                                                                                </div>

                                                                                <div class="col-md-6">
                                                                                    <input id="phone_<?php echo $j; ?>" class="phone form-control phone_no_type" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" value="<?php echo $phone_type->phone ?>">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-left">
                                                                            <a style="font-size: 20px;" class="text-danger kt-padding-l-10" value="Delete" onclick="deleteRow(this)">
                                                                                <i class="flaticon2-delete"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            <?php else : ?>

                                                                <tr>
                                                                    <td>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <select class="form-control phone_type_select" id="phone_type_1" name="phone_type[]" required>
                                                                                    <option value="Phone" selected>Phone</option>
                                                                                    <option value="Fax">Fax</option>
                                                                                    <option value="Mobile">Mobile</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <input id="phone_1" class="phone form-control phone_no_type phone-format" type="text" name="phone[]" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)">
                                                                            </div>
                                                                        </div>


                                                                    </td>
                                                                    <td class="text-left">
                                                                        <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                                        <a style="font-size: 20px;" class="text-danger kt-padding-l-10" value="Delete" onclick="deleteRow(this)">
                                                                            <i class="flaticon2-delete"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>

                                                    <button id="add-item" class="btn btn-info" name="add-new-item" onclick="addInputField('addItem');" type="button" style="margin: 23px 0px -4px;">
                                                        <span class="fa fa-plus"></span>
                                                        Add Phone
                                                    </button>

                                                </div>
                                            </div>
                                        </div>

                                        <?php if (!isset($data->customer_id) || empty($data->customer_id)) : ?>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="password" class="form-control" id="password" placeholder="Enter password here" name="password" required>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (isset($data->customer_id) || !empty($data->customer_id)) :  ?>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input type="password" class="form-control" id="password" placeholder="*****" name="password" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Confirm Password</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input type="password" class="form-control" id="confirm" placeholder="*****" name="confirm" required>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Company</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="company" name="company" value="<?php if (isset($data->company)) : echo $data->company;
                                                                                                                            else : echo set_value('company');
                                                                                                                            endif; ?>" placeholder="Enter company name here" required>
                                                <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="address" name="address" value="<?php if (isset($data->address)) : echo $data->address;
                                                                                                                            else : echo set_value('address');
                                                                                                                            endif; ?>" placeholder="Enter address here" autocomplete="off" required>
                                                <!--<span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>-->
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="city" name="city" value="<?php if (isset($data->city)) : echo $data->city;
                                                                                                                        else : echo set_value('city');
                                                                                                                        endif; ?>" placeholder="Enter city here">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="state" name="state" value="<?php if (isset($data->state)) : echo $data->state;
                                                                                                                        else : echo set_value('state');
                                                                                                                        endif; ?>" placeholder="Enter status here">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Zip</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="zip_code" placeholder="Enter zip code here" value="<?php if (isset($data->zip_code)) : echo $data->zip_code;
                                                                                                                                                else : echo set_value('zip_code');
                                                                                                                                                endif; ?>" name="zip_code">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Country code</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="country_code" placeholder="Enter country code here" value="<?php if (isset($data->country_code)) : echo $data->country_code;
                                                                                                                                                        else : echo set_value('country_code');
                                                                                                                                                        endif; ?>" name="country_code">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Sub Domain</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" id="sub_domain" placeholder="Sub domain" onkeyup="sub_domain_validate()" value="<?php if (isset($user_info_detail->sub_domain)) : echo $user_info_detail->sub_domain; endif; ?>" name="sub_domain">
                                                <span id="subdomain_validate" style="color: green;"></span>
                                                <span id="subdomain_not_validate" style="color: red;"></span>

                                            </div>
                                        </div>


                                        <input type="hidden" name="customer_id" value="<?php if (isset($data->customer_id) && !empty($data->customer_id)) : echo $data->customer_id;
                                                                                        endif ?>">
                                        <input type="hidden" class="" id="business" name="customer_type" value="business">
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                    <?php

                                                    $url = base_url('super_admin/Customer_controller/add_customer_api');
                                                    if (isset($data->customer_id) && !empty($data->customer_id))
                                                        $url = base_url('super_admin/Customer_controller/update_customer_api');

                                                    ?>


                                                    <button type="button" onclick="add_update_details('add_update_form','<?php echo $url  ?>')" class="btn btn-label-brand btn-bold form-post-btn">Save
                                                    </button>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->
</div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

<script>
    load_country_dropdown('phone_1', country_code_phone_format); // For phone format

    //    ============= its for  google geocomplete =====================
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                        //                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);

                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });

        });


    });

    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            alert("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count,
                e = document.createElement("tr");
            e.innerHTML = `<td>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control phone_type_select" id='phone_type_` + count + `' name="phone_type[]" required>
                                    <option value="Phone" selected>Phone</option>
                                    <option value="Fax">Fax</option>
                                    <option value="Mobile">Mobile</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                            <input id='phone_` + count + `'
                                class='phone form-control phone_no_type current_row phone-format'
                                type='text'
                                onkeyup='special_character(` + count + `)'
                                name='phone[]' placeholder='+1 (XXX) XXX-XXXX'>
                            </div>
                        </div>


                    </td>
                    <td class="text-left">

                        <a style="font-size: 20px;" class="text-danger kt-padding-l-10"  value="Delete" onclick="deleteRow(this)">
                            <i class="flaticon2-delete"></i>
                        </a>
                    </td>`,
                document.getElementById(t).appendChild(e),
                load_country_dropdown('phone_' + count, country_code_phone_format); // For phone format
            //                    document.getElementById(a).focus(),
            count++;
            //=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function(e) {
                    var key = e.charCode || e.keyCode || 0;
                    var phone = $(this);
                    if (phone.val().length === 0) {
                        phone.val(phone.val() + '+1 (');
                    }
                    // Auto-format- do not expose the mask as the user begins to type
                    if (key !== 8 && key !== 9) {
                        //alert("D");
                        if (phone.val().length === 6) {

                            phone.val(phone.val());
                            phone = phone;
                        }
                        if (phone.val().length === 7) {
                            phone.val(phone.val() + ') ');
                        }
                        if (phone.val().length === 12) {
                            phone.val(phone.val() + '-');
                        }
                        if (phone.val().length >= 17) {
                            phone.val(phone.val().slice(0, 16));
                        }
                    }
                    // Allow numeric (and tab, backspace, delete) keys only
                    return (key == 8 ||
                        key == 9 ||
                        key == 46 ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
                })
                .on('focus', function() {
                    phone = $(this);

                    if (phone.val().length === 0) {

                        phone.val('+1 (');
                    } else {
                        var val = phone.val();
                        phone.val('').val(val); // Ensure cursor remains at the end
                    }
                })

                .on('blur', function() {
                    $phone = $(this);

                    if ($phone.val() === '(') {
                        $phone.val('');
                    }
                });
            //            ========== close phone format ============


        }
    }

    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            alert("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);

            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function() {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });

            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function() {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }

    function special_character(t) {
        //        alert(t);
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function(string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }
</script>

<script>
    function is_update_package() {
        var update_package = $('#update_package').is(":checked");
        if (update_package == true) {
            $('#is_package_update').val(1)
        }
        if (update_package == false) {
            $('#is_package_update').val(0)
        }
    }
</script>

<script>
    function sub_domain_validate() 
    {
        var sub_domain = $('#sub_domain').val();
        var mailformat = /^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/;
        if (sub_domain.match(mailformat)) {
            $('#subdomain_not_validate').text('');
            $('#subdomain_validate').text('');
            return true;
        } else {
            $('#subdomain_validate').text('');
            $('#subdomain_not_validate').text("Special characters Not allowed");
            return false;
        }
    }
</script>

