<?php $assets_url =  base_url() . 'super_admin_assets/'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <base href="">
    <!--end::Base Path -->
    <meta charset="utf-8" />
    <title>BMS LINK</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts -->
    <!--begin::Page Custom Styles(used by this page) -->
    <link href="<?php echo $assets_url; ?>css/demo2/pages/login/login-1.css" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->
    <!--begin:: Global Mandatory Vendors -->
    <link href="<?php echo $assets_url; ?>vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!--end:: Global Mandatory Vendors -->
    <!--begin:: Global Optional Vendors -->
    <link href="<?php echo $assets_url; ?>vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/quill/dist/quill.snow.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/dual-listbox/dist/dual-listbox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $assets_url; ?>my_assets/iziToast.min.css" rel="stylesheet" type="text/css" />
    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->

    <link href="<?php echo $assets_url; ?>css/demo2/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <!--end::Layout Skins -->

    <link rel="shortcut icon" href="<?php echo $assets_url; ?>media/logos/favicon.ico" />

    <!-- phone format -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/phone_format/css/intlTelInput.css" />
    <script type="text/javascript">
        var mybase_url = '<?php echo base_url(); ?>';
        var country_code_phone_format = 'us';
    </script>
</head>
<!-- end::Head -->