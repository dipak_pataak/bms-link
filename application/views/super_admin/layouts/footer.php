<?php $uri_segment_1 = $this->uri->segment(1); ?>
<?php $uri_segment_2 = $this->uri->segment(2); ?>
<?php if($uri_segment_1 == 'super-admin' && $uri_segment_2 != '') : ?>
<?php //echo load_view('super_admin/layouts/footer_text');?>
<?php endif;?>
<?php $assets_url =  base_url().'super_admin_assets/';?>

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {"colors":{"state":{"brand":"#374afb","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
</script>
<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="<?php echo $assets_url;?>vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="<?php echo $assets_url;?>vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/dropzone.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/quill/dist/quill.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/@yaireo/tagify/dist/tagify.polyfills.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/@yaireo/tagify/dist/tagify.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/dual-listbox/dist/dual-listbox.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->

<script src="<?php echo $assets_url;?>js/demo2/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?php echo $assets_url;?>vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="<?php echo $assets_url;?>js/demo2/pages/dashboard.js" type="text/javascript"></script>
<!--end::Page Scripts -->

<script src="<?php echo $assets_url;?>vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>my_assets/iziToast.min.js" type="text/javascript"></script>
<script src="<?php echo $assets_url;?>my_assets/common.js" type="text/javascript"></script>

<!-- its for phone format -->
<script src="<?php echo base_url(); ?>assets/phone_format/js/intlTelInput.js"></script>
<script src="<?php echo base_url(); ?>assets/phone_format/js/cleave.min.js"></script>
<script src="<?php echo base_url(); ?>assets/phone_format/js/cleave-phone.i18n.js"></script>
<script src="<?php echo base_url(); ?>assets/phone_format/js/common-phone.js"></script>
</body>
<!-- end::Body -->
</html>



<!--
</div>
<div id="footer">
    <p>Copyright &copy; 2019 BMS Link. All Rights Reserved.</p>
</div>

<script language="javascript" type="text/javascript" src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/excanvas.min.js"></script>


<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/jquery.flot.min.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/bootstrap.bundle.min.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/tiny_mce/jquery.tinymce.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/smooth.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/smooth.menu.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/smooth.chart.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/scripts/select2.full.js"></script>
<script src="<?php /*echo base_url(); */?>assets/b_level/resources/js/bootstrap-datepicker.js"></script>
</div>
</body>
</html>
-->
