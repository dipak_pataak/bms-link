<?php $uri_segment_1 = $this->uri->segment(2); ?>
<?php
$user_id = $this->session->userdata('user_id');
$user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();

?>
<?php if($user_id==1){?>

<div class="kt-header__bottom">
    <div class="kt-container ">
        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i>
        </button>
        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                <ul class="kt-menu__nav ">
                    <li class="kt-menu__item <?php if($uri_segment_1 == 'dashboard'): ?> kt-menu__item--here <?php endif;?>">
                <?php
                $modules = $this->db->select('*')->from('admin_menusetup_tbl')->where('menu_type', 1)->group_by('module')->order_by('ordering', 'asc')->get()->result();
                 $menu_title = $modules[0]->menu_title;



                ?>
                      <a href="<?php echo base_url(); ?>super-admin/dashboard" class="kt-menu__link">
                            <span class="kt-menu__link-text"><?php echo ucfirst($menu_title); ?></span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>            
        <?php
            $modules = $this->db->select('*')->from('admin_menusetup_tbl')->where('menu_type', 1)->where('status', 1)
                            ->group_by('module')->order_by('ordering', 'asc')->get()->result();
                             //dd($modules );
         

            foreach ($modules as $module) {

                
                    if ($module->menu_id==147 && $user_info[0]->user_type_status!=1) 
                    {
                        

                    }else{    

                              if ($this->permission->module($module->module)->access()) {

                                    $menu_item = $this->db->select('*')
                                                    ->from('admin_menusetup_tbl')
                                                    ->where('module', $module->module)
                                                    ->where('parent_menu =', $module->id)
                                                    ->where('status', 1)
                                                    ->order_by('ordering', 'asc')
                                                    ->get()->result();
                                    $module_id = $module->parent_menu;    
                                    $menu_title = '';      
                                    if (empty($menu_item)) {
                                        $link = base_url() . $module->page_url;
                                    }else {
                                        $link = "#" . $module->module;
                                    } 

                              
                    ?> 
                    <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                        <div class="kt-header__brand-nav">
                            <div class="dropdown">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><?php echo ucwords(str_replace('_', ' ',$module->menu_title)); ?></button>
                                   <div class="dropdown-menu dropdown-menu-md">
                                    <ul class="kt-nav kt-nav--bold kt-nav--md-space">
                                       
                                    <?php
                                    foreach ($menu_item as $menu) {
                                         if ($this->permission->check_label($menu->menu_title)->access()) {
                                            $parent_id = $menu->menu_id;
                                            $sub_sub_menu = $this->db->select('*')
                                                ->from('admin_menusetup_tbl')
                                                ->where('module', $menu->module)
                                                ->where('parent_menu =',$menu->id)
                                                ->where('status', 1)
                                                ->order_by('ordering', 'asc')
                                                ->get()->result();
                                                 $submenu_title = $menu->menu_title;
                                    if (empty($sub_sub_menu)) {
                                        $link = base_url() . $menu->page_url;
                                    }else {
                                        $link = "#" . $menu->module;
                                    } 
                                    ?>

                                            <li class="kt-nav__item">
                                                 <a class="kt-nav__link active" href="<?php echo $link;?>">
                                                    <span class="kt-nav__link-icon"><i class="flaticon2-user"></i></span>
                                                    <span class="kt-nav__link-text"><?php echo ucwords(str_replace('_', ' ', $submenu_title)); ?>
                                                    </span>
                                                </a> 

                                            </li> 
                                    <?php                                           
                                    }
                                    }
                                    ?>
                                     </ul>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                      } 
                      }
                    }?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php }else{ ?>
    <?php
        $user_id = $this->session->userdata('user_id');
        $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();

        $id=$user_role_info[0]->role_id;
        $user_data_role=$this->db->query("SELECT * FROM `admin_role_permission_tbl` WHERE ((`role_id` = $id) AND ((`can_access` = 1) OR (`can_create` = 1) OR (`can_delete` = 1) OR (`can_edit` = 1)))")->result();

        foreach ($user_data_role as $user_role_access)
       {
        $menu_ids[]=$user_role_access->menu_id;

    }    
    ?>
<div class="kt-header__bottom">
    <div class="kt-container ">
        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i>
        </button>
        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                <ul class="kt-menu__nav ">
                    <li class="kt-menu__item <?php if($uri_segment_1 == 'dashboard'): ?> kt-menu__item--here <?php endif;?>">
                        <?php
                            $modules = $this->db->select('*')->from('admin_menusetup_tbl')->where('menu_type', 1)->group_by('module')->order_by('ordering', 'asc')->get()->result();
                            //print_r($modules);
                             $menu_title = $modules[0]->menu_title;

                        ?>
                        <a href="<?php echo base_url(); ?>super-admin/dashboard" class="kt-menu__link">
                            <span class="kt-menu__link-text"><?php echo ucfirst($menu_title); ?></span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a> 
                    </li>
                </ul>
        <?php
        $menus =$this->db->select('*')->from('admin_menusetup_tbl')->where_in('id',$menu_ids)->where('parent_menu',0)->group_by('module')->order_by('ordering', 'asc')->get()->result();    
        foreach ($menus as $menu_item)
        {    
         $modules=$menu_item;
        $child_menus = $this->db->select('*')->from('admin_menusetup_tbl')->where('module', $modules->module)->where('parent_menu =', $modules->id)->where_in('id',$menu_ids)->where('status', 1)->order_by('ordering', 'asc')->get()->result_array();
        ?>
                                        
                                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                                                <div class="kt-header__brand-nav">
                                                    <div class="dropdown">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                               <?php echo ucwords(str_replace('_', ' ',$modules->module)); ?>
                                                        </button>
                                                            <div class="dropdown-menu dropdown-menu-md">
                                                                <ul class="kt-nav kt-nav--bold kt-nav--md-space">
                                                                <?php 
                                                                foreach ($child_menus as $key=> $chiled_menu)
                                                                {
                                                                ?>
                                                                <li class="kt-nav__item">
                                                                       <a class="kt-nav__link active" href="<?php echo base_url();echo $chiled_menu['page_url'];?>">
                                                                                <span class="kt-nav__link-icon"><i class="<?php echo $chiled_menu['icon'];?>"></i></span>
                                                                                <span class="kt-nav__link-text"><?php echo ucwords(str_replace('_', ' ',$chiled_menu['menu_title'])); ?>
                                                                                </span>
                                                                        </a>
                                                                    </li>   
                                                                <?php }  ?>
                                                                </ul>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
        <?php } ?>   
        
            </div>
        </div>
    </div>
</div>




                
                    <!-- <li class="kt-menu__item <?php if($uri_segment_1 == 'coupon'): ?> kt-menu__item--here <?php endif;?>">
                        <a href="<?php echo base_url(); ?>super-admin/coupon/list" class="kt-menu__link">
                            <span class="kt-menu__link-text">Coupon</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li> -->

           
<!-- after else -->

<?php }  ?>
