<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         Change Billing Date
                    </h3>
                </div>
            </div>


            <div class="kt-portlet__body">
                <form action="<?php echo base_url(); ?>super_admin/Package_controller/wholesaler_update_subscription_date" method="POST">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">

                                        <input type="hidden" name="user_id" value="<?php echo $user_detail->id; ?>">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Subscription Id</label>
                                            <div class="col-lg-9 col-xl-6 col-form-label">
                                               <?php echo $subscription_id; ?>
                                             <input type="hidden" name="subscription_id" value="<?php echo $subscription_id; ?>">

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Start Billing Date</label>
                                            <div class="col-lg-9 col-xl-6 col-form-label">
                                               <?php echo $user_detail->billing_date; ?>

                                            </div>
                                        </div>
                                       
                                        <?php   
                                              if ($user_detail->package_duration=='1'){
                                             $date = date('d-m-Y', strtotime('+1 month', strtotime($user_detail->billing_date)));
                                             }else{
                                                $date = date('d-m-Y', strtotime('+1 year', strtotime($user_detail->billing_date)));
                                             }

                                        ?>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Next Billing Date</label>
                                            <div class="col-lg-9 col-xl-6 col-form-label">
                                               <?php echo $date; ?>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">New Billing Date</label>
                                            <div class="col-lg-3 col-xl-6">
                                                <input type="text" class="form-control datepicker-13" name="date" required="required">
                                            </div>
                                        </div>

                                      
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                                    <button class="btn btn-label-brand btn-bold form-post-btn">Save
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>

<script>
    $(function() {
        $( ".datepicker-13" ).datepicker();
    });
</script>


