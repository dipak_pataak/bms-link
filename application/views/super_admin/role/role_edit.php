<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
         <?php
            $message = $this->session->flashdata('message');
            if ($message != '') {
                echo $message;
            }
        ?>
        <div class="title row">
            <h5>Edit Role Permissions</h5>
        </div>
            <div class="kt-portlet__body">
               <form action="<?php echo base_url(); ?>super_admin/Role_controller/role_update" id="moduleFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Role Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" value="<?php echo $roleInfo->role_name; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Description</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <textarea class="form-control" name="role_description" id="role_description" placeholder="Role Description"><?php echo $roleInfo->description; ?></textarea>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-6">
                                               <input type="checkbox" class="form-check-input" id="select_deselect">
                                            <label class="form-check-label" for="select_deselect">Select / De-select</label>
                                            </div>
                                        </div>  

                                        <div class="form-group row mt-2">
                            <div class="col-12 float-right">
                                <input type="hidden" class="float-right" name="role_id" value="<?php echo $roleInfo->id ?>">
                                <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-primary btn-sm text-white float-right ml-1">Back</a>
                                <button type="submit" class="btn btn-info btn-sm module_btn float-right">Update</button>
                            </div>
                        </div>                                                         
</div>
                    </div>
                    </div>
                        </div>
                    </div>
              </div>
      <?php
                $m = 0;
                foreach ($modules as $module) {
                    $menu_item = $this->db->select('*')->from('admin_menusetup_tbl')->where('module', $module->module)->where('status', 1)
                                    ->get()->result();
                    ?>
                    <input type="hidden" name="module[]" value="<?php echo $module->module; ?>">
                    
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <h2><?php echo str_replace("_", " ", ucwords($module->module)); ?></h2>
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">Sl No</th>
                                <th class="" width="30%">Menu Title </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_create_all">Can Create</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_create_all" id="<?php echo $module->module; ?>_can_create_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<<?php echo $module->module; ?>_can_read_all">Can Read</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_read_all" id="<?php echo $module->module; ?>_can_read_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_edit_all">Can Edit</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_edit_all" id="<?php echo $module->module; ?>_can_edit_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_delete_all">Can Delete</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_delete_all" id="<?php echo $module->module; ?>_can_delete_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php
                            if (!empty($menu_item)) {

                                    $sl = 0;
                                    foreach ($menu_item as $menu) {
                                        $ck_data = $this->db->select('*')
                                                        ->where('menu_id', $menu->id)
                                                        ->where('role_id', $roleInfo->id)->get('admin_role_permission_tbl')->row();
                        


                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $sl + 1; ?></td>
                                            <td class="text-<?php echo ($menu->parent_menu ? 'right' : '') ?>"><?php echo str_replace("_", " ", ucwords($menu->menu_title)); ?></td>
                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="create[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_create == 1) ? "checked" : null) ?> id="create[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_create">
                                                    <label for="create[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="read[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_access == 1) ? "checked" : null) ?> id="read[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_read">
                                                    <label for="read[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td> 
                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="edit[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_edit == 1) ? "checked" : null) ?> id="edit[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_edit">
                                                    <label for="edit[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                            <td>
                                                <div class="checkbox checkbox-success text-center">
                                                    <input type="checkbox" name="delete[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" <?php echo ((@$ck_data->can_delete == 1) ? "checked" : NULL) ?> id="delete[<?php echo $m ?>]<?php echo $sl ?>"  class="sameChecked <?php echo $menu->module; ?>_can_delete">
                                                    <label for="delete[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                </div>
                                            </td>

                                    <input type="hidden" name="menu_id[<?php echo $m ?>][<?php echo $sl ?>][]" value="<?php echo $menu->id ?>">

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                <?php
                                $m++;
                            }
                            ?>
                            </tbody>
                    </table>
                    </div>
                    
                <?php }
                ?>

                <div class="form-group row">
                    <div class="col-md-2 pt-1">
                        <button type="submit" class="btn btn-info module_btn">Save & Next</button>
                    </div>
                </div>

             </div>
        </form>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->                      

<?php echo load_view('super_admin/layouts/footer'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#select_deselect', function () {

            $(".sameChecked").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_create_all', function () {
            var create_value = $(this).val();
            $("." + create_value + "_can_create").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_read_all', function () {
            var read_value = $(this).val();
            $("." + read_value + "_can_read").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_edit_all', function () {
            var edit_value = $(this).val();
            $("." + edit_value + "_can_edit").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_delete_all', function () {
            var delete_value = $(this).val();
            $("." + delete_value + "_can_delete").prop('checked', $(this).prop('checked'));
        });
    });
</script>
