<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title row">
            <h5>User Access Role</h5>
        </div>
          <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'User Access Role';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
         <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>

        </div>
      
            <div  id="results_menu">
                <div class="table-responsive p-3">
                    <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>User Name</th>
                        <th>Role Name</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    // echo "<pre>";                            print_r($user_access_role);
                    if (!empty($user_access_role)) {
                        $sl = 0;
                        foreach ($user_access_role as $key => $value) {
                            $sql = "SELECT a.role_id, a.user_id, b.role_name FROM admin_user_access_tbl a 
                            JOIN admin_role_tbl b ON b.id = a.role_id 
                        WHERE a.user_id = '$value->user_id'";
//                            echo $sql;
                            $query = $this->db->query($sql)->result();
//                            echo "<pre>";   print_r($query);
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo $value->first_name . " " . @$value->last_name; ?></td>
                                <td>
                                    <ul>
                                        <?php
                                        foreach ($query as $role) {
                                            echo "<li>" . $role->role_name . "</li>";
                                        }
                                        ?>
                                    </ul>
                                </td>
                                <?php
                                    $user_id = $this->session->userdata('user_id');
                                    $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                    $id=$user_role_info[0]->role_id;
                                    $user_data = $this->db->select('*')
                                                            ->from('admin_role_permission_tbl')
                                                            ->where('menu_id =',29)
                                                            ->where('role_id', $id)
                                                            ->get()->result();
                                ?> 

                                <td class="text-right">
                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                    <a href="<?php echo base_url(); ?>super_admin/role_controller/edit_user_access_role/<?php echo $value->role_acc_id; ?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"> </i></a>
                                    <?php } ?>
                                    <?php  if($user_data[0]->can_delete==1 or $this->session->userdata('user_id')==1){?>
                                    <a href="<?php echo base_url(); ?>super_admin/role_controller/delete_user_access_role/<?php echo $value->role_acc_id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($user_access_role)) { ?>
                    <tfoot>
                        <tr>
                            <th class="text-danger text-center" colspan="6">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
                </div>
            </div>
<?php echo load_view('super_admin/layouts/footer'); ?>
