<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                       <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                       Assign User Role Update
                    </h3>
                

                </div>
            </div>
      
                </div>
            </div>
        </div>
           <div class="kt-portlet__body">
               <?php echo form_open_multipart("super_admin/role_controller/assign_user_role_update/" . $edit_user_access_role->role_acc_id, array('class' => 'form-horizontal')) ?>
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">User Name</label>
                                            <div class="col-md-4">
                                               <select class="form-control" name="user_id" id="user_id">
                                                <option value="">-- select one --</option>
                                                <?php
                                                foreach ($user_list as $user) {
                                                    if ($edit_user_access_role->user_id == $user->id) {
                                                        echo '<option selected value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                                                    } else {
                                                        echo '<option value="' . $user->id . '">' . $user->first_name . " " . $user->last_name . " ->( " . $user->email . " ) " . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            </div>
                                        </div>
                                        <?php
                                            foreach ($assign_role as $role) {
                                                $role_id[] = $role->role_id;
                                            }
                                        ?>


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                            <div class="col-lg-9 col-xl-6">
                                              <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true">
                                                <?php foreach ($role_list as $val) { ?>
                                                    <option value="<?= $val->id ?>" <?php if (in_array($val->id, $role_id)) { echo 'selected'; }?>>
                                                        <?= ucwords($val->role_name); ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <?php foreach ($role_list as $val) { ?>
                        <!--                        <label class="radio-inline">
                                                    <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"
                                                    <?php
                                                    if (in_array($val->id, $role_id)) {
                                                        echo 'checked';
                                                    }
                                                    ?> >
                                                           <?php echo $val->role_name; ?>
                                                </label> -->
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                      <div class="col-lg-5 px-4">
                                    <div class="form-group text-right">
                                        <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                                        <button type="submit" class="btn btn-success w-md m-b-5">Update</button>
                                    </div>
                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <?php echo form_close() ?>'
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>