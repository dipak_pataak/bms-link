<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                       <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Assign User Role
                    </h3>
                    <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Assign User Role';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
             <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                </div>
            </div>
      
                </div>
            </div>
        </div>
           <div class="kt-portlet__body">
              <form method="post" action="<?php echo base_url(); ?>super_admin/role_controller/assign_user_role_save" class="form-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">User</label>
                                            <div class="col-md-4">
                                               <select name="user_id" class="form-control" id="user_id" onchange="userRole(this.value)" data-placeholder="Select Option" required>

                                                <option value=""></option>
                                                <?php
                                                foreach ($get_users as $user) {
                                                    echo '<option value="' . $user->id . '">' . $user->fullname . " ->( " . $user->email . " ) " . '</option>';
                                                }
                                                ?>
                                            </select>
                                            </div>
                                            <div class="col-md-4">
                                            <h4>Assigned Role</h4>                                                            
                                            <div id="existrole">
                                                <ul style="margin-left: 20px !important; margin-bottom: 15px !important;">

                                                </ul>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                            <div class="col-lg-9 col-xl-6">
                                              <select class="selectpicker form-control role_id" id="role_id" name="role_id[]" multiple data-live-search="true" required>
                                              <?php foreach ($role_list as $val) { ?>
                                              <option value="<?= $val->id ?>"><?= ucwords($val->role_name); ?></option>
                                               <?php } ?>
                                              </select>
                                              <?php foreach ($role_list as $val) { ?>
                                                <!--                                    <label class="radio-inline">
                                                                                        <input type="checkbox" id="role_id" name="role_id[]" value="<?php echo $val->id; ?>"> <?php echo $val->role_name; ?>
                                                                                    </label> -->
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                    <div class="col-lg-5 px-4">
                                        <div class="form-group text-right">
                                             <?php
                                            $user_id = $this->session->userdata('user_id');
                                            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                            $id=$user_role_info[0]->role_id;
                                            $user_data = $this->db->select('*')
                                                                    ->from('admin_role_permission_tbl')
                                                                    ->where('menu_id =',30)
                                                                    ->where('role_id', $id)
                                                                    ->get()->result();
                                            ?> 
                                            <?php  if($user_data[0]->can_create==1 or $this->session->userdata('user_id')==1){?>
                                            <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                                            <?php } ?>

                                        </div>
                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>
<!-- end content / right -->
<script type="text/javascript">
    function userRole(t) {

        $.ajax({
            url: "<?php echo base_url(); ?>super-admin-check-user-role",
            type: 'post',
            data: {user_id: t},
            success: function (r) {
                r = JSON.parse(r);
                $("#existrole ul").empty();
                $.each(r, function (ar, typeval) {
                    if (typeval.role_name == 'Not Found') {
                        $("#existrole ul").html("Not Found!");
                        $("#exitrole ul").css({'color': 'red'});
                    } else {
                        $("#existrole ul").append('<li>' + typeval.role_name + '</li>');
                    }
                });
            }
        });
    }
</script>