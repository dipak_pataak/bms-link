<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

           <!--  <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         SMS Configuration
                    </h3>
                </div>
            </div>
 -->
        <div class="title row">
            <h5>SMS Configuration</h5>
              <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $message = $this->session->flashdata('message');
        if ($message != '') {
            echo $message;
        }
        if ($error != '') {
            echo $error;
        }
        ?>
            <div class="kt-portlet__body">             
            <form action="<?php echo base_url(); ?>super_admin/menusetup_controller/menusetup_update/<?php echo $single_menu_edit->id; ?>" method="post" class="form-horizontal">
                <div class="panel">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Menu Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <input type="text" class="form-control" name="menu_name" id="menu_name" placeholder="English Name" value="<?php echo $single_menu_edit->menu_title; ?>" readonly>
                                            </div>
                                            <div class="col-sm-4 col-md-3">
                                            <input type="text" class="form-control" name="korean_name" id="korean_name" placeholder="My Language" value="<?php echo $single_menu_edit->korean_name; ?>">
                                        </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Menu URL</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="url" id="url" placeholder="URL" value="<?php echo $single_menu_edit->page_url; ?>" readonly>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Module</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <input class="form-control" name="module" id="module" placeholder="Enter Module" value="<?php echo $single_menu_edit->module; ?>" readonly>
                                        </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Order</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <select  class="form-control" name="order" id="order" data-placeholder="-- select one --">
                                                 <option value=""></option>
                                    <?php
                                    for ($i = 1; $i < 51; $i++) {
                                        if ($single_menu_edit->ordering == $i) {
                                            echo "<option selected value='$i'>$i</option>";
                                        } else {
                                            echo "<option value='$i'>$i</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Menu Type</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <select  class="form-control" name="menu_type" id="menu_type" onchange="menu_type_wise_parent_menu(this.value);" data-placeholder="-- select one --" tabindex="7">
                                    <option value=""></option>
                                    <option value="1" <?php
                                    if ($single_menu_edit->menu_type == 1) {
                                        echo 'selected';
                                    }
                                    ?>>Left Menu</option>
                                    <option value="2" <?php
                                    if ($single_menu_edit->menu_type == 2) {
                                        echo 'selected';
                                    }
                                    ?>>System Menu</option>
                                    <option value="3" <?php
                                    if ($single_menu_edit->menu_type == 3) {
                                        echo 'selected';
                                    }
                                    ?>>Top Menu</option>
                                </select>
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Parent Menu</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <select  class="form-control" name="parent_menu" id="parent_menu" data-placeholder="-- select one --">
                                    <option value="0">None</option>
                                    <?php
                                    foreach ($parent_menu as $parent) {
                                        if ($single_menu_edit->parent_menu === $parent->id) {
                                            echo "<option selected value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
                                        } else {
                                            echo "<option value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
                                        }
                                    }
                                    ?>
                                </select>
                                            </div>
                                        </div>    
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Icon</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="icon" id="url" placeholder="Icon Class" value="<?php echo $single_menu_edit->icon; ?>">
                                        </div>
                                    </div>
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                                   <input type="submit" id="" class="btn btn-success btn-large float-right ml-1" name="add-user" value="Update" />
                                                   <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white float-right">Back</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>

        </div>

</div>
<?php echo load_view('super_admin/layouts/footer'); ?>
<script type="text/javascript">

//    ========== its for menu_type_wise_parent_menu ===========
    function menu_type_wise_parent_menu(t) {
        $.ajax({
            url: "<?php echo base_url('b_level/Menusetup_controller/menu_type_wise_parent_menu'); ?>",
            type: 'post',
            data: {type_id: t},
            success: function (r) {
                r = JSON.parse(r);
//                    swal(r);
                $("#parent_menu").empty();
                $("#parent_menu").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $('#parent_menu').append($('<option>').text(typeval.menu_title).attr('value', typeval.id));
                });
            }
        });
    }
</script>

