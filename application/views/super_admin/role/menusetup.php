<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title row">
            <h5>Menu setup</h5>
        </div>
          <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Menu setup';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
             <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
         </div>
         <?php
        $user_id = $this->session->userdata('user_id');
        $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
        $id=$user_role_info[0]->role_id;
        $user_data = $this->db->select('*')
                                ->from('admin_role_permission_tbl')
                                ->where('menu_id =',26)
                                ->where('role_id', $id)
                                ->get()->result();
             
        ?>
      
        <?php  if($user_data[0]->can_create==1 or $this->session->userdata('user_id')==1){?>
        <div class="row">
            <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2"  data-toggle="modal" data-target="#groupSms" style="font-size: 12px; ">Add Menu</a>
        </div>
        <?php }?>
        <div class="col-sm-12 text-right">
            <div class="form-group row search_div m-0 float-right mb-3">
                <label for="keyword" class="col-form-label text-right"></label>
                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="menukeyup_search()" placeholder="Search..." tabindex="">
                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" onClick="ExportMethod('<?php echo base_url(); ?>super-admin/menu-export-csv')" class="dropdown-item">Export to CSV</a></li>
                        <li><a href="javascript:void(0)"  data-toggle="modal" data-target="#importMenu" class="dropdown-item">Import from CSV</a></li>
                    </ul>
            </div>          
        </div>
<!-- Modal -->
        <div class="modal fade" id="groupSms" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Employee</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <?= form_open('super_admin/menusetup_controller/menusetup_save', array('class' => 'p-3', 'name' => 'userFrm')); ?>
                        <div class="col-md-12 px-4"> 

                            <div class="form-group row">
                                <label for="customer_id" class="col-md-3 control-label text-right">Menu Name<sup class="text-danger">*</sup></label>
                                 <div class="col-md-6">
                                      <input type="text" class="form-control" name="menu_name" id="menu_name" placeholder="English Name" required tabindex="1" required>
                                   
                                </div>
                                <div class="col-sm-4 col-md-3">
                                <input type="text" class="form-control" name="korean_name" id="korean_name" placeholder="My Language"  tabindex="2" required>
                            </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Menu URL<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                   <input type="text" class="form-control" name="url" id="url" placeholder="URL" tabindex="3" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Module<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                     <input type="text" class="form-control" name="module" id="module" placeholder="Enter Module" tabindex="4" required>
                                <span id="error"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Order<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                   <select  class="form-control" name="order" id="order" data-placeholder="-- select one --" tabindex="5">
                                    <option value=""></option>
                                    <?php
                                    for ($i = 1; $i < 51; $i++) {
                                        echo "<option value='$i'>$i</option>";
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Menu Type<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                   <select  class="form-control" name="menu_type" id="menu_type" onchange="menu_type_wise_parent_menu(this.value);"  data-placeholder="-- select one --" tabindex="6" required>
                                    <option value=""></option>
                                    <option value="1">Left Menu</option>
                                    <option value="2">System Menu</option>
                                    <option value="3">Top Menu</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Parent Menu<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                   <select  class="form-control" name="parent_menu" id="parent_menu" data-placeholder="-- select firstly menu type --" tabindex="7">
                                    <option value=""></option>
                                    <?php
                                    foreach ($parent_menu as $parent) {
                                       echo "<option value='$parent->id'>" . ucwords(str_replace('_', ' ', $parent->menu_title)) . "</option>'";
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Icon<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon Class" tabindex="8" required>
                                </div>
                            </div>
                        <div class="col-lg-5 px-4">
                       
                            <div class="form-group text-right">
                                 <input type="submit" id="" class="btn btn-primary btn-large float-right" name="add-user" value="Save" tabindex="8" />
                            </div>
                   
                        </div>

                        <?= form_close(); ?>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>          
                      <div  id="results_menu">
                        <div class="table-responsive p-3">
                            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="15%">Menu Name</th>
                        <th width="15%">My Language</th>
                        <th width="15%">URL</th>
                        <th width="15%">Module</th>
                        <th width="15%">Parent Menu</th>
                        <th width="18%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($menusetuplist)) {
                        $sl = 0 + $pagenum;
                        foreach ($menusetuplist as $key => $value) {
                            $parent_menu = $this->db->select('*')->where('created_by', $this->session->userdata('user_id'))->where('id', $value->parent_menu)->get('admin_menusetup_tbl')->row();
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $sl; ?></td>
                                <td><?php echo str_replace("_", " ", ucfirst($value->menu_title)); ?></td>
                                <td><?php echo str_replace("_", " ", ucfirst($value->korean_name)); ?></td>
                                <td><?php echo $value->page_url; ?></td>
                                <td><?php echo $value->module ?></td>
                                <td><?php echo str_replace("_", " ", ucfirst(@$parent_menu->menu_title)); ?></td>
                                <td class="text-center">
                                    <?php
                                    $status = $value->status;
                                    if ($status == 1) {
                                        ?>
                                    <a href="<?php echo base_url(); ?>super_admin/menusetup_controller/menusetup_inactive/<?php echo $value->id; ?>" data-toggle='tooltip' data-placement='top' data-original-title='Make Inactive' onclick="return confirm('Are you sure inactive it ?')" title="Inactive" class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i></a>
                                        <?php
                                    }
                                    if ($status == 0) {
                                        ?>
                                    <a href="<?php echo base_url(); ?>super_admin/menusetup_controller/menusetup_active/<?php echo $value->id; ?>" data-toggle='tooltip' data-placement='top' data-original-title='Make Active' onclick="return confirm('Are you sure active it ?')" title="Active" class="btn btn-sm btn-info"><i class="fa fa-check-circle"></i></a>
                                    <?php } ?>
                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                    <a href = "<?php echo base_url(); ?>super_admin/menusetup_controller/menusetup_edit/<?php echo $value->id; ?>" title = "" class = "btn btn-info btn-sm simple-icon-note" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class = "fa fa-edit"></i></a>
                                    <?php }?>
                                   <!--  <?php  if($user_data[0]->can_delete==1 or $this->session->userdata('user_id')==1){?>
                                    <a href = "<?php echo base_url(); ?>super_admin/Menusetup_controller/menusetup_delete/<?php echo $value->id; ?>" title = "" onclick = "return confirm('Do you want to delete it?');" class = "btn btn-danger btn-sm simple-icon-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class = "fa fa-trash"></i></a>
                                    <?php } ?> -->
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
                <?php if (empty($menusetuplist)) { ?>
                    <tfoot>
                        <tr>
                            <th colspan="7" class="text-danger text-center">Record not found!</th>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table> 
              <?php echo $links; ?>
            </div>
        </div>
        </div>

        <div class="modal fade" id="menu_exp" role="dialog">            
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Export Menu</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form id="expUrl" action="#" method="post">

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Start from<sup class="text-danger">*</sup></label>
                                <div class="col-xs-6">
                                    <input type="number" min="1" name="ofset" id="ofset" class="form-control" required="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label  class="col-xs-2 control-label">Limit<sup class="text-danger">*</sup></label>
                                <div class="col-xs-6" style="margin-left: 7%;">
                                    <input type="number" min="1" name="limit" id="limit" class="form-control" required="">
                                </div>
                            </div>

                            <div class="form-group row">
        
                                <div class="col-xs-6">
                                <center><button class="btn-sm btn-success" id="closeModal"> Export</button></center>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>  
          <!-- Modal -->
    <div class="modal fade" id="importMenu" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Menu Bulk Update</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <span class="text-primary">The first line in exported csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                    <?php echo form_open_multipart('super-admin/import-menu-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                    <div class="form-group row">
                        <label for="upload_csv_file" class="col-xs-3 control-label">File<sup class="text-danger">*</sup></label>
                        <div class="col-xs-6">
                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group  text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                    </div>

                    </form>
                </div>
               <!--  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
    <!-- end:: Content -->          


<?php echo load_view('super_admin/layouts/footer'); ?>

<script type="text/javascript">
    function ExportMethod(url){
        $("#expUrl").attr("action", url);
        $("#menu_exp").modal('show');
    }

    function menukeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>super-admin-menu-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#results_menu").html(r);
            }
        });
    }


//    ========== its for menu_type_wise_parent_menu ===========

    function menu_type_wise_parent_menu(t) {
       alert(t);
        $.ajax({
            url: "<?php echo base_url('super_admin/Menusetup_controller/menu_type_wise_parent_menu'); ?>",
            type: 'post',
            data: {type_id: t},
            success: function (r) {
                // alert(r);
                r = JSON.parse(r);
//                 
                $("#parent_menu").empty();
                $("#parent_menu").html("<option value=''>-- select one -- </option>");
                $.each(r, function (ar, typeval) {
                    $('#parent_menu').append($('<option>').text(typeval.menu_title).attr('value', typeval.id));
                });
            }
        });
    }
</script>
