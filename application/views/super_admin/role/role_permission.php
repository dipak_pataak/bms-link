<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
          <?php
        $message = $this->session->flashdata('message');
        if ($message)
            echo $message;
        ?>
        <div class="title row">
            <h5>Role Permissions</h5>
           <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Role Permissions';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
            <div class="kt-portlet__body">
                <form action="<?php echo base_url('super_admin/role_controller/role_save'); ?>" id="moduleFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Role Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Description</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <textarea class="form-control" name="role_description" id="role_description" placeholder="Role Description"></textarea>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-6">
                                               <input type="checkbox" class="form-check-input" id="select_deselect">
                                            <label class="form-check-label" for="select_deselect">Select / De-select</label>
                                        </div>
                                        </div>                                                                      

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      <?php
                $m = 0;
                foreach ($modules as $module) {
                    $menu_item = $this->db->select('*')->from('admin_menusetup_tbl')->where('module', $module->module)->where('status', 1)
                                    ->get()->result();
                    ?>
                    <input type="hidden" name="module[]" value="<?php echo $module->module; ?>">
                    
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <h2><?php echo str_replace("_", " ", ucwords($module->module)); ?></h2>
                        <thead>
                            <tr>
                                <th class="text-center" width="10%">Sl No</th>
                                <th class="" width="30%">Menu Title </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_create_all">Can Create</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_create_all" id="<?php echo $module->module; ?>_can_create_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<<?php echo $module->module; ?>_can_read_all">Can Read</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_read_all" id="<?php echo $module->module; ?>_can_read_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_edit_all">Can Edit</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_edit_all" id="<?php echo $module->module; ?>_can_edit_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                                <th class="text-center" width="15%">
                                    <div class="form-check p-0">
                                                            <label class="form-check-label" for="<?php echo $module->module; ?>_can_delete_all">Can Delete</label>
                                                            <span class="right_checkbox">All <input type="checkbox" class="form-check-input can_delete_all" id="<?php echo $module->module; ?>_can_delete_all" value="<?php echo $module->module; ?>"></span>
                                                        </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($menu_item)) {
                                $sl = 0;
                                foreach ($menu_item as $menu) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $sl + 1; ?></td>
                                        <td class="text-<?php echo ($menu->parent_menu ? 'right' : '') ?>"><?php echo str_replace("_", " ", ucwords($menu->menu_title)); ?></td>
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox"  name="create[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="create[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_create">
                                                <label for="create[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="read[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="read[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_read">
                                                <label for="read[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td> 
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="edit[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="edit[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_edit">
                                                <label for="edit[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td> 
                                        <td>
                                            <div class="checkbox checkbox-success text-center">
                                                <input type="checkbox" name="delete[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="delete[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo $menu->module; ?>_can_delete">
                                                <label for="delete[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                            </div>
                                        </td>
                                <input type="hidden" name="menu_id[<?php echo $m ?>][<?php echo $sl ?>][]" value="<?php echo $menu->id ?>">

                                </tr>
                                <?php
                                $sl++;
                            }
                            ?>
                            <?php
                            $m++;
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                    
                <?php }
                ?>
                <?php
                $user_id = $this->session->userdata('user_id');
                $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                $id=$user_role_info[0]->role_id;
                $user_data = $this->db->select('*')
                                        ->from('admin_role_permission_tbl')
                                        ->where('menu_id =',28)
                                        ->where('role_id', $id)
                                        ->get()->result();
                ?> 

                <div class="form-group row">
                    <?php  if($user_data[0]->can_create==1 or $this->session->userdata('user_id')==1){?>
                    <div class="col-md-2 pt-1">
                        <button type="submit" class="btn btn-info module_btn">Save & Next</button>
                    </div>
                <?php } ?>
                </div>

             </div>
        </form>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->                      

<?php echo load_view('super_admin/layouts/footer'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#select_deselect', function () {

            $(".sameChecked").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_create_all', function () {
            var create_value = $(this).val();
            $("." + create_value + "_can_create").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_read_all', function () {
            var read_value = $(this).val();
            $("." + read_value + "_can_read").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_edit_all', function () {
            var edit_value = $(this).val();
            $("." + edit_value + "_can_edit").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_delete_all', function () {
            var delete_value = $(this).val();
            $("." + delete_value + "_can_delete").prop('checked', $(this).prop('checked'));
        });
    });
</script>
