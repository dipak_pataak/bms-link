<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
  
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
        <!-- box / title -->
        <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
        if ($error != '') {
            echo $error;
        }
        if ($success != '') {
            echo $success;
        }
        ?>

        <div class="title row">
            <h5>Role List</h5>
        </div>
        <?php
            $page_url = $this->uri->segment(1);
            $get_favorite = get_b_favorite_detail($page_url);
            $class = "notfavorite_icon";
            $fav_title = 'Role List';
            $onclick = 'add_favorite()';
            if (!empty($get_favorite)) {
                $class = "favorites_icon";
                $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
            }
        ?>
           <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>

        </div>

        <?php
            $user_id = $this->session->userdata('user_id');
            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
            $id=$user_role_info[0]->role_id;
            $user_data = $this->db->select('*')
                                    ->from('admin_role_permission_tbl')
                                    ->where('menu_id =',27)
                                    ->where('role_id', $id)
                                    ->get()->result();
        ?>
        <?php  if($user_data[0]->can_create==1 or $this->session->userdata('user_id')==1){?>
        <div class="row">
            <a href="<?php echo base_url(); ?>super_admin/role_controller/role_permission" class="btn btn-success ml-3 mb-1">New Role</a>
        </div>
        <?php } ?>
      
            <div  id="results_menu">
                <div class="table-responsive p-3">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Role Name</th>
                                <th>Description</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    <tbody>
                                <?php
                                if (!empty($role_list)) {
                                    $sl = 0;
                                    foreach ($role_list as $key => $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->role_name; ?></td>
                                            <td><?php echo $value->description; ?></td>
                                            <td class="text-right">
                                                <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                                <a href="<?php echo base_url(); ?>super_admin/role_controller/role_edit/<?php echo $value->id; ?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                                <?php } ?>                                                <?php  if($user_data[0]->can_delete==1 or $this->session->userdata('user_id')==1){?>
                                                <a href="<?php echo base_url(); ?>super_admin/role_controller/role_delete/<?php echo $value->id; ?>" title="" onclick="return confirm('Do you want to delete it?');" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if(empty($role_list)){ ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-danger text-center" colspan="6">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                    </table> 
                            <?php echo $links; ?>
                </div>
            </div>
<?php echo load_view('super_admin/layouts/footer'); ?>
