<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

           <!--  <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         SMS Configuration
                    </h3>
                </div>
            </div>
 -->
        <div class="title row">
            <h5>Mail Configuration</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'SMS Configuration';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
            <div class="kt-portlet__body">
                <form action="<?php echo base_url(); ?>super-admin/update-mail-configure" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Protocol</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <input class="form-control" name="protocol" id="protocol" type="text" value="<?php echo $get_mail_config[0]->protocol; ?>" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <p> smtp </p>
                                            </div>
                                         </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">SMTP Host</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="smtp_host" id="smtp_host" type="text" value="<?php echo $get_mail_config[0]->smtp_host; ?>" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <p> OR /usr/sbin/sendmail </p>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">SMTP Port</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="smtp_port" id="smtp_port" type="text" value="<?php echo $get_mail_config[0]->smtp_port; ?>" required>
                                        </div>
                                        <div class="col-sm-3">
                                            <p> 465 </p>
                                        </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Sender Email</label>
                                            <div class="col-lg-9 col-xl-6">
                                                  <input class="form-control" name="smtp_user" id="smtp_user" type="email" value="<?php echo $get_mail_config[0]->smtp_user; ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="smtp_pass" id="smtp_pass" type="password" value="<?php echo $get_mail_config[0]->smtp_pass; ?>" required>
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Mail Type</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <select class="form-control" name="mailtype" id="mailtype" required>
                                                    <option value="">Select One</option>
                                                    <option value="html" <?php if($get_mail_config[0]->mailtype == 'html'){ echo 'selected'; } ?>>Html</option>
                                                    <option value="text" <?php if($get_mail_config[0]->mailtype == 'text'){ echo 'selected'; } ?>>Text</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                            <p> html </p>
                                            </div>
                                        </div>  

                                          <?php
                                            $user_id = $this->session->userdata('user_id');
                                            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                            $id=$user_role_info[0]->role_id;
                                            $user_data = $this->db->select('*')
                                                                    ->from('admin_role_permission_tbl')
                                                                    ->where('menu_id =',21)
                                                                    ->where('role_id', $id)
                                                                    ->get()->result();
                                           ?>                                   
                                       <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-2 col-md-2 col-lg-2 col-form-label"></label>
                                        <div class="col-sm-6 text-right">
                                            <?php if ($user_data[0]->can_edit==1 or $user_id==1) {?>
                                            <?php if($get_mail_config[0]->is_verified != 1) { ?>
                                            <a href="javascript:void(0)" id="verify_configuration_btn" class="btn btn-success">Verify</a>
                                            <?php } ?>

                                            <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                                           <?php } ?>
                                        </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
           </div>                       
    </div>
</div>
<div id="verify_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send test email</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array('id' => 'verify_configuration', 'name' => 'verify_configuration', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>
                <div class="col-lg-12 px-4">
                    <div class="form-group">
                        <label for="email_id" class="mb-2">EmailId</label>
                        <input class="form-control" type="text" name="email_id" id="email_id" required>
                         <span id="error"></span>
                    </div>
                </div>
                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Send mail</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end:: Content -->                        </div>
</div>
<?php echo load_view('super_admin/layouts/footer'); ?>

<script>
$('#verify_configuration_btn').on('click', function () {
    $("#verify_configuration").attr("action", '<?php echo base_url(); ?>super_admin/Setting_controller/verify');
    $('#verify_modal').modal('show');
});
</script>
