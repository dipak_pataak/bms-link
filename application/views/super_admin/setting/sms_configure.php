<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

           <!--  <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         SMS Configuration
                    </h3>
                </div>
            </div>
 -->
        <div class="title row">
            <h5>SMS Configuration</h5>
            <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'SMS Configuration';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
            <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
            <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
            <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
            <div class="kt-portlet__body">
                <form action="<?php base_url(); ?>/super-admin/sms-config-save" method="post">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Provider Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Username</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2" required>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <input type="password" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" required>
                                        </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Phone</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <input type="number" maxlength="10"  name="phone" class="form-control phone-format" id="phone" placeholder="+12062024567 This is the valid format" tabindex="4" required >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Sender name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" required>
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Test SMS Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="number" name="test_sms_number" class="form-control phone-format" id="test_sms_number" placeholder="+12062024567 This is the valid format" tabindex="5" required>
                                            </div>
                                        </div>                                     
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                
                                                <?php
                                                    $user_id = $this->session->userdata('user_id');
                                                    $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                                    $id=$user_role_info[0]->role_id;
                                                    $user_data = $this->db->select('*')
                                                                            ->from('admin_role_permission_tbl')
                                                                            ->where('menu_id =',22)
                                                                            ->where('role_id', $id)
                                                                            ->get()->result();
                                                         
                                                    ?>
                                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1)
                                                    {?>   
                                                    <button type="submit" class="btn btn-success w-md m-b-5" tabindex="6">Save</button>
                                                    <button type="reset" class="btn btn-primary w-md m-b-5" tabindex="7">Reset</button>
                                                <?php }?>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>

        </div>
        <div class="title">
                            <h3>List Of SMS Configuration</h3>
                        </div>
                        <div class="table-responsive p-3">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#.</th>
                                        <th width="10%">Name</th>
                                        <th width="15%">User Name</th>
                                        <th width="15%">Password</th>
                                        <th width="10%">Phone</th>
                                        <th width="10%">Sender</th>
                                        <th width="4%">Status</th>
                                        <th width="4%">Verified</th>
                                        <th width="30%" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sl = 0; // + $pagenum;
                                    foreach ($get_sms_conf as $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->provider_name; ?></td>
                                            <td><?php echo $value->user; ?></td>
                                            <td><?php echo $value->password; ?></td>
                                            <td><?php echo $value->phone; ?></td>
                                            <td><?php echo $value->authentication; ?></td>

                                            <td>
                                                <?php
                                                    if ($value->default_status == 1) {
                                                        echo 'Active';
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?></td>
                                            <td>
                                                <?php
                                                    if ($value->is_verify == 1) {
                                                        echo 'Yes';
                                                    } else {
                                                        echo "No";
                                                    }
                                                    ?></td>

                                                   <!--  $route['sms-config-edit/(:any)'] = 'b_level/Setting_controller/sms_config_edit/$1'; -->

                                            <td class="text-center">
                                                <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                                <?php if ($value->is_verify == 0) { ?>
                                            <a href="<?php echo base_url(); ?>super-admin/sms-config-verified/<?php echo $value->gateway_id; ?>" class="btn btn-info default btn-sm" title="Verified Configuration" data-toggle="tooltip" data-placement="top" data-original-title="Verified Configuration"><i class="fa fa-check"></i></a>
                                                <?php } ?>
                                                
                                            <a href="<?php echo base_url();?>super-admin/sms-config-edit/<?php echo $value->gateway_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            <?php } ?>
                                            <?php  if($user_data[0]->can_delete==1 or $this->session->userdata('user_id')==1){?>    
                                               <a href="<?php echo base_url(); ?>super-admin/sms-config-delete/<?php echo $value->gateway_id; ?>" class="btn btn-danger danger btn-sm" onclick="return confirm('Do you want to delete it?')" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                                <?php if (empty($get_sms_config)) { ?>
                                    <tfoot>
                                        <th colspan="9" class="text-center">
                                            No result found!
                                        </th>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>
<?php echo load_view('super_admin/layouts/footer'); ?>
<script>
$("#phone").keydown(function(event) {
  k = event.which;
  if ((k >= 96 && k <= 105) || k == 8) {
    if ($(this).val().length == 10) {
      if (k == 8) {
        return true;
      } else {
        event.preventDefault();
        return false;

      }
    }
  } else {
    event.preventDefault();
    return false;
  }

});
</script>
<script>
$("#test_sms_number").keydown(function(event) {
  k = event.which;
  if ((k >= 96 && k <= 105) || k == 8) {
    if ($(this).val().length == 10) {
      if (k == 8) {
        return true;
      } else {
        event.preventDefault();
        return false;

      }
    }
  } else {
    event.preventDefault();
    return false;
  }

});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
