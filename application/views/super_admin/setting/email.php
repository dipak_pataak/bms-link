<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         Custom EMAIL
                    </h3>
                      <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Custom SMS';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
                </div>
            </div>
              <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>


        <?php
            $user_id = $this->session->userdata('user_id');
            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
            $id=$user_role_info[0]->role_id;
            $user_data = $this->db->select('*')->from('admin_role_permission_tbl')->where('menu_id =',23)->where('role_id', $id)->get()->result();?>
            
          <div class="kt-portlet__body">
               <form action="<?php echo base_url('super-admin/email-send'); ?>" method="post" name="customerFrm" class="p-3" enctype="multipart/form-data">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">

                                <div class="form-group row">
                                <label class="mb-3 control-label text-right">User Type<sup class="text-danger">*</sup></label>
                                 <div class="col-lg-9 col-xl-6">
                                    <select class="selectpicker form-control select-all"onchange="get_selected(this.value);">
                                        <option>Select one</option>
                                        <option value="b" class="select-all">Wholesaler</option>
                                        <option value="c" class="select-all">Retailer</option>
                                    </select>
                                   
                                </div>
                            </div>


                                    <div id="b_user_list" style="display: none;">
                                        <div class="form-group row"  >
                                             <label class="mb-3 control-label text-right">Receiver Email</label>
                                            <div class="col-lg-9 col-xl-6">
                                            <select class="selectpicker form-control select-all" id="customer_email" name="customer_email[]" multiple data-live-search="true" data-selected-text-format="count>2">
                                           
                                           <?php foreach ($customer_email_b as $val){ ?>

                                           <option value="<?= $val->email ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option> 
                                            <?php } ?>
                                            </select>
                                            </div>
                                        </div>

                                    <div id="c_user_list" style="display: none;">
                                         <div class="form-group row">
                                            <label class="mb-3">Receiver Email</label>
                                            <div class="col-lg-9 col-xl-6">
                                            <select class="selectpicker form-control select-all" id="customer_email" name="customer_email[]" multiple data-live-search="true" data-selected-text-format="count>2">                                           
                                           <?php foreach ($customer_email_c as $val){ ?>

                                           <option value="<?= $val->email ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option> 
                                            <?php } ?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                            <div class="form-group row">
                                            <label for="custom_email" class="mb-3">Custom Email</label>
                                             <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="custom_email" class="form-control" id="custom_email" placeholder="test@gmail.com, test1@gmail.com" tabindex="1" >
                                            </div>
                                </div>
                                        </div>

                                        <div class="form-group row">
                                              <label for="message" class="mb-3">Message<sup class="text-danger">*</sup></label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <textarea name="message"  id="message"class="form-control tinymce" placeholder="Message" rows="7"  tabindex="2"></textarea>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                      <div class="col-lg-5 px-4">
                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                    <div class="form-group text-right">
                                   <button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
                                   <button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="4">Reset</button>
                                   </div>
                                    <?php } ?>
                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>

<script>
function get_selected(val) {
    if (val=="b"){
    $("#b_user_list").show();
    $("#c_user_list").hide();
   }else{
    $("#c_user_list").show();
    $("#b_user_list").hide();
   }
}
</script>
