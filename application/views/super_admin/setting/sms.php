<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         Custom SMS
                    </h3>
                      <?php
                $page_url = $this->uri->segment(1);
                $get_favorite = get_b_favorite_detail($page_url);
                $class = "notfavorite_icon";
                $fav_title = 'Custom SMS';
                $onclick = 'add_favorite()';
                if (!empty($get_favorite)) {
                    $class = "favorites_icon";
                    $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                }
            ?>
                </div>
            </div>
        <div class="row">
            <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2"  data-toggle="modal" data-target="#bulkSms" style="font-size: 12px; ">Bulk SMS</a>
            <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2"  data-toggle="modal" data-target="#groupSms" style="font-size: 12px; ">Multi User SMS</a>
        </div>
       <!-- Modal -->
        <?php
            $user_id = $this->session->userdata('user_id');
            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
            $id=$user_role_info[0]->role_id;
            $user_data = $this->db->select('*')
                                    ->from('admin_role_permission_tbl')
                                    ->where('menu_id =',24)
                                    ->where('role_id', $id)
                                    ->get()->result();
        ?>
                                                   
        <div class="modal fade" id="bulkSms" role="dialog"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!--<h4 class="modal-title">Modal Header</h4>-->
                    </div>
                    <div class="modal-body">
                        <a href="<?php echo base_url('assets/b_level/csv/sms_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        <span class="text-primary">The first line in downloaded csv file should remain as it is.<br> Please do not change the order of columns.</span><br><br>
                        <?php echo form_open_multipart('super-admin/sms-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group row">
                            <label for="upload_csv_file" class="col-md-3 control-label">File<sup class="text-danger">*</sup></label>
                            <div class="col-xs-6">
                                <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                            </div>
                        </div>
                        <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>   
                        <div class="form-group  text-right">
                            <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                        </div>
                        <?php }?>

                        </form>
                    </div>
                </div>
            </div>
        </div>

          <!-- Modal -->
        <div class="modal fade" id="groupSms" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Multi User SMS</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open('super-admin/group-sms-send', array('class' => 'form-row px-3', 'id' => '')); ?>
                        <div class="col-md-12 px-4"> 

                            <div class="form-group row">
                                <label for="customer_id" class="col-md-3 control-label text-right">User Type<sup class="text-danger">*</sup></label>
                                 <div class="col-md-6">
                                    <select class="selectpicker form-control select-all"onchange="get_selected(this.value);">
                                        <option>Select one</option>
                                        <option value="b" class="select-all">Wholesaler</option>
                                        <option value="c" class="select-all">Retailer</option>
                                    </select>
                                   
                                </div>
                            </div>

                            <div class="form-group row" id="user_typelist">
                            </div>
                             
                            <div class="form-group row">
                                <label for="message" class="col-md-3 col-form-label text-right">Message<sup class="text-danger">*</sup></label>
                                <div class="col-md-6">
                                    <textarea name="message"  id="message"class="form-control" placeholder="Message" rows="7"  tabindex="2" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 px-4">
                            <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                            </div>
                            <?php } ?>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>


            <div class="kt-portlet__body">
                <form action="<?php echo base_url(); ?>super-admin/sms-send" method="post">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Receiver Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <input type="text" name="receiver_id" class="form-control phone" id="receiver_id" placeholder="+1 (XXX)-XXX-XXXX" tabindex="1" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Message</label>
                                            <div class="col-lg-9 col-xl-6">
                                               <textarea name="message"  id="message"class="form-control tinymce" placeholder="Message" rows="7"  tabindex="2"></textarea>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                      <div class="col-lg-5 px-4">
                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                    <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
                                    <button type="reset" class="btn btn-primary w-md m-b-5"  tabindex="4">Reset</button>
                                    <?php } ?>
                                   </div>
                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>
<script>
function get_selected(val) {
   
    $.ajax({
        url: '<?php echo base_url();?>super_admin/Setting_controller/User_Type',
        type: 'POST',
         data: {'val':val},
        success: function(data){  
        // alert(data);      
        $('#user_typelist').html(data);
         $('#customer_phone').selectpicker('refresh');
        } 

    }); 
}
</script>
