<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head --> 
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
        <div class="title row">
            <h5>SMS Configuration</h5>
        </div>
         <div class="" style="margin: 10px;">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
            <div class="kt-portlet__body">
                <form action="<?php echo base_url('super-admin/sms-config-update/' . $sms_config_edit[0]['gateway_id']); ?>" method="post" class="col-sm-7 col-md-7 col-lg-5">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Provider Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                 <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" value="<?php echo $sms_config_edit[0]['provider_name']; ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Username</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="user_name" class="form-control" id="user_name"  tabindex="2"  value="<?php echo $sms_config_edit[0]['user']; ?>" required>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" value="<?php echo $sms_config_edit[0]['password']; ?>" required>
                                        </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Phone</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="phone" class="form-control phone-format" id="phone" placeholder="Phone" tabindex="4" value="<?php echo $sms_config_edit[0]['phone']; ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Sender name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" value="<?php echo $sms_config_edit[0]['authentication']; ?>" required>
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Test SMS Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" name="test_sms_number" class="form-control" id="test_sms_number" placeholder="+12062024567 This is the valid format" tabindex="5" value="<?php echo $sms_config_edit[0]['test_sms_number']; ?>" required>
                                            </div>
                                        </div>  
                                        <div class="form-group row">
                 
                    <label for="is_active" class="col-xs-3 col-form-label">Is Active<sup class="text-danger">*</sup></label>
                    <div class="col-xs-9">
                        <select name="is_active" class="is_active form-control select" id="is_active" data-placeholder='-- select one --'>
                            <option value=""></option>
                            <option value="1" <?php if($sms_config_edit[0]['default_status'] == 1){ echo 'selected'; }?>>Active</option>
                            <option value="0" <?php if($sms_config_edit[0]['default_status'] == 0){ echo 'selected'; }?>>Inactive</option>
                        </select>
                    </div>
                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                   
                                                      <button type="submit" class="btn btn-success w-md m-b-5"  tabindex="6">Update</button>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    //---------phone_format---------
    $(document).ready(function(){
        load_country_dropdown('phone',country);
    })
</script>


<?php echo load_view('super_admin/layouts/footer'); ?>

<script>
$("#phone").keydown(function(event) {
  k = event.which;
  if ((k >= 96 && k <= 105) || k == 8) {
    if ($(this).val().length == 10) {
      if (k == 8) {
        return true;
      } else {
        event.preventDefault();
        return false;

      }
    }
  } else {
    event.preventDefault();
    return false;
  }

});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
