

<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Orders list
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="search_table">
                        <thead>
                        <tr>
                            <th>Order/Quote No</th>
                            <th>Client name</th>
                            <th>Side mark</th>
                            <th>Order date</th>
                            <th>Order due</th>
                            <th>Order stage</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>

<script>
    var order_stage_arr = [];
</script>
<?php foreach (get_order_stage_list() AS $key => $value) : ?>
    <script>
        var columnObj = {};
        columnObj.order_stage = "<?= $value['order_stage']?>";
        columnObj.stage = "<?= $value['stage']?>";

        order_stage_arr.push(columnObj);
    </script>
<?php endforeach;?>

<script>
    $(document).ready(function () {
        search_by_key();
    });

    function search_by_key() {

        var column_value = ['order_id', 'first_name', 'side_mark', 'order_date', 'due','order_stage','action'];

        var dataObj = {};

        /*dataObj.txtSearch = $("#txtSearch").val();*/
        dataObj.column_value = column_value;
        dataObj.filter_order_stage = $('#search_order_stage').val();

        var column = [];
        for (var i = 0; i < column_value.length; i++) {
            var columnObj = {};
            columnObj.data = column_value[i];
            column.push(columnObj);
        }


        var table_id = 'search_table';
        var url = "<?php echo base_url();?>super-admin/search/customer-order-list-api";

       var order_stage_option = '<option value="">Select order stage</option>';
        for (var i = 0; i < order_stage_arr.length; i++)
        {
            var result = order_stage_arr[i];
            var select = '';
            if(dataObj.filter_order_stage == result.order_stage)
                var select = 'selected';

            order_stage_option += '<option '+select+' value="'+result.order_stage+'">'+result.stage+'</option>';
        }

        search_by_datatable(table_id, url, dataObj, column, [-1]);


        $('.dataTables_filter').append('<div class="pull-right" style="padding-right: 15px;">' +
            '<select class="form-control" id="search_order_stage" onchange="search_by_key()">'+
            order_stage_option+
            '</select>' +
            '</div>'); //example is our table id

        $(".dataTables_filter label").addClass("pull-right");


    }
</script>
