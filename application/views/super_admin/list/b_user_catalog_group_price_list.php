
<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                             Group Price list
                        </h3>
                    </div>
                   
                </div>

                <div class="kt-portlet__body">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="search_table">
                        <thead>
                        <tr>
                            <th>Serial No.</th>
                            <th>Price Sheet Name</th>
                            <th>Assigned Product</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>


<script>
    $(document).ready(function () {
        search_by_key();
    });

    function search_by_key() {
        var column_value = ['serial_no', 'price_sheet_name', 'assigned_product', 'status'];
        var dataObj = {};
        dataObj.column_value = column_value;
        dataObj.b_user_id = '<?= $b_user_id; ?>';
        var column = [];
        for (var i = 0; i < column_value.length; i++) {
            var columnObj = {};
            columnObj.data = column_value[i];
            column.push(columnObj);
        }
        var table_id = 'search_table';
        var url = "<?php echo base_url();?>super-admin/search/wholesaler-catalog-group-price-list-api";
        search_by_datatable(table_id, url, dataObj, column, [-1]);
    }
</script>