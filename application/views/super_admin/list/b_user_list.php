<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            wholesaler List
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                        <?php
                        $user_id = $this->session->userdata('user_id');
                        $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                        $id=$user_role_info[0]->role_id;
                        $user_data = $this->db->select('*')
                                                ->from('admin_role_permission_tbl')
                                                ->where('menu_id =',13)
                                                ->where('role_id', $id)
                                                ->get()->result();
                        ?>
                            <?php if($user_data[0]->can_create==1 or $user_id==1){?>
                            <div class="kt-portlet__head-actions">
                                &nbsp;
                                <a href="<?php echo base_url(); ?>super-admin/wholesaler/add"
                                   class="btn btn-brand btn-elevate btn-icon-sm">
                                    <i class="la la-plus"></i>
                                    New wholesaler
                                </a>
                            </div>
                            <?php } ?>


                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="search_table">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>User Type</th>
                            <th>status</th>
                            <th>Action</th>
                            <th>Subscription</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>


<script>
    $(document).ready(function () {
        search_by_key();
    });

    function search_by_key() {
        var column_value = ['company', 'email', 'phone', 'address','user_type_status','status','action','subscription'];

        var dataObj = {};

        /*dataObj.txtSearch = $("#txtSearch").val();*/
        dataObj.column_value = column_value;

        var column = [];
        for (var i = 0; i < column_value.length; i++) {
            var columnObj = {};
            columnObj.data = column_value[i];
            column.push(columnObj);
        }


        var table_id = 'search_table';
        var url = "<?php echo base_url();?>super-admin/search/wholesaler-list-api";
        search_by_datatable(table_id, url, dataObj, column, [-1,-2]);
    }
</script>

<script>
    function user_status_change(user_id)
    {
        $.ajax({
            url: '<?php echo base_url(); ?>super_admin/B_user_controller/user_status_change/'+user_id,
            type: 'GET',
            dataType: 'json',
            success: function (data)
            {
                window.location.reload();
            }
        });
    }

    function user_delete(user_id)
    {
        if(confirm("Are You Want To Sure To Delete user"))
        {
            $.ajax({
                url: '<?php echo base_url(); ?>super_admin/B_user_controller/user_delete/'+user_id,
                type: 'GET',
                dataType: 'json',
                success: function (data)
                {
                    window.location.reload();
                }
            });
        }
    }

    
</script>

