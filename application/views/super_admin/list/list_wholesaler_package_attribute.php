<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Package Attribute List
                        </h3>
                    </div>
                </div>
                 <?php
                        $user_id = $this->session->userdata('user_id');
                        $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                        $id=$user_role_info[0]->role_id;
                        $user_data = $this->db->select('*')
                                                ->from('admin_role_permission_tbl')
                                                ->where('menu_id =',18)
                                                ->where('role_id', $id)
                                                ->get()->result();
                        ?>
                       

                <div class="kt-portlet__body">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="search_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Package Attribute</th>
                                <?php if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_data as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><?php echo $value->title; ?></td>
                                    <?php if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){?>
                                    <td>
                                        <a href="<?php echo base_url(); ?>super-admin/wholesaler/package-attribute/edit/<?php echo $value->id; ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                          <i class="la la-edit"></i>
                                        </a>
                                    </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo load_view('super_admin/layouts/footer'); ?>



