
<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            retailer transaction list
                        </h3>
                    </div>
                   <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                      
                          
                       
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <!--begin: Datatable -->
							<form class="form-horizontal" action="<?= base_url('super-admin/package-transaction/list') ?>" method="post">
		               		 	<div class="row">
                            		<div class="col-md-3">
                                	 	<select type="text" class="form-control created_at" placeholder="Colors Number" name="package_type" id="package_type" >
                                	 		<option value="2" >Advance</option>
                                	 		<option value="3">pro</option>
                                	 	</select>
                            		</div><div class="or_cls">-- OR --</div> 
                            		<div class="col-md-3">
                                		<input type="date" class="form-control colorname" placeholder="date" name="date" id="date">
                            		</div>
                            		<div>
                                    	<button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    	<a href="<?= base_url('super-admin/package-transaction/list') ?>" 
                                    	 type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</a>
                                	</div>
                                </div>
                            </form>

		        				<table class="table table-bordered table-hover">
		                			<thead>
						                    <tr>
						                       <th style=" width: 1%;">#</th>
						                        <th style=" width: 20%;">User Name</th>
						                        <th style=" width: 22%;">Email</th>  
						                        <th style=" width: 20%;">Transaction For</th>  
						                        <th style=" width: 18%;">Amount</th>
						                        <th style=" width: 20%;">Transaction</th>
						                        <th style=" width: 20%;">Status</th>
						                    </tr>
		                			</thead>
		                			<tbody>
		                               <?php $x='1'; ?>
		                                 <?php foreach ($transaction as $key => $value){?>

		                                    <tr>
		                                    <?php if($value->transaction_id!=0){?>
		                                       <td><?php echo $x;?></td>
		                                       <td><span class="kt-widget11__title"><?php echo $value->first_name." "; if (isset($value->last_name)) { echo $value->last_name; } ?></span></td>
		                                       <td><?php echo $value->email; ?></td>
		                                       <td><?php echo $value->package; ?></td>
		                                       <td><?php echo $value->amount; ?>$</td>
		                                       <td><?php echo $value->transaction_id; ?></td>
		                                       <td><?php 
		                                        if ($value->status==1) {
		                                           echo '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Active</span>';
		                                        }else{
		                                           echo '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Inactive</span>';
		                                        }
		                                       ?></td>
		                                      <?php $x++; } ?> 
		                                    </tr>
		                                 <?php } ?>
		                             </tbody>
		               
					                <tfoot>
					                        <tr>
					                            <th colspan="7" class="text-danger text-center">Record not found!</th>
					                        </tr>
					                </tfoot>
		           				</table> 
		                <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>