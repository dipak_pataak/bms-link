<?php $assets_url = base_url() . 'super_admin_assets/'; ?>

<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>
<!--begin::Page Custom Styles(used by this page) -->
<link href="<?php echo $assets_url; ?>css/demo2/pages/pricing/pricing-4.css" rel="stylesheet" type="text/css"/>
<!--end::Page Custom Styles -->
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content Head -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Package details</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                </div>

            </div>
        </div>
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <!--begin::Basic Pricing 4-->
                    <div class="kt-pricing-4">
                        <div class="kt-pricing-4__top">
                            <div class="kt-pricing-4__top-container kt-pricing-4__top-container--fixed">
                                <div class="kt-pricing-4__top-header">
                                    <div class="kt-pricing-4__top-title kt-font-light">
                                        <h1>Package details</h1>
                                    </div>
                                </div>
                                <div class="kt-pricing-4__top-body">

                                    <!--begin::Pricing Items-->
                                    <div class="kt-pricing-4__top-items">
                                        <?php foreach ($package_list_data AS $key => $value) : ?>
                                        <!--begin::Pricing Item-->

                                        <?php 
                                        $column = '';
                                        switch ($key) {
                                            case 0:
                                                $column = 'basic';
                                                break;
                                            case 1:
                                                $column = 'advanced';
                                                break;
                                            case 2:
                                                $column = 'pro';
                                                break;
                                        } ?>


                                        <div class="kt-pricing-4__top-item">
                                            <span class="kt-pricing-4__icon kt-font-info">
                                                <?php if($key == 0):
                                                echo '<i class="fa flaticon-rocket"></i>';
                                                elseif($key == 1):
                                                echo '<i class="fa flaticon-confetti"></i>';
                                                elseif($key == 2):
                                                echo '<i class="fa flaticon-gift"></i>';
                                                endif; ?>
                                            </span>
                                            <h2 class="kt-pricing-4__subtitle">
                                                <?= $value->package ?>
                                            </h2>
                                            <div class="kt-pricing-4__features">
                                                <span>Trial days : <?= $value->trial_days;?></span><br>
                                                <span>Installation price : <?= $value->installation_price;?></span><br>
                                                <span>Monthly price : <?= $value->monthly_price;?></span><br>
                                                <span>Yearly price : <?= $value->yearly_price;?></span><br>

                                            </div>
                                            <span class="kt-pricing-4__price"><?= $value->monthly_price;?></span>
                                            <span class="kt-pricing-4__label">$</span>

                                         <?php
                                            $user_id = $this->session->userdata('user_id');
                                            $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                            $id=$user_role_info[0]->role_id;
                                            $user_data = $this->db->select('*')
                                                                    ->from('admin_role_permission_tbl')
                                                                    ->where('menu_id =',17)
                                                                    ->where('role_id', $id)
                                                                    ->get()->result();
                                           
                                            if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1){
                                                ?>
                                            <div class="kt-pricing-4__btn">
                                                <a href="<?= base_url();?>super-admin/wholesaler/package/edit/<?= $value->id; ?>/<?php echo $column; ?>" class="btn btn-pill btn-info btn-upper btn-bold">
                                                    Edit
                                                </a>
                                            </div>
                                            <?php } ?>

                                        </div>
                                        <!--end::Pricing Items-->

                                        <?php endforeach; ?>


                                    </div>
                                    <!--end::Pricing Items-->
                                </div>
                            </div>
                        </div>
                        <div class="kt-pricing-4__bottom">
                            <div class="kt-pricing-4__bottok-container kt-pricing-4__bottok-container--fixed">
                                
                                <?php foreach ($package_option_list_data as $key => $value) { ?>
                                    <div class="kt-pricing-4__bottom-items">
                                        <div class="kt-pricing-4__bottom-item">
                                            <?php echo $value->title; ?>
                                        </div>
                                        <div class="kt-pricing-4__bottom-item">
                                            <i class="flaticon2-<?php if($value->basic==1){ echo 'check-mark'; }else{ echo 'cross'; } ?>"></i>
                                        </div>
                                        <div class="kt-pricing-4__bottom-item">
                                            <i class="flaticon2-<?php if($value->advanced==1){ echo 'check-mark'; }else{ echo 'cross'; } ?>"></i>
                                        </div>
                                        <div class="kt-pricing-4__bottom-item">
                                            <i class="flaticon2-<?php if($value->pro==1){ echo 'check-mark'; }else{ echo 'cross'; } ?>"></i>
                                        </div>
                                    </div>
                                <?php } ?>"


                            </div>
                        </div>

                    </div>
                    <!--end::Basic Pricing 4-->
                </div>
            </div>
            <!--end::Portlet-->


        </div>
        <!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>

