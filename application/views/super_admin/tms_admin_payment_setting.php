<?php echo load_view('super_admin/layouts/header'); ?>
<?php echo load_view('super_admin/layouts/top_header'); ?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
     
    </div>
    <!-- end:: Content Head -->
    
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">

            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                         Tms Payment Setting 
                    </h3>
                </div>
            </div>


            <div class="kt-portlet__body">
                <form id="add_update_form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <div class="kt-form kt-form--label-right">
                                <div class="kt-form__body">

                                <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">URL</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="url" value="<?php if (isset($tms_setting->url)): echo $tms_setting->url; else: echo set_value('url'); endif; ?>" >
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Username</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="user_name" value="<?php if (isset($tms_setting->user_name)): echo $tms_setting->user_name; else: echo set_value('user_name'); endif; ?>" >
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input type="text" class="form-control" name="password" value="<?php if (isset($tms_setting->password)): echo $tms_setting->password; else: echo set_value('password'); endif; ?>" >
                                            </div>
                                        </div>

                                        
                                        <input type="hidden" name="tms_payment_setting_id" value="<?php if (isset($tms_setting->id) && !empty($tms_setting->id)): echo $tms_setting->id; endif ?>">
                                      
                                        <div class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>

                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-xl-3"></div>
                                                <div class="col-lg-9 col-xl-6">
                                                

                                                    <?php
                                                    $user_id = $this->session->userdata('user_id');
                                                    $user_role_info = $this->db->select('*')->from('admin_user_access_tbl')->where('user_id',$user_id)->get()->result();
                                                    $id=$user_role_info[0]->role_id;
                                                    $user_data = $this->db->select('*')
                                                                            ->from('admin_role_permission_tbl')
                                                                            ->where('menu_id =',20)
                                                                            ->where('role_id', $id)
                                                                            ->get()->result();
                                                    ?>
                                                    <?php  if($user_data[0]->can_edit==1 or $this->session->userdata('user_id')==1)
                                                    {?>
                                                    <button type="button"
                                                            onclick="add_update_details('add_update_form','<?php echo base_url(); ?>super-admin/tms-payment-setting')"
                                                            class="btn btn-label-brand btn-bold form-post-btn">Save
                                                    </button>
                                                    <?php } ?>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end:: Content -->                        </div>
</div>


<?php echo load_view('super_admin/layouts/footer'); ?>

