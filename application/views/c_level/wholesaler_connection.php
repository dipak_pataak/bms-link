<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<main>
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-12">
				<div class="mb-3">
					<!-- <h1>Wholesaler Connection</h1> -->
				</div>
				
			</div>
		</div>




		<div class="card mb-4">
			<div class="card-body">
			
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<h1>Wholesaler Connection</h1>
					</div>

					<div class="col-sm-6 col-md-6">
						<label class="switch">
						    <input type="checkbox" id="wholsaler_connection" onclick="update_wholesaler_connection()" <?php if ($user_detail->wholesaler_connection==1) { echo 'checked'; } ?>  >
						    <span class="slider round"></span>
						</label>
					</div>
				</div>
				
			</div>
		</div>

	</div>
</main>

<script>

	function update_wholesaler_connection()
    {
    	if($('#wholsaler_connection').is(":checked"))
		{
		  var status = 1;
		}else{
		  var status = 0;
		}

         $.ajax({
            url: "<?php echo base_url(); ?>c_level/Customer_controller/update_wholesaler_connection/"+status,
            dataType: "JSON",
            success: function (data) {

                if (data==true) 
                {
                    console.log('success');
                }
               
            },error: function() {

            }
        });
     }
</script>




