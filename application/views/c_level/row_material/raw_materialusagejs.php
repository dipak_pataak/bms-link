<script type="text/javascript">
$(document).ready(function(){

$("body").on('click','.add_new_data', function(){

cnt = $(this).data('cnt');

var get_item_data=$("#get_item_data").val();

$.ajax({
			url: "c_level/Row_materials/get_item_details/",
			type: "POST",
			data: {item_order_id: get_item_data},
			success: function (r) {
				$("#item_name_order_"+cnt).html(r);
			}
			});
			
        var row = $("#addinvoiceItem_"+cnt+" tr").length;
        var count = row + 1;
		var testes='addinvoiceItem';
		$('.add_new_data').hide();
		
			cnt++;
				$('#add_new_data_'+cnt).show();
			$html ='<table class="table table-bordered table-hover" id="normalinvoice"><thead><tr><th class="text-center" width="15%">Item Name</th><th class="text-center" width="15%">Height</th><th class="text-center" width="15%">Width</th><th class="text-center" width="10%">Height*Width</th><th class="text-center" width="10%"></th><th class="text-center" width="10%"></th></tr></thead><tbody id="addinvoiceItem_test_'+cnt+'"><tr><td><select  id="item_name_order_'+cnt+'" name="item_name_order['+cnt+'][]" onchange="get_item_id_order(' + cnt + ')" class="form-control select2" tabindex="3"  required></select></td><td><input type="text" class="form-control" id="i_width_'+cnt+'" value="" readonly="" /></td><td><input type="text" class="form-control" id="i_height_'+cnt+'" value="" readonly="" /></td><td class="text-right"><input type="text" class="form-control" id="i_total_'+cnt+'" value="" readonly="" /></td><td class="text-right"></td><td class="text-right"></td></tr></tbody>';
			
$html += '<thead><tr><th class="text-center" width="15%">Material</th><th class="text-center" width="15%">Color</th><th class="text-center" width="15%">Pattern</th><th class="text-center" width="10%">Available Quantity/Unit</th><th class="text-center" width="10%">Quantity/Unit</th><th class="text-center" width="10%">Action</th></tr></thead>';
 
 
 
$html +='<tbody id="addinvoiceItem_'+cnt+'"><tr><td><select  id="rmtt_id_'+count+'_'+cnt+'" onchange="service_cals1(' + count + ',' + cnt + ')" name="rmtt_id['+cnt+'][]" class="form-control select2" tabindex="3" data-placeholder="-- select one --" required><option value=""></option><?php foreach ($rmtts as $val) { ?><option value="<?= $val->id ?>"> <?= $val->material_name; ?></option><?php } ?></select></td><td><select  id="color_id_'+count+'_'+cnt+'" onchange="color_cals1(' + count + ',' + cnt + ')" name="color_id['+cnt+'][]" class="form-control select2" tabindex="4" data-placeholder="-- select one --" required><option value="">-- select one --</option></select></td>';
$html+='<td><select  id="pattern_model_id_'+count+'_'+cnt+'"" name="pattern_model_id['+cnt+'][]" class="form-control select2" tabindex="5" data-placeholder="-- select one --" required><option value=""></option></select></td> <td class="text-right"><input type="text" name="available_qnt['+cnt+'][]"  class="form-control text-center"  id="available_qnt_'+count+'_'+cnt+'"" tabindex="6" min="0"></td><td class="text-right"><input type="text" name="quantity['+cnt+'][]"  class="form-control text-center" onkeyup="quantity_check(' + count + ',' + cnt + ')" id="quantity_'+count+'_'+cnt+'"" tabindex="6" min="0"></td><td class="text-right"><button class="btn btn-danger" type="button" value="Delete" onclick="deleteRow(this)" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="simple-icon-trash"></i></button></td></tr></tbody>';
$html +='<tfoot><tr><td colspan="5" rowspan="1"><input id="add-invoice-item" class="btn btn-info" name="add-new-item" onclick="addInputField1('+cnt+')" value="Add Material" type="button" style="margin: 0px 15px 15px;"><a href="javascript:void(0)" class="btn btn-info add_new_data" id="add_new_data_'+cnt+'" data-cnt="'+cnt+'" style="float:right;">Add Item</a></td></tr></tfoot></table>';
			
			 $("#append_row").append($html);
			
});
 });
 
 function get_item_id_order(id)
{
		var item_raw_id=$("#item_name_order_"+id).val();
			$.ajax({
			url: "c_level/Row_materials/get_item_dimension/",
			type: "POST",
			data: {item_raw_id: item_raw_id},
			success: function (result) {
				obj = jQuery.parseJSON(result);
				
				$("#i_width_"+id).val(obj.get_width);
				$("#i_height_"+id).val(obj.get_height);
				$("#i_total_"+id).val(obj.get_total);
			}
			});

}

//    var count = 2;
//    limits = 500;
// ========== its for row add dynamically =============
    function addInputField(t,rcnt) {
        var row = $("#addinvoiceItem tr").length;
        var count = row + 1;
        var limits = 500;
		
        if (count == limits) {
            Swal.fire("You have reached the limit of adding" + count + "inputs");
        } else {
            var a = "rmtt_id_" + count, e = document.createElement("tr");

            e.innerHTML = "<td>\n\
                            <select  id='rmtt_id_" + count + "' onchange='service_cals(" + count + ")' name='rmtt_id[" + rcnt + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
<?php foreach ($rmtts as $val) { ?> <option value='<?php echo $val->id ?>'> <?php echo $val->material_name; ?></option><?php } ?>  </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='color_id_" + count + "'  onchange='color_cals(" + count + ")' name='color_id[" + rcnt + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='pattern_model_id_" + count + "' name='pattern_model_id[" + rcnt + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
							<td class='text-right'><input type='text' name='available_qnt["+rcnt+"][]'  class='form-control text-center'  id='available_qnt_" + count + "' tabindex='6' min='0'></td>\n\
                <td><input type='text' class='form-control' name='quantity[" + rcnt + "][]' id='quantity_" + count + "' min='0' placeholder='' onkeyup='quantity_check(" + count + ")'  style='text-align:center'></td>\n\
                <td class='text-center'>\n\
                <button class='btn btn-danger' type='button' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></button></td>\n\
                ",
                    document.getElementById(t).appendChild(e), document.getElementById(a).focus(), count++,
                    $('.select2').select2();
        }
    }
	
	 function addInputField1(id) {
        //var row = $('#addinvoiceItem_'+id+ 'tr').length;
		var row = $("#addinvoiceItem_"+id+" tr").length;
        
			var count = row+1;
		
		var cnt=id+1;
        var limits = 500;
		var t1="addinvoiceItem_"+id;
        if (count == limits) {
            Swal.fire("You have reached the limit of adding" + count + "inputs");
        } else {
            var a = "rmtt_id_" + count, e = document.createElement("tr");

            e.innerHTML = "<td>\n\
                            <select  id='rmtt_id_" + count +'_'+ cnt + "' onchange='service_cals1(" + count + "," + cnt + ")' name='rmtt_id[" + id + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
<?php foreach ($rmtts as $val) { ?> <option value='<?php echo $val->id ?>'> <?php echo $val->material_name; ?></option><?php } ?>  </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='color_id_" + count +'_'+ cnt + "'  onchange='color_cals1(" + count + "," + cnt + ")' name='color_id[" + id + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
                            <td>\n\
                            <select  id='pattern_model_id_" + count +'_'+ cnt + "' name='pattern_model_id[" + id + "][]' class='form-control select2' tabindex='1' data-placeholder='-- select one --' required>>\n\
                                <option value=''>-- select one --</option>\n\
                            </select>\n\
                            </td>\n\
							<td class='text-right'><input type='text' name='available_qnt["+id+"][]'  class='form-control text-center'  id='available_qnt_" + count +'_'+ cnt + "' tabindex='6' min='0'></td>\n\
                <td><input type='text' class='form-control' name='quantity[" + id + "][]' id='quantity_" + count +'_'+ cnt + "' min='0' placeholder='' onkeyup='quantity_check(" + count + "," + cnt + ")'  style='text-align:center'></td>\n\
                <td class='text-center'>\n\
                <button class='btn btn-danger' type='button' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></button></td>\n\
                ",
                    document.getElementById(t1).appendChild(e), document.getElementById(a).focus(), count++,
                    $('.select2').select2();
					cnt++;
        }
    }
// ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalinvoice > tbody > tr").length;
        if (1 == a) {
            Swal.fire("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
        }
        calculateSum()
    }

//Calcucate Invoice Add Items
//    function quantity_calculate(item) {
//        var available_qnt = $("#available_qnt_" + item).val();
//        var product_quantity = $("#product_quantity_" + item).val();
//        var product_rate = $("#product_rate_" + item).val();
//        var product_discount = $("#product_discount_" + item).val();
//        var total_price = $("#total_price_" + item).val();
//        var invoice_discount = $("#invoice_discount").val();
//        var total_discount = $("#total_discount_" + item).val();
//        if (product_quantity > 0 && product_rate > 0) {
//            var total_amount = product_quantity * product_rate;
//            $("#total_price_" + item).val(total_amount);
//        }
//        if (product_discount > 0) {
//            $("#total_price_" + item).val(total_amount - product_discount);
//        }
//        calculateSum();
//    }
//
//
//    function calculateSum() {
//        var t = 0,
//                a = 0,
//                e = 0,
//                o = 0,
//                p = 0;
//        $(".total_price").each(function () {
//            isNaN(this.value) || 0 == this.value.length || (e += parseFloat(this.value))
//        }),
//                $(".product_discount").each(function () {
//            isNaN(this.value) || 0 == this.value.length || (p += parseFloat(this.value))
//        }),
//                $("#grandTotal").val(e.toFixed(2));
//        var gt = $("#grandTotal").val();
//        var invoiceDiscount = $("#invoice_discount").val();
//        var ttl_discount = +invoiceDiscount + +p.toFixed(2, 2);
//        $("#total_discount").val(ttl_discount);
//        var grandTotals = e.toFixed(2) - invoiceDiscount;
//        $("#grandTotal").val(grandTotals);
//        invoice_paidamount();
//    }
//
////Invoice Paid Amount
//    function invoice_paidamount() {
//        var t = $("#grandTotal").val(),
//                a = $("#paidAmount").val(),
//                e = t - a;
//        $("#dueAmmount").val(e.toFixed(2, 2))
//    }
//============== its for quantity_check ============
    function quantity_check(item) {
//        Swal.fire($("#quantity_" + item).val());
        var material_id = $("#rmtt_id_" + item).val();
        var color_id = $("#color_id_" + item).val();
        var pattern_model_id = $("#pattern_model_id_" + item).val();
        var quantity = $("#quantity_" + item).val();
//        if ($("#rmtt_id_" + item).val() == '') {
//            Swal.fire("Material must be required");
////            $("#rmtt_id_" + item).val('');
//            return false;
//        } else if ($("#color_id_" + item).val() == '') {
//            Swal.fire("Color must be required");
////            $("#color_id_" + item).val('');
//            return false;
//        } else if ($("#pattern_model_id_" + item).val() == '') {
//            Swal.fire("Pattern must be required");
////            $("#pattern_model_id_" + item).val('');
//            return false;
//        } else {
//        Swal.fire(quantity);
        $.ajax({
            url: "c_level/Row_materials/quantity_check/",
            type: "POST",
            data: {material_id: material_id, color_id: color_id, pattern_model_id: pattern_model_id, quantity: quantity},
            success: function (r) {
                if (r != '') {
                    Swal.fire(r);
                }
            }
        });
//        }
    }

function quantity_check1(item,item1) {
//        Swal.fire($("#quantity_" + item).val());
        var material_id = $("#rmtt_id_" + item+'_'+item1).val();
        var color_id = $("#color_id_" + item+'_'+item1).val();
        var pattern_model_id = $("#pattern_model_id_" + item+'_'+item1).val();
        var quantity = $("#quantity_" + item+'_'+item1).val();
//        if ($("#rmtt_id_" + item).val() == '') {
//            Swal.fire("Material must be required");
////            $("#rmtt_id_" + item).val('');
//            return false;
//        } else if ($("#color_id_" + item).val() == '') {
//            Swal.fire("Color must be required");
////            $("#color_id_" + item).val('');
//            return false;
//        } else if ($("#pattern_model_id_" + item).val() == '') {
//            Swal.fire("Pattern must be required");
////            $("#pattern_model_id_" + item).val('');
//            return false;
//        } else {
//        Swal.fire(quantity);
        $.ajax({
            url: "c_level/Row_materials/quantity_check/",
            type: "POST",
            data: {material_id: material_id, color_id: color_id, pattern_model_id: pattern_model_id, quantity: quantity},
            success: function (r) {
                if (r != '') {
                    Swal.fire(r);
                }
            }
        });
//        }
    }

 function service_cals(item) {
	
        var invoice_form = $("#purchase_frm").serializeArray();
        var raw_material_id = $("#rmtt_id_" + item).val(); 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_wise_color_info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj);
                $("#color_id_" + item).empty();
                $("#color_id_" + item).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
                    $('#color_id_' + item).append($('<option>').text(typevar.color_name).attr('value', typevar.id));
                });
            },
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_wise_pattern_info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
                $("#pattern_model_id_" + item).empty();
//                $("#pattern_model_id_" + item).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
//                    console.log(typevar);
                    $('#pattern_model_id_' + item).append($('<option>').text(typevar.pattern_name).attr('value', typevar.pattern_model_id));
                });
            },
        });
    }
	
    function service_cals1(item,item1) {
	
        var invoice_form = $("#purchase_frm").serializeArray();
        var raw_material_id = $("#rmtt_id_" + item+'_'+item1).val(); 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_wise_color_info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj);
                $("#color_id_" + item+'_'+item1).empty();
                $("#color_id_" + item+'_'+item1).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
                    $('#color_id_' + item+'_'+item1).append($('<option>').text(typevar.color_name).attr('value', typevar.id));
                });
            },
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_wise_pattern_info/" + raw_material_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
                $("#pattern_model_id_" + item+'_'+item1).empty();
//                $("#pattern_model_id_" + item).html("<option value=''>Select One</option>");
                $.each(obj, function (r, typevar) {
//                    console.log(typevar);
                    $('#pattern_model_id_' + item+'_'+item1).append($('<option>').text(typevar.pattern_name).attr('value', typevar.pattern_model_id));
                });
            },
        });
    }
//============= its for material, color and pattern wise stock summation when change color_cals function =============
    function color_cals(item) {
        var raw_material_id = $("#rmtt_id_" + item).val();
        var color_id = $("#color_id_" + item).val();
        var pattern_model_id = $("#pattern_model_id_" + item).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_color_pattern_wise_stock_qnt/" + raw_material_id + "/" + color_id + "/" + pattern_model_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj.in_quantity);
                $('#available_qnt_' + item).val(obj.available_quantity);
//                $('#product_rate_' + item).val(obj.out_quantity);
            },
        });
    }
	
	function color_cals1(item,item1) {
        var raw_material_id = $("#rmtt_id_" + item+'_'+item1).val();
        var color_id = $("#color_id_" + item+'_'+item1).val();
        var pattern_model_id = $("#pattern_model_id_" + item+'_'+item1).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>c_level/Purchase_controller/get_material_color_pattern_wise_stock_qnt/" + raw_material_id + "/" + color_id + "/" + pattern_model_id,
            success: function (s) {
                var obj = jQuery.parseJSON(s);
//                console.log(obj.in_quantity);
                $('#available_qnt_' + item+'_'+item1).val(obj.available_quantity);
//                $('#product_rate_' + item).val(obj.out_quantity);
            },
        });
    }

//    function flight_calculate(item) {
//        var invoice_form = $("#invoice_frm").serializeArray();
//        var service_id = $("#service_id_" + item).val();
//        var flight_id = $("#flight_id_" + item).val();
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>get-service-info/" + service_id + "/" + flight_id,
//            success: function (data) {
//                var obj = jQuery.parseJSON(data);
////                        Swal.fire(obj.total_service);
//                $('#available_qnt_' + item).val(obj.total_service);
//
//            },
//        });
//    }

    var count = 2,
            limits = 500;
</script>

