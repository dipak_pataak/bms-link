<style type="text/css">
    .condition_checkbox{
        font-size: 14px;
        overflow: scroll;
        height: 76px;
    }
</style>
<?php
if ($category_wise_condition) {
    foreach ($category_wise_condition as $condition) {
        ?>
        <label for="condition_id_<?php echo $condition->condition_id; ?>" style="min-width: 190px; min-height: 20px;">
            <input type="checkbox" name="condition_id[]" id="condition_id_<?php echo $condition->condition_id; ?>" value="<?php echo $condition->condition_id; ?>"> <?php echo ucwords($condition->condition_text); ?>
        </label>
    <?php
    }
} else {
    echo "Not Found!";
}
?>