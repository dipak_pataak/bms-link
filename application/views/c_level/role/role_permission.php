
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Role Permissions</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Role Permissions';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <?php
                            $user_data=access_role_permission(35);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Role Permissions</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo base_url(); ?>role-save" id="moduleFrm" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="form-group row">
                                        <label for="role_name" class="col-sm-3 col-form-label">Role Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="role_description" class="col-sm-3 col-form-label">Description</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="role_description" id="role_description" placeholder="Role Description"></textarea>
                                        </div>
                                    </div>
                                    <div class="float-right" style="margin: 0px 20px 0px 0px;">
										<div class="form-check">
											<span class="right_checkbox"><label class="form-check-label" for="select_deselect">Select / De-select</label>
											<input type="checkbox" class="form-check-input" id="select_deselect"></span>
										</div>
                                    </div>
                                    <?php
                                    $m = 0;
                                    foreach ($modules as $module) {
                                        $menu_item = $this->db->select('*')->from('c_menusetup_tbl')->where('module', $module->module)->where('status', 1)
                                                        ->where('level_id', $level_id)->get()->result();
                                        ?>
                                        <input type="hidden" name="module[]" value="<?php echo $module->module; ?>">
										<div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <h2><?php echo ucwords($module->module); ?></h2>
                                            <thead>
                                                <tr>
                                                    <th class="text-center" width="10%">Sl No</th>
                                                    <th >Menu Title </th>
                                                    <th class="text-center" width="15%">
														<div class="form-check p-0">
															<label class="form-check-label" for="<?php echo $module->module; ?>_can_create_all">Can Create</label>
															<span class="right_checkbox">All <input type="checkbox" class="form-check-input can_create_all" id="<?php echo $module->module; ?>_can_create_all" value="<?php echo strtolower($module->module); ?>"></span>
														</div>
                                                    </th>
                                                    <th class="text-center" width="15%">
														<div class="form-check p-0">
															<label class="form-check-label" for="<?php echo $module->module; ?>_can_read_all">Can Read</label>
															<span class="right_checkbox">All <input type="checkbox" class="form-check-input can_read_all" id="<?php echo $module->module; ?>_can_read_all" value="<?php echo strtolower($module->module); ?>"></span>
														</div>
                                                    </th>
                                                    <th class="text-center" width="15%">
														<div class="form-check p-0">
															<label class="form-check-label" for="<?php echo $module->module; ?>_can_edit_all">Can Edit</label>
															<span class="right_checkbox">All <input type="checkbox" class="form-check-input can_edit_all" id="<?php echo $module->module; ?>_can_edit_all" value="<?php echo strtolower($module->module); ?>"></span>
														</div>
                                                    </th>
                                                    <th class="text-center" width="15%">
														<div class="form-check p-0">
															<label class="form-check-label" for="<?php echo $module->module; ?>_can_delete_all">Can Delete</label>
															<span class="right_checkbox">All <input type="checkbox" class="form-check-input can_delete_all" id="<?php echo $module->module; ?>_can_delete_all" value="<?php echo strtolower($module->module); ?>"></span>
														</div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($menu_item)) {
                                                    $sl = 0;
                                                    foreach ($menu_item as $menu) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-center"><?php echo $sl + 1; ?></td>
                                                            <td class="text-<?php echo ($menu->parent_menu ? 'right' : '') ?>">
                                                                <?php echo str_replace("_", " ", ucwords($menu->menu_title)); ?>
                                                            </td>
                                                            <td>
                                                                <div class="checkbox checkbox-success text-center">
                                                                    <input type="checkbox" name="create[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="create[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo strtolower($menu->module); ?>_can_create">
                                                                    <label for="create[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="checkbox checkbox-success text-center">
                                                                    <input type="checkbox" name="read[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="read[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo strtolower($menu->module); ?>_can_read">
                                                                    <label for="read[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                                </div>
                                                            </td> 
                                                            <td>
                                                                <div class="checkbox checkbox-success text-center">
                                                                    <input type="checkbox" name="edit[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="edit[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo strtolower($menu->module); ?>_can_edit">
                                                                    <label for="edit[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                                </div>
                                                            </td> 
                                                            <td>
                                                                <div class="checkbox checkbox-success text-center">
                                                                    <input type="checkbox" name="delete[<?php echo $m ?>][<?php echo $sl ?>][]" value="1" id="delete[<?php echo $m ?>]<?php echo $sl ?>" class="sameChecked <?php echo strtolower($menu->module); ?>_can_delete">
                                                                    <label for="delete[<?php echo $m ?>]<?php echo $sl ?>"></label>
                                                                </div>
                                                            </td>
                                                    <input type="hidden" name="menu_id[<?php echo $m ?>][<?php echo $sl ?>][]" value="<?php echo $menu->menu_id ?>">

                                                    </tr>
                                                    <?php
                                                    $sl++;
                                                }
                                                ?>
                                                <?php
                                                $m++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
										</div>
								   <?php }
                                    ?>

                                    <div class="form-group row">
                                        <div class="col-md-2 pt-1">
                                            <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                            <button type="submit" class="btn btn-info module_btn">Save & Next</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#select_deselect', function () {

            $(".sameChecked").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_create_all', function () {
            var create_value = $(this).val();
            $("." + create_value + "_can_create").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_read_all', function () {
            var read_value = $(this).val();
            $("." + read_value + "_can_read").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_edit_all', function () {
            var edit_value = $(this).val();
            $("." + edit_value + "_can_edit").prop('checked', $(this).prop('checked'));
        });
        $('body').on('click', '.can_delete_all', function () {
            var delete_value = $(this).val();
            $("." + delete_value + "_can_delete").prop('checked', $(this).prop('checked'));
        });
    });
</script>
