<?php
if ($this->session->userdata('isAdmin') == 1) {
    $level_id = $this->session->userdata('user_id');
} else {
    $level_id = $this->session->userdata('admin_created_by');
}
$user_id = $this->session->userdata('user_id');
$user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
?>
<style>
.sidebar .main-menu ul li a {
	text-align: center;
}
</style>
<div class="sidebar">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="<?php
                if ($this->uri->segment(1) == 'retailer-dashboard') {
                    echo 'active';
                }
                ?>" id="remove_active">
                    <?php
//                    ========= its for only dashboard show by statically ===============
                    $allmoduless = $this->db->select('*')->from('menusetup_tbl')->where('menu_type', 1)
                                    ->group_by('module')->order_by('ordering', 'asc')->get()->result();
                    if (@$user_info[0]->language == 'English') {
                        $menu_title = $allmoduless[0]->menu_title;
                    } elseif (@$user_info[0]->language == 'Korean') {
                        $menu_title = $allmoduless[0]->korean_name;
                    } else {
                        $menu_title = $allmoduless[0]->menu_title;
                    }
                    ?>
                    <a href="<?php echo base_url(); ?>retailer-dashboard">
                        <i class="iconsmind-Shop-4"></i>
                        <span><?php echo ucfirst($menu_title); ?></span>
                    </a>
                </li>
                  <?php $user_id= $this->session->userdata('user_id');

       $sub = $this->db->select('*')->where('id',$user_id)->from('user_info')->get()->result_array();
       $bsub = $this->db->select('*')->where('id',$sub[0]['created_by'])->from('user_info')->get()->result_array();
    

        ?>

                <?php
                $allmodule = $this->db->select('*')->from('c_menusetup_tbl')->where('menu_type', 1)->where('status', 1)
                                ->where('level_id', $level_id)
                                ->group_by('module')->order_by('ordering', 'asc')->get()->result();
                foreach ($allmodule as $module) {
                    $menu_item = $this->db->select('*')
                                    ->from('c_menusetup_tbl')
                                    ->where('module', $module->module)
                                    ->where('parent_menu =', $module->menu_id)
                                    ->where('status', 1)
                                    ->where('level_id', $level_id)
                                    ->order_by('ordering', 'asc')
                                    ->get()->result();
                    $module_id = $module->parent_menu;

                    if ($user_info[0]->language == 'English') {
                        $menu_title = $module->menu_title;
                    } elseif ($user_info[0]->language == 'Korean') {
                        $menu_title = $module->korean_name;
                    } else {
                        $menu_title = $module->menu_title;
                    }
                    // $i++;
                    if (empty($menu_item)) {
                        $link = base_url() . $module->page_url;
                    } else {
                        if($menu_item[0]->menu_id==73 && $user_info[0]->package_id==1) 
                        {
                            if($module->module=='gallery'){
                                $link = base_url() .'wholeseller_images'; 

                            }else{

                            $link = base_url() . $menu_item[1]->page_url;
                            }
                           
                        }else{
                            $link = "#" . $module->module;
                        }
                        
                    }
                    if ($this->permission->module($module->module)->access()) {
                        ?>

                    <?php $check = package_access($module->menu_id);?>
                    <?php if($check==true) { ?>
                        <?php if ($menu_title!='' || $user_info[0]->package_id==1) { ?>
                            <?php //echo @$menu_title;  ?>
                            <li class="" id="<?php echo $module->module; ?>">               
                                <?php if ($user_info[0]->package_id!=1 ){ ?>
                                <a href="<?php echo $link; ?>">
                                    <i class="<?php echo $module->icon; ?>"></i> <?php echo ucfirst(@$menu_title); ?>
                                </a>
                                  <?php } else if ($user_info[0]->package_id==1 && $bsub[0]['package_id']=='0' ) { ?>
                                      <a href="<?php echo base_url(); ?>package">
                                    <i class="<?php echo $module->icon; ?>"></i> <?php echo ucfirst(@$menu_title); ?>
                                </a>
                                  <?php }else{ ?>
                                      <a href="<?php echo $link; ?>">
                                    <i class="<?php echo $module->icon; ?>"></i> <?php echo ucfirst(@$menu_title); ?>
                                </a>

                                      <?php } ?>


                            </li>
                        <?php } ?>


                    <?php } ?>




                        <?php
                    }
                }
                ?>
                <?php if ($this->session->userdata('isAdmin') == '1') { ?>
                    <!--                    <li>
                                            <a href="#setting">
                                                <i class="simple-icon-settings"></i> Settings
                                            </a>
                                        </li>-->
                <?php } ?>

                

            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <?php
            foreach ($allmodule as $module) {
                $menu_item = $this->db->select('*')
                                ->from('c_menusetup_tbl')
                                ->where('module', $module->module)
                                ->where('parent_menu =', $module->menu_id)
                                ->where('status', 1)
                                ->where('level_id', $level_id)
                                ->order_by('ordering', 'asc')
                                ->get()->result();
                $module_id = $module->parent_menu;
//              
                ?>
                <ul class="list-unstyled" data-link="<?php echo $module->module; ?>">
                    <?php
                    foreach ($menu_item as $menu) {
                        if ($user_info[0]->language == 'English') {
                            $submenu_title = $menu->menu_title;
                        } elseif ($user_info[0]->language == 'Korean') {
                            $submenu_title = $menu->korean_name;
                        } else {
                            $submenu_title = $menu->menu_title;
                        }
//                        var_dump($this->permission->check_label($menu->menu_id)->access());exit();
                        if ($this->permission->check_label($menu->menu_id)->access()) {
                            $parent_id = $menu->menu_id;
                            $sub_sub_menu = $this->db->select('*')
                                            ->from('c_menusetup_tbl')
                                            ->where('module', $menu->module)
                                            ->where('parent_menu =', $parent_id)
                                            ->where('status', 1)
                                            ->where('level_id', $level_id)
                                            ->order_by('ordering', 'asc')
                                            ->get()->result();
                            ?>
                            <li>
                                <?php if (!empty($sub_sub_menu)) { ?>
                                    <?php $check = package_access($menu->menu_id); ?>
                                    <?php if($check==true) { ?>
                                        <a href="#" class="menu-link"> 
                                            <?php echo ucwords(str_replace('_', ' ', $submenu_title)); ?> <i class="simple-icon-arrow-down"></i>
                                        </a>
                                    <?php } ?>

                                    <ul class="list-unstyled">
                                        <li>
                                            <?php
                                            foreach ($sub_sub_menu as $child) {
                                                if ($user_info[0]->language == 'English') {
                                                    $childmenu_title = $child->menu_title;
                                                } elseif ($user_info[0]->language == 'Korean') {
                                                    $childmenu_title = $child->korean_name;
                                                } else {
                                                    $childmenu_title = $child->menu_title;
                                                }
                                                if ($this->permission->check_label($child->menu_id)->access()) {
                                                    ?>
                                                    <?php $check = package_access($child->menu_id); ?>
                                                        <?php if($check==true) { ?>                                                       
                                                <?php if ($user_info[0]->package_id!=1 ){ ?>
                                                        <a href="<?php echo base_url(); ?><?php echo $child->page_url; ?>">
                                                            <?php echo ucwords(str_replace('_', ' ', $childmenu_title)); ?>                                                        </a>

                                                <?php } else if ($user_info[0]->package_id==1 && $bsub[0]['package_id']=='0' ) { ?>

                                                            <a href="<?php echo base_url(); ?>package">
                                                            <?php echo ucwords(str_replace('_', ' ', $childmenu_title)); ?>
                                                           </a>
                                                          
                                                <?php }else{ ?>

                                                              <a href="<?php echo base_url(); ?><?php echo $child->page_url; ?>">
                                                            <?php echo ucwords(str_replace('_', ' ', $childmenu_title)); ?>
                                                              </a>
                                                           
                                                       
                                                 <?php } ?>




                                                    <?php } ?>    
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                <?php } else {
                                    ?>
<!-- access -->

                                <?php $check = package_access($menu->menu_id); ?>

                                    <?php if($check==true) { ?>

                                        <?php if($submenu_title=='order_to_wholesaler' && $user_info[0]->package_id!=1 ){ ?> 

                                            <?php if ($user_info[0]->package_id==1) { ?>
                                               <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>" class="menu-link">
                                                    <?php echo ucwords(str_replace('_', ' ', @$submenu_title)); ?>
                                                </a>
                                            <?php } ?>
                                        <?php } else{ ?>
                                             <?php if ( $user_info[0]->package_id!=1 or $bsub[0]['package_id']!='0') { ?>
                                            <a href="<?php echo base_url(); ?><?php echo $menu->page_url; ?>" class="menu-link">
                                                <?php echo ucwords(str_replace('_', ' ', @$submenu_title)); ?>
                                            </a>
                                             <?php } ?>
                                        <?php } ?>

                                <?php } ?>


<!-- access -->


                                <?php } ?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            <?php } ?>

            <?php if ($this->session->userdata('isAdmin') == '1') { ?>
                <!--                <ul class="list-unstyled" data-link="setting">
                                    <li>
                                        <a href="<?php echo base_url(); ?>company-profile"> Company Profile Info</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>payment-setting"> Payment Option</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>retailer-cost-factor"> Cost Factor</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>iframe-code"> Web Iframe Code</a>
                                    </li>
                                    <li>
                                        <a href="#" class="menu-link"> User &amp; Access<i class="simple-icon-arrow-down"></i></a>
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="<?php echo base_url(); ?>users">Users</a>
                                                <a href="<?php echo base_url(); ?>menu-setup">Menu Setup</a>
                                                <a href="<?php echo base_url(); ?>role-permission">Role Permission</a>
                                                <a href="<?php echo base_url(); ?>role-list">Role List</a>
                                                <a href="<?php echo base_url(); ?>user-role">Add User Roles</a> 
                                                <a href="<?php echo base_url(); ?>access-role">Access Role</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>retailer-us-state"> US State</a>
                                    </li>
                                </ul>-->
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var segment = "<?php echo $this->uri->segment(1) ?>";
        var segment2 = "<?php echo $this->uri->segment(2) ?>";
        var segment3 = "<?php echo $this->uri->segment(3) ?>";
        if (segment == 'retailer-dashboard') {
            $("#dashboard").addClass("active");
        } else if (segment == 'add-customer' || segment == 'customer-edit' || segment == 'customer-view' || segment == 'customer-list' || segment == 'customer-bulk-upload' || segment == 'customer-comment-view' || segment == 'retailer-top-search-customer-order-info') {
            $("#customer").addClass("active");
        } else if (segment == 'new-quotation' || segment == 'manage-order' || segment2 == 'invoice_receipt' || segment == 'manage-invoice' || segment == 'order-cancel' || segment == 'customer-return' || segment == 'track-order' || segment2 == 'order_controller' || segment2 == 'make_payment' || segment == 'retailer-customer-order-return' || segment == 'purchase-return' || segment == 'retailer-purchase-order-return' || segment == 'retailer-order-return-detials-show' || segment2 == 'order_bulk_upload') {
            $("#order").addClass("active");
        } else if (segment2 == 'quotation_controller') {
            $("#quotation").addClass('active');
        } else if (segment == 'chart-of-account' || segment == 'account-chart' || segment == 'voucher-debit' || segment == 'voucher-credit' || segment == 'voucher-journal' || segment == 'contra-voucher' || segment == 'voucher-approval' || segment == 'cash-book' || segment == 'bank-book' || segment == 'cash-flow' || segment == 'voucher-reports' || segment == 'general-ledger' || segment == 'profit-loss' || segment == 'trial-balance' || segment == 'retailer-accounts-report-search' || segment == 'retailer-cash-flow-report-search' || segment == 'retailer-profit-loss-report-search' || segment == 'retailer-trial-balance-report' || segment == 'voucher-edit' || segment == 'retailer-vouchar-cash') {
            $("#account").addClass('active');
        } else if (segment == 'company-profile' || segment == 'payment-setting' || segment == 'menu-setup' || segment == 'retailer-cost-factor' || segment == 'iframe-code' || segment == 'users' || segment == 'add-user' || segment == 'user-edit' || segment == 'menusetup-edit' || segment2 == 'Setting_controller' || segment2 == 'setting_controller' || segment == 'retailer-gateway-edit' || segment == 'role-permission' || segment == 'role-list' || segment == 'role-edit' || segment == 'edit-user-access-role' || segment == 'user-role' || segment == 'access-role' || segment == 'retailer-us-state' || segment == 'retailer-us-state-edit' || segment == 'retailer-account' || segment == 'logs' || segment == 'my-orders' || segment == 'retailer-password-change' || segment == 'appointment-form' || segment == 'payment-gateway' || segment2 == 'notification' || segment == 'retailer-menu-setup' || segment == 'retailer-menusetup-edit' || segment2 == 'payment_log') {
            $("#settings").addClass('active');
        } else if (segment == 'retailer_gallery_image') {
            $("#gallery").addClass('active');
        } else if (segment3 == 'retailer_commission_report') {
            $("#Commission").addClass('active');
        }
    });
</script>
