<script src="//code.jquery.com/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/b_level/resources/font-awesome-4.7.0/css/font-awesome.min.css');?>">
<style>
    .hidden{
        display: none;
    }
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Attribute Info</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Add Attribute';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <?php if(isset($data->attribute_id))
                                    echo 'Edit category';
                                else
                                    echo 'Add Category';
                                ?>

                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <!-- <div class="row form-fix-width"> -->
        <div class="row form-fix-width11">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>customer/catalog/attribute/add-update-api" method="post" enctype="multipart/form-data" class="frm_update_attributes">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="category_name">Attribute Name*<span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="attribute_name"
                                           name="attribute"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($attribute->attribute_name) && !empty($attribute->attribute_name)): echo $attribute->attribute_name; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="category">Parent category</label>
                                    <select class="form-control" id="category" name="category" >
                                        <option value="0">Select category</option>
                                        <?php if(isset($parent_category) && count($parent_category) != 0) : ?>
                                        <?php foreach ($parent_category AS $key => $value) : ?>
                                        <option <?php if(isset($attribute->category_id) && $attribute->category_id == $value->category_id){ echo 'selected'; }?>  value="<?= $value->category_id ?>">
                                            <?= $value->category_name ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="position">Position*<span class="text-danger"> * </span></label>
                                    <input type="number"
                                           class="form-control"
                                           id="position"
                                           name="position"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($attribute->position) && !empty($attribute->position)): echo $attribute->position; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="attribute_type">Attribute Type</label>
                                    <select class="form-control" id="attribute_type" name="attribute_type" >
                                        <option value="">Select Type</option>
                                        <option value="1" <?=($attribute->attribute_type==1?'selected':'');?> >Text</option>
                                        <option value="2" <?=($attribute->attribute_type==2?'selected':'');?> >Option</option>
                                        <option value="3" <?=($attribute->attribute_type==3?'selected':'');?> >Multi text</option>
                                        <option value="4" <?=($attribute->attribute_type==4?'selected':'');?> >Multi option</option>
                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>


<!-- ************************************ -->         
   <?php 
                if($attribute->attribute_type==2 || $attribute->attribute_type==3 || $attribute->attribute_type== 4){
            ?>
                <div class="att_opt" style="margin-top: 10px">
                    <!-- <a style="margin-bottom: 10px" href="javascript:void(0);" class="btn btn-primary add_button "><span class="glyphicon glyphicon-plus"></span>Add Options</a> -->
                    <div class="field_wrapper">

            <?php 
                $i=0;
                foreach ($option as $ko => $op) {
                    
                    $option_option = "SELECT * FROM attr_options_option_tbl WHERE option_id = $op->att_op_id";
                    $option_option_result = $this->db->query($option_option)->result();
            ?>
                        <div class='row main_option' id='item_<?=$i?>'>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Attribute Option</label>
                                <input class='form-control' type='text' value="<?=$op->option_name?>" name='option_name[]' required>
                            </div>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Option Type</label>
                                <select class='form-control option_type' name='option_type[]' id='option_type' onchange='addOption("<?=$i?>")'>
                                    <option value='0'>--Select--</option>
                                    <option value='1' <?=($op->option_type==1?'selected':'')?>>Text</option>
                                    <option value='2' <?=($op->option_type==2?'selected':'')?>>Option</option>
                                    <option value='3' <?=($op->option_type==3?'selected':'')?>>Multi text</option>
                                    <option value='4' <?=($op->option_type==4?'selected':'')?>>Multi option</option>
                                </select>
                            </div>

                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Price Type</label>
                                <select class='form-control'  name='price_type[]'  >
                                    <option value='1' <?=($op->price_type==1?'selected':'')?>>$</option>
                                    <option value='2' <?=($op->price_type==2?'selected':'')?>>%</option>
                                </select>
                            </div>
                            <div class='form-group col-sm-2'>
                                <label for='attr_option' class='mb-2'>Price </label>
                                <input class='form-control' type='text' value="<?=$op->price;?>" name='price[]' >
                            </div>
                            <div class='form-group col-sm-3'>
                                <label for='attr_option' class='mb-2'> Option condition </label>
                                <input class='form-control' type='text' value="<?=$op->op_condition;?>" name='condition[]' >
                            </div> 
                            <input type="hidden" name="option_id[]" value="<?=$op->att_op_id?>" class="form-control">
                            <div class='form-group col-sm-1'>
                                <label for='attr_option' class='mb-2'> </label>
                                <button  class='btn btn-danger delete_old_option' style='margin-top:20px;' data-record_id='<?=$op->att_op_id?>'><i class='glyph-icon simple-icon-trash'></i></button>
                            </div>

                            <!-- File upload : START -->
                            <div class='form-group col-sm-4 img_div'>
                                <label for='product_img' class='mb-2'>File Upload</label>
                                <input type='file' class='form-control attr_img' name='attr_img[]'>
                                <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                <input type="hidden" class="form-control" name="hid_attr_img[]" value="<?= @$op->attributes_images; ?>">
                            </div>
                            <div class='form-group col-sm-2 prev_img_div'>
                                <img src="<?php if($op->attributes_images != '') {   
                                    echo base_url().$op->attributes_images; 
                                } else { 
                                    echo base_url('assets/no-image.png');
                                }?>"  class='cls_prev_img' width="100px" height="100px">
                            </div>
                            <!-- File upload : END -->

                            <div class='col-md-12'>

                            <?php 
                            $k =0;
                            foreach ($option_option_result as $kk => $opop) {

                                $op_op_op = "SELECT * FROM attr_options_option_option_tbl WHERE att_op_op_id = $opop->op_op_id";
                                $op_op_op_result = $this->db->query($op_op_op)->result();

                            ?>
                                <div class='row sub_option' id="<?=$ko."_".$kk?>">
                                    
                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Attribute Sub Option</label>
                                        <input class='form-control' type='text' value="<?=$opop->op_op_name?>" name='option_option_name[<?=$i?>][]' required>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub option Type</label>
                                        <select class='form-control' name='option_option_type[<?=$i?>][]'>
                                            <option value='0'>--Select--</option>
                                            <option value='1' <?=($opop->type==1?'selected':'')?>> Text</option>
                                            <option value='2' <?=($opop->type==2?'selected':'')?> > Option</option>
                                            <option value='3' <?=($opop->type==3?'selected':'')?> > Multi text</option>
                                            <option value='4' <?=($opop->type==4?'selected':'')?> > Multi option</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub Price Type</label>
                                        <select class='form-control' name='op_op_price_type[<?=$i?>][]'  >
                                            <option value='1' <?=($opop->att_op_op_price_type==1?'selected':'')?>>$</option>
                                            <option value='2' <?=($opop->att_op_op_price_type==2?'selected':'')?>>%</option>
                                        </select>
                                    </div>

                                    <div class='form-group col-sm-2'>
                                        <label for='attr_option' class='mb-2'>Sub Option Price </label>
                                        <input class='form-control' type='text' value="<?=$opop->att_op_op_price;?>" name='op_op_price[<?=$i?>][]' >
                                    </div>

                                    <div class='form-group col-sm-3'>
                                        <label for='attr_option' class='mb-2'>Sub Option condition </label>
                                        <input class='form-control' type='text' value="<?=$opop->att_op_op_condition;?>" name='op_op_condition[<?=$i?>][]' >
                                    </div> 
                                    <input type="hidden" name="op_op_id[<?=$i?>][]" value="<?=$opop->op_op_id?>" class="form-control">
                                    <div class='form-group col-sm-1'>
                                        <label for='attr_option' class='mb-2'> </label>
                                        <button  class='btn btn-danger delete_old_option_option' style='margin-top:20px;' data-record_id='<?=$opop->op_op_id?>'><i class='glyph-icon simple-icon-trash'></i></button>
                                    </div>

                                    <!-- File upload : START -->
                                    <div class='form-group col-sm-4 img_div'>
                                        <label for='product_img' class='mb-2'>File Upload</label>
                                        <input type='file' class='form-control attr_img' name='op_op_attr_img[<?=$i?>][]'>
                                        <input type="hidden" class="form-control" name="hid_op_op_attr_img[<?=$i?>][]" value="<?= @$opop->att_op_op_images; ?>">
                                        <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                    </div>
                                    <div class='form-group col-sm-2 prev_img_div'>
                                        <img src="<?php if($opop->att_op_op_images != '') {   
                                            echo base_url().$opop->att_op_op_images; 
                                            } else { 
                                            echo base_url('assets/no-image.png');
                                            }?>"  class='cls_prev_img' width="100px" height="100px">
                                    </div>
                                    <!-- File upload : END -->

                                    <div class='col-md-12'>
                                        <?php 
                                            $j =0;
                                            foreach ($op_op_op_result as $kkk => $opopop) {
                                        ?>

                                                <div class='row sub_sub_option' id="<?=$ko."_".$kk."_".$kkk?>">
                                                    
                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Attribute Sub-Sub Option</label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_name?>" name='op_op_op_name[<?=$i?>][<?=$k?>][]' required>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub option Type</label>
                                                        <select class='form-control' name='op_op_op_type[<?=$i?>][<?=$k?>][]'>
                                                            <option value='0'>--Select--</option>
                                                            <option value='1' <?=($opopop->att_op_op_op_type==1?'selected':'')?>> Text </option>
                                                            <option value='2' <?=($opopop->att_op_op_op_type==2?'selected':'')?> > Option </option>
                                                            <option value='3' <?=($opopop->att_op_op_op_type==3?'selected':'')?> > Multi text </option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Price Type</label>
                                                        <select class='form-control'  name='op_op_op_price_type[<?=$i?>][<?=$k?>][]'  >
                                                            <option value='1' <?=($opopop->att_op_op_op_price_type==1?'selected':'')?>>$</option>
                                                            <option value='2' <?=($opopop->att_op_op_op_price_type==2?'selected':'')?>>%</option>
                                                        </select>
                                                    </div>

                                                    <div class='form-group col-sm-2'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub Option Price </label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_price;?>" name='op_op_op_price[<?=$i?>][<?=$k?>][]' >
                                                    </div>

                                                    <div class='form-group col-sm-3'>
                                                        <label for='attr_option' class='mb-2'>Sub-Sub condition </label>
                                                        <input class='form-control' type='text' value="<?=$opopop->att_op_op_op_condition;?>" name='op_op_op_condition[<?=$i?>][<?=$k?>][]' >
                                                    </div> 

                                                    <input type="hidden" name="op_op_op_id[<?=$i?>][<?=$k?>][]" value="<?=$opopop->att_op_op_op_id?>" class="form-control">
                                                    <div class='form-group col-sm-1'>
                                                        <label for='attr_option' class='mb-2'> </label>
                                                        <button  class='btn btn-danger delete_old_sub_option_option' style='margin-top:20px;' data-record_id='<?=$opopop->att_op_op_op_id?>'><i class='glyph-icon simple-icon-trash'></i></button>
                                                    </div>
                                                    <!-- File upload : START -->
                                                    <div class='form-group col-sm-4 img_div'>
                                                        <label for='product_img' class='mb-2'>File Upload</label>
                                                        <input type='file' class='form-control attr_img' name='op_op_op_attr_img[<?=$i?>][<?=$k?>][]'>
                                                        <p>Extension: jpg|jpeg|png. File size: 2MB</p>
                                                        <input type="hidden" class="form-control" name="hid_op_op_op_attr_img[<?=$i?>][<?=$k?>][]" value="<?= @$opopop->att_op_op_op_images; ?>">
                                                    </div>
                                                    <div class='form-group col-sm-2 prev_img_div'>
                                                        <img src="<?php if($opopop->att_op_op_op_images != '') {   
                                                            echo base_url().$opopop->att_op_op_op_images; 
                                                        } else { 
                                                            echo base_url('assets/no-image.png');
                                                        }?>"  class='cls_prev_img' width="100px" height="100px">
                                                    </div>
                                                    <!-- File upload : END -->
                                                </div>

                                        <?php $j++; } ?>

                                    </div>
                                    
                                </div>

                                <?php $k++; } ?>

                            </div>

                        </div>

                    <?php $i++; } ?>

                    </div>
                </div>
            <?php  } ?>                      
<!-- ************************************ -->
                                <div class="att_opt hidden col-12">
                                    <a href="javascript:void(0);" class="btn btn-primary add_button mb-3"><span class="glyphicon glyphicon-plus"></span>Add Options</a>
                                    <div class="field_wrapper">
                                    </div>
                                </div>
                                
                            

                            </div>
                            <input type="hidden" name="attribute_id" value="<?php if(isset($attribute->attribute_id) && !empty($attribute->attribute_id)){ echo $attribute->attribute_id; }else{ echo 0;}?>"> 
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn <?php if(isset($attribute->attribute_id)) { echo "update_attr_btn"; } ?>">
                                <?php if(isset($attribute->attribute_id))
                                    echo 'Update';
                                else
                                    echo 'Add';
                                ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

<?php if(!isset($attribute->attribute_id)){ 
    $this->load->view('b_level/attributes/att_add_js');
}?>



<?php if(!isset($attribute->attribute_id)){ ?>
<script>
    $('button[type=submit]').prop('disabled', true);
</script>
<?php } ?>
<script type="text/javascript">


    function required_validation() {

        if ($("#category_name").val() != '') {

            $('button[type=submit]').prop('disabled', false);

        }
        else {
            $('button[type=submit]').prop('disabled', true);
        }
    }


        $(document).ready(function(){

            var wrapper = $('.field_wrapper');

            // For Delete Stored database data : START
            $(wrapper).on('click', '.delete_old_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('c_level/Catalog_controller/remove_main_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });

            $(wrapper).on('click', '.delete_old_option_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('c_level/Catalog_controller/remove_sub_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });

            $(wrapper).on('click', '.delete_old_sub_option_option', function(e){ 
                e.preventDefault();
                var result = confirm("Are you sure you want to delete?");
                var option_id = $(this).attr('data-record_id');
                var _this = $(this);
                if (result && option_id != '') {
                     $.ajax({
                        url: "<?php echo base_url('c_level/Catalog_controller/remove_sub_sub_option'); ?>",
                        type: 'post',
                        data: {"option_id":option_id},
                        success: function (r) {
                            $(_this).parent().parent().remove(); 
                        }
                    });
                }
            });
            // For Delete Stored database data : END

        });

        $("body").on("change", ".img_div .attr_img", function(e) {
       
            if ($(this).val() != '') {
                var file = (this.files[0].name);
                var size = (this.files[0].size);
                var ext = file.substr((file.lastIndexOf('.') + 1));
                // check extention
                if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                    swal("Please upload file jpg | jpeg | png types are allowed. Thanks!!", {
                        icon: "error",
                    });
                    $(this).val('');
                }
                // chec size
                if (size > 2000000) {
                    swal("Please upload file less than 2MB. Thanks!!", {
                        icon: "error",
                    });
                    $(this).val('');
                }
            }    

            filePreview(this);
        });

        function filePreview(input) {
            if ($(input).val() != '' && input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src',e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }else{
                $(input).parent('.img_div').next('.prev_img_div').children('.cls_prev_img').attr('src','<?=base_url('assets/no-image.png')?>');
            }
        }


        function rename_option_name(){
            $(".main_option").each(function(key_main_op,main_option_html) {
                // Rename main option name
                var main_option_id = $(main_option_html).attr('id');

                $("#"+ main_option_id + " .sub_option").each(function(key_sub_op,sub_option_html) {
                    var sub_option_id = $(sub_option_html).attr('id');
                    // Rename sub option name : START
                    $("#"+ sub_option_id +" .form-control").each(function(key_sub_op_k,sub_option_html_k) {
                        if(key_sub_op_k == 0){
                            var namee = 'option_option_name[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 1){
                            var namee = 'option_option_type[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 2){
                            var namee = 'op_op_price_type[' + key_main_op +']'+'['+key_sub_op+']';    
                        }else if(key_sub_op_k == 3){
                            var namee = 'op_op_price[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 4){
                            var namee = 'op_op_condition[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 5){
                            var namee = 'op_op_id[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 6){
                            var namee = 'op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']';
                        }else if(key_sub_op_k == 7){
                            var namee = 'hid_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']';
                        }else{
                            return false; 
                        }

                        $(this).attr('name',namee);
                    });
                    // Rename sub option name : END

                    $("#"+ main_option_id + " #"+ sub_option_id + " .sub_sub_option").each(function(key_sub_sub_op,sub_sub_option_html) {
                        var sub_sub_option_id = $(sub_sub_option_html).attr('id');
                        // Rename sub sub option name : START                   
                        $("#"+ sub_sub_option_id +" .form-control").each(function(key_sub_sub_op_k,sub_sub_option_html_k) {
                            if(key_sub_sub_op_k == 0){
                                var namee = 'op_op_op_name[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 1){
                                var namee = 'op_op_op_type[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 2){
                                var namee = 'op_op_op_price_type[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';    
                            }else if(key_sub_sub_op_k == 3){
                                var namee = 'op_op_op_price[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 4){
                                var namee = 'op_op_op_condition[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 5){
                                var namee = 'op_op_op_id[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 6){
                                var namee = 'op_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }else if(key_sub_sub_op_k == 7){
                                var namee = 'hid_op_op_op_attr_img[' + key_main_op +']'+'['+key_sub_op+']'+'['+key_sub_sub_op+']';
                            }
                            $(this).attr('name',namee);
                        });
                        // Rename sub sub option name : END
                        
                    });

                });
            });

            $(".frm_update_attributes").submit();
        }

        $('.update_attr_btn').on("click",function(e){ 
            e.preventDefault();
            rename_option_name();
        });

</script>