<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Group Price Info</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Group Price Info';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Add Group Price</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">



                        <div class="col-md-12">

            <div class="alert alert-warning">
                <strong>Notes! </strong> 1. Do not use any text. 2. Please use (') single quote twice to show inch symbol. ex: 60''
            </div>

            <!--<div class="">
                    <select class="form-control select2" name="" id="style_id" onchange="editTable()" data-placeholder="-- select one --">
                        <option value=""></option>
            <?php foreach ($style_slist as $s) { ?>
                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
            <?php } ?>
                    </select>
                </div> -->

            <!--            <div class="btn-group">
            <?php foreach ($style_slist as $s) { ?>
                                                <a  href="javascript:void" class="btn btn-primary btn-sm" onclick="editTable('<?php echo $s->style_id ?>')"><?= $s->style_name ?></a>
            <?php } ?>
                        </div>-->
        </div>




                <div class="col-sm-12">
                    <div id="exdata" class="mb-3">
                        <p>Paste excel data here:</p>
                        <textarea name="excel_data" style="width:100%;height:150px;" onblur="javascript:generateTable()"></textarea>
                    </div>



                        <form action="<?php echo base_url(); ?>customer/catalog/group-price/save" method="post">
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label">Price Style name</label>
                                    <input type="text" name="price_style_name" id="" class="form-control" placeholder="Enter style name" required onkeyup="required_validation()">
                                    <input type="hidden" name="style_type" class="form-control style_type" value="4">
                                </div>


                                <div class="form-group col-md-12">
                                    <label for="productquantity" class="col-form-label">Price Sheet</label>
                                    <div id="excel_table"></div>
                                </div>


                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn"> Add </button>
                                </div>
                            </div>
                            
                           
                        </form>
                    </div>



                    <!-- <p>The .table-bordered class adds borders to a table:</p>             -->

                    
<!--             <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Edit Price by</th>
                        <th>Apply to</th>
                        <th>Value</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>

                <tbody>

                    <tr>

                        <td>
                            <div class="form-check mb-1">
                              <input class="form-check-input" type="radio" name="optradio" id="percentage">
                              <label class="form-check-label" for="percentage">
                                Percentage
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="optradio" id="fixed">
                              <label class="form-check-label" for="fixed">
                                Fixed
                              </label>
                            </div>
                        </td>

                        <td>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="apply" id="whole">
                              <label class="form-check-label" for="fixed">
                                Whole table
                              </label>
                            </div>
                        </td>

                        <td>
                            <label class="mb-1">
                                Enter value
                            </label>

                            <input type="number" name="val" id="setVal" class="form-control" min="0">

                        </td>

                        <td class="text-right">
                            <button class="btn btn-sm btn-success" onclick="plusData()" data-toggle="tooltip" data-placement="top" data-original-title="Add">+</button>
                            <button class="btn btn-sm btn-danger" onclick="minusData()" data-toggle="tooltip" data-placement="top" data-original-title="Remove">-</button>

                        </td>

                    </tr>

                </tbody>
            </table>
            </div>
 -->



                </div>
            </div>

        </div>
    </div>
</main>
<?php $this->load->view('b_level/pricemodel/_price_style_js') ;?>

