<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Color Info</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Add Color';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <?php if(isset($data->category_id))
                                    echo 'Edit color';
                                else
                                    echo 'Add color';
                                ?>

                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>customer/catalog/color/add-update-api" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="color_name">Color Name*<span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="color_name"
                                           name="color_name"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->color_name) && !empty($data->color_name)): echo $data->color_name; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="color_number">Color Number*<span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="color_number"
                                           name="color_number"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->color_number) && !empty($data->color_number)): echo $data->color_number; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="parent_category">Pattern</label>
                                    <select class="form-control" id="pattern_id" name="pattern_id" >
                                        <?php if(isset($patterns) && count($patterns) != 0) : ?>
                                            <?php foreach ($patterns AS $key => $value) : ?>
                                                <option <?php if(isset($data->pattern_id) && $data->pattern_id == $value->pattern_model_id){ echo 'selected'; }?>  value="<?= $value->pattern_model_id; ?>">
                                                    <?= $value->pattern_name ?>
                                                </option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label for="color_img" class="mb-2">File Upload</label>
                                            <input type="file" class="form-control" name="color_img" id="color_img">
                                            <p>Extension: jpg|jpeg|png. <br> File size: 2MB</p>
                                            <input type="hidden" name="hid_color_img" value="<?= @$data->color_img ?>">
                                        </div>
                                        <div class="col-sm-4">
                                            <img src="<?php if($data->color_img != '') {   
                                                echo base_url().$data->color_img; 
                                                } else { 
                                                echo base_url('assets/no-image.png');
                                                }?> ?>" id="prev_color_img" width="100px" height="100px">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <input type="hidden" name="color_id" value="<?php if(isset($data->id) && !empty($data->id)){ echo $data->id; }else{ echo 0;}?>">
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">
                                <?php if(isset($data->id))
                                    echo 'Update';
                                else
                                    echo 'Add';
                                ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
<?php if(!isset($data->id)){ ?>
    <script>
        $('button[type=submit]').prop('disabled', true);
    </script>
<?php } ?>
<script type="text/javascript">

    $("body").on("change", "#color_img", function(e) {
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                Swal.fire(
                "Please upload file jpg | jpeg | png types are allowed. Thanks!!");
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                Swal.fire("Please upload file less than 2MB. Thanks!!");
                $(this).val('');
            }
        }    
        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev_color_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#prev_color_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }

    function required_validation() {

        if ($("#color_name").val() != '' && $("#color_number").val() != '') {

            $('button[type=submit]').prop('disabled', false);

        }
        else {
            $('button[type=submit]').prop('disabled', true);
        }
    }





</script>