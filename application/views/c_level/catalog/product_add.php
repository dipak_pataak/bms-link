<script src="//code.jquery.com/jquery.min.js"></script>
<script src="http://live-bms-link.ti/assets/c_level/resources/js/bootstrap-datepicker.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Product Info</h1>
                    <?php
                        $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Add Product';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                        ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Add Product
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    } ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>customer/catalog/product/save" method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_name" class="mb-2">Product Name (Model Name)</label>
                                        <input class="form-control" type="text" name="product_name" placeholder="Product Name" id="product_name" onmouseout="replaceString(this.value)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="category_id">Select category</label>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            <option value="0">Select category</option>
                                            <?php if(isset($get_category) && count($get_category) != 0) : ?>
                                            <?php foreach ($get_category AS $key => $value) : ?>
                                            <option value="<?= $value->category_id ?>"><?= $value->category_name ?></option>
                                            <?php endforeach;?>
                                            <?php endif;?>
                                        </select>
                                        <div class="valid-tooltip">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="form-group" id="subcategory_id">
                                    </div>
                                    <div class="form-group">
                                        <label for="pattern_model_id" class="mb-2">Pattern</label> <!--<a href="#">add new</a>-->
                                        <div class="pattern_model_id">
                                            <select class="selectpicker form-control" id="pattern_model_id" name="pattern_model_id[]" multiple data-live-search="true" required>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="theSelect" class="mb-2">Price Model/Style</label>
                                        <select name="price_style_type" class="form-control price_select select2" id="theSelect" onchange="price_model_wise_style(this.value)" data-placeholder="-- select one --" required>
                                            <option value=""></option>
                                            <option value="3">Fixed product Price Style</option>
                                            <option value="4">Group style</option>
                                            <option value="2">Price by Sq.ft style </option>
                                            <option value="1" selected>Row/Column style</option>
                                            <option value="5">Sqm price style</option>
                                        </select>
                                    </div>
                                    <div class="hidden isnull_price mb-3"></div>
                                    <div class="mb-3 is1 hidden">
                                        <div id="ss_pp">
                                            <select name="price_rowcol_style_id" id="price_rowcol_style_id" onchange="setValue(this.value)" class="form-control select2" data-placeholder="-- select style --" required>
                                                <?php foreach ($style_slist as $s) { ?>
                                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="price_style_id" id="price_style_id">
                                            <!-- <a href="#" data-toggle="modal" data-target="#myModal"  class="btn btn-success btn-xs" style="margin-top: 8px;">add new price style</a> -->
                                        </div>
                                        <br>
                                        <label for="Row_Column" class="mb-2">Row Column Style</label>
                                        <div id="priceseet"></div>
                                    </div>
                                    <div class="hidden is2 mb-3">
                                        <label for="sqft" class="mb-2">SqFt. Style</label>
                                        <input type="text" name="sqft_price" class="form-control" placeholder="$80/sq.ft">
                                    </div>
                                    <div class="hidden is3 mb-3">
                                        <label for="fixedprice" class="mb-2">Fixed Price Style</label>
                                        <input type="text" name="fixed_price" class="form-control" placeholder="$29">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group product_code_div">
                                        <label for="color_id" class="mb-2" >Color :</label>
                                        <div class="">
                                            <select class="selectpicker form-control select-all" id="color_id" name="color_id[]" multiple data-live-search="true" required="" data-selected-text-format="count>2">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_code" class="mb-2">Status</label>
                                        <select name="status" class="form-control select2" data-placeholder="-- select one --" required>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label for="product_code" class="mb-2">Default Discount (%)</label>
                                        <input class="form-control" min="0" name="dealer_price" type="number" placeholder="20%" value="" required>
                                        </div> -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <label for="cost_factor" class="mb-2">Cost Factor</label>
                                                <input type="number" name="dealer_price" id="dealer_price" class="form-control NumbersAndDot" onkeyup="cost_factor_calculation(this.value)" onblur="cost_factor_calculation(this.value)" min="0.1" max="2.0" step="0.001" autocomplete="off" required value="1">
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="individual_price" class="mb-2">Discount (%)</label>
                                                <input type="text" name="individual_price" id="individual_price" class="form-control" readonly value="0.00">
                                            </div>
                                        </div>        
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <label for="product_img" class="mb-2">File Upload</label>
                                                <input type="file" class="form-control" name="product_img" id="product_img">
                                                <p>Extension: jpg|jpeg|png. <br> File size: 2MB</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <img src="<?=base_url('assets/no-image.png')?>" id="prev_product_img" width="100px" height="100px">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="radio" name="is_taxable" value="1"> &nbsp; Taxable
                                        <input type="radio" name="is_taxable" value="0" checked> &nbsp; Nontaxable
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="product_description" class="mb-2">Description</label>
                                        <textarea class="form-control" name="product_description" id="product_description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success w-md m-b-5">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">

    $(document).ready(function(){
        CKEDITOR.replace('product_description');
    })

    function cost_factor_calculation(item) {
        var cost_factor = $("#dealer_price").val();
        var discount = 0;
        if(cost_factor != ''){
            cost_factor = (cost_factor*100).toFixed(2); 
            discount = (100 - (cost_factor));
        }
        $("#individual_price").val(discount.toFixed(2));
    }

    $("body").on("keypress keyup blur",".NumbersAndDot",function (event) {
      $(this).val($(this).val().replace(/[^0-9\.]/g,''));
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
    });

    $("body").on("change", "#product_img", function(e) {
        if ($(this).val() != '') {
            var file = (this.files[0].name);
            var size = (this.files[0].size);
            var ext = file.substr((file.lastIndexOf('.') + 1));
            // check extention
            if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG') {
                Swal.fire(
                "Please upload file jpg | jpeg | png types are allowed. Thanks!!");
                $(this).val('');
            }
            // chec size
            if (size > 2000000) {
                Swal.fire("Please upload file less than 2MB. Thanks!!");
                $(this).val('');
            }
        }    

        filePreview(this);
    });

    function filePreview(input) {
        if ($(input).val() != '' && input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#prev_product_img').attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }else{
            $('#prev_product_img').attr('src','<?=base_url('assets/no-image.png')?>');
        }
    }


    function replaceString(v) {
        $('#product_name').val((v.replace(/['"]+/g, '&quot;')));
    }


    //    ============ its for price_model_wise_style =============
    
    function price_model_wise_style(id) {

        $.ajax({
            url: "<?php echo base_url(); ?>c_level/catalog_controller/price_model_wise_style",
            type: "post",
            data: {id: id},
            success: function (r) {

                    r = JSON.parse(r);
                    $("#price_rowcol_style_id").empty();
                    // $("#price_rowcol_style_id").html("<option value=''>-- select one -- </option>");

                    $.each(r, function (ar, typeval) {
                        $('#price_rowcol_style_id').append($('<option>').text(typeval.style_name).attr('value', typeval.style_id));
                    });
                }

        });
    }



    $(document).ready(function () {
        $('select.selectpicker').on('change', function(){
            var selected = $('.selectpicker option:selected').val();
        });

        $("#theSelect").change();
        $('#price_rowcol_style_id').change();
  
        // ================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>retailer-category-wise-subcategory/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().removeClass("hidden");
                        $('.select2').select2();
                    } else {
                        $("#subcategory_id").html(r);
                        $('#subcategory_id').slideDown().addClass("hidden");
                    }
                }
            });
            //========== its for category wise condition ===========
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/catalog_controller/category_wise_condition/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $(".condition_checkbox").empty();
                        $(".condition_checkbox").html(r);
                    }
                }
            });
            //  ============= its for category-wise-pattern ==============

            $.ajax({
                url: "<?php echo base_url(); ?>c_level/catalog_controller/category_wise_pattern/" + category_id,
                type: 'get',
                success: function (r) {
                    if (r !== '') {
                        $(".pattern_model_id").html(r);
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $('.selectpicker').selectpicker();
                    } else {
                        $(".pattern_model_id").html("Not Found!");
                        $("#color_id").html(''); 
                        $(".product_code_div").hide();
                        $(".pattern_model_id").css({'color': 'red'});
                    }
                }
            });
        });

        $('.is1').removeClass("hidden");

    });



    $("#theSelect").change(function () {

        var value = $("#theSelect option:selected").val();
        var theDiv = $(".is" + value);
        if(value==='4' || value==='5'){
            $('.is1').slideUp().addClass("hidden");
            $('.is2').slideUp().addClass("hidden");
            $('.is3').slideUp().addClass("hidden");
        }

        theDiv.slideDown().removeClass("hidden");

        theDiv.siblings('[class*=is]').slideUp(function () {

            $(this).addClass("hidden");

        });

    });

    // $("#attrSelect").change(function () {

    //     var value = $("#attrSelect option:selected").val();
    //     var theDiv = $(".is" + value);

    //     theDiv.slideDown().removeClass("hidden");
    //     theDiv.siblings('[class*=is]').slideUp(function () {
    //         $(this).addClass("hidden");
    //     });
    // });


    $("#attrSelect").change(function () {

        var attr_type_id = $("#attrSelect option:selected").val();

        $.ajax({
            url: "<?php base_url() ?>c_level/Catalog_controller/get_attr_by_attr_types/" + attr_type_id,
            type: 'get',
            success: function (r) {
                $("#iscolor").html(r);
                $('#iscolor').slideDown().removeClass("hidden");
            }
        });


    });


    function shuffle_pricebox(elem) {
        if (!document.getElementById(elem).disabled)
            document.getElementById(elem).disabled = true;
        else
            document.getElementById(elem).disabled = false;
    }

</script>

<?php
$this->load->view($js)?>

<script type="text/javascript">

    $("form").on('change','#pattern_model_id',function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length > 0){
            
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/catalog_controller/get_colors_based_on_pattern_ids",
                type: 'post',
                data: {"pattern_ids":pattern_ids},
                success: function (r) {
                    if (r !== '') {
                        var color_data = JSON.parse(r);
                        var color_option = '';
                        $.each(color_data, function(index, item) {
                            color_option += '<option value="'+item.id+'">'+item.color_name+'->('+item.color_number +')';
                        });
                        var select_all = '';
                        if(color_option != ''){
                            select_all = '<option value="[all]" class="select-all">Select All</option>';
                            $("#color_id").html(select_all + color_option); 
                            $('#color_id').selectpicker('refresh');
                            // $('#color_id option').attr("selected","selected");
                            $('a.select-all').click();
                        }else{
                            $("#color_id").html('');
                            $('#color_id').selectpicker('refresh'); 
                        }
                        $(".product_code_div").show();
                    } else {
                        $("#color_id").html(''); 
                        $('#color_id').selectpicker('refresh');
                        $(".product_code_div").hide();
                    }
                }

            });

        }else{
            $("#color_id").html(''); 
            $('#color_id').selectpicker('refresh');
            $(".product_code_div").hide();
        }
    })

    $(document).ready(function(){
        var pattern_ids = $("#pattern_model_id").val();
        if(pattern_ids.length == 0){
            $(".product_code_div").hide();
        }
    });
</script>