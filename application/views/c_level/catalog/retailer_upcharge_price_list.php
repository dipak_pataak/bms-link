<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }

    .or_cls {
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    td.attr_td_section, td.wholesaler_td_section, td.retailer_td_section{
        padding: 0px;
    }
    td.attr_td_section table, td.wholesaler_td_section table, td.retailer_td_section table{
        margin-bottom: 0px;
    }
    td.attr_td_section .table td, td.wholesaler_td_section .table td, td.retailer_td_section .table td{
        text-align: left;
        border : none;
        border-bottom: 1px solid #dee2e6;
    }
    td.attr_td_section .table tbody tr:hover,
    td.wholesaler_td_section .table tbody tr:hover,
    td.retailer_td_section .table tbody tr:hover{
        background-color: transparent;
    }
    td.attr_td_section .table td.attr_option{
        padding-left: 10px;
    }
    td.attr_td_section .table td.attr_op_option{
        padding-left: 30px;
    }
    td.attr_td_section .table td.attr_op_op_option{
        padding-left: 50px;
    }
    td.attr_td_section .table td.attr_op_op_op_option{
        padding-left: 70px;
    }
    .retailer_attr_table .retailer_price_type, .retailer_attr_table .retailer_price_text{
        height: 29px;
    }
    .retailer_attr_table td{
        height: 43px;
        padding:7px;
    }
    .retailer_attr_table td .retailer_price_type{
        width: 40px;
    }

    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }

    @media only screen and (max-width: 580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }

        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
    <?php
        $user_data=access_role_permission(81);
        $log_data=employ_role_access($id);
                                            
     ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Wholesaler to Retailer Upcharges</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Wholesaler to Retailer Upcharges';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Wholesaler to Retailer Upcharges</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <form class="form-horizontal" action="<?php echo base_url('retailer-upcharge-price'); ?>" id="AttributeFilterFrm" method="post">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control attribute_name" name="attribute_name" placeholder="Attribute Name" value="<?=$search->attribute_name?>">
                            </div>
                            <div class="or_cls">-- OR --</div>
                            <div class="col-sm-2">
                                <select name="category_id" class="form-control select2 category_id" data-placeholder="-- select category --" id="category_id">
                                    <option value="">Select Category</option>
                                    <?php foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>".$category->category_name."</option>";
                                    }?>
                                </select>
                                <script>
                                    $("#category_id").val('<?=$search->category_id?>');
                                </script>    
                            </div>
                            <div class="col-sm-3 text-left">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <a href="<?php echo base_url('retailer-upcharge-price'); ?>" class="btn btn-sm btn-danger default" name="All" value="All">Reset</a>
                                </div>
                            </div>

                        </div>

                    </fieldset>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    } ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('retailer-upcharge-price-save') ?>">
                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>Serial No.</th>
                                        <th>Category Name</th>
                                        <th>Attribute Name</th>
                                        <th>Option</th>
                                        <th>Wholesaler Price</th>
                                        <th colspan="2">Retailer Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($get_attribute as $value) {
                                        $sl++; ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?= $value->category_name; ?></td>
                                            <td><?= $value->attribute_name; ?></td>
                                            <td class="attr_td_section">
                                                <?php
                                                $oprion_html = "";
                                                $wholesaler_html = "";
                                                $retailer_html = "";
                                                $user_id = $this->session->userdata('user_id');
                                                if ($value->attribute_type)
                                                {
                                                    $option = "SELECT * FROM attr_options WHERE attribute_id = $value->attribute_id";
                                                    $option_result = $this->db->query($option)->result();
                                                    if (count($option_result) != 0) {
                                                        $oprion_html .= "<table class='table custom_attr_table'>";
                                                        $wholesaler_html .= "<table class='table wholesaler_attr_table'>";
                                                        $retailer_html .= "<table class='table retailer_attr_table'>";
                                                        foreach ($option_result as $key => $op) {
                                                            $oprion_html .= "<tr><td class='attr_option'>" . $op->option_name . "</td></tr>";

                                                            // For Wholesaler Price : START
                                                            if($op->price_type == 1){
                                                                $wh_price = "$".$op->price;
                                                            }else{
                                                                $wh_price = $op->price."%";
                                                            }
                                                            $wholesaler_html .= "<tr><td class='wholesaler_price'>" . $wh_price . "</td></tr>";
                                                            // For Wholesaler Price : END

                                                            //Get retailer upcharge : START
                                                            $ret_attr_query = "SELECT * FROM ret_attr_options WHERE att_op_id = $op->att_op_id AND created_by = $user_id LIMIT 1";
                                                            $ret_attr_result = $this->db->query($ret_attr_query)->row();
                                                            //Get retailer upcharge : END

                                                            // For Retailer price : START
                                                            $select_dollar = '';
                                                            $select_per = '';
                                                            if(@$ret_attr_result->retailer_price_type == 2){
                                                                $select_per = 'selected';
                                                            }else{
                                                                $select_dollar = 'selected';
                                                            }

                                                            $retailer_html .= "<tr><td class='wholesaler_price_type'>";
                                                            $retailer_html .= "<select name='retailer_price_type[]' class='retailer_price_type'>";
                                                            $retailer_html .= "<option value='1' ".$select_dollar.">$</option>";
                                                            $retailer_html .= "<option value='2' ".$select_per.">%</option>";
                                                            $retailer_html .= "</select>";
                                                            $retailer_html .= "</td>";

                                                            $retailer_html .= "<td class='retailer_price'><input type='text' name='retailer_price[]' class='retailer_price_text' value='".@$ret_attr_result->retailer_price."'>";
                                                            $retailer_html .= "<input type='hidden' name='hid_att_op_id[]' value='".$op->att_op_id."'><input type='hidden' name='hid_retailer_att_op_id[]' value='".@$ret_attr_result->retailer_att_op_id."'></td></tr>";
                                                            // For Retailer price : END

                                                            $option_option_result = $this->db->where('option_id', $op->att_op_id)->get('attr_options_option_tbl')->result();


                                                            foreach ($option_option_result as $key => $opop) {
                                                                $opopops = $this->db->where('att_op_op_id', $opop->op_op_id)->get('attr_options_option_option_tbl')->result();

                                                                $oprion_html .= "<tr><td class='attr_op_option'>" . $opop->op_op_name . "</td></tr>";

                                                                // For Wholesaler Price : START
                                                                if($opop->att_op_op_price_type == 1){
                                                                    $wh_price = "$".$opop->att_op_op_price;
                                                                }else{
                                                                    $wh_price = $opop->att_op_op_price."%";
                                                                }
                                                                $wholesaler_html .= "<tr><td class='wholesaler_price'>" . $wh_price . "</td></tr>";
                                                                // For Wholesaler Price : END

                                                                //Get retailer upcharge : START
                                                                $ret_attr_query = "SELECT * FROM ret_attr_options_option_tbl WHERE op_op_id = $opop->op_op_id AND created_by = $user_id LIMIT 1";
                                                                $ret_attr_result = $this->db->query($ret_attr_query)->row();
                                                                //Get retailer upcharge : END

                                                                // For Retailer price : START
                                                                $select_dollar = '';
                                                                $select_per = '';
                                                                if(@$ret_attr_result->retailer_att_op_op_price_type == 2){
                                                                    $select_per = 'selected';
                                                                }else{
                                                                    $select_dollar = 'selected';
                                                                }

                                                                $retailer_html .= "<tr><td class='wholesaler_price_type'>";
                                                                $retailer_html .= "<select name='retailer_att_op_op_price_type[]' class='retailer_price_type '>";
                                                                $retailer_html .= "<option value='1' ".$select_dollar.">$</option>";
                                                                $retailer_html .= "<option value='2' ".$select_per.">%</option>";
                                                                $retailer_html .= "</select>";
                                                                $retailer_html .= "</td>";

                                                                $retailer_html .= "<td class='retailer_price'><input type='text' name='retailer_att_op_op_price[]' class='retailer_price_text' value='".@$ret_attr_result->retailer_att_op_op_price."'>";
                                                                $retailer_html .= "<input type='hidden' name='hid_op_op_id[]' value='".$opop->op_op_id."'><input type='hidden' name='hid_retailer_op_op_id[]' value='".@$ret_attr_result->retailer_op_op_id."'></td></tr>";
                                                                // For Retailer price : END

                                                                foreach ($opopops as $key => $opopop) {
                                                                    $opopopops = $this->db->where('op_op_op_id', $opopop->att_op_op_op_id)->get('attr_op_op_op_op_tbl')->result();

                                                                    $oprion_html .= "<tr><td class='attr_op_op_option'>" . @$opopop->att_op_op_op_name . "</td></tr>";

                                                                    // For Wholesaler Price : START
                                                                    if($opopop->att_op_op_op_price_type == 1){
                                                                        $wh_price = "$".$opopop->att_op_op_op_price;
                                                                    }else{
                                                                        $wh_price = $opopop->att_op_op_op_price."%";
                                                                    }
                                                                    $wholesaler_html .= "<tr><td class='wholesaler_price'>" . $wh_price . "</td></tr>";
                                                                    // For Wholesaler Price : END

                                                                    //Get retailer upcharge : START
                                                                    $ret_attr_query = "SELECT * FROM ret_attr_options_option_option_tbl WHERE att_op_op_op_id = $opopop->att_op_op_op_id AND created_by = $user_id LIMIT 1";
                                                                    $ret_attr_result = $this->db->query($ret_attr_query)->row();
                                                                    //Get retailer upcharge : END

                                                                    // For Retailer price : START
                                                                    $select_dollar = '';
                                                                    $select_per = '';
                                                                    if(@$ret_attr_result->retailer_att_op_op_op_price_type == 2){
                                                                        $select_per = 'selected';
                                                                    }else{
                                                                        $select_dollar = 'selected';
                                                                    }

                                                                    $retailer_html .= "<tr><td class='wholesaler_price_type'>";
                                                                    $retailer_html .= "<select name='retailer_att_op_op_op_price_type[]' class='retailer_price_type '>";
                                                                    $retailer_html .= "<option value='1' ".$select_dollar.">$</option>";
                                                                    $retailer_html .= "<option value='2' ".$select_per.">%</option>";
                                                                    $retailer_html .= "</select>";
                                                                    $retailer_html .= "</td>";

                                                                    $retailer_html .= "<td class='retailer_price'><input type='text' name='retailer_att_op_op_op_price[]' class='retailer_price_text' value='".@$ret_attr_result->retailer_att_op_op_op_price."'>";
                                                                    $retailer_html .= "<input type='hidden' name='hid_att_op_op_op_id[]' value='".$opopop->att_op_op_op_id."'><input type='hidden' name='hid_retailer_att_op_op_op_id[]' value='".@$ret_attr_result->retailer_att_op_op_op_id."'></td></tr>";
                                                                    // For Retailer price : END
                                                
                                                                    foreach ($opopopops as $key => $opopopop) {
                                                                        $oprion_html .= "<tr><td class='attr_op_op_op_option'>" . @$opopopop->att_op_op_op_op_name . "</td></tr>";

                                                                        // For Wholesaler Price : START
                                                                        if($opopopop->att_op_op_op_op_price_type == 1){
                                                                            $wh_price = "$".$opopopop->att_op_op_op_op_price;
                                                                        }else{
                                                                            $wh_price = $opopopop->att_op_op_op_op_price."%";
                                                                        }
                                                                        $wholesaler_html .= "<tr><td class='wholesaler_price'>" . $wh_price . "</td></tr>";
                                                                        // For Wholesaler Price : END

                                                                        //Get retailer upcharge : START
                                                                        $ret_attr_query = "SELECT * FROM ret_attr_op_op_op_op_tbl WHERE att_op_op_op_op_id = $opopopop->att_op_op_op_op_id AND created_by = $user_id LIMIT 1";
                                                                        $ret_attr_result = $this->db->query($ret_attr_query)->row();
                                                                        //Get retailer upcharge : END


                                                                        // For Retailer price : START
                                                                        $select_dollar = '';
                                                                        $select_per = '';
                                                                        if(@$ret_attr_result->retailer_att_op_op_op_op_price_type == 2){
                                                                            $select_per = 'selected';
                                                                        }else{
                                                                            $select_dollar = 'selected';
                                                                        }

                                                                        $retailer_html .= "<tr><td class='wholesaler_price_type'>";
                                                                        $retailer_html .= "<select name='retailer_att_op_op_op_op_price_type[]' class='retailer_price_type '>";
                                                                        $retailer_html .= "<option value='1' ".$select_dollar.">$</option>";
                                                                        $retailer_html .= "<option value='2' ".$select_per.">%</option>";
                                                                        $retailer_html .= "</select>";
                                                                        $retailer_html .= "</td>";

                                                                        $retailer_html .= "<td class='retailer_price'><input type='text' name='retailer_att_op_op_op_op_price[]' class='retailer_price_text' value='".@$ret_attr_result->retailer_att_op_op_op_op_price."'>";
                                                                        $retailer_html .= "<input type='hidden' name='hid_att_op_op_op_op_id[]' value='".$opopopop->att_op_op_op_op_id."'><input type='hidden' name='hid_retailer_att_op_op_op_op_id[]' value='".@$ret_attr_result->retailer_att_op_op_op_op_id."'></td></tr>";
                                                                        // For Retailer price : END
                                                                    }
                                                                }
                                                            }
                                                        } 
                                                        $oprion_html .= "</table>";
                                                        $wholesaler_html .= "</table>";
                                                        $retailer_html .= "</table>";
                                                    }
                                                }
                                                echo $oprion_html;
                                                ?>
                                            </td>
                                            <td class="wholesaler_td_section"><?=$wholesaler_html;?></td>
                                            <td class="retailer_td_section"><?=$retailer_html;?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_attribute)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                            <input type="submit" value="Submit" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

