<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Category Info</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Category Info';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <?php if(isset($data->category_id))
                                    echo 'Edit category';
                                else
                                    echo 'Add Category';
                                ?>

                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>customer/catalog/category/add-update-api" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="category_name">Category Name*<span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="category_name"
                                           name="category_name"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->category_name) && !empty($data->category_name)): echo $data->category_name; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="parent_category">Parent category</label>
                                    <select class="form-control" id="parent_category" name="parent_category" >
                                        <option value="0">Select category</option>
                                        <?php if(isset($parent_category) && count($parent_category) != 0) : ?>
                                        <?php foreach ($parent_category AS $key => $value) : ?>
                                        <option <?php if(isset($data->parent_category) && $data->parent_category == $value->category_id){ echo 'selected'; }?>  value="<?= $value->category_id ?>">
                                            <?= $value->category_name ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="description">Description</label>
                                    <textarea  class="form-control"
                                               id="description"
                                               name="description"><?php if(isset($data->description) && !empty($data->description)): echo $data->description; endif; ?></textarea>
                                    <span id="error"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        <option <?php if(isset($data->status) && $data->status == 1){ echo 'selected'; }?>  value="1">Active</option>
                                        <option <?php if(isset($data->status) && $data->status == 0){ echo 'selected'; }?> value="0">Inactive</option>

                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="category_id" value="<?php if(isset($data->category_id) && !empty($data->category_id)){ echo $data->category_id; }else{ echo 0;}?>">
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">
                                <?php if(isset($data->category_id))
                                    echo 'Update';
                                else
                                    echo 'Add';
                                ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
<?php if(!isset($data->category_id)){ ?>
<script>
    $('button[type=submit]').prop('disabled', true);
</script>
<?php } ?>
<script type="text/javascript">


    function required_validation() {

        if ($("#category_name").val() != '') {

            $('button[type=submit]').prop('disabled', false);

        }
        else {
            $('button[type=submit]').prop('disabled', true);
        }
    }





</script>