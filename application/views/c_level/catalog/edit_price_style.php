<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Price Info</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> Edit Price </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">



                        <div class="col-md-12">

            <div class="alert alert-warning">
                <strong>Notes! </strong> 1. Do not use any text. 2. Please use (') single quote twice to show inch symbol. ex: 60''
            </div>

            <!--<div class="">
                    <select class="form-control select2" name="" id="style_id" onchange="editTable()" data-placeholder="-- select one --">
                        <option value=""></option>
            <?php foreach ($style_slist as $s) { ?>
                                <option value="<?php echo $s->style_id ?>"><?= $s->style_name ?></option>
            <?php } ?>
                    </select>
                </div> -->

            <!--            <div class="btn-group">
            <?php foreach ($style_slist as $s) { ?>
                                                <a  href="javascript:void" class="btn btn-primary btn-sm" onclick="editTable('<?php echo $s->style_id ?>')"><?= $s->style_name ?></a>
            <?php } ?>
                        </div>-->
        </div>




                <div class="col-sm-12">
                    <div id="exdata" class="mb-3">
                        <p>Paste excel data here:</p>
                        <textarea name="excel_data" style="width:100%;height:150px;" onblur="javascript:generateTable()"></textarea>
                    </div>



                        <form action="<?php echo base_url(); ?>c_level/Catalog_controller/catalog_price_update" method="post">
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="col-form-label">Price Style name</label>
                                    <input type="text" name="price_style_name" id="" class="form-control" placeholder="Enter style name" required onkeyup="required_validation()" value="<?=$style_slist->style_name;?>">
                                    <input type="hidden" name="style_type" class="form-control style_type" value="<?=$style_slist->style_type;?>">
                                    <input type="hidden" name="style_id" value="<?=$style_slist->style_id;?>">
                                </div>


                                <div class="form-group col-md-12">
                                    <label for="productquantity" class="col-form-label">Price Sheet</label>
                                    <div id="excel_table">
                                        <?=$list;?>
                                    </div>

                                </div>


                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn"> Update </button>
                                </div>
                            </div>
                            
                           
                        </form>
                    </div>



                    <!-- <p>The .table-bordered class adds borders to a table:</p>             -->



                </div>
            </div>

        </div>
    </div>
</main>
<?php $this->load->view('b_level/pricemodel/_price_style_js') ;?>

