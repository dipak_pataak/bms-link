<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Category List</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Category List';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <?php
                            $user_data=access_role_permission(77);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Category List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>customer/catalog/category/list">
                    <fieldset>
                        <div class="row or-filter">
                            <div class="col-sm-2">
                                <input type="text" class="form-control cat_name" placeholder="Enter Name" name="cat_name" value="<?=$this->session->userdata('search_categoryname')?>">
                            </div>
                            <div class="or_cls"> -- OR -- </div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="parent_cat" class="form-control parent_cat select2" id="parent_cat" data-placeholder="-- select category --">
                                        <option value=""></option>
                                        <?php foreach ($get_category as $category) { ?>
                                            <option value='<?php echo $category->category_id; ?>'>
                                                <?php echo $category->category_name; ?>
                                            </option>
                                        <?php }
                                        ?>
                                    </select>
                                    <script>
                                        $("#parent_cat").val('<?=$this->session->userdata('search_parent_category')?>');
                                    </script>    
                                </div>
                            </div>
                            <div class="or_cls"> -- OR -- </div>
                            <div class="col-sm-2">
                                <div class="filter_select2_cls">
                                    <select name="category_status" class="form-control category_status select2" id="category_status" data-placeholder="-- select status --">
                                        <option value=""></option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <script>
                                        $("#category_status").val('<?=$this->session->userdata('search_status')?>');
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default" name="Search" value="Search" id="customerFilterBtn">Go</button>
                                    <a href="<?php echo base_url(); ?>customer/catalog/category/list" class="btn btn-sm btn-danger default" name="All" value="All">Reset</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-left">
                                 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onclick="return action_delete(document.recordlist)">Delete</a>
                                <?php } ?>
                            </div>


                            <div class="form-group row m-0 mb-3  float-right">
                                <label for="keyword" class="mb-2"></label>
                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button onclick="window.location.href='<?= base_url(); ?>customer/catalog/category/add'" class="btn btn-info  mb-2" type="button">
                                    New Category
                                    <i class="fa fa-plus"> </i>
                                </button>
                                <?php } ?>
                               <!-- <button class="btn btn-info dropdown-toggle mb-2" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                    <span class="caret"></span></button>-->
                              <!--  <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-csv')" class="dropdown-item">Export to CSV</a></li>
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-pdf')"  class="dropdown-item">Export to PDF</a></li>
                                    <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                                </ul>-->
                            </div>
                        </div>

<form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Catalog_controller/multipal_catagory_delete') ?>">
    <input type="hidden" name="action">
                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" id="SellectAll"/></th>
                                        <th>Serial No.</th>
                                        <th>Category Name</th>
                                        <th>Parent Category</th>
                                        <th>Description</th>
                                        <th>Fractions</th>
                                        <th>Assigned Products</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($get_category as $category) {
                                        $sl++;
                                        $assigned_products = $this->db->select('product_name')->from('product_tbl')->where('category_id', $category->category_id)->get()->result();
                                        $assigned_products_list = '';
                                        if ($assigned_products) {
                                            foreach ($assigned_products as $product) {
                                                $assigned_products_list .= "<ul>";
                                                $assigned_products_list .= "<li> => " . $product->product_name . "</li>";
                                                $assigned_products_list .= "</ul>";
                                            }
                                        } else {
                                            $assigned_products_list .= "The products will be assigned at the later stage";
                                        }

                                        $category_status = '';
                                        if ($category->status == '1') {
                                            $category_status =  "Active";
                                        } else {
                                            $category_status = 'Inactive';
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <?php if($category->created_status == 1){ ?>
                                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?php echo $category->category_id; ?>" class="checkbox_list">
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $sl; ?></td>
                                            <td><?= $category->category_name; ?></td>
                                            <td><?= $category->parent_category_name; ?></td>
                                            <td><?= $category->description; ?></td>
                                            <td></td>
                                            <td><?= $assigned_products_list ?></td>
                                            <td><?= $category_status; ?></td>
                                            <td>
                                                <?php if($category->created_status == 1){ ?>
                                                    <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?php echo base_url(); ?>customer/catalog/category/edit/<?php echo $category->category_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                                    <?php } ?>
                                                    <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                <a href="<?php echo base_url(); ?>retailer-category-delete/<?php echo $category->category_id; ?>" class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm(\'Do you want to delete it?\')"><i class="simple-icon-trash"></i>
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_category)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
</form>
                        <?php echo $links; ?>
                    </div>


                    <div class="modal fade" id="customer_address_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Customer Address Map Show</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="customer_comment_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">All Comments</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_comment_info">
                                    <form class="" action="<?php echo base_url('b_level/Return_controller/return_resend_comment'); ?>" method="post">
                                        <textarea class="form-control" name="resend_comment" required ></textarea>
                                        <input type="hidden" name="return_id" id="return_id">
                                        <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="customer_exp" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"> Export Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">
                                    <form id="expUrl" action="#" method="post">

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label">Start From <i class="text-danger">*</i></label>
                                            <div class="col-xs-6">
                                                <input type="number" min="1" name="ofset" id="ofset" class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label">Limit <i class="text-danger">*</i></label>
                                            <div class="col-xs-6">
                                                <input type="number" min="1" name="limit" id="limit" class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label"></label>
                                            <div class="col-xs-6">
                                                <button class="btn-sm btn-success" id="closeModal"> Export</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>

    function show_customer_record(t, statuss) {
        if(statuss == 'scheduled' || statuss == 're-schedule' || statuss == 'cancelled'){
            $.post("<?php echo base_url(); ?>show-customer-record/" + t, 'status=' + statuss, function (t) {
                $("#customer_info").html(t);
                //Date picker
                $('.datepicker').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    showOn: "focus",
                });
                // ======== its timer =========== format: 'LT'
                $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
                $('#datepicker-left-header').datetimepicker({
                    // 'format': "HH:mm", // HH:mm:ss its for 24 hours format
                    format: 'LT', /// its for 12 hours format
                });
                var st = $('#sta').val();
                $('#status').val(statuss);
                $("form :input").attr("autocomplete", "off");
                $("#appointschedule").modal('show');
            });
        } else{
            $.post("<?php echo base_url(); ?>change-customer-status/", {status : statuss, customer_id : t}, function (res) {
                toastr.success('Success! - Status Change Successfully');
                setTimeout(function () {
                    window.location.href = window.location.href;
                }, 2000);
            });
        }
    }

    //    ========== its for customer search ======
    function customerkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>retailer-customer-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#result_search").html(r);
            }
        });
    }

    //============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)

        $.post("<?php echo base_url(); ?>retailer-show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
    // ============ its for show_old_comment ============
    function show_old_comment(id) {
        $.ajax({
            url: "<?php echo base_url(); ?>show-old-retailer-cstomer-comment",
            type: "post",
            data: {customer_id: id},
            success: function (t) {
                $("#customer_comment_info").html(t);
                $('#customer_comment_modal_info').modal('show');
            }
        });
    }
    //============= its for show_export_pdf =============
    function ExportMethod(url) {
        $("#expUrl").attr("action", url);
        $("#customer_exp").modal('show');
    }

    $("body").on('click', '#closeModal', function () {
        $("#customer_exp").modal('hide');
    });


    $(document).ready(function(){
        // For Open filter Box : START
        var first_name = $("#filter-form input[name=first_name]").val();
        var sidemark = $("#filter-form input[name=sidemark]").val();
        var phone = $("#filter-form input[name=phone]").val();
        var address = $("#filter-form input[name=address]").val();
        var filter_status = $("#filter-form select[name=filter_status]").val();

        if(first_name != '' || sidemark != '' || phone != '' || address != '' || filter_status !=''){
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
    })

</script>
<script type="text/javascript">
    $('#s1').hide();
    $('#c1').hide();
    function viewFild(id) {
        if (id == 1) {
            $('#s1').show();
            $('#c1').hide();
        } else if (id == 2) {
            $('#s1').show();
            $('#c1').show();

        } else {
            $('#s1').hide();
            $('#c1').hide();
        }
    }
</script>

<script>
    function action_delete(frm) {
        with(frm)
        {
            var flag = false;
            str = '';
            field = document.getElementsByName('Id_List[]');
            for (i = 0; i < field.length; i++)
            {
                if(field[i].checked == true)
                { 
                    flag = true;
                    break;
                }
                else
                    field[i].checked = false;
            }
            if(flag == false)
            {
                Swal.fire("Please select atleast one record");
                return false;
            }
        }
        if(confirm("Are you sure to delete selected records ?"))
        {
            // frm.action.value = "action_delete";
            $("input[name=action]").val('action_delete');
            frm.submit();
            return true ;
        }
    }
</script>
