<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Sqm Price Mapping</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Sqm Price Mapping';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Sqm Price
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>c_level/catalog_controller/save_sqm_maping_price" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="sqm_price">Sqm price*<span class="text-danger"> * </span></label>
                                    <input type="number"
                                           class="form-control"
                                           id="sqm_price"
                                           name="sqm_price"
                                           min="1" 
                                           onkeyup="required_validation()"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="product_id">Select Product</label>
                                    <select class="form-control" id="product_id" name="product_id" onselect="get_product_pattern_list()" onchange="get_product_pattern_list()">
                                        <option value="0">Select Product</option>
                                        <?php if(isset($customet_product_list) && count($customet_product_list) != 0) : ?>
                                        <?php foreach ($customet_product_list AS $key => $value) : ?>
                                        <option  value="<?= $value->product_id ?>">
                                            <?= $value->product_name ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    </select>
                                </div>
                             
                                <div class="form-group col-md-6">
                                    <label for="last_name">Pattern</label>
                                    <select class="form-control" id="pattern_idss" name="pattern_id[]" required="required">
                                        
                                    </select>
                                </div>

                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>




        <div class="row">
            <div class="col-xl-12 mb-4">

              
                <div class="card mb-4">
                    <div class="card-body">

                            <div class="form-group row m-0 mb-3  float-left">
                                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onclick="return action_delete(document.recordlist)">Delete</a>
                            </div>

                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3 float-right">
                                <label for="keyword" class="mb-2"></label>
                                
                            </div>
                        </div>

<form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Catalog_controller/multipal_sqm_price_mapping_delete') ?>">
    <input type="hidden" name="action">
                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" id="SellectAll"/></th>
                                        <th>Serial No.</th>
                                        <th>Product Name</th>
                                        <th>Pattern Name</th>
                                        <th>Sqm Price</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($get_sqm_price as $value) {
                                        $sl++; ?>
                                        <tr>
                                            <td>
                                                <?php if($value->created_status == 1){ ?>
                                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?php echo $value->id; ?>" class="checkbox_list">
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $sl; ?></td>
                                            <td><?= $value->product_name; ?></td>
                                            <td><?= $value->pattern_name; ?></td>
                                            <td><?= $value->price; ?></td>
                                            <td>
                                                <?php if($value->created_status == 1){ ?>
                                                    <a href="<?php echo base_url(); ?>c_level/catalog_controller/catalog_sqm_price_delete/<?php echo $value->id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-trash"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_sqm_price)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
</form>
                        <?php echo $links; ?>
                    </div>

                </div>
            </div>
        </div>



    </div>
</main>

<script type="text/javascript">
    function required_validation() {

        if ($("#sqm_price").val() != '') {


            $('button[type=submit]').prop('disabled', false);

        }
        else {
            $('button[type=submit]').prop('disabled', true);
        }
    }
</script>

<script>
    function get_product_pattern_list()
    {
        var product_id = $('#product_id').val();
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/catalog_controller/get_product_pattern/"+product_id,
            type: 'GET',
            dataType:'JSON',
            success: function (data) {
                if(data.length != 0)
                {
                    $('#pattern_idss').empty();
                    for (var i = 0; i < data.length; i++)
                    {
                        var result = data[i];
                        $('#pattern_idss').append(`<option value="`+result.pattern_model_id+`">`+result.pattern_name+`</option>`);
                    }
                }
            }
        });
    }
</script>

<script type="text/javascript">
  function action_delete(frm) {
        with(frm)
        {
            var flag = false;
            str = '';
            field = document.getElementsByName('Id_List[]');
            for (i = 0; i < field.length; i++)
            {
                if(field[i].checked == true)
                { 
                    flag = true;
                    break;
                }
                else
                    field[i].checked = false;
            }
            if(flag == false)
            {
                Swal.fire("Please select atleast one record");
                return false;
            }
        }
        if(confirm("Are you sure to delete selected records ?"))
        {
            // frm.action.value = "action_delete";
            $("input[name=action]").val('action_delete');
            frm.submit();
            return true ;
        }
    }

</script>
