<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Product List</h1>
                    <?php
                    $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Product List';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                   <?php
                            $user_data=access_role_permission(79);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Product List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <form class="form-horizontal" action="<?php echo base_url(); ?>customer/catalog/product/list" method="post">
                    <fieldset>
                        <div class="row">
                           
                            <div class="col-sm-3 mb-2">
                                <select class="form-control select2 category_name" name="category_id" data-placeholder="-- Select Category --">
                                    <option value="">Select Catagory</option>
                                    <?php
                                    foreach ($get_category as $category) {
                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                    }
                                    ?>
                                </select>
                            </div> 

                             <div class="col-sm-3 mb-2">
                                <select class="form-control select2 price_style_type mb-2" name="price_style_type" data-placeholder="-- Select Price Style --">
                                    <option value="">Select style</option>
                                    <option value="1">Row/Column style</option>
                                    <option value="4">Group style</option>
                                    <option value="5">Sqm price style</option>
                                    <option value="2">Price by Sq.ft style </option>
                                    <option value="3">Fixed product Price Style</option>
                                </select>
                            </div>
                             <div class="col-sm-3 mb-2">
                                <select class="form-control select2 status mb-2" name="status" data-placeholder="-- Select Status --">
                                    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                    
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <div>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-left">
                                 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                <a href="javascript:void(0)" class="btn btn-danger btn-sm action-delete" onclick="return action_delete(document.recordlist)">Delete</a>
                                <?php } ?>
                            </div>

                            <div class="form-group row m-0 mb-3 float-right">
                                <label for="keyword" class="mb-2"></label>
                                 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button onclick="window.location.href='<?= base_url(); ?>customer/catalog/product/add'" class="btn btn-info  mb-2" type="button">
                                    New Product
                                    <i class="fa fa-plus"> </i>
                                </button>
                                <?php } ?>
                            </div>
                        </div>

<form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Catalog_controller/multipal_product_delete') ?>">
<input type="hidden" name="action">
                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" id="SellectAll"/></th>
                                        <th>Serial No.</th>
                                        <th>Product Name</th>
                                        <th>Category</th>
                                        <th>Price Style Type</th>
                                        <th>Dflt Discnt.(%)</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($get_product as $value) {
                                        $sl++; ?>
                                        <tr>
                                            <td>
                                                <?php if($value->created_status == 1){ ?>
                                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?php echo $value->product_id; ?>" class="checkbox_list">
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $sl; ?></td>
                                            <td><?= $value->product_name; ?></td>
                                            <td><?= $value->category_name; ?></td>
                                            <td>
                                                <?php
                                                if ($value->price_style_type == 1) {
                                                    echo "Row/Column style";
                                                } elseif ($value->price_style_type == 2) {
                                                    echo "Price by Sq.ft style";
                                                } elseif ($value->price_style_type == 4) {
                                                    echo "Group Price";
                                                } elseif($value->price_style_type == 3){
                                                    echo "Fixed product Price Style";
                                                }
                                                elseif($value->price_style_type == 5){
                                                    echo "Sqm price style";
                                                }
                                                ?>
                                            </td>
                                            <td><?= $value->dealer_price; ?></td>
                                            <td>
                                                <?php if ($value->active_status == 1)
                                                    echo 'Active';
                                                else echo 'Inactive';
                                                ?>
                                            </td>
                                            <td>
                                                <?php if($value->created_status == 1){ ?>
                                                     <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?php echo base_url(); ?>customer/catalog/product/edit/<?php echo $value->product_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                                      <?php } ?>
                                                    <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?php echo base_url(); ?>c_level/catalog_controller/delete_color/<?php echo $value->product_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" ><i class="simple-icon-trash"></i></a>
                                                     <?php } ?>
                                                <?php } ?>

                                            </td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_product)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
</form>
                        <?php echo $links; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
  function action_delete(frm) {
        with(frm)
        {
            var flag = false;
            str = '';
            field = document.getElementsByName('Id_List[]');
            for (i = 0; i < field.length; i++)
            {
                if(field[i].checked == true)
                { 
                    flag = true;
                    break;
                }
                else
                    field[i].checked = false;
            }
            if(flag == false)
            {
                Swal.fire("Please select atleast one record");
                return false;
            }
        }
        if(confirm("Are you sure to delete selected records ?"))
        {
            // frm.action.value = "action_delete";
            $("input[name=action]").val('action_delete');
            frm.submit();
            return true ;
        }
    }

</script>
