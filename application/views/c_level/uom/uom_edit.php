<script src="//code.jquery.com/jquery.min.js"></script>
<script src="http://live-bms-link.ti/assets/c_level/resources/js/bootstrap-datepicker.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Unit of Measurement</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Edit Unit
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>c_level/Setting_controller/uom_update/<?php echo $uom_edit[0]['uom_id']; ?>" id="menusetupFrm" method="post">
                            <div class="form-row">
                                
                                <div class="col-md-12">
                                
                                    <div class="form-group">
                                       <label for="uom_name" class="mb-2">Unit of Measurement</label>
                                        <input class="form-control" type="text" name="uom_name" placeholder="Unit of measurement" id="uom_name" required value="<?php echo $uom_edit[0]['uom_name']; ?>" tabindex="1">
                                    </div>



                                    <div class="form-group">
                                        <label for="unit_types">Unit Type</label>
                                        <select class="form-control" id="unit_types" name="unit_types" >
                                            <option value="Unit" <?php if($uom_edit[0]['unit_types']=='Unit') echo 'selected="selected"'?>>Unit</option>
                                        <option value="Quantity" <?php if($uom_edit[0]['unit_types']=='Quantity') echo 'selected="selected"'?>>Quantity</option>
                                        </select>
                                       
                                    </div>



                                    <div class="form-group">
                                        <label for="unit_types">Status</label>
                                               <select name="status" class="form-control select2" id="status" data-placeholder="-- select one --" tabindex="2">
                                            <option value=""></option>
                                            <option value="1" <?php
                                            if ($uom_edit[0]['status'] == '1') {
                                                echo 'selected';
                                            }
                                            ?>>Active</option>
                                            <option value="0" <?php
                                            if ($uom_edit[0]['status'] == '0') {
                                                echo 'selected';
                                            }
                                            ?>>Inactive</option>
                                        </select>
                                       
                                    </div>

                                   
                        </div>


                        
                              
                        </div>

                        <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                                <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Update" tabindex="7" />

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


