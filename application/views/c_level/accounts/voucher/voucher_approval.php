<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Voucher Approval</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Voucher Approval';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Account</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Credit</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                    <?php
                            $user_data=access_role_permission(18);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
						<div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>SL No.</th>
                                    <th>Voucher No</th>
                                    <th>Remark</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($aprrove))  ?>
                                <?php $sl = 1; ?>
                                <?php foreach ($aprrove as $approve) { ?>
                                    <tr>
                                        <td><?php echo $sl++; ?></td>
                                        <td><?php echo $approve->VNo; ?></td>
                                        <td><?php echo $approve->Narration; ?></td>
                                        <td><?php echo $approve->Debit; ?></td>
                                        <td><?php echo $approve->Credit; ?></td>
                                        <td>
                                            <?php if($user_data[0]->can_access==1 or $log_data[0]->is_admin==1){ ?>
                                            <a href="<?php echo base_url("isactive/$approve->VNo/active") ?>" onclick="return confirm('Are you sure?')" class="btn btn-warning btn-sm" title="Approved" >Approved</a>
                                            <?php } ?>
                                            <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                            <a href="<?php echo base_url("voucher-edit/$approve->VNo") ?>" class="btn btn-info btn-sm" title="Edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                            <?php } ?>

                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                            <?php
                            if (empty($aprrove)) {
                                ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-center text-danger" colspan="6">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
						</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
