<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 166.132px !important;
        left: 413.455px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Trial Ballance</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Trial Ballance';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <?php
                            $user_data=access_role_permission(26);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Trial Ballance</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?= form_open_multipart('retailer-trial-balance-report/') ?>
                        <div class="row" id="">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="date" class="col-sm-3 col-md-2 col-form-label">From Date</label>
                                    <div class="col-sm-5 col-md-4">
                                        <input type="text" name="dtpFromDate" value="" placeholder="From Date" class="datepicker form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="date" class="col-sm-3 col-md-2 col-form-label">To Date</label>
                                    <div class="col-sm-5 col-md-4">
                                        <input type="text"  name="dtpToDate" value="" placeholder="To Date" class="datepicker form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="date" class="col-sm-3 col-md-2 col-form-label"></label>
                                    <div class="col-sm-5 col-md-4">
                                        <input type="checkbox" id="chkWithOpening" name="chkWithOpening" size="40"/>&nbsp;&nbsp;&nbsp;<label for="chkWithOpening">With Details</label>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <?php if($user_data[0]->can_access==1 or $log_data[0]->is_admin==1){ ?>
                                    <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Find</button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
