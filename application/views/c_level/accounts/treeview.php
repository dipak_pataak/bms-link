
<style>
    .inactive{
        color: red;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Account Chart</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Account Chart';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Chart</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <?php
            $user_data=access_role_permission(65);
            $log_data=employ_role_access($id);
        ?>

        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php echo form_open('chart-of-account'); ?>
                        <h3>
                              <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                            <input type="checkbox" name="inactive" value="test" <?php
                            if ($inactive == 0) {
                                echo 'Checked';
                            }
                            ?>>Include Inactive
                            &nbsp; <input type="submit" name="inactive_btn" value="Submit">
                            <?php } ?>
                        </h3>

                        <?php echo form_close(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <ul id="tree1">
                                    <?php
                                    $isActive = 0;
                                    $visit = array();

                                    for ($i = 0; $i < count($userList); $i++) {
                                        $visit[$i] = false;
                                    }

                                    $this->Account_model->dfs("COA", "0", $userList, $visit, 0, $isActive);
                                    ?>
                                </ul>
                            </div>
                           
                            <?php // if ($this->permission->method('accounts', 'update')->access() || $this->permission->method('accounts', 'create')->access()):  ?>
                            <div class="col-md-6" id="newform"></div>
                            <?php // endif;  ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</main>

<script>
    $(document).ready(function () {
        "use strict"; // Start of use strict

        $.fn.extend({
            treed: function (o) {

                var openedClass = 'simple-icon-people';
                var closedClass = 'fa-folder-o';

                if (typeof o !== 'undefined') {
                    if (typeof o.openedClass !== 'undefined') {
                        openedClass = o.openedClass;
                    }
                    if (typeof o.closedClass !== 'undefined') {
                        closedClass = o.closedClass;
                    }
                }
                ;

                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator fa " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this === e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    });
                    branch.children().children().toggle();
                });
                //fire event from the dynamically added icon
                tree.find('.branch .indicator').each(function () {
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });

        //Initialization of treeviews

        $('#tree3').treed({openedClass: 'fa-folder-open-o', closedClass: 'fa-folder'});

    });

</script>


<script type="text/javascript">
    function loadData(id) {
        $.ajax({
            url: "<?php echo site_url('show-selected-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data){
                $('#newform').html(data);
                $('#btnSave').hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                Swal.fire('Error get data from ajax');
            }
        });
    }


</script>
<script type="text/javascript">
    function newdata(id) {
        $.ajax({
            url: "<?php echo site_url('new-account-form/') ?>" + id,
            type: "GET",
            dataType: "json",
            success: function (data)
            {
                // console.log(data.headcode);
                console.log(data.rowdata);
                var headlabel = data.headlabel;
                $('#txtHeadCode').val(data.headcode);
                document.getElementById("txtHeadName").value = '';
                $('#txtPHead').val(data.rowdata.HeadName);
                $('#txtHeadLevel').val(headlabel);
                $('#btnSave').prop("disabled", false);
                $('#btnSave').show();
                $('#btnUpdate').hide();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                Swal.fire('Error get data from ajax');
            }
        });
    }

</script>
