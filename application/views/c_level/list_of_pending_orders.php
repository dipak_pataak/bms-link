<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Pending Orders</h1>

                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Pending Orders</a>
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>S.no</th>
                                        <th>Order Number</th>
                                        <th>Customer Name </th>
                                        <th>Side Mark </th>
                                        <th>Order date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($pending_orders)) {
                                        $i = 1;
                                        foreach ($pending_orders as $key => $value) {
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $value->order_id; ?></td>
                                                <td><?php echo $value->customer_name; ?></td>
                                                <td><?php echo $value->side_mark; ?></td>
                                                <td><?php echo (@$value->order_date); ?></td>
                                                <td><?php echo $currency; ?><?= $value->grand_total ?></td>
                                                <td>
                                                    <select class="form-control select2-single select2-hidden-accessible" disabled="" data-placeholder="-- select one --" tabindex="-1" aria-hidden="true">
                                                            <option value=""></option>
                                                            <option value="3" <?php echo ($value->order_stage == '3' ? 'selected' : '') ?>>Partially Paid</option>
                                                            <option value="4" <?php echo ($value->order_stage == '4' ? 'selected' : '') ?>>Manufacturing</option>
                                                            <option value="8" <?php echo ($value->order_stage == '8' ? 'selected' : '') ?>>Ready to be shipped</option>
                                                            <option value="5" <?php echo ($value->order_stage == '5' ? 'selected' : '') ?>>Shipping</option>
                                                            <option value="9" <?php echo ($value->order_stage == '9' ? 'selected' : '') ?>>In Transit</option>
                                                            <option value="2" <?php echo ($value->order_stage == '2' ? 'selected' : '') ?>>Paid</option>
                                                            <option value="10" <?php echo ($value->order_stage == '10' ? 'selected' : '') ?>>Confirmation</option>
                                                            <option value="11" <?php echo ($value->order_stage == '11' ? 'selected' : '') ?>>Manufacturing</option>
                                                        </select>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url('retailer-invoice-receipt/'.$value->order_id) ?>" class="btn btn-success btn-xs default" title="" data-toggle="tooltip" data-placement="top" data-original-title="View">
                                                        <i class="simple-icon-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    } else {
                                        ?>
                                    <div class="alert alert-danger"> There have no order found..</div>
                                <?php } ?>
                                </tbody>
                            </table>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    $(window).ready(function(){
        $('.dataTable').dataTable();
    });
</script>
