<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
            flex: 0 0 35px;
            max-width: 35px;
            text-align: center;
    }    
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    .container-radio{
      display: block;
      position: relative;
      padding-left: 28px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 16px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default radio button */
    .container-radio input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark-radio {
      position: absolute;
      top: 2px;
      left: 0;
      height: 20px;
      width: 20px;
      background-color: #eee;
      border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container-radio:hover input ~ .checkmark-radio {
      background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container-radio input:checked ~ .checkmark-radio {
      background-color: #145388;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark-radio:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container-radio input:checked ~ .checkmark-radio:after {
      display: block;
    }

    /* Style the indicator (dot/circle) */
    .container-radio .checkmark-radio:after {
        top: 6px;
        left: 6px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
   /* .modal-content input[type=radio] {
        width: 20px;
        height: 20px;        
    }
    .modal-content input[type=checkbox]:checked + label {
        background-color: red !important;
    }*/
  /*  .radio-part {
        position: relative;
    }
    .radio-part label {
        position: absolute;
        margin-top: 18px;
    }*/

    .text-part .form-control {
         min-height: 120px !important;
    }


    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>RMA View List</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Customer List';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                      <?php
                            $user_data=access_role_permission(114);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">RMA View List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3 search_div float-right">
                                <label for="keyword" class="mb-2"></label>
                                   <!--  <input type="text" class="form-control mb-2" name="keyword" id="keyword" onkeyup="customerkeyup_search()" placeholder="Search..." tabindex=""> -->
                            </div>          
                        </div>
                        
                        
                            <input type="hidden" name="action">
                            <div class="table-responsive">
                            <table class="datatable2 table table-bordered table-hover" id="result_search">
                                <thead>
                                    <tr>
                                        <th width="4%">SL#</th>
                                        <th width="11%">Order Id</th>
                                        <th width="9%">Item Name</th>
                                        <th width="9%">Customer Name</th>
                                        <th width="9%">Return Qty</th>
                                        <th width="9%">Replace Qty</th>
                                        <th width="14%">Replace/Repair</th>
                                        <th width="11%">Status</th>
                                        <th width="10%">Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $sl = 0 + $pagenum;
                                        foreach ($get_items_for_employee as $value) {
                                            $sl++;
                                    ?>
                                        <tr>
                                            <td> <?php echo $sl; ?> </td>
                                            <td> <?php echo $value->order_id; ?> </td>
                                            <td> 
                                                <a href="javascript:void(0)" id="address_1" class="address phone_email_link" data-toggle="modal" data-target="#exampleModalCenter_<?php echo $value->id; ?>" data-orderid="<?php echo $value->order_id; ?>">
                                                    <?php echo $value->product_name; ?>
                                                </a>
                                            </td>
                                            <!-- Modal -->
                                            <form method="post" id="popup-form" class="popup-form" action="<?php echo base_url('c_level/C_rma_controller/manage_rma_data') ?>">
                                            <div class="modal fade" id="exampleModalCenter_<?php echo $value->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                              <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Item Status</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                        <input type="hidden" name="hidden_id" value="<?php echo $value->id; ?>">
                                                        <input type="hidden" name="hidden_customer_id" value="<?php echo $value->customer_id; ?>">
                                                        <input type="hidden" name="hidden_order_id" value="<?php echo $value->order_id; ?>">
                                                        <input type="hidden" name="hidden_product_id" value="<?php echo $value->product_id; ?>">
                                                        <input type="hidden" name="hidden_return_type" value="<?php echo $value->return_type; ?>">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <select class="form-control" id="item_status" name="item_status">
                                                                    <option value="">Select Status</option>
                                                                    <option value="1">Production</option>
                                                                    <option value="2">Shipping</option>
                                                                    <option value="3">Delivered</option>
                                                                </select>
                                                                <span class="error-msg" style="color:red">Please select status</span>
                                                                <div class="custom-radio-div mt-4">
                                                                    <label class="container-radio">Return to wholeseller
                                                                      <input type="checkbox" name="radio" value="1">
                                                                      <span class="checkmark-radio"></span>
                                                                    </label>
                                                                </div>                                                             
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="text-part"> 
                                                                    <textarea class="form-control" id="item_comment" name="item_comment" placeholder="Enter comment here.."></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mt-3">
                                                        </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <input type="submit" onClick="this.form.submit(); this.disabled=true; this.value='Submiting'; " id="btn-save-changes" class="btn btn-primary btn-save-changes" data-id="<?php echo $value->id; ?>" value="Save changes">
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            </form>
                                            <td> <?php echo ucwords($value->cust_name); ?> </td>
                                            <td> <?php echo $value->return_qty; ?> </td>
                                            <td> <?php echo $value->replace_qty; ?> </td>
                                            <td> <?php 
                                                if($value->return_type == "1")
                                                    echo "Replace";
                                                else
                                                    echo "Repair";
                                            ?> </td>
                                            <td> <?php 
                                                if($value->is_approved == "0")
                                                    echo "Pending";
                                                else if($value->is_approved == "1")
                                                    echo "Approved";
                                                else
                                                    echo "Cancel";
                                            ?> </td>
                                            <td> <?php echo $value->return_comments; ?> </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <?php if (empty($get_items_for_employee)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>
                            </div>
                        <?php echo $links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>

<script>

    $(document).ready(function(){
        $(':input[type="submit"]').prop('disabled', true);
        $('select').on('change', function() {
            $status_val = $(this).val();

            if($status_val == ""){
                $('.error-msg').show();
                $(':input[type="submit"]').prop('disabled', true);
            }else{
                $('.error-msg').hide();
                $(':input[type="submit"]').prop('disabled', false);
            }
        });

        // $( "#popup-form" ).submit(function( event ) {
        //   console.log( $( this ).serializeArray() );
        //   event.preventDefault();
        // });

        // $(".btn-save-changes").on('click',function(){
        //     // alert($(this).data('id'));
        //     // var data = $('#popup-form').serialize();
        //     var data = $('.popup-form').serialize();
        //     $.ajax({
        //         url: "<?php // echo base_url('c_level/C_rma_controller/manage_rma_data') ?>",
        //         type: 'POST',
        //         data: data,
        //         success: function (response) {
        //             // $("#result_search").html(r);
        //             console.log(response);
        //             if(response == 1)
        //             {
        //                 // toastr.success('Success! - Status Changed Successfully');
        //                 // setTimeout(function() {
        //                 //     window.location.href = window.location.href;
        //                 // }, 2000);
        //             }
        //         }
        //     });
        // });

    });
    //    ========== its for customer search ======
    function customerkeyup_search() {
    var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>retailer-customer-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#result_search").html(r);
            }
        });
    }
    
</script>