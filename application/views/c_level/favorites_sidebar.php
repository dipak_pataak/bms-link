<?php if (!empty($favorites)) { ?>
    <div class="favorite-div-header"> <a href="javascript:toggle_fav_menu(this)" class="style-toggle open" id="side-fav-toggle" title="Favorites"> <span class="sidebar-star-icon" style="padding-top:15%;"></span> </a>
        <div class="favorites-content-main">
            <ul id="favorites-sortable" class="ui-sortable">
                <?php foreach ($favorites as $key => $favorite) { ?>
                    <li class="ui-state-default ui-sortable-handle" id="fav-<?php echo $favorite['id']; ?>">
                        <div class="fav-action-div">
                            <input type="text" class="form-control d-none input_title" id="title_<?php echo $key; ?>" name="title" onkeypress="save_fav_edit(event,this,<?php echo $favorite['id']; ?>)" required="required" autocomplete="off" value="<?php echo $favorite['favorite_title']; ?>" maxlength="30">
                            <a href="<?php echo base_url() . $favorite['page_url']; ?>" class="page_url_cls" title="<?php echo $favorite['favorite_title']; ?>"><?php echo $favorite['favorite_title']; ?></a>
                            <div class="fav-action">
                                <a href="javascript:void(0)" onclick="show_edit(this)"><i class="simple-icon-pencil" aria-hidden="true" style="font-size: 15px;"></i></a>
                                <a href="javascript:void(0)" onclick="remove_favorite(<?php echo $favorite['id']; ?>,this)" data-page-url="<?php echo $favorite['page_url']; ?>"><span class="fav-remove-icon" style="font-size: 23px;"></span></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>
<script>
    function show_edit(input) {
        var li_id = $(input).parents('li').attr('id');
        $(input).parent().hide();
        $('#' + li_id + ' .input_title').removeClass('d-none');
        $('#' + li_id + ' .page_url_cls').hide();
    }

    function save_fav_edit(keyevent, input, favorite_id) {
        var keycode = (keyevent.keyCode ? keyevent.keyCode : ekeyeventvent.which);
        if (keycode == '13') {
            var title = $.trim($(input).val());
            if (title != '') {
                $.ajax({
                    url: mybase_url + "c_level/dashboard_controller/save_fav_edit",
                    type: "post",
                    data: {
                        'title': title,
                        'favorite_id': favorite_id
                    },
                    success: function(response) {
                        var response = $.parseJSON(response);
                        if (response.status == true) {
                            load_favorites_sidebar();
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                    }
                });
            } else {
                toastr.error('Error! - Please enter title');
            }
        }
    }
</script>