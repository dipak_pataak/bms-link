
<style type="text/css">
    #results_menu .iti {
        position: relative;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>SMS Configuration</h1>
                    <?php
                        $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'SMS Configuration';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">SMS Configuration</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                  <?php
                        $user_data=access_role_permission(69);
                        $log_data=employ_role_access($id);
                                                                
                    ?>

                <div class="card mb-4">
                    <div class="card-body" id="results_menu">
                        <form action="<?php base_url(); ?>retailer-sms-config-save" method="post">
                            <div class="form-group row">
                                <label for="provider_name" class="col-xs-3 col-form-label">Provider Name <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_name" class="col-xs-3 col-form-label">User Name <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-xs-3 col-form-label">Password <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-xs-3 col-form-label">Phone <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="phone" class="form-control phone-format" id="phone" placeholder="+12062024567 This is the valid format" tabindex="4" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sender_name" class="col-xs-3 col-form-label">Sender Name <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="test_sms_number" class="col-xs-3 col-form-label">Test SMS Number <span class="text-danger"> * </span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="test_sms_number" class="form-control phone-format" id="test_sms_number" placeholder="+12062024567 This is the valid format" tabindex="5" required>
                                </div>
                            </div>

                            <div class="form-group  text-right">
                                 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button type="submit" class="btn btn-success w-md m-b-5" tabindex="6">Save</button>
                                 <?php } ?>
                                 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                <button type="reset" class="btn btn-primary w-md m-b-5" tabindex="7">Reset</button>
                                <?php } ?>
                            </div>
                        </form>

                        <div class="title">
                            <h3>List Of SMS Configuration</h3>
                        </div>
                        <div class="table-responsive p-3">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#.</th>
                                        <th width="10%">Name</th>
                                        <th width="15%">User Name</th>
                                        <th width="15%">Password</th>
                                        <th width="10%">Phone</th>
                                        <th width="10%">Sender</th>
                                        <th width="4%">Status</th>
                                        <th width="4%">Verified</th>
                                        <th width="30%" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sl = 0; // + $pagenum;
                                    foreach ($get_sms_config as $value) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $value->provider_name; ?></td>
                                            <td><?php echo $value->user; ?></td>
                                            <td><?php echo $value->password; ?></td>
                                            <td><?php echo $value->phone; ?></td>
                                            <td><?php echo $value->authentication; ?></td>

                                            <td>
                                                <?php
                                                    if ($value->default_status == 1) {
                                                        echo 'Active';
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?></td>
                                            <td>
                                                <?php
                                                    if ($value->is_verify == 1) {
                                                        echo 'Yes';
                                                    } else {
                                                        echo "No";
                                                    }
                                                    ?></td>
                                            <td class="text-center">
                                                <?php if ($value->is_verify == 0) { ?>
                                                     <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?php echo base_url(); ?>retailer-sms-config-verified/<?php echo $value->gateway_id; ?>" class="btn btn-info default btn-sm" title="Verified Configuration" data-toggle="tooltip" data-placement="top" data-original-title="Verified Configuration"><i class="simple-icon-check"></i></a>
                                                    <?php } ?>
                                                <?php } ?>
                                                 <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                <a href="<?php echo base_url(); ?>retailer-sms-config-edit/<?php echo $value->gateway_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                                 <?php } ?>
                                                 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                <a href="<?php echo base_url(); ?>retailer-sms-config-delete/<?php echo $value->gateway_id; ?>" class="btn btn-danger danger btn-sm" onclick="return confirm('Do you want to delete it?')" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                                <?php if (empty($get_sms_config)) { ?>
                                    <tfoot>
                                        <th colspan="9" class="text-center">
                                            No result found!
                                        </th>
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function() {
        load_country_dropdown('phone', country_code_phone_format); // For phone format
        load_country_dropdown('test_sms_number', country_code_phone_format); // For phone format
    });
</script>
