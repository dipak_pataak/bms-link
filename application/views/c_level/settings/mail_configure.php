<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Mail Configuration</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Mail Configuration';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Mail Configuration</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                    <?php
                        $user_data=access_role_permission(70);
                        $log_data=employ_role_access($id);
                    ?>


                <div class="card mb-4">
                    <div class="card-body" id="results_menu">
                        <?php 
                            if($get_mail_config[0]->is_verified != 1) {
                                echo "<div class='alert alert-danger'>Mail configuration is not verified. Please verify it.</div>";
                            }
                        ?>
                        <form action="<?php base_url(); ?>retailer-mail-config-save" method="post">
                            <div class="form-group row">
                                <label for="protocol" class="col-sm-3 col-form-label">Protocol <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="protocol" id="protocol" type="text" value="<?php echo $get_mail_config[0]->protocol; ?>" required>
                                </div>
                                <div class="col-sm-3">
                                    <p> smtp </p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="smtp_host" class="col-sm-3 col-form-label">SMTP Host <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="smtp_host" id="smtp_host" type="text" value="<?php echo $get_mail_config[0]->smtp_host; ?>" required>
                                </div>
                                <div class="col-sm-3">
                                    <p> OR /usr/sbin/sendmail </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="smtp_port" class="col-sm-3 col-form-label">SMTP Port <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="smtp_port" id="smtp_port" type="text" value="<?php echo $get_mail_config[0]->smtp_port; ?>" required>
                                </div>

                                <div class="col-sm-3">
                                    <p> 465 </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="smtp_user" class="col-sm-3 col-form-label">Sender Email <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="smtp_user" id="smtp_user" type="email" value="<?php echo $get_mail_config[0]->smtp_user; ?>" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="smtp_pass" class="col-sm-3 col-form-label">Password <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input class="form-control" name="smtp_pass" id="smtp_pass" type="password" value="<?php echo $get_mail_config[0]->smtp_pass; ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mailtype" class="col-sm-3 col-form-label">Mail Type <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="mailtype" id="mailtype" required>
                                        <option value="">Select One</option>
                                        <option value="html" <?php if($get_mail_config[0]->mailtype == 'html'){ echo 'selected'; } ?>>Html</option>
                                        <option value="text" <?php if($get_mail_config[0]->mailtype == 'text'){ echo 'selected'; } ?>>Text</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <p> html </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6 text-right">

                                    <?php if($get_mail_config[0]->is_verified != 1) { ?>
                                    <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                    <a href="javascript:void(0)" id="verify_configuration_btn" class="btn btn-success">Verify</a>
                                    <?php } ?>

                                    <?php } ?>
                                    <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                    <input type="submit" class="btn btn-success btn-large" value="Save Changes">
                                     <?php } ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div id="verify_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send test email</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $attributes = array('id' => 'verify_configuration', 'name' => 'verify_configuration', 'class' => 'form-row px-3');
                echo form_open('#', $attributes);
                ?>
                <div class="col-lg-12 px-4">
                    <div class="form-group">
                        <label for="email_id" class="mb-2">EmailId</label>
                        <input class="form-control" type="email" name="email_id" id="email_id" required>
                         <span id="error"></span>
                    </div>
                </div>
                <div class="col-lg-12 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5" id="save">Send mail</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
$('#verify_configuration_btn').on('click', function () {
    $("#verify_configuration").attr("action", 'c_level/Setting_controller/verify');
    $('#verify_modal').modal('show');
});
</script>
<!-- end content / right -->
