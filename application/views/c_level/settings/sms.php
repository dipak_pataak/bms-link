<style type="text/css">
    #content div.box h5 {
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }

    .c-sms-send .iti {
        position: relative;
        display: block;
    }
</style>


<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Custom SMS</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Custom SMS';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                   <?php
                            $user_data=access_role_permission(68);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">SMS</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="mb-4">
                             <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                            <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2" data-toggle="modal" data-target="#bulkSms" style="font-size: 12px; ">Bulk SMS</a>
                            <a href="javascript:void(0)" class="btn btn-success  btn-sm ml-2 mb-2" data-toggle="modal" data-target="#groupSms" style="font-size: 12px; ">Multi User SMS</a>
                            <?php  }?>
                        </div>

                        <form action="<?php base_url(); ?>c-sms-send" method="post" class="c-sms-send">
                            <div class="form-group row">
                                <label for="receiver_id" class="col-xs-3 col-form-label text-right">Receiver Number <span class="text-danger"> *</span></label>
                                <div class="col-xs-9">
                                    <input type="text" name="receiver_id" class="form-control phone phone-format" id="receiver_id" placeholder="+1 (XXX)-XXX-XXXX" tabindex="1" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="message" class="col-xs-3 col-form-label text-right">Message <span class="text-danger"> *</span></label>
                                <div class="col-xs-9">
                                    <textarea name="message" id="message" class="form-control tinymce" placeholder="Message" rows="7" tabindex="2"></textarea>
                                </div>
                            </div>

                            <div class="form-group  text-right">
                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
                                <?php } ?>
                                <button type="reset" class="btn btn-primary w-md m-b-5" tabindex="4">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Modal -->
<div class="modal fade" id="bulkSms" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--<h4 class="modal-title">Modal Header</h4>-->
            </div>
            <div class="modal-body">
                <a href="<?php echo base_url('assets/c_level/csv/sms_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>
                <?php echo form_open_multipart('retailer-sms-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">

                    <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                </div>

                </form>
            </div>
            <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="groupSms" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Multi User SMS</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php echo form_open('retailer-group-sms-send', array('class' => 'form-row px-3', 'id' => '')); ?>
                <div class="col-md-12 px-4">
                    <div class="form-group row">
                        <label for="customer_id" class="col-md-3 control-label text-right</label>">Customer's Name <span class="text-danger"> *</span></label>
                        <div class="col-md-6">
                            <select class="selectpicker form-control" id="customer_phone" name="customer_phone[]" multiple data-live-search="true" data-selected-text-format="count>2" data-all="false" required>
                                <option value="All">Select All</option>
                                <?php foreach ($customers as $val) { ?>
                                    <option value="<?= $val->phone ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="message" class="col-xs-3 col-form-label text-right">Message <span class="text-danger"> *</span></label>
                        <div class="col-xs-9">
                            <textarea name="message" id="message" class="form-control" placeholder="Message" rows="7" tabindex="2" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 px-4">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success w-md m-b-5">Send</button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        load_country_dropdown('receiver_id', country_code_phone_format); // For phone format
    });
    $('#customer_phone').on('change', function() {
        var thisObj = $(this);
        var isAllSelected = thisObj.find('option[value="All"]').prop('selected');
        var lastAllSelected = $(this).data('all');
        var selectedOptions = (thisObj.val()) ? thisObj.val() : [];
        var allOptionsLength = thisObj.find('option[value!="All"]').length;

        console.log(selectedOptions);
        var selectedOptionsLength = selectedOptions.length;

        if (isAllSelected == lastAllSelected) {

            if ($.inArray("All", selectedOptions) >= 0) {
                selectedOptionsLength -= 1;
            }

            if (allOptionsLength <= selectedOptionsLength) {

                thisObj.find('option[value="All"]').prop('selected', true).parent().selectpicker('refresh');
                isAllSelected = true;
            } else {
                thisObj.find('option[value="All"]').prop('selected', false).parent().selectpicker('refresh');
                isAllSelected = false;
            }

        } else {
            thisObj.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
        }

        $(this).data('all', isAllSelected);
    }).trigger('change');
</script>
