<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>QuickBooks Settings</h1>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">QuickBooks</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <form action="<?php echo $action; ?>" class="form-vertical" id="" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <input type="hidden" name="hidden_user_id" value="<?php echo $user_id; ?>">
                        <input type="hidden" name="hidden_user_type" value="<?php echo $user_type; ?>">
                            <div class="form-group row">
                                <label for="client_id" class="col-sm-3 col-form-label">Client Id <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="client_id" id="client_id" value="<?php echo $quickbooks_setting_data->client_id; ?>" required <?php echo ($connection == "Connected") ? 'readonly' : ''; ?>>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="client_secret" class="col-sm-3 col-form-label">Client Secret <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="client_secret" id="client_secret" value="<?php echo $quickbooks_setting_data->client_secret; ?>" required <?php echo ($connection == "Connected") ? 'readonly' : ''; ?>>
                                </div>
                            </div>
                            <div class="w-100 text-center d-flex justify-content-center">
                                <input type="submit" class="btn btn-sm d-block btn-success mr-3" <?php echo ($connection == "Connected") ? 'disabled' : '';?>  value="<?php echo ($connection == "Connected") ? 'Connected' : 'Connect';?>">
                                <?php if($connection == 'Connected') { ?>
                                  <input type="submit" class="btn btn-sm d-block btn-danger" value="Disconnected">
                                <?php } ?>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
