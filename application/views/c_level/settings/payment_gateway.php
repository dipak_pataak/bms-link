<style>
@media only screen and (max-width: 580px) {
	.col-form-label.text-right {
		text-align: left !important;
	}
}
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Payment Gateway</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Payment Gateway';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                    <?php
                        $user_data=access_role_permission(61);
                        $log_data=employ_role_access($id);
                    ?>

                        <form action="<?php echo base_url('payment-gateway-update'); ?>" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="form-group row">
                                <label for="url" class="col-sm-3 col-form-label text-right">URL<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="url" id="url" class="form-control" value="<?= @$gateway_edit->url; ?>" placeholder="URL" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_name" class="col-sm-3 col-form-label text-right">User Name<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="user_name" id="user_name" class="form-control" value="<?= @$gateway_edit->user_name ?>" placeholder="Username" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label text-right">Password<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="password" name="password" id="password" class="form-control" value="<?= @$gateway_edit->password ?>" placeholder="Payment Password" required>
                                </div>
                            </div>



                            <!-- <div class="form-group row">
                                <label for="mode" class="col-sm-3 col-form-label">Mode<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                                        <option value="1" <?php
                            if ($gateway_edit->mode == 1) {
                                echo 'selected';
                            }
                            ?>>Production</option>
                                        <option value="0" <?php
                            if ($gateway_edit->mode == 0) {
                                echo 'selected';
                            }
                            ?>>Development</option>
                                    </select>
                                </div>
                            </div> -->


                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6 text-right">
                                    <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                    <input type="submit" class="btn btn-success btn-sm" value="Update">
                                    <?php } ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
