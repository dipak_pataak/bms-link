<style type="text/css">
	.cost-cms-main {
			position: relative;
	}
	.print-btn {
			position: absolute;
			right: 0;
			z-index: 5;
	}
  .no-result .print-btn{
      position: relative;
      text-align: right;
  }
  .logo-img {
  	text-align: center;
  }
  .logo-img img{
  	margin: 0 5px;
  }
</style>
<main>
	   				<?php
                            $user_data=access_role_permission(112);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
		<div class="container-fluid cost-cms-main <?php if($b_company_detail->logo == '') echo "no-result";?> ">
				<div class="row">
						<div class="col-12">

								<div class="mb-3">
										<h1><?=$b_company_detail->company_name?> Cost Factor</h1>
										<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
												<ol class="breadcrumb pt-0">
														<li class="breadcrumb-item">
																<a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
														</li>
														<li class="breadcrumb-item active" aria-current="page">wholesaler Cost Factor</li>
												</ol>
										</nav>
								</div>
								<div class="separator mb-5"></div>
						</div>
				</div>
				<div class="print-btn">
				
						<button type="button" class="btn btn-success" onclick="printContent('printableArea')">Print</button>
				</div>
				<div id="printableArea">
						<div class="row">
								<div class="col-xl-12 mb-2 logo-img">
                <?php
                if($b_company_detail->logo != '') { ?>
										<img src="assets/b_level/uploads/appsettings/<?=$b_company_detail->logo?>" alt="company logo">
                <?php } 

                if($c_company_detail->logo != '') { ?>
										<img src="assets/c_level/uploads/appsettings/<?=$c_company_detail->logo?>" alt="company logo">
								<?php } 
                ?>
								</div>


								<div class="col-xl-12 mb-4">
										<div class="card mb-4">
												<div class="card-body">                        
														<div>
									<div class="table-responsive">
																		<table class="table table-bordered table-hover" id="bcost_factor">
																				<thead>
																						<tr>
																								<th>Product Name</th>
																								<th> Cost Factor</th>
																								<th>Discount</th> 
																						</tr>
																				</thead>
																				<tbody>
																						<?php
																						if(count($blevel_cost) > 0){                    
																								foreach ($blevel_cost as $bcost) {
																										?>
																										<tr>
																												<td>
																														<?php echo $bcost->product_name; ?>
																												</td>
																												<td>
																														<?php echo $bcost->dealer_cost_factor; ?>
																												</td>
																												 <td>
																													 <?php echo 100 - (100 * $bcost->dealer_cost_factor) ; ?>
																												</td>                                           
																										</tr>
																								<?php 
																								} 
																						}
																						else {
																						?>
																								<div class="alert alert-danger"> There have no order found..</div>
																						<?php
																						}
																						?>
																				</tbody>
																		</table>
									</div>
														</div>

												</div>
										</div>

								</div>

						</div>
				</div>

		</div>
</main>
<script type="text/javascript">

		function printContent(el) {
				$('body').css({"background-color": "#fff"});
				$('p').css({"font-size": "18px"});
				$('table').css({"font-size": "17px"});
				$('.logo-img').css({"text-align": "center"});
				$('img').css({"margin-left": "5px","margin-right": "5px"});
				var restorepage = $('body').html();
				var printcontent = $('#' + el).clone();
				$('body').empty().html(printcontent);
				window.print();
				$('body').html(restorepage);
				location.reload();
		}
</script>
