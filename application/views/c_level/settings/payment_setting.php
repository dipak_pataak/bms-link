
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Paypal Settings</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Paypal Settings';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Payment</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                    <?php
                        $user_data=access_role_permission(29);
                        $log_data=employ_role_access($id);
                    ?>
                        <form action="<?php echo base_url(); ?>retailer-save-gateway" class="form-vertical" id="insert_customer" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="form-group row">
                                <label for="payment_gateway" class="col-sm-3 col-form-label">Gateway <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2-single" name="payment_gateway" id="payment_gateway" data-placeholder='-- select one --'>
                                        <option value=""></option>
                                        <option value="Paypal" selected>Paypal</option>
                                        <!--<option value="Sandbox">Sandbox</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="payment_mail" class="col-sm-3 col-form-label">Paypal Mail<i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" name="payment_mail" id="payment_mail" class="form-control" placeholder="Payment Mail" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="currency" class="col-sm-3 col-form-label">Currency <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2-single" name="currency" id="currency" data-placeholder="-- select one --" required>
                                        <option value=""></option>
                                        <option value="USD">(USD) U.S. Dollar</option>
                                        <option value="EUR">(EUR) Euro</option>
                                        <option value="AUD">(AUD) Australian Dollar</option>
                                        <option value="CAD">(CAD) Canadian Dollar</option>
                                        <option value="CZK">(CZK) Czech Koruna</option>
                                        <option value="DKK">(DKK) Danish Krone</option>
                                        <option value="HKD">(HKD) Hong Kong Dollar</option>
                                        <option value="Yen">(YEN) Japanese</option>
                                        <option value="MXN">(MXN) Mexican Peso</option>
                                        <option value="NOK">(NOK) Norwegian Krone</option>
                                        <option value="NZD">(NZD) New Zealand Dollar</option>
                                        <option value="PHP">(PHP) Philippine Peso</option>
                                        <option value="PLN">(PLN) Polish Zloty</option>
                                        <option value="SGD">(SGD) Singapore Dollar</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Mode <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <select name="mode" id="mode" class="form-control select2" data-placeholder="-- select one --">
                                        <option value="0">Development</option>
                                        <option value="1">Production</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-6 text-right">
                                     <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                    <input type="submit" class="btn btn-success btn-sm" value="Save Changes">
                                     <?php } ?>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-xs-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <!--                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" id="customer_info">
                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                        <?php // dd($get_customer); ?>
						<div class="table-responsive">
                        <table class="datatable2 table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="8%">SL No.</th>
                                    <th width="15%">Name</th>
                                    <th width="10%">Email</th>
                                    <th width="10%">Currency</th>
                                    <th width="10%">Is Active</th>
                                    <th width="10%">Mode</th>
                                    <th width="12%" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sl = 0 + $pagenum;
                                foreach ($gateway_list as $gateway) {
                                    $sl++;
                                    ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $gateway->payment_gateway; ?></td>
                                        <td><?php echo $gateway->payment_mail; ?></td>
                                        <td><?php echo $gateway->currency; ?></td>
                                        <td><?php
                                            if ($gateway->default_status == 1) {
                                                echo 'Active';
                                            } else {
                                                echo 'Inactive';
                                            }
                                            ?></td>
                                        <td><?php
                                            if ($gateway->status == 1) {
                                                echo 'Production';
                                            } else {
                                                echo 'Development';
                                            }
                                            ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url(); ?>retailer-gateway-edit/<?php echo $gateway->id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="Edit" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                            <a href="<?php echo base_url(); ?>retailer-gateway-delete/<?php echo $gateway->id; ?>" class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="Delete" data-original-title="Delete" onclick="return confirm('Do you want to delete it?')"><i class="simple-icon-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <?php if (empty($gateway_list)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="8" class="text-danger text-center">No record found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
                        </div>
						<?php echo $links; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
