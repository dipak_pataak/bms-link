<!--<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/b_level/card/card_style.css">-->
<style>
.iti.iti--allow-dropdown {
    width:100%
}
.pac-container {
    z-index: 10000 !important;
}
.add_payment {
	border: 1px dashed #cccccc;
	height: 150px;
	padding: 60px 10px;
}
label.error{
    color: red;
}
</style>
<main>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>My card info</h1>
                    <?php
                        $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'My card info';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">My card info</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="mt-5">	
                <div class="row">
                    <div class="col-sm-12">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>               
                    </div>               
                    <?php if (!empty($info)) {
                        foreach ($info as $key => $value) {
                            $cardName = strtolower(getCreditCardType(str_replace('-', '',$value->card_number)));
                    ?>
                        <div class="text-left col-md-6 col-sm-12 mb-3">
                            <div class="card">
                                <div class="card-body">
                                        <div class="row">
                                            <div class="text-left col-md-2 col-sm-12">
                                                <img data-num="<?=$value->card_number;?>" src="<?=base_url()?>assets/c_level/img/<?= $cardName?>.png" style="width:50px;"/>
                                            </div>
                                            <div class="text-left col-md-10 col-sm-12">
                                            <h3 class="m-0"><?= $cardName ?> &bull;&bull;&bull;&bull; <?php echo substr(str_replace('-', '', $value->card_number), -4); ?></h3>
                                            <p class="mb-5">Expires <?php echo $value->expiry_month; ?>/<?php echo $value->expiry_year; ?></p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="text-left col-md-4 col-sm-12">
                                                <p class="m-0">
                                                    <?php $status = $value->is_active;
                                                        if($status == 1) { ?>
                                                             Primary
                                                    <?php } ?>
                                                    <?php if($status == 0) { ?>
                                                        <a onclick="set_primary(<?php echo $value->id; ?>)">Secondary</a> 
                                                    <?php } ?>
                                                </p>
                                            </div>
                                            <div class="text-right col-md-8 col-sm-12">
                                                <a href="javascript:void(0)" onclick="editData('<?php echo $value->id; ?>')"  data-toggle="modal" data-backdrop="static" data-target="#exampleModalRight" title="Edit" class="btn btn-info btn-xs simple-icon-note" data-toggle="tooltip" data-placement="top" data-original-title="Edit"></a>
                                                <a href="<?php echo base_url(); ?>c_level/setting_controller/card_delete/<?php echo $value->id; ?>" title="Delete" class="btn btn-danger btn-xs simple-icon-trash delete-card" data-toggle="tooltip" data-placement="top" data-original-title="Delete"></a>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    <?php } }?>
                    
                        <div class="text-center col-md-6 col-sm-12 mt-5">
                            <div class="add_payment">
                                <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">ADD PAYMENT MENTHOD</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-fix-width collapse mt-5"  id="collapseExample">
                    <div class="col-xl-12 mb-4">

                        <div class="card mb-4">
                            <div class="card-body">

                                <form action="<?php echo base_url(); ?>c_level/setting_controller/card_info_update/"  method="post" enctype="multipart/form-data" id="add-card-form">

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="card_number">Card number <span class="text-danger"> *</span></label>
                                            <input name="card_number" onkeyup="hypen_generate(this.value)" onkeypress="hypen_generate(this.value)" class="form-control card_number" type="text"  placeholder="Card number" maxlength="19" required>
                                            <!-- <p>Please enter the valid card number</p> -->
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="expiry_month">Expiration month <span class="text-danger"> *</span></label>
                                            <select name="expiry_month" class="form-control select2-single" data-placeholder="-- select month --" required>
                                                <?php
                                                $i = 1;
                                                for ($i = 1; $i <= 12; $i++) {
                                                    if ($i < 10) {
                                                        $i = '0' . $i;
                                                    }

                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="expiry_year">Expiration year </label>
                                            <select name="expiry_year" class="form-control select2" data-placeholder="-- select Year --" required>
                                                <?php
                                                $y10 = date('Y') + 10;

                                                for ($i = date('Y'); $i <= $y10; $i++) {
                                                    echo '<option value="' . substr($i, 2) . '">' . $i . '</option>';
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="card_holder_first_name">First Name   <span class="text-danger"> *</span></label>
                                            <input name="card_holder_first_name" class="form-control" type="card_holder_first_name" placeholder="Card holder first name"  value="<?= @$info->card_holder_first_name ?>" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="card_holder_last_name">Last Name   <span class="text-danger"> *</span></label>
                                            <input name="card_holder_last_name" class="form-control" type="card_holder_last_name" placeholder="Card holder last name"  value="<?= @$info->card_holder_last_name ?>" required>
                                        </div>                                
                                        <div class="form-group col-md-6">
                                            <label for="email">Email <span class="text-danger"> * </span></label>
                                            <input type="email" class="form-control" id="email" name="email" required/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                                            <input id="phone_1" class="phone form-control phone_no_type phone-format" type="text" name="phone_number" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)" style="" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="address">Billing Address <span class="text-danger"> * </span></label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder=""  required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="company">Business Name </label>
                                            <input type="text" class="form-control" id="company" name="company">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" id="city" name="city">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" id="state" name="state">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="zip_code">Zip</label>
                                            <input type="text" class="form-control" id="zip_code" name="zip_code">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="country_code">Country Code</label>
                                            <input type="text" class="form-control" id="country_code" name="country_code">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="is_active">Status  <span class="text-danger"> *</span></label>
                                            <select name="is_active" class="form-control select2-single" data-placeholder="-- select is_active --" required>
                                                <option value="1">Active</option>
                                                <option value="0">In active</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" name="package_confirm_page_url" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-sm btn-success w-md m-b-5">Save</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(".delete-card").on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure ??",
            // text: "You will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
               window.location.href = $(this).attr('href');
            } else {
                swal("Your data is safe!");
            }
        });
    });
    function editData(id) {

        $("#myModal").modal("exampleModalRight");

        var formData = {'id': id};
        var submit_url = "<?php echo base_url(); ?>" + "c_level/setting_controller/get_card_info/" + id;

        $.ajax({
            url: submit_url,
            type: 'post',
            data: formData,
            success: function (data) {
                
                var obj = jQuery.parseJSON(data);
                $("#card_number").val(obj.card_number);
                $("#expiry_month").val(obj.expiry_month);
                $("#expiry_year").val(obj.expiry_year);
                $("#card_holder_first_name").val(obj.card_holder_first_name);
                $("#card_holder_last_name").val(obj.card_holder_last_name);
                $("#is_active").val(obj.is_active);
                
                $("#edit_email").val(obj.email);
                $("#edit_phone_1").val(obj.phone_number);
                $("#edit_address").val(obj.address);
                $("#edit_company").val(obj.company);
                $("#edit_city").val(obj.city);
                $("#edit_state").val(obj.state);
                $("#edit_zip_code").val(obj.zip_code);
                $("#edit_country_code").val(obj.country_code);
                $("#edit_country_code").val(obj.country_code);
                $('#edit_phone1').trigger('change');
                $("#id").val(obj.id);
                load_country_dropdown('edit_phone_1', country_code_phone_format); // For phone format 
                $("#myModal").modal("exampleModalRight");

            }, error: function () {

            }

        });







    }

</script>



<div class="modal fade modal-right" id="exampleModalRight" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalRight" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Card</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>



            <div class="modal-body">

                <form action="<?php echo base_url(); ?>c_level/setting_controller/card_info_update/"  method="post" enctype="multipart/form-data" id="update_card">
                    <input type="hidden" name="id" id="id">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="card_number">Card number <span class="text-danger"> *</span></label>
                            <input name="card_number" class="form-control card_number" type="text" onkeyup="hypen_generate(this.value)" onkeypress="hypen_generate(this.value)" id="card_number" placeholder="Card number" maxlength="19" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="expiry_month">Month <span class="text-danger"> *</span></label>
                            <select name="expiry_month" class="form-control select2-single" id="expiry_month" data-placeholder="-- select month --" required>
                                <?php
                                $i = 1;
                                for ($i = 1; $i <= 12; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }

                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                }
                                ?> 
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="expiry_year">Year </label>
                            <select name="expiry_year" class="form-control select2" id="expiry_year" data-placeholder="-- select Year --" required>
                                <?php
                                $y10 = date('Y') + 10;

                                for ($i = date('Y'); $i <= $y10; $i++) {
                                    echo '<option value="' . substr($i, 2) . '">' . $i . '</option>';
                                }
                                ?> 
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="card_holder_first_name">First Name   <span class="text-danger"> *</span></label>
                            <input name="card_holder_first_name" class="form-control" type="card_holder_first_name" placeholder="First Name" id="card_holder_first_name"  required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="card_holder_last_name">Last Name   <span class="text-danger"> *</span></label>
                            <input name="card_holder_last_name" class="form-control" type="card_holder_last_name" placeholder="Last Name" id="card_holder_last_name"  required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="email">Email <span class="text-danger"> * </span></label>
                            <input type="email" class="form-control" id="edit_email" name="email" required/>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                            <input id="edit_phone_1" class="phone form-control phone_no_type phone-format" type="text" name="phone_number" placeholder="+1 (XXX) XXX-XXXX" onkeyup="special_character(1)" style="" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">Address <span class="text-danger"> * </span></label>
                            <input type="text" class="form-control" id="edit_address" name="address" placeholder=""  required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Company </label>
                            <input type="text" class="form-control" id="edit_company" name="company">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="edit_city" name="city">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="state">State</label>
                            <input type="text" class="form-control" id="edit_state" name="state">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="zip_code">Zip</label>
                            <input type="text" class="form-control" id="edit_zip_code" name="zip_code">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="country_code">Country Code</label>
                            <input type="text" class="form-control" id="edit_country_code" name="country_code">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="is_active">Ative status  <span class="text-danger"> *</span></label>
                            <select name="is_active" class="form-control" id="is_active" required>
                                <option value="">Select</option>
                                <option  value="1"> Active </option>
                                <option value="0"> In active </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success w-md m-b-5">Update</button>
                    </div>

                </form>

            </div>


        </div>
    </div>

</div>
<script src="<?php echo base_url(); ?>assets/c_level/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/additional-methods.min.js"></script>
<script type="text/javascript">
    $("#add-card-form").validate({
      rules: {
        card_number: {
          required: true,
          creditcard: true
        }
      }
    });

    $("#update_card").validate({
      rules: {
        card_number: {
          required: true,
          creditcard: true
        }
      }
    });

    $(document).ready(function() {
        load_country_dropdown('phone_1', country_code_phone_format); // For phone format 
        
    });
    function hypen_generate(id) {
        var total_number = id.length;
        if (total_number == 4) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 9) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 14) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
    }
    //=========== its for get special character =========

    function special_character(t) {
        //        Swal.fire(t);
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function(string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            Swal.fire(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }
    //    ======= its for google place address geocomplete =============
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;
                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }
                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                        //                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        Swal.fire('No results found');
                    }
                } else {
                    Swal.fire('Geocoder failed due to: ' + status);
                }
            });
        });
        var edit_places = new google.maps.places.Autocomplete(document.getElementById('edit_address'));
        google.maps.event.addListener(edit_places, 'place_changed', function() {
            var place = edit_places.getPlace();
            console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;
                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }
                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                        //                        console.log(zipcode);
                        $("#edit_city").val(city);
                        $("#edit_state").val(state);
                        $("#edit_zip_code").val(zipcode);
                        $("#edit_country_code").val(country_code);
                    } else {
                        Swal.fire('No results found');
                    }
                } else {
                    Swal.fire('Geocoder failed due to: ' + status);
                }
            });
        });
    });
</script>

<script>
    function set_primary(id)
    {
        $.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '<?php echo base_url() ?>c_level/package_controller/set_primary_card/'+id,
            success: function(msg) 
            {
                location.reload();
            
            }
        });
    }
</script>
