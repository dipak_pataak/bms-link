<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
<style>
    #result_search_filter {
        display:none;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Cost Factor</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Cost Factor';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                <?php
                    $user_data=access_role_permission(30);
                    $log_data=employ_role_access($id);
                ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cost Factor</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="p-1">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <div>
                            <form id="frm_cost_factor" action="<?php echo base_url(); ?>retailer-cost-factor-save" method="post">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="txt_individual_cost_factor" class="mb-2">Cost Factor <span class="text-danger"> * </span></label>
                                        <input type="hidden" name="submit_type" id="submit_type" value="">
                                        <div class="row">
                                            <div class="col-sm-8 mb-2">
                                                <input type="number" name="txt_individual_cost_factor" class="form-control" id="txt_individual_cost_factor" min="0.1" max="2.0" step="0.001">
                                            </div>
                                            <div class="col-sm-2">
                                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                                <input type="button" onClick="submitForm();" class="button btn btn-success" value="Save">
                                                 <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-3 offset-md-3 offset-sm-2">
                                        <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Search..." tabindex="">
                                    </div>
                                </div>
								<div class="table-responsive">
                                <table class="table table-bordered table-hover" id="result_search">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="SellectAll"/></th>
                                            <th>Product List</th>
                                            <th class="text-right">Cost Factor</th>
                                            <th class="text-right">Discount %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($get_product as $single_product) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $single_product->product_id; ?>" class="checkbox_list">  
                                                </td>
                                                <td>
                                                    <?php echo $single_product->product_name; ?>
                                                    <input type="hidden" name="product_id[]" class="form-control" value="<?php echo $single_product->product_id; ?>">
                                                </td>
                                                <?php
                                                $cost_factor = 1;
                                                $discount = 0;
                                                
                                                if ($individual_cost_factor[$single_product->product_id] != '') {
                                                    $cost_factor = $individual_cost_factor[$single_product->product_id];
                                                    $discount = $costfactor_discount[$single_product->product_id];
                                                }
                                                // echo "<br> COst".$cost_factor;
                                                ?>
                                                <td>
                                                    <input type="number" name="individual_cost_factor[]" id="individual_cost_factor_<?php echo $i; ?>" class="form-control text-right" onkeyup="cost_factor_calculation(this.value, '<?php echo $i; ?>')" onblur="cost_factor_calculation(this.value, '<?php echo $i; ?>')" value="<?php echo $cost_factor; ?>" min="0.1" max="2.0" step="0.001">
                                                </td>
                                                <td>
                                                    <input type="text" name="costfactor_discount[]" id="costfactor_discount_<?php echo $i; ?>" class="form-control text-right" value="<?php echo $discount; ?>" readonly>
                                                </td>
                                            </tr>
                                        <?php $cost_factor = $discount = 0;
                                        
                                                } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4" class="text-right">
                                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                                <input type="submit" class="btn btn-success btn-sm" value="Save Change">
                                                <?php } ?>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
								</div>
							</form>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</main>
<script type="text/javascript">
    function cost_factor_calculation(item, sl) {
        var cost_factor = $("#individual_cost_factor_" + sl).val();
        cost_factor = (cost_factor*100).toFixed(2); 
        var discount = (100 - (cost_factor));
        $("#costfactor_discount_" + sl).val(discount.toFixed(2));
//    Swal.fire(discount);
    }
    function submitForm() {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            Swal.fire("Please select atleast one record");
            return false;
        } else {
            $('#submit_type').val('bulk');
            if($("#txt_individual_cost_factor").val() >= 0 && $("#txt_individual_cost_factor").val() <= 1){
                $('#frm_cost_factor').submit();
            }else{
                Swal.fire("Please enter value between 0 to 1");
                return false;
            }    
        }
    }
</script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        oTable = $('#result_search').DataTable( {
            "paging": false,
            "bInfo": false,
        });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#keyword').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    } );
</script>
