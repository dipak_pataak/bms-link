<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>SMS Configuration</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">SMS Configuration</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>


                <div class="card mb-4">
                    <div class="card-body" id="results_menu">
                        <form action="<?php echo base_url('retailer-sms-config-update/' . $sms_config_edit[0]['gateway_id']); ?>" method="post">
                                <div class="form-group row">
                                    <label for="provider_name" class="col-xs-3 col-form-label">Provider Name  <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="provider_name" class="form-control" id="provider_name" placeholder="Provider Name" tabindex="1" value="<?php echo $sms_config_edit[0]['provider_name']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="user_name" class="col-xs-3 col-form-label">User Name  <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="user_name" class="form-control" id="user_name" placeholder="User Name" tabindex="2"  value="<?php echo $sms_config_edit[0]['user']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-xs-3 col-form-label">Password  <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="password" class="form-control" id="password" placeholder="Password" tabindex="3" value="<?php echo $sms_config_edit[0]['password']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-xs-3 col-form-label">Phone  <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone" tabindex="4" value="<?php echo $sms_config_edit[0]['phone']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sender_name" class="col-xs-3 col-form-label">Sender Name <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="sender_name" class="form-control" id="sender_name" placeholder="Sender Name" tabindex="5" value="<?php echo $sms_config_edit[0]['authentication']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="test_sms_number" class="col-xs-3 col-form-label">Test SMS Number <span class="text-danger"> * </span></label>
                                    <div class="col-xs-9">
                                        <input type="text" name="test_sms_number" class="form-control" id="test_sms_number" placeholder="+12062024567 This is the valid format" tabindex="5" value="<?php echo $sms_config_edit[0]['test_sms_number']; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="is_active" class="col-xs-3 col-form-label">Is Active <span class="text-danger"> </span></label>
                                    <div class="col-xs-9">
                                        <select name="is_active" class="is_active form-control select2" id="is_active" data-placeholder='-- select one --'>
                                            <option value="1" <?php if($sms_config_edit[0]['default_status'] == 1){ echo 'selected'; }?>>Active</option>
                                            <option value="0" <?php if($sms_config_edit[0]['default_status'] == 0){ echo 'selected'; }?>>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group  text-right">
                                    <button type="submit" class="btn btn-success w-md m-b-5"  tabindex="6">Update</button>
                                </div>
                            </form>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
