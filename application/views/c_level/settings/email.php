<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .note-group-image-url {
        display: none;
    }
    .note-toolbar {
        z-index: 4 !important;
    }
    .note-modal-footer {
        padding: 0 25px !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Custom Email</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Custom Email';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Email</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="">
            <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
                if ($error != '') {
                    echo $error;
                }
                if ($success != '') {
                    echo $success;
                }
            ?>
        </div>
        <?php
            $user_data=access_role_permission(71);
            $log_data=employ_role_access($id);
        ?>
        <div class="row">
            <div class="col-xl-12 mb-4">        
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url('retailer-email-send'); ?>" method="post" name="customerFrm" id="customerFrm" class="p-3" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="mb-2">Receiver Email</label>
                                    <select class="selectpicker form-control" id="customer_email" name="customer_email[]" multiple data-live-search="true" data-selected-text-format="count>2" data-all="false">
                                        <option value="All">Select All</option>
                                        <?php foreach ($customers as $val) { ?>
                                            <option value="<?= $val->email ?>"><?php echo ucwords($val->first_name) . " " . ucwords($val->last_name); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="custom_email" class="mb-2">Custom Email</label>
                                    <input type="text" name="custom_email" class="form-control" id="custom_email" placeholder="test@gmail.com, test1@gmail.com" tabindex="1" >
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="message" class="mb-2">Message  <span class="text-danger"> *</span></label>
                                    <textarea name="message"  id="message"class="form-control tinymce" placeholder="Message" rows="7"  tabindex="2"></textarea>
                                </div>
                            </div>

                            <div class="form-group  text-right">
                                 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button type="submit" class="btn btn-success w-md m-b-5" tabindex="3">Send</button>
                                 <?php  }?>
                                <button type="reset" onclick="reset_data()" class="btn btn-primary w-md m-b-5"  tabindex="4">Reset</button>
                            </div>

                        </form>
                    </div>
                </div>        
            </div>
        </div>  
    </div>
</main> 
<!-- content / right -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/plugins/summernote/summernote.css" />
<script src="<?php echo base_url(); ?>assets/c_level/plugins/summernote/summernote.js"></script>

<script>
    $(document).ready(function() {
        $('#message').summernote({
            height: 250,
        });
        $(".note-icon-trash").trigger('click');
    });

    function reset_data(){
        $("#customer_email").val('');
        $("#customer_email").selectpicker("refresh");
        $("#message").val('');
        $(".note-editable").html('<p><br></p>');
    }
</script>
<!-- end content / right -->

<script>
    $('#customer_email').on('change', function(){
        var thisObj = $(this);
        var isAllSelected = thisObj.find('option[value="All"]').prop('selected');
        var lastAllSelected = $(this).data('all');
        var selectedOptions = (thisObj.val())?thisObj.val():[];
        var allOptionsLength = thisObj.find('option[value!="All"]').length;
     
        console.log(selectedOptions);
        var selectedOptionsLength = selectedOptions.length;
     
        if(isAllSelected == lastAllSelected){
        
        if($.inArray("All", selectedOptions) >= 0){
            selectedOptionsLength -= 1;      
        }
                
          if(allOptionsLength <= selectedOptionsLength){
          
          thisObj.find('option[value="All"]').prop('selected', true).parent().selectpicker('refresh');
          isAllSelected = true;
          }else{       
            thisObj.find('option[value="All"]').prop('selected', false).parent().selectpicker('refresh');
             isAllSelected = false;
          }
          
        }else{          
            thisObj.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
        }
   
        $(this).data('all', isAllSelected);
    }).trigger('change');
</script>  
