<html lang="en" class="gr__dev_bmslink_biz" style="height: 100%;">
<head>
    <meta charset="UTF-8">
    <title>BMS Link</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/c_level/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/c_level/css/custom_style.css">
</head>

<body id="app-container" class="menu-default menu-sub-hidden sub-hidden" data-gr-c-s-loaded="true" style="position: relative; min-height: 100%; top: 0px;">
    
<main class="" style="opacity: 1;">
    <div class="container-fluid">
		<div class="mt-5">	
			<div class="row">	


				<?php foreach ($card_list as $key => $value) { ?>

					<div class="text-left col-md-6 col-sm-12">
						<div class="card">
							<div class="card-body">
									<a href="<?php echo base_url(); ?>retailer-confirm-paid-upgrade-package/<?php echo $package_id; ?>/<?php echo $value->id; ?>"><div class="row">
										<div class="text-left col-md-2 col-sm-12">
											<img src="<?php echo base_url(); ?>assets/c_level/img/<?php if($value->type==1){
												echo "mastercard_text.jpg";
											} elseif($value->type==2){ echo "payment-card.png"; } ?>" style="width:50px;"/>
										</div>
										<div class="text-left col-md-10 col-sm-12">
										<h3 class="m-0"><?php if($value->type==1){
												echo "Mastercard";
											} ?> &bull;&bull;&bull;&bull; <?php echo substr($value->card_number, 12);  ?></h3>
										<p class="mb-5">Expires <?php echo $value->exp_month.'/'.$value->exp_year; ?></p>
										</div>
									</div></a>
									<hr>

									<?php if($value->status==1){ ?>
										<div class="row">
											<div class="text-right col-md-12 col-sm-12">
												<a href="<?php echo base_url(); ?>retailer-package-edit-card/<?php echo $value->id; ?>" class="">Edit</a>
											</div>
										</div>
									<?php } ?>	
									<?php if($value->status==0){ ?>
										<div class="row">
											<div class="text-left col-md-2 col-sm-12">
												<a onclick="set_primary(<?php echo $value->id; ?>)">Primary</a>
											</div>
											<div class="text-right col-md-10 col-sm-12">
											  <a onclick="remove_card(<?php echo $value->id; ?>);" class="">Remove</a>&nbsp;  &nbsp;
											  <a href="<?php echo base_url(); ?>retailer-package-edit-card/<?php echo $value->id; ?>" class="">Edit</a>
											</div>
										</div>
									<?php } ?>

							</div>
						</div>
					</div>

				<?php } ?>




				<div class="text-center col-md-6 col-sm-12 mt-5">
					<div class="add_payment">
						<button data-toggle="modal" data-target="#myModal">ADD PAYMENT MENTHOD</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<!-- Model -->	
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Card Detail</h4>
      </div>
      <div class="modal-body">

      	<form action="<?php echo base_url(); ?>c_level/Package_controller/save_card" method="POST">
   			
   			<input type="hidden" name="package_id" value="<?php echo $package_id; ?>">

   			<input type="hidden" id="type" name="type" value="0">
   			<span id="image"></span>

	        <div class="form-group col-md-12">
	            <label for="city">Card Number</label>
	            <input type="number" onkeyup="validate_card(this.value);" class="form-control" name="card_number" required="required">
	        </div>

	        <div class="form-group col-md-12">
	            <label for="card_holder_name">Card Holder Name</label>
	            <input type="text" class="form-control" name="card_holder_name" required="required">
	        </div>

	        <div class="form-group col-md-12">
	        	<div class="form-group col-md-4">
	            	 <input type="text" class="form-control" name="exp_month" required="required" placeholder="Month" maxlength="2">
	            </div>	
	        	<div class="form-group col-md-4">
	            	 <input type="text" class="form-control" name="exp_year" required="required" placeholder="Year" maxlength="2">
	        	</div>
	        </div>


	        <button>Submit</button>
        </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Model -->	

</body>
</html>

<script>
	function remove_card(id)
	{
		$.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '<?php echo base_url() ?>c_level/package_controller/remove_card/'+id,
            success: function(msg) 
            {
            	location.reload();
            
            }
        });
	}
</script>


<script>
	function set_primary(id)
	{
		$.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: '<?php echo base_url() ?>c_level/package_controller/set_primary_card/'+id,
            success: function(msg) 
            {
            	location.reload();
            
            }
        });
	}
</script>

<script>
	function validate_card(val)
	{
		var visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
		var mastercard = /^(?:5[1-5][0-9]{14})$/;
		var american_exp = /^(?:3[47][0-9]{13})$/;

		var digit = val.toString().length;
		if(digit ==1 || digit==16) 
		{
			if(val==4) 
			{
				$('#type').val(2);

				$('#image').empty();
				$('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/payment-card.png" height="60px" width="80px">`);
			}
			if (val==5) 
			{
				$('#type').val(1);

				$('#image').empty();
				$('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/mastercard_text.jpg" height="60px" width="80px">`);
			}
			if (val.match(visa)) 
			{
				$('#type').val(2);

				$('#image').empty();
				$('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/payment-card.png" height="60px" width="80px">`);
			}
			if (val.match(mastercard)) 
			{
				$('#type').val(1);

				$('#image').empty();
				$('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/mastercard_text.jpg" height="60px" width="80px">`);
			}
			
			if (val.match(american_exp)) 
			{
				$('#type').val(3);
			}
		}
						

	}
</script>


