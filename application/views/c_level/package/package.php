<!DOCTYPE html>
<html lang="en">
<head>
  <title>Packages</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
<style>
/* Read More Start */
.read-more-state {
  display: none;
}

.read-more-target {
  opacity: 0;
  max-height: 0;
  font-size: 0;
  transition: .25s ease;
}

.read-more-state:checked ~ .read-more-wrap .read-more-target {
  opacity: 1;
  max-height: 999em;
}

.read-more-state ~ .read-more-trigger:before {
  content: '\f063';
	font-family:'FontAwesome';
    font-weight: 900;
}

.read-more-state:checked ~ .read-more-trigger:before {
  content: '\f062';	
	font-family:'FontAwesome';
    font-weight: 900;
}

.read-more-trigger {
  cursor: pointer;
  display: inline-block;
  padding: 0;
  color: #666;
  font-size: .9em;
 margin: 0px 0 0 42px;
	height: 20px;
	width: 20px;
	position: relative;
}
/* Read More CSS End */
div.retailer-heading {
    color: #000;
    position: absolute;
    text-align: center;
    top: 190px;
    left: 10px;
    z-index: 99;
    background: #a0e6d9 !important;
    width: 97.5%;
    border: 7px solid #fff;
}
div.wholesale-heading {
    color: #000;
    background: #a0e6d9 !important;
    border: 7px solid #fff;
}
.pricing-plan-wrapper .pricing-table-plan-name {
    margin: 5px 0;
    font-size: 12px;
}
.p-t-wholesale.pricing-table-column {
	width: 30%;
}
.plan-recommended.pricing-table-column .pricing-table-header-bg {
    min-height: 150px;
}

.pricing-table-column .pricing-table-header-bg {
    min-height: 110px;
    padding-bottom: inherit;
}
img.package {
	max-width: 100%;
	width:100%;
	height: 100%;
}
.pricing-table-column {
	position: relative;
    display: table-cell;
    vertical-align: top;
    width: 33.3%;
    margin: 40px 0 0;
    border-left: 10px solid transparent;
}
.plan-recommended.pricing-table-column .pricing-table-header {
    margin-top: 0;
}
#pricing .wpb_content_element {
	margin-top: 50px;
}
.pricing-table-column.omnichannel-features:nth-child(4) {
    border-left: 0;
    border-right: 10px solid transparent;
}
.pricing-table-column.omnichannel-features:last-child {
	border-left: 30px solid transparent;
}
.pricing-table-row {
	position: relative;
}
h6.retailer {
	margin-top: 55px;
    margin-bottom: 15px;
}
.header-blog {
	height: auto;
	padding: 40px 0;
}
div.retailer-heading, div.wholesale-heading {
	color: #000;
    background: #a0e6d9 !important;
    border: 7px solid #fff;
}
.pricing-table-row .fa.fa-check-circle {
	color: #128686;
}
.pricing-table-row h2, .pricing-table-row p, .pricing-tavle-row, .pricing-table-row h6 {
	font-family: 'Montserrat', sans-serif !important;
}
.pricing-table-row a.button {
	font-family: 'Nunito', sans-serif !important;
	font-weight: normal !important;
}
.pricing-table-column .pricing-table-header {
    background-color: #eff4f8 !important;
}
.pricing-table-features-list {
    margin: 0;
    padding: 0 20px;
    list-style: none;
}
.pricing-table-plan-features {
    max-height: 0;
    overflow: hidden;
    text-align: left;
    transition: max-height .5s ease,padding-top .5s ease;
}
.pricing-table-plan-price .pricing-currency-symbol {
    position: absolute;
    top: 50%;
    right: 101%;
    display: inline-block;
    margin-top: -20px;
    font-size: 1.5rem;
    line-height: 1;
}
.odometer.odometer-auto-theme, .odometer.odometer-theme-default {
    display: inline;
}
.odometer.odometer-auto-theme, .odometer.odometer-theme-minimal {
    display: inline-block;
    display: inline;
    vertical-align: middle;
    vertical-align: auto;
    zoom: 1;
    position: relative;
}
.pricing-table-plan-period {
    position: relative;
    transition: padding-bottom .5s ease;
    margin: 5px 0;
}
.omnichannel-features.pricing-table-column .button--small {
    word-break: keep-all;
}
.button.button--small {
    padding: 7px 20px;
    font-size: .75rem;
    min-width: 100px;
	color: #ffffff;
}

.pricing-table-plan-price {
    position: relative;
    margin: 30px 0 0;
    line-height: 55px;
    font-size: 3rem;
    display: none;
}
.p-basic .pricing-table-plan-price {
	margin-top: 36px;
}
.pricing-table-column .pricing-table-header {
    background-color: #eff4f8 !important;
}
.pricing-table-header {
    overflow: hidden;
    border-radius: 3px;
}

.plan-recommended .pricing-table-body, .plan-recommended .pricing-table-footer, .plan-recommended .pricing-table-header, .plan-recommended .pricing-table-mobile-view-options {
    z-index: 1;
}

.pricing-table-column .pricing-table-header {
    background-color: #eff4f8 !important;
}
.pricing-table-body, .pricing-table-footer, .pricing-table-header, .pricing-table-mobile-view-options {
    position: relative;
}
.pricing-table-header-bg {
    width: 100%;
    height: 0;
    padding-bottom: 68%;
    background-color: #d8f4bf;
    background-position: bottom center;
    background-size: cover;
    background-repeat: no-repeat;
}
.pricing-table-plan-name {
    margin: 5px 0;
    font-size: .875rem;
    letter-spacing: .5px;
	  text-align: center;
}
.pricing-table-plan-value {
	    padding-bottom: 20px;
    margin: 0 15px;
	text-align: center;
}
.button--small, .button--solid {
    background-color: #959696 !important;
}

.btncolor {
    background-color: skyblue !important;
}

.pricing-table-features-list li {
    margin: 0 0 0px 20px;
    padding-left: 0;
    font-size: 12px;
    line-height: 2;
}
.pricing-table-column .pricing-table-header {
    margin-top: 40px;
}
.pricing-table-plan-value {
    border-bottom: 1px solid rgba(37,193,111,.3);
}
.pricing-table-plan-price {
    font-size: 16px;
    line-height: 22px;
	text-align: center;
}
.pricing-table-plan-price .pricing-currency-symbol {
    margin-top: -20px;
}
.plan-recommended.pricing-table-column {
		width: 21%;
		z-index: 1;
	border-left: 0px;
	}

.pricing-table-body {
		background-color: rgb(250, 251, 255);
		padding-bottom: 20px;
	border: 1px solid #eee;
		border-top: 0;
		-webkit-box-shadow: 0 10px 30px 0 rgba(5, 16, 44, .15);
    box-shadow: 0 10px 30px 0 rgba(5, 16, 44, .15);
	}
	.plan-recommended .pricing-table-body {
		margin-top: 0;	
		background-color: #fff !important;
	}
.pricing-table-plan-features {
    padding-top: 40px;
    max-height: inherit;
    transition: none;
    overflow: visible;
}
.p-pro .pricing-table-plan-features {
    padding-top: 10px;
}
/* Packages */
@media only screen and (max-width: 768px) {
	div.retailer-heading {
		position: relative !important;
		top: 45px !important;
		border : 0;
		left: 0 !important;
	}
	.pricing-table-column.omnichannel-features {
		border:0!important;
	}
	.pricing-table-column, div.retailer-heading  {
		width: 70% !important;
		margin-left: auto !important;
		margin-right: auto !important;
	}
	h6.retailer {
	margin-top: 0px;
}
	#footer .widget-title:after {
		left: 45%;
	}
	#footer ul.lsi-social-icons {
		text-align: center !important;
	}
}
@media only screen and (max-width: 480px) {
	.pricing-table-column, div.retailer-heading  {
		width: 100% !important;
	}
}
</style>
</head>
<body>

<main>    
<div id="monthly" class="container">
    <div id="pricing-chart-wrap">
        <div id="pricing-chart">





<?php 
$user_id = $this->session->userdata('user_id');
        $user_info_2 = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
$created_by_info_2 = $this->db->select('*')->from('user_info')->where('id', $user_info_2[0]->created_by)->get()->row();
    $employee_access_2 = 1;
     if(isset($created_by_info_2) && !empty($created_by_info_2)) 
     {
         if ($created_by_info_2->user_type=='c') 
          {
               $employee_access_2 =0;
          } 
    } ?>

    <?php if($employee_access_2==1) {  ?>
                       
                    
            <div id="smaller-plans col-sm-12" class="col-sm-8 offset-sm-2">
                <div class="row pricing-table-row mt-lg">
                    <div class="retailer-heading">
                        <h6 class="pricing-table-plan-name">Retailer</h6>
                    </div>
                    
                    <?php 
                    $user_id = $this->session->userdata('user_id'); 
                    $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();
                    ?>

                   <?php foreach ($package_data as $key => $value) { ?>
                    <div class="p-basic pricing-table-column omnichannel-features">
                        <!-- Pricing table header -->
                        <div class="pricing-table-header">
                            <div class="pricing-table-header-bg">
                                <?php if ($key==1) { ?>
                                    <img src="<?php echo base_url(); ?>super_admin_assets/package_image/adv-gf.gif" alt="Advanced" class="package package-advanced">
                                <?php }else{ ?>
                                    <img src="<?php echo base_url(); ?>super_admin_assets/package_image/BASIC.png" alt="Basic" class="package package-basic">
                                <?php } ?>
                            </div>
                            <h6 class="pricing-table-plan-name retailer">
                                <strong><?php echo $value->package; ?></strong>
                            </h6>

                        </div>
                        <!-- End pricing table header -->
                        <div class="pricing-table-body">
                            <div class="pricing-table-plan-value">
                                <h2 class="pricing-table-plan-price" style="display: inline-block;">
                                    <span class="pricing-currency-symbol"></span>
                                    <span class="plan-price odometer odometer-auto-theme" id="blossom_odometers" data-plan="blossom_odometers">
                                      <?php if ($value->monthly_price!='0.00') { ?>
                                         $<?php echo $value->monthly_price; ?> / month</span>
                                        <br>
                                        + $<?php echo $value->installation_price; ?> one time
                                      <?php }else{ ?>  
                                        FREE
                                      <?php } ?>
                                    
                                </h2>
                                <div class="pricing-table-plan-period">
                                    <p></p>
                                </div>
                                <div class="align-center pricing-table-plan-btn">

                                    <?php if ($value->id == $user_deatail->package_id) { ?>
                                        <span style="color: green;"> <strong>Currently Active</strong></span> <br> 

                                        <?php if ($user_deatail->package_id !=1) { ?>
                                            <?php if ($user_deatail->package_expire != null) { ?>
                                            <span style="color: red;"><?php echo date("M-d-Y", strtotime($user_deatail->package_expire)); ?> </span>
                                            <?php } ?>
                                        <?php } ?>
                                            

                                    <?php }else{ ?>

                                        <?php if ($key==0) { ?>
                                            <a class="button button--ghost button--small" href="<?php echo base_url(); ?>c_level/Package_controller/default_package/<?php echo $value->id; ?>">Downgrade</a>
                                        <?php } ?>

                                        <?php if ($key==1) { ?>
                                            <?php if ($user_deatail->advanced_trail==1 || $user_deatail->pro_trail==1) { ?>
                                                <a class="button button--ghost button--small" href="<?php echo base_url(); ?>retailer-confirm-paid-upgrade-package/<?php echo $value->id; ?>">
                                                    <?php if ($user_deatail->package_id==1) { echo "Upgrade"; } ?>
                                                    <?php if ($user_deatail->package_id==3) { echo "Downgrade"; } ?>
                                            </a>
                                            <?php }else{ ?>
                                                    
                                            <?php 
                                                     $level_id = $this->session->userdata('user_id');
                                                  $card = $this->db->where('created_by',$level_id)->get('c_card_info')->result(); 
                                                 ?>
                                                 <?php if(!empty($card)){?>
                                                <a title="" class="button button--ghost button--small" id="pricing-signup-blossom" target="" href="retailer-trail-upgrade-package/<?php echo $value->id; ?>"> START TRIAL</a>
                                            <?php }else{ ?>
                                                  <a title="" class="button button--ghost button--small" id="pricing-signup-blossom" target="" href="<?php echo base_url(); ?>retailer-credit-card"> START TRIAL</a>
                                            <?php }
                                             ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($key==2) { ?>
                                            <!-- <?php if ($user_deatail->pro_trail==0) { ?>
                                                <a title="" class="button button--ghost button--small" id="pricing-signup-blossom" target="" href="retailer-trail-upgrade-package/<?php echo $value->id; ?>"> START TRIAL</a>
                                            <?php } ?> -->
                                            <?php if ($user_deatail->pro_trail==1 || $user_deatail->advanced_trail==1) { ?>
                                                <a class="button button--ghost button--small" href="<?php echo base_url(); ?>retailer-confirm-paid-upgrade-package/<?php echo $value->id; ?>">Upgrade</a>
                                            <?php }else{ ?>

                                                  <?php 
                                                   $level_id = $this->session->userdata('user_id');
                                                  $card = $this->db->where('created_by',$level_id)->get('c_card_info')->result();  
                                                 ?>
                                                 <?php if(!empty($card)){?>
                                                <a title="" class="button button--ghost button--small" id="pricing-signup-blossom" target="" href="retailer-trail-upgrade-package/<?php echo $value->id; ?>"> START TRIAL</a>
                                                  <?php }else{ ?>
                                                  <a title="" class="button button--ghost button--small" id="pricing-signup-blossom" target="" href="<?php echo base_url(); ?>retailer-credit-card"> START TRIAL</a>
                                            <?php }
                                             ?>
                                            <?php } ?>
                                        <?php } ?>
                                    
                                    <?php } ?>



                                </div>
                            </div>
                            <?php $package_option_data = $this->Package_model->get_package_option_list(); ?>
                            <!-- End pricing table plan value -->
                            <div class="pricing-table-plan-features">
                                <div class="pricing-plan-wrapper" style="">
                                    <ul class="pricing-table-features-list">
                                        <?php foreach ($package_option_data as $key => $val) { ?>
                                            
                                            <?php if($value->id==1) { ?>
                                                <?php if ($val->basic==1) { ?>
                                                    <li class=" feature-tool-tip">
                                                        <i class="fa fa-check-circle"></i>
                                                        <?php echo $val->title; ?>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>


                                            <?php if($value->id==2) { ?>
                                                <?php if ($val->advanced==1) { ?>
                                                    <li class=" feature-tool-tip">
                                                        <i class="fa fa-check-circle"></i>
                                                        <?php echo $val->title; ?>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>


                                            <?php if($value->id==3) { ?>
                                                <?php if ($val->pro==1) { ?>
                                                    <li class=" feature-tool-tip">
                                                        <i class="fa fa-check-circle"></i>
                                                        <?php echo $val->title; ?>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>

                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <!-- End pricing table plan features -->
                        </div>
                        <!-- End pricing table body -->
                    </div>

                   <?php } ?> 


                    
                </div>
            </div>

<?php } ?>


        </div>
    </div>
</div>
</main>
</body>
</html>
