<main class="" style="opacity: 1;">
<style>
.input-group.correct-coupon{
    justify-content: flex-end;
}
.coupon-edit-btn{
    width: 20px;
    height: 20px;
    margin-left: 0.5rem;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #ffc107;
    font-weight: bold;
    border-radius: 4px;
    cursor: pointer;
    font-size: 11px;
    box-shadow: -1px 5px 15px 2px rgba(0,0,0,.04), -2px 5px 6px 3px rgba(0,0,0,.04);
}
</style>
    <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>


    <div class="container-fluid">
        <div class="card mb-4">
        <div class="card-body">
           

    <form action="<?php echo base_url(); ?>retailer-paid-upgrade-package" id="MyForm" onsubmit="return PymentValidation()" method="post" accept-charset="utf-8">
<input type="hidden" id="last_applied_coupon" value="">
            <input type="hidden" name="package_id" id="package_id" value="<?php echo $package_details->id; ?>"> 
            <input type="hidden" name="coupon_status" id="coupon_status" value="0">
            
            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <h1>Package Type - <?php echo $package_details->package; ?></h1>
                </div>

                <div class="text-right col-md-3 col-sm-12">
                  <a href="<?php echo base_url(); ?>package" class="btn btn-primary btn-sm d-block float-right customer_btn">Change Package</a>
                </div>
            </div>

            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <p>Duration</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                     <?php $temp_duration =  $this->session->userdata('temp_duration'); ?>
                    <!-- <select name="duration" id="duration" class="form-control" onchange="get_package_amount(this.value); get_old_rmaining_amount(this.value); setCard(this.value);  " autocomplete="off">
                        <option value="monthly" <?php if (isset($temp_duration) && $temp_duration=='monthly') {
                                echo "selected"; } ?>>Monthly</option>
                        <option value="yearly" <?php if (isset($temp_duration) && $temp_duration=='yearly') {
                                echo "selected"; } ?>>Yearly</option>
                    </select> -->
                    <div class="custom-control custom-checkbox custom-control-inline">
                      <input  type="radio" id="monthly" name="duration" value="monthly" class="custom-control-input"  onchange="get_package_amount('monthly'); get_old_rmaining_amount('monthly'); setCard('monthly');" checked>
                      <label class="custom-control-label" for="monthly">Monthly</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                      <input  type="radio" id="yearly" name="duration" value="yearly" class="custom-control-input"  onchange="get_package_amount('yearly'); get_old_rmaining_amount('yearly'); setCard('yearly');  ">
                      <label class="custom-control-label" for="yearly">Yearly</label>
                    </div>
                </div>
            </div>

            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <p>Installation Amount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <p>
                        <div class="instalation_amount"><?php echo $currencys->currency; ?>0.00</div>
                        <input type="hidden" id="instalation_amount" class="form-control text-right" value="0" readonly="">
                    </p>
                </div>
            </div>
            
            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <p>Recurring Amount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <p>
                       <div class="amount"></div>
                       <input type="hidden" id="amount" class="form-control text-right" value="" readonly="">
                    </p>
                </div>
            </div>

            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <p>Old Remaining Amount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                  <p><div class="old_remaining_amount"><?php echo $currencys->currency; ?>0.00</div>
                        <input type="hidden" id="old_remaining_amount" class="form-control text-right" value="" readonly="">
                  </p>
                </div>
            </div>

            <div class="row">    
                <div class="text-left col-md-9 col-sm-12">
                    <p>Discount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <p><div class="discount"><?php echo $currencys->currency; ?>0.00</div>
                        <input type="hidden" class="form-control text-right" id="discount" value="0.00" readonly="">
                    </p>
                </div>
            </div>
            <hr>
            <div class="row">   
                <div class="text-left col-md-9 col-sm-12">
                    <p>Total Amount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <h2>
                        <p><div class="total_amount"></div></p>
                        <input type="hidden" id="total_amount" class="form-control text-right" value="" readonly="">
                    </h2>
                    <input type="hidden" id="fixed_total_amount" class="form-control text-right" value="" readonly="">
                </div>
            </div>

        <?php if ($user_current_detail->package_id!=3){ ?>
            <div class="row">   
                <div class="text-right col-lg-3 col-md-6 col-sm-12 offset-lg-9 offset-md-6">
                    <div class="input-group input-group-sm mb-3" id="hide_coupon_box" >
                          <input type="text" id="coupon_code" class="form-control" name="coupon_code" placeholder="Coupon code">
                          <div class="input-group-append">
                            <span class="coupon_submit_button">
                                <a class="btn btn-success mb-0 rounded-0 text-light btn-sm" onclick="check_coupon()"> Apply</a>
                            </span>
                          </div>
                    </div>
                    <span id="coupon_not_found" style="color: red;"></span>
                    <!-- <input type="text" id="coupon_code" name="coupon_code" placeholder="Coupon code">
                    <span class="coupon_submit_button">
                        <a class="btn btn-success btn-xs" onclick="check_coupon()"> Submit</a>
                    </span> -->
                </div>
            </div>
        <?php } ?>


        </div>
        </div>
        <div class="mt-5">  
            <div class="row">   
                <div class="text-left col-xl-7 col-lg-8 col-sm-12 offset-lg-4 offset-xl-5">
                    <div class="card">
                        <div class="card-body">
                        <div class="row">

                            <?php if (!empty($card_details)) { ?>
                                <div class="text-left col-md-9 col-9">
                                    <p>Payment</p>
                                </div>
                                <div class="text-left col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="text-left col-md-2 col-sm-12">
                                              <img src="<?php  echo base_url().'assets/c_level/img/unknown.png'; ?>" style="width:50px;"/>
                                        </div>
                                        <div class="text-left col-md-5 col-sm-12">
                                            <div class="" style="position:absolute; top: -34px;right: 75px;">
                                                <a href="<?php echo base_url(); ?>retailer-credit-card/" class="phone_email_link">Edit</a>
                                            </div>
                                            <p class="m-0">&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; <?php  echo substr($card_details->card_number, 12); ?></p>
                                            <p class="mb-5">Mastercard - Expries <?php  echo $card_details->expiry_month; ?>/<?php  echo $card_details->expiry_year; ?></p>
                                        </div>
                                        <div class="col-md-5 col-sm-12">
                                                <input type="hidden" name="card_id" value="<?php if (isset($card_details->id)) { echo $card_details->id; } ?>">
                                                <input type="hidden" name="card_number" placeholder="1234 5678 9012 3456" id="card_number" autocomplete="off" class="required" style="background-position: 2px -163px, 260px -61px;" value="<?php if (isset($card_details->card_number)) { echo $card_details->card_number; } ?>">
                                                <input type="hidden" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month" autocomplete="off" value="<?php if (isset($card_details->expiry_month)) { echo $card_details->expiry_month; } ?>">
                                                <input type="hidden" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year" autocomplete="off" value="<?php if (isset($card_details->expiry_year)) { echo $card_details->expiry_year; } ?>">                    
                                                <input type="hidden" name="card_holder_name" placeholder="Name on card" id="name_on_card" autocomplete="off" value="<?php if (isset($card_details->card_holder_last_name)) { echo $card_details->card_holder_first_name.' '.$card_details->card_holder_last_name; } ?>">
                                            <div class="input-group input-group-sm mb-3">
                                            <input type="password" name="cvv" minlength="3" maxlength="4" id="cvv" autocomplete="off" class="form-control" placeholder="CVV" required="required">
                                                  <div class="input-group-append">
                                                  <button type="submit" class="btn btn-success btn-sm d-block float-right customer_btn mb-0" id="gqi" autocomplete="off">Pay Now</button>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            <?php }else{ ?> 
                                <a class="btn btn-primary btn-sm d-block float-right customer_btn" href="<?php echo base_url(); ?>retailer-credit-card">ADD PAYMENT MENTHOD</a>
                            <?php } ?>

                        </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="text-left col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-body pb-2">
                        <div class="row">

                            <input type="hidden" name="card_id" value="<?php if (isset($card_details->id)) { echo $card_details->id; } ?>">
                            <input type="hidden" name="card_number" placeholder="1234 5678 9012 3456" id="card_number" autocomplete="off" class="required" style="background-position: 2px -163px, 260px -61px;" value="<?php if (isset($card_details->card_number)) { echo $card_details->card_number; } ?>">
                            <input type="hidden" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month" autocomplete="off" value="<?php if (isset($card_details->expiry_month)) { echo $card_details->expiry_month; } ?>">
                            <input type="hidden" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year" autocomplete="off" value="<?php if (isset($card_details->expiry_year)) { echo $card_details->expiry_year; } ?>">                    
                            <input type="hidden" name="card_holder_name" placeholder="Name on card" id="name_on_card" autocomplete="off" value="<?php if (isset($card_details->card_holder_last_name)) { echo $card_details->card_holder_first_name.' '.$card_details->card_holder_last_name; } ?>">

                            <div class="text-right col-md-6 col-sm-12">
                                <input type="password" name="cvv" minlength="3" maxlength="4" id="cvv" autocomplete="off" class="form-control" placeholder="CVV" required="required">
                            </div>
                                                  
                                <button type="submit" class="btn btn-success btn-sm d-block float-right customer_btn" id="gqi" autocomplete="off">Pay Now</button>

                        </div>
                        </div>
                    </div> -->
                </div>
    </form>

            </div>
        </div>
    </div>
</main>


<script>

    $(document).ready(function () {
        get_package_amount("monthly");
        var backPosition = '2px -121px, 260px -87px';
        $('#card_number').css("background-position", backPosition);
    });


    function get_package_amount(duration)
    {
        var package_id = $('#package_id').val();
         $.ajax({
            url: "<?php echo base_url(); ?>c_level/package_controller/get_package_detal/"+package_id,
            type: 'post',
            dataType: "JSON",
            success: function (data) {

               $('#instalation_amount').val(data.installation_price);
               $('.instalation_amount').text('<?php echo $currencys->currency; ?>'+data.installation_price);

                if (duration=='monthly') 
                {
                    $('#total_amount').val(data.installation_price);
                    $('#fixed_total_amount').val(data.installation_price);

                    if (data.installation_price < 0) 
                    {
                        $('.total_amount').text('<?php echo $currencys->currency; ?>0.00');
                    }else{
                        $('.total_amount').text('<?php echo $currencys->currency; ?>'+data.installation_price);
                    }
                    

                    $('.amount').empty();
                    $('.amount').text('<?php echo $currencys->currency; ?>'+data.monthly_price);

                    $('.discount').text('0.00');
                    $('#discount').val(0);
                    $('#coupon_status').val(0);

                    $('#hide_coupon_box').empty();
                    $('#hide_coupon_box').append(`<input type="text" id="coupon_code" class="form-control" name="coupon_code" placeholder="Coupon code">
                                                    <div class="input-group-append">
                                                        <span class="coupon_submit_button">
                                                            <a class="btn btn-success mb-0 rounded-0 text-light btn-sm" onclick="check_coupon()"> Submit</a>
                                                        </span>
                                                    </div>`);
                }
                if (duration=='yearly') 
                {
                    $('#total_amount').val(data.installation_price);
                    $('#fixed_total_amount').val(data.installation_price);

                    if (data.installation_price < 0) 
                    {
                        $('.total_amount').text('<?php echo $currencys->currency; ?>0.00');
                    }else{
                        $('.total_amount').text('<?php echo $currencys->currency; ?>'+data.installation_price);
                    }

                    $('.amount').empty();
                    $('.amount').text('<?php echo $currencys->currency; ?>'+data.yearly_price);

                    $('.discount').text('0.00');
                    $('#discount').val(0);
                    $('#coupon_status').val(0);

                    $('#hide_coupon_box').empty();
                    $('#hide_coupon_box').append(`<input type="text" id="coupon_code" class="form-control" name="coupon_code" placeholder="Coupon code">
                                                    <div class="input-group-append">
                                                        <span class="coupon_submit_button">
                                                            <a class="btn btn-success mb-0 rounded-0 text-light btn-sm" onclick="check_coupon()"> Submit</a>
                                                        </span>
                                                   </div>`);
                }
               
            },error: function() {

            }
        });
     }
</script>

<script>
function edit_and_reset_coupon()
{
    // var total_amount_reset = $('#total_amount').val();
    // var discount_reset = $('#discount').val();
    // var reset_total_amount = +total_amount_reset+ + discount_reset;
    //
    // $('.total_amount').text(reset_total_amount);
    // $('#total_amount').val(reset_total_amount);
    //
    //
    // $('.discount').text('0.00');
    // $('#discount').val(0);
    // $('#coupon_status').val(0);

    var last_applied_coupon = $('#last_applied_coupon').val();

    $('#hide_coupon_box').empty();
    $('#hide_coupon_box').append(`<input type="text" id="coupon_code" class="form-control" name="coupon_code" placeholder="Coupon code" value="`+last_applied_coupon+`">
                                    <div class="input-group-append">
                                        <span class="coupon_submit_button">
                                            <a class="btn btn-success mb-0 rounded-0 text-light btn-sm" onclick="check_coupon()"> Submit</a>
                                        </span>
                                   </div>`);
}
</script>

<script>
    function check_coupon()
    {
        var coupon_code = $('#coupon_code').val();
        if (coupon_code=='') 
        {
            Swal.fire('Please Enter coupon Code');
            return false;
        } 

            $.ajax({
                url: "<?php echo base_url(); ?>c_level/package_controller/get_coupon_detail/"+coupon_code,
                type: 'post',
                dataType: "JSON",
                success: function (data) {

                    if(data!=false) 
                    {
                        $('#coupon_not_found').empty();
/*******************/
                        var fixed_total_amount = $('#fixed_total_amount').val();
                        var total_amount = $('#total_amount').val();
                        var min_purchase = parseInt(data.min_purchase)

                        if(data.type==1) 
                        {
                            var flat = parseInt(data.flat)
                            if(total_amount < min_purchase) 
                            {
                               $('#discount').val('0.00');
                               $('.discount').empty();
                               $('.discount').text('<?php echo $currencys->currency; ?>0.00');

                               $('#coupon_status').val(0);
                               $('#coupon_not_found').text('Coupon valid on Minimum Purchase of '+data.min_purchase); 
                               return false;
                            }
                            else
                            {
                               var new_amount = fixed_total_amount - flat;
                               $('#total_amount').val(new_amount);

                               if (new_amount < 0) 
                                {
                                    $('.total_amount').text('<?php echo $currencys->currency; ?>0.00');
                                }else{
                                    $('.total_amount').text('<?php echo $currencys->currency; ?>'+new_amount);
                                }

                               $('#discount').val(data.flat);
                               $('.discount').empty();
                               $('.discount').text('<?php echo $currencys->currency; ?>'+data.flat);

                               $('#coupon_status').val(1);
                            }

                        }
                        if(data.type==2) 
                        {
                            if(total_amount < min_purchase) 
                            {
                               $('#discount').val('0.00');
                               $('.discount').empty();
                               $('.discount').text('<?php echo $currencys->currency; ?>0.00');

                               $('#coupon_status').val(0);
                               $('#coupon_not_found').text('Coupon valid on Minimum Purchase of '+data.min_purchase); 
                               return false;
                            }
                            else
                            {
                               var percent = parseInt(data.percent);
                               var max_discount = parseInt(data.max_discount);

                               console.log(percent);
                               console.log(total_amount);
                               percent_ot_total_amount = fixed_total_amount * percent/100;

                                discount = 0;
                               if (percent_ot_total_amount > max_discount) 
                               {
                                    discount = max_discount;
                               }else{
                                    discount = percent_ot_total_amount;
                               }

                                console.log(discount);

                               var new_amount = fixed_total_amount - discount;
                               $('#total_amount').val(new_amount);

                                if (new_amount < 0) 
                                {
                                    $('.total_amount').text('<?php echo $currencys->currency; ?>0.00');
                                }else{
                                    $('.total_amount').text('<?php echo $currencys->currency; ?>'+new_amount);
                                }

                               $('#discount').val(discount);
                               $('.discount').empty();
                               $('.discount').text('<?php echo $currencys->currency; ?>'+discount);

                               $('#coupon_status').val(1);

                            }

                        }

                        $('#hide_coupon_box').empty();
                        $('#hide_coupon_box').append(`
                                <input type="hidden" name="coupon_code" value="`+data.coupon+`">
                                <h6 class="font-weight-bold text-success text-uppercase mb-0">`+data.coupon+`</h6>&nbsp;<a class="coupon-edit-btn" onclick="edit_and_reset_coupon();"> <i class="simple-icon-pencil"></i></a>`);
                        //$('.coupon_submit_button').hide();
                        // $('.coupon_submit_button').text('Coupon Applied');
                        $('#hide_coupon_box').addClass('correct-coupon');
                        $('#last_applied_coupon').val(data.coupon);

/*******************/
                    }else{
                        $('#coupon_status').val(0);
                        $('#coupon_not_found').text('Invalid Coupon');
                    }

                },error: function() {

                }
            });
    }
</script>



<script>

    $(document).ready(function () {
        var time = $('#duration').val();
        if (time='yearly') 
        {
            console.log(time);
           get_old_rmaining_amount('yearly'); 
        }
    });


    function get_old_rmaining_amount(time)
    {
        if(time=='yearly') 
        {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/package_controller/old_package_remaining_amount/",
                type: 'post',
                dataType: "JSON",
                success: function (data) {

                    if (data!=false) 
                    {
                        $('#old_remaining_amount').val(data);
                        $('.old_remaining_amount').empty();
                        $('.old_remaining_amount').text('-'+data);

                      var total_amount =  $('#total_amount').val();
                      var new_amount = total_amount-data;
                        $('#amount').val(total_amount);
                        $('#total_amount').val(new_amount);

                            if (new_amount < 0) 
                            {
                                $('.total_amount').text('<?php echo $currencys->currency; ?>0.00');
                            }else{
                                $('.total_amount').text('<?php echo $currencys->currency; ?>'+new_amount);
                            }

                    }

                }

            });
        }

        if(time=='monthly') 
        {
            $('#old_remaining_amount').val(0);
            $('.old_remaining_amount').empty();
            $('.old_remaining_amount').text('<?php echo $currencys->currency; ?>0.00');

            $('#amount').val(0);
            $('#discount').val(0); 
            $('.discount').empty();
            $('.discount').text('<?php echo $currencys->currency; ?>0.00');
        }

       
    } 



</script>
