<link rel="stylesheet" href="<?php echo base_url(); ?>assets/c_level/card/card_style.css" />

<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Payment</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                               Payment
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?php echo base_url(); ?>retailer-paid-upgrade-package" id="MyForm" onsubmit="return PymentValidation()" method="post" accept-charset="utf-8">

                                        <table class="table table-bordered ">
                                            <tbody>

                                                <input type="hidden" name="package_id" id="package_id" value="<?php echo $package_details->id; ?>">   
                                                <input type="hidden" name="coupon_status" id="coupon_status" value="0">   
                                                <?php $temp_duration =  $this->session->userdata('temp_duration'); ?>
                                                <tr>
                                                    <td>Duration</td>
                                                    <td>
                                                        <select name="duration" id="duration" class="form-control" onchange="get_package_amount(this.value); get_old_rmaining_amount(this.value); setCard(this.value);  " autocomplete="off">
                                                            <option value="monthly" <?php if (isset($temp_duration) && $temp_duration=='monthly') {
                                                                echo "selected"; } ?>>Monthly</option>
                                                            <option value="yearly" <?php if (isset($temp_duration) && $temp_duration=='yearly') {
                                                                echo "selected"; } ?>>Yearly</option>
                                                        </select>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Old Remaining Amount</td>
                                                    <td>
                                                        <input type="text" id="old_remaining_amount" class="form-control text-right" value="" readonly="">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Amount</td>
                                                    <td>
                                                        <input type="text" id="amount" class="form-control text-right" value="" readonly="">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Total Amount</td>
                                                    <td>
                                                        <input type="text" id="total_amount" class="form-control text-right" value="" readonly="">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Discount</td>
                                                    <td>
                                                        <input type="text" class="form-control text-right" id="discount" value="0.00" readonly="">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Coupon Code</td>
                                                    <td>
                                                        <span id="coupon_not_found" style="color: red;"></span>
                                                       <input type="text" id="coupon_code" name="coupon_code" class="form-control">
                                                       <a class="btn btn-success btn-xs" onclick="check_coupon()"> Submit</a>
                                                   
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>

                                        <div class="card_method" style="">
                                            <div id="paymentForm">
                                                <input type="hidden" name="card_id" value="<?php if (isset($card_details->id)) { echo $card_details->id; } ?>">
                                                <ul>
                                                    <li>
                                                        <input type="text" name="card_number" placeholder="1234 5678 9012 3456" id="card_number" autocomplete="off" class="required" style="background-position: 2px -163px, 260px -61px;" value="<?php if (isset($card_details->card_number)) { echo $card_details->card_number; } ?>">
                                                    </li>

                                                    <li class="vertical">

                                                        <ul>

                                                            <li>
                                                                <label for="expiry_month">Month</label>
                                                                <input type="text" name="expiry_month" placeholder="MM" maxlength="2" id="expiry_month" autocomplete="off" value="<?php if (isset($card_details->exp_month)) { echo $card_details->exp_month; } ?>">
                                                            </li>

                                                            <li style="margin-left: 5px;">
                                                                <label for="expiry_year">Year</label>
                                                                <input type="text" name="expiry_year" placeholder="YY" maxlength="2" id="expiry_year" autocomplete="off" value="<?php if (isset($card_details->exp_year)) { echo $card_details->exp_year; } ?>">
                                                            </li>

                                                            <li>
                                                                <label for="cvv">CVV</label>
                                                                <input type="text" name="cvv" minlength="3" maxlength="4" id="cvv" autocomplete="off">
                                                            </li>

                                                        </ul>

                                                    </li>

                                                    <li>
                                                        <label for="name_on_card">Name on card</label>
                                                        <input type="text" name="card_holder_name" placeholder="Name on card" id="name_on_card" autocomplete="off" value="<?php if (isset($card_details->card_holder_name)) { echo $card_details->card_holder_name; } ?>">
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>


                                        <div class="col-lg-6 offset-lg-6 text-right" style="margin-top: 10px">
                                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn" id="gqi" autocomplete="off">Pay Now</button>
                                        </div>

                    </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>





<script type="text/javascript">

        $(document).ready(function () {
            //var grand_total = parseFloat($('#grand_total').val());
            //$('#due').val(grand_total);
            calDuePaid();
        });


function PymentValidation(){

    var grand_total = parseFloat($('#grand_total').val());
    var due_amm = parseFloat($('#due_amm').val());

    var paid_amount = parseFloat($('#paid_amount').val());
    var pval = '<?=$pval?>';

    if(grand_total == due_amm){


        if(paid_amount<pval){
            Swal.fire("Can't pay less than "+pval);
            $('#paid_amount').val(pval);
            calDuePaid();
                return false;
        }
        if(paid_amount>grand_total){
            Swal.fire("Can't pay more than "+grand_total);
            $('#paid_amount').val(pval);
            calDuePaid();
                return false;
        }


    } else {

        if(1>paid_amount){
            Swal.fire("Can't pay less than 1");
            $('#paid_amount').val(pval);
            calDuePaid();
                return false;
        }

        if(due_amm<paid_amount){
            Swal.fire("Can't pay more than "+due_amm);
            $('#paid_amount').val(pval);
            calDuePaid();
                return false;
        }

    }
}

// $('#MyForm').on('submit', function() {

   
// });


        function calDuePaid(){

            var grand_total = parseFloat($('#grand_total').val());
            var due_amm = parseFloat($('#due_amm').val());

            var paid_amount = parseFloat($('#paid_amount').val());
            var pval = '<?=$pval?>';

            if(isNaN(paid_amount)) {
                var paid_amount = 0;
            }else{
                var paid_amount = parseFloat($('#paid_amount').val());
            }

            var due = (due_amm-paid_amount);
            $('#due').val(due.toFixed(2));

        }




        $('#card_number').keyup(function()
        {
            $(this).val(function(i, v)
            {
                if(v.length>19){
                    var str = v.substring(0,19);
                    var str = str.replace(/[^\d]/g, '').match(/.{1,4}/g);
                    return str ? str.join('-') : '';
                }else{
                    var v = v.replace(/[^\d]/g, '').match(/.{1,4}/g);
                    return v ? v.join('-') : '';
                }
            });

        });

        function setCard(value){

            if(value==='card'){

                $(".card_method").slideToggle();
                $('#check_number').val('');
                $('#check_image').val('');


                var submit_url = "<?php echo base_url(); ?>"+"c_level/setting_controller/getCard/";

                $.ajax({
                    url: submit_url,
                    type: 'post',
                    success: function (data) {

                        var obj = jQuery.parseJSON(data);

                        $("#card_number").val(obj.card_number);
                        $("#expiry_month").val(obj.expiry_month);
                        $("#expiry_year").val(obj.expiry_year);
                        $("#name_on_card").val(obj.card_holder_first_name + ' ' + obj.card_holder_last_name);
                        $('#card_number').trigger('keyup');
                    },error: function() {

                    }

                });


                $("#MyForm").attr("action", '<?=base_url()?>c_level/tmspayment_gateway');

                $('.check_method').slideUp();


            }else if(value==='cash'){

                $("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

                $('#card_number').val('');
                $('#card_holder_name').val('');
                $('#expiry_date').val('');
                $('#cvv').val('');

                $('#check_number').val('');
                $('#check_image').val('');

                $('.card_method').slideUp();
                $('.check_method').slideUp();


            }else if(value==='check'){
                $("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

                $(".check_method").slideToggle();

                $('#card_number').val('');
                $('#card_holder_name').val('');
                $('#expiry_date').val('');
                $('#cvv').val('');
                
                $('.card_method').slideUp();

            }else if(value==='paypal'){

                $("#MyForm").attr("action", '<?=base_url()?>c_level/make_payment/order_payment_update');

                $('#card_number').val('');
                $('#card_holder_name').val('');
                $('#expiry_date').val('');
                $('#cvv').val('');

                $('#check_number').val('');
                $('#check_image').val('');

                $('.card_method').slideUp();
                $('.check_method').slideUp();

            }

        }




        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);   
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);   
        });


</script>

<script src="<?php echo base_url();?>assets/c_level/card/creditCardValidator.js"></script>

<script>
function cardFormValidate(){
    var cardValid = 0;
      
    //card number validation
    $('#card_number').validateCreditCard(function(result){ console.log(result);

        var cardType = (result.card_type == null)?'':result.card_type.name;
        if(cardType == 'visa'){
            var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
        }else if(cardType == 'visa_electron'){
            var backPosition = result.valid?'2px -205px, 260px -87px':'2px -163px, 260px -61px';
        }else if(cardType == 'masterCard'){
            var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
        }else if(cardType == 'maestro'){
            var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
        }else if(cardType == 'discover'){
            var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
        }else if(cardType == 'amex'){
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }else{
            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
        }

        $('#card_number').css("background-position", backPosition);
        if(result.valid){
            $("#card_type").val(cardType);
            $("#card_number").removeClass('required');
            cardValid = 1;
        }else{
            $("#card_type").val('');
            $("#card_number").addClass('required');
            cardValid = 0;
        }
    });
      
    //card details validation
    var cardName = $("#name_on_card").val();
    var expMonth = $("#expiry_month").val();
    var expYear = $("#expiry_year").val();
    
    var cvv = $("#cvv").val();
    var regName = /^[a-z ,.'-]+$/i;
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^19|20|21|22|23|24|25|26|27|28|29|30|31$/;
    var regCVV = /^[0-9]{3,4}$/;
    if (cardValid == 0) {
        $("#card_number").addClass('required');
        $("#card_number").focus();
        return false;
    }else if (!regMonth.test(expMonth)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").addClass('required');
        $("#expiry_month").focus();
        return false;
    }else if (!regYear.test(expYear)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").addClass('required');
        $("#expiry_year").focus();
        return false;
    }else if (!regCVV.test(cvv)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").addClass('required');
        $("#cvv").focus();
        return false;
    }else if (!regName.test(cardName)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").addClass('required');
        $("#name_on_card").focus();
        return false;
    }else{
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").removeClass('required');
        $("#cardSubmitBtn").removeAttr('disabled');
        return true;
    }
}



$(document).ready(function() {
    //Demo card numbers
    $('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function(e) {
        e.preventDefault();
        $('.card-payment .numbers').slideUp(100);
        cardFormValidate();
        return $('#card_number').val($(this).text()).trigger('input');
    });
    $('body').click(function() {
        return $('.card-payment .numbers').slideUp(100);
    });
    $('#sample-numbers-trigger').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        return $('.card-payment .numbers').slideDown(100);
    });
    
    //Card form validation on input fields
    $('#paymentForm input[type=text]').on('keyup',function(){
        cardFormValidate();
    });
    
    //Submit card form
    $("#cardSubmitBtn").on('click',function(){
        if(cardFormValidate()){
            var card_number = $('#card_number').val();
            var valid_thru = $('#expiry_month').val()+'/'+$('#expiry_year').val();
            var cvv = $('#cvv').val();
            var card_name = $('#name_on_card').val();
            var cardInfo = '<p>Card Number: <span>'+card_number+'</span></p><p>Valid Thru: <span>'+valid_thru+'</span></p><p>CVV: <span>'+cvv+'</span></p><p>Name on Card: <span>'+card_name+'</span></p><p>Status: <span>VALID</span></p>';
            $('.cardInfo').slideDown('slow');
            $('.cardInfo').html(cardInfo);
        }else{
            $('.cardInfo').slideDown('slow');
            $('.cardInfo').html('<p>Wrong card details given, please try again.</p>');
        }
    });
});
</script>


<script>

    $(document).ready(function () {
        get_package_amount("monthly");
        var backPosition = '2px -121px, 260px -87px';
        $('#card_number').css("background-position", backPosition);
    });


    function get_package_amount(duration)
    {
        var package_id = $('#package_id').val();
         $.ajax({
            url: "<?php echo base_url(); ?>c_level/package_controller/get_package_detal/"+package_id,
            type: 'post',
            dataType: "JSON",
            success: function (data) {

                if (duration=='monthly') 
                {
                    $('#total_amount').val(data.monthly_price);
                }
                if (duration=='yearly') 
                {
                    $('#total_amount').val(data.yearly_price);
                }
               
            },error: function() {

            }
        });
     }
</script>


<script>
    function check_coupon()
    {
        var coupon_code = $('#coupon_code').val();
        if (coupon_code=='') 
        {
            Swal.fire('Please Enter coupon Code');
            return false;
        } 

            $.ajax({
                url: "<?php echo base_url(); ?>c_level/package_controller/get_coupon_detail/"+coupon_code,
                type: 'post',
                dataType: "JSON",
                success: function (data) {

                    if(data!=false) 
                    {
                        $('#coupon_not_found').empty();
/*******************/
                        var total_amount = $('#total_amount').val();
                        var min_purchase = parseInt(data.min_purchase)

                        if(data.type==1) 
                        {
                            var flat = parseInt(data.flat)
                            if(total_amount < min_purchase) 
                            {
                               $('#discount').val('0.00');
                               $('#coupon_status').val(0);
                               $('#coupon_not_found').text('Coupon valid on Minimum Purchase of '+data.min_purchase); 
                               return false;
                            }
                            else
                            {
                               var new_amount = total_amount - flat;
                               $('#total_amount').val(new_amount);
                               $('#discount').val(data.flat);
                               $('#coupon_status').val(1);
                            }

                        }
                        if(data.type==2) 
                        {
                            if(total_amount < min_purchase) 
                            {
                               $('#discount').val('0.00');
                               $('#coupon_status').val(0);
                               $('#coupon_not_found').text('Coupon valid on Minimum Purchase of '+data.min_purchase); 
                               return false;
                            }
                            else
                            {
                               var percent = parseInt(data.percent);
                               var max_discount = parseInt(data.max_discount);

                               percent_ot_total_amount = total_amount * percent/100;

                               if (percent_ot_total_amount > max_discount) 
                               {
                                    discount = max_discount;
                               }else{
                                    discount = percent_ot_total_amount;
                               }

                               var new_amount = total_amount - discount;
                               $('#total_amount').val(new_amount);
                               $('#discount').val(discount);
                               $('#coupon_status').val(1);

                            }

                        }

/*******************/
                    }else{
                        $('#coupon_status').val(0);
                        $('#coupon_not_found').text('Invalid Coupon');
                    }

                },error: function() {

                }
            });
    }
</script>

<script>

    $(document).ready(function () {
        var time = $('#duration').val();
        if (time='yearly') 
        {
            console.log(time);
           /*get_old_rmaining_amount('yearly'); */
        }
    });


    function get_old_rmaining_amount(time)
    {
        if(time=='yearly') 
        {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/package_controller/old_package_remaining_amount/",
                type: 'post',
                dataType: "JSON",
                success: function (data) {

                    if (data!=false) 
                    {
                        $('#old_remaining_amount').val(data);

                      var total_amount =  $('#total_amount').val();
                      var new_amount = total_amount-data;
                        $('#amount').val(total_amount);
                        $('#total_amount').val(new_amount);
                    }

                }

            });
        }

        if(time=='monthly') 
        {
            $('#old_remaining_amount').val(0);
            $('#amount').val(0);
            $('#discount').val(0); 
        }

       
    } 
</script>
