<main class="" style="opacity: 1;">
        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>
    <div class="container-fluid">
        <div class="card mb-4">
        <div class="card-body">
            <div class="row">
                <div class="text-left col-md-8 col-sm-12">
                    <p>Your Retailer plan (billed <?php if($user_deatail->package_duration==null) 
                                  { 
                                     echo "Free"; 
                                  }else{
                                        if($user_deatail->package_duration==1) 
                                        { 
                                            echo "monthly";
                                        }
                                        if($user_deatail->package_duration==2) 
                                        { 
                                            echo "Yearly";
                                        }
                                  }
                            ?>)</p>
                </div>
                <div class="text-right col-md-4 col-sm-12">

                     <?php if($user_deatail->package_id==2 || $user_deatail->package_id==3) {  ?>
                            <a href="" class="btn btn-danger btn-sm d-block float-right customer_btn" onclick="delete_subscription();">Cancel Subscription</a>
                    <?php } ?>


                <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn" disabled="" autocomplete="off">
                        <a href="<?php echo base_url(); ?>package" style="color:#fff;">
                        <?php   if($user_deatail->package_id==1) 
                                { 
                                     echo "Upgrade Plan"; 
                                }
                                if($user_deatail->package_id==2) 
                                { 
                                     echo "Upgrade Plan"; 
                                }
                                if($user_deatail->package_id==3) 
                                { 
                                     echo "Downgrade Plan"; 
                                }
                            ?>
                        </a>
                    </button>

                
                </div>
                <div class="text-left col-md-9 col-sm-12">
                    <h2><?php if (isset($package_details->package)) { echo $package_details->package; } ?></h2>
                </div>
                <div class="text-left col-md-9 col-sm-12">
                    <p>License</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <p><?php if($user_deatail->package_duration==null) 
                                  { 
                                     echo "$ 0.00"; 
                                  }else{
                                        if($user_deatail->package_duration==1) 
                                        { 
                                            echo '$ '.$package_details->monthly_price;
                                        }
                                        if($user_deatail->package_duration==2) 
                                        { 
                                            echo '$ '.$package_details->yearly_price;
                                        }
                                  }
                            ?></p>
                </div>
                <!-- <div class="text-left col-md-9 col-sm-12">
                    <p class="m-0">5% off Discount</p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <p class="m-0">-$ 12.00</p>
                </div> -->
            </div>
            <hr>
            <div class="row">   
                <div class="text-left col-md-9 col-sm-12">
                    <p>Total <?php if($user_deatail->package_duration==null) 
                                  { 
                                     echo "Amount "; 
                                  }else{
                                        if($user_deatail->package_duration==1) 
                                        { 
                                            echo 'per Month';
                                        }
                                        if($user_deatail->package_duration==2) 
                                        { 
                                            echo 'per Year';
                                        }
                                  }
                            ?>
                        </p>
                </div>
                <div class="text-right col-md-3 col-sm-12">
                    <h2><?php if($user_deatail->package_duration==null) 
                                  { 
                                     echo "$ 0.00"; 
                                  }else{
                                        if($user_deatail->package_duration==1) 
                                        { 
                                            echo '$ '.$package_details->monthly_price;
                                        }
                                        if($user_deatail->package_duration==2) 
                                        { 
                                            echo '$ '.$package_details->yearly_price;
                                        }
                                  }
                            ?></h2>
                </div>
            </div>
        </div>
        </div>
        <div class="mt-5">  
            <div class="row">   
                <div class="text-left col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                        <div class="row">
                            <div class="text-left col-md-9 col-sm-12">
                                <p>Payment</p>
                            </div>
                            <div class="text-right col-md-3 col-sm-12">
                                <a href="<?php echo base_url(); ?>retailer-credit-card" class="phone_email_link">Edit</a>
                            </div>
                            <div class="text-left col-md-9 col-sm-12">
                                <div class="row">
                                    <div class="text-left col-md-2 col-sm-12">
                                        <img src="<?php echo base_url().'assets/c_level/img/unknown.png'; ?>" style="width:50px;"/>
                                    </div>
                                    <div class="text-left col-md-10 col-sm-12">
                                    <p class="m-0">&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; <?php echo substr($card_detail->card_number, 15); ?></p>
                                    <p class="mb-5">Mastercard - Expries <?php echo $card_detail->expiry_month; ?>/<?php echo $card_detail->expiry_year; ?></p>
                                    </div>
                                </div>
                                <!-- <p class="m-0">Billed on first of every month.</p> -->
                                <p>Next billing on <?php echo date("d-m-Y", strtotime($user_deatail->package_expire)); ?>.</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="text-left col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-body pb-2">
                        <div class="row">
                            <div class="text-left col-md-9 col-sm-12">
                                <p>Transaction</p>
                            </div>
                            <div class="text-right col-md-3 col-sm-12">
                                <a href="<?php echo base_url().'package-transaction/list'; ?>" class="phone_email_link">View all</a>
                            </div>



                            <div class="text-left col-md-4 col-sm-12">
                                <p>Date</p>
                            </div>
                            <div class="text-right col-md-4 col-sm-12">
                                <p>Card No</p>
                            </div>
                            <div class="text-right col-md-4 col-sm-12">
                                <p>Amount</p>
                            </div>


                            <?php foreach ($package_transaction as $key => $value) { ?>
                                <?php if ($key < 4) { ?>
                                  
                                    <div class="text-left col-md-4 col-sm-12">
                                        <p><?php echo date("d-m-Y", strtotime($value->created_at)); ?></p>
                                    </div>
                                    <div class="text-right col-md-4 col-sm-12">
                                        <p><?php echo $value->card_no; ?></p>
                                    </div>
                                    <div class="text-right col-md-4 col-sm-12">
                                        <p>$<?php echo $value->amount; ?> <!-- <a href="#"><span class="simple-icon-arrow-down-circle"></span></a> --></p>
                                    </div>
                                <?php } ?>

                            <?php } ?>


                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php if ($user_deatail->package_id!=1 && $user_deatail->sub_domain==null) { ?>

    <!-- Modal -->
    <div id="subdomain_add" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">

            <table class="table table-bordered mb-4">
                <tbody>
                    <tr>
                        <td>Enter Sub Domain</td>
                        <td><span id="alert_subdomain" style="color:red;"></span><input type="text" id="subdomain" class="form-control" autocomplete="off"></td>
                        
                    </tr>
                    <tr>
                        <td id="tax_text"></td>
                        <td>
                           <a onclick="save_domain()" class="btn btn-success" autocomplete="off">Submit</a>
                        </td>
                    </tr>
                </tbody>
            </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

<?php } ?>

<script>
    function save_domain()
    {
        var subdomain = $('#subdomain').val();

        if (subdomain=='') 
        {
            $('#alert_subdomain').text('please Enter Subdomain');
            return false;
        }

         $.ajax({
                type: 'POST',
                url: '<?php echo base_url() ?>c_level/Package_controller/add_subdomain/' + subdomain,
                dataType:'JSON',
                success: function(msg) {
                    console.log(msg);
                    if (msg==true) 
                    {
                        $('#subdomain_add').modal('hide');
                    }else{
                        $('#alert_subdomain').text('Subdomain Not Available');
                        return false;
                    }
                }
            });
    }
</script>
