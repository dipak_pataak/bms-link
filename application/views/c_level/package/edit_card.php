<script src="//code.jquery.com/jquery.min.js"></script>
<script src="http://live-bms-link.ti/assets/c_level/resources/js/bootstrap-datepicker.js"></script>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Edit Payment method</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Edit Payment method
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                       <?php echo form_open('retailer-package-edit-card'); ?>
                        <input type="hidden" name="id" value="<?= $card_detail->id; ?>">
                            <div class="form-row">

                                <input type="hidden" id="type" name="type" value="0">
                                <span id="image"></span>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="card_number" class="mb-2">Card Number</label>
                                        <input class="form-control" type="number" name="card_number" id="card_number" required value="<?= $card_detail->card_number; ?>" onkeyup="validate_card(this.value);">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="card_number" class="mb-2">Card Holder Name</label>
                                        <input class="form-control" type="text" name="card_holder_name" id="card_holder_name" required value="<?= $card_detail->card_holder_name; ?>">
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="form-group col-md-6">
                                        <label for="exp_month" class="mb-2">Expire On</label>
                                        <input class="form-control" type="text" name="exp_month" id="exp_month" required value="<?= $card_detail->exp_month; ?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exp_year" class="mb-2">Expire On</label>
                                        <input class="form-control" type="text" name="exp_year" id="exp_year" required value="<?= $card_detail->exp_year; ?>">
                                    </div>
                                </div>

                            </div>

                            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                            <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Update" tabindex="7" />

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

<script>
    function validate_card(val)
    {
        var visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
        var mastercard = /^(?:5[1-5][0-9]{14})$/;
        var american_exp = /^(?:3[47][0-9]{13})$/;

        var digit = val.toString().length;
        if(digit ==1 || digit==16) 
        {
            if(val==4) 
            {
                $('#type').val(2);

                $('#image').empty();
                $('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/payment-card.png" height="60px" width="80px">`);
            }
            if (val==5) 
            {
                $('#type').val(1);

                $('#image').empty();
                $('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/mastercard_text.jpg" height="60px" width="80px">`);
            }
            if (val.match(visa)) 
            {
                $('#type').val(2);

                $('#image').empty();
                $('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/payment-card.png" height="60px" width="80px">`);
            }
            if (val.match(mastercard)) 
            {
                $('#type').val(1);

                $('#image').empty();
                $('#image').append(`<img src="<?php echo base_url(); ?>assets/c_level/img/mastercard_text.jpg" height="60px" width="80px">`);
            }
            
            if (val.match(american_exp)) 
            {
                $('#type').val(3);
            }
        }
                        

    }
</script>


