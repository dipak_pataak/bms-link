<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
       <?php
                            $user_data=access_role_permission(88);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Manufacturing List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manufacturing List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="card-body">

                <p class="mb-0">
                    <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" id="filter-collpase-btn">
                        Filter
                    </button>
                </p>

                <div class="collapse" id="collapseExample">
                    <div class="p-4 border mt-4">
                        <form class="form-horizontal" action="<?= base_url('retailer-manufactur/list'); ?>" method="post" id="filter-form">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select class="form-control mb-3" name="company_id" >
                                            <option value="">--Select Customer--</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option value="<?= $c->customer_id ?>" <?php
                                                if ($customerid == $c->customer_id) {
                                                    echo 'selected';
                                                }
                                                ?>>
                                                <?= $c->first_name." ".$c->last_name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <select class="form-control mb-3" name="sidemark" >
                                            <option value="">--Select Side Mark--</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option value="<?= $c->side_mark ?>" <?php
                                                if ($side_mark == $c->side_mark) {
                                                    echo 'selected';
                                                }
                                                ?>>
                                                <?= $c->side_mark; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="text"  name="order_date" id="order_date" class="form-control datepicker mb-3" value="<?php echo $order_date; ?>" placeholder="Order Date">
                                    </div>                          

                                    <div class="col-sm-2">
                                        <select class="form-control mb-3" name="order_stage" >
                                            <option value="">--Select Status--</option>
                                            <option value="1" <?php
                                            if ($order_stage == 1) {
                                                echo 'selected';
                                            }
                                            ?>>Quote</option>
                                            <option value="2" <?php
                                            if ($order_stage == 2) {
                                                echo 'selected';
                                            }
                                            ?>>Paid</option>
                                            <option value="3" <?php
                                            if ($order_stage == 3) {
                                                echo 'selected';
                                            }
                                            ?>>Partially Paid</option>
                                            <option value="4" <?php
                                            if ($order_stage == 4) {
                                                echo 'selected';
                                            }
                                            ?>>Shipping</option>
                                            <option value="5" <?php
                                            if ($order_stage == 5) {
                                                echo 'selected';
                                            }
                                            ?>>Cancelled</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-1">
                                        <div>
                                            <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                            <!--<button type="reset" class="btn btn-sm btn-danger default">Reset</button>-->
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>        



        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-right">
                                
                               <!-- <button class="btn btn-info dropdown-toggle mb-2" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                    <span class="caret"></span></button>-->
                              <!--  <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-csv')" class="dropdown-item">Export to CSV</a></li>
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-pdf')"  class="dropdown-item">Export to PDF</a></li>
                                    <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                                </ul>-->
                            </div>
                        </div>


                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>Order/Quote No</th>
                                        <th>Customer Name </th>
                                        <th>Side mark</th>
                                        <th>Order date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($get_manufactur as $manufactur) {
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?= $manufactur->order_id; ?></td>
                                            <td>
                                            <?php
                                            if ($manufactur->level_id != 1) {
                                                    $sql = "SELECT * FROM company_profile WHERE user_id = $manufactur->level_id";
                                                    $sql_result = $this->db->query($sql)->result();
                                                    echo @$sql_result[0]->company_name;
                                                } else {
                                                    echo $manufactur->customer_name;
                                                }
                                            ?>
                                            </td>
                                            <td><?= $manufactur->side_mark; ?></td>
                                            <td><?= $manufactur->order_date; ?></td>
                                            
                                            <td>
                                                
                                                   <!--  <a href="<?php echo base_url(); ?>c_level/Order_controller/quotation/<?php echo $manufactur->order_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i></a> -->

                                                    <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?= base_url('retailer-quotation/') . $manufactur->order_id; ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="Manufacturing"> <i class="simple-icon-settings"></i> </a>
                                                    <?php } ?>

                                                     <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?= base_url('retailer-invoice-receipt/'.$manufactur->order_id) ?>" class="btn btn-success btn-sm default" data-toggle="tooltip" data-placement="top" data-original-title="View"> <i class="simple-icon-eye"></i> </a>
                                                    <?php } ?>
                                         
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_manufactur)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>

                        <?php echo $links; ?>
                    </div>


                    <div class="modal fade" id="customer_address_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Customer Address Map Show</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="customer_comment_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">All Comments</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_comment_info">
                                    <form class="" action="<?php echo base_url('b_level/Return_controller/return_resend_comment'); ?>" method="post">
                                        <textarea class="form-control" name="resend_comment" required ></textarea>
                                        <input type="hidden" name="return_id" id="return_id">
                                        <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="customer_exp" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"> Export Customer</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">
                                    <form id="expUrl" action="#" method="post">

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label">Start From <i class="text-danger">*</i></label>
                                            <div class="col-xs-6">
                                                <input type="number" min="1" name="ofset" id="ofset" class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label">Limit <i class="text-danger">*</i></label>
                                            <div class="col-xs-6">
                                                <input type="number" min="1" name="limit" id="limit" class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label  class="col-xs-2 control-label"></label>
                                            <div class="col-xs-6">
                                                <button class="btn-sm btn-success" id="closeModal"> Export</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script>

    function show_customer_record(t, statuss) {
        if(statuss == 'scheduled' || statuss == 're-schedule' || statuss == 'cancelled'){
            $.post("<?php echo base_url(); ?>show-customer-record/" + t, 'status=' + statuss, function (t) {
                $("#customer_info").html(t);
                //Date picker
                $('.datepicker').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    showOn: "focus",
                });
                // ======== its timer =========== format: 'LT'
                $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
                $('#datepicker-left-header').datetimepicker({
                    // 'format': "HH:mm", // HH:mm:ss its for 24 hours format
                    format: 'LT', /// its for 12 hours format
                });
                var st = $('#sta').val();
                $('#status').val(statuss);
                $("form :input").attr("autocomplete", "off");
                $("#appointschedule").modal('show');
            });
        } else{
            $.post("<?php echo base_url(); ?>change-customer-status/", {status : statuss, customer_id : t}, function (res) {
                toastr.success('Success! - Status Change Successfully');
                setTimeout(function () {
                    window.location.href = window.location.href;
                }, 2000);
            });
        }
    }

    //    ========== its for customer search ======
    function customerkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>retailer-customer-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#result_search").html(r);
            }
        });
    }

    //============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)

        $.post("<?php echo base_url(); ?>retailer-show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
    // ============ its for show_old_comment ============
    function show_old_comment(id) {
        $.ajax({
            url: "<?php echo base_url(); ?>show-old-retailer-cstomer-comment",
            type: "post",
            data: {customer_id: id},
            success: function (t) {
                $("#customer_comment_info").html(t);
                $('#customer_comment_modal_info').modal('show');
            }
        });
    }
    //============= its for show_export_pdf =============
    function ExportMethod(url) {
        $("#expUrl").attr("action", url);
        $("#customer_exp").modal('show');
    }

    $("body").on('click', '#closeModal', function () {
        $("#customer_exp").modal('hide');
    });


    $(document).ready(function(){
        // For Open filter Box : START
        var company_id = $("#filter-form select[name=company_id]").val();
        var sidemark = $("#filter-form select[name=sidemark]").val();
        var order_date = $("#filter-form input[name=order_date]").val();
        var order_stage = $("#filter-form select[name=order_stage]").val();

        if(company_id != '' || sidemark != '' || order_date != '' || order_stage != ''){
            $("#filter-collpase-btn").click();
        }
        // For Open filter Box : END
    })

</script>
<script type="text/javascript">
    $('#s1').hide();
    $('#c1').hide();
    function viewFild(id) {
        if (id == 1) {
            $('#s1').show();
            $('#c1').hide();
        } else if (id == 2) {
            $('#s1').show();
            $('#c1').show();

        } else {
            $('#s1').hide();
            $('#c1').hide();
        }
    }
</script>
