<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    #content div.box table {
        margin: 0;
        padding: 0;
        width: 100%;
        border-collapse: collapse;
    }
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
    .hidden {
        display: none;
    }
    .nav-pills .nav-link {
        border: 1px groove;
    }
    .tab-pane .table table{
        border-left: 1px solid #cdcdcd;
    }
    .table table td {
        border: 1px solid #2f2f2f;
    }
    .tab-pane .table table {
        border-left: 0px;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Manufacturing Info</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">

                                Manufacturing

                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                
                <div class="card mb-4">
                    <div class="card-body">

                        	<div class="bs_tab">
                    

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <ul class="nav nav-pills mb-3" role="tablist">
                                <?php
                                    foreach ($categories as $key => $value) {
                                        if ($value == 'Blinds' && count($categories) == 1) { 
                                            $blind_active = "active";
                                        } else if($value == 'Shades' && count($categories) == 1) {
                                            $shades_active = "active";
                                        } else {
                                            $blind_active = "active";
                                        }
                                    
                                        if ($value == 'Blinds') {
                                            ?>
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#blind" role="tab">Blind Work</a>
                                </li>
                                <?php } if ($value == 'Shades') { ?>
                                <li class="nav-item">
                                    <a class="nav-link <?= @$shades_active ?>" data-toggle="tab" href="#shade" role="tab">Shade Work</a>
                                </li>
                                <?php }
                                    } ?>
                            </ul>

                            <div class="tab-pane p-2 <?=@$blind_active?>" id="blind" role="tabpanel">

                                <div class="row" >

                                    <?php 

                                        if(!empty($chit['blinds'])){
                                            foreach ($chit['blinds'] as $key => $value) {
                                               echo @$value;
                                            }
                                        }

                                    ?>

                                </div>
              
                            </div>


                            <div class="tab-pane p-2 <?=@$shades_active?>" id="shade"  role="tabpanel">
                                
                                <div class="row" >

                                    <?php 

                                        if(!empty($chit['shades'])){
                                            foreach ($chit['shades'] as $key => $value) {
                                               echo @$value;
                                            }
                                        }

                                    ?>

                                </div>

                                <!-- <button type="button" class="btn btn-success" onclick="printContent('printableArea2')" style="margin-left: 25px;">Print labels</button> -->

                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
<?php if(!isset($data->id)){ ?>
    <script>
        $('button[type=submit]').prop('disabled', true);
    </script>
<?php } ?>

<style type="text/css">
    .rotated { 
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
</style>

<script type="text/javascript">
    function required_validation() {

        if ($("#color_name").val() != '' && $("#color_number").val() != '') {

            $('button[type=submit]').prop('disabled', false);

        }
        else {
            $('button[type=submit]').prop('disabled', true);
        }
    }

    function printContent(el) {
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }

</script>

