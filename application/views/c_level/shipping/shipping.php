<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/b_level/resources/css/daterangepicker.css" />
<style type="text/css">
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
		top: 262.594px !important;
		left: 188px;
		z-index: 10;
		display: block;
	}
</style>
<?php $currency = $company_profile[0]->currency; ?>
<main>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="mb-3">
					<h1>Manage Shipping Method</h1>
					<?php
						$page_url = $this->uri->segment(1);
						$get_favorite = get_c_favorite_detail($page_url);
						$class = "notfavorite_icon";
						$fav_title = 'Manage Quotation/Order';
						$onclick = 'add_favorite()';
						if (!empty($get_favorite)) {
							$class = "favorites_icon";
							$onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
						}
					?>
					 <?php
                            $user_data=access_role_permission(111);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
					<input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
					<input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
					<span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
					<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
						<ol class="breadcrumb pt-0">
							<li class="breadcrumb-item">
								<a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
							</li>
							<!-- <li class="breadcrumb-item">
								<a href="#">Shipping</a>
							</li> -->
							<li class="breadcrumb-item active" aria-current="page">Shipping Method</li>
						</ol>
					</nav>
				</div>
				<div class="separator mb-5"></div>
			</div>
		</div>
		<div class="card mb-4">
			<div class="card-body">
				<h5>Shipping Method</h5>
				<?php
				$page_url = $this->uri->segment(1);
				$get_favorite = get_b_favorite_detail($page_url);
				$class = "notfavorite_icon";
				$fav_title = 'Shipping Method';
				$onclick = 'add_favorite()';
				if (!empty($get_favorite)) {
				$class = "favorites_icon";
				$onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
				}
				?>
				<input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
				<input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
				<span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
				<?php echo form_open('c_level/setting_controller/save_shipping_method');?>
				<div class="panel">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-7 col-md-7 col-lg-5">
								<div class="form-group row">
									<label for="method_name" class="col-sm-4 control-label">Shipping method name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="method_name" id="method_name" placeholder="Shipping method name" required >
									</div>
								</div>
								<div class="form-group row">
									<label for="username" class="col-sm-4 control-label">User name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="username" id="username" placeholder="Username" required >
									</div>
								</div>
								<div class="form-group row">
									<label for="password" class="col-sm-4 control-label">Password</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="password" id="password" placeholder="Password" required >
									</div>
								</div>
								<div class="form-group row">
									<label for="account_id" class="col-sm-4 control-label">Account id</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="account_id" id="account_id" placeholder="Account ID" required >
									</div>
								</div>
								<div class="form-group row">
									<label for="access_token" class="col-sm-4 control-label">Access Token</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="access_token" id="access_token" placeholder="Access Token" required >
									</div>
								</div>
								<div class="form-group row">
									<div class="col-12 text-right">
										<?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
										<input type="submit"  class="btn btn-success btn-large" value="Submit">
										<?php } ?>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close();?>
			</div>
		</div>
		<div class="">
			<?php
			$error = $this->session->flashdata('error');
			$success = $this->session->flashdata('success');
			if ($error != '') {
				echo $error;
			}
			if ($success != '') {
				echo $success;
			}
			?>
			<?php
			$message = $this->session->flashdata('message');
			if ($message != '') {
				echo $message;
			}
			?>
		</div>
		<?php
				$user_id = $this->session->userdata('user_id');
		$user_deatail = $this->db->where('id',$user_id)->get('user_info')->row();
		?>
		<div class="row">
			<div class="col-xl-12 mb-4">
				<div class="card mb-4">
					<div class="card-body">
						
						<h3>Manage Shipping Method</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Sl no.</th>
										<th>Shipping method</th>
										<th>User Name</th>
										<th>Password</th>
										<th>Account Id</th>
										<th>Access token</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1;
									foreach ($lists as $key => $val) {
									?>
									<tr>
										<td><?=$i++?></td>
										<td><?=$val->method_name?></td>
										<td><?=$val->username?></td>
										<td><?=$val->password?></td>
										<td><?=$val->account_id?></td>
										<td><?=$val->access_token?></td>
										
										<td class="text-right">
											 <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
											<a href="<?php echo base_url(); ?>customer-shipping-edit/<?=$val->id?>" class="btn btn-warning btn-xs simple-icon-note" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
											  <?php } ?>

											<?php if($val->is_default != '1'){ ?>
												<?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
												<a href="<?php echo base_url(); ?>c_level/setting_controller/delete_shipping_method/<?=$val->id?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs simple-icon-trash" data-placement="top" data-original-title="Edit"><i class="fa fa-trash-o"></i></a>
												 <?php } ?>
											<?php } ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
								<?php if (empty($lists)) { ?>
								<tfoot>
								<tr>
									<th colspan="7" class="text-center">No record found!</th>
								</tr>
								</tfoot>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
