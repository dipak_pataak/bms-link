<script src="//code.jquery.com/jquery.min.js"></script>
<script src="http://live-bms-link.ti/assets/c_level/resources/js/bootstrap-datepicker.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Edit Shipping method</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Edit Shipping method
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                       <?php echo form_open('c_level/setting_controller/update_shipping_method'); ?>
                        <input type="hidden" name="id" value="<?= $method->id; ?>">
                            <div class="form-row">
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="method_name" class="mb-2">Shipping method name</label>
                                        <input class="form-control" type="text" name="method_name" id="method_name" required value="<?= $method->method_name; ?>" <?php if($method->is_default == '1'){ echo "readonly"; } ?>>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="username" class="mb-2">User name</label>
                                        <input class="form-control" type="text" name="username" id="username" required value="<?= $method->username; ?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="password" class="mb-2">Password</label>
                                        <input class="form-control" type="text" name="password" id="password" required value="<?= $method->password; ?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="account_id" class="mb-2">Account id</label>
                                        <input class="form-control" type="text" name="account_id" id="account_id" required value="<?= $method->account_id; ?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                       <label for="access_token" class="mb-2">Access Token</label>
                                        <input class="form-control" type="text" name="access_token" id="access_token" required value="<?= $method->access_token; ?>">
                                    </div>
                                </div>

                                <?php
                                    $pickup_method = array(
                                        '01' => 'Daily Pickup',
                                        '03' => 'Customer Counter',
                                        '06' => 'One Time Pickup',
                                        '07' => 'On Call Air Pickup',
                                        '19' => 'Letter Center',
                                        '20' => 'Air Service Center'
                                    );
                                ?>

                                <div class="form-group col-md-12" style="display:none;">
                                    <label for="access_token">Pickup Method</label>
                                    <select class="form-control select2" id="pickup_method" name="pickup_method" >
                                        <?php
                                            foreach ($pickup_method as $key => $val) {
                                                echo "<option value='$key' " . ($method->pickup_method == $key ? 'selected' : '') . ">$val</option>";
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="access_token">Test or Production Mode</label>
                                    <select class="form-control select2" name="mode" data-placeholder='-- select one --'>
                                        <?php
                                        $mode = array(
                                            'test' => 'Test',
                                            'production' => 'Production'
                                        );
                                        foreach ($mode as $key => $val) {
                                            echo "<option value='$key' " . ($method->mode == $key ? 'selected' : '') . ">$val</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="access_token">Status</label>
                                    <select class="form-control select2" name="status" data-placeholder='-- select one --'>
                                          <?php
                                            $status = array(
                                                '1' => 'Active',
                                                '0' => 'Inactive'
                                            );
                                            foreach ($status as $key => $val) {
                                                echo "<option value='$key' " . ($method->status == $key ? 'selected' : '') . ">$val</option>";
                                            }
                                          ?>
                                    </select>
                                </div>
                        
                              
                            </div>

                            <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-primary btn-large text-white">Back</a>
                            <input type="submit" id="" class="btn btn-primary btn-large" name="add-user" value="Update" tabindex="7" />

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


