<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Return Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>



        <div class="card mb-4">
            <div class="card-body">
                <div class="col-sm-6">
                    <table class="table table-bordered table-hover">
                        <?php // dd($orders); ?>
                        <tr>
                            <td class="text-center">Order ID</td>
                            <td class="text-center"><?= ($orders->order_id) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Order Date</td>
                            <td class="text-center"><?= (date('M-d-Y', strtotime($orders->order_date))) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Client Name</td>
                            <td class="text-center"><?= $orders->customer_name ?></td>
                        </tr>
                        </tr>

                    </table>
                </div>

                <div class="table-responsive" style="margin-top: 10px">
                    <form action="<?php echo base_url(); ?>retailer-customer-order-return-save" method="post" enctype="multipart/form-data">
                        <table class="table table-bordered table-hover customer-return-table" id="normalinvoice">
                            <thead>
                                <tr>
                                    <th class="text-center">Product Name</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Returned</th>
                                    <th class="text-center">Return Qty </th>
                                    <th class="text-center">Comment </th>
                                    <th class="text-center">WantTo </th>
                                    <th class="text-center">Image </th>
                                </tr>
                            </thead>
                            <tbody id="addinvoiceItem">
                                <?php
                                if (!empty($order_details)) {
//                                    dd($order_details);
                                    $i = 1;
                                    foreach ($order_details as $key => $p) {
                                        $i++;
                                        $get_return_info = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $p->order_id)->get()->row();
                                        $return_id = @$get_return_info->return_id;
                                        $get_return_details_info = $this->db->select('a.*, sum(a.return_qty) as total_return')
                                                        ->from('order_return_details a')
                                                        ->where('a.return_id', $return_id)
                                                        ->where('a.product_id', $p->product_id)
                                                        ->get()->result();
                                        $width_fraction = $this->db->where('id', $p->width_fraction_id)->get('width_height_fractions')->row();
                                        $height_fraction = $this->db->where('id', $p->height_fraction_id)->get('width_height_fractions')->row();
//                                        echo $this->db->last_query();
//                                        $product_id = $get_return_details_info[$j]->product_id;
//                                        
//                                        $check_replace_qty = $this->db->select('a.*, sum(a.replace_qty) as replace_quantity')
//                                                        ->from('order_return_details a')
//                                                        ->where('a.return_id', $return_id)
//                                                        ->where('a.product_id', $product_id)
////                                                        ->where('a.replace_qty!=', 0)
//                                                        ->get()->result();
//                                        echo $get_return_info->return_id; echo '<br>'; echo $p->product_id;
//                                        echo '<pre>';      print_r($get_return_details_info);                                       echo '</pre>';
                                        ?>    
                                        <tr>
                                            <td> 
                                                <a onClick="javascript:viewOrderHtml('<?=$p->order_id?>', '<?=$p->row_id?>')" href="#"><strong><?= $p->product_name; ?></strong><br />
                                                <?php
                                                    if ($p->pattern_name) {
                                                        echo $p->pattern_name . '<br/>';
                                                    }
                                                    ?>
                                                W <?= $p->width; ?> <?= @$width_fraction->fraction_value ?>,
                                                H <?= $p->height; ?> <?= @$height_fraction->fraction_value ?>,
                                                <?= $p->color_number; ?>
                                                <?= $p->color_name . ', '; ?>

                                                <?php
                                                    if (isset($selected_rooms[$p->room])) {
                                                        echo $p->room . ' ' . $selected_rooms[$p->room];
                                                        $selected_rooms[$p->room]++;
                                                    } else {
                                                        echo $p->room . ' 1';
                                                        $selected_rooms[$p->room] = 2;
                                                    }
                                                    ?> </a>
                                                <input type="hidden" name="product_id[]" value="<?= ($p->product_id) ?>">
                                            </td>
                                            <td class="">
                                                <input type="number" name="quantity" value="<?= $p->product_qty; ?>" id="quantity_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                            </td>
                                            <td class="">
                                                <input type="number" name="already_return" value="<?php if($get_return_details_info[0]->total_return){echo $get_return_details_info[0]->total_return; }else{echo '0'; } ?>" id="already_return_<?php echo $i; ?>"  class="form-control text-center" readonly>
                                            </td>
                                            <td class="text-right">
                                                <?php if ($p->product_qty == $get_return_details_info[0]->total_return) { ?>
                                                <input type="text" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control text-right" readonly>
                                                <?php } else { ?>
                                                    <input type="text" name="return_qty[]" id="return_quantity_<?php echo $i; ?>" min="0" onkeyup="return_calculation(<?php echo $i; ?>)" onchange="return_calculation(<?php echo $i; ?>)" class="form-control text-right">
                                                <?php } ?>
                                            </td>
                                            <td class="text-right">
                                                <input type="text" name="return_comments[]"  value='<?php if($get_return_details_info[0]->return_comments){echo $get_return_details_info[0]->return_comments; } ?>'  <?php if($get_return_details_info[0]->return_comments){echo "readonly"; } ?> class="form-control" id="return_comments_<?php echo $i; ?>">
                                            </td>
                                            <td class="text-left"><br/>
                                                <input type="radio" value="1" <?=($get_return_details_info[0]->return_type == 1) ? 'checked' : ''?> <?=($get_return_details_info[0]->return_type != '') ? 'disabled' : ''?> name="return_type_<?=$p->product_id?>"  class="return_type_<?php echo $i; ?>"> Replace<br/>
                                                <input type="radio" value="2" <?=($get_return_details_info[0]->return_type == 2) ? 'checked' : ''?> <?=($get_return_details_info[0]->return_type != '') ? 'disabled' : ''?> name="return_type_<?=$p->product_id?>"  class="return_type_<?php echo $i; ?>"> Repair
                                            </td>
                                            <td class="text-right">
                                                <?php 
                                                    if($get_return_details_info[0]->return_img != '') {
                                                        $imgArr = explode(',', $get_return_details_info[0]->return_img);
                                                        foreach($imgArr as $img) { ?>
                                                            <a href="<?php echo base_url(); ?>assets/c_level/uploads/customers/return/<?=$img?>" target="_blank"><?=$img;?></a><br/>
                                                        <?php }
                                                    } else {
                                                ?>
                                                <input type="file" multiple name="return_img_<?=$p->product_id?>[]" id="return_img_<?=$p->product_id?>" onChange="javascript:limitFileUpload('return_img_<?=$p->product_id?>');">
                                                <?php }?>
                                                <input type="hidden" multiple name="return_img_hdn_<?=$p->product_id?>[]" value="<?php echo @$get_return_details_info[0]->return_img; ?>">
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                            <tr>
                                <td colspan="6">
                                <td colspan="1">
                                    <input type="hidden" name="customer_id" value="<?= ($orders->customer_id) ?>">
                                    <input type="hidden" name="order_id" value="<?= ($orders->order_id) ?>">
                                    <button type="submit" class="btn btn-success form-control" style="float: right; "><strong>Confirm</strong></button>
                                </td>
                            </tr>
                        </table> 
                    </form>
                </div>
            </div>
        </div>
<div id="Modal-Manage-Order" class="modal fade modal-place-order" role="dialog">
    <div class="modal-dialog modal-xl" style="margin: auto; max-width: 900px; ">
        <div class="modal-content" style="width: 1024px;">
            <div class="modal-header">
                <h4 class="modal-title"><span class="action_type"></span> Order</h4>
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="padding-top:20px!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

        <script type="text/javascript">
            function return_calculation(item) {
                var stock_quantity = parseInt($("#stock_quantity_" + item).val());
                var quantity = parseInt($("#quantity_" + item).val());
                var already_quantity = parseInt($("#already_return_" + item).val());
                var available_quantity = parseInt(quantity)-parseInt(already_quantity);
                var return_quantity = $("#return_quantity_" + item).val();
                if(return_quantity != '') {
                    return_quantity = parseInt($("#return_quantity_" + item).val());
                }
//                Swal.fire(already_quantity);                Swal.fire(available_quantity);
                //        console.log(typeof (stock_quantity));
//                if (stock_quantity < return_quantity) {
//                    Swal.fire("Return Quantity is not greater than stock quantity");
//                    $("#stock_quantity_" + item).css({'border': '2px solid red'});
//                    $("#return_quantity_" + item).css({'border': '2px solid red'});
//                    $('button[type=submit]').prop('disabled', true);
//                } else {
//                    $("#stock_quantity_" + item).css({'border': '2px solid green'});
//                    $("#return_quantity_" + item).css({'border': '2px solid green'});
//                    $('button[type=submit]').prop('disabled', false);
//                }
                if (available_quantity < return_quantity) {
                    Swal.fire("Return Quantity is not greater than quantity");
                    $("#quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).val('').focus();
                    $('button[type=submit]').prop('disabled', true);
                } else if (return_quantity == '0') {
                    Swal.fire("0 is not allowed");
                    $("#quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).val('').focus();
                    $('button[type=submit]').prop('disabled', true);
                } else {
                    console.log('return_quantity', return_quantity);
                    if(return_quantity == '') {
                        $('#return_comments_'+ item).prop('required', false);
                        $('.return_type_'+ item).prop('required', false); 
                    } else {
                        $('#return_comments_'+ item).prop('required', true);
                        $('.return_type_'+ item).prop('required', true);
                    }
                    $("#quantity_" + item).css({'border': '2px solid green'});
                    $("#return_quantity_" + item).css({'border': '2px solid green'});
                    $('button[type=submit]').prop('disabled', false);
                }
            }

    function viewOrderHtml(order_id, row_id) {
        var orderid = order_id;
        var rowid = row_id;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Order_controller/getOrderData') ?>",
            data: {
                order_id: orderid,
                row_id: rowid

            },
            success: function(data) {

                $('#Modal-Manage-Order .action_type').text('View');
                $('#Modal-Manage-Order .modal-body').html(data);
                //$('#Modal-Manage-Order #customer_id').val(quote_cutomer_id);
                //$('#Modal-Manage-Order #customer_id').change();
                $('#Modal-Manage-Order').modal('show');

            },
            error: function() {
                Swal.fire('error');
            }
        });
    }

    function limitFileUpload(uploadfiles) {
        var fileUpload = $("#"+uploadfiles);
        if (parseInt(fileUpload.get(0).files.length) > 4){
            Swal.fire("You can only upload a maximum of 4 files");
            $("#"+uploadfiles).val('');
        }
    }
        </script>
