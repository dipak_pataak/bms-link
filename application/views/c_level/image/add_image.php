<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .clr_btn {
        position: relative;
        right: -320px;
        top: -45px;
        border-radius: 0px;
    }
</style>


<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Image Info</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Add Image';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Add Image</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form form action="<?php echo base_url('c_level/Image_controller/save_image'); ?>" method="post" name="imageFrm" class="p-3" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="file_upload" class="col-sm-4 col-form-label">File Upload</label>
                                        <div class="col-sm-8">
                                           <input type="file" class="form-control" name="file_upload[]" id="file_upload" required multiple>
                                            <span>You can upload multiple images</span>
                                            <p>Extension:JPG/PNG File size: 2MB</p>
                                        </div>                            
                                    </div>
                                    <?php
                                    if (!empty($tags)) {
                                        foreach ($tags as $key => $val) {
                                            ?>
                                            <div class="form-group row">
                                                <label for="<?= $val->tag_name; ?>" class="col-sm-4 col-form-label"><?= $val->tag_name; ?></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="<?= $val->tag_name; ?>" name="tag_<?=$val->id; ?>" placeholder="<?= $val->tag_name; ?>">
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <div class="form-group text-right">
                                        <button type="submit" name="btnSave" class="btn btn-success w-md m-b-5" autocomplete="off">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>


<script type="text/javascript">    
    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
    }
    // -------- Image Preview Ends --------------
    var file_input_index = 0;
        $('input[type=file]').each(function () {
        file_input_index++;
        $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
        $(this).after('<input type="button" value="Clear" class="btn btn-danger clr_btn" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" />');
    });
</script>






