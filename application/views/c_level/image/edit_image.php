<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .clr_btn {
        position: relative;
        right: -320px;
        top: -45px;
        border-radius: 0px;
    }
</style>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Image Info</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Image</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <form form action="<?php echo base_url('update_retailer_gallery_image/'. $image_id); ?>" method="post" name="imageEditForm" class="p-3" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="file_upload" class="col-sm-4 col-form-label">File Upload</label>
                                        <div class="col-sm-8">
                                           <input type="file" class="form-control" name="file_upload" id="file_upload">
                                            <input type="hidden" name="hidden_image" value="<?=$images_tag_data->image?>"/>                                
                                            <p>Extension:JPG/PNG PDF & DOC. File size: 2MB</p>
                                            <img src="<?php echo base_url(); ?><?=$images_tag_data->image?>" class="img-thumbnail" width="100" height="50" id="prevImg">
                                        </div>                            
                                    </div>
                                    <?php
                                    $tags = $this->db->from('tag_tbl')->where('tag_tbl.created_by', $this->session->userdata('user_id'))->get()->result();
                                    if (!empty($tags)) {
                                        foreach ($tags as $key => $val) {
                                            ?>
                                            <div class="form-group row">
                                                <label for="<?= $val->tag_name; ?>" class="col-sm-4 col-form-label"><?= $val->tag_name; ?></label>
                                                <div class="col-sm-8">
                                                    <?php
                                                    $image_id = $images_tag_data->image_id;
                                                    if(in_array($val->id, $tag_ids)){
                                                    ?>
                                                    <input type="text" class="form-control" id="<?= $val->tag_name; ?>" name="tag_<?= $val->id; ?>" placeholder="<?= $val->tag_name; ?>" value="<?=$tag_data[$val->id]?>">
                                                <?php 
                                                    } else { ?>
                                                        <input type="text" class="form-control" id="<?= $val->tag_name; ?>" name="tag_<?= $val->id; ?>" placeholder="<?= $val->tag_name; ?>" value="">
                                                   <?php 
                                                }
                                                ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <div class="form-group text-right">
                                        <button type="submit" name="btnSave" class="btn btn-success w-md m-b-5" autocomplete="off">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">    
        // -------- Show Image Preview once File selected ----
        $("body").on("change", "#file_upload", function (e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.getElementById('prevImg');
                var reader = new FileReader();
                reader.onloadend = function () {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("logo").after(img);

                $("#prevImg").show();
            }
        });

        function reset_html(id) {
            $('#' + id).html($('#' + id).html());
            $('#file_upload').attr('required',true);
            $("#prevImg").hide();
        }
        // -------- Image Preview Ends --------------
        var file_input_index = 0;
        $('input[type=file]').each(function () {
            file_input_index++;
            $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
            $(this).after('<input type="button" value="Clear" class="btn btn-danger clr_btn" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" />');

        });
</script>






