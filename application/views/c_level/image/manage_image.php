<style type="text/css">
    #content div.box h5{
        border-bottom: 0;
        padding: 0;
        margin: 0;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
    }
    .gallery-image-wrapper .gallery-image-outer img{
        width: 100%;
        height: 145px;
    }
    .gallery-image-wrapper .gallery-image-outer {
        position: relative;
        margin-bottom: 20px;
        overflow: hidden;
    }
   .gallery-image-wrapper .gallery-image-outer:before {
        content: '';
        position: absolute;
        top: -80px;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(0,0,0,0.4);
        opacity: 0;
         -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .gallery-image-wrapper .gallery-image-outer:hover:before {
        top: 0;
        opacity: 1;
         -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .gallery-image-wrapper .gallery-manage-btnmain {
        text-align: center;
        position: absolute;
        top: auto;
        bottom: -10px;
        margin: 0 auto;
        left: 0;
        right: 0;
        opacity: 0;        
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        bottom: 10px;
        webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .gallery-image-wrapper {
        text-align: center;
    }
    .gallery-image-wrapper a.load-more-btn {
        font-size: 14px;
        color: #ffffff;
        background-color: #336699;
        vertical-align: middle;
        padding: 0 15px;
        height: 40px;
        line-height: 40px;
        border-radius: 3px;
        margin: 30px auto 0 auto;
        text-decoration: none;
        display: none;
    }
    .gallery-image-wrapper i {
        display: inline-block;
        vertical-align: middle;
    }
    /* grid & list */
    .grid-list-wrapper button {
        font-size: 14px;
        background-color: #369;
        color: #ffffff;
        text-shadow: none;
        padding: 0;
        height: 30px;
        width: 30px;
        border: none;
    }
    .gallery-image-wrapper .some-list-load.row {
        margin-left: 0;
        margin-right: 0;
    }
    .gallery-image-wrapper .some-list-load.list {
        display: block;
        text-align: left;
    }
    .gallery-image-wrapper .some-list-load.grid .gallery-manage-btnmain-list {
        display: none;
    }
    .gallery-image-wrapper .product-grid .gallery-manage-btnmain a span {
        display: none;
    }
    .gallery-image-wrapper .product-list img {
        width: 25%;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain {
        right: 15px;
        bottom: auto;
        top: 0;
        left: auto;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a {
        display: block;
        margin: 5px 0;
    }
    .gallery-image-wrapper .product-list .gallery-manage-btnmain  a i {
        margin-right: 5px;
        display: inline-block;
        vertical-align: middle;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer .gallery-manage-btnmain ,
    .gallery-image-wrapper .product-list .gallery-image-outer:hover .gallery-manage-btnmain {
        opacity: 1;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%); 
        -o-transform: translateY(-50%); 
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
        bottom: auto;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer {
        border: 1px solid #eeeeee;
        padding: 15px;
    }
    .gallery-image-wrapper .product-list .gallery-image-outer:hover:before {
        opacity: 0;
    }
    .retailer-gallery-image-view {
        z-index: 99999;
    }
    .gallery-manage-check {
        position: absolute;
        top: 10px;
        right: 10px;
    }
    @media(max-width: 1199px){
        .gallery-image-wrapper .gallery-image-outer img {
            min-height: 175px;
        }
    }
    @media (min-width: 767px) {
        .retailer-gallery-image-view .modal-dialog {
            max-width: 1000px;
        }
    }
</style>
<main>
                    <?php
                            $user_data=access_role_permission(75);
                            $log_data=employ_role_access($id);
                                                                
                    ?>

    <div class="container-fluid gallery-details">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Image List</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Image List';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Image List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

         <?php 
                $user_id = $this->session->userdata('user_id');
                $user_detail = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row(); 
         ?>
         <?php if ($user_detail->package_id!=1) { ?>
            <div class="card mb-4">
                <div class="card-body">  
                    <div>
                        <p>
                            <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                            <a href="<?php echo base_url('add_retailer_gallery_image'); ?>" class="btn btn-success mt-1 " id="add_new_tag" style="margin-top: -6px !important;">Add Image</a>
                          
                            <?php if ($user_detail->wholesaler_connection) { ?>
                                <a href="<?php echo base_url('wholeseller_images'); ?>" class="btn btn-success mt-1" style="margin-top: -6px !important;"><?= ($wholeseller_company != '') ? $wholeseller_company: 'B Level'?> Gallery</a>
                            <?php } ?>


                            <?php if(count($images) > 0){ ?>
                                <button type="button" class="btn btn-info mt-1" data-toggle="modal" data-target="#importGalleryTag" style="margin-top: -6px !important;">Bulk Import Upload</button>
                            <?php } ?>   
                              <?php } ?> 
                            <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                            <a href="javascript:void(0)" class="btn btn-danger mt-1 action-delete" onClick="return action_delete_order(document.recordlist)" style="margin-top: -6px !important;">Delete</a>
                            <?php } ?>
                        </p>
                    <?php if(count($filter_arr) > 0) { ?>
                        <div class="p-4 border mt-4">
                            <form class="form-horizontal" action="<?php echo base_url('retailer_gallery_image'); ?>" method="post" id="galleryFilterFrm">
                                <div class="row">
                                    <?php  foreach($filter_arr as $filter) { ?>
                                        <div class="col-sm-3">
                                            <select class="form-control filter-select-box" name="tag_<?=$filter['tag_id']?>" id="tag_<?=$filter['tag_id']?>" >
                                                <option value="">--Select <?= $filter['tag_name']; ?>--</option>
                                                <?php foreach($filter['value'] as $value) { ?>
                                                    <option value="<?= $value['tag_value']?>"><?= $value['tag_value']?></option>
                                                <?php }?>
                                            </select>
                                            <script>
                                                $('#tag_'+<?=$filter['tag_id']?>).val('<?= $filter['selected_val']; ?>');
                                            </script>
                                        </div>
                                    <?php } ?>
                                    <div class="col-sm-4">
                                        <div>
                                            <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn">Go</button>
                                            <button type="button" class="btn btn-sm btn-danger default" onclick="filter_field_reset()">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                     <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    $message = $this->session->flashdata('message');
                    if ($message)
                        echo $message;
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                    <div id="results_color">
                    <form class="p-3" name="recordlist" id="mainform" method="post" action="<?php echo base_url('c_level/Image_controller/manage_action') ?>">
                    <input type="hidden" name="action">
                    <div class="gallery-image-wrapper">
                        <div class="row">
                        <?php
                        $i = 0 + $pagenum;
                        if (!empty($images)) { ?>
                            <div class="col-sm-12 text-left" style="margin: 10px;">
                                <input type="checkbox" name="" id="master"> 
                                <label><b>Select All</b></label>                        
                            </div>
                            <?php foreach ($images as $key => $val) {
                                ?>
                                <div class="col-xl-3 col-lg-4 col-sm-4 col-md-12 content product-grid">
                                    <div class="gallery-image-outer">
                                        <div class="gallery-manage-check">
                                            <input type="checkbox" name="gallery_img_id[]" value="<?= $val->id; ?>" class="sub_chk">
                                        </div> 
                                        <img src="<?= base_url($val->image)?>" class='gallery_image'>
                                        <div class="gallery-manage-btnmain">
                                            <a href="javascript:void(0)" class="btn btn-primary default btn-sm view_image" id="view_image" data-data_id="<?= $val->id ?>" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="iconsmind-Eye-Visible" aria-hidden="true"></i><span>view image</span>
                                            </a>
                                            <a href="<?php echo base_url('view_retailer_gallery_image/'.$val->id); ?>" class="btn btn-primary default btn-sm" id="view_image" target="_blank"><i class="iconsmind-Eye-Visible" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="View"></i><span>view image</span>
                                                </a>
                                            <a href="<?php echo base_url('edit_retailer_gallery_image/'.$val->id); ?>" class="btn btn-warning default btn-sm edit_image" id="edit_image" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-pencil"></i><span>edit image</span></a>
                                            <a href="<?php echo base_url('c_level/Image_controller/delete_image/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-sm default delete_image" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i><span>delete image</span></a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        </div>
                    </div>

                    <?php if (empty($images)) { ?>
                        <div class="text-center text-danger">No record found!</div>
                    <?php } ?>
                <?php echo $links; ?>
            </form>
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importGalleryTag" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gallery Tag Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <a href="<?php echo base_url('export_retailer_gallery_image'); ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                <?php echo form_open_multipart('import_retailer_gallery_image_tag', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                <div class="form-group row">
                    <label for="upload_csv_file" class="col-xs-2 control-label">File<sup class="text-danger">*</sup></label>
                    <div class="col-xs-6">
                        <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                    </div>
                </div>
                <div class="form-group  text-right">
                    <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                </div>

                </form>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
<!-- Modal -->

</main>
<script>
function filter_field_reset() {
    $(".filter-select-box").val('');
    $('#galleryFilterFrm').submit();
}
$(document).ready(function(){
    $('.view_image').on('click', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "c_level/Image_controller/view_b_gallery/" + id;
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    var data = JSON.parse(res);
                    var tag_details = '';
                    $("#prevImg").attr('src',data[0].image);

                    $.each(data[2], function (key, value) {
                        tag_details += '<label class="col-sm-3 col-form-label">' + value.tag_name + ' :</label>';
                        tag_details += '<div class="col-sm-3 col-form-data"><span>' + value.tag_value + '</span></div>';
                    }); 
                    $("#tag-data").html(tag_details);
                    $('#myTag').modal('show');

                }, error: function () {

                }
            });
        });
});

$(document).on("change", ".sub_chk", function() {
    var all_checked = $("input[type='checkbox'].sub_chk");

    var values = new Array();
    $.each($("input[type='checkbox']:checked"), function() {
        values.push($(this).val());
    });
    $('#check_id').val(values);

    if (all_checked.length == all_checked.filter(":checked").length) {
        $("#master").prop('checked', true);
    } else {
        $("#master").prop('checked', false);
    }
});

$('#master').on('change', function(e) {
    if ($(this).is(':checked', true)) {
        $(".sub_chk").prop('checked', true);
        var values = new Array();
        $.each($("input[class='sub_chk']:checked"), function() {
            values.push($(this).val());
        });
        $('#check_id').val(values);
    } else {
        $(".sub_chk").prop('checked', false);
    }
});

function action_delete_order(frm) {
    with(frm) {
        var flag = false;
        str = '';
        field = document.getElementsByName('gallery_img_id[]');
        for (i = 0; i < field.length; i++) {
            if (field[i].checked == true) {
                flag = true;
                break;
            } else
                field[i].checked = false;
        }
        if (flag == false) {
            swal("Please select atleast one record");
            return false;
        }
    }
    if (confirm("Are you sure to delete selected records ?")) {
        frm.action.value = "action_delete";
        // frm.mul_image_delete.value = $("#hidden_multi_order_id").val();
        frm.submit();
        return true;
    }
}
</script>
<?php
$this->load->view('c_level/image/view_image_popup');
?>
