<style type="text/css">
    .retailer-gallery-image-view .col-form-label {
        font-size: 13px;
        font-weight: bold;
    }
    .retailer-gallery-image-view .col-form-data span {
        line-height: 32px;
        font-size: 13px;
    }
</style>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Image Info</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">View Image</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width retailer-gallery-image-view">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                   <img src="<?php echo base_url(); ?><?=$images_tag_data->image?>" class="img-thumbnail" id="prevImg">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                $tags = $this->db->from('tag_tbl')->where('tag_tbl.created_by', $this->session->userdata('user_id'))->get()->result();
                                if (!empty($tags)) {
                                    foreach ($tags as $key => $val) {
                                        ?>
                                        <div class="form-group row">
                                            <label for="<?= $val->tag_name; ?>" class="col-sm-3 col-form-label"><?= $val->tag_name; ?> : </label>
                                            <div class="col-sm-9 col-form-data">
                                                <?php
                                                $image_id = $images_tag_data->image_id;
                                                if(in_array($val->id, $tag_ids)){
                                                ?>
                                                <span><?=$tag_data[$val->id]?></span>
                                            <?php 
                                                } else { ?>
                                                     <span></span>
                                               <?php 
                                            }
                                            ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                <div class="form-group text-left">
                    <a href="<?php echo base_url('edit_retailer_gallery_image/'.$images_tag_data->id); ?>" class="btn btn-primary w-md m-b-5" autocomplete="off">Edit</a>
                    <a href="<?= base_url('retailer_gallery_image') ?>" class="btn btn-success w-md m-b-5" autocomplete="off">Back</a>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
