<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/c_level/resources/css/daterangepicker.css" />
<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        /*top: 0px !important;*/
        top: 262px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Scan Product</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_b_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Scan Product';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><i class="fa fa-star" aria-hidden="true"></i></span>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    $message = $this->session->flashdata('message');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    if ($message != '') {
                        echo $message;
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <form class="form-horizontal" method="post">
                            <fieldset>
                                <div class="row text-center">
                                
                                    <form method="post">
                                        <div class="col-sm-6 col-md-4">
                                            <input type="hidden" class="form-control mb-3" placeholder="Scanned product Id" name="scan_product" autofocus id="scan_product">      
                                            <input type="text" class="form-control" placeholder="Scanned product Id" name="scan_product_disply" autofocus id="scan_product_disply">   
                                            <input type="hidden" class="form-control" name="existing_product_id" id="existing_product_id">   
                                        </div>
                                    </form>
                                
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('c_level/Scan_product/update_status') ?>">
                            <input type="hidden" name="payment_multiple" class="hidden_multi_order_id">
                            <div class="table-responsive px-3">
                                <table class="table table-bordered table-hover text-center" id="example1">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Order No</th>
                                            <th>Product Name</th>
                                            <th>Sidemark</th>
                                            <th>Order Details</th>
                                            <!-- <th>Tilt</th> -->
                                            <th>Room</th>
                                            <th>Manufactured DateTime</th>
                                            <th>Action</th>
                                        </tr>
                                </thead>
                                <tbody>                                                          
                                </tbody>
                                </table>
                                <div class="text-right">
                                    <button type="button" class="btn btn-sm btn-success btn-product-detail" id="btn_change_status" style="display: none;" data-toggle="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye"></i> Save Scanned Items</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<style type="text/css">
    .btn-product-detail{
        margin-top: 10px;
        float: right;
    }
    h3{
        border-bottom: none !important;
    }
</style>
<script src="<?php echo base_url(); ?>assets/c_level/resources/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/resources/js/jquery.scannerdetection.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"> 
    var table = $('#example1').DataTable( {
        "language": {
            "emptyTable": "No records found.."
        },
        "processing": true,
        "searching": false,
        'autoWidth': false,
        'paging': false,
        'info': false, 
        "ordering": false,
    });
    var count = 0;
    $("#scan_product_disply").change(function(){
        $('#btn_change_status').css("display","block");
        var existing_val = $('#existing_product_id').val();
        var existing_arr = existing_val.split(",");
        var scanned_val = $('#scan_product_disply').val();
        if(jQuery.inArray(scanned_val, existing_arr) !== -1){
            swal({
                title: "Alert!",
                text: "You have already scanned this item!",
            }).then(function() {
                    swal.close();
                    document.getElementById("scan_product_disply").focus();
            });
        } else{
            $.ajax({
                type: "POST",
                url : "<?php echo base_url('c_level/Scan_product/get_order_product_data') ?>",
                data: { orderId:  $("#scan_product_disply").val() },
                dataType: 'json',  
                crossDomain: true,
                beforeSend: function(){
                  $('.ajax-load').show();
                },    
                complete: function() {
                  $('.ajax-load').hide();
                },
                success: function(json){
                  // console.log(json.data);
                  if (json.data.length == 0) {
                    swal({
                        title: "Alert!",
                        text: "This item is not available in System!",
                    }).then(function() {
                            swal.close();
                            document.getElementById("scan_product_disply").focus();
                    });
                  } else{
                    $.each(json.data, function(key,val) {
                        count++;
                        table.rows.add(
                           [[ count,val.order_id,val.product_name,val.side_mark,val.product_name+'<br/>'+val.width+' x '+val.height+'(CM) '+val.color_name,val.room, val.manufactured_scan_date,'<a href="" data-order_id="'+val.row_id+'" class="editor_remove btn btn-danger btn-sm"><i class="glyph-icon simple-icon-trash"></i></a>' ]]
                        ).draw();
                    });
                    var existing_val = $('#existing_product_id').val();
                    var barcode_val_sting;
                    if(existing_val){
                        barcode_val_sting = (existing_val + ',' + scanned_val).replace(/^, /, '');
                    }else{
                        barcode_val_sting = (scanned_val).replace(/^, /, '')
                    }
                    $("#existing_product_id").val(barcode_val_sting);
                  }
                },
            });
        }
        $("#scan_product_disply").val('');
    })

    jQuery(document).ready(function(){
        jQuery('.loader-wrapper').css("display","none");
        document.getElementById("scan_product_disply").focus();
        $('#example1').on("click", "a.editor_remove", function(e){
            var order_ids = [];
            e.preventDefault();
            var clicked_order = $(this).data('order_id');

            $.ajax({
                "url": "<?php echo base_url('c_level/Scan_product/update_manufacture_date') ?>",
                "type": "POST",
                "data":{ manFactIdNull:  clicked_order },
                success: function (data) { 
                    // console.log('success');
                },error: function() {

                }
            });
            if( $('.editor_remove').length == 1 ) {
                $('#btn_change_status').css("display","none");
            }
            $('.editor_remove').each(function(){
                if(clicked_order != $(this).data('order_id')){
                    order_ids.push($(this).data('order_id'));
                }
            });
            var string_order_ids = order_ids.toString();
            var prev = $(this).prev().val(string_order_ids);
            $(this).parent().parent().remove();
            // $("#existing_product_id").val(string_order_ids);
        });

        $("#btn_change_status").click(function() {
            var orderIds = [];
            $('.editor_remove').each(function(){
                orderIds.push($(this).data('order_id'));
            });
            var orderRowIds = orderIds.toString();
            $.ajax({
                "url": "<?php echo base_url('c_level/Scan_product/update_status') ?>",
                "type": "POST",
                "data":{ orderRowIds:  orderRowIds },
                success: function (data) { 
                    window.location='<?php echo base_url('wholesaler-order-list') ?>';
                },error: function() {

                }
            });
        });
    });
    $('#scan_product_disply').scannerDetection({
        timeBeforeScanTest: 200,
        avgTimeByChar: 40, 
        preventDefault: true,
        minLength: 1,
        endChar: [13],
        onComplete: function(barcode, qty){
            validScan = true;
            $.ajax({
                "url": "<?php echo base_url('c_level/Scan_product/update_manufacture_date') ?>",
                "type": "POST",
                "data":{ manFactId:  barcode },
                success: function (data) { 
                    if (data) {
                        $("#scan_product_disply").val('');
                        $("#example1").dataTable().fnDestroy();
                        $("#scan_product_disply").trigger("change");
                        toastr.error(data);
                    }
                },error: function() {

                }
            });            
            $('#scan_product_disply').val(barcode);
            $("#scan_product_disply").trigger("change");
        },
        onError: function(string, qty) {
            $('#scan_product').val ($('#scan_product').val()  + string);
        }
    });
</script>
