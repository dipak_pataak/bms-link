<main>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css"/>
    <style>
        .card {
            background-color: #fff;
            border-radius: 10px;
            border: none;
            position: relative;
            margin-bottom: 30px;
            box-shadow: 0 0.46875rem 2.1875rem rgba(90, 97, 105, 0.1), 0 0.9375rem 1.40625rem rgba(90, 97, 105, 0.1), 0 0.25rem 0.53125rem rgba(90, 97, 105, 0.12), 0 0.125rem 0.1875rem rgba(90, 97, 105, 0.1);
        }

        .card .card-header {
            border-bottom-color: #f9f9f9;
            line-height: 30px;
            -ms-grid-row-align: center;
            align-self: center;
            width: 100%;
            padding: 10px 25px;
            display: flex;
            align-items: center;
        }

        .card .card-header h4 {
            font-size: 17px;
            line-height: 28px;
            padding-right: 10px;
            margin-bottom: 0;
            color: #212529;
            font-weight: 700;
        }

        .card .card-body {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .empty-state {
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 40px;
        }

        .empty-state .empty-state-icon {
            position: relative;
            background-color: #6777ef;
            width: 80px;
            height: 80px;
            line-height: 100px;
            border-radius: 5px;
        }

        .empty-state .empty-state-icon i {
            font-size: 40px;
            color: #fff;
            position: relative;
            z-index: 1;
        }

        .bg-danger {
            background-color: #fc544b !important;
        }

        .empty-state h2 {
            font-size: 20px;
            margin-top: 30px;
            font-weight: 700;
        }

        .card .card-body p {
            font-weight: 500;
            color: #212529;
        }

        .empty-state p {
            font-size: 16px;
        }

        .lead {
            line-height: 34px;
        }

        .btn {
            font-weight: 600;
            font-size: 12px;
            line-height: 24px;
            padding: 0.3rem 0.8rem;
            letter-spacing: 0.5px;
        }

        a.bb {
            text-decoration: none;
            border-bottom: 1px solid #2f4f4f;
            padding-bottom: 1px;
        }
        a:hover, a:active {
            text-decoration: initial;
            color: #2f4f4f;
        }

        .btn-warning,
        .btn-warning.disabled {
            box-shadow: 0 2px 6px #ffc473;
            background-color: #ffa426;
            border-color: #ffa426;
            color: #fff;
        }
    </style>
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="empty-state" data-height="400" style="height: 400px;">
                        <div class="empty-state-icon bg-danger">
                            <i class="fas fa-times"></i>
                        </div>
                        <h2>Access Denied</h2>
                        <p class="lead">
                        You don't have permission to access this resouce
                        </p>
                        <a href="#" class="btn btn-warning mt-4">Try Again</a>
                        <a href="#" class="mt-4 bb"><i class="fas fa-long-arrow-alt-left mr-2"></i> Go Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>