
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Supplier List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Supplier List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                 <?php
                    $user_id = $this->session->userdata('user_id');
                    $user_role_info = $this->db->select('*')->from('user_access_tbl')->where('user_id',$user_id)->get()->result();
                    $id=$user_role_info[0]->role_id;
                    $user_data = $this->db->select('*')
                                            ->from('role_permission_tbl')
                                            ->where('menu_id =',89)
                                            ->where('role_id', $id)
                                            ->get()->result();
                    $log_data = $this->db->select('*')
                                            ->from('log_info')
                                          
                                            ->where('user_id', $user_id)
                                            ->get()->result();
                                                               
                    ?>
                <div class="card mb-4">
                    <div class="card-body">
                        
                        <div class="col-sm-12">


                            <form class="form-horizontal" action="<?php echo base_url(); ?>c_level/Supplier_controller/supplier_filter" method="post">
                                <fieldset>
                                    <div class="row or-filter">
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control mb-2 sku" name="sku" placeholder="Enter Contact SKU">
                                        </div><div class="or_cls">-- OR --</div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control mb-2 name" name="name" placeholder="Enter Contact Name">
                                        </div><div class="or_cls">-- OR --</div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control mb-2 email" name="email" placeholder="Enter Email">
                                        </div><div class="or_cls">-- OR --</div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control mb-2 phone" name="phone" placeholder="Enter Phone No">
                                        </div>
                                        <div class="col-sm-2">
                                            <div>
                                                <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                                <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>


                            <div class="form-group row m-0 mb-3  float-right">
                                <label for="keyword" class="mb-2"></label>
                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <button onclick="window.location.href='<?= base_url(); ?>retailer-supplier/add-edit'" class="btn btn-info  mb-2" type="button">
                                    New Supplier
                                    <i class="fa fa-plus"> </i>
                                <?php } ?>

                                </button>
                               <!-- <button class="btn btn-info dropdown-toggle mb-2" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                    <span class="caret"></span></button>-->
                              <!--  <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-csv')" class="dropdown-item">Export to CSV</a></li>
                                    <li><a href="javascript:void(0)" onClick="ExportMethod('<?php /*echo base_url(); */?>retailer-customer-export-pdf')"  class="dropdown-item">Export to PDF</a></li>
                                    <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                                </ul>-->
                            </div>
                        </div>


                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>Serial No.</th>
                                        <th>Supplier's Company</th>
                                        <th>Contact Name</th>
                                        <th>Material/Product</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                     <?php
                    $sl = 0;
                    foreach ($supplier_list as $supplier) {
                        $this->db->select('a.id, a.material_name');
                        $this->db->from('row_material_tbl a');
                        $this->db->join('supplier_raw_material_mapping  b', 'b.raw_material_id = a.id');
                        $this->db->where('b.supplier_id', $supplier->supplier_id);
                        $supplier_material_results = $this->db->get()->result();
                        $sl++
                        ?>
                        <tr>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $supplier->company_name; ?></td>
                            <td><?php echo $supplier->supplier_name; ?></td>
                            <td class="text-left">
                                <?php
                                $i = 0;
                                foreach ($supplier_material_results as $result) {
                                    $i++;
                                    echo "<ul>";
                                    echo "<li>" . $i . ") " . $result->material_name . "</li>";
                                    echo "</ul>";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="tel:<?php echo $supplier->phone; ?>"><?php echo $supplier->phone; ?></a>
                            </td>
                            <td>
                                <a href="mailto:<?php echo $supplier->email; ?>"><?php echo $supplier->email; ?></a>
                            </td>
                            <!--<td>000</td>-->
                            <td class="text-right">
                                <?php if($user_data[0]->can_access==1 or $log_data[0]->is_admin==1){ ?>
                                <a href="<?php echo base_url(); ?>c_level/supplier_controller/supplier_invoice/<?php echo $supplier->supplier_id; ?>" class="btn btn-success default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="simple-icon-eye"></i></a>
                                 <?php } ?>
                                <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                                <a href="<?php echo base_url(); ?>retailer-supplier/add-edit/<?php echo $supplier->supplier_id; ?>" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="simple-icon-pencil"></i></a>
                                <?php } ?>
                                <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                <a href="<?php echo base_url(); ?>c_level/Supplier_controller/supplier_delete/<?php echo $supplier->supplier_id; ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="Top" title="Delete" onclick="return confirm('Do you want to delete it!')"><i class="simple-icon-trash"></i></a>
                                <?php } ?>
    <!--                                <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>-->
                            </td>
                        </tr>
                    <?php } ?>
                                </tbody>
                                    <?php if (empty($supplier_list)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>

                        <?php echo $links; ?>
                    </div> 
                  
                </div>
            </div>
        </div>
    </div>
</main>



