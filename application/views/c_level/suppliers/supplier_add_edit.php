<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Supplier Add</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Supplier Add</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '')
                    {
                        echo $error;
                    }
                    if ($success != '')
                    {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                            
                        <?php 
                            $url = '';
                              if (isset($data) && !empty($data)) 
                              { 
                                 $url = 'supplier_update/'.$data->supplier_id;
                              }else{
                                 $url = 'supplier_save';
                              }
                            ?>


                           <form action="<?php echo base_url(); ?>c_level/supplier_controller/<?php echo $url; ?>" method="post">

                            <input type="hidden" name="supplier_id" value="<?php if(isset($data->supplier_id) && !empty($data->supplier_id)){ echo $data->supplier_id; }else{ echo 0;}?>">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="company_name">Supplier’s Company <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="company_name"
                                           name="company_name"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->company_name) && !empty($data->company_name)): echo $data->company_name; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="supplier_name">Contact Name <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="supplier_name"
                                           name="supplier_name"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->supplier_name) && !empty($data->supplier_name)): echo $data->supplier_name; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="supplier_sku">Supplier Sku <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="supplier_sku"
                                           name="supplier_sku"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->supplier_sku) && !empty($data->supplier_sku)): echo $data->supplier_sku; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">Email <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="email"
                                           name="email"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->email) && !empty($data->email)): echo $data->email; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="phone">Phone <span class="text-danger"> * </span></label>
                                    <input type="number"
                                           class="form-control"
                                           id="phone"
                                           name="phone"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->phone) && !empty($data->phone)): echo $data->phone; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="raw_material_id">Material/Product</label>
                                    <select class="form-control" id="raw_material_id" name="raw_material_id[]" >
                                        <option value="0">Select Material</option>

                                            <?php
                                                $raw_material = array();
                                                foreach ($supplier_wise_material as $single_material) {
                                                    $raw_material[] = $single_material->raw_material_id;
                                                }
                                        foreach ($get_raw_material as $val) {
                                            ?>
                                            <option value="<?= $val->id ?>" <?php
                                            if (in_array($val->id, $raw_material)) {
                                                echo 'selected';
                                            }
                        ?>><?= ucwords($val->material_name); ?></option>
                            <?php } ?>

                                    </select>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="address">Address <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="address"
                                           name="address"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->address) && !empty($data->address)): echo $data->address; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="city">City <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="city"
                                           name="city"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->city) && !empty($data->city)): echo $data->city; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="state">Status <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="state"
                                           name="state"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->state) && !empty($data->state)): echo $data->state; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="zip">Zip <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="zip"
                                           name="zip"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->zip) && !empty($data->zip)): echo $data->zip; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="country_code">Country Code <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="country_code"
                                           name="country_code"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->country_code) && !empty($data->country_code)): echo $data->country_code; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="previous_balance">Previous Balance <span class="text-danger"> * </span></label>
                                    <input type="text"
                                           class="form-control"
                                           id="previous_balance"
                                           name="previous_balance"
                                           onkeyup="required_validation()"
                                           value="<?php if(isset($data->previous_balance) && !empty($data->previous_balance)): echo $data->previous_balance; endif; ?>"
                                           required>
                                    <div class="valid-tooltip">
                                        Looks good!
                                    </div>
                                </div>

                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-sm d-block float-right customer_btn">
                                <?php if(isset($data) && !empty($data))
                                    echo 'Update';
                                else
                                    echo 'Add';
                                ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

  /    ============= its for  google geocomplete =====================
<script>
  
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));

        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            //console.log(place);
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    //console.log(results)
                    if (results[0]) {
                        //document.getElementById('location').innerHTML = results[0].formatted_address;
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;

                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;

                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }

                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }

                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
//                        console.log(zipcode);
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        Swal.fire('No results found');
                    }
                } else {
                    Swal.fire('Geocoder failed due to: ' + status);
                }
            });

        });


    });
</script>