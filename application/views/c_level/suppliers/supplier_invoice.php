
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Manage Invoice</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage Invoice</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-right">
                                <label for="keyword" class="mb-2"></label>
                               <!--  <button onclick="window.location.href='<?= base_url(); ?>c-supplier/add-edit'" class="btn btn-info  mb-2" type="button">
                                    New Supplier
                                    <i class="fa fa-plus"> </i>
                                </button> -->
                           
                            </div>
                        </div>



                         <div class="collapse px-3 mb-3" id="collapseExample">
            <div class="border px-3 pt-3">
                <form class="form-horizontal" method="post">
                    <fieldset>

                        <div class="row">

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Supplier ID">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Name">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Product">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Stock">
                            </div>

                            <div class="col-md-4">
                                <input type="text" class="form-control mb-3" placeholder="Phone">
                            </div>

                            <div class="col-md-4">
                                <input type="email" class="form-control mb-3" placeholder="email">
                            </div>

                            <div class="col-md-12 text-right">
                                <div>
                                    <button type="reset" class="btn btn-sm btn-danger default">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                </div>
                            </div>

                        </div>

                    </fieldset>

                </form>
            </div>
        </div>


                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>SL No.</th>
                                        <th>Supplier Name</th>
                                        <th>Purchase id</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Payment Type</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                     <?php
                                        if ($get_supplier_invoice != NULL) {
                                            $i = 1;
                                            foreach ($get_supplier_invoice as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $val->supplier_name; ?></td>
                                                    <td><?= $val->purchase_id; ?></td>
                                                    <td><?= date('M-d-Y', strtotime($val->date)); ?></td>
                                                    <td><?= $val->grand_total; ?></td>
                                                    <td><?= ($val->payment_type == 1 ? 'Cash' : 'Check'); ?></td>
                                                    <td class="text-right">
                                                        <a href="<?php echo base_url(); ?>view-purchase/<?= $val->purchase_id ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <!--<a href="edit-purchase/<?= $val->purchase_id ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="edit"><i class="fa fa-edit" aria-hidden="true"></i></a>-->
                                                        <a href="<?php echo base_url(); ?>raw-material-return-purchase/<?= $val->purchase_id ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Return "><i class="fa fa-retweet" aria-hidden="true"></i></a>
                                                        <a href="<?php echo base_url(); ?>delete-purchase/<?= $val->purchase_id ?>" onclick="return confirm('Are you sure want to delete it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                    ?>
                                </tbody>
                                    <?php if (empty($get_supplier_invoice)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>

                        <?php // echo $links; ?>
                    </div> 
                  
                </div>
            </div>
        </div>
    </div>
</main>



