<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Manage Quotation</h1>
                    <?php
                        $page_url = $this->uri->segment(1). '/' .$this->uri->segment(2). '/' .$this->uri->segment(3);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Manage Quotation';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <?php
                        $user_data=access_role_permission(43);
                        $log_data=employ_role_access($id);                                                                
                    ?>
                  
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Quotation</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>



        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="text-right">
                      <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                    <a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
                <?php } ?>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <div class="">
                            <?php
                            $error = $this->session->flashdata('error');
                            $success = $this->session->flashdata('success');
                            if ($error != '') {
                                echo $error;
                            }
                            if ($success != '') {
                                echo $success;
                            }
                            ?>
                        </div>
                        <div class="table-responsive">
                            <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/quotation_controller/manage_action') ?>">
                                <input type="hidden" name="action">
                                <table class="table table-bordered table-hover text-center">

                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="SellectAll"/></th>
                                            <th>S.no</th>
                                            <th>Customer Name </th>
                                            <th>Quotation date</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php
                                        if (!empty($quotation)) {
                                            $i = 1;
                                            foreach ($quotation as $key => $value) {
                                                ?>

                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $value->qt_id; ?>" class="checkbox_list">  
                                                    </td>
                                                    <td><?= $i; ?></td>
                                                    <td><?= $value->customer_name; ?></td>
                                                    <td><?= (@$value->qt_order_date); ?></td>
                                                    <td><?= $currency; ?><?= $value->qt_grand_total ?></td>

                                                    <td>
                                                       <?php if($user_data[0]->can_access==1 or $log_data[0]->is_admin==1){ ?>
                                                        <a href="<?= base_url('retailer-quotation-receipt/') ?><?= $value->qt_id; ?>" class="btn btn-success btn-xs default"  title="View" data-toggle="tooltip" data-placement="top" data-original-title="View">
                                                            Final Quot./Order
                                                        </a>
                                                        <?php } ?>
                                                        <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                        <a href="<?= base_url('retailer-edit-quotation/') . $value->qt_id ?>" class="btn btn-warning btn-xs" title="Edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="glyph-icon simple-icon-pencil"></i></a>
                                                        <?php } ?>
                                                        <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                        <a href="<?= base_url('retailer-delete-quotation/') . $value->qt_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
                                                    <?php } ?>
                                                    </td>


                                                </tr>

                                                <?php
                                                $i++;
                                            }
                                        } else {
                                            ?>

                                        <div class="alert alert-danger"> There have no order found..</div>
                                    <?php } ?>

                                    </tbody>
                                </table>
                            </form>     
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
