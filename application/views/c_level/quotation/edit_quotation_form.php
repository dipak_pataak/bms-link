<style>

    .price-details{
        width: 80%;
        margin: auto;
    }

    /*.price-details tr td:last-child{
        height: 30px;
        vertical-align: middle;
        width: 25%;
    }

    .price-details tr td:last-child{
        text-align: right;
    }

    .price-details tr:last-child td{
        height: 40px;
        border-top: 1px solid #fff;
    }*/



    .fixed_item{
        background: #145388;
        padding: 15px !important;
    }
    #tprice {
        color: #fff;
    }
    .overlay{
        position: fixed;
        height: 100%;
        width: 100%;
        top: 0px;
        left: 0px;
        background-color: rgba(255,255,255,0.95);
        z-index: 1;
        color: #3498db;
        text-align: center;
    }

    .loader {
        display: block;
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20%;
    }

    .hidden{
        display: none;
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
</style>
    <div class="row" style="position: relative;">
        <div class="overlay">
            <div class="loader"></div>
            <br>
            <br>
            <h2>Please Wait while form is loading</h2>
        </div>
        <div class="col-sm-9">
            <?= form_open('c_level/quotation_controller/update_quotation_details', array('id' => 'UpdateQuotDetails')) ?>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="row">
                        <label for="" class="col-sm-3">Select Category</label>
                        <div class="col-sm-4">
                            <select class="form-control select2-single" name="category_id" id="category_id"  required="" data-placeholder="--select one --">
                                <option value=""></option>
                                <?php
                                foreach ($get_category as $category) {
                                    echo "<option value='$category->category_id'>$category->category_name</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12" id="subcategory_id">
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <label for="" class="col-sm-3">Select Product</label>
                        <div class="col-sm-4">
                            <select class="form-control select2-single" name="product_id" id="product_id" onchange="getAttribute(this.value)" required data-placeholder="-- select one --">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12" id="color_model">
                    <div class="row">
                        <label for="" class="col-sm-3">Pattern</label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="pattern_model_id" id="pattern_id" data-placeholder="-- select pattern --" required="">
                                <option value="">-- select pattern --</option>
                                <!-- <option value="1">Smooth</option> -->
                            </select>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group col-md-12" id="pattern_color_model">
                    <div class="row">
                        <label for="" class="col-sm-3">Color</label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="color_id" id="color_id" onchange="getColorCode(this.value)" required="" data-placeholder="-- select one --">
                                <option value="">-- select one --</option>
                                <option value="11">Alabaster</option>
                                <option value="10">Antique White</option>
                                <option value="9">White White</option>
                            </select>
                        </div>
                        <div class="col-sm-1">Color code :</div>
                        <div class="col-sm-2">
                            <input type="text" id="colorcode" onkeyup="getColorCode_select(this.value)" class="form-control">
                        </div>
                    </div>
                </div> -->
                <div class="form-group col-md-12">
                    <div class="row">
                        <label for="" class="col-sm-3">Width</label>
                        <div class="col-sm-4">
                            <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0"  required >
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' >
                                
                                <?php
                                foreach ($fractions as $f) {
                                    echo "<option value='$f->id'>$f->fraction_value</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <label class="col-sm-3">Height</label>
                        <div class="col-sm-4">
                            <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0" required >
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control " name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' >
                                
                                <?php
                                foreach ($fractions as $f) {
                                    echo "<option value='$f->id'>$f->fraction_value</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12" id="ssssttt14"></div>
                <div class="form-group col-md-12">
                </div>
                <!-- atributs area -->
                <div class="form-group col-md-12" id="attr">
                </div>
                <!-- End atributs area -->
                <div class="form-group col-md-12">
                    <div class="row">
                        <label for="" class="col-sm-3">Room</label>
                        <div class="col-sm-4">
                            <select class="form-control select2-single" name="room" id="room"  required data-placeholder="--select one --" >
                                <option value=""></option>
                                <?php
                                foreach ($rooms as $r) {
                                    echo "<option value='$r->room_name'>$r->room_name</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <label class="col-sm-3">Note</label>
                        <div class="col-sm-4">
                            <textarea class="form-control" name="notes" rows="6" id="notes"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-11 mb-0 btns-box">
                    <input type="hidden" name="update_product" value="<?php echo $quotDetails['selectedKey']; ?>">
                    <button type="submit" class="btn btn-primary mb-0 float-right">Update Product</button>
                </div>
            </div>
            <input type="hidden" name="total_price" id="total_price">
            <?= form_close(); ?>
        </div>
        <div class="col-sm-3">
            <div class="fixed_item">
                <div id="tprice"></p>
                </div>
            </div>
        </div>
    </div>
<script>

    var var_currency = '$';

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////-------category product pattern change events starts | Height and width keyup / change events starts----------->
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('body').on('change', '#category_id', function () {
        var category_id = $(this).val();
        onCategoryChange(category_id);
    });
    $('body').on('change', '#product_id', function () {
        var product_id = $(this).val();
        onProductChange(product_id);
    });
    $('body').on('change', '#pattern_id', function () {
        var pattern_id = $(this).val();
        OnPatternChange(pattern_id);
    });
    $('body').on('keyup', '#width', function () {
        var hif = $(this).val().split(".")[1];
        var category_id = $('#category_id').val();
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_height_width_fraction/" + hif + "/" + category_id,
                type: 'get',
                success: function (r) {
                    $("#width_fraction_id ").val(r);
                }
            });
        } else {
            $("#width_fraction_id ").val('');
        }
    });
    $('body').on('keyup', '#height', function () {
        var hif = $(this).val().split(".")[1];
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id ").val(r);
                }
            });
        } else {
            $("#height_fraction_id ").val('');
        }
    });
    $('body').on('change', function () {
        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);
    });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////-------category product pattern change events starts | Height and width keyup / change events End----------->
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function loadPStyle(callback = false) {
        var product_id = $('#product_id').val();
        var pattern_id = $('#pattern_id').val();
        var pricestyle = $('#pricestyle').val();
        var price = $("#height").parent().parent().parent().next();
        var hif = ($("#height_fraction_id :selected").text().split("/")[0] / $("#height_fraction_id :selected").text().split("/")[1]);
        var wif = ($("#width_fraction_id :selected").text().split("/")[0] / $("#width_fraction_id :selected").text().split("/")[1]);
        var width = parseFloat($('#width').val()) + (isNaN(wif) ? 0 : wif);
        var height = parseFloat($('#height').val()) + (isNaN(hif) ? 0 : hif);
        var width_w = (isNaN(width) ? '' : width);
        var height_w = (isNaN(height) ? '' : height);
        if (pricestyle !== '2') {
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_row_col_price/') ?>" + height_w + "/" + width_w + "/" + product_id + "/" + pattern_id,
                type: 'get',
                success: function (r) {
                    var obj = jQuery.parseJSON(r);
                    $(price).html(obj.ht);
                    if (isNaN(parseFloat(obj.prince))) {
                        // $("#tprice").text("Total Price = $0");
                        $("#tprice").text("");
                    } else {
                        // $("#tprice").text("Total Price = $" + parseFloat(obj.prince));
                        $("#tprice").text("");
                    }
                    // $("#height").val(obj.col);
                    if (obj.st === 1) {
                        $('#cartbtn').removeAttr('disabled');
                    } else if (obj.st === 2) {
                        //$('#cartbtn').prop('disabled', true);
                    }
                    if (callback != false) {
                        callback();
                    }
                    cal();
                }
            });
        } else if (pricestyle === '2') {
            var main_p = parseFloat($('#sqr_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var new_width = parseFloat(width + 3);
            var new_height = parseFloat(height + 1.5);

            var sum = (new_width * new_height) / 144;
            
            // var sum = (width * height) / 144;
            var price = main_price * sum;
            $('#main_price').val(price.toFixed(2));
            if (callback != false) {
                callback();
            }
            cal();
        } else {
            if (callback != false) {
                callback();
            }
            cal();
        }
        //console.log('loadPStyle calculated');
    }

    function callTrigger() {
        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }

    function cal() {
        var contribut_prices_array = {};

        var w = $('body #width').val();
        var h = $('body #height').val();
        if (w !== '' && h !== '') {
            var contribut_price = 0;
            $("body .contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contribut_price += parseFloat(this.value));
                var contri_price_for = $(this).parent().parent().children('label').text();

                // For cart item order issue : START
                contri_price_for = " "+contri_price_for;
                // For cart item order issue : END
                
                if (contribut_prices_array[contri_price_for] == undefined) {
                    contribut_prices_array[contri_price_for] = parseFloat(this.value);
                } else {
                    contribut_prices_array[contri_price_for] += parseFloat(this.value);
                }
            });

            var main_price = parseFloat($('#main_price').val());


            //console.log(contribut_prices_array);
            var priceListHtml = '<table class="price-details">';
            priceListHtml += "<tr><td>W*H:</td><td>"+ var_currency + main_price +"</td></tr>";
            for (var key in contribut_prices_array) {
                if (contribut_prices_array.hasOwnProperty(key)) {
                    if (contribut_prices_array[key] > 0) {
                        priceListHtml += "<tr><td>"+key+":</td><td>"+ var_currency + contribut_prices_array[key] +"</td></tr>";
                    }
                }
            }
            if (isNaN(main_price)) {
                var prc = 0;
            } else {
                var prc = main_price;
            }
            var total_price = (contribut_price + prc);
            var t = (isNaN(total_price) ? 0 : total_price);
            $("body #total_price").val(t.toFixed(2));
            //$("body #tprice").text("Total Price = " + var_currency + t);
            // priceListHtml += "<tr><td>Total Price = </td><td>" + var_currency + t +"</td></table>";
            $("body #tprice").html(priceListHtml);
        }
        //console.log('calculated');
        // console.log(contribut_price);       
        // console.log(main_price);       
        // console.log(total_price);       
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////-------function for change trigger with callback functionality starts here--------------------->
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function onCategoryChange(selectedCat, callback = false) {
        if ($('#category_id').find(":selected").text() == 'Arch') {
            $('#height').val(0).attr('disabled', 'true');
            $('#height_fraction_id').val(0).attr('disabled', 'true');
            $('#height').parent().parent('.row').parent('.form-group').hide();
        } else {
            $('#height').removeAttr('disabled');
            $('#height_fraction_id').removeAttr('disabled');
            $('#height').parent().parent('.row').parent('.form-group').show();
        }
        $.ajax({
            url: "<?php echo base_url('c_level/order_controller/category_wise_subcategory/') ?>" + selectedCat,
            type: 'get',
            success: function (r) {
                $("#subcategory_id").html(r);
                $.ajax({
                    url: "<?php echo base_url('c_level/order_controller/get_product_by_category/') ?>" + selectedCat,
                    type: 'get',
                    success: function (r) {
                        $("#product_id").html(r);
                        $.ajax({
                            url: "<?php echo base_url('c_level/order_controller/get_cat_fraction/') ?>" + selectedCat,
                            type: 'get',
                            dataType: 'JSON',
                            success: function (r) {
                                $("#width_fraction_id").html('');
                                $("#width_fraction_id").html(r.html);
                                $("#height_fraction_id").html('');
                                $("#height_fraction_id").html(r.html);
                                if (callback != false) {
                                    callback();
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    function onProductChange(selectedProduct, callback = false) {
        $.ajax({
            url: "<?php echo base_url('c_level/order_controller/get_color_partan_model/') ?>" + selectedProduct,
            type: 'get',
            success: function (r) {
                $('#color_model').html(r);
                if (callback != false) {
                    callback();
                }
                callTrigger();
            }
        });
    }

    function OnPatternChange(selectedPattern, callback = false) {
        var product_id = $('body #product_id').val();
        if (product_id != "" && product_id != null) {
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_color_model/') ?>" + product_id + "/" + selectedPattern,
                type: 'get',
                success: function (r) {
                    $('body #pattern_color_model').html('');
                    $('body  #pattern_color_model').html(r);
                    $('body #color_id').parent().removeClass('col-sm-3').addClass('col-sm-6');
                    $('body #color_id').parent().next().removeClass('col-sm-2').addClass('col-sm-1');
                    if (callback != false) {
                        callback();
                    }
                }
            });
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////-------function for change trigger with callback functionality ends here--------------------->
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $(document).ready(function () {
        if($('.sticky_container .sticky_item').length > 0){
            $('.sticky_container .sticky_item').theiaStickySidebar({
                additionalMarginTop: 110
            });
        }   
    }); 

    $( document ).ajaxStop(function(){
        //Swal.fire('ajax is completed');
        $('.overlay').hide();
    });
    $(document).ajaxStart(function(){
        $('.overlay').show();
    });


    $('#category_id').val(<?php echo $quotDetails['category_id']; ?>);
    $('#room').val("<?php echo $quotDetails['room']; ?>");
    $('#notes').val("<?php echo $quotDetails['notes']; ?>");
    onCategoryChange("<?php echo $quotDetails['category_id']; ?>", function () {
        $('#product_id').val("<?php echo $quotDetails['product_id']; ?>");
        onProductChange("<?php echo $quotDetails['product_id']; ?>", function () {
            $('#pattern_id').val("<?php echo $quotDetails['pattern_model_id']; ?>");
            OnPatternChange("<?php echo $quotDetails['pattern_model_id']; ?>", function () {
                $('#width').val("<?php echo $quotDetails['width']; ?>");
                //$('#width_fraction_id').val("<?php echo $quotDetails['width_fraction_id']; ?>");

                $('#width_fraction_id option').each(function(){
                    if ($(this).text() == "<?php echo $quotDetails['width_fraction_id']; ?>") {
                        $(this).attr('selected', true);
                    }
                });

                $('#height').val("<?php echo $quotDetails['height']; ?>");
                //$('#height_fraction_id').val("<?php echo $quotDetails['height_fraction_id']; ?>");

                $('#height_fraction_id option').each(function(){
                    if ($(this).text() == "<?php echo $quotDetails['height_fraction_id']; ?>") {
                        $(this).attr('selected', true);
                    }
                });
                loadPStyle();
            });
        });
    });
</script>
