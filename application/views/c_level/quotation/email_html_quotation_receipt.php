<!DOCTYPE html>
<html>
    <head>
        <style>
            table { 
                border-spacing: 0px;
                border-collapse: separate;
            }
            table.table_print tr th, table.table_print tr td {
                border-color: #222 !important;
                border:1px solid #222;
                padding: 5px;
            }
            .title-center{
                text-align:center;
                padding:10px;
                margin-bottom:15px;
                border-bottom: 1px solid black;
            }
            .left-content{
                max-width:48%;
                float:left;
            }
            .right-content{
                max-width:48%;
                float:right;
            }
        </style>
    </head>
    <body>
        <div class="row" id="printableArea" style="background-color: #FFF;">

            <div class="col-lg-5 col-md-5 col-sm-5">
                <img
                    src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">
            </div>

            <div class="col-lg-7 col-md-7 col-sm-7 mb-4 title-center">
                <h2 style="margin-top: 100px;">Quotation Comparison</h2>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-6 left-content">
                <div class="card mb-4">
                    <div class="card-body">
                        <h2><?= $company_profile[0]->company_name; ?>,</h2>
                        <h4><?= $company_profile[0]->address; ?>, <?= $company_profile[0]->city; ?>,</h4>
                        <h4><?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                            <?= $company_profile[0]->country_code; ?></h4>
                        <h4><?= $company_profile[0]->phone; ?></h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 offset-lg-1 offset-md-1 right-content">
                <div class="card mb-4">
                    <div class="card-body">
                        <table>
                            <tr>
                                <td style="width: 40%;"><strong>Date</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= date_format(date_create($quotation->qt_order_date), 'Y-M-d'); ?></td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;<br /></td>
                            </tr>
                            <tr>
                                <td><strong>Customer Name</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->first_name . " " . $quotation->last_name; ?></td>
                            </tr>
                            <?php if ($quotation->company != "") { ?>
                                <tr>
                                    <td><strong>Company Name</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->company; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><strong>Address</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->address; ?>, <?= $quotation->city; ?>,
                                    <?= $quotation->state; ?>,
                                    <?= $quotation->zip_code; ?>, <?= $quotation->country_code; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Phone Number</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->phone; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Email ID</strong></td>
                                <td>&nbsp;:&nbsp;&nbsp;</td>
                                <td><?= $quotation->email; ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>

            <?php $invoiceDetailsArray = []; ?>

            <div class="col-lg-12">

                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Details</h5>
                        <div class="separator mb-5"></div>
                        <div class="table-responsive">
                            <style>
                                table.table_print tr th {
                                    padding-left:5px !important;
                                    padding-right:5px !important;
                                }
                                table.table_print tr th,
                                table.table_print tr td {
                                    border-color: #000 !important;
                                    border: 2px solid black !important;
                                    border-style: solid !important;
                                }
                            </style>
                            <table class=" table table-bordered table-hover table_print"
                                   style="border:2px solid black;">
                                <thead>
                                    <tr style="border:1px solid black;">
                                        <th style="border:1px solid black;">SL</th>
                                        <th style="border:1px solid black;">Room</th>
                                        <th style="border:1px solid black;" colspan="2">Window size</th>
                                        <th style="border:1px solid black;">Comment</th>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $key => $val) {
                                            if ($val == "Misc") {
                                                echo "<th style='border:1px solid black;' colspan='2'>" . $val . "</th>";
                                            } else {
                                                echo "<th style='border:1px solid black;' colspan='3'>" . $val . "</th>";
                                            }

                                            $invoiceDetailsArray[$val . "-" . $catNo]['subtotal'] = 0;
                                            $invoiceDetailsArray[$val . "-" . $catNo]['discount'] = 0;
                                            $invoiceDetailsArray[$val . "-" . $catNo]['upcharge'] = 0;
                                            $invoiceDetailsArray[$val . "-" . $catNo]['salestax'] = 0;
                                            $invoiceDetailsArray[$val . "-" . $catNo]['grandtotal'] = 0;
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <th style="border:1px solid black;">SL#</th>
                                        <th style="border:1px solid black;">Room</th>
                                        <th style="border:1px solid black;">Width</th>
                                        <th style="border:1px solid black;">Height</th>
                                        <th></th>
                                        <?php foreach ($categories as $key => $val) { ?>
                                            <th style="border:1px solid black;">Product </th>
                                            <?php if ($val != 'Misc') { ?><th style="border:1px solid black;">Pattern
                                                </th>
                                            <?php } ?>
                                            <th style="border:1px solid black;">Price</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($roomWiseArray as $key => $room) { ?>
                                        <tr>
                                            <td rowspan="<?php echo sizeof($room); ?>"><?php echo $key + 1; ?></td>
                                            <td rowspan="<?php echo sizeof($room); ?>">
                                                <?php echo $room[0]->room_name; ?>
                                            </td>
                                            <?php
                                            for ($i = 0; $i < sizeof($room); $i++) {
                                                if ($i == 0) {
                                                    ?>
                                                    <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?>
                                                    </td>
                                                    <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $room[$i]->win_comment; ?>
                                                    </td>
                                                    <?php
                                                    $roomNo = $room[$i]->qd_id;
                                                    $winCategories = json_decode($room[$i]->win_categories);
                                                    $newWinCategories = [];
                                                    $isPrinted = false;
                                                    $catNo = 1;
                                                    foreach ($categories as $cat => $catVal) {
                                                        if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                            for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                                if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                                                    echo "<td>" . $winCategories[$j]->win_product_name . "</td>";
                                                                    if ($winCategories[$j]->win_category_name != "Misc") {
                                                                        echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                    }
                                                                        $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);
                                                                    echo "<td>" . sprintf("%.2f",(round($discounted_price,2))). "</td>";
                                                                    $isPrinted = true;

                                                                    if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                        $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                    } else {
                                                                        $fSubTotal = 0;
                                                                    }

                                                                    if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                        $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                    } else {
                                                                        $fUpcharges = 0;
                                                                    }

                                                                    if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                        $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                    } else {
                                                                        $fDiscount = 0;
                                                                    }

                                                                    $fTotal = $fSubTotal + $fUpcharges;

                                                                    if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                        $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                    } else {
                                                                        $fsalestax = 0;
                                                                    }

                                                                    if ($fSubTotal != 0) {
                                                                        $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                    }
                                                                } else {
                                                                    array_push($newWinCategories, $winCategories[$j]);
                                                                }
                                                            }
                                                            if ($isPrinted != true) {
                                                                if ($catVal == 'Misc') {
                                                                    echo "<td colspan='2'>&nbsp;</td>";
                                                                } else {
                                                                    echo "<td colspan='3'>&nbsp;</td>";
                                                                }
                                                            }
                                                            $winCategories = $newWinCategories;
                                                            $newWinCategories = [];
                                                            $isPrinted = false;
                                                        } else {
                                                            if ($catVal == "Misc") {
                                                                echo "<td colspan='2'>empty</td>";
                                                            } else {
                                                                echo "<td colspan='3'>empty</td>";
                                                            }
                                                        }
                                                        $catNo++;
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        if (sizeof($room) > 0) {
                                            ?><?php
                                            for ($i = 0; $i < sizeof($room); $i++) {
                                                if ($i != 0) {
                                                    ?><tr class="here">
                                                        <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?>
                                                        </td>
                                                        <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $room[$i]->win_comment; ?>
                                                        </td>
                                                        <?php
                                                        $roomNo = $room[$i]->qd_id;
                                                        $winCategories = json_decode($room[$i]->win_categories);
                                                        $newWinCategories = [];
                                                        $isPrinted = false;
                                                        $catNo = 1;
                                                        foreach ($categories as $cat => $catVal) {
                                                            if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                                for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                                    if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                                                        echo "<td>" . $winCategories[$j]->win_product_name . "</td>";
                                                                        if ($winCategories[$j]->win_category_name != "Misc") {
                                                                            echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                        }
                                                                        $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);
                                                                        echo "<td>" . sprintf("%.2f",(round($discounted_price,2))). "</td>";
                                                                        $isPrinted = true;

                                                                        if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                            $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                        } else {
                                                                            $fSubTotal = 0;
                                                                        }

                                                                        if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                            $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                        } else {
                                                                            $fUpcharges = 0;
                                                                        }

                                                                        if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                            $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                        } else {
                                                                            $fDiscount = 0;
                                                                        }

                                                                        $fTotal = $fSubTotal + $fUpcharges;

                                                                        if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                            $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                        } else {
                                                                            $fsalestax = 0;
                                                                        }

                                                                        if ($fSubTotal != 0) {
                                                                            $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                        }
                                                                    } else {
                                                                        array_push($newWinCategories, $winCategories[$j]);
                                                                    }
                                                                }
                                                                if ($isPrinted != true) {
                                                                    if ($catVal == 'Misc') {
                                                                        echo "<td colspan='2'>&nbsp;</td>";
                                                                    } else {
                                                                        echo "<td colspan='3'>&nbsp;</td>";
                                                                    }
                                                                }
                                                                $winCategories = $newWinCategories;
                                                                $newWinCategories = [];
                                                                $isPrinted = false;
                                                            } else {
                                                                if ($catVal == "Misc") {
                                                                    echo "<td colspan='2'>empty</td>";
                                                                } else {
                                                                    echo "<td colspan='3'>empty</td>";
                                                                }
                                                            }
                                                            $catNo++;
                                                        }
                                                    }
                                                    ?>
                                                </tr><?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="5">Sub Total (with discount)
                                            (<?= @$company_profile[0]->currency; ?>)</td>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $cat => $catVal) {
                                            if ($catVal == "Misc") {
                                                echo "<td style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                            } else {
                                                echo "<td colspan='2' style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                            }
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td colspan="5">Total Discount(<?= @$company_profile[0]->currency; ?>)</td>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $cat => $catVal) {
                                            if ($catVal == "Misc") {
                                                echo "<td style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                            } else {
                                                echo "<td colspan='2' style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                            }
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td colspan="5">Upcharge</td>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $cat => $catVal) {
                                            if ($catVal == "Misc") {
                                                echo "<td style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                            } else {
                                                echo "<td colspan='2' style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                            }
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    $total = $quotation->qt_subtotal - $quotation->total_discount + $quotation->total_upcharge;
                                    ?>
                                    <tr>
                                        <td colspan="5">Sales Tax
                                            (<?= number_format($quotation->qt_tax, 2, '.', ''); ?>%)</td>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $cat => $catVal) {
                                            if ($catVal == "Misc") {
                                                echo "<td style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                            } else {
                                                echo "<td colspan='2' style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                            }
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td colspan="5">Grand Total (<?= @$company_profile[0]->currency; ?>)</td>
                                        <?php
                                        $catNo = 1;
                                        foreach ($categories as $cat => $catVal) {
                                            if ($catVal == "Misc") {
                                                echo "<td style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
                                            } else {
                                                echo "<td colspan='2' style='border-right:0;'></td>";
                                                echo "<td style='border-left:0;'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '') . "</td>";
                                            }
                                            $catNo++;
                                        }
                                        ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <hr />

            <div class="col-lg-12" style="border-top:1px solid #000; text-align: center;">
                <p><?= $company_profile[0]->company_name; ?>, Address: <?= $company_profile[0]->address; ?>,
                    <?= $company_profile[0]->city; ?>,
                    <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                    <?= $company_profile[0]->country_code; ?>,
                    Phone: <?= $company_profile[0]->phone; ?></p>
            </div>
        </div>
    </body>
</html>        

