<?php $currency = $company_profile[0]->currency; ?>
<style>
    .modal-fullscreen {
        padding: 0 !important;
    }
    
    .modal-fullscreen .modal-dialog {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        max-width: 100% !important;
    }
    
    .modal-fullscreen .modal-content {
        height: auto;
        min-height: 100%;
        border: 0 none;
        border-radius: 0;
    }
    /*css for extra button*/
    .extra_discount_btn{
        background-color: #145388;
        color: #fff;
        cursor: pointer;
        border: none;
    }
    .extra_discount_btn:disabled {
        opacity: 0.5;
        cursor: no-drop;
    }
    .misc_extra_discount{
        width: 54%;
        float: right;
    }
    /*css for extra button*/
    /*remove arrow from number input*/
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
    /*remove arrow from number input*/

    /*table.table_print tr th,
    table.table_print tr td {
        border-color: #000 !important;
        border: 2px solid black !important;
        border-style: solid !important;
    }*/
    .pac-container {
        z-index: 10000 !important;
    }
    .leftalign {
        text-align: right !important;
        padding-left: 20% !important;
    }

    .selected-prouct {
        float: left;
        height: 20px;
        width: 20px;
    }

    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width: 1200px;
        }
    }

    .modal .modal-header {
        padding: 1rem !important;
    }

    .modal .modal-body,
    .modal .modal-footer {
        padding: 0.5rem !important;
    }

    /*    #Modal-Place-Order input.form-control{
                    padding: 5px !important;
                }*/

    .tab-content {
        padding: 5px !important;
    }

    .tab-content main {
        margin: 0px !important;
    }

    .modal .footer_section {
        overflow-y: scroll;
        height: 500px;
    }

    /* width */
    .modal .footer_section::-webkit-scrollbar {
        width: 10px;
    }

    /* Track */
    .modal .footer_section::-webkit-scrollbar-track {
        background: #f1f1f1;
        border-radius: 5px;
    }

    /* Handle */
    .modal .footer_section::-webkit-scrollbar-thumb {
        background: #888;
        border-radius: 5px;
    }

    /* Handle on hover */
    .modal .footer_section::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    .col-lg-12.mb-4 .card {
        margin-bottom: 0 !important;
    }

    .col-lg-12.mb-4 .card-body {
        margin-bottom: 0 !important;
    }

    li.active a {
        background: #e9ecef;
    }

    /* width */
    .tab-content::-webkit-scrollbar {
        width: 10px;
    }

    /* Track */
    .tab-content::-webkit-scrollbar-track {
        background: #f1f1f1;
        border-radius: 5px;
    }

    /* Handle */
    .tab-content::-webkit-scrollbar-thumb {
        background: #888;
        border-radius: 5px;
    }

    /* Handle on hover */
    .tab-content::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    .cursor {
        cursor: pointer;
    }


    .modal-header .btnGrp {
        position: absolute;
        top: 8px;
        right: 10px;
    }


    .min {
        width: 250px;
        height: 35px;
        overflow: hidden !important;
        padding: 0px !important;
        margin: 0px;

        float: left;
        position: static !important;
    }

    .min .modal-dialog,
    .min .modal-content {
        height: 100%;
        width: 100%;
        margin: 0px !important;
        padding: 0px !important;
    }

    .min .modal-header {
        height: 100%;
        width: 100%;
        margin: 0px !important;
        padding: 3px 5px !important;
    }

    .display-none {
        display: none;
    }

    button .fa {
        font-size: 16px;
        margin-left: 10px;
    }

    .min .fa {
        font-size: 14px;
    }

    .min .menuTab {
        display: none;
    }

    button:focus {
        outline: none;
    }

    .minmaxCon {
        height: 35px;
        bottom: 1px;
        left: 1px;
        position: fixed;
        right: auto;
        z-index: 9999;
        box-shadow: 0 1px 13px rgba(0, 0, 0, .04), 7px -4px 6px rgba(0, 0, 0, .04);
    }

    .tab-content {
        max-height: 900px !important;
    }

    .nav-tabs li {
        border: 1px solid lightgrey !important;
    }

    .nav-tabs li span {
        background-color: lightgrey !important;
        padding: 10px;
        height: 100%;
        margin: 0px;
        margin-left: -5px;
    }

    .hidden {
        display: none;
    }

    #Modal-Place-Order .nav-tabs li a{
        pointer-events: none;
    }

    #inputGroup-sizing-default {
        border-left-width:0;left:-2px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        margin-bottom: 0px;
        background-color: #e9ecef;
        border: 1px solid #ced4da;
        padding:10px;
        cursor: pointer;
    }

    .save-discount-text {
        width: 70px !important;
        float: right;
        text-align: right;
    }    

    i.icon-popover{
        padding-left: 5px;
        cursor: pointer;
        color: red;
    }

    #inputGroup-sizing-default {
        border-left-width:0;left:-2px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        margin-bottom: 0px;
        background-color: #e9ecef;
        border: 1px solid #ced4da;
        padding:10px;
        cursor: pointer;
    }

    .save-discount-text {
        width: 70px !important;
        float: right;
        text-align: right;
    }
</style>

<div class="container1">
    <main>
        <div class="container-fluid">
            <input type="hidden" name="curQuotId" id="curQuotId" value="<?php echo $quote_id; ?>" />
            <div class="row">
                <div class="col-12">
                    <div class="mb-3">
                        <h1>Quotation</h1>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Quotation View</li>
                            </ol>
                        </nav>
                        <button type="button" class="btn btn-success pull-right"
                                onclick="printContent('printableArea')">Print</button>
                        <button type="button" class="btn btn-default pull-right"
                                onclick="mailContent('printableArea')">Email</button>
                    </div>
                    <div class="separator mb-5"> </div>

                </div>
            </div>

            <div class="row" id="printableArea" style="background-color: #FFF;">

                <div class="col-lg-5 col-md-5 col-sm-5">
                    <img
                        src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>">
                </div>

                <div class="col-lg-7 col-md-7 col-sm-7 mb-4 title-center">
                    <h2 style="margin-top: 100px;">Quotation Comparison</h2>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 left-content custom-div-section">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h2><?= $company_profile[0]->company_name; ?>,</h2>
                            <h4><?= $company_profile[0]->address; ?>, <?= $company_profile[0]->city; ?>,</h4>
                            <h4><?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                                <?= $company_profile[0]->country_code; ?></h4>
                            <h4><?= $company_profile[0]->phone; ?></h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 offset-lg-1 offset-md-1 right-content custom-div-section">
                    <div class="card mb-4">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td style="width: 40%;"><strong>Date</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= date_format(date_create($quotation->qt_order_date), 'Y-M-d'); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;<br /></td>
                                </tr>
                                <tr>
                                    <td><strong>Customer Name</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->first_name . " " . $quotation->last_name; ?></td>
                                </tr>
                                <?php if ($quotation->company != "") { ?>
                                    <tr>
                                        <td><strong>Company Name</strong></td>
                                        <td>&nbsp;:&nbsp;&nbsp;</td>
                                        <td><?= $quotation->company; ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><strong>Address</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->address; ?>, <?= $quotation->city; ?>,
                                        <?= $quotation->state; ?>,
                                        <?= $quotation->zip_code; ?>, <?= $quotation->country_code; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Phone Number</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->phone; ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Email ID</strong></td>
                                    <td>&nbsp;:&nbsp;&nbsp;</td>
                                    <td><?= $quotation->email; ?></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

                <div style="clear: both; margin-bottom: 10px;"></div>

                <?php $invoiceDetailsArray = []; ?>

                <div class="col-lg-12">

                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4">Details</h5>
                            <div class="separator mb-5"></div>
                            <div class="table-responsive">
                                <style>
                                    table.table_print tr th {
                                        padding-left:5px !important;
                                        padding-right:5px !important;
                                    }
                                    /*table.table_print tr th,
                                    table.table_print tr td {
                                        border: 1px solid #000 !important;
                                    }*/
                                </style>
                                <table class=" table table-bordered table-hover table_print" cellspacing="0" cellpadding="0" width="100%" border="1">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Room</th>
                                            <th colspan="2">Window size</th>
                                            <th>Comment</th>
                                            <?php
                                            $catNo = 1;
                                            foreach ($categories as $key => $val) {
                                                if ($val == "Misc") {
                                                    echo "<th colspan='2'>" . $val . "</th>";
                                                } else {
                                                    echo "<th colspan='3'>" . $val . "</th>";
                                                }

                                                $invoiceDetailsArray[$val . "-" . $catNo]['subtotal'] = 0;
                                                $invoiceDetailsArray[$val . "-" . $catNo]['discount'] = 0;
                                                $invoiceDetailsArray[$val . "-" . $catNo]['upcharge'] = 0;
                                                $invoiceDetailsArray[$val . "-" . $catNo]['salestax'] = 0;
                                                $invoiceDetailsArray[$val . "-" . $catNo]['grandtotal'] = 0;
                                                $invoiceDetailsArray[$val . "-" . $catNo]['extra_discount'] = 0;
                                                // store extra discount details in array
                                                $invoiceDetailsArray[$val . "-" . $catNo]['extra_discount_details'] = array();
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Room</th>
                                            <th>Width</th>
                                            <th>Height</th>
                                            <th></th>
                                            <?php foreach ($categories as $key => $val) { ?>
                                                <th>Product </th>
                                                <?php if ($val != 'Misc') { ?><th> Pattern </th> <?php } ?>
                                                <th>Price</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            foreach ($roomWiseArray as $key => $room) { ?>
                                            <tr>
                                                <td rowspan="<?php echo sizeof($room); ?>"><?php echo $key + 1; ?></td>
                                                <td rowspan="<?php echo sizeof($room); ?>">
                                                    <?php echo $room[0]->room_name; ?>
                                                </td>
                                                <?php
                                                for ($i = 0; $i < sizeof($room); $i++) {
                                                    if ($i == 0) {
                                                        ?>
                                                        <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?>
                                                        </td>
                                                        <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $room[$i]->win_comment; ?>
                                                        </td>
                                                        <?php
                                                        $roomNo = $room[$i]->qd_id;
                                                        $winCategories = json_decode($room[$i]->win_categories);
                                                        $newWinCategories = [];
                                                        $isPrinted = false;
                                                        $catNo = 1;
                                                        foreach ($categories as $cat => $catVal) {
                                                            if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                                for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                                    if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {

                                                                        // Calculate & Display the upcharge price tooltip : START
                                                                        $upcharge_details = json_decode($winCategories[$j]->win_upcharges_array);
                                                                        $final_up_info = '';
                                                                        if(count($upcharge_details) > 0){
                                                                            $upcharge_info_html = '<div class=\'upcharge_tooltip\'><table>';
                                                                            foreach($upcharge_details as $upcharge_info){

                                                                                $res_up_name = $this->db->select('*')->where('id',$upcharge_info->id)->get('c_level_upcharges')->row_array();

                                                                                if($upcharge_info->value != ''){

                                                                                    $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);

                                                                                    $up_price = 0;
                                                                                    if($upcharge_info->type == 'Percantage'){
                                                                                        // Percentage
                                                                                        $up_per = $upcharge_info->value;
                                                                                        $up_price = sprintf("%.2f",(($discounted_price * $up_per) / 100));

                                                                                    }else{
                                                                                        // Fix Amount
                                                                                        $up_price = $upcharge_info->value;
                                                                                    }

                                                                                    $upcharge_info_html .= '<tr>';
                                                                                    $upcharge_info_html .= '<td><b>'.$res_up_name['name'].'</b></td>';
                                                                                    $upcharge_info_html .= '<td class=\'up_price_td\'> : '.$currency.$up_price.'</td>';
                                                                                    $upcharge_info_html .= '</tr>';
                                                                                }    

                                                                            }
                                                                            $upcharge_info_html .= '</table></div>';
                                                                            $final_up_info = '<i class="simple-icon-info popoverData icon-popover" href="#" data-content="'.$upcharge_info_html.'" rel="popover" data-placement="top" data-html="true" data-original-title="Upcharges" data-trigger="hover"></i>';
                                                                        }    
                                                                        // Calculate & Display the upcharge price tooltip : END
                                                                        //add product details in extra discount array by DM : start
                                                                        $extra_amount_details = array(
                                                                            'qd_id'      => $roomNo,
                                                                            'product_id' => $winCategories[$j]->win_product,
                                                                            'pattern_id' => $winCategories[$j]->win_pattern,
                                                                            'product_extra_discount' => isset($winCategories[$j]->extra_discount) ? $winCategories[$j]->extra_discount : 0,
                                                                        );
                                                                        $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount_details'][] = $extra_amount_details;
                                                                        //add product details in extra discount array by DM : end

                                                                        echo "<td><input class='selected-prouct' type='checkbox' val='this value' name='" . $roomNo . "_" . $winCategories[$j]->win_category_name . "_" . $catNo . "'/>" . $winCategories[$j]->win_product_name . "<span class='edit_btn_link'>".$final_up_info."<br/><br/><span style='color:#1E02D2; cursor:pointer;' class='edit-link' onclick='editQuatation(this)'>EDIT</span></span></td>";
                                                                        if ($winCategories[$j]->win_category_name != "Misc") {
                                                                            echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                        }
                                                                            $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);
                                                                        echo "<td>" . $currency.sprintf("%.2f",(round($discounted_price,2))) . "</td>";
                                                                        $isPrinted = true;

                                                                        if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                            $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                        } else {
                                                                            $fSubTotal = 0;
                                                                        }

                                                                        if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                            $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                        } else {
                                                                            $fUpcharges = 0;
                                                                        }

                                                                        if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                            $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                        } else {
                                                                            $fDiscount = 0;
                                                                        }
                                                                        /************ Start: Add Extra Discount ****************/
                                                                        /*$fExtraDiscount = 0;
                                                                        if (isset($winCategories[$j]->win_extra_discount_amt) && ($winCategories[$j]->win_extra_discount_amt != 0 || $winCategories[$j]->win_extra_discount_amt != "")) {
                                                                            $fExtraDiscount = floatval($winCategories[$j]->win_extra_discount_amt);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount'] += $fExtraDiscount;
                                                                        }*/
                                                                        /************ End: Add Extra Discount ****************/

                                                                        $fTotal = $fSubTotal + $fUpcharges;

                                                                        if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                            $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                        } else {
                                                                            $fsalestax = 0;
                                                                        }

                                                                        if ($fSubTotal != 0) {
                                                                            $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges; //- $fExtraDiscount;
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                        }
                                                                    } else {
                                                                        array_push($newWinCategories, $winCategories[$j]);
                                                                    }
                                                                }
                                                                if ($isPrinted != true) {
                                                                    if ($catVal == 'Misc') {
                                                                        echo "<td colspan='2'>&nbsp;</td>";
                                                                    } else {
                                                                        echo "<td colspan='3'>&nbsp;</td>";
                                                                    }
                                                                }
                                                                $winCategories = $newWinCategories;
                                                                $newWinCategories = [];
                                                                $isPrinted = false;
                                                            } else {
                                                                if ($catVal == "Misc") {
                                                                    echo "<td colspan='2'>empty</td>";
                                                                } else {
                                                                    echo "<td colspan='3'>empty</td>";
                                                                }
                                                            }
                                                            $catNo++;
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                            if (sizeof($room) > 0) {
                                                ?><?php
                                                for ($i = 0; $i < sizeof($room); $i++) {
                                                    if ($i != 0) {
                                                        ?><tr class="here">
                                                            <td><?php echo $room[$i]->win_width . "<sup style='color:red !important;'>" . $room[$i]->win_wfraction . "</sup>"; ?>
                                                            </td>
                                                            <td><?php echo $room[$i]->win_height . "<sup style='color:red !important;'>" . $room[$i]->win_htfraction . "</sup>" ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $room[$i]->win_comment; ?>
                                                            </td>
                                                            <?php
                                                            $roomNo = $room[$i]->qd_id;
                                                            $winCategories = json_decode($room[$i]->win_categories);
                                                            $newWinCategories = [];
                                                            $isPrinted = false;
                                                            $catNo = 1;
                                                            foreach ($categories as $cat => $catVal) {
                                                                if ($room[$i]->win_categories != null || $room[$i]->win_categories != "") {
                                                                    for ($j = 0; $j < sizeof($winCategories); $j++) {
                                                                        if ($winCategories[$j]->win_category_name == $catVal && $isPrinted == false) {
                                                                            
                                                                            // Calculate & Display the upcharge price tooltip : START
                                                                            $upcharge_details = json_decode($winCategories[$j]->win_upcharges_array);
                                                                            $final_up_info = '';
                                                                            if(count($upcharge_details) > 0){
                                                                                $upcharge_info_html = '<div class=\'upcharge_tooltip\'><table>';
                                                                                foreach($upcharge_details as $key => $upcharge_info){

                                                                                    $res_up_name = $this->db->select('*')->where('id',$upcharge_info->id)->get('c_level_upcharges')->row_array();

                                                                                    if($upcharge_info->value != ''){

                                                                                        $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);

                                                                                        $up_price = 0;
                                                                                        if($upcharge_info->type == 'Percantage'){
                                                                                            // Percentage
                                                                                            $up_per = $upcharge_info->value;
                                                                                            $up_price = sprintf("%.2f",(($discounted_price * $up_per) / 100));

                                                                                        }else{
                                                                                            // Fix Amount
                                                                                            $up_price = $upcharge_info->value;
                                                                                        }

                                                                                        $upcharge_info_html .= '<tr>';
                                                                                        $upcharge_info_html .= '<td><b>'.$res_up_name['name'].'</b></td>';
                                                                                        $upcharge_info_html .= '<td class=\'up_price_td\'> : '.$currency.$up_price.'</td>';
                                                                                        $upcharge_info_html .= '</tr>';
                                                                                    }    

                                                                                }
                                                                                $upcharge_info_html .= '</table></div>';
                                                                                $final_up_info = '<i class="simple-icon-info popoverData icon-popover" href="#" data-content="'.$upcharge_info_html.'" rel="popover" data-placement="top" data-html="true" data-original-title="Upcharges" data-trigger="hover"></i>';
                                                                            }    
                                                                            // Calculate & Display the upcharge price tooltip : END
                                                                            //add product details in extra discount array by DM : start
                                                                            $extra_amount_details = array(
                                                                                'qd_id'      => $roomNo,
                                                                                'product_id' => $winCategories[$j]->win_product,
                                                                                'pattern_id' => $winCategories[$j]->win_pattern,
                                                                                'product_extra_discount' => isset($winCategories[$j]->extra_discount) ? $winCategories[$j]->extra_discount : 0,
                                                                            );
                                                                            $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount_details'][] = $extra_amount_details;
                                                                            //add product details in extra discount array by DM : end
                                                                            echo "<td><input class='selected-prouct' type='checkbox' val='this value' name='" . $roomNo . "_" . $winCategories[$j]->win_category_name . "_" . $catNo . "'/>" . $winCategories[$j]->win_product_name . "<span class='edit_btn_link'>".$final_up_info."<br/><br/><span style='color:#1E02D2; cursor:pointer;' class='edit-link' onclick='editQuatation(this)'>EDIT</span></span></td></td>";
                                                                            if ($winCategories[$j]->win_category_name != "Misc") {
                                                                                echo "<td>" . $winCategories[$j]->win_pattern_name . "</td>";
                                                                            }
                                                                            $discounted_price = $winCategories[$j]->win_price - floatval($winCategories[$j]->win_price * $winCategories[$j]->win_discount_amt / 100);
                                                                            echo "<td>" . $currency.sprintf("%.2f",(round($discounted_price,2))) . "</td>";
                                                                            $isPrinted = true;

                                                                            if ($winCategories[$j]->win_price != 0 || $winCategories[$j]->win_price != "") {
                                                                                $fSubTotal = floatval($winCategories[$j]->win_price) - (floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100));
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'] += $fSubTotal;
                                                                            } else {
                                                                                $fSubTotal = 0;
                                                                            }

                                                                            if ($winCategories[$j]->win_upcharges != 0 || $winCategories[$j]->win_upcharges != "") {
                                                                                $fUpcharges = floatval($winCategories[$j]->win_upcharges);
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'] += $fUpcharges;
                                                                            } else {
                                                                                $fUpcharges = 0;
                                                                            }

                                                                            if ($winCategories[$j]->win_discount_amt != 0 || $winCategories[$j]->win_discount_amt != "") {
                                                                                $fDiscount = floatval($winCategories[$j]->win_price) * floatval($winCategories[$j]->win_discount_amt / 100);
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['discount'] += $fDiscount;
                                                                            } else {
                                                                                $fDiscount = 0;
                                                                            }
                                                                            /************ Start: Add Extra Discount ****************/
                                                                            /*$fExtraDiscount = 0;
                                                                            if (isset($winCategories[$j]->win_extra_discount_amt) && ($winCategories[$j]->win_extra_discount_amt != 0 || $winCategories[$j]->win_extra_discount_amt != "")) {
                                                                                $fExtraDiscount = floatval($winCategories[$j]->win_extra_discount_amt);
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount'] += $fExtraDiscount;
                                                                            }*/
                                                                            /************ Start: Add Extra Discount ****************/

                                                                            $fTotal = $fSubTotal + $fUpcharges;

                                                                            if ($quotation->qt_tax != 0 || $quotation->qt_tax != "") {
                                                                                $fsalestax = $fTotal * (floatval($quotation->qt_tax) / 100);
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'] += $fsalestax;
                                                                            } else {
                                                                                $fsalestax = 0;
                                                                            }

                                                                            if ($fSubTotal != 0) {
                                                                                $fGrandtotal = $fsalestax + $fSubTotal + $fUpcharges;// - $fExtraDiscount;
                                                                                $invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'] += $fGrandtotal;
                                                                            }
                                                                        } else {
                                                                            array_push($newWinCategories, $winCategories[$j]);
                                                                        }
                                                                    }
                                                                    if ($isPrinted != true) {
                                                                        if ($catVal == 'Misc') {
                                                                            echo "<td colspan='2'>&nbsp;</td>";
                                                                        } else {
                                                                            echo "<td colspan='3'>&nbsp;</td>";
                                                                        }
                                                                    }
                                                                    $winCategories = $newWinCategories;
                                                                    $newWinCategories = [];
                                                                    $isPrinted = false;
                                                                } else {
                                                                    if ($catVal == "Misc") {
                                                                        echo "<td colspan='2'>empty</td>";
                                                                    } else {
                                                                        echo "<td colspan='3'>empty</td>";
                                                                    }
                                                                }
                                                                $catNo++;
                                                            }
                                                        }
                                                        ?>
                                                    </tr><?php
                                                }
                                                ?>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="5">Sub Total (with discount)
                                                (<?= @$company_profile[0]->currency; ?>)</td>
                                            <?php
                                            $catNo = 1;
                                            foreach ($categories as $cat => $catVal) {
                                                if ($catVal == "Misc") {
                                                    echo "<td style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_sub_total_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                                } else {
                                                    echo "<td colspan='2' style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_sub_total_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['subtotal'], 2, '.', '') . "</td>";
                                                }
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Total Discount(<?= @$company_profile[0]->currency; ?>)</td>
                                            <?php
                                            $catNo = 1;
                                            foreach ($categories as $cat => $catVal) {
                                                if ($catVal == "Misc") {
                                                    echo "<td style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_total_discount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                                } else {
                                                    echo "<td colspan='2' style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_total_discount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['discount'], 2, '.', '') . "</td>";
                                                }
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Upcharge</td>
                                            <?php
                                            $catNo = 1;
                                            foreach ($categories as $cat => $catVal) {
                                                if ($catVal == "Misc") {
                                                    echo "<td style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_upcharge_amount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                                } else {
                                                    echo "<td colspan='2' style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_upcharge_amount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['upcharge'], 2, '.', '') . "</td>";
                                                }
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $total = $quotation->qt_subtotal - $quotation->total_discount + $quotation->total_upcharge;
                                        ?>
                                        <tr>
                                            <td colspan="5">Sales Tax
                                                (<?= number_format($quotation->qt_tax, 2, '.', ''); ?>%)</td>
                                            <?php
                                            $catNo = 1;
                                            foreach ($categories as $cat => $catVal) {
                                                if ($catVal == "Misc") {
                                                    echo "<td style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_sales_tax_amount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                                } else {
                                                    echo "<td colspan='2' style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_sales_tax_amount_$cat'>" . number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['salestax'], 2, '.', '') . "</td>";
                                                }
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                        <!--tr>
                                            <td colspan="5">Extra Discount</td>
                                            <?php
                                            /*$catNo = 1;
                                            $index_counter = 0;

                                            foreach ($categories as $cat => $catVal) {
                                                if ($catVal == "Misc") {*/
                                            ?>
                                                    <td colspan="2" style='border-left:0;'>
                                                        <form class="form-inline" style="float: right;margin-right: 7px;">
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <input id="quotation_extra_discount_value_<?php //echo $index_counter; ?>" type="text" class="form-control save-discount-text" onkeypress="return filterToDecimal(event,this)" autocomplete="off" value="<?php //echo number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount'], 2); ?>">
                                                                    <div class="input-group-append" onclick="calculateAndSaveExtraDiscount(<?php //echo $index_counter; ?>)">
                                                                        <span class="input-group-text quotation_extra_discount_btn_<?php //echo $index_counter; ?>" id="inputGroup-sizing-default" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Click To Save" data-original-title="" title="">💾
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </td>
                                            <?php
                                                //} else {
                                            ?>
                                                    <td colspan="3" style='border-left:0;'>
                                                    <form class="form-inline" style="float: right;margin-right: 7px;">
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <input id="quotation_extra_discount_value_<?php //echo $index_counter; ?>" type="text" class="form-control save-discount-text" onkeypress="return filterToDecimal(event,this)" autocomplete="off" value="<?php //echo number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount'], 2); ?>">
                                                                <div class="input-group-append" onclick="calculateAndSaveExtraDiscount(<?php //echo $index_counter; ?>)">
                                                                    <span class="input-group-text quotation_extra_discount_btn_<?php //echo $index_counter; ?>" id="inputGroup-sizing-default" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Click To Save" data-original-title="" title="">💾
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </td>
                                            <?php
                                               /* }
                                                $index_counter++;
                                                $catNo++;
                                            }*/
                                            ?>
                                        </tr-->
                                        <!-- Start Code for extra discount by DM 31-01-2020 -->
                                            <tr>
                                                <td colspan="5">Extra Discount</td>
                                                <?php
                                                $catNo = 1;
                                                $totalIndexCounter = 0;
                                                $products_discount = array();
                                                foreach ($categories as $cat => $catVal) {
                                                    $total_discount_product = array();
                                                    $quotation_details_ids      = $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount_details'];
                                                    $product_extra_discount_arr = $invoiceDetailsArray[$catVal . "-" . $catNo]['extra_discount_details'];
                                                    if ($product_extra_discount_arr) {
                                                        foreach ($product_extra_discount_arr as $ext_key => $ext_value) {
                                                            array_push($total_discount_product, $ext_value['product_extra_discount']);
                                                        }
                                                    }
                                                    $total_product_discount  = !empty($total_discount_product) ? array_sum($total_discount_product) : 0;
                                                    $products_discount[$cat] = $total_product_discount;
                                                    $encoded_quotation_details  = json_encode($quotation_details_ids);
                                                    $disabled_btn = empty($total_product_discount) ? 'disabled' : '';
                                                    $qt_id = $quotation->qt_id;
                                                    if ($catVal == "Misc") {
                                                        echo "<td style='border-right:0;'></td>";
                                                        echo "<td style='border-left:0;'>
                                                            <div class='input-group misc_extra_discount'>
                                                              <input type='number' class='form-control extra_discount allownumericwithdecimal' data-position='$cat' placeholder='Extra Discount' name='extra_discount_$cat' id='extra_discount_$cat' value='$total_product_discount'>
                                                              <div class='input-group-append'>
                                                                <button $disabled_btn  class='extra_discount_btn' id='btn_extra_discount_$cat' onclick='UpdateExtraDiscount($encoded_quotation_details,$cat,$qt_id)'>Update</button>
                                                              </div>
                                                            </div>
                                                            <span id='extra_discount_error_$cat' class='text-danger'></span>
                                                        </td>";
                                                    } else {
                                                        echo "<td colspan='3'>
                                                            <div class='input-group'>
                                                              <input type='number' class='form-control extra_discount allownumericwithdecimal' data-position='$cat' placeholder='Extra Discount' name='extra_discount_$cat' id='extra_discount_$cat' value='$total_product_discount'>
                                                              <div class='input-group-append'>
                                                                <button $disabled_btn  class='extra_discount_btn' id='btn_extra_discount_$cat' onclick='UpdateExtraDiscount($encoded_quotation_details,$cat,$qt_id)'>Update</button>
                                                              </div>
                                                            </div>
                                                            <span id='extra_discount_error_$cat' class='text-danger'></span>
                                                        </td>";
                                                    }
                                                    $totalIndexCounter++;
                                                    $catNo++;
                                                }
                                                ?>
                                            </tr>
                                        <!-- End Code for extra discount by DM 31-01-2020 -->
                                        <tr>
                                            <td colspan="5">Grand Total (<?= @$company_profile[0]->currency; ?>)</td>
                                            <?php
                                            $catNo = 1;
                                            $totalIndexCounter = 0;
                                            foreach ($categories as $cat => $catVal) {
                                                $grand_total        = number_format($invoiceDetailsArray[$catVal . "-" . $catNo]['grandtotal'], 2, '.', '');
                                                $extra_discount     = $products_discount[$cat];
                                                $final_grand_total  = $grand_total - $extra_discount;
                                                if ($catVal == "Misc") {
                                                    echo "<td style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_grand_total_$totalIndexCounter'>" . number_format($final_grand_total, 2, '.', '') . "</td>";
                                                } else {
                                                    echo "<td colspan='2' style='border-right:0;'></td>";
                                                    echo "<td style='border-left:0;' id='quotation_table_grand_total_$totalIndexCounter'>" . number_format($final_grand_total, 2, '.', '') . "</td>";
                                                }
                                                $totalIndexCounter++;
                                                $catNo++;
                                            }
                                            ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success place-order" onclick="placeOrder();"
                            style="float: right;">Order Entry</button>
                    <div style="clear: both;"></div>
                </div>

                <hr />

                <div class="col-lg-12" style="border-top:1px solid #000; text-align: center;">
                    <p><?= $company_profile[0]->company_name; ?>, Address: <?= $company_profile[0]->address; ?>,
                        <?= $company_profile[0]->city; ?>,
                        <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                        <?= $company_profile[0]->country_code; ?>,
                        Phone: <?= $company_profile[0]->phone; ?></p>
                </div>
            </div>


        </div>
    </main>

    <!-- Modal -->
    <div id="Modal-Place-Order" class="modal fade modal-place-order modal-fullscreen" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Order Entry</h4>
                    <div>
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        <button type="button" class="close order_place_close" >&times;</button>
                        <button class="close modalMinimize"> <i class='iconsmind-Minimize'></i> </button>
                    </div>
                </div>
                <div class="modal-body" style="padding:0px !important;">

                    <div class="container-fluid" style="padding:5px;">
                        <div class="next-btn" style=" float: right;">
                            <button type="button" class="btn btn-md btn-success" onclick="getOrderFooter()">Order Details</button>
                        </div>
                        <div role="tabpanel" style="padding: 0px;">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <!-- dynamic data appear here -->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content"
                                 style="min-height: 210px; max-height: 210px; overflow-y: scroll; background-color: #F3F3F3;overflow-x:hidden;padding:0px!important; width:100%">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- <div class="modal-footer" style="padding-top:20px!important;">
                    <button type="button" class="btn btn-md btn-success" onclick="getOrderFooter()">Next</button> -->
                    <!--                <button type="button" class="btn btn-md btn-success" onclick="saveOrder()">Save & Proceed</button>
                                    <button type="button" class="btn btn-md btn-default" onclick="addMoreCats()">Add More Categories</button>
                                    <button type="button" class="btn btn-md btn-danger" onclick="cancelOrder()">Cancel Order</button>
                    -->
                <!-- </div> -->
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="Modal-Cart" class="modal fade modal-place-order modal-fullscreen" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <div>
                        <!-- <button type="button" class="close empty_cart" data-dismiss="modal">&times;</button> -->
                        <button type="button" class="close empty_cart">&times;</button>
                        <button class="close modalMinimize"> <i class='iconsmind-Minimize'></i> </button>
                    </div>
                </div>
                <div class="modal-body" style="padding:0px !important">
                </div>
                <div class="modal-footer" style="padding-top:20px!important;">
                    <button type="button" class="btn btn-md btn-success" onclick="placeOrder()">Order Entry</button>
                </div>
            </div>
        </div>
    </div>

    <div id="editOrderModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 500px; overflow-y: scroll;">
                    <div class="row form-area container">

                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" id="submitUpdateRequest" class="btn btn-default"
                            data-dismiss="modal">Submit</button>
                    <button type="button" id="closeUpdateRequest" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>

    <div class="minmaxCon"></div>

    <input type="hidden" id="quote_cutomer_id" value="<?php echo $quotation->customer_id; ?>" />


    <!-- Modal -->
    <div id="editQuatationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Quotation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        /*function calculateAndSaveExtraDiscount(elementPosition) {
            var quotation_extra_discount = $('#quotation_extra_discount_value_'+elementPosition).val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/quotation_controller/calculate_and_save_extra_discount') ?>",
                data: {
                    extra_discount: quotation_extra_discount,
                    array_position: elementPosition,
                    category_grand_total: $.trim($('#quotation_table_grand_total_'+elementPosition).text()),
                    quotation_id: "<?php echo $this->uri->segment(2); ?>"
                },
                success: function (res) {
                    var obj = jQuery.parseJSON( res );
                    if(obj.status == 'success') {
                        $('#quotation_table_grand_total_'+elementPosition).html(obj.grand_total);
                    }
                    Swal.fire(obj.message);
                },
                error: function () {
                    Swal.fire('error');
                }
            });
        }*/
        function printContent(el) {

            var css = '.custom-div-section .card-body h4{ line-height: normal !important;margin-bottom : 0px !important;} .mb-4{ margin-bottom:0px !important; } .title-center h2 { margin-top:20px !important; text-align: center; } .custom-div-section{ width: 50% !important;float: left; } .card { box-shadow : none }',
                head = document.head || document.getElementsByTagName('head')[0],
                style = document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);

            $('.place-order').remove();
            $('.selected-prouct').remove();
            var restorepage = $('body').html();
            $('.edit_btn_link').hide();
            var printcontent = $('#' + el).clone();
            $('body').empty().html(printcontent);
            window.print();
            $('body').html(restorepage);
            location.reload();
        }

        function mailContent(el) {
            $('.place-order').remove();
            $('.selected-prouct').remove();
            $('.edit_btn_link').remove();
            var printcontent = $('#' + el).html();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/send_invoice_email') ?>",
                data: {
                    custemail: "<?php echo $quotation->email; ?>",
                    printcontent: printcontent,
                },
                success: function (res) {
                    Swal.fire(res);
                    location.reload();
                },
                error: function () {
                    Swal.fire('error');
                }
            });
        }

        var catArray = JSON.parse('<?php echo json_encode($categories) ?>');
        var orderData = {};
        var fetchedProductsArray = [];


        function placeOrder() {
            //clear localstorage when new order select by dm
            // localStorage.clear();
            //clear localstorage when new order select by dm
            fetchedProductsArray = [];
            orderData = {};

            $('.modal').modal('hide');
            var newCatArray = [];
            for (var i = 0; i < catArray.length; i++) {
                var elementCount = 1;
                var isElementAdded = false;
                while (isElementAdded == false) {
                    if (newCatArray.indexOf(catArray[i] + "_" + elementCount) < 0) {
                        newCatArray.push(catArray[i] + "_" + elementCount);
                        isElementAdded = true;
                    } else {
                        elementCount++;
                    }
                }
            }

            var selectedProductsIndexArray = [];
            var selectedProducts = {};
            $(".selected-prouct").each(function (index) {
                if ($(this).is(":checked")) {
                    var productName = $(this).attr('Name');
                    var productArray = productName.split("_");
                    var catNoFromnewCatArray = parseInt(productArray[2]) - 1;
                    var newCatArrayA = newCatArray[catNoFromnewCatArray].split("_");
                    var newProductArray = {
                        roomno: productArray[0],
                        catname: productArray[1],
                        catpos: newCatArrayA[1],
                    }
                    if (fetchedProductsArray.indexOf(productName) < 0) {
                        selectedProducts[productName] = newProductArray;
                        orderData[productName] = {};
                        selectedProductsIndexArray.push(productName);
                    }
                }
            });

            if (jQuery.isEmptyObject(selectedProducts)) {
                if (fetchedProductsArray.length <= 0) {
                    Swal.fire("Please select products for place order!");
                } else {
                    showOrderModal();
                }
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('c_level/Quotation_controller/get_product_details') ?>",
                    data: selectedProducts,
                    success: function (data) {
                        data = JSON.parse(data);
                        for (var i = 0; i < selectedProductsIndexArray.length; i++) {
                            orderData[selectedProductsIndexArray[i]]['win_category_id'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_category'];
                            orderData[selectedProductsIndexArray[i]]['win_category_id'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_category'];
                            orderData[selectedProductsIndexArray[i]]['win_pattern'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_pattern'];

                            orderData[selectedProductsIndexArray[i]]['win_comment'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_comment'];
                            orderData[selectedProductsIndexArray[i]]['win_category_name'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_category_name'];

                            orderData[selectedProductsIndexArray[i]]['win_product_id'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_product'];
                            orderData[selectedProductsIndexArray[i]]['win_product_name'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_product_name'];
                            // orderData[selectedProductsIndexArray[i]]['win_pattern_id'] = data[selectedProductsIndexArray[i]]['selCatDet']['win_pattern'];
                            orderData[selectedProductsIndexArray[i]]['win_pattern_name'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_pattern_name'];
                            orderData[selectedProductsIndexArray[i]]['win_price_amt'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_price'];
                            orderData[selectedProductsIndexArray[i]]['win_upcharges_arr'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_upcharges_array'];
                            orderData[selectedProductsIndexArray[i]]['win_upcharges_amt'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_upcharges'];
                            orderData[selectedProductsIndexArray[i]]['win_discount_amt'] = data[
                                    selectedProductsIndexArray[i]]['selCatDet']['win_discount_amt'];
                            orderData[selectedProductsIndexArray[i]]['win_room_name'] = data[
                                    selectedProductsIndexArray[i]]['selWinDet']['room_name'];
                            orderData[selectedProductsIndexArray[i]]['win_height'] = data[
                                    selectedProductsIndexArray[i]]['selWinDet']['win_height'];
                            orderData[selectedProductsIndexArray[i]]['win_htfraction'] = data[
                                    selectedProductsIndexArray[i]]['selWinDet']['win_htfraction'];
                            orderData[selectedProductsIndexArray[i]]['win_width'] = data[
                                    selectedProductsIndexArray[
                                            i]]['selWinDet']['win_width'];
                            orderData[selectedProductsIndexArray[i]]['win_wfraction'] = data[
                                    selectedProductsIndexArray[i]]['selWinDet']['win_wfraction'];
                            fetchedProductsArray.push(selectedProductsIndexArray[i]);
                        }
                        showOrderModal();
                    },
                    error: function () {
                        Swal.fire('error');
                    }
                });
            }
        }

        // var OrderForm = null;
        // var OrderForm2 = null;

        var addMoreWindowBtn =
                '<li role="presentation" class="add_win_btn" onclick="getNewBlankOrder()"><a href="#"><span>+Add Window</span></a></li>';
        var customWindows = 1;

        function getNewBlankOrder() {
            $('#Modal-Place-Order .nav-tabs').find('li').removeClass('active');
            $('#Modal-Place-Order .tab-content .tab-pane').removeClass('active');
            $('#Modal-Place-Order .tab-content .tab-pane').removeClass('show');

            var navTabsHTML = '<li role="presentation" id="btn-customWindow' + customWindows +
                    '" class="tab active"><a href="#customWindow' + customWindows + '" aria-controls="customWindow' +
                    customWindows + '" class="show" role="tab" data-toggle="tab" onclick="generateOrderForm(\'customWindow' +
                    customWindows + '\', true)">Custome Window' + customWindows +
                    '</a><span class="close" onclick="delete_window(this, \'customWindow' + customWindows +
                    '\')">X</span>&nbsp;&nbsp;</li>';
            var tabsContentHTML = '<div role="tabpanel" class="tab-pane active show" id="customWindow' + customWindows +
                    '"></div>';
            $('#Modal-Place-Order .modal-body .tab-content').append(tabsContentHTML);
            $('#Modal-Place-Order .nav-tabs').append(navTabsHTML);
            $('#Modal-Place-Order .nav-tabs').find('.add_win_btn').remove();
            $('#Modal-Place-Order .nav-tabs').append(addMoreWindowBtn);
            $('#Modal-Place-Order .nav-tabs #btn-customWindow' + customWindows + ' a').trigger('click');
            customWindows++;
        }


        function showOrderModal() {
            var selectedProductID = null;
            var navTabsHTML = "";
            var tabsContentHTML = "";
            var win_counts = [];
            var tmp_rooms = [];
            for (var i = 0; i < fetchedProductsArray.length; i++) {

                if (win_counts[orderData[fetchedProductsArray[i]]['win_room_name']] == undefined) {
                    win_counts[orderData[fetchedProductsArray[i]]['win_room_name']] = 1;
                } else {
                    win_counts[orderData[fetchedProductsArray[i]]['win_room_name']]++;
                }

                tmp_rooms.push(orderData[fetchedProductsArray[i]]['win_room_name']);
                if (i === 0) {
                    navTabsHTML += '<li role="presentation" id="li_' + fetchedProductsArray[i] + '" class="tab active li_' + fetchedProductsArray[i] + '"><a id="btn-' +
                            fetchedProductsArray[i] + '" href="#' + fetchedProductsArray[i] +
                            '" aria-controls="' + fetchedProductsArray[i] +
                            '" class="active show" role="tab" data-toggle="tab" onclick="generateOrderForm(\'' +
                            fetchedProductsArray[i] + '\')">' + orderData[fetchedProductsArray[i]][
                            'win_room_name'
                    ] + ' ' + win_counts[orderData[fetchedProductsArray[i]]['win_room_name']] +
                            '</a></li>';
                    tabsContentHTML += '<div role="tabpanel" class="tab-pane active show" id="' +
                            fetchedProductsArray[i] + '"></div>';
                    selectedProductID = fetchedProductsArray[i];
                } else {
                    navTabsHTML += '<li role="presentation" id="li_' + fetchedProductsArray[i] + '" class="tab li_' + fetchedProductsArray[i] + '"><a href="#' + fetchedProductsArray[i] +
                            '" aria-controls="' + fetchedProductsArray[i] +
                            '" class="show " role="tab" data-toggle="tab" onclick="generateOrderForm(\'' +
                            fetchedProductsArray[i] + '\')"> ' + orderData[fetchedProductsArray[i]][
                            'win_room_name'
                    ] + ' ' + win_counts[orderData[fetchedProductsArray[i]]['win_room_name']] +
                            '</a></li>';
                    tabsContentHTML += '<div role="tabpanel" class="tab-pane" id="' + fetchedProductsArray[
                            i] + '"></div>';

                }
            }
            $('#Modal-Place-Order .modal-body .nav-tabs').empty();
            $('#Modal-Place-Order .modal-body .nav-tabs').html(navTabsHTML);
            $('#Modal-Place-Order .modal-body .tab-content').empty();
            $('#Modal-Place-Order .modal-body .tab-content').html(tabsContentHTML);
            $('#Modal-Place-Order .nav-tabs').append(addMoreWindowBtn);
            generateOrderForm(selectedProductID);
            //getOrderFooter();
        }


        function generateOrderForm(tarElement, isblank = false) {
            if (isblank == false) {
                var data = {
                    data: orderData[tarElement]
                }
            } else {
                var data = {};
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/get_new_order_form') ?>",
                data: data,
                success: function (response) {
                    $('#Modal-Place-Order .tab-content .tab-pane').each(function () {
                        $(this).empty();
                    });
                    $('#Modal-Place-Order .tab-content #' + tarElement).html(
                            '<div class="row" style="margin:10px; max-height:600px; overflow-y:scroll;">' +
                            response + '</div>');


                    $('#AddToCart').attr("id", 'AddToCartNew');
                    $('#Modal-Place-Order li').removeClass('active');
                    $('#Modal-Place-Order #btn-' + tarElement).addClass('active');
                    $('#Modal-Place-Order .li_' + tarElement).addClass('active');
                    // add class in form for get data by dm
                        var active_form_class = $("li.tab.active").attr("id");
                        $('form').addClass(active_form_class);
                    // add class in form for get data by dm
                    $('#Modal-Place-Order').modal('show');

                }
            });
        }

        $('body').on('submit', "#AddToCartNew", function (e) {
            e.preventDefault();
            var submit_url = "<?php echo base_url('c_level/order_controller/add_to_cart') ?>"
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: $(this).serialize(),
                success: function (res) {
                    if (res == 1) {
                        toastr.success('Success! - Add to cart Successfully');
                        // check if from quatation receipt then click on next : START
                        if($(".cls_next_add_to_cart").length){
                            $('body #next-slide').click();
                        }
                        // check if from quatation receipt then click on next : END

                    } else if (res == 2) {
                        toastr.success('Success! - cart updated Successfully');
                    }
                    //getOrderFooter();
                }
            });
        });
        function initAutocomplete() {
            var input = document.getElementById("mapLocation");
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                var psCode = (place.address_components[0] != '') ? place.address_components[0].long_name : '';
                var roadName = (place.address_components[1] != '') ? place.address_components[1].long_name: '';
                var city = (place.address_components[2] != '') ? place.address_components[2].long_name : '';
                var country = (place.address_components[3] != '') ? place.address_components[3].long_name : '';

                var address = psCode + ',' + roadName + ',' + city + ',' + country;
                address = address.replace(/,\s*$/, "").replace(",,", ",").replace(/^,/, '');
                $("#mapLocation").val(address);
                getSalesTax(city);
            });
        }

        function getOrderFooter() {
            var active_form_class  = $("li.tab.active").attr("id");
            var previous_form_data = $('.'+active_form_class).serializeArray();
            localStorage.setItem(active_form_class, JSON.stringify(previous_form_data));
            previous_slide_data = JSON.parse(localStorage.getItem(active_form_class));
            $('.modal').modal('hide');
            var quote_cutomer_id = $('#quote_cutomer_id').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/get_new_order_form_footer') ?>",
                data: {
                    order_form: 1,
                    quote_cutomer_id: quote_cutomer_id
                },
                success: function (data) {
                    $('#Modal-Cart .modal-body').html('');
                    $('#Modal-Cart .modal-body').html(data);
                    initAutocomplete();
                    $('#Modal-Cart #footer_form').show();
                    $('#Modal-Cart #customer_id').val(quote_cutomer_id);
                    $('#Modal-Cart #customer_id').change();

                    $('#Modal-Cart .btn-warning').remove();

                    $('#save_order_form').submit(function (event) {
                        // var formData = $(this).serialize();
                        // save_order(formData);
                        event.preventDefault();
                        if($("#save_order_form input.product_id").length == 0){
                            // IF cart is empty then not submit the form
                            popit = 'true';
                            swal("Your cart is empty!");
                        }else {
                            swal({
                                title: "Are you sure ??",
                                text: "Go to Order Info Page",
                                icon: "success",
                                buttons: true,
                                dangerMode: true,
                            }).then((is_success) => {
                                if (is_success) {
                                    $('#save_order_form').unbind('submit').submit();
                                    $('#save_order_form').submit();
                                } else {
                                    swal("Your data is safe!");
                                    popit = 'true';
                                }
                            });
                        }
                    });
                    $.ajax({
                        url: "<?= base_url() ?>c_level/order_controller/order_id_generate/" +
                                quote_cutomer_id,
                        type: 'get',
                        success: function (r) {
                            $("#orderid").val(r);
                        }
                    });
                    //$('#Modal-Place-Order').modal('show');
                },
                error: function () {
                    Swal.fire('error');
                }
            });

            $('#Modal-Cart').modal('show');
        }

        function load_add_order_form(rowId) {
            $('.modal').modal('hide');
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/Order_controller/get_order_form",
                data: {
                    row_id: rowId,
                    action: 1
                },
                success: function (result) {

                    console.log($('#Modal-Place-Order .tab-content .tab-pane'));

                    $('#Modal-Place-Order .tab-content .tab-pane').each(function () {
                        $(this).empty();
                    });
                    $('#editOrderModal .modal-body .form-area').html('');
                    $('#editOrderModal .modal-body .form-area').html(result);
                    $('.btns-box #cartbtn').remove();
                    $('.btns-box #clrbtn').remove();
                    $("#AddToCart").attr('action',
                            '<?php echo base_url(); ?>c_level/Order_controller/add_to_cart');
                    $('#AddToCart').attr('id', 'UpdateOrderItem');
                    
                    if($('.sticky_container .sticky_item').length > 0){
                        $('.sticky_container .sticky_item').theiaStickySidebar({
                            additionalMarginTop: 110
                        });
                    }

                    $('#editOrderModal').modal('show');
                }
            });
        }

        $('#submitUpdateRequest').click(function () {
            $("#UpdateOrderItem").submit();
        });
        $('#closeUpdateRequest').click(function () {
            getOrderFooter();
        });


        // submit form and add data
        $('body').on('submit', "#UpdateOrderItem", function (e) {
            e.preventDefault();
            var submit_url = "<?php echo base_url(); ?>c_level/order_controller/add_to_cart";
            $.ajax({
                type: 'POST',
                url: submit_url,
                data: $(this).serialize(),
                success: function (res) {
                    if (res == 1) {
                        toastr.success('Success! - Add to cart Successfully');
                    } else if (res == 2) {
                        toastr.success('Success! - cart updated Successfully');
                    }
                    getOrderFooter();
                }
            });
        });

        $('body').on('click', '#delete_cart_item', function (e) {
            $(document).ajaxStop(function () {
                $('#Modal-Cart .btn-warning').remove();
            });
        });

        function delete_window(triggerElement, targetElement) {
            $(triggerElement).parent().remove();
            $('#Modal-Place-Order .tab-content #' + targetElement).remove();

            // Go to the last tab.
            $("#Modal-Place-Order ul.nav-tabs li:nth-last-child(2)").children('a').click();
        }


        $(document).ready(function () {
            $("[data-toggle=popover]").popover();
            $('.popoverData').popover();
            $("body").on('click', '#shipaddress', function () {
                $(".ship_addr").slideToggle();
            });
            var $content, $modal, $apnData, $modalCon;

            $content = $(".min");


            //To fire modal
            $(".mdlFire").click(function (e) {

                e.preventDefault();

                var $id = $(this).attr("data-target");

                $($id).modal({
                    backdrop: false,
                    keyboard: false
                });

            });


            $(".modalMinimize").on("click", function () {

                $modalCon = $(this).closest(".modal-place-order").attr("id");

                $apnData = $(this).closest(".modal-place-order");

                $modal = "#" + $modalCon;

                $(".modal-backdrop").toggleClass("display-none");

                $($modal).toggleClass("min");

                if ($($modal).hasClass("min")) {

                    $(".minmaxCon").append($apnData);
                    $(this).find("i").toggleClass('iconsmind-Minimize').toggleClass('iconsmind-Maximize');



                } else {
                    $(".container1").append($apnData);

                    $(this).find("i").toggleClass('iconsmind-Maximize').toggleClass('iconsmind-Minimize');
                }
                ;

                if ($("#app-container").hasClass("modal-open")) {
                    $("#app-container").removeClass('modal-open');
                } else {
                    $("#app-container").addClass('modal-open');
                }

            });

            $("body").on('click', '.order_place_close', function () {
                localStorage.clear();
                // $(this).closest(".modal-place-order").removeClass("min");
                // $(".container1").removeClass($apnData);

                // $(this).next('.modalMinimize').find("i").removeClass('iconsmind-Maximize').addClass('iconsmind-Minimize');


                // $('#Modal-Place-Order .nav-tabs').html('');
                // $('#Modal-Place-Order .tab-content').html('');
                // $('#Modal-Place-Order').modal('hide');
                // window.location.reload();

                swal({
                    title: "Are you sure ??",
                    text: "Your cart item will be lost!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#Modal-Place-Order').modal('hide');
                        window.location.reload();  
                    } else {
                        swal("Your data is safe!");
                    }
                });
      

            });

            $("body").on('click','.empty_cart', function (){
                swal({
                    title: "Are you sure ??",
                    text: "Your cart item will be lost!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#Modal-Cart.modal').modal('hide');
                        window.location.reload();  
                    } else {
                        swal("Your data is safe!");
                    }
                });
            });

        });

        // For reload event confirmation when place order and cart modal open : START
        var popit = 'true';
        window.onbeforeunload = function() {
            if($("#Modal-Place-Order.modal.show").length > 0 || $("#Modal-Cart.modal.show").length > 0){
                if (popit == 'true') {
                    return "Are you sure you want to navigate away?";
                }else if(popit == 'custom_prevent_alert'){ 
                    $(window).unbind();
                    return undefined;
                }
            } 
        }
        // For reload event confirmation when place order and cart modal open : END

        $('body').on("click","#Modal-Cart #footer_form #gq",function(){
          popit = 'custom_prevent_alert';
        });

        function editQuatation(thatEle) {
            var newCatArray = [];
            for (var i = 0; i < catArray.length; i++) {
                var elementCount = 1;
                var isElementAdded = false;
                while (isElementAdded == false) {
                    if (newCatArray.indexOf(catArray[i] + "_" + elementCount) < 0) {
                        newCatArray.push(catArray[i] + "_" + elementCount);
                        isElementAdded = true;
                    } else {
                        elementCount++;
                    }
                }
            }
            var selectedProduct = {};
            // var productName = $(thatEle).parent().children('input').attr('name');
            var productName = $(thatEle).closest('td').children('input').attr('name');
            var productArray = productName.split("_");
            var catNoFromnewCatArray = parseInt(productArray[2]) - 1;
            var newCatArrayA = newCatArray[catNoFromnewCatArray].split("_");
            var newProductArray = {
                roomno: productArray[0],
                catname: productArray[1],
                catpos: newCatArrayA[1],
            }
            selectedProduct[productName] = newProductArray;

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('c_level/Quotation_controller/get_quotation_edit_form') ?>",
                data: selectedProduct,
                success: function (data) {
                    $('#editQuatationModal .modal-body').html('');
                    $('#editQuatationModal .modal-body').html(data);
                    $('#editQuatationModal').modal('show');
                }
            });
        }

        $('body').on('submit', "#UpdateQuotDetails", function (e) {
            e.preventDefault();
            var submit_url = "<?php echo base_url('c_level/quotation_controller/update_quotation_details') ?>";
            var formData = $(this).serializeArray();

            formData.push({
                name: 'wfractionText',
                value: $('#width_fraction_id option:selected').text()
            });
            formData.push({
                name: 'hfractionText',
                value: $('#height_fraction_id option:selected').text()
            });

            $.ajax({
                type: 'POST',
                url: submit_url,
                data: formData,
                success: function (res) {
                    if (res == 1) {
                        toastr.success('Success! - Quatation Updated Successfully');
                        $('#editQuatationModal').modal('hide');
                        location.reload();
                    }
                }
            });
        })

        $('body').on('click', '#previous-slide', function () {
            $("#Modal-Place-Order ul.nav-tabs li.active").prev('li').children('a').click();
            //start code for get next form records and add it in localstorage by DM
                var next_form_class  = $("li.tab.active").attr("id");
                var next_form_data = $('.'+next_form_class).serializeArray();
                localStorage.setItem(next_form_class, JSON.stringify(next_form_data));
                next_slide_data = JSON.parse(localStorage.getItem(next_form_class));
            //end code for get next form records and add it in localstorage by DM
        });
        $('body').on('click', '#next-slide', function () {
            //start code for get prev form records and add it in localstorage by DM
                var active_form_class  = $("li.tab.active").attr("id");
                var previous_form_data = $('.'+active_form_class).serializeArray();
                localStorage.setItem(active_form_class, JSON.stringify(previous_form_data));
                previous_slide_data = JSON.parse(localStorage.getItem(active_form_class));
            //end get prev form records and add it in localstorage by DM
            var last_cls = $("#Modal-Place-Order ul.nav-tabs li.active").next('li').attr('class');
            if(last_cls.indexOf('add_win_btn') != -1){
                getNewBlankOrder();
            }else{
                $("#Modal-Place-Order ul.nav-tabs li.active").next('li').children('a').click();
            }
        });

        $('body').on('click','#editOrderModal .modal-header button.close', function(){
            getOrderFooter();
        })

    function filterToDecimal(evt, element) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
        return false;
      else {
        var len = $(element).val().length;
        var index = $(element).val().indexOf('.');
        if (index > 0 && charCode == 46) {
          return false;
        }
        if (index > 0) {
          var CharAfterdot = (len + 1) - index;
          if (CharAfterdot > 3) {
            return false;
          }
        }

      }
      return true;
    }
    </script>

<script type="text/javascript">
    function UpdateExtraDiscount(details,position,qt_id){
        var extra_discount = $("#extra_discount_"+position).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/quotation_controller/UpdateExtraDiscount') ?>",
            data: {
                details: details,
                position: position,
                total_extra_discount: extra_discount,
                qt_id: qt_id,
            },
            success: function (res) {
                var obj = jQuery.parseJSON( res );
                if(obj.status == 'success') {
                    var sub_total           = $('#quotation_table_sub_total_'+position).html();
                    var salestax_amount      = $('#quotation_table_sales_tax_amount_'+position).html();
                    var Upcharge            = $('#quotation_table_upcharge_amount_'+position).html();
                    var final_amount        = parseFloat(sub_total) + parseFloat(Upcharge) + parseFloat(salestax_amount);
                    var final_grand_total   = final_amount - extra_discount;
                    $('#quotation_table_grand_total_'+position).html(final_grand_total.toFixed(2));
                }
                Swal.fire(obj.message);
            },
            error: function () {
                Swal.fire('error');
            }
        });
    }
    //Remove spacial characters from extra discount 
    $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
           //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
           if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
               event.preventDefault();
           }
    });
    //validation for extra discount 
    $(".extra_discount").on('blur mouseover', function () {
        var position        = $(this).data('position');
        var extra_discount  = $("#extra_discount_"+position).val();
        // var grand_total     = $("#quotation_table_grand_total_"+position).html();
        var grand_total     = $("#quotation_table_sub_total_"+position).html();
        var grand_total     = Math.round(grand_total);
        if (extra_discount == 0) {
            $("#btn_extra_discount_"+position).attr("disabled", true);
        }else if(extra_discount >= grand_total){
            $("#btn_extra_discount_"+position).attr("disabled", true);
            $("#extra_discount_error_"+position).html('Extra discount must be less than Grand Total !');
        }else{
            $("#extra_discount_error_"+position).html('');
            $("#btn_extra_discount_"+position).attr("disabled", false);
        }
    });
</script>