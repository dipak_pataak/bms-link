<style>
    .btn.btn-danger {
        color: #fff;
    }
    #save_order_form .card {
        box-shadow: none;
    }
    #save_order_form .card-body {
        padding: 0 !important;
    }
    #clearCart {
        color: #fff;
    }
</style>
<div class="col-lg-12 mb-4 footer_section">

    <div id="footer_form">
        <?= form_open_multipart('c_level/order_controller/save_order', array('id' => 'save_order_form')) ?>
        <input type="hidden" name="order_trns_email" id="put_transfer_email" value="">
        <div class="card mb-4">
            <div class="card-body">

                <h5 class="mb-4">Customer Info</h5>
                <div class="separator mb-5"></div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                            <div class="col-sm-8">
                                <p><input type="text" name="orderid" id="orderid" value="<?php echo $orderid; ?>" class="form-control" readonly></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Date</label>
                            <div class="col-sm-8">
                                <p>
                                    <input type="text" name="order_date" id="order_date" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>">
                                </p>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="customer_id" name="customer_id">
                    <input type="hidden" name="customertype" id="customertype" value="">

                    <div class="form-group col-md-6">

                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Side Mark</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="side_mark" name="side_mark" readonly>
                            </div>
                        </div>

                    </div>

                    <div class="form-group col-lg-6">
                        <div class="row m-0">
                            <div class="col-sm-8 offset-sm-4 mb-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="shipaddress">
                                    <label class="custom-control-label" for="shipaddress">Different Shipping Address</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="synk_status" value="1" id="synk_status" onclick="synk_new_status()">
                                    <label class="custom-control-label" for="synk_status">Send Order to <?= $binfo->company_name; ?></label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="transfer_order_and_email"
                                        id="transfer_order_and_email" onclick="transfer_order()">
                                    <label class="custom-control-label" for="transfer_order_and_email">External order</label>
                                </div>
                                <div class="transfer_order_and_email_input" style="display: none;">
                                    <div class="form-group ">
                                        <div class="row m-0">
                                            <div class="col-sm-12">
                                                <input type="text" onchange="put_prder_transfer_email(this.value)" class="form-control" placeholder="Enter Email Address">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-6" >
                        <div class="row m-0 ship_addr" style="display: none;">
                            <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                            <div class="col-sm-8">
                                <input type="text" id="mapLocation" class="form-control" onkeyup="getSalesTax(this.value)">
                                <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                            </div>
                        </div>
                    </div>


                </div>


                <h5>Order Details</h5>
                <div class="separator mb-3"></div>

                <div class="" id="cartItems">
                    <div class="table-responsive order_item_cart">
                        <?php

                            $data['company_profile'] = $this->settings->company_profile();
                            echo $this->load->view('c_level/orders/new_order_cart', $data);

                        ?>
                    </div>
                </div>

            </div>
        </div>



        <div class="col-lg-5 offset-lg-7">

            <div class="card mb-4">

                <div class="card-body">

                    <table class="table table-bordered mb-4">

                        <tr>
                            <td>Sub Total (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="number" name="subtotal" id="subtotal" readonly="" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td id='tax_text'>Sales Tax (%)</td>
                            <td>
                                <input type="hidden" name="tax" onchange="calculetsPrice()" onclick="calculetsPrice()" id="tax" value="0" class="form-control text-right" readonly="">
                                <input type="text"  id="tax_val" value="0" class="form-control text-right" readonly="">
                            </td>
                        </tr>

                        <tr>
                            <td>Installation Charge (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="number" name="install_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="install_charge" step="any" class="form-control text-right"></td>

                        </tr>

                        <tr>
                            <td>Other Charge (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="number" name="other_charge" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" id="other_charge" step="any"  class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Misc (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="number" name="misc" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" id="misc" min="0" step="any"  class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Discount (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="decimal" name="invoice_discount" onchange="calculetsPrice()" onkeyup="calculetsPrice()" value="0" min="0" step="any"   id="invoice_discount" class="form-control text-right"></td>
                        </tr>

                        <tr>
                            <td>Grand Total (<?= $currencys[0]->currency; ?>)</td>
                            <td><input type="number" name="grand_total" id="grand_total" class="form-control text-right" readonly="" required></td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>

        <input type="hidden" name="order_status" id="order_status">
        <input type="hidden" name="transfer_email" id="transfer_email" value="0">

        <div class="col-lg-6 offset-lg-6 text-right">
            <button type="submit" class="btn btn-success" id="gq">Submit</button>
            <a class="btn btn-danger" id="clearCart" onclick="ClearDataFromCart()">Clear All</a>
        </div>


        <?= form_close() ?>
    </div>

</div>
<script>
    $('#footer_form').hide();
    $('body').on('click', '.btn_get_fform', function () {
        $('#footer_form').show();
       // getOrderFooter();
    });

    function put_prder_transfer_email(email)
    {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(mailformat))
        {
        }else{
            Swal.fire("You have entered an invalid email address!")
            $('#put_transfer_email').val('');
            return false;
        }
        $('#put_transfer_email').val(email);

    }

    function synk_new_status(){
        var synk_status_data = $('#synk_status').is(":checked");
        if (synk_status_data) {
            $("#transfer_order_and_email").prop("checked", false);
            $('#transfer_email').val(0);
            $('.transfer_order_and_email_input').hide();
        }
    }

    function transfer_order() {
        var transfer_order = $('#transfer_order_and_email').is(":checked");
        if (transfer_order) {
            $('#transfer_email').val(1);
            $('.transfer_order_and_email_input').show();
            $("#synk_status").prop("checked", false);
        } else {
            $('#transfer_email').val(0);
            $('.transfer_order_and_email_input').hide();
        }
    }    
</script>