<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
        top: 262.594px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
    .select2-container--disabled .select2-selection.select2-selection--single.form-control {
        background-color: #dee2e6;
    }
</style>

<?php $currency = $company_profile[0]->currency; ?>



<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>My order</h1>
                    <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'My order';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <?php
                            $user_data=access_role_permission(99);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Order</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">My Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>


        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>
        </div>

        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="<?= base_url('retailer-multiple-payment-wholesaler'); ?>" method="post">
                            <input type="hidden" name="payment_multiple" id="hidden_multi_order_id">
                             <?php
                                $user_id = $this->session->userdata('user_id');
                                $user_info = $this->db->select('*')->from('user_info a')->where('a.id', $user_id)->get()->result();
                            if($user_info[0]->package_id!=1) {?>
                            <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){
                             ?>
                            <input type="submit" name="" class="btn btn-primary pull-right" id="make_multi_payment" value="Make Payment">
                            <?php } ?>
                             <?php } ?>
                        </form>
                        <div class="table-responsive">


                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="" id="master"></th>
                                        <th>Customer Invoice No </th>
                                        <th><?= $binfo->company; ?><br>Order No </th>
                                        <th>Client Name </th>
                                        <th>Side mark</th>
                                        <th>Order date</th>
                                        <th>Price</th>
                                        <th>Order Due</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php

                                    if (!empty($orderd)) {

                                        // echo "<pre>";
                                        // print_r($orderd);
                                        // exit;
                                        foreach ($orderd as $key => $value) {

                                            ?>

                                            <tr style="<?= ($value->synk_status == '1') ?>">
                                                <td>
                                                    <?php if ($value->order_stage != 2) { ?>
                                                        <input type="checkbox" name="order_List" value="<?= $value->order_id; ?>" data-customer_id="<?= $value->customer_id; ?>" class="sub_chk">
                                                    <?php } ?>
                                                </td>
                                                <td><?= $value->order_id; ?></td>
                                                <td>
                                                    <?php
                                                        $blevel_order_id = $value->order_id;
                                                        if ($value->synk_status != 0) {
                                                            $blevel_order_id = $this->db->select('*')->where('clevel_order_id', $value->order_id)->order_by('id','desc')->get('b_level_quatation_tbl')->row();
                                                            $value->grand_total = @$blevel_order_id->grand_total;
                                                            $value->due = @$blevel_order_id->due;
                                                            $wholesaler_payment_status = @$blevel_order_id->order_stage;
                                                            $blevel_order_id = @$blevel_order_id->order_id;
                                                            echo $blevel_order_id;
                                                        } ?>
                                                </td>
                                                <td><?= $value->customer_name; ?></td>
                                                <td><?= $value->side_mark; ?></td>
                                                <td><?= date_format(date_create($value->order_date), 'M-d-Y'); ?></td>
                                                <td><?= $currency; ?><?= round($value->grand_total,2) ?></td>
                                                <td><?= $currency; ?><?= round($value->due,2) ?></td>
                                                <?php print_r($user_log);
                                                ;?>


                                                <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                    <td>
                                                          <?php
                                                           if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                        <select  class="form-control select2-single" disabled="true" data-placeholder="-- select one --">
                                                            <option value=""></option>
                                                            <option value="1" <?= ($wholesaler_payment_status == 1 ? 'selected' : '') ?>>Quote</option>
                                                            <option value="3" <?= ($wholesaler_payment_status == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                            <option value="4" <?= ($wholesaler_payment_status == 4 ? 'selected' : '') ?>>Manufacturing</option>
                                                            <option value="8" <?= ($wholesaler_payment_status == 8 ? 'selected' : '') ?>>Ready to be shipped</option>
                                                            <option value="5" <?= ($wholesaler_payment_status == 5 ? 'selected' : '') ?>>Shipping</option>
                                                            <option value="9" <?= ($wholesaler_payment_status == 9 ? 'selected' : '') ?>>In Transit</option>
                                                            <option value="7" <?= ($wholesaler_payment_status == 7 ? 'selected' : '') ?>>Delivered</option>
                                                            <option value="2" <?= ($wholesaler_payment_status == 2 ? 'selected' : '') ?>>Paid</option>
                                                            <option value="6" <?= ($wholesaler_payment_status == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                            <option value="10" <?= ($wholesaler_payment_status == 10 ? 'selected' : '') ?>>Confirmation</option>
                                                            <option value="11" <?= ($wholesaler_payment_status == 11 ? 'selected' : '') ?>>Manufacturing</option>
                                                        </select>
                                                    </td>
                                                <?php } ?>
                                                <?php } ?>

                                                <td>
                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                                                
                                                        <?php if ($value->order_stage == 7) { ?>
                                                             <?php
                                                           if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                            <a href="c-customer-order-return/<?= $value->order_id ?>" class="btn btn-primary btn-sm" title="Return "><i class="iconsmind-Repeat-6" aria-hidden="true"></i></a>
                                                        <?php } ?>
                                                        <?php } ?>

                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>

                                                        <?php if ($value->synk_status == 0) { ?>
                                                            <a href="<?= base_url('retailer-synk-wholesaler/') ?><?= $value->order_id; ?>" class="btn btn-primary btn-xs default" title="Sync to wholesaler">
                                                                <i class="iconsmind-Repeat-2"></i>
                                                            </a>
                                                        <?php } ?>

                                                    <?php } ?>
                                                  
                                                    <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>
                                                        <?php
                                                           if($user_data[0]->can_access==1 or $log_data[0]->is_admin==1){ ?>
                                                        <a href="<?= base_url('retailer-my-receipt/') ?><?php echo $blevel_order_id; ?>" class="btn btn-success btn-xs default" title="View">
                                                            <i class="simple-icon-eye"></i>
                                                        </a>
                                                        <?php } ?>
                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->delete()->access()) { ?>
                                                          <?php
                                                           if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                        <a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
                                                         <?php } ?>
                                                    <?php } ?>

                                                </td>
                                            </tr>

                                        <?php
                                            }
                                        } else {
                                            ?>

                                        <div class="alert alert-danger"> There have no order found..</div>
                                    <?php } ?>

                                </tbody>
                            </table>

                            <?= @$links; ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>


<div class="modal fade" id="cancel_comment_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Comments</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="cancel_comment_info">
                <form class="" action="<?php echo base_url('c_level/Order_controller/cancel_comment'); ?>" method="post">
                    <textarea class="form-control" name="cancel_comment" required></textarea>
                    <input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="stage_id" id="stage_id">
                    <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $('.sub_chk, #make_multi_payment').on('click', function() {

            var order_ids = '';
            var customer_ids = [];
            var checked_customer_ids = [];

            $('[name="order_List"]').each(function(i, e) {
                if ($(e).is(':checked')) {
                    var comma = order_ids.length === 0 ? '' : ',';
                    order_ids += (comma + e.value);
                    customer_id = $(this).data('customer_id');

                    if (jQuery.inArray(customer_id, customer_ids) || customer_ids.length == 0) {
                        customer_ids.push(customer_id);
                    }

                    checked_customer_ids.push(customer_id);
                }
                $("#hidden_multi_order_id").val(order_ids);
            });
            // if(customer_ids.length == 1){
            //     $("#make_multi_payment").show(800);
            // }else{
            //     // swal("Alert!", "Please select same Customer Invoice No..");
            //     $("#make_multi_payment").hide(800);
            // }
        });

        $('#master').on('click', function() {
            if ($(this).prop("checked") == true) {
                var order_ids = '';
                $(".sub_chk").trigger('click');
                $('[name="order_List"]').each(function(i, e) {
                    if ($(e).is(':checked')) {
                        var comma = order_ids.length === 0 ? '' : ',';
                        order_ids += (comma + e.value);
                    }
                });
                $("#hidden_multi_order_id").val(order_ids);
            } else {
                $("#hidden_multi_order_id").val("");
            }

            console.log(order_ids);
        });
    })



    $(document).on("change", ".sub_chk", function() {
        var all_checked = $("input[type='checkbox'].sub_chk");

        var values = new Array();
        $.each($("input[type='checkbox']:checked"), function() {
            values.push($(this).val());
        });
        $('#check_id').val(values);

        if (all_checked.length == all_checked.filter(":checked").length) {
            $("#master").prop('checked', true);
        } else {
            $("#master").prop('checked', false);
        }
    });

    $('#master').on('change', function(e) {
        if ($(this).is(':checked', true)) {
            $(".sub_chk").prop('checked', true);
            var values = new Array();
            $.each($("input[class='sub_chk']:checked"), function() {
                values.push($(this).val());
            });
            $('#check_id').val(values);
        } else {
            $(".sub_chk").prop('checked', false);
        }
    });
</script>

<script type="text/javascript">
    function setOrderStage(stage_id, order_id) {

        if (stage_id != '') {

            if (stage_id === '2' || stage_id === '3') {
                window.location.href = 'retailer-order-view/' + order_id;
            }


            if (stage_id === '9' || stage_id === '8' || stage_id === '7' || stage_id === '6' || stage_id === '4' || stage_id === '5' || stage_id === '1') {


                $.ajax({
                    url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function(r) {
                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function() {
                            window.location.href = window.location.href;
                        }, 2000);
                    }
                });

            }


        } else {

            $("#order_id").val(order_id);
            $("#stage_id").val(stage_id);
            $("#cancel_comment_info").html();
            $('#cancel_comment_modal_info').modal('show');
        }

    }
</script>
