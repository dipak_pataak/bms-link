<main id="main_print_div">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <?php $currency = $company_profile[0]->currency; ?>
                <div id="page-wrap">
                    <div id="print_div">
                        <style>
                            #page-wrap { width: 800px; margin: 0 auto; }

                            .inner_print_div table { border-collapse: collapse; }
                            .inner_print_div table td, .inner_print_div table th { border: 1px solid black; padding: 5px; }

                            .inner_print_div #header { height: unset; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

                            .inner_print_div #customer { overflow: hidden; }
                            .inner_print_div #customer-title { font-size: 20px; font-weight: bold; float: left; }
                            .inner_print_div #meta { margin-top: 1px; width: 300px; float: right; }
                            .inner_print_div #meta td { text-align: right;  }
                            .inner_print_div #meta td.meta-head { text-align: left; background: #eee; }

                            .inner_print_div #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
                            .inner_print_div #items th { background: #eee; }
                            .inner_print_div #items tr.item-row td { border: 0; vertical-align: top; }
                            .inner_print_div #items td.description { width: 300px; }
                            .inner_print_div #items td.item-name { width: 175px; }

                            .inner_print_div #terms { text-align: center; margin: 20px 0 0 0; }
                            .inner_print_div #terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
                            @page { size: landscape; }
                            .inner_print_div a {
                                color: #007bff !important;
                            }
                            .inner_print_div #items {
                                clear: both;
                                width: 100%;
                                margin: 30px 0 0 0;
                                border: 1px solid black;
                            }
                            .inner_print_div #items th{
                                text-align: center;
                            }
                            .inner_print_div p{
                                margin: 0px !important;
                                /*font-family: Georgia, serif;*/
                            }
                        </style>    
                        <div class="inner_print_div">
                            <div id="header">Money Receipt</div>

                            <div class="row1">
                                <table width="100%" cellpadding="2" cellspacing="2" border="0">
                                    <tr>
                                        <td align="left" style="border:none">
                                            <p><?= $company_profile[0]->company_name; ?></p>
                                            <p><?= $company_profile[0]->address; ?></p>
                                            <p><?= $company_profile[0]->city; ?>,
                                                <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                                                <?= $company_profile[0]->country_code; ?></p>
                                            <p><?= $company_profile[0]->phone; ?></p>
                                            <p><?= $company_profile[0]->email; ?></p>
                                        </td>
                                        <td align="right" style="border:none">
                                            <img src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>"
                                                width="150px">
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div style="clear:both"></div>
                            <div id="customer">
                                <div id="customer-title" style="font-size: 14px;"><br>
                                    <strong>Sold To: </strong><?= $customer->first_name; ?> <?= $customer->last_name; ?>
                                    <p><?= $customer->address; ?></p>
                                    <p><?= $customer->city; ?>,
                                        <?= $customer->state; ?>, <?= $customer->zip_code; ?>,
                                        <?= $customer->country_code; ?></p>
                                    <p><?= $customer->phone; ?></p>
                                    <p><?= $customer->email; ?></p>
                                </div>
                                <table id="meta">
                                    <tr>
                                        <td class="meta-head">Order #</td>
                                        <td><?= $orderd->order_id ?></td>
                                    </tr>
                                    <tr>
                                        <td class="meta-head">Date</td>
                                        <td><?= date_format(date_create(@$orderd->order_date),'M-d-Y') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="meta-head">Grand Total</td>
                                        <td><?= $currency; ?><?= @$orderd->grand_total ?></td>
                                    </tr>
                                    <tr>
                                        <td class="meta-head">Amount Due</td>
                                        <td><?= $currency; ?><?= @$orderd->due ?></td>
                                    </tr>
                                </table>
                            </div>

                            <table id="items">
                                <tr>
                                    <th>Description</th>
                                    <th>Payment Method</th>
                                    <th>Date</th>
                                    <th align="right">Amount</th>
                                </tr>
                                <tr class="item-row">
                                    <td class="description">Payment Received for Order# <?= $orderd->order_id ?> with thanks.</td>
                                    <td><?=@$payment->payment_method?></td>
                                    <td align="center"><?= date_format(date_create(@$payment->payment_date),'M-d-Y') ?></td>
                                    <td align="right"><?= $currency; ?><?=@$payment->paid_amount?></td>
                                </tr>
                            </table>
                            <div id="terms">
                                <h5>&nbsp;</h5>
                                This is a computer generated receipt. No signature required.<br />
                                Please contact <a href="http://bmslink.net"><?= $company_profile[0]->company_name; ?></a> for further details.
                            </div>
                        </div>    
                    </div>
                    <!-- <button type="button" onclick="goBack()" style="float: left;">Back</button> -->
                    <button type="button" onclick="printDiv('print_div')" style="float: right;">Print</button>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
    function printDiv(divName) {
        var restorepage = document.body.innerHTML;
        $('body').css('min-height', 'auto');
        var printAllContent = $('#'+divName).html();
        document.body.innerHTML = printAllContent;
        window.print();
        // document.body.innerHTML = restorepage;
        location.reload();
    }

    function goBack() {
        window.history.back();
    }
</script>
