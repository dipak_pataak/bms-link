<style type="text/css">
    .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control {
        margin-left: 34.6%;
        margin-bottom: 1rem;

    }
    .col-sm-3 + .col-sm-3 br+.col-sm-12 input+.form-control {
        margin-left: 0!important;
    }
    .form-group {
        margin: 0px;
    }
    .form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6 {
        padding-left: 0px;
    }
    .col-sm-3, label.col-sm-3 {	
        line-height:14px;
        padding-top:10px;
        margin-bottom: 0!important;
    }            
    .row {
        margin-bottom: 1rem;
        clear:both;
    }
    div.col-sm-3 + div.col-sm-3 {
        margin:0 !important;
    }
    br {
        display:none;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 958.55px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }

    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }

    /*--- for retailer --*/
    .col-sm-9 {
        padding-right: 0;
    }
    .col-sm-3 + .col-sm-3 {
        flex: 0 0 100% !important;
        max-width: 100% !important;
        clear: both;
        float: none;
        padding: 0;
    }
    .col-sm-3 .col-sm-12 {
        clear: both;
        padding: 0;
        margin: 0 -15px;
    }
    .col-sm-3 + .col-sm-3 .col-sm-12 label {
        width: 34.7%;
        float: left;
        padding: 0px 15px;
        line-height:14px;
        padding-top:10px;
    }
    .col-sm-3 + .col-sm-3 .col-sm-12 .form-control {
        width: 41.7%;
        float: left;
        margin-bottom: 1rem;
    }
    .card-body .col-sm-9 .col-sm-2 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 16.66667%;
        flex: 0 0 16.66667%;
        max-width: 16.66667%;
    }
    .card-body .col-sm-9 .col-sm-3 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.33%;
        flex: 0 0 33.33%;
        max-width: 33.33%;
    }
    .card-body .col-sm-9 .col-sm-3 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.33%;
        flex: 0 0 33.33%;
        max-width: 33.33%;
    }
    .card-body .col-sm-9 .col-sm-4, .card-body .col-sm-9 .col-sm-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 40%;
        flex: 0 0 40%;
        max-width: 40%;
        padding-right:0;
    }   
    #pattern_color_model .col-sm-1 {
        padding-right:0;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 14%;
        flex: 0 0 14%;
        max-width: 14%;
        line-height:14px;
        padding-top: 10px;
    }
    #pattern_color_model .col-sm-2 {
        padding:0;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 11%;
        flex: 0 0 11%;
        max-width: 11%;
    }
    @media only screen and (max-width: 480px) {
        .form-row {
            padding-right: 25px;
            margin-right:0;
        }
        .card-body .col-sm-9 .col-sm-3, #pattern_color_model .col-sm-1,.form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6, #pattern_color_model .col-sm-2, .card-body .col-sm-9 .col-sm-4, .card-body .col-sm-9 .col-sm-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
        .col-sm-3 + .col-sm-3 .col-sm-12 label {
            width: 53.7%;
        }
        .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control{
            margin-left: 54%;
        }
        .col-sm-3 + .col-sm-3 .col-sm-12 .form-control {
            width: 54%;
        }
        .card-body .col-sm-9 .col-sm-2:last-child {
            margin-left:46.3%;
            margin-top: 1rem;
            -webkit-box-flex: 0;
            -ms-flex: 0 0 54%;
            flex: 0 0 54%;
            max-width: 54%;
            padding-right: 0;
        }
        #pattern_color_model .col-sm-1 {
            margin-top: 1rem;
        }
        #pattern_color_model .col-sm-2 {
            margin-left: 0;
        }
    }
</style>

<!-- add customer css end-->
<main id="orderPage" style="margin: 0px !important; position: relative;">
    <div id="cover-until-load" style="position: absolute; top: 0px; left: 0px;
         background-color: rgba(255,255,255,0.75); height: 100%; width: 100%; z-index: 999999;"></div>
    <div class="container-fluid" style="margin-bottom:0;">
        <div style="margin-bottom:0;">
            <div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row sticky_container">
                            <div class="col-sm-9">
                                <div class="form-row">

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Category</label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2-single" name="category_id" id="category_id"  required="" data-placeholder="--select one --" onchange="onCategoryChange(this);" <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($get_category as $category) {
                                                        if ($item_details->category_id == $category->category_id) {
                                                            echo "<option value='$category->category_id' selected>$category->category_name</option>";
                                                        } else {
                                                            echo "<option value='$category->category_id'>$category->category_name</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="subcategory_id">
                                        <?php echo $category_wise_subcategory; ?>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Product</label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2-single" name="product_id" id="product_id" onchange="onProductChange(this)" required="" data-placeholder="-- select one --" <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                                    <?php echo $customer_product_by_category; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="color_model"></div>
                                    <div class="form-group col-md-12" id="pattern_color_model"></div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Width</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0"  required="" onkeyup="onWidthKeyup(this)" value="<?php echo $item_details->width; ?>" <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                                    <?php echo $cat_fraction; ?>
                                                    <!-- <option value="">--Select one--</option>
                                                    <?php
                                                    //foreach ($fractions as $f) {
                                                    //echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    //}
                                                    ?> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Height</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0" required="" onkeyup="onHeightKeyup(this)" value="<?php echo $item_details->height; ?>" <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --' <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                                    <?php echo $cat_fraction; ?>
                                                    <!-- <option value="">--Select one--</option>
                                                    <?php
                                                    //foreach ($fractions as $f) {
                                                    //echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    //}
                                                    ?> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="ssssttt14"></div>

                                    <div class="form-group col-md-12">
                                    </div>

                                    <!-- atributs area -->
                                    <div class="form-group col-md-12" id="attr">

                                    </div>
                                    <!-- End atributs area -->


                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Room</label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2-single" name="room" id="room"  required="" data-placeholder="--select one --" <?php if ($is_view == 1): ?> disabled <?php endif ?>>
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($rooms as $r) {
                                                        if ($item_details->room == $r->room_name) {
                                                            echo "<option value='$r->room_name' selected>$r->room_name</option>";
                                                        } else {
                                                            echo "<option value='$r->room_name'>$r->room_name</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Note</label>
                                            <div class="col-sm-4">
                                                <textarea class="form-control" name="notes" rows="6" <?php if ($is_view == 1): ?> disabled <?php endif ?>></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="total_price" id="total_price">
                            </div>
                            <div class="col-sm-3 sticky_item">
                                <div class="fixed_item">
                                    <p id="tprice"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script type="text/javascript">
    var formReneredSuccess = false;

    var formAttrCount = 0;

    function onCategoryChange(element) {
        console.log('got ehre');
        var category_id = $(element).val();
        if ($('#category_id').find(":selected").text() == 'Arch') {
            $('#height').val(0).attr('disabled', 'true');
            $('#height_fraction_id').val(0).attr('disabled', 'true');
            $('#height').parent().parent('.row').parent('.form-group').hide();
        } else {
            $('#height').removeAttr('disabled');
            $('#height_fraction_id').removeAttr('disabled');
            $('#height').parent().parent('.row').parent('.form-group').show();
        }

        $.ajax({
            url: "<?= base_url() ?>c_level/order_controller/category_wise_subcategory/" + category_id,
            type: 'get',
            success: function (r) {
                $("#subcategory_id").html(r);
            }
        });
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/order_controller/get_cat_fraction/" + category_id,
            type: 'get',
            dataType: 'JSON',
            success: function (r) {
                $("#width_fraction_id").html('');
                $("#width_fraction_id").html(r.html);
                $("#height_fraction_id").html('');
                $("#height_fraction_id").html(r.html);
            }
        });
        $("#total_price").val(0);
        $("#main_price").val(0);
        $("#product_id").val('');
        // $("#tprice").text("Total Price = " + var_currency + "0");
        $("#tprice").text("");
        $("#attr").load(location.href + " #attr>*", "");
        loadPStyle();
        $.ajax({
            url: "<?php echo base_url('c_level/order_controller/get_customer_product_by_category/'); ?>" + category_id,
            type: 'get',
            success: function (r) {
                $("#product_id").html(r);
            }
        });
    }

    function onProductChange(element) {
        var product_id = $(element).val();
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/order_controller/get_product_to_attribute/" + product_id,
            type: 'get',
            success: function (r) {
                $("#attr").html('');
                $("#attr").html(r);
                if (!formReneredSuccess) {
                    onProductChangeTriggeredOne();
                } else {
                    callTrigger();
                }
                $('#cartbtn').removeAttr('disabled');
            }
        });
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/order_controller/get_color_partan_model/" + product_id,
            type: 'get',
            success: function (r) {
                $('#color_model').html(r);
                if (!formReneredSuccess) {
                    onProductChangeTriggeredTwo();
                }
            }
        });
    }

    function loadPStyle() {
        var product_id = $('#product_id').val();
        var pattern_id = $('#pattern_id').val();
        var pricestyle = $('#pricestyle').val();
        var hif = ($("#height_fraction_id :selected").text().split("/")[0] / $("#height_fraction_id :selected").text().split("/")[1]);
        var wif = ($("#width_fraction_id :selected").text().split("/")[0] / $("#width_fraction_id :selected").text().split("/")[1]);
        var height = parseFloat($('#height').val()) + (isNaN(hif) ? 0 : hif);
        var width = parseFloat($('#width').val()) + (isNaN(wif) ? 0 : wif);
        var width_w = (isNaN(width) ? '' : width);
        var height_w = (isNaN(height) ? '' : height);
        var price = $("#height").parent().parent().parent().next();
        if (pricestyle === '4') {
            if (pattern_id == '') {
                Swal.fire('Please select the Pattern/Model');
                $("#pattern_id").css("border-color", "red").focus();
            }
        }
        if (pricestyle === '1' || pricestyle === '4' || pricestyle === '5') {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_product_row_col_price/" + height_w + "/" + width_w + "/" + product_id + "/" + pattern_id,
                type: 'get',
                success: function (r) {
                    var obj = jQuery.parseJSON(r);
                    $(price).html(obj.ht);
                    cal();
                    if (obj.st === 1) {
                        $('#cartbtn').removeAttr('disabled');
                    }
                }
            });
        }
        if (pricestyle === '2') {
            var main_p = parseFloat($('#sqr_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var new_width = parseFloat(width + 3);
            var new_height = parseFloat(height + 1.5);

            var sum = (new_width * new_height) / 144;
            
            // var sum = (width * height) / 144;
            var price = sum * main_price;
            $('#main_price').val(price.toFixed(2));
        }
        cal();
    }
</script>

<script>
    var var_currency = '$';
    <?php if (isset($currencys[0]->currency) && !empty($currencys[0]->currency)) { ?>
            var var_currency = '<?= $currencys[0]->currency; ?>';
    <?php } ?>

    $('body').on('change', function () {
        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);
    })

    $('body').on('click', function () {
        $("#pattern_id").css("border-color", "");
        cal();
    })

    $('#install_charge').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#other_charge').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#invoice_discount').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    $('#misc').on('keyup', function () {
        var value = $(this).val();
        var att = value.split(".")[1];
        if (att.length > 2) {
            $(this).val(parseFloat(value).toFixed(2));
        }
    })

    function multiOptionPriceValue(att_op_op_op_op_id) {
        var op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var att = att_op_op_op_op_id.split("_")[1];
        var op_op_id = att_op_op_op_op_id.split("_")[2];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }
        if (op_op_op_id) {
            var wrapper = $("#mul_op_op_id" + op_op_id).parent().next();
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/multioption_price_value/') ?>" + op_op_op_id + "/" + att + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }

    function OptionFive(att_op_op_op_op_id, attribute_id) {
        var op_op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id = att_op_op_op_op_id.split("_")[1];

        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_op_id) {

            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_product_attr_op_five/" + op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });
        }

        cal();
    }


    function OptionOptionsOptionOption(pro_att_op_id, attribute_id) {

        var op_op_op_id = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_id) {

            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_product_attr_op_op_op_op/" + op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }

    function OptionOptionsOption(pro_att_op_id, attribute_id) {


        var op_op_id = pro_att_op_id.split("_")[0];

        var id = pro_att_op_id.split("_")[1];

        var op_id = pro_att_op_id.split("_")[2];

        var main_p = parseFloat($('#main_price').val());


        if (isNaN(main_p)) {

            var main_price = 0;

        } else {

            var main_price = main_p;

        }

        if (op_op_id) {

            var wrapper = $("#op_" + op_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_product_attr_op_op_op/" + op_op_id + "/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });

        }
    }

    function OptionOptions(pro_att_op_id, attribute_id) {
        if (pro_att_op_id) {
            var id = pro_att_op_id.split("_")[0];
            var optin_id = pro_att_op_id.split("_")[1];
            var main_p = parseFloat($('#main_price').val());
            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            //console.log(id+" / "+attribute_id+" / "+main_price);

            var wrapper = $(".options_" + attribute_id).parent().next().next();
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_product_attr_option_option/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                    //console.log('formAttrCount ==> '+formAttrCount)
                    if (!formReneredSuccess) {
                        onOpOpLoadTriggered();
                    }
                }
            });
        }
    }


    function getColorCode(id) {
        var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code/" + id;
        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {
                $('#colorcode').val(res);
                if (!formReneredSuccess) {
                    onColorChangeTriggered();
                }
            }, error: function () {
                Swal.fire('error');
            }
        });
    }


    function getColorCode_select(keyword) {
        var pattern_id = $("#pattern_id").val();
        if (keyword !== '') {
            var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code_select/" + keyword + '/'+ pattern_id;
            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $('#color_id').val(res);

                }, error: function () {
                    Swal.fire('error');
                }
            });
        }
    }

    function cal() {
        var w = $('#width').val();
        var h = $('#height').val();

        if (w !== '' && h !== '') {
            var contibut_price = 0;
            $(".contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contibut_price += parseFloat(this.value))
            });

            var main_price = parseFloat($('#main_price').val());

            if (isNaN(main_price)) {
                var prc = 0;
            } else {

                var prc = main_price;
            }

            var total_price = (contibut_price + prc);
            var t = (isNaN(total_price) ? 0 : total_price);

            $("#total_price").val(t);
            // $("#tprice").text("Total Price = " + var_currency + t);
            $("#tprice").text("");
        }
    }

    function getSalesTax(v) {
        var formData = {'vs': v};
        $.ajax({
            url: '<?= base_url() ?>c_level/order_controller/different_shipping_tax',
            type: 'post',
            data: formData,
            success: function (data) {

                var obj = jQuery.parseJSON(data);
                var tax = 0;
                if(obj != null){
                    var tax = (obj.tax_rate != null ? obj.tax_rate : 0);
                }
                $('#tax').val(tax);
                customerWiseComission();

            }
        });
    }


    function onWidthKeyup(element) {
        var hif = $(element).val().split(".")[1];
        var category_id = $('#category_id').val();
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_height_width_fraction/" + hif + "/" + category_id,
                type: 'get',
                success: function (r) {

                    $("#width_fraction_id ").val(r);

                }
            });
        } else {
            $("#width_fraction_id ").val('');
        }
    }

    function onHeightKeyup(element) {
        var hif = $(element).val().split(".")[1];
        if (hif) {
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_height_width_fraction/" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id ").val(r);
                }
            });
        } else {
            $("#height_fraction_id ").val('');
        }
    }

    function callTrigger() {
        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('change', '#pattern_id', function () {
            var pattern_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/order_controller/get_color_model/" + pattern_id,
                type: 'get',
                success: function (r) {
                    $('#pattern_color_model').html(r);
                    if (!formReneredSuccess) {
                        onPatternChangeTriggered();
                    }
                }
            });
        });
    });
</script>

<script>
    var item_attr = JSON.parse('<?php echo $item_attr; ?>');


    $(document).ready(function () {
        $('.sticky_container .sticky_item').theiaStickySidebar({
            additionalMarginTop: 110
        });
        $('#width_fraction_id').val('<?php echo $item_details->width_fraction_id ?>');
        $('#height_fraction_id').val('<?php echo $item_details->height_fraction_id ?>');
        $('#height_fraction_id').val('<?php echo $item_details->height_fraction_id ?>');
        $('textarea[name="notes"]').text('<?php echo $item_details->notes ?>');
        $('#product_id').trigger('change');
    });


    function onProductChangeTriggeredOne() {
        for (var i = 0; i < item_attr.length; i++) {
            // if (formAttrCount == i) {
            //console.log("formAttrCount == "+i);
            for (var j = 0; j < item_attr[i]['options'].length; j++) {
                $("[name='op_id_" + item_attr[i]['attribute_id'] + "[]']").val(item_attr[i]['options'][j]['option_id'] + "_" + item_attr[i]['options'][j]['option_id']);
                $("[name='op_id_" + item_attr[i]['attribute_id'] + "[]']").trigger('change');
<?php if ($is_view == 1): ?>
                    $("[name='op_id_" + item_attr[i]['attribute_id'] + "[]']").prop('disabled', true);
<?php endif ?>
            }
            // }
        }
    }

    function onOpOpLoadTriggered() {
        for (var i = 0; i < item_attr.length; i++) {
            for (var j = 0; j < item_attr[i]['opop'].length; j++) {
                $("[name='op_op_value_" + item_attr[i]['attribute_id'] + "[]']").each(function (index) {
                    if (index == j) {
                        if (item_attr[i]['opop'][j]['option_type'] == 3) {
                            $(this).val(item_attr[i]['opop'][j]['op_op_value']);
                        } else if (item_attr[i]['opop'][j]['option_type'] == 5) {
                            var splitVal = item_attr[i]['opop'][j]['op_op_value'].split(' ');
                            $(this).val(splitVal[0]);
                            $(this).next().children('select').val(splitVal[1]);
                        }
<?php if ($is_view == 1): ?>
                            $(this).prop('disabled', true);
                            $(this).next().children('select').prop('disabled', true);
<?php endif ?>
                    }
                });
            }
<?php if ($is_view == 1): ?>
                $("[name='op_value_" + item_attr[i]['attribute_id'] + "']").prop('disabled', true);
<?php endif ?>
        }
    }


    function onProductChangeTriggeredTwo() {
        $('#pattern_id').val('<?php echo $item_details->pattern_model_id ?>');
        $('#pattern_id').trigger('change');
    }

    function onPatternChangeTriggered() {
        $('#color_id').val('<?php echo $item_details->color_id ?>');
        $('#color_id').trigger('change');
    }
    ;


    function onColorChangeTriggered() {
        $('#width_fraction_id').trigger('change');

<?php if ($is_view == 1): ?>

            $('#pattern_id').attr('disabled', true);
            $('#color_id').attr('disabled', true);
            $('#colorcode').attr('disabled', true);

<?php endif ?>

        formReneredSuccess = true;
        $('#cover-until-load').hide();
    }
</script>

