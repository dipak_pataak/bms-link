<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
.pac-container {
    border-top: 0 !important;
}

select.form-control,
.select2-dropdown,
.select2-container--bootstrap .select2-selection.form-control {
    padding-left: 2px;
}

.theiaStickySidebar {
    background: #145388;
    padding: 0px !important;
}

#tprice {
    color: #fff;
}

.col-sm-3+.col-sm-3 br+.col-sm-12 .form-control {
    margin-left: 34.6%;
    margin-bottom: 1rem;
}

.col-sm-3+.col-sm-3 br+.col-sm-12 input+.form-control {
    margin-left: 0 !important;
}

.form-group {
    margin: 0px;
}

.form-group.col-md-12 .row div.col-sm-4,
.form-group.col-md-12 .row div.col-sm-6 {
    padding-left: 0px;
}

.col-sm-3,
label.col-sm-3 {
    line-height: 14px;
    padding-top: 10px;
    margin-bottom: 0 !important;
}

.row {
    margin-bottom: 1rem;
    clear: both;
}

div.col-sm-3+div.col-sm-3 {
    margin: 0 !important;
}

br {
    display: none;
}

.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom {
    top: 958.55px !important;
    left: 188px;
    z-index: 10;
    display: block;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
}

/*--- for retailer --*/
.col-sm-9 {
    padding-right: 0;
}

.col-sm-3+.col-sm-3 {
    flex: 0 0 100% !important;
    max-width: 100% !important;
    clear: both;
    float: none;
    padding: 0;
}

.col-sm-3 .col-sm-12 {
    clear: both;
    padding: 0;
    margin: 0 -15px;
}

.col-sm-3+.col-sm-3 .col-sm-12 label {
    width: 34.7%;
    float: left;
    padding: 0px 15px;
    line-height: 14px;
    padding-top: 10px;
}

.col-sm-3+.col-sm-3 .col-sm-12 .form-control {
    width: 41.7%;
    float: left;
    margin-bottom: 1rem;
}

.card-body .col-sm-9 .col-sm-2 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 16.66667%;
    flex: 0 0 16.66667%;
    max-width: 16.66667%;
}

.card-body .col-sm-9 .col-sm-3 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 33.33%;
    flex: 0 0 33.33%;
    max-width: 33.33%;
}

.card-body .col-sm-9 .col-sm-3 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 33.33%;
    flex: 0 0 33.33%;
    max-width: 33.33%;
}

.card-body .col-sm-9 .col-sm-4,
.card-body .col-sm-9 .col-sm-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 40%;
    flex: 0 0 40%;
    max-width: 40%;
    padding-right: 0;
}

#pattern_color_model .col-sm-1 {
    padding-right: 0;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 14%;
    flex: 0 0 14%;
    max-width: 14%;
    line-height: 14px;
    padding-top: 10px;
}

#pattern_color_model .col-sm-2 {
    padding: 0;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 11%;
    flex: 0 0 11%;
    max-width: 11%;
}

@media only screen and (max-width: 480px) {
    .form-row {
        padding-right: 25px;
        margin-right: 0;
    }

    .card-body .col-sm-9 .col-sm-3,
    #pattern_color_model .col-sm-1,
    .form-group.col-md-12 .row div.col-sm-4,
    .form-group.col-md-12 .row div.col-sm-6,
    #pattern_color_model .col-sm-2,
    .card-body .col-sm-9 .col-sm-4,
    .card-body .col-sm-9 .col-sm-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }

    .col-sm-3+.col-sm-3 .col-sm-12 label {
        width: 53.7%;
    }

    .col-sm-3+.col-sm-3 br+.col-sm-12 .form-control {
        margin-left: 54%;
    }

    .col-sm-3+.col-sm-3 .col-sm-12 .form-control {
        width: 46%;
    }

    .card-body .col-sm-9 .col-sm-2:last-child {
        margin-left: 46.3%;
        margin-top: 1rem;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 54%;
        flex: 0 0 54%;
        max-width: 54%;
        padding-right: 0;
    }

    #pattern_color_model .col-sm-1 {
        margin-top: 1rem;
    }

    #pattern_color_model .col-sm-2 {
        margin-left: 0;
    }
}

#addItem td:first-child {
    display: inline-flex
}

#addItem .phone_no_type {
    width: auto;
}
</style>
<!-- add customer css-->
<style>
.pac-card {
    margin: 10px 10px 0 0;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    background-color: #fff;
    font-family: Roboto;
}

#pac-container {
    padding-bottom: 12px;
    margin-right: 12px;
}

.pac-container {
    width: inherit !important;
    left: inherit !important;
    top: inherit !important;
    display: block !important;
}

.pac-controls {
    display: inline-block;
    padding: 5px 11px;
}

.pac-controls label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
}

.phone-input {
    margin-top: 10px;
    margin-bottom: 10px;
}

.phone_type_select {
    width: 85px;
    display: inline-block;
    border-radius: 0;
    vertical-align: top;
    background: #ddd;
}

.phone_no_type {
    display: inline-block;
    width: calc(100% - 85px);
    margin-left: -4px;
    border-radius: 0;
    vertical-align: top;
    line-height: 19px;
}

#normalItem tr td {
    border: none !important;
}

#addItem_file tr td {
    border: none !important;
}

.hidden {
    display: none;
}
</style>
<!-- add customer css end-->
<main id="orderPage">
    <div class="container-fluid" style="margin-bottom:0;">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'New Order';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>"
                        data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <!-- Customer Info : START -->
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4">Customer Info</h5>
                <div class="separator mb-5"></div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Order Id</label>
                            <div class="col-sm-8">
                                <p><input type="text" name="orderid" id="orderid" class="form-control" readonly></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Date</label>
                            <div class="col-sm-8">
                                <p>
                                    <input type="text" name="order_date" id="order_date" class="form-control datepicker"
                                        value="<?php echo date('Y-m-d'); ?>">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="customer_id" class="col-form-label col-sm-4">Customer Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="customer_id" onblur="order_to_wholesaler();">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="new_customer_id" value="0">
                    <div class="form-group col-md-6">
                        <div class="row m-0">
                            <label for="orderid" class="col-form-label col-sm-4">Side Mark<span style="color: red;">*</span></label>
                            <div class="col-sm-8">
                                <span id="side_mark_alert" style="color: red;"></span>
                                <input type="text" class="form-control" id="side_mark" name="side_mark" onblur="order_to_wholesaler();" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="row ship_addr m-0" style="display: none;">
                            <label for="orderid" class="col-form-label col-sm-4">Different Shipping Address</label>
                            <div class="col-sm-8">
                                <input type="text" id="mapLocation" class="form-control"
                                    onkeyup="getSalesTax(this.value)">
                                <p>ex: 300 BOYLSTON AVE E,SEATTLE,WA,98102,USA</p>
                            </div>
                        </div>
                    </div>
                   
                    
                    <div class="form-group col-lg-6">
                        <div class="row m-0">
                            <div class="col-sm-8 offset-sm-4 mb-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="shipaddress" value="1">
                                    <label class="custom-control-label" for="shipaddress">Different Shipping
                                        Address</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" checked="true" disabled="disabled" class="custom-control-input" name="synk_status" value="1"
                                        id="synk_status">
                                    <label class="custom-control-label" for="synk_status">Send Order to
                                        <?= $binfo->company_name; ?></label>
                                </div>
                                
                                <div class="transfer_order_and_email_input" style="display: none;">
                                    <div class="form-group ">
                                        <div class="row m-0">
                                            <div class="col-sm-12">
                                                <input type="text" onchange="put_prder_transfer_email(this.value)" class="form-control" placeholder="Enter Email Address">
                                            </div>
                                        </div>
                                    </div>
                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Customer Info : END -->

        <div style="margin-bottom:0;">

            <!-- Add New Order : START -->
            <div class="card mb-4" id="order_area_sec">
                <div class="card-body">
                    <h5 class="mb-4">Add new Order </h5>
                    <div class="separator mb-5"></div>
                    <div class="row sticky_container" id="add_order_form">

                    </div>
                </div>
            </div>
            <!-- Add New Order : END -->

            <!-- Final Cart Info : START -->
            <form action="<?php echo base_url('c_level/customer_order_controller/save_order'); ?>" id="save_order"
                enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <input type="hidden" name="orderid" id="hid_orderid">
                <input type="hidden" name="order_date" id="hid_order_date">
                <input type="hidden" name="customer_id" id="hid_customer_id">
                <input type="hidden" name="side_mark" id="hid_side_mark">
                <input type="hidden" name="shippin_address" id="hid_shippin_address">
                <input type="hidden" name="different_address" id="hid_different_address">
                <input type="hidden" name="customertype" id="customertype" value="">
                <input type="hidden" name="synk_status" id="hid_synk_status" value="">
                <input type="hidden" name="order_trns_email" id="put_transfer_email" value="">

                <div>
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4">Customer Info</h5>
                            <div class="separator mb-5"></div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <div class="row m-0">
                                        <label for="file_upload" class="col-form-label col-sm-4">File Upload</label>
                                        <div class="col-sm-8">
                                            <input type="file" class="form-control" name="file_upload" id="file_upload">
                                            <p>Extension: pdf|doc|docx|xls|xlsx. File size: 2MB</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5>Order Details</h5>
                            <div class="separator mb-3"></div>
                            <div class="" id="cartItems">
                                <div class="table-responsive order_item_cart">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 offset-lg-7">
                        <div class="card mb-4">
                            <div class="card-body">
                                <table class="table table-bordered mb-4">
                                    <tr>
                                        <td>Sub Total (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" name="subtotal" id="subtotal" readonly=""
                                                class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Upcharge (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" id="upcharges_line" readonly=""
                                                class="form-control text-right"></td>
                                    </tr>
                                    
                                   <!--  <tr>
                                        <td>Installation Charge (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" name="install_charge" onchange="calculetsPrice()"
                                                onkeyup="calculetsPrice()" value="0" min="0" id="install_charge"
                                                step="any" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Other Charge (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" name="other_charge" onchange="calculetsPrice()"
                                                onkeyup="calculetsPrice()" value="0" min="0" id="other_charge"
                                                step="any" class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Misc (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" name="misc" onchange="calculetsPrice()"
                                                onkeyup="calculetsPrice()" value="0" id="misc" min="0" step="any"
                                                class="form-control text-right"></td>
                                    </tr>
                                    <tr>
                                        <td>Discount (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="decimal" name="invoice_discount" onchange="calculetsPrice()"
                                                onkeyup="calculetsPrice()" value="0" min="0" step="any"
                                                id="invoice_discount" class="form-control text-right"></td>
                                    </tr> -->
                                    <tr>
                                        <td>Grand Total (<?= $currencys[0]->currency; ?>)</td>
                                        <td><input type="number" name="grand_total" id="grand_total"
                                                class="form-control text-right" readonly="" required></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="order_status" id="order_status">
                <input type="hidden" name="transfer_email" id="transfer_email" value="0">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6 text-right">
                        <button type="submit" class="btn btn-success" id="gq">Submit</button>
                        <a href="<?= base_url(); ?>c_level/customer_order_controller/clear_cart" class="btn btn-danger"
                            id="clearCart"> Clear All</a>
                    </div>
                </div>
            </form>
            <!-- Final Cart Info : END -->

        </div>
    </div>

</main>


<!-- Add customer Modal -->
<div id="Add-New-Customer-Modal" class="modal fade" role="dialog" style="overflow-y: scroll; height: 800px;">
    <div class="modal-dialog modal-xl">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Customer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <script src="//code.jquery.com/jquery.min.js"></script>
                <div class="card-body">

                    <form id="add-customer-form" action="<?php echo base_url(); ?>customer-save" method="post"
                        enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="first_name">First Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="first_name" name="first_name"
                                    placeholder="John" onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="last_name">Last Name <span class="text-danger"> * </span></label>
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                    placeholder="Doe" onkeyup="required_validation()" required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email <span class="text-danger"> * </span></label>
                                <input type="email" class="form-control" id="email" onkeyup="check_email_keyup()"
                                    name="email" placeholder="johndoe@yahoo.com">
                                <span id="error"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem" style="margin-top: 25px;">
                                    <thead>
                                        <!--                            <tr>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Action </th>
                                        </tr>-->
                                    </thead>
                                    <tbody id="addItem">
                                        <tr>
                                            <td>
                                                <select class="form-control phone_type_select" id="phone_type_1"
                                                    name="phone_type[]" required>
                                                    <option value="Phone" selected>Phone</option>
                                                    <option value="Fax">Fax</option>
                                                    <option value="Mobile">Mobile</option>
                                                </select>
                                                <input id="phone_1"
                                                    class="phone form-control phone_no_type phone-format" type="text"
                                                    name="phone[]" placeholder="+1 (XXX) XXX-XXXX"
                                                    onkeyup="special_character(1)" style="">
                                            </td>
                                            <td class="text-left">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputField('addItem');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger" value="Delete"
                                                    onclick="deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="add-item" class=" btn-info pull-right" name="add-new-item"
                                    onclick="addInputField('addItem');" type="button"
                                    style="margin: 5px 15px 15px;"><span class="simple-icon-plus"></span></button>
                            </div>
                            <!--                                <div class="form-group col-md-6">
                                                                <label for="phone">Mobile No <span class="text-danger"> * </span></label>
                                                                <input type="text" class="form-control phone" id="phone" name="phone" placeholder="+1 (XXX)-XXX-XXXX" onkeyup="required_validation()" required>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="work_phone">Work Phone</label>
                                                                <input type="text" class="form-control phone" id="work_phone" name="work_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="home_phone">Home Phone</label>
                                                                <input type="text" class="form-control phone" id="home_phone" name="home_phone" placeholder="+1 (XXX)-XXX-XXXX">
                                                            </div>-->
                            <div class="form-group col-md-6">
                                <label for="address">Address <span class="text-danger"> * </span></label>


                                <input type="text" class="form-control" id="address" name="address" placeholder=""
                                    required autocomplete="off">

                            </div>
                            <div class="form-group col-md-6">
                                <label for="company">Company </label>
                                <input type="text" class="form-control" id="company" name="company"
                                    placeholder="ABC Tech">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="state">State</label>
                                <input type="text" class="form-control" id="state" name="state">
                            </div>
                            <!--                                <div class="form-group col-md-4">
                                                                <label for="state">State</label>
                                                                <select class="form-control select2" id="state" name="state">
                                                                    <option value="">-- select one -- </option>
                            <?php
                            foreach ($get_states as $states) {
                                echo "<option value='$states->state_id'>$states->state_name</option>";
                            }
                            ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="city">City</label>
                                                                <input type="text" class="form-control" id="city" name="city" placeholder="Ariana">
                                                                <select class="form-control select2" id="city" name="city" data-placeholder="-- select one --">
                                                                    <option value="">-- select one --</option>
                                                                </select>
                                                            </div>-->
                            <div class="form-group col-md-3">
                                <label for="zip_code">Zip</label>
                                <input type="text" class="form-control" id="zip_code" name="zip_code">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="country_code">Country Code</label>
                                <input type="text" class="form-control" id="country_code" name="country_code">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="reference">Referred By</label>
                                <input type="text" class="form-control" id="reference" name="reference"
                                    placeholder="John Clark">
                            </div>
                            <div class="form-group col-md-6">
                                <table class="" id="normalItem_file" style="margin-bottom: 5px; margin-top: 25px;">
                                    <!-- <thead>
                                        <tr>
                                            <th class="text-center">Files</th>
                                            <th class="text-center">Action </th>
                                        </tr>
                                    </thead>-->
                                    <tbody id="addItem_file">
                                        <tr id="1">
                                            <td>
                                                <input id="file_upload_1" class="form-control" type="file"
                                                    name="file_upload[]" multiple>
                                            </td>
                                            <td class="text-left" width="25%">
                                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                                <a style="font-size: 20px;" class="text-danger" value="Delete"
                                                    onclick="file_deleteRow(this)"><i class="simple-icon-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="add-item" class="btn-info pull-right" name="add-new-item"
                                    onclick="addInputFile('addItem_file');" type="button"
                                    style="margin: 0px 15px 15px;"><span class="simple-icon-plus"></span></button>
                                <!--<input id="add-item" class="btn btn-info pull-right" name="add-new-item" onclick="addInputFile('addItem_file');" value="Add New" type="button" style="margin: 0px 15px 15px;">-->
                                <span class="file_type_cls text-warning"> [ File size maximum 2 MB and
                                    jpg|png|jpeg|pdf|doc|docx|xls|xlsx types are allowed. ]</span>
                            </div>
                        </div>
                        <button type="submit"
                            class="btn btn-primary btn-sm d-block float-right customer_btn">Add</button>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
$(document).on('submit', '#myForm', function(e) {
    e.preventDefault();
    var formData = $(this).serialize();
});

function addCustomer() {
    $('#Add-New-Customer-Modal').modal('show');
    load_country_dropdown('phone_1', country_code_phone_format); // For phone format
    $('#Add-New-Customer-Modal .customer_btn').prop('disabled', false);
}

$(document).ready(function(){
    var places = new google.maps.places.Autocomplete(document.getElementById('address'));
    google.maps.event.addListener(places, 'place_changed', function() {
        var place = places.getPlace();
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: parseFloat(latitude),
            lng: parseFloat(longitude)
        };
        geocoder.geocode({
            'location': latlng
        }, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    var street = "";
                    var city = "";
                    var state = "";
                    var country = "";
                    var country_code = "";
                    var zipcode = "";
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].types[0] === "locality") {
                            city = results[i].address_components[0].long_name;
                            state = results[i].address_components[2].short_name;
                        }
                        if (results[i].types[0] === "postal_code" && zipcode == "") {
                            zipcode = results[i].address_components[0].long_name;
                        }
                        if (results[i].types[0] === "country") {
                            country = results[i].address_components[0].long_name;
                        }
                        if (results[i].types[0] === "country") {
                            country_code = results[i].address_components[0].short_name;
                        }
                        if (results[i].types[0] === "route" && street == "") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }
                        }
                        if (results[i].types[0] === "street_address") {
                            for (var j = 0; j < 4; j++) {
                                if (j == 0) {
                                    street = results[i].address_components[j].long_name;
                                } else {
                                    street += ", " + results[i].address_components[j].long_name;
                                }
                            }
                        }
                    }
                    if (zipcode == "") {
                        if (typeof results[0].address_components[8] !== 'undefined') {
                            zipcode = results[0].address_components[8].long_name;
                        }
                    }
                    if (country == "") {
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country = results[0].address_components[7].long_name;
                        }
                        if (typeof results[0].address_components[7] !== 'undefined') {
                            country_code = results[0].address_components[7].short_name;
                        }
                    }
                    if (state == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            state = results[0].address_components[5].short_name;
                        }
                    }
                    if (city == "") {
                        if (typeof results[0].address_components[5] !== 'undefined') {
                            city = results[0].address_components[5].long_name;
                        }
                    }
                    var address = {
                        "street": street,
                        "city": city,
                        "state": state,
                        "country": country,
                        "country_code": country_code,
                        "zipcode": zipcode,
                    };
                    $("#city").val(city);
                    $("#state").val(state);
                    $("#zip_code").val(zipcode);
                    $("#country_code").val(country_code);
                } else {
                    Swal.fire('No results found');
                }
            } else {
                Swal.fire('Geocoder failed due to: ' + status);
            }
        });
    });

    $('#add-customer-form').submit(function(event) {
        event.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: "<?php echo base_url(); ?>c_level/Quotation_controller/customer_save_new",
            data: formData,
        }).done(function(response) {
            if (response.msg == 'success') {
                //  $('#Add-New-Customer-Modal').modal();
                $('#Add-New-Customer-Modal').modal('hide');
                $("#add-customer-form")[0].reset();
                toastr.success("Customer info save successfully!");
                $.ajax({
                    type: 'GET',
                    url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_customers_opt",
                }).done(function(data) {
                    $('#customer_id').html('');
                    $('#customer_id').html(data);
                    $('select#customer_id').val(response.id);
                    $('select#customer_id').change();
                });
            } else {
                toastr.alert("Customer not saved successfully!");
            }
        });
        
    });
})

function special_character(t) {
    var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
    var check = function(string) {
        for (i = 0; i < specialChars.length; i++) {
            if (string.indexOf(specialChars[i]) > -1) {
                return true
            }
        }
        return false;
    }
    if (check($('#phone_' + t).val()) == false) {
        // Code that needs to execute when none of the above is in the string
    } else {
        Swal.fire(specialChars + " these special character are not allows");
        $("#phone_" + t).focus();
        $("#phone_" + t).val('');
    }
}

$('#address').on('keyup', function() {
    if ($(this).parent().find('pac-container') > 0) {} else {
        var pacContainer = $('.pac-container');
        $('#address').parent().append(pacContainer);
    }
});

function required_validation() {
    if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#address").val() !=
        '') {
        $('button[type=submit]').prop('disabled', false);
        return false;
    }
}

function check_email_keyup() {
    var email = $("#email").val();
    var email = encodeURIComponent(email);
    var data_string = "email=" + email;
    $.ajax({
        url: "<?php echo base_url(); ?>get-check-retailer-customer-unique-email",
        type: "post",
        data: data_string,
        success: function(data) {
            if (data != 0) {
                $("#error").html("This email already exists!");
                $("#error").css({
                    'color': 'red',
                    'font-weight': 'bold',
                    'display': 'block',
                    'margin-top': '5px'
                });
                $("#email").css({
                    'border': '2px solid red'
                }).focus();
                return false;
            } else {
                $("#error").hide();
                $("#email").css({
                    'border': '2px solid green'
                }).focus();
            }
        }
    });
}

$("body").on("change", "#file_upload_1", function(e) {
    var file = (this.files[0].name);
    var size = (this.files[0].size);
    var ext = file.substr((file.lastIndexOf('.') + 1));
    // check extention
    if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' &&
        ext !== 'pdf' && ext !== 'doc' && ext != 'docx' && ext !== 'xls' && ext !== 'xlsx') {
        Swal.fire(
        "Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
        $(this).val('');
    }
    // chec size
    if (size > 2000000) {
        Swal.fire("Please upload file less than 2MB. Thanks!!");
        $(this).val('');
    }
});

$("body").on("change", "#file_upload_2", function(e) {
    var file = (this.files[0].name);
    var size = (this.files[0].size);
    var ext = file.substr((file.lastIndexOf('.') + 1));
    // check extention
    if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' &&
        ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
        Swal.fire(
        "Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
        $(this).val('');
    }
    // chec size
    if (size > 2000000) {
        Swal.fire("Please upload file less than 2MB. Thanks!!");
        $(this).val('');
    }
});

$("body").on("change", "#file_upload_3", function(e) {
    var file = (this.files[0].name);
    var size = (this.files[0].size);
    var ext = file.substr((file.lastIndexOf('.') + 1));
    // check extention
    if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' &&
        ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
        Swal.fire(
        "Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
        $(this).val('');
    }
    // chec size
    if (size > 2000000) {
        Swal.fire("Please upload file less than 2MB. Thanks!!");
        $(this).val('');
    }
});

$("body").on("change", "#file_upload_4", function(e) {
    var file = (this.files[0].name);
    var size = (this.files[0].size);
    var ext = file.substr((file.lastIndexOf('.') + 1));
    // check extention
    if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' &&
        ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
        Swal.fire(
        "Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
        $(this).val('');
    }
    // chec size
    if (size > 2000000) {
        Swal.fire("Please upload file less than 2MB. Thanks!!");
        $(this).val('');
    }
});

//========== its for row add dynamically =============
function addInputField(t) {
    var row = $("#normalItem tbody tr").length;
    var count = row + 1;
    var limits = 4;
    if (count == limits) {
        Swal.fire("You have reached the limit of adding 3 inputs");
    } else {
        var a = "phone_type_" + count,
            e = document.createElement("tr");
        e.innerHTML = "\n\
                                     <td class='text-center'><select id='phone_type_" + count +
            "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value='Phone' selected>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" +
            count +
            "' class='phone form-control phone_no_type current_row phone-format' type='text' onkeyup='special_character(" +
            count + ")' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
            document.getElementById(t).appendChild(e),
            load_country_dropdown('phone_' + count, country_code_phone_format); // For phone format
        //  document.getElementById(a).focus(),
        count++;
        //=========== its for phone format when  add new ==============
        $('.phone').on('keypress', function(e) {
            var key = e.charCode || e.keyCode || 0;
            var phone = $(this);
            if (phone.val().length === 0) {
                phone.val(phone.val() + '+1 (');
            }
            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                //Swal.fire("D");
                if (phone.val().length === 6) {
                    phone.val(phone.val());
                    phone = phone;
                }
                if (phone.val().length === 7) {
                    phone.val(phone.val() + ') ');
                }
                if (phone.val().length === 12) {
                    phone.val(phone.val() + '-');
                }
                if (phone.val().length >= 17) {
                    phone.val(phone.val().slice(0, 16));
                }
            }
            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
        }).on('focus', function() {
            phone = $(this);
            if (phone.val().length === 0) {
                phone.val('+1 (');
            } else {
                var val = phone.val();
                phone.val('').val(val); // Ensure cursor remains at the end
            }
        }).on('blur', function() {
            $phone = $(this);
            if ($phone.val() === '(') {
                $phone.val('');
            }
        });
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            showOn: "focus",
        });
    }
}
//============= its for row delete dynamically =========
function deleteRow(t) {
    var a = $("#normalItem > tbody > tr").length;
    if (1 == a) {
        Swal.fire("There only one row you can't delete it.");
    } else {
        var e = t.parentNode.parentNode;
        e.parentNode.removeChild(e);
        var current_phone_type = 1;
        $("#normalItem > tbody > tr td select.current_phone_type").each(function() {
            current_phone_type++;
            $(this).attr('id', 'phone_type_' + current_phone_type);
            $(this).attr('name', 'phone_type[]');
        });
        var current_row = 1;
        $("#normalItem > tbody > tr td input.current_row").each(function() {
            current_row++;
            $(this).attr('id', 'phone_' + current_row);
            $(this).attr('name', 'phone[]');
        });
    }
}

//========== its for customer multiple file upload ==============
function addInputFile(t) {
    var table_row_count = $("#normalItem_file > tbody > tr").length;
    var file_count = table_row_count + 1;
    file_limits = 5;
    if (file_count == file_limits) {
        //   Swal.fire("You have reached the limit of adding " + file_count + " files");
        Swal.fire("You have reached the limit of adding 4 files");
    } else {
        //  Swal.fire(table_row_count);
        var a = "file_upload_" + file_count,
            e = document.createElement("tr");
        e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
            document.getElementById(t).appendChild(e),
            //   document.getElementById(a).focus(),
            file_count++;
    }
}

function file_deleteRow(t) {
    var a = $("#normalItem_file > tbody > tr").length;
    if (1 == a) {
        Swal.fire("There only one row you can't delete it.");
    } else {
        var e = t.parentNode.parentNode;
        e.parentNode.removeChild(e);
        var current_file_count = 1;
        $("#normalItem_file > tbody > tr td input.current_file_count").each(function() {
            current_file_count++;
            $(this).attr('class', 'form-control');
            $(this).attr('id', 'file_upload_' + current_file_count);
            $(this).attr('name', 'file_upload_' + current_file_count);
        });
    }
}

$('body').on('click', '.customer_btn', function() {
    if ($('#phone_type_1').val() == '' || $('#phone_1').val() == '') {
        Swal.fire("Please select phone type or no properly");
        return false;
    } else {
        $('button[type=submit]').prop('disabled', false);
    }
    if ($('#phone_type_2').val() == '' || $('#phone_2').val() == '') {
        Swal.fire("Please select phone type or no properly");
        return false;
    } else {
        $('button[type=submit]').prop('disabled', false);
    }
    if ($('#phone_type_3').val() == '' || $('#phone_3').val() == '') {
        Swal.fire("Please select phone type or no properly");
        return false;
    } else {
        $('button[type=submit]').prop('disabled', false);
    }
});


function load_add_order_form(rowid, act) {
    // rowid id of producst in cart, 0 = new blank form
    // act view = 0 , edit = 1
    var row_id = 0;
    var action = 0;
    if (rowid != '') {
        row_id = rowid;
    }
    if (act != '') {
        action = act;
    }
    $.ajax({
        url: "<?php echo base_url('c_level/customer_order_controller/get_order_form') ?>",
        type: 'get',
        data: {
            row_id: row_id,
            action: action
        },
        success: function(r) {
            $('#add_order_form').html(r);
        }
    });
}

load_add_order_form();
</script>
<?php $this->load->view($orderjs) ?>

<script>
    function put_prder_transfer_email(email)
    {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(mailformat))
        {
        }else{
            Swal.fire("You have entered an invalid email address!")
            $('#put_transfer_email').val('');
            return false;
        }
        $('#put_transfer_email').val(email);

    }

    function transfer_order() {
    var transfer_order = $('#transfer_order_and_email').is(":checked")

    if (transfer_order) {
        $('#transfer_email').val(1);
        $('.transfer_order_and_email_input').show();
    } else {
        $('#transfer_email').val(0);
        $('.transfer_order_and_email_input').hide();
    }

}
var popit = true;
window.onbeforeunload = function() {
    if (popit == true) {
        popit = false;
        return "Are you sure you want to leave?";
    }
}

window.addEventListener("beforeunload", function(event) {
    event.returnValue = "Are you sure you want to reload? current changes will not be saved";
});
</script>

<script>
$(document).ready(function () {
    order_to_wholesaler();
    });
</script>

