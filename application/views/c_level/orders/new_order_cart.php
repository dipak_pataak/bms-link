<?php $currency = $company_profile[0]->currency; ?>
<table class="datatable2 table table-bordered table-hover">
    <thead>
        <tr>
            <th style="width: 30px">SL No.</th>
            <th style="width: 250px">Name of Product</th>
            <th style="width: 120px">Room</th>
            <th style="width: 30px">Qty</th>
            <th style="width: 30px">Table Price</th>
            <th style="width: 30px">Discount (%)</th>
            <th style="width: 100px">List Price(<?= $currency ?>)</th>
            <th style="width: 30px">Upcharge Price</th>
            <th style="width: 100px">Price (<?= $currency ?>)</th>
            <th>Comments</th>
            <th style="width: 150px">Action</th>
        </tr>
    </thead>

    <tbody>
        <?php $i = 1; ?>

        <?php foreach ($this->cart->contents() as $items): ?>

        <input type="hidden" name="attributes[]" value='<?php echo $items['att_options']; ?>'>
        <input type="hidden" name="category_id[]" value='<?php echo $items['category_id']; ?>'>
        <input type="hidden" name="pattern_model_id[]" value='<?php echo $items['pattern_model_id']; ?>'>
        <input type="hidden" name="color_id[]" value='<?php echo $items['color_id']; ?>'>
        <input type="hidden" name="width[]" value='<?php echo $items['width']; ?>'>
        <input type="hidden" name="height[]" value='<?php echo $items['height']; ?>'>
        <input type="hidden" name="width_fraction_id[]" value='<?php echo $items['width_fraction_id']; ?>'>
        <input type="hidden" name="height_fraction_id[]" value='<?php echo $items['height_fraction_id']; ?>'>
        <input type="hidden" name="notes[]" value='<?php echo $items['notes']; ?>'>

        <input type="hidden" name="row_status[]" value="">
        <input type="hidden" name="room[]" value="<?php echo $items['room']; ?>">

        <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
        <tr>
            <td><?= $i ?></td>
            <td class="view_order_btn" data-rowid="<?php echo $items['rowid']; ?>" style="cursor: pointer; color:blue;">

                <input type="hidden" name="product_id[]" id="product_id_<?= $i ?>" class="product_id" value="<?= $items['product_id'] ?>">
                <strong><?= $items['name'] . ', '; ?></strong><br/>
                <?php
                $pat = $this->db->where('pattern_model_id', $items['pattern_model_id'])->get('pattern_model_tbl')->row();
                ?>
                <br>  <?php
                if (isset($pat->pattern_name)) {
                    echo $pat->pattern_name . ', ';
                }
                ?><br> 
                W <?= $items['width']; ?> <?php
                $hwf = $this->db->where('id', $items['width_fraction_id'])->get('width_height_fractions')->row();
                if (isset($hwf->fraction_value)) {
                    echo $hwf->fraction_value . '';
                }
                ?> X
                H <?= $items['height']; ?> <?php
                $hwf = $this->db->where('id', $items['height_fraction_id'])->get('width_height_fractions')->row();
                if (isset($hwf->fraction_value)) {
                    echo $hwf->fraction_value . '';
                }
                ?>,
                <?php
                $col = $this->db->where('id', $items['color_id'])->get('color_tbl')->row();
                ?>
                <?php
                if (isset($col->color_name)) {
                    echo $col->color_name . '';
                }
                ?>
            </td>
            <td>
                <?php
                if (isset($selected_rooms[$items['room']])) {
                    echo $items['room'] . ' ' . $selected_rooms[$items['room']];
                    $selected_rooms[$items['room']] ++;
                } else {
                    echo $items['room'] . ' 1';
                    $selected_rooms[$items['room']] = 2;
                }
                ?>
            </td>
            <td style="width: 150px;">
                <div id="field1">
                    <button type="button" id="sub" class="sub" >-</button>
                    <input type="number" name="qty[]"  onchange="customerWiseComission()" value="<?= $items['qty'] ?>" id="qty_<?= $i; ?>" min="1" class="qty_input" style="width: 40px;">
                    <button type="button" id="add" class="add">+</button>
                </div>
                <input type="hidden" value="<?= $i ?>">
            </td>
            <td><input type="hidden" id="h_w_price_<?= $i; ?>" class="h_w_price_loop" value="<?= $items['h_w_price'] ?>"> <?= $items['h_w_price'] ?></td>
            <td><input type="text" name="discount[]" value="" readonly="" id="discount_<?= $i ?>" class="form-control text-right"></td>
            <td>
                <input type="text" name="final_list_price[]" id="final_list_price_<?= $i; ?>" readonly="" class="form-control text-right">
                <input type="hidden" name="list_price[]" value="<?= $items['price'] ?>" id="list_price_<?= $i; ?>" readonly="" class="form-control text-right">
            </td>
            <td><input type="hidden" id="upcharge_price_<?= $i; ?>" class="upcharge_price_loop"  value="<?= $items['upcharge_price'] ?>"> <?= $items['upcharge_price'] ?></td>
            <td><input type="text" name="utprice[]" value="" id="utprice_<?= $i; ?>" class="form-control utprice text-right" readonly="" ></td>
            <td><?= $items['notes']; ?></td>
            <td>
                <a href="javascript:void(0)" onclick="load_add_order_form('<?= $items['rowid'] ?>', 0)" class="btn btn-warning default btn-xs copy-btn" ><i class="glyph-icon glyphicon-edit"></i>Copy</a>
                <a href="javascript:void(0)" onclick="load_add_order_form('<?= $items['rowid'] ?>', 1)" class="btn btn-info default btn-xs" ><i class="glyph-icon glyphicon-pencil"></i>Edit</a>

                <a href="javascript:void(0)" onclick="deleteCartItem('<?= $items['rowid'] ?>')" class="btn btn-danger default btn-xs" id="delete_cart_item" ><i class="glyph-icon simple-icon-trash"></i></a>
            </td>
        </tr>
        <?php $i++; ?>
    <?php endforeach; ?>
</tbody>
</table>
<!-- Modal -->
<div id="Modal-Manage-Order" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin: auto;">
        <div class="modal-content" style="width: 100%; box-shadow: 0px 0px 250px 250px rgba(0,0,0,0.25)">
            <div class="modal-header">
                <h4 class="modal-title"><span class="action_type"></span> Order</h4>
                <div>
                    <button type="button" class="close" onclick="$('#Modal-Manage-Order').modal('hide');">&times;</button>
                </div>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer" style="padding-top:20px!important;">
                <button type="button" class="btn btn-default" onclick="$('#Modal-Manage-Order').modal('hide');">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
$("body").on('click', '.view_order_btn', function() {
    var rowid = $(this).data('rowid');
    viewOrderHtml(rowid);
});


$("body").click(function(){
    if ($(event.target).attr('id') == "Modal-Cart") {
        $('#Modal-Manage-Order').modal('hide');
    }
});


function viewOrderHtml(rowid) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('c_level/Order_controller/getCartData') ?>",
        data: {
            rowid: rowid
        },
        success: function(data) {

            $('#Modal-Manage-Order .action_type').text('View');
            $('#Modal-Manage-Order .modal-body').html(data);
            $('#Modal-Manage-Order').modal('show');
        },
        error: function() {
            Swal.fire('error');
        }
    });
}
});
</script>