<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 262.594px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<?php $currency = $company_profile[0]->currency; ?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Purchase Quotation/Order</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Purchase Quotation/Order';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                         <?php
                            $user_data=access_role_permission(64);
                            $log_data=employ_role_access($id);
                                                                
                    ?>

                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Order</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="">
            <?php
            $error = $this->session->flashdata('error');
            $success = $this->session->flashdata('success');
            if ($error != '') {
                echo $error;
            }
            if ($success != '') {
                echo $success;
            }
            ?>

            <?php
            $message = $this->session->flashdata('message');

            if ($message != '') {
                echo $message;
            }
            ?>


        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">

                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
									 <th>Customer Invoice No</th>
                                        <th>
Order No </th>
                                        <th>Client Name </th>
                                        <th>Side mark</th>
                                        <th>Order date</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    if (!empty($orderd)) {

                                        foreach ($orderd as $key => $value) {

                                            $products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
                                           
                                            $attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();
                                            $order_return_info = $this->db->where('order_id', $value->order_id)->get('order_return_tbl')->row();
                                            /*  echo "<br>".$value->order_id.'<pre>';                                            print_r($order_return_info); */
 $blevel_order_id = $this->db->select('order_id')->where('clevel_order_id',$value->order_id)->get('b_level_quatation_tbl')->row();
                                            
                                            ?>
										<?php if(!empty($value->order_id) && !empty($blevel_order_id->order_id)){?>
                                            <tr style="">
 												<td><?= $value->order_id; ?></td>
                                                <td><?php 
                                                        if ($value->synk_status != 0) {
                                                           
                                                            echo @$orderid = @$blevel_order_id->order_id;
                                                        }
                                                    ?></td>
                                                <td><?= $value->customer_name; ?></td>
                                                <td><?= $value->side_mark; ?></td>
                                                <td><?= date_format(date_create($value->order_date), 'M-d-Y'); ?></td>
                                                <td><?= $currency; ?><?= $value->grand_total ?></td>
                                                <td>
                                                    <?php 
                                                    $is_approved = @$order_return_info->is_approved;
                                                    if(@$is_approved == 0){
                                                        echo "Pending";
                                                    }
                                                    if(@$is_approved == 1){
                                                        echo "Approved";
                                                    }
													
													 if(@$is_approved == 2){
                                                        echo "Cancel";
                                                    }
                                                    ?>
                                                </td>

                                                <?php if ($this->permission->check_label('manage_order')->update()->redirect()) { ?>

                                    <!--                                                    <td>
                                                                                            <select class="form-control select2-single" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')" data-placeholder="-- select one --">
                                                                                                <option value=""></option>
                                                                                                <option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
                                                                                                <option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
                                                                                                <option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
                                                                                                 <option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>Manufacturing</option> 
                                                                                                <option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping</option>
                                                                                                <option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
                                                                                                <option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>
                                                                                            </select>
                                                                                        </td>-->
                                                <?php } ?>

                                                <td>
                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>

                                                        <?php if ($value->order_stage == 17) { ?>
                                                           
                                                              <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                       <a href="retailer-customer-order-return/<?= $value->order_id ?>" class="btn btn-primary btn-sm" title="Return " data-toggle="tooltip" data-placement="top" data-original-title="Return"><i class="simple-icon-trophy" aria-hidden="true"></i></a>
                                                              <?php } ?>

                                                        <?php } ?>

                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->update()->access()) { ?>
                                                        <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){?>
                                                        <a href="<?php echo base_url('retailer-order-return-detials-show/'); ?><?php echo $value->order_id; ?>" class="btn btn-success btn-sm" title="View" data-toggle="tooltip" data-placement="top" data-original-title="View">
                                                            <i class="simple-icon-eye"></i>
                                                        </a>
                                                    <?php } ?>
                                                     <?php } ?>
                                                    <?php if ($this->permission->check_label('manage_order')->read()->access()) { ?>

                                    <!--                                                        <a href="<?= base_url('retailer-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-xs default" title="View">
                                                                                                <i class="simple-icon-eye"></i>
                                                                                            </a>-->

                                                    <?php } ?>

                                                    <?php if ($this->permission->check_label('manage_order')->delete()->access()) { ?>
                                                                                            <!--<a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>-->
                                                    <?php } ?>
                                                    <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){?>
                                                   <a href="<?php echo base_url(); ?>retailer-purchase-order-return/<?= $blevel_order_id->order_id ?>" class="btn btn-primary btn-sm" title="Return "><i class="simple-icon-trophy" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="Return"></i></a> 
                                                    <?php } ?>
                                                </td>


                                            </tr>
											<?php }?>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <?php if (empty($orderd)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th colspan="8" class="text-center text-danger">Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="cancel_comment_modal_info" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Comments</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="cancel_comment_info">
                <form class="" action="<?php echo base_url('c_level/Order_controller/cancel_comment'); ?>" method="post">
                    <textarea class="form-control" name="cancel_comment" required ></textarea>
                    <input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="stage_id" id="stage_id">
                    <input type="submit" class="btn btn-info" style="margin-top: 10px;">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function setOrderStage(stage_id, order_id) {
        if (stage_id != 6) {
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
                type: 'GET',
                success: function (r) {
                    toastr.success('Success! - Order Stage Set Successfully');
                    setTimeout(function () {
                        window.location.href = window.location.href;
                    }, 2000);
                }
            });
        } else {
            $("#order_id").val(order_id);
            $("#stage_id").val(stage_id);
            $("#cancel_comment_info").html();
            $('#cancel_comment_modal_info').modal('show');
        }

    }

</script>   
