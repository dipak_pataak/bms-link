<style type="text/css">
    select.form-control, .select2-dropdown, .select2-container--bootstrap .select2-selection.form-control {
        padding-left: 2px;
    }
    .theiaStickySidebar {
        background: #145388;
        padding: 0px !important;
    }
    #tprice {
        color: #fff;
    }
    .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control {
        margin-left: 34.6%;
        margin-bottom: 1rem;
    }
    .col-sm-3 + .col-sm-3 br+.col-sm-12 input+.form-control {
        margin-left: 0!important;
    }
    .form-group {
        margin: 0px;
    }
    .form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6 {
        padding-left: 0px;
    }
    .col-sm-3, label.col-sm-3 {	
        line-height:14px;
        padding-top:10px;
        margin-bottom: 0!important;
    }            
    .row {
        margin-bottom: 1rem;
        clear:both;
    }
    div.col-sm-3 + div.col-sm-3 {
        margin:0 !important;
    }
    br {
        display:none;
    }
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 958.55px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }

    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }

    /*--- for retailer --*/
    .col-sm-9 {
        padding-right: 0;
    }
    .col-sm-3 + .col-sm-3 {
        flex: 0 0 100% !important;
        max-width: 100% !important;
        clear: both;
        float: none;
        padding: 0;
    }
    .col-sm-3 .col-sm-12 {
        clear: both;
        padding: 0;
        margin: 0 -15px;
    }
    .col-sm-3 + .col-sm-3 .col-sm-12 label {
        width: 34.7%;
        float: left;
        padding: 0px 15px;
        line-height:14px;
        padding-top:10px;
    }
    .col-sm-3 + .col-sm-3 .col-sm-12 .form-control {
        width: 41.7%;
        float: left;
        margin-bottom: 1rem;
    }
    .card-body .col-sm-9 .col-sm-2 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 16.66667%;
        flex: 0 0 16.66667%;
        max-width: 16.66667%;
    }
    .card-body .col-sm-9 .col-sm-3 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.33%;
        flex: 0 0 33.33%;
        max-width: 33.33%;
    }
    .card-body .col-sm-9 .col-sm-3 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.33%;
        flex: 0 0 33.33%;
        max-width: 33.33%;
    }
    .card-body .col-sm-9 .col-sm-4, .card-body .col-sm-9 .col-sm-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 40%;
        flex: 0 0 40%;
        max-width: 40%;
        padding-right:0;
    }   
    #pattern_color_model .col-sm-1 {
        padding-right:0;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 14%;
        flex: 0 0 14%;
        max-width: 14%;
        line-height:14px;
        padding-top: 10px;
    }
    #pattern_color_model .col-sm-2 {
        padding:0;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 11%;
        flex: 0 0 11%;
        max-width: 11%;
    }
    @media only screen and (max-width: 480px) {
        .form-row {
            padding-right: 25px;
            margin-right:0;
        }
        .card-body .col-sm-9 .col-sm-3, #pattern_color_model .col-sm-1,.form-group.col-md-12 .row div.col-sm-4, .form-group.col-md-12 .row div.col-sm-6, #pattern_color_model .col-sm-2, .card-body .col-sm-9 .col-sm-4, .card-body .col-sm-9 .col-sm-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
        .col-sm-3 + .col-sm-3 .col-sm-12 label {
            width: 53.7%;
        }
        .col-sm-3 + .col-sm-3 br+.col-sm-12 .form-control{
            margin-left: 54%;
        }
        .col-sm-3 + .col-sm-3 .col-sm-12 .form-control {
            width: 46%;
        }
        .card-body .col-sm-9 .col-sm-2:last-child {
            margin-left:46.3%;
            margin-top: 1rem;
            -webkit-box-flex: 0;
            -ms-flex: 0 0 54%;
            flex: 0 0 54%;
            max-width: 54%;
            padding-right: 0;
        }
        #pattern_color_model .col-sm-1 {
            margin-top: 1rem;
        }
        #pattern_color_model .col-sm-2 {
            margin-left: 0;
        }
    }
</style>

<!-- add customer css-->
<style>
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-container{
        width: inherit !important;
        left: inherit  !important;
        top: inherit  !important;
        display: block  !important;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .phone-input{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .phone_type_select{
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top; background: #ddd;
    }
    .phone_no_type{
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }
    #normalItem tr td{
        border: none !important;
    }
    #addItem_file tr td{
        border: none !important;
    }
</style>
<?php
//print_r($get_product_order_info);
//print_r($selected_attributes);
//die();
?>
<!-- add customer css end-->
<main id="orderPage">
    <div class="container-fluid" style="margin-bottom:0;">

        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">New Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div style="margin-bottom:0;">

            <div>
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Edit Order </h5>
                        <div class="separator mb-5"></div>

                        <div class="row sticky_container">
                            <div class="col-sm-9">
                                <?= form_open_multipart('c_level/order_controller/save_order_update', array('id' => '')) ?>
                                <input type="hidden" name="quotation_details_row_id" value="<?php echo $get_product_order_info->row_id; ?>">
                                <input type="hidden" name="order_id" value="<?php echo $get_product_order_info->order_id; ?>">
                                <input type="hidden" name="tprice" id="tprice_id" value="<?php echo $get_product_order_info->unit_total_price; ?>">
                                <input type="hidden" name="discount"  value="<?php echo $get_product_order_info->discount; ?>">

                                <div class="form-row">

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Category</label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2-single" name="category_id" id="category_id"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($get_category as $category) {
                                                        echo "<option value='$category->category_id'>$category->category_name</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="subcategory_id">

                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Select Product</label>
                                            <div class="col-sm-4">
                                                <select class="form-control select2-single" name="product_id" id="product_id" onchange="getAttribute(this.value)" required="" data-placeholder="-- select one --">

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="color_model"></div>
                                    <div class="form-group col-md-12" id="pattern_color_model"></div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Width</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="width" class="form-control" id="width" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0"  required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="width_fraction_id" id="width_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                    foreach ($fractions as $f) {
                                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Height</label>
                                            <div class="col-sm-4">
                                                <input type="number" name="height" class="form-control" id="height" onChange="loadPStyle()" onKeyup="masked_two_decimal(this);loadPStyle()" min="0" required="" >
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="form-control " name="height_fraction_id" id="height_fraction_id" onKeyup="loadPStyle()" onChange="loadPStyle()" data-placeholder='-- select one --'>
                                                    <option value="">--Select one--</option>
                                                    <?php
                                                    foreach ($fractions as $f) {
                                                        echo "<option value='$f->id'>$f->fraction_value</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" id="ssssttt14"></div>

                                    <div class="form-group col-md-12">
                                    </div>

                                    <!-- attribute area -->
                                    <div class="form-group col-md-12" id="attr">

                                    </div>
                                    <!-- End attribute area -->


                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label for="" class="col-sm-3">Room</label>
                                            <div class="col-sm-4">
                                                <!--<select class="form-control select2-single" name="room" id="room"  required="" data-placeholder="--select one --">-->
                                                <select class="form-control" name="room" id="room"  required="" data-placeholder="--select one --">
                                                    <option value=""></option>
                                                    <?php
                                                    foreach ($rooms as $r) {
                                                        echo "<option value='$r->room_name'>$r->room_name</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="row">
                                            <label class="col-sm-3">Note</label>
                                            <div class="col-sm-4">
                                                <textarea class="form-control" name="notes" id="notes" rows="6"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <input type="hidden" name="total_price" id="total_price">
                                <div class="row">	
                                    <div class="col-lg-6 offset-lg-6 text-right">

                                        <!--<button type="submit" class="btn btn-success" id="gq" >Update</button>-->
                                        <?php
                                        //   echo $can_edit;
                                        if ($can_edit) {
                                            ?>
                                            <button type="submit" class="btn btn-success mb-0 float-right">Update Product</button>
                                        <?php } ?>
                                        <a href="<?php echo base_url('retailer-invoice-receipt/' . $order_id); ?>" class="btn btn-primary mb-0 float-right">Back</a>

                                        <!--<a href="<?= base_url(); ?>c_level/order_controller/clear_cart" class="btn btn-danger" id="clearCart"> Clear All</a>-->

                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>


                            <div class="col-sm-3 sticky_item">
                                <div class="fixed_item">
                                    <div id="tprice"></p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>

        <?php //$this->load->view($customerjs) ?>
</main>



<script type="text/javascript">
    $(document).on('submit', '#myForm', function (e) {
        e.preventDefault();
        var formData = $(this).serialize();
    });

    function addCustomer() {
        $('#Add-New-Customer-Modal').modal('show');
        load_country_dropdown('phone_1', country_code_phone_format); // For phone format
        $('#Add-New-Customer-Modal .customer_btn').prop('disabled', false);
    }

    $(document).ready(function(){
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            var geocoder = new google.maps.Geocoder;
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        var street = "";
                        var city = "";
                        var state = "";
                        var country = "";
                        var country_code = "";
                        var zipcode = "";
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                city = results[i].address_components[0].long_name;
                                state = results[i].address_components[2].short_name;
                            }
                            if (results[i].types[0] === "postal_code" && zipcode == "") {
                                zipcode = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country = results[i].address_components[0].long_name;
                            }
                            if (results[i].types[0] === "country") {
                                country_code = results[i].address_components[0].short_name;
                            }
                            if (results[i].types[0] === "route" && street == "") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                            if (results[i].types[0] === "street_address") {
                                for (var j = 0; j < 4; j++) {
                                    if (j == 0) {
                                        street = results[i].address_components[j].long_name;
                                    } else {
                                        street += ", " + results[i].address_components[j].long_name;
                                    }
                                }
                            }
                        }
                        if (zipcode == "") {
                            if (typeof results[0].address_components[8] !== 'undefined') {
                                zipcode = results[0].address_components[8].long_name;
                            }
                        }
                        if (country == "") {
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country = results[0].address_components[7].long_name;
                            }
                            if (typeof results[0].address_components[7] !== 'undefined') {
                                country_code = results[0].address_components[7].short_name;
                            }
                        }
                        if (state == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                state = results[0].address_components[5].short_name;
                            }
                        }
                        if (city == "") {
                            if (typeof results[0].address_components[5] !== 'undefined') {
                                city = results[0].address_components[5].long_name;
                            }
                        }
                        var address = {
                            "street": street,
                            "city": city,
                            "state": state,
                            "country": country,
                            "country_code": country_code,
                            "zipcode": zipcode,
                        };
                        $("#city").val(city);
                        $("#state").val(state);
                        $("#zip_code").val(zipcode);
                        $("#country_code").val(country_code);
                    } else {
                        Swal.fire('No results found');
                    }
                } else {
                    Swal.fire('Geocoder failed due to: ' + status);
                }
            });
        });

        $('#add-customer-form').submit(function(event) {
            event.preventDefault();
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: "<?php echo base_url(); ?>c_level/Quotation_controller/customer_save_new",
                data: formData,
            }).done(function(response) {
                if (response.msg == 'success') {
                    //  $('#Add-New-Customer-Modal').modal();
                    $('#Add-New-Customer-Modal').modal('hide');
                    $("#add-customer-form")[0].reset();
                    toastr.success("Customer info save successfully!");
                    $.ajax({
                        type: 'GET',
                        url: "<?php echo base_url(); ?>c_level/Quotation_controller/get_customers_opt",
                    }).done(function(data) {
                        $('#customer_id').html('');
                        $('#customer_id').html(data);
                        $('select#customer_id').val(response.id);
                        $('select#customer_id').change();
                    });
                } else {
                    toastr.alert("Customer not saved successfully!");
                }
            });
            
        });
    })

    function special_character(t) {
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            Swal.fire(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }

    $('#address').on('keyup', function () {
        if ($(this).parent().find('pac-container') > 0) {
        } else {
            var pacContainer = $('.pac-container');
            $('#address').parent().append(pacContainer);
        }
    });

    function required_validation() {
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#phone").val() != '' && $("#address").val() != '') {
            $('button[type=submit]').prop('disabled', false);
            return false;
        }
    }

    function check_email_keyup() {
        var email = $("#email").val();
        var email = encodeURIComponent(email);
        var data_string = "email=" + email;
        $.ajax({
            url: "<?php echo base_url(); ?>get-check-retailer-customer-unique-email",
            type: "post",
            data: data_string,
            success: function (data) {
                if (data != 0) {
                    $("#error").html("This email already exists!");
                    $("#error").css({
                        'color': 'red',
                        'font-weight': 'bold',
                        'display': 'block',
                        'margin-top': '5px'
                    });
                    $("#email").css({
                        'border': '2px solid red'
                    }).focus();
                    return false;
                } else {
                    $("#error").hide();
                    $("#email").css({
                        'border': '2px solid green'
                    }).focus();
                }
            }
        });
    }
    $("body").on("change", "#file_upload_1", function (e) {
        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext != 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            Swal.fire("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            Swal.fire("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }
    });
    $("body").on("change", "#file_upload_2", function (e) {
        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            Swal.fire("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            Swal.fire("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }
    });
    $("body").on("change", "#file_upload_3", function (e) {
        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            Swal.fire("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            Swal.fire("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }
    });
    $("body").on("change", "#file_upload_4", function (e) {
        var file = (this.files[0].name);
        var size = (this.files[0].size);
        var ext = file.substr((file.lastIndexOf('.') + 1));
        // check extention
        if (ext !== 'jpg' && ext !== 'JPG' && ext !== 'png' && ext !== 'PNG' && ext !== 'jpeg' && ext !== 'JPEG' && ext !== 'pdf' && ext !== 'doc' && ext !== 'docx' && ext !== 'xls' && ext !== 'xlsx') {
            Swal.fire("Please upload file jpg | png | jpeg | pdf | doc | docx | xls | xlsx types are allowed. Thanks!!");
            $(this).val('');
        }
        // chec size
        if (size > 2000000) {
            Swal.fire("Please upload file less than 2MB. Thanks!!");
            $(this).val('');
        }
    });
//    ========== its for row add dynamically =============
    function addInputField(t) {
        var row = $("#normalItem tbody tr").length;
        var count = row + 1;
        var limits = 4;
        if (count == limits) {
            Swal.fire("You have reached the limit of adding 3 inputs");
        } else {
            var a = "phone_type_" + count,
                    e = document.createElement("tr");
            e.innerHTML = "\n\
                                     <td class='text-center'><select id='phone_type_" + count + "' class='phone form-control phone_type_select  current_phone_type' name='phone_type[]' required><option value='Phone' selected>Phone</option><option value='Fax'>Fax</option><option value='Mobile'>Mobile</option></select><input id='phone_" + count + "' class='phone form-control phone_no_type current_row' type='text' onkeyup='special_character(" + count + ")' name='phone[]' placeholder='+1 (XXX) XXX-XXXX'></td>\n\
                                    <td class='text-left' ><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
                    //  document.getElementById(a).focus(),
                    count++;
            //=========== its for phone format when  add new ==============
            $('.phone').on('keypress', function (e) {
                var key = e.charCode || e.keyCode || 0;
                var phone = $(this);
                if (phone.val().length === 0) {
                    phone.val(phone.val() + '+1 (');
                }
                // Auto-format- do not expose the mask as the user begins to type
                if (key !== 8 && key !== 9) {
                    //Swal.fire("D");
                    if (phone.val().length === 6) {
                        phone.val(phone.val());
                        phone = phone;
                    }
                    if (phone.val().length === 7) {
                        phone.val(phone.val() + ') ');
                    }
                    if (phone.val().length === 12) {
                        phone.val(phone.val() + '-');
                    }
                    if (phone.val().length >= 17) {
                        phone.val(phone.val().slice(0, 16));
                    }
                }
                // Allow numeric (and tab, backspace, delete) keys only
                return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
            }).on('focus', function () {
                phone = $(this);
                if (phone.val().length === 0) {
                    phone.val('+1 (');
                } else {
                    var val = phone.val();
                    phone.val('').val(val); // Ensure cursor remains at the end
                }
            }).on('blur', function () {
                $phone = $(this);
                if ($phone.val() === '(') {
                    $phone.val('');
                }
            });
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
        }
    }
//    ============= its for row delete dynamically =========
    function deleteRow(t) {
        var a = $("#normalItem > tbody > tr").length;
        if (1 == a) {
            Swal.fire("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_phone_type = 1;
            $("#normalItem > tbody > tr td select.current_phone_type").each(function () {
                current_phone_type++;
                $(this).attr('id', 'phone_type_' + current_phone_type);
                $(this).attr('name', 'phone_type[]');
            });
            var current_row = 1;
            $("#normalItem > tbody > tr td input.current_row").each(function () {
                current_row++;
                $(this).attr('id', 'phone_' + current_row);
                $(this).attr('name', 'phone[]');
            });
        }
    }
//    ======== close ===========
//========== its for customer multiple file upload ==============
    function addInputFile(t) {
        var table_row_count = $("#normalItem_file > tbody > tr").length;
        var file_count = table_row_count + 1;
        file_limits = 5;
        if (file_count == file_limits) {
            //   Swal.fire("You have reached the limit of adding " + file_count + " files");
            Swal.fire("You have reached the limit of adding 4 files");
        } else {
            //  Swal.fire(table_row_count);
            var a = "file_upload_" + file_count,
                    e = document.createElement("tr");
            e.innerHTML = "<td class='text-left'><input id='file_upload_" + file_count + "' class='current_file_count form-control' type='file' name='file_upload[]' multiple></td>\n\
                                    <td class='text-left'><a style='font-size: 20px;' class='text-danger' value='Delete' onclick='file_deleteRow(this)'><i class='simple-icon-trash'></i></a></td>\n\
                                    ",
                    document.getElementById(t).appendChild(e),
                    //   document.getElementById(a).focus(),
                    file_count++;
        }
    }

    function file_deleteRow(t) {
        var a = $("#normalItem_file > tbody > tr").length;
        if (1 == a) {
            Swal.fire("There only one row you can't delete it.");
        } else {
            var e = t.parentNode.parentNode;
            e.parentNode.removeChild(e);
            var current_file_count = 1;
            $("#normalItem_file > tbody > tr td input.current_file_count").each(function () {
                current_file_count++;
                $(this).attr('class', 'form-control');
                $(this).attr('id', 'file_upload_' + current_file_count);
                $(this).attr('name', 'file_upload_' + current_file_count);
            });
        }
    }
    $('body').on('click', '.customer_btn', function () {
        if ($('#phone_type_1').val() == '' || $('#phone_1').val() == '') {
            Swal.fire("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_2').val() == '' || $('#phone_2').val() == '') {
            Swal.fire("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
        if ($('#phone_type_3').val() == '' || $('#phone_3').val() == '') {
            Swal.fire("Please select phone type or no properly");
            return false;
        } else {
            $('button[type=submit]').prop('disabled', false);
        }
    });


</script>
<!-- end content / right -->
<script type="text/javascript">

    var var_currency = '<?= $currency; ?>';

    function multiOptionPriceValue(att_op_op_op_op_id) {

        var op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var att = att_op_op_op_op_id.split("_")[1];
        var op_op_id = att_op_op_op_op_id.split("_")[2];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_id) {

            var wrapper = $("#mul_op_op_id" + op_op_id).parent().next();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/multioption_price_value/') ?>" + op_op_op_id + "/" + att + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }

        cal();
    }



    function OptionFive(att_op_op_op_op_id, attribute_id) {

        var op_op_op_op_id = att_op_op_op_op_id.split("_")[0];
        var op_op_op_id = att_op_op_op_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_op_op_id) {

            var wrapper = $("#op_op_op_" + op_op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_attr_op_five/') ?>" + op_op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {
                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOptionOption(pro_att_op_id, attribute_id) {

        var op_op_op_id = pro_att_op_id.split("_")[0];
        var op_op_id = pro_att_op_id.split("_")[1];
        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {
            var main_price = 0;
        } else {
            var main_price = main_p;
        }

        if (op_op_id) {

            var wrapper = $("#op_op_" + op_op_id).next().next();
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_attr_op_op_op_op/') ?>" + op_op_op_id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });
        }
        cal();
    }


    function OptionOptionsOption(pro_att_op_id, attribute_id) {


        var op_op_id = pro_att_op_id.split("_")[0];

        var id = pro_att_op_id.split("_")[1];

        var op_id = pro_att_op_id.split("_")[2];

        var main_p = parseFloat($('#main_price').val());

        if (isNaN(main_p)) {

            var main_price = 0;

        } else {

            var main_price = main_p;

        }

        if (op_op_id) {

            var wrapper = $("#op_" + op_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_attr_op_op_op/') ?>" + op_op_id + "/" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                }
            });

        }

        cal();

    }



    function OptionOptions(pro_att_op_id, attribute_id) {

        if (pro_att_op_id) {
            //id
            var id = pro_att_op_id.split("_")[0];
            //option_id
            var optin_id = pro_att_op_id.split("_")[1];
            //mainprice
            var main_p = parseFloat($('#main_price').val());

            if (isNaN(main_p)) {

                var main_price = 0;

            } else {

                var main_price = main_p;

            }

            var wrapper = $(".options_" + attribute_id).parent().next().next();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_attr_option_option/') ?>" + id + "/" + attribute_id + "/" + main_price,
                type: 'get',
                success: function (r) {

                    $(wrapper).html(r);
                    // console.log('here');

<?php
foreach ($selected_attributes as $records) {
    // echo 'here ran<br>';
    if ($records->attribute_id == 1) {
        $opop_val1 = $records->opop[0]->op_op_value;
        $opop_val2 = $records->opop[1]->op_op_value;
        $opop_val3 = $records->opop[2]->op_op_value;
        ?>
                            // $('input[name="op_op_value_1[]"]').val("<?php // echo $records->opop[0]->option_key_value;             ?>");
                            var i = 0;
                            $('input[name="op_op_value_1[]"]').each(function () {
                                if (i == 0) {
                                    $(this).val("<?php echo $opop_val1; ?>");
                                }
                                if (i == 1) {
                                    $(this).val("<?php echo $opop_val2; ?>");
                                }
                                if (i == 2) {
                                    $(this).val("<?php echo $opop_val3; ?>");
                                }

                                i++;
                            });

        <?php
    }
    if ($records->attribute_id == 14) {
        if (isset($records->opop[0]) && $records->opop[0]->op_op_value != '') {
            $opop_val1 = explode(" ", $records->opop[0]->op_op_value);
        }
        if (isset($records->opop[1]) && $records->opop[1]->op_op_value != '') {
            $opop_val2 = explode(" ", $records->opop[1]->op_op_value);
        }
        if (isset($records->opop[2]) && $records->opop[2]->op_op_value != '') {
            $opop_val3 = explode(" ", $records->opop[2]->op_op_value);
        }
        ?>
                            // $('input[name="op_op_value_1[]"]').val("<?php // echo $records->opop[0]->option_key_value;             ?>");
                            var i = 0;
                            $('input[name="op_op_value_14[]"]').each(function () {
                                if (i == 0) {
                                    $(this).val("<?php echo $opop_val1[0]; ?>");
                                    $('select[name="fraction_14[]"]').val("<?php echo $opop_val1[0]; ?>");

                                }
                                if (i == 1) {
                                    $(this).val("<?php echo $opop_val2[0]; ?>");
                                }
                                if (i == 2) {
                                    $(this).val("<?php echo $opop_val3[0]; ?>");
                                }

                                i++;
                            });

                            var i = 0;
                            $('select[name="fraction_14[]"]').each(function () {
                                if (i == 0) {
                                    $(this).val("<?php echo $opop_val1[1]; ?>");
                                }
                                if (i == 1) {
                                    $(this).val("<?php echo $opop_val2[1]; ?>");
                                }
                                if (i == 2) {
                                    $(this).val("<?php echo $opop_val3[1]; ?>");
                                }

                                i++;
                            });

        <?php
    }
}
?>
                }
            });

        } else {

        }

        cal();

    }


    $('body').on('click', '#row', function () {
        cal();

        $('#width').val($('#width').val().split(".")[0]);
        $('#height').val($('#height').val().split(".")[0]);

    });


    function cal() {

        var w = $('#width').val();
        var h = $('#height').val();

        if (w !== '' && h !== '') {

            var contribut_price = 0;
            $(".contri_price").each(function () {
                isNaN(this.value) || 0 == this.value.length || (contribut_price += parseFloat(this.value))
            });
            var main_price = parseFloat($('#main_price').val());

            if (isNaN(main_price)) {
                var prc = 0;
            } else {
                var prc = main_price;
            }

            var total_price = (contribut_price + prc);
            var t = (isNaN(total_price) ? 0 : total_price);

            $("#total_price").val(t.toFixed(2));
            // $("#tprice").text("Total Price = " + var_currency + t);
            $("#tprice").text("");
            $('#tprice_id').val(t);
        }

    }



//width fraction get 
    $('#width').on('keyup', function () {

        var hif = $(this).val().split(".")[1];

        if (hif) {

            $.ajax({

                url: "<?php echo base_url('c_level/order_controller/get_height_width_fraction/') ?>" + hif,
                type: 'get',
                success: function (r) {
                    $("#width_fraction_id").val(r);
                }

            });

        } else {

            $("#width_fraction_id").val('');
        }

    });
//--end---


// Height fraction 
    $('#height').on('keyup', function () {

        var hif = $(this).val().split(".")[1];
        if (hif) {

            $.ajax({

                url: "<?php echo base_url('c_level/order_controller/get_height_width_fraction/') ?>" + hif,
                type: 'get',
                success: function (r) {
                    $("#height_fraction_id").val(r);
                }
            });

        } else {

            $("#height_fraction_id").val('');
        }

    });
//---End----




    function loadPStyle() {

        var product_id = $('#product_id').val();

        var pattern_id = $('#pattern_id').val();

        var pricestyle = $('#pricestyle').val();

        var price = $("#height").parent().parent().parent().next();



        var hif = ($("#height_fraction_id :selected").text().split("/")[0] / $("#height_fraction_id :selected").text().split("/")[1]);

        var wif = ($("#width_fraction_id :selected").text().split("/")[0] / $("#width_fraction_id :selected").text().split("/")[1]);

        var width = parseFloat($('#width').val()) + (isNaN(wif) ? 0 : wif);

        var height = parseFloat($('#height').val()) + (isNaN(hif) ? 0 : hif);

        var width_w = (isNaN(width) ? '' : width);
        var height_w = (isNaN(height) ? '' : height);

        if (pricestyle !== '2') {

            // IF category Arch then assign height 0 : START
            var category_name = $("#category_id :selected").text();
            if(height_w == '' && category_name == 'Arch'){
                height_w = 0;
                $('#height').val(0);
            }
            // IF category Arch then assign height 0 : END
            
            $.ajax({

                url: "<?php echo base_url('c_level/order_controller/get_product_row_col_price/') ?>" + height_w + "/" + width_w + "/" + product_id + "/" + pattern_id,

                type: 'get',

                success: function (r) {

                    var obj = jQuery.parseJSON(r);
                    $(price).html(obj.ht);

                    if (isNaN(parseFloat(obj.prince))) {

                        // $("#tprice").text("Total Price = $0");
                        $("#tprice").text("");
                        $('#tprice_id').val(0);

                    } else {

                        // $("#tprice").text("Total Price = $" + parseFloat(obj.prince));
                        $("#tprice").text("");
                        $('#tprice_id').val(parseFloat(obj.prince));

                    }

                    // $("#height").val(obj.col);
                    if (obj.st === 1) {

                        $('#cartbtn').removeAttr('disabled');

                    } else if (obj.st === 2) {
                        //$('#cartbtn').prop('disabled', true);
                    }

                    cal();
                }
            });

        }

        if (pricestyle === '2') {

            var main_p = parseFloat($('#sqr_price').val());

            if (isNaN(main_p)) {
                var main_price = 0;
            } else {
                var main_price = main_p;
            }

            var new_width = parseFloat(width + 3);
            var new_height = parseFloat(height + 1.5);

            var sum = (new_width * new_height) / 144;
            // var sum = (width * height) / 144;
            var price = main_price * sum;

            $('#main_price').val(price.toFixed(2));

            cal();

        }
        //---End--

        cal();

    }


    $(document).ready(function () {

        //---------------------------
        // Get attribute  by product id
        // --------------------------

        $('body').on('change', '#product_id', function () {

            var product_id = $(this).val();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_to_attribute/') ?>" + product_id,
                type: 'get',
                success: function (r) {

                    $("#attr").html(r);
                    callTrigger();
                    $('#cartbtn').removeAttr('disabled');

                }
            });

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_color_partan_model/') ?>" + product_id,
                type: 'get',
                success: function (r) {

                    $('#color_model').html(r);
                    $('body #pattern_id').val("<?php echo $get_product_order_info->pattern_model_id; ?>");
                    $('#pattern_id').trigger('change');



                    // select attributes
                    // do code here to select attributes by default

<?php
$i = 1;
foreach ($selected_attributes as $records) {
    ?>
                        $('body .options_<?php echo $i; ?>').val("<?php echo $records->options[0]->option_key_value; ?>");
                        OptionOptions("<?php echo $records->options[0]->option_key_value; ?>", <?php echo $i; ?>);
    <?php
    $i++;

    //  if ($records->attribute_id == 1) {
    // multi blinds dp value
    ?>
                        //                 $('body .options_1').val("<?php // echo $records->options[0]->option_key_value;            ?>");
                        //                 OptionOptions("<?php // echo $records->options[0]->option_key_value;            ?>", 1);
    <?php
//}
// if ($records->attribute_id == 2) {
//  mount
    ?>
                        //     $('body .options_2').val("<?php // echo $records->options[0]->option_key_value;            ?>");
                        //       OptionOptions("<?php // echo $records->options[0]->option_key_value;            ?>", 2);
    <?php
//  }
}
?>



                }
            });
        });
        $('body').on('change', '#pattern_id', function () {

            var pattern_id = $(this).val();
            var product_id = $('#product_id').val();

            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_color_model/') ?>" + product_id + "/" + pattern_id,
                type: 'get',
                success: function (r) {

                    $('#pattern_color_model').html(r);
                    $('body #color_id').val("<?php echo $get_product_order_info->color_id; ?>");
                    //   $('body #color_id').trigger('change');
                    getColorCode("<?php echo $get_product_order_info->color_id; ?>");
                }
            });
        });

        //---------------------------
        // End
        // --------------------------



        $("#shipaddress").click(function () {
            $(".ship_addr").slideToggle();
        });


        $.ajax({
            url: "order-id-generate",
            type: 'get',
            success: function (r) {
                $("#orderid").val(r);
            }
        });


        //================== its for category wise subcategory show ======================
        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/category_wise_subcategory/') ?>" + category_id,
                type: 'get',
                success: function (r) {
                    $("#subcategory_id").html(r);
                }
            });
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_cat_fraction/') ?>" + category_id,
                type: 'get',
                dataType: 'JSON',
                success: function (r) {
                    $("#width_fraction_id").html('');
                    $("#width_fraction_id").html(r.html);

                    $("#height_fraction_id").html('');
                    $("#height_fraction_id").html(r.html);

                    $('#width_fraction_id').val("<?php echo $get_product_order_info->width_fraction_id; ?>");
                    $('#height_fraction_id').val("<?php echo $get_product_order_info->height_fraction_id; ?>");
                    // loadPStyle();
                    //     $('#tprice').val("<?php echo $get_product_order_info->unit_total_price; ?>");
                    // $("#tprice").text("Total Price = " + var_currency + "<?php echo $get_product_order_info->unit_total_price; ?>");
                    $('#tprice_id').val(<?php echo $get_product_order_info->unit_total_price; ?>);
                }
            });

        });


//---------------------------
// Get product  by category id
// --------------------------

        $('body').on('change', '#category_id', function () {
            var category_id = $(this).val();

            $("#total_price").val(0);
            $("#main_price").val(0);
            $("#product_id").val('');

            // $("#tprice").text("Total Price = " + var_currency + "0");
            $("#attr").load(location.href + " #attr>*", "");

            loadPStyle();


            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_by_category/') ?>" + category_id,
                type: 'get',
                success: function (r) {

                    $("#product_id").html(r);
                    $('body #product_id').select2("val", "<?php echo $get_product_order_info->product_id; ?>");
                    //   pattern_id
                    $('#height').trigger('change');

                }
            });
        });

//---------------------------
// Get sub_category  by category id
// --------------------------

        $('body').on('change', '#sub_category_id', function () {
            var sub_category_id = $(this).val();
            $.ajax({
                url: "<?php echo base_url('c_level/order_controller/get_product_by_subcategory/') ?>" + sub_category_id,
                type: 'get',
                success: function (r) {
                    $("#product_id").html(r);
                }
            });
        });



//---------------------------
// Get customer-wise-sidemark
// --------------------------

        $("body").on('change', '#customer_id', function () {

            var customer_id = $(this).val();

            $.ajax({

                url: "<?php echo base_url('c_level/Order_controller/customer_wise_sidemark/') ?>" + customer_id,
                type: 'get',
                success: function (data) {


                    if (data == 0) {

                        $("#side_mark").val("None");

                    } else {

                        var obj = jQuery.parseJSON(data);
                        var tax = (obj.tax_rate != null ? obj.tax_rate : 0);

                        $('#side_mark').val(obj.side_mark);
                        $('#tax').val(tax);

                        if (obj.side_mark) {
                            $('#side_mark').val(obj.side_mark);
                        }

                        $('#customertype').val(obj.customer_type);

                        customerWiseComission();
                    }
                }

            });

        });


        $("body").on('click', '#gq', function () {
            $('#order_status').val(1);
        });

        $("body").on('click', '#gqi', function () {
            $('#order_status').val(2);
        });


    });





    function callTrigger() {

        $('.op_op_load').trigger('change');
        $('.op_op_op_load').trigger('change');
        $('.op_op_op_op_load').trigger('change');
        $('.op_op_op_op_op_load').trigger('change');
    }



//---------------------------
// submit form and add data
// --------------------------

    // submit form and add data
    $("#AddToCart").on('submit', function (e) {

        e.preventDefault();

        var submit_url = "<?php echo base_url('c_level/order_controller/add_to_cart') ?>";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                window.location.reload();
                toastr.success('Success! - Product add to cart ');
                setTimeout(function () {
                    $("#cartItems").load(location.href + " #cartItems>*", "");
                }, 2000);

            }, error: function () {
                Swal.fire('error');
            }
        });

    });



//---------------------------
// submit to cleare cart 
// --------------------------

    // submit form and add data
    $("#clearCart").on('click', function (e) {

        e.preventDefault();

        var submit_url = "<?= base_url(); ?>c_level/order_controller/clear_cart_new";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                Swal.fire('error');
            }
        });
    });




//---------------------------
// get color code by color id
// --------------------------

    function getColorCode(id) {

        var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code/" + id;

        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {

                $('#colorcode').val(res);

            }, error: function () {
                Swal.fire('error');
            }
        });

    }





    function getColorCode_select(keyword) {
        var pattern_id = $("#pattern_id").val();
        if (keyword !== '') {
            
            var submit_url = "<?= base_url(); ?>c_level/order_controller/get_color_code_select/" + keyword + '/'+ pattern_id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {
                    $('#color_id').val(res);

                }, error: function () {
                    Swal.fire('error');
                }
            });
        }

    }



//---------------------------
// Delete cart item
// --------------------------

    function deleteCartItem(id) {

        var submit_url = "<?= base_url(); ?>c_level/order_controller/delete_cart_item/" + id;

        $.ajax({
            type: 'GET',
            url: submit_url,
            success: function (res) {

                $("#cartItems").load(location.href + " #cartItems>*", "");

            }, error: function () {
                Swal.fire('error');
            }
        });

    }



//---------------------------
// customer Wise Comission
// --------------------------

    function customerWiseComission() {

        var customertype = $('#customertype').val();

        if (customertype == '') {

            Swal.fire('Please select customer');
            $("#customer_id").focus();

        } else {

            var i = 1;

            $(".product_id").each(function () {

                var productid = (this.value);
                var customer_id = $('#customer_id').val();

                var submit_url = "<?= base_url(); ?>c_level/order_controller/getproductcomission_new/" + productid + '/' + customer_id;

                $.ajax({
                    type: 'GET',
                    url: submit_url,
                    success: function (res) {

                        var obj = jQuery.parseJSON(res);

                        var qty = $('#qty_' + i).val();

                        var list_price = parseFloat($('#list_price_' + i).val());
                        var total_list_price = (list_price * qty);


                        //if (customertype === 'business') {

                        $('#discount_' + i).val(obj.dealer_price);

                        var discount = (total_list_price * obj.dealer_price) / 100;

                        // } else {

                        //     $('#discount_' + i).val(obj.individual_price);
                        //     var discount = (list_price * obj.individual_price) / 100;

                        // }

                        $('#utprice_' + i).val(total_list_price - discount.toFixed(2));

                        calculetsPrice();

                        i++;

                    }, error: function () {
                        Swal.fire('error');
                    }
                });



            });
        }

    }




    function calculetsPrice() {

        var subtotal = 0;
        $(".utprice").each(function () {
            isNaN(this.value) || 0 == this.value.length || (subtotal += parseFloat(this.value))
        });


        $('#subtotal').val(subtotal.toFixed(2));

        //var taxs = parseFloat($('#tax').val());
        //var tax = (subtotal*taxs)/100;
        var install_charge = parseFloat($('#install_charge').val());
        var other_charge = parseFloat($('#other_charge').val());
        var invoice_discount = parseFloat($('#invoice_discount').val());
        var misc = parseFloat($('#misc').val());


        // var install_charge  = $('#install_charge').val().split(".")[1];
        // if (install_charge.length > 2 ){
        //     $('#install_charge').val(parseFloat($('#install_charge').val()).toFixed(2));
        // }


        // var other_charge  = $('#other_charge').val().split(".")[1];
        // if (other_charge.length > 2 ){
        //     $('#other_charge').val(parseFloat($('#other_charge').val()).toFixed(2));
        // }


        // var invoice_discount  = $('#invoice_discount').val().split(".")[1];
        // if (invoice_discount.length > 2 ){
        //     $('#invoice_discount').val(parseFloat($('#invoice_discount').val()).toFixed(2));
        // }


        // var misc  = $('#misc').val().split(".")[1];
        // if (misc.length > 2 ){
        //     $('#misc').val(parseFloat($('#misc').val()).toFixed(2));
        // }


        //var grandtotal = (subtotal + install_charge + other_charge + misc);
        var grandtotal = (subtotal);
        var gtotal = grandtotal - invoice_discount;
        $('#grand_total').val(gtotal.toFixed(2));
        calDuePaid();
    }



    function calDuePaid() {

        var grand_total = parseFloat($('#grand_total').val());

        var paid_amount = parseFloat($('#paid_amount').val());
        var due = (grand_total - paid_amount);
        $('#due').val(due.toFixed(2));

    }



    $('#card_area').hide();
    $('#card_area2').hide();

    function setCard(value) {

        if (value === 'card') {
            $('#card_area').slideDown();
            $('#card_area2').slideDown();
        } else if (value === 'cash') {

            $('#card_number').val('');
            $('#issuer').val('');
            $('#card_area').slideUp();
            $('#card_area2').slideUp();

        }
    }



    // submit form and add data
    $("#save_order").on('submit', function (e) {

        e.preventDefault();

        var order_status = $(this).val();
        var order_status = $('#order_status').val(order_status);

        var submit_url = "<?= base_url(); ?>b_level/order_controller/save_order";
        // $.ajax({
        //     type: 'POST',
        //     url: submit_url,
        //     data: $(this).serialize(),
        //     success: function(res) {
        //         $("#cartItems").load(location.href+" #cartItems>*",""); 
        //     },error: function() {
        //         Swal.fire('error');
        //     }
        // });
    });


    // -------- Show Image Preview once File selected ----
    $("body").on("change", "#file_upload", function (e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.getElementById('prevImg');
            var reader = new FileReader();
            reader.onloadend = function () {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("logo").after(img);

            $("#prevImg").show();
        }
    });
// -------- Image Preview Ends --------------
    $("#openDialog").click(function () {
        $.post("<?php echo base_url(); ?>show-customer-new-window-popup", function (t) {
            var w = window.open("", "popupWindow", "width=600, height=500, scrollbars=yes");
            var $w = $(w.document.body);
//        $w.html("<textarea></textarea>");
            $w.html(t);
        });
    });
//========== its for file reset starts =======
    var file_input_index = 0;
    $('input[type=file]').each(function () {
        file_input_index++;
        $(this).wrap('<div id="file_input_container_' + file_input_index + '"></div>');
        $(this).after('<input type="button" value="Clear" class="btn btn-danger clr_btn" onclick="reset_html(\'file_input_container_' + file_input_index + '\')" />');

    });
    function reset_html(id) {
        $('#' + id).html($('#' + id).html());
        $("#prevImg").hide();
    }
//========== its for file reset close=======
//========= its for new_customer_modal ==========
    function new_customer_modal() {
        $.post("<?php echo base_url(); ?>new-customer-modal", function (t) {
            $("#new_customer_info").html(t);
            $('#new_customer_modal_info').modal('show');
        });
    }



    function customerWiseComission_Inc_Dic(qty, item) {

        var customer_id = $("#customer_id").val();

        if (customer_id === '') {

            Swal.fire('Please select customer');
            $("#customer_id").focus();

        } else {

            var qty = $('#qty_' + item).val();

            var list_price = parseFloat($('#list_price_' + item).val());
            var total_list_price = list_price * qty;

            var dealer_price = $('#discount_' + item).val();

            var discount = (total_list_price * dealer_price) / 100;

            $('#utprice_' + item).val(total_list_price - discount.toFixed(2));
            calculetsPrice();
        }

    }


</script>


<script>

    $('.add').click(function () {


        if ($(this).prev().val() < 1000) {
            $(this).prev().val(+$(this).prev().val() + 1);
            var qty = $(this).prev().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);
        }

    });

    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1)
                $(this).next().val(+$(this).next().val() - 1);

            var qty = $(this).next().val();
            var item = $(this).parent().next().val();
            customerWiseComission_Inc_Dic(qty, item);

        }
    });
</script>



<script>
    $(document).ready(function () {
        $('.sticky_container .sticky_item').theiaStickySidebar({
            additionalMarginTop: 110
        });
        console.log('cat id ' +<?php echo $get_product_order_info->category_id; ?>);
        $('#category_id').val("<?php echo $get_product_order_info->category_id; ?>");
        $('body #category_id').select2("val", "<?php echo $category_id; ?>");
        $('#category_id').trigger('change');
        $('#room').val("<?php echo $get_product_order_info->room; ?>");
        //   $('body #room').select2("val", "<?php echo $get_product_order_info->room; ?>");
        $('#notes').val("<?php echo $get_product_order_info->notes; ?>");
        $('#height').val("<?php echo $get_product_order_info->height; ?>");
        $('#width').val("<?php echo $get_product_order_info->width; ?>");
        $('#width_fraction_id').val("<?php echo $get_product_order_info->width_fraction_id; ?>");
        $('#height_fraction_id').val("<?php echo $get_product_order_info->height_fraction_id; ?>");
        // console.log("width fraction <?php //echo $get_product_order_info->width_fraction_id;                                ?>");
        // $('#width_fraction_id').trigger('change');

        $('#height').trigger('change');
    });

</script>

