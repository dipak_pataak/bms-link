 <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/b_level/resources/css/daterangepicker.css" />
<style type="text/css">
    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        /*top: 0px !important;*/
        top: 262px !important;
        left: 188px;
        z-index: 10;
        display: block;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Manage Invoice</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Dashboard';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage Invoice</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">

                <p class="mb-0">
                    <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Filter
                    </button>
                </p>


                <div class="collapse show" id="collapseExample">

                    <div class="p-4 border mt-4">

                        <form class="form-horizontal" action="<?= base_url('manage-invoice') ?>" method="post" id="frm_manage_invoice">
                            <fieldset>
                                <div class="row">

                                    <div class="col-sm-6 col-md-3">
                                        <input type="text" class="form-control mb-3" placeholder="Order No." name="order_id" value="<?=$search->order_id?>">
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <input type="text" class="form-control mb-3" id="search_daterange" name="search_daterange" value="<?=$search->search_daterange?>"/>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <select class="form-control mb-3" name="customer_id" id="customer_id">
                                            <option value="">--Select customer--</option>
                                            <?php foreach ($customers as $c) { ?>
                                                <option value="<?= $c->customer_id ?>"><?= $c->first_name; ?> <?= $c->last_name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <script>
                                            $("#customer_id").val(<?=$search->customer_id?>);
                                        </script>    
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <select class="form-control mb-3" name="order_stage" id="order_stage" >
                                            <option value="">--Select Status--</option>
                                            <option value="1">Quote</option>
                                            <option value="2">Paid</option>
                                            <option value="3">Partially Paid</option>
                                            <option value="4">Shipping</option>
                                            <option value="5">Cancelled</option>
                                        </select>
                                        <script>
                                            $("#order_stage").val(<?=$search->order_stage?>);
                                        </script>
                                    </div>

                                    <div class="col-md-12 text-right">
                                        <div>
                                            <button type="reset" class="btn btn-sm btn-danger default" onClick="reset_filter()">Reset</button>
                                            <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>

                                    <tr>
                                        <th>Invoice #</th>
                                        <th>Date</th>
                                        <th>Sidemark</th>
                                        <th>Client Name </th>
                                        <th>Telelphone</th>
                                        <th>Invoice amount</th>
                                        <th>Amt Received</th>
                                        <!-- <th>Order details (Name of product include specifications)</th>
                                        <th>Grand total</th> -->
                                    </tr>

                                </thead>

                                <tbody>

                                    <?php
                                    if (!empty($orderd)) {

                                        foreach ($orderd as $key => $value) {

                                            // $query = $this->db->select("qutation_details.*,
                                            //     product_tbl.product_name,
                                            //     quatation_attributes.product_attribute,
                                            //     pattern_model_tbl.pattern_name,
                                            //     color_tbl.color_name")
                                            //                 ->from('qutation_details')
                                            //                 ->join('product_tbl', 'product_tbl.product_id=qutation_details.product_id', 'left')
                                            //                 ->join('quatation_attributes', 'quatation_attributes.fk_od_id=qutation_details.row_id', 'left')
                                            //                 ->join('pattern_model_tbl', 'pattern_model_tbl.pattern_model_id=qutation_details.pattern_model_id', 'left')
                                            //                 ->join('color_tbl', 'color_tbl.id=qutation_details.color_id', 'left')
                                            //                 ->where('qutation_details.order_id', $value->order_id)
                                            //                 ->get()->result();
                                            ?>

                                            <tr>
                                                <td ><a class="text-success" href="<?php echo base_url('retailer-invoice-receipt/') . $value->order_id; ?>"><?= $value->order_id; ?></a></td>
                                                <td><?= $value->order_date; ?></td>
                                                <td><?= $value->side_mark; ?></td>
                                                <td><?= $value->customer_name; ?></td>

                                                <td><?= $value->phone; ?></td>
                                                <td><?= $cmp_info[0]->currency ?><?= $value->grand_total; ?></td>
                                                <td><?= $cmp_info[0]->currency ?><?= $value->paid_amount; ?></td>

                                                <!-- <td><?php
                                                    $products = '';
                                                    foreach ($query as $key => $val) {
                                                        $products .= $val->product_name . ', ';
                                                    }
                                                    echo rtrim($products, ', ');
                                                    ?>    
                                                </td> -->
                                                <!-- <td><?= $cmp_info[0]->currency ?><?= $value->grand_total ?></td> -->

                                            </tr>

                                            <?php
                                        }
                                    } else {
                                        ?>

                                        <!--<div class="alert alert-danger"> There have no order found..</div>-->

                                    <?php } ?>

                                    
                                </tbody>
                                <?php if (empty($orderd)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="7">Record not found!</th>
                                        </tr>
                                    </tfoot>
                                <?php } ?>
                            </table>

                            <?=@$links?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
$(function() {
  $('#search_daterange').daterangepicker({
    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last Week': [moment().subtract(1, 'weeks'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
});

function reset_filter(){
    $('#search_daterange').val('');
    $("#frm_manage_invoice").submit();
}
</script>