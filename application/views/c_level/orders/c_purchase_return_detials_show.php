<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Order</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Return Order</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>



        <div class="card mb-4">
            <div class="card-body">
                <div class="col-sm-6">
                    <table class="table table-bordered table-hover">
                        <?php // dd($orders); ?>
                        <tr>
                            <td class="text-center">Order ID</td>
                            <td class="text-center"><?= ($orders->order_id) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Order Date</td>
                            <td class="text-center"><?= (date('M-d-Y', strtotime($orders->order_date))) ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">Client Name</td>
                            <td class="text-center"><?= $orders->customer_name ?></td>
                        </tr>
                        </tr>

                    </table>
                </div>

                <div class="table-responsive" style="margin-top: 10px">
                    <!--<form action="<?php echo base_url(); ?>retailer-customer-purchase-order-return-save" method="post">-->
                    <table class="table table-bordered table-hover" id="normalinvoice">
                        <thead>
                            <tr>
                                <th class="text-center">Product Nmae</th>
                                <th class="text-center">Return Qnt </th>
                                <th class="text-center">Replace Qnt </th>
                                <th class="text-center">Replace Date </th>
                            </tr>
                        </thead>
                        <tbody id="addinvoiceItem">
                            <?php
                            if (!empty($return_detais_info)) { 
                                $i = 1;
                                foreach ($return_detais_info as $key => $p) {
								
								
								 $order_return = $this->db->query('SELECT * FROM order_return_tbl WHERE return_id='.$p->return_id)->row();
								 
								  $quotion_details = $this->db->query('SELECT * FROM qutation_details WHERE order_id="'.$order_return->order_id.'"')->row();
								  $color_name1 = $this->db->query('SELECT * FROM color_tbl WHERE id="'.$quotion_details->color_id.'"')->row();
								  
                                    $i++;
                                    ?>    
                                    <tr>
                                        <td><?php echo $p->product_name; ?>
										<br /><strong><?php  if ($single->pattern_name) {
                                                    echo $single->pattern_name . '<br/>';
                                                }?></strong>(W <?= $quotion_details->width; ?>, 
                                                H <?= $quotion_details->height; ?>,<?= $color_name1->color_number; ?> 
                                                <?= $color_name1->color_name.', ';  ?><?php
                                             
                                                    echo $quotion_details->room 
                                                  
                                                ?>)</td>
                                        <td><?php echo $p->return_quantity; ?></td>
                                        <td><?php echo $p->replace_quantity; ?></td>
                                        <td><?php echo $p->replace_date; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <?php
                            if (empty($return_detais_info)) {
                                echo "<th colspan='6' class='text-center text-danger'>Record not found!</th>";
                            }
                            ?>
                        </tfoot>
                    </table> 
                    <!--</form>-->
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function return_calculation(item) {
                var stock_quantity = parseInt($("#stock_quantity_" + item).val());
                var quantity = parseInt($("#quantity_" + item).val());
                var return_quantity = parseInt($("#return_quantity_" + item).val());
                //        console.log(typeof (stock_quantity));
//                if (stock_quantity < return_quantity) {
//                    Swal.fire("Return Quantity is not greater than stock quantity");
//                    $("#stock_quantity_" + item).css({'border': '2px solid red'});
//                    $("#return_quantity_" + item).css({'border': '2px solid red'});
//                    $('button[type=submit]').prop('disabled', true);
//                } else {
//                    $("#stock_quantity_" + item).css({'border': '2px solid green'});
//                    $("#return_quantity_" + item).css({'border': '2px solid green'});
//                    $('button[type=submit]').prop('disabled', false);
//                }
                if (quantity < return_quantity) {
                    Swal.fire("Return Quantity is not greater than quantity");
                    $("#quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).css({'border': '2px solid red'});
                    $('button[type=submit]').prop('disabled', true);
                } else if (return_quantity == '0') {
                    Swal.fire("0 is not allowed");
                    $("#quantity_" + item).css({'border': '2px solid red'});
                    $("#return_quantity_" + item).css({'border': '2px solid red'});
                    $('button[type=submit]').prop('disabled', true);
                } else {
                    $("#quantity_" + item).css({'border': '2px solid green'});
                    $("#return_quantity_" + item).css({'border': '2px solid green'});
                    $('button[type=submit]').prop('disabled', false);
                }
            }
        </script>
