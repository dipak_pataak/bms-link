<style type="text/css">
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width: 1200px;
        }
    }
    table tbody tr.color-orange td {
        background: #e2a533 !important;
    }
    table tbody tr.color-green td {
        background: #77b16c !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Invoice</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Invoice</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="col-lg-6 offset-lg-6 text-right">
                    <?php 
                        $user_id = $this->session->userdata('user_id');
                        $user_detail = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row(); 
                    ?>
                    <?php if ($orderd->synk_status == 0 && $orderd->order_stage == 1 || $orderd->order_stage == 2 || $orderd->order_stage == 3) { ?>
                        <a href="<?= base_url('retailer_go_manufacturing/'); ?><?= $orderd->order_id ?>"
                        class="btn btn-success">Go Manufacturing</a>
                    <?php } ?>
                    <?php if ($user_detail->package_id == 3) { // Allow access to pro user only?>
                        <?php if ($shipping == '' && $orderd->order_stage == 4 || $orderd->order_stage == 11) { ?>
                        <a href="<?= base_url('c_level/order_controller/shipment/'); ?><?= $orderd->order_id ?>"
                            class="btn btn-success">Go Shipment</a>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($orderd->due > 0) { 
                    ?>
                    <a href="<?php echo base_url('retailer-order-view/'); ?><?php echo $orderd->order_id?>" class="btn btn-success">Customer Payment</a>
                    <?php } 
                    ?>

                    <?php 
                        // Get b level order id based on retailer order id : START
                        $b_order = $this->db->where('clevel_order_id', $orderd->order_id)->limit('1')->get('b_level_quatation_tbl')->row();
                        // Get b level order id based on retailer order id : END
                    ?>
                    <?php if ($orderd->synk_status == 1) { ?>
                        <a href="<?= base_url('retailer-make-payment-wholesaler/'); ?><?= $b_order->order_id ?>" class="btn btn-success">Wholesale Payment</a>
                    <?php } ?>
                    <button type="button" class="btn btn-success" onclick="printContent('printableArea')">Print</button>
                    <?php
                    if ($orderd->order_stage == 7) { ?>
                        <button type="button" class="btn btn-success" onclick="installer_copy_content('printable_installer_copy')">Installer Copy</button>
                    <?php } ?>
                </div>

                <div class="card mb-4">
                    <div class="card-body" id="printableArea">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <div class="" style="width:200px; height: 120px;">
                                    <img src="<?php echo base_url('assets/c_level/uploads/appsettings/') . $company_profile[0]->logo; ?>" style="width:200px; height: auto;">
                                </div>
                                <span><?= $company_profile[0]->company_name; ?></span><br />
                                <span><?= $company_profile[0]->address; ?></span><br />
                                <span><?= $company_profile[0]->city; ?>,<br />
                                    <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                                    <?= $company_profile[0]->country_code; ?></span><br />
                                <span><?= $company_profile[0]->phone; ?></span><br />
                                <span><?= $company_profile[0]->email; ?></span><br />
                            </div>
                            <div class="form-group col-md-5" style="margin-left: 20px">

                                <table class="table table-bordered mb-4">

                                    <tr class="text-center">
                                        <td>Order Date</td>
                                        <td class="text-right"><?= date_format(date_create($orderd->order_date), 'M-d-Y'); ?></td>
                                    </tr>

                                    <tr class="text-center">
                                        <td>Order Id</td>
                                        <td class="text-right"><?= $orderd->order_id ?></td>
                                    </tr>

                                    <?php if ($shipping != NULL) { ?>
                                        <tr class="text-center">
                                            <td>Tracking Number (<?= $shipping->method_name ?>) </td>
                                            <td class="text-right"> <?= $shipping->track_number ?> </td>
                                        </tr>
                                    <?php } ?>
                                    <?php 
                                        $user_id = $this->session->userdata('user_id');
                                        $user_detail = $this->db->select('*')->from('user_info')->where('id',$user_id)->get()->row(); 
                                    ?>
                                    <?php if ($orderd->synk_status == 0) { ?>
                                        <?php if ($user_detail->package_id == 3) { ?>
                                            <tr class="text-center">
                                                <td>Barcode</td>
                                                <td class="text-right">
                                                    <?php
                                                    if ($orderd->barcode != NULL) {
                                                        echo '<img src="' . base_url() . $orderd->barcode . '" width="100%" style="height:50px;"/>';
                                                    } 
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <br />
                        <div class="row">

                            <div class="form-group col-md-6">
                                <strong>Sold To: </strong><?= $customer->first_name; ?> <?= $customer->last_name; ?><br />
                                <span><?= $customer->address; ?></span><br />
                                <span><?= $customer->city; ?>,
                                    <?= $customer->state; ?>, <?= $customer->zip_code; ?>,
                                    <?= $customer->country_code; ?></span><br />
                                <span><?= $customer->phone; ?></span><br />
                                <span><?= $customer->email; ?></span><br />
                            </div>

                            <div class="form-group col-md-5" style="margin-left: 20px;">

                                <strong>Ship To: </strong><?= $customer->first_name; ?> <?= $customer->last_name; ?><br />
                                <?php if ($orderd->is_different_shipping == 1 && $orderd->different_shipping_address != '') { ?>
                                    <span><?= $orderd->different_shipping_address ?></span><br />
                                <?php } else { ?>
                                    <span><?= $customer->address; ?></span><br />
                                    <span><?= $customer->city; ?>,
                                        <?= $customer->state; ?>, <?= $customer->zip_code; ?>,
                                        <?= $customer->country_code; ?></span><br />
                                    <span><?= $customer->phone; ?></span><br />
                                    <span><?= $customer->email; ?></span><br />
                                <?php } ?>

                            </div>
                        </div>

                        <h5>Order Details</h5>

                        <div class="separator mb-3"></div>

                        <div class="table-responsive m-b-20">

                            <table class="table table-bordered mb-4">

                                <thead>
                                    <tr>
                                        <th>Item.</th>
                                        <th>Qty</th>
                                        <th width="400px;">Discription</th>
                                        <th>List</th>
                                        <th>Discount(%)</th>
                                        <th>Price</th>
                                        <th>Notes</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php $i = 1; ?>

                                    <?php
                                    $selected_rooms = array();
                                    foreach ($order_details as $items) :
                                        if ($items->manufactured_scan_date && $items->shipping_scan_date) {
                                            $background_class = "color-orange";
                                        } else if ($items->manufactured_scan_date) {
                                            $background_class = "color-green";
                                        } else {
                                            $background_class = '';
                                        }

                                        $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                                        $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                                        ?>

                                        <tr class="<?php echo $background_class; ?>">
                                            <td><?= $i ?></td>
                                            <td><?= $items->product_qty; ?></td>
                                            <td align="left">
                                                <strong><?= $items->product_name; ?></strong><br />
                                                <?php
                                                    if ($items->pattern_name) {
                                                        echo $items->pattern_name . '<br/>';
                                                    }
                                                    ?>
                                                W <?= $items->width; ?> <?= @$width_fraction->fraction_value ?>,
                                                H <?= $items->height; ?> <?= @$height_fraction->fraction_value ?>,
                                                <?= $items->color_number; ?>
                                                <?= $items->color_name . ', '; ?>

                                                <?php
                                                    if (isset($selected_rooms[$items->room])) {
                                                        echo $items->room . ' ' . $selected_rooms[$items->room];
                                                        $selected_rooms[$items->room]++;
                                                    } else {
                                                        echo $items->room . ' 1';
                                                        $selected_rooms[$items->room] = 2;
                                                    }
                                                    ?>

                                            </td>

                                            <td><?= $company_profile[0]->currency; ?><?= number_format($items->list_price, 2) ?></td>
                                            <td><?= ($items->discount) ?></td>
                                            <td><?= $company_profile[0]->currency; ?> <?= @number_format($items->unit_total_price, 2) ?> </td>
                                            <td><?= $items->notes ?></td>
                                            <td class="noprint">
                                                <!--<a onclick="viewItem(<?php // echo $items->row_id;               
                                                                                ?>)" class="btn btn-success default btn-xs" id="view-item-btn" title="View"><i class="simple-icon-eye"></i></a>-->
                                                <!--<a href="<?php // echo base_url();            
                                                                    ?>c_level/order-edit/<?php // echo $items->row_id;            
                                                                                                ?>/0" class="btn btn-success btn-sm" title="View"><i class="fa fa-eye"></i>View</a>-->
                                                <a href="#" data-row_id="<?php echo $items->row_id; ?>" data-order_id="<?php echo $items->order_id; ?>" class="btn btn-success btn-sm view_order_btn" title="View"><i class="fa fa-eye"></i>View</a>
                                                <?php if ($this->permission->check_label('manage_order')->update()->access() && $orderd->order_stage == 1) { ?>
                                                    <!-- <a href="<?php echo base_url(); ?>c_level/order-edit/<?php echo $items->row_id; ?>/1" class="btn btn-info btn-sm" title="Edit"><i class="fa fa-edit"></i>Edit</a> -->

                                                    <a class="btn btn-warning btn-sm" onclick="showEditModal(<?php echo $items->row_id ?>);"><i class="fa fa-edit"></i>Edit</a>

                                                    <a href="<?php echo base_url(); ?>c_level/order-window-delete/<?php echo $items->row_id; ?>" class="btn btn-danger  btn-sm" title="Delete"><i class="fa fa-trash"></i>Delete</a>
                                                <?php } ?>

                                            </td>

                                        </tr>

                                        <?php $i++; ?>

                                    <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>

                        <div class="table-responsive m-b-20">

                            <table class="table table-bordered mb-4">

                                <thead>
                                    <tr>
                                        <?php if (isset($shipping->shipping_charges) && $shipping->shipping_charges != NULL) { ?>
                                            <th>Shipping cost</th>
                                        <?php } ?>
                                        <th>Sales Tax</th>
                                        <th>Sub-Total</th>
                                        <th>Installation Charge</th>
                                        <th>Other Charge</th>
                                        <th>Misc</th>
                                        <th>Discount</th>
                                        <th>Grand Total</th>
                                        <th>Deposit</th>
                                        <th>Due </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php if (isset($shipping->shipping_charges) && $shipping->shipping_charges != NULL) { ?>
                                        <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($shipping->shipping_charges, 2) ?> </td>
                                    <?php } ?>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->state_tax, 2) ?></td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->subtotal, 2) ?> </td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->installation_charge, 2) ?> </td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->other_charge, 2) ?></td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->misc, 2) ?> </td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->invoice_discount, 2) ?></td>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->grand_total, 2) ?>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->paid_amount, 2) ?>
                                    <td class="text-right"> <?= $company_profile[0]->currency; ?> <?= number_format($orderd->due, 2) ?>
                                </tbody>
                            </table>

                            <?php if (isset($shipping->graphic_image) && $shipping->graphic_image != NULL) { ?>
                                <div class="col-lg-6 offset-lg-4">
                                    <img src="<?php echo base_url() . $shipping->graphic_image ?>" width="300" height='200'>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>

                <div class="card mb-4 d-none" id="printable_installer_copy">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12" style="text-align: center;">
                                <h1>Installer Copy</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <span><strong><?= $company_profile[0]->company_name; ?></strong></span><br />
                                <span><?= $company_profile[0]->address; ?></span><br />
                                <span><?= $company_profile[0]->city; ?>,<br />
                                    <?= $company_profile[0]->state; ?>, <?= $company_profile[0]->zip_code; ?>,
                                    <?= $company_profile[0]->country_code; ?></span><br />
                                <span><?= $company_profile[0]->phone; ?></span><br />
                                <span><?= $company_profile[0]->email; ?></span><br />
                            </div>
                            <div class="form-group col-md-5" style="margin-left: 20px;">
                                <div style="border: 1px solid black;padding: 5px;">
                                    <strong>Sold To: </strong><?= $customer->first_name; ?> <?= $customer->last_name; ?>
                                </div>
                                <div style="border: 1px solid black;padding: 5px;margin-top: 10px;">
                                    <strong>Install Location: </strong><span><?= $customer->address; ?>,</span><br />
                                    <span><?= $customer->city; ?>,</span><br />
                                    <span><?= $customer->state; ?>, <?= $customer->zip_code; ?>,
                                        <?= $customer->country_code; ?></span>
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <table class="table table-bordered mb-4" style="border: 2px solid black;">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Order</th>
                                    <th>Salesperson</th>
                                    <th>Sidemark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo date_format(date_create($orderd->order_date), 'm/d/Y'); ?></td>
                                    <td><?php echo $orderd->order_id; ?></td>
                                    <td><?php echo $company_profile[0]->company_name; ?></td>
                                    <td><?php echo $orderd->side_mark; ?></td>
                                </tr>
                            </tbody>
                            </thead>
                        </table>
                        <br /><br />
                        <div class="table-responsive m-b-20">
                            <table class="table table-bordered mb-4" style="border: 2px solid black;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>QTY</th>
                                        <th>ROOM</th>
                                        <th>M</th>
                                        <th>W</th>
                                        <th>H</th>
                                        <th>PRODUCT</th>
                                        <th>COLOR</th>
                                        <th>NOTES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $selected_rooms = array();
                                    if (!empty($order_details)) {
                                        $i = 0;
                                        foreach ($order_details as $items) {
                                            $i++;
                                            $width_fraction = $this->db->where('id', $items->width_fraction_id)->get('width_height_fractions')->row();
                                            $height_fraction = $this->db->where('id', $items->height_fraction_id)->get('width_height_fractions')->row();
                                            $selected_attributes = json_decode($items->product_attribute);

                                            ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $items->product_qty; ?></td>
                                                <td>
                                                    <?php if (isset($selected_rooms[$items->room])) {
                                                                echo $items->room . ' ' . $selected_rooms[$items->room];
                                                                $selected_rooms[$items->room]++;
                                                            } else {
                                                                echo $items->room . ' 1';
                                                                $selected_rooms[$items->room] = 2;
                                                            }
                                                            ?>
                                                </td>
                                                <td><?php foreach ($selected_attributes as $atributes) {
                                                                $at_id = $atributes->attribute_id;
                                                                $att_name = $this->db->where('attribute_id', $at_id)->get('attribute_tbl')->row();
                                                                $attribute_name = $att_name->attribute_name;
                                                                $at_op_id = $atributes->options[0]->option_id;
                                                                $att_op_name = $this->db->where('att_op_id', $at_op_id)->get('attr_options')->row();
                                                                if ($attribute_name == "Mount") {
                                                                    echo $att_op_name->option_name;
                                                                }
                                                            }
                                                            ?></td>
                                                <td><?php echo $items->width; ?> <?php echo @$width_fraction->fraction_value; ?></td>
                                                <td><?php echo $items->height; ?> <?php echo @$height_fraction->fraction_value; ?></td>
                                                <td><?php echo $items->product_name; ?>
                                                    <?php
                                                            if ($items->pattern_name) {
                                                                echo $items->pattern_name . '<br/>';
                                                            }
                                                            ?>
                                                </td>
                                                <td><?php echo (!empty($items->color_number)) ? $items->color_number . '/' : ''; ?>
                                                    <?php echo $items->color_name; ?></td>
                                                <td><?php echo $items->notes; ?></td>

                                            </tr>
                                        <?php }
                                        } else { ?>
                                        <tr>
                                            <td colspan="9">No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div>
                                <p><strong>Install</strong><br />
                                    <span>Notes:</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="Modal-Manage-Order" class="modal fade modal-place-order" role="dialog">
    <div class="modal-dialog modal-xl" style="margin: auto; max-width: 900px; ">
        <div class="modal-content" style="width: 1024px;">
            <div class="modal-header">
                <h4 class="modal-title"><span class="action_type"></span> Order</h4>
                <div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="padding-top:20px!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div id="actionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="padding: 5px !important;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="editOrderModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row form-area">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="submitUpdateRequest" class="btn btn-default" data-dismiss="modal">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
    $(".dropdown-item").click(function() {
        $(".btn-pay").slideToggle();
    });

    $(".view_order_btn").click(function() {
        var row_id = $(this).data('row_id');
        var order_id = $(this).data('order_id');
        viewOrderHtml(order_id, row_id);
    });

    function viewOrderHtml(order_id, row_id) {
        var orderid = order_id;
        var rowid = row_id;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('c_level/Order_controller/getOrderData') ?>",
            data: {
                order_id: orderid,
                row_id: rowid

            },
            success: function(data) {

                $('#Modal-Manage-Order .action_type').text('View');
                $('#Modal-Manage-Order .modal-body').html(data);
                //$('#Modal-Manage-Order #customer_id').val(quote_cutomer_id);
                //$('#Modal-Manage-Order #customer_id').change();
                $('#Modal-Manage-Order').modal('show');

            },
            error: function() {
                Swal.fire('error');
            }
        });
    }


    function showEditModal(rowId) {
        $.ajax({
            url: "<?php echo base_url(); ?>c_level/Order_controller/get_new_order_edit/" + rowId + "/1",
            success: function(result) {
                $('#editOrderModal').modal('show');
                $('#editOrderModal .modal-body .form-area').html('');
                $('#editOrderModal .modal-body .form-area').html(result);
                $('.btns-box #cartbtn').remove();
                $('.btns-box #clrbtn').remove();
                $("#AddToCart").attr('action', '<?php echo base_url(); ?>c_level/Order_controller/update_order_item_details');
                $('#AddToCart').attr('id', 'UpdateOrderItem');
                
                if($('.sticky_container .sticky_item').length > 0){
                    $('.sticky_container .sticky_item').theiaStickySidebar({
                        additionalMarginTop: 110
                    });
                }
            
                console.log(rowId);
            }
        });
    }

    $('#submitUpdateRequest').click(function() {
        $("#UpdateOrderItem").submit();
    });

    // submit form and add data
    $('body').on('submit', "#UpdateOrderItem", function(e) {
        e.preventDefault();
        var submit_url = "<?php echo base_url(); ?>c_level/Order_controller/update_order_item_details";
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $(this).serialize(),
            success: function(res) {
                location.reload();
            }
        });
    });

    // function viewOrderHtml(order_id, row_id) {
    //     var orderid = order_id;
    //     var rowid = row_id;
    //     $.ajax({
    //         type: "POST",
    //         url: "<?php //echo base_url('c_level/Order_controller/getOrderData') 
                        ?>",
    //         data: {
    //             order_id: orderid,
    //             row_id: rowid
    //         },
    //         success: function (data) {
    //             $('#Modal-Manage-Order .action_type').text('View');
    //             $('#Modal-Manage-Order .modal-body').html(data);
    //             $('#Modal-Manage-Order').modal('show');
    //         },
    //         error: function () {
    //             Swal.fire('error');
    //         }
    //     });
    // }

    // function viewItem(id) {
    //     $.ajax({
    //         url: "<?php //echo base_url('c_level/order_controller/update_order_item'); 
                        ?>",
    //         type: "POST",
    //         data: {row_id: id, is_view_only: 1},
    //         success: function (data) {
    //             $('#actionModal .modal-title').html('Update Item');
    //             $('#actionModal .modal-body').html(data);
    //             $('#actionModal').modal('show');
    //         }
    //     });
    // }

    // function updateItem(id) {
    //     $.ajax({
    //         url: "<?php //echo base_url('c_level/order_controller/update_order_item'); 
                        ?>",
    //         type: "POST",
    //         data: {row_id: id, is_view_only: 0},
    //         success: function (data) {
    //             $('#actionModal .modal-title').html('Update Item');
    //             $('#actionModal .modal-body').html(data);
    //             $('#actionModal').modal('show');
    //         }
    //     });
    // }

    function printContent(el) {
        $('body').css({
            "background-color": "#fff"
        });
        $('p').css({
            "font-size": "18px"
        });
        $('table').css({
            "font-size": "17px"
        });
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }

    function installer_copy_content(el) {
        $("#" + el).removeClass('d-none');
        $('body').css({
            "background-color": "#fff"
        });
        $('p').css({
            "font-size": "18px"
        });
        $('table').css({
            "font-size": "17px"
        });
        var restorepage = $('body').html();
        var printcontent = $('#' + el).clone();
        console.log(printcontent);
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }
</script>