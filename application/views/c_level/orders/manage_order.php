 <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/b_level/resources/css/daterangepicker.css" />
<style type="text/css">
	.datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
		top: 262.594px !important;
		left: 188px;
		z-index: 10;
		display: block;
	}
</style>
<?php $currency = $company_profile[0]->currency; ?>

<main>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">

				<div class="mb-3">
					<h1>Manage Orders</h1>
					<?php
						$page_url = $this->uri->segment(1);
						$get_favorite = get_c_favorite_detail($page_url);
						$class = "notfavorite_icon";
						$fav_title = 'Manage Orders';
						$onclick = 'add_favorite()';
						if (!empty($get_favorite)) {
							$class = "favorites_icon";
							$onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
						}
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
					<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
						<ol class="breadcrumb pt-0">
							<li class="breadcrumb-item">
								<a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
							</li>
							<li class="breadcrumb-item">
								<a href="#">Order</a>
							</li>
							<li class="breadcrumb-item active" aria-current="page">Manage</li>
						</ol>
					</nav>
				</div>
				<div class="separator mb-5"></div>
			</div>
		</div>
        <?php
        $user_data=access_role_permission(6);
        $log_data=employ_role_access($id);
        ?>



		<div class="card mb-4">
			<div class="card-body">

				<p class="mb-0">
					<button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						Filter
					</button>
				</p>

				<div class="collapse" id="collapseExample">
					<div class="p-4 border mt-4">
						<form class="form-horizontal" action="<?= base_url('manage-order') ?>" method="post">
							<fieldset>
								<div class="row">
									<div class="col-sm-6 col-md-3">
										<input type="text" class="form-control mb-3 order_id" placeholder="Order No." name="order_id" >
									</div>

									<div class="col-sm-6 col-md-3">
										<select class="form-control customer_id mb-3 select2-single" name="customer_id" data-placeholder="--Select customer--">
											<option value=""></option>
											<?php foreach ($customers as $c) { ?>
												<option value="<?= $c->customer_id ?>"><?= $c->first_name; ?> <?= $c->last_name; ?></option>
											<?php } ?>
										</select>
									</div>

									<div class="col-sm-6 col-md-3">
										<input type="text"  name="search_daterange" id="search_daterange"
										 name="search_daterange" class="form-control mb-3 order_date" value="<?=$search->search_daterange?>">
									</div>                          

									<div class="col-sm-6 col-md-3">
										<select class="form-control order_stage mb-3 select2-single" name="order_stage" data-placeholder="----Select Status----">
											<option value=""></option>
											<option value="1">Quote</option>
											<option value="2">Paid</option>
											<option value="3">Partially Paid</option>
											<option value="4">Manufacturing</option>
											<option value="5">Shipping</option>
											<option value="6">Cancelled</option>
											<option value="7">Delivered</option>
										</select>
									</div>

									<div class="col-md-12 text-right">
										<div>
											<button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
											<button type="submit" class="btn btn-sm btn-success default">Go</button>
										</div>
									</div>

								</div>

							</fieldset>

						</form>

					</div>
				</div>
			</div>
		</div>


		<div class="">
			<?php
			$error = $this->session->flashdata('error');
			$success = $this->session->flashdata('success');
			if ($error != '') {
				echo $error;
			}
			if ($success != '') {
				echo $success;
			}
			?>

			<?php
			$message = $this->session->flashdata('message');

			if ($message != '') {
				echo $message;
			}
			?>


		</div>

<?php 
		$user_id = $this->session->userdata('user_id');
        $user_deatail = $this->db->where('id',$user_id)->get('user_info')->row(); 
?>

		<div class="row">

			<div class="col-xl-12 mb-4">
				<div class="text-right">
					 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
					<a href="<?php echo base_url('retailer-bulk-order-upload'); ?>" class="btn btn-success btn-sm mt-1">Order bulk upload</a>
					<?php } ?>
					 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
					<a href="javascript:void(0)" class="btn btn-danger btn-sm mt-1 action-delete" onClick="return action_delete(document.recordlist)" >Delete</a>
				<?php } ?>
				</div>

				<div class="card mb-4">
					<div class="card-body">
						<form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Order_controller/manage_action') ?>">
							<input type="hidden" name="action">
							<div class="table-responsive">

								<table class="table table-bordered table-hover text-center">

									<thead>
										<tr>
											<th><input type="checkbox" id="SellectAll"/></th>
											<th>Order/Quote No</th>
											<th>Client Name </th>
											<th>Side mark</th>
											<th>Order date</th>
											<th>Price</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>

										<?php
										if (!empty($orderd)) {

											foreach ($orderd as $key => $value) {

												$products = $this->db->where('order_id', $value->order_id)->get('qutation_details')->result();
												$attributes = $this->db->where('order_id', $value->order_id)->get('quatation_attributes')->row();

												$paid_array = array('1','2','3','4');
												$partiallly_paid_array = array('1','3','4');
												$in_process_array = array('1','4');
												$mfg_array = array('1','2','3','4','11');
												$ready_to_ship_array = array('1','2','3','4','8','11');
												$in_transit_array = array('1','2','3','4','8','9','11');
												?>

												<tr style="<?= ($value->synk_status == '1' ? 'background:#a6c5a652' : '') ?>">
													<td>
														<input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $value->id; ?>" class="checkbox_list">  
													</td>
													<td><?= $value->order_id; ?></td>
													<td><?= $value->customer_name; ?></td>
													<td><?= $value->side_mark; ?></td>
													<td><?= date_format(date_create($value->order_date), 'M-d-Y'); ?></td>
													<td><?= $currency; ?><?= round($value->grand_total,2) ?></td>

													<?php if ($this->permission_c->check_label_multi(array(6,48))->update()->redirect()) { ?>

														<td>
															<select <?php if($value->order_stage == 7 || $value->order_stage == 10){ ?> disabled="disabled"; <?php } ?> class="form-control select2-single" onchange="setOrderStage(this.value, '<?= $value->order_id; ?>')" data-placeholder="-- select one --">
																<option value=""></option>
																<?php if($value->order_stage == 1){ ?>
																	<option value="1" <?= ($value->order_stage == 1 ? 'selected' : '') ?>>Quote</option>
																<?php } ?>

																<?php if(in_array($value->order_stage, $paid_array)){ ?>
																	<option value="2" <?= ($value->order_stage == 2 ? 'selected' : '') ?>>Paid</option>
																<?php } ?>

																<?php if(in_array($value->order_stage, $partiallly_paid_array)){ ?>
																	<option value="3" <?= ($value->order_stage == 3 ? 'selected' : '') ?>>Partially Paid</option>
																<?php } ?>

																<?php if($value->synk_status != 0){ ?>

																	<?php if(in_array($value->order_stage, $in_process_array)){ ?>
																		<option value="4" <?= ($value->order_stage == 4 ? 'selected' : '') ?>>In Process</option>
																	<?php } ?>

																	<option value="5" <?= ($value->order_stage == 5 ? 'selected' : '') ?>>Shipping/Installation</option>

																<?php } ?>


																<option value="6" <?= ($value->order_stage == 6 ? 'selected' : '') ?>>Cancelled</option>
																<option value="7" <?= ($value->order_stage == 7 ? 'selected' : '') ?>>Delivered</option>

															<?php if ($user_deatail->package_id==3 && $value->synk_status == 0) { ?>									
																<?php if(in_array($value->order_stage, $mfg_array)){ ?>
			                                                    	<option value="11" <?= ($value->order_stage == 11 ? 'selected' : '') ?>>Manufacturing</option>
			                                                	<?php } ?>

			                                                	<?php if(in_array($value->order_stage, $ready_to_ship_array)){ ?>
																	<option value="8" <?= ($value->order_stage == 8 ? 'selected' : '') ?>>Ready to be Shipped</option>
																<?php } ?>

																<?php if(in_array($value->order_stage, $in_transit_array)){ ?>
			                                                    	<option value="9" <?= ($value->order_stage == 9 ? 'selected' : '') ?>>In Transit</option>
			                                                	<?php } ?>
															<?php } ?>
																<option value="10" <?= ($value->order_stage == 10 ? 'selected' : '') ?>>Confirmation</option>
															</select>
														</td>
														
													<?php } ?>

													<td>
														<?php if ($this->permission_c->check_label_multi(array(6,48))->update()->access()) { ?>

															<?php if ($value->order_stage == 7) { ?>
																<a href="<?php echo base_url(); ?>retailer-customer-order-return/<?= $value->order_id ?>" class="btn btn-warning btn-xs" title="Return " data-toggle="tooltip" data-placement="top" data-original-title="Return"><i class="iconsmind-Repeat-6" aria-hidden="true"></i></a>
															<?php } ?>

														<?php } ?>


						<?php 
						$this->db->select('*');
						$this->db->from('qutation_details');
						$this->db->where('order_id',$value->order_id);
						$this->db->where('created_status',0);
						$is_check = $this->db->get()->row();
						 ?>
						 						<?php if(!empty($is_check)) { ?>

														<?php if ($this->permission_c->check_label_multi(array(6,48))->update()->access()) { ?>


				<?php if ($value->synk_status == 0 && ($value->order_stage == 1 || $value->order_stage == 2 || $value->order_stage == 3)) { ?>
																<a href="<?= base_url('c_level/invoice_receipt/synk_to_b/') ?><?= $value->order_id; ?>" class="btn btn-primary btn-xs default"  title="Send to <?=isset($b_level_company_info['company'])?$b_level_company_info['company']:''?>" data-toggle="tooltip" data-placement="top" data-original-title="Send to <?=isset($b_level_company_info['company'])?$b_level_company_info['company']:''?>">
																	<i class="iconsmind-Repeat-2"></i>
																</a>
															<?php } ?>

														<?php } ?>

						 						<?php } ?>

						 								<?php if ($value->order_stage == 11) { ?>
																<a href="<?= base_url('retailer-quotation/') . $value->order_id; ?>" class="btn btn-success btn-xs default" title="Manufacturing" data-toggle="tooltip" data-placement="top" data-original-title="Manufacturing"><i class="simple-icon-settings" aria-hidden="true"></i></a>
														<?php } ?>

														<?php if ($this->permission_c->check_label_multi(array(6,48))->read()->access()) { ?>
															<?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>

															<a href="<?= base_url('retailer-invoice-receipt/') ?><?= $value->order_id; ?>" class="btn btn-success btn-xs default" title="View" data-toggle="tooltip" data-placement="top" data-original-title="View">
																<i class="simple-icon-eye"></i>
															</a>
                                                           <?php } ?>
														<?php } ?>

														<?php if ($this->permission_c->check_label_multi(array(6,48))->delete()->access()) { ?>
															 <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
															<a href="c_level/order_controller/delete_order/<?= $value->order_id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger default btn-xs" title="Delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></a>
															<?php } ?>
														<?php } ?>

													</td>


												</tr>

												<?php
											}
										} else {
											?>

											<!--<div class="alert alert-danger"> There have no order found..</div>-->
										<?php } ?>

									</tbody>
									<?php if (empty($orderd)) { ?>
										<tfoot>
											<tr>
												<th class="text-center text-danger" colspan="8">Record not found!</th>
											</tr>
										</tfoot>
									<?php } ?>

								</table>
								<?php echo $links; ?>

							</div>
						</form>    
					</div>

				</div>

			</div>
		</div>
	</div>
</main>



<div class="modal fade" id="cancel_comment_modal_info" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Comments</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" id="cancel_comment_info">
				<form class="" action="<?php echo base_url('c_level/Order_controller/cancel_comment'); ?>" method="post">
					<textarea class="form-control" name="cancel_comment" required ></textarea>
					<input type="hidden" name="order_id" id="order_id">
					<input type="hidden" name="stage_id" id="stage_id">
					<input type="submit" class="btn btn-info" style="margin-top: 10px;">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

	function setOrderStage(stage_id, order_id) {

		if (stage_id != '') {
			if (stage_id === '2' || stage_id === '3') 
			{
				window.location.href = 'retailer-order-view/' + order_id;
			}
			if (stage_id === '7' || stage_id === '4' || stage_id === '11' || stage_id === '5' || stage_id === '1') 
			{

				$.ajax({
					url: "<?php echo base_url('c_level/order_controller/set_order_stage/'); ?>" + stage_id + "/" + order_id,
					type: 'GET',
					success: function (r) {
						toastr.success('Success! - Order Stage Set Successfully');
						setTimeout(function () {
							window.location.href = window.location.href;
						}, 2000);
					}
				});

			} 
			if (stage_id === '10') 
			{
				$.ajax({
                    url: "<?php echo base_url(); ?>c_level/order_controller/send_confirmation_email/" + stage_id + "/" + order_id,
                    type: 'GET',
                    success: function(r) {
                        toastr.success('Success! - Order Stage Set Successfully');
                        setTimeout(function() {
                            window.location.href = window.location.href;
                        }, 3000);
                    }
                });
			} 

			if (stage_id != '8' || stage_id != '9') 
			{

			}else {
				$("#order_id").val(order_id);
				$("#stage_id").val(stage_id);
				$("#cancel_comment_info").html();
				$('#cancel_comment_modal_info').modal('show');
			}

		}
//        else {
//                $("#order_id").val(order_id);
//                $("#stage_id").val(stage_id);
//                $("#cancel_comment_info").html();
//                $('#cancel_comment_modal_info').modal('show');
//        }   

}

</script>   
<script>
$(function() {
  $('#search_daterange').daterangepicker({
    ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last Week': [moment().subtract(1, 'weeks'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
});
</script>
