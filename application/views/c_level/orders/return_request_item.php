<?php
if (!empty($order_id)) {
        $get_return_info = $this->db->select('*')->from('order_return_tbl a')->where('a.order_id', $product_details->order_id)->get()->row();
        $return_id = @$get_return_info->return_id;
        $get_return_details_info = $this->db->select('a.*, sum(a.return_qty) as total_return')
                        ->from('order_return_details a')
                        ->where('a.return_id', $return_id)
                        ->where('a.product_id', $product_details->product_id)
                        ->get()->result();
        $width_fraction = $this->db->where('id', $product_details->width_fraction_id)->get('width_height_fractions')->row();
        $height_fraction = $this->db->where('id', $product_details->height_fraction_id)->get('width_height_fractions')->row();?>    
        <tr id="product_<?=$product_details->product_id?>">
            <td> 
                <a href="javascript:void(0)" onClick="javascript:viewOrderHtml('<?=$product_details->order_id?>', '<?=$product_details->row_id?>')"><strong><?= $product_details->product_name; ?></strong><br />
                <?php
                    if ($product_details->pattern_name) {
                        echo $product_details->pattern_name . '<br/>';
                    }
                    ?>
                W <?= $product_details->width; ?> <?= @$width_fraction->fraction_value ?>,
                H <?= $product_details->height; ?> <?= @$height_fraction->fraction_value ?>,
                <?= $product_details->color_number; ?>
                <?= $product_details->color_name . ', '; ?>

                <?php
                    if (isset($selected_rooms[$product_details->room])) {
                        echo $product_details->room . ' ' . $selected_rooms[$product_details->room];
                        $selected_rooms[$product_details->room]++;
                    } else {
                        echo $product_details->room . ' 1';
                        $selected_rooms[$product_details->room] = 2;
                    }
                    ?> </a>
                <input type="hidden" name="product_id[]" value="<?= ($product_details->product_id) ?>">
            </td>
            <td class="">
                <input type="number" name="quantity" value="<?= $product_details->product_qty; ?>" id="quantity_<?php echo $i; ?>"  class="form-control text-center" readonly>
            </td>
            <td class="text-right">
                <input type="text" name="return_qty[]" value="<?= $return_qty; ?>" class="form-control text-right" readonly>
            </td>
            <td class="text-right">
                <input type="text" name="return_comments[]" class="form-control text-right" value='<?= $return_comments; ?>' readonly>
            </td>
            <td class="text-left">
                <?php if($return_type == 1) {
                    echo "Replace";
                } else {
                    echo "Repair";
                } ?>
                <input type="hidden" name="return_type[]" class="form-control text-right" value='<?= $return_type; ?>'>
            </td>
            <td class="text-right">
                <?php 
                    if($return_img != '') {
                        $imgArr = explode(',', $return_img);
                        foreach($imgArr as $img) { ?>
                            <a href="<?php echo base_url(); ?>assets/c_level/uploads/customers/return/<?=$img?>" target="_blank"><?=$img;?></a><br/>
                        <?php } ?>
                            <input type="hidden" name="return_img[]" value="<?=$return_img?>">
                    <?php } ?>
            </td>
            <td class="text-right">
                <a href="javascript:void(0)" class="delete-product btn btn-danger default btn-xs" data-product-id="<?=$product_details->product_id;?>"><i class="glyph-icon simple-icon-trash"></i></a>
            </td>
        </tr>
    <?php
}
?>