<table class="table table-bordered mb-3">
    <thead>
        <tr>
            <th>Sl no.</th>
            <th>Tag Name</th>
            <th width="15%">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0 + $pagenum;
        if (!empty($tags)) {
            foreach ($tags as $key => $val) {
                $i++;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $val->tag_name; ?></td>
                    <td width="100">
                        <a href="javascript:void(0)" class="btn btn-warning default btn-xs edit_tag" id="edit_tag" data-data_id="<?= $val->id ?>" ><i class="simple-icon-pencil"></i></a>
                        <a href="<?php echo base_url('c_level/tag_controller/delete_tag/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-xs" ><i class="glyph-icon simple-icon-trash"></i></a>
                    </td>
                </tr>
        <?php } } ?>
    </tbody>
    <?php if (empty($tags)) { ?>
        <tfoot>
            <tr>
                <th colspan="4" class="text-center text-danger">No record found!</th>
            </tr> 
        </tfoot>
    <?php } ?>
</table>