<style type="text/css">
    .or_cls{
        font-size: 8px;
        margin-top: 14px;
        font-weight: bold;
    }    
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Customer List</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Tag List';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                   <?php
                        $user_data=access_role_permission(74);
                        $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Tag List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <p class="mb-0">
                    <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#importTag">Bulk Import Upload</button>
                    <a href="javascript:void" class="btn btn-success" id="add_new_tag">Add Tag</a>
                    <?php }?>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="col-sm-12 text-right">
                            <div class="form-group row">
                                <label for="keyword" class="col-sm-2 col-form-label offset-3 offset-md-7 text-right mb-3"></label>
                                <div class="col-sm-4 col-md-3 mb-3">
                                    <input type="text" class="form-control" name="keyword" id="keyword" onkeyup="tagkeyup_search()" placeholder="Search..." tabindex="">
                                </div>
                            
                            </div>          
                        </div>
                        <?php // dd($get_customer); ?>
                        <div id="results_color">
                            <table class="table table-bordered mb-3">
                                <thead>
                                    <tr>
                                        <th>Sl no.</th>
                                        <th>Tag Name</th>
                                        <th width="15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0 + $pagenum;
                                    if (!empty($tags)) {
                                        foreach ($tags as $key => $val) {
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?= $i ?></td>
                                                <td><?= $val->tag_name; ?></td>
                                                <td width="100">
                                                    <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="javascript:void(0)" class="btn btn-warning default btn-xs edit_tag" id="edit_tag" data-data_id="<?= $val->id ?>" ><i class="simple-icon-pencil"></i></a>
                                                    <?php } ?>
                                                     <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                                    <a href="<?php echo base_url('c_level/tag_controller/delete_tag/'); ?><?= $val->id ?>" onclick="return confirm('Are you sure want to delete it')" class="btn btn-danger btn-xs" ><i class="glyph-icon simple-icon-trash"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                    <?php } } ?>
                                </tbody>
                                <?php if (empty($tags)) { ?>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4" class="text-center text-danger">No record found!</th>
                                        </tr> 
                                    </tfoot>
                                <?php } ?>
                            </table>
                        </div>    
                        <?php echo $links; ?>
                    </div>

                    <!-- Modal : START -->
                    <div class="modal fade" id="importTag" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tags Bulk Upload</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">

                                    <a href="<?php echo base_url('assets/c_level/csv/tag_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                                    <span class="text-primary">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br><br>

                                    <?php echo form_open_multipart('retailer-import-tag-save', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                                    <div class="form-group row">
                                        <label for="upload_csv_file" class="col-xs-2 control-label">File *</label>
                                        <div class="col-xs-6">
                                            <input type="file" name="upload_csv_file" id="upload_csv_file" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group  text-right">
                                        <button type="submit" class="btn btn-success w-md m-b-5">Import</button>
                                    </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal : END -->

                </div>
            </div>
        </div>
    </div>
</div>
</main>



<!-- end content / right -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_tag').on('click', function () {
            $('#add_tag')[0].reset();
            $("#add_tag").attr("action", 'c_level/tag_controller/save_tag');
            $('#pattern_id').val('').trigger('change');
            $('#myTag').modal('show');
            //  $('.modal-title').text('Add New Color');
        });


        $('.edit_tag').on('click', function () {

            var id = $(this).attr('data-data_id');
            var submit_url = "<?php echo base_url(); ?>" + "c_level/tag_controller/get_tag/" + id;

            $.ajax({
                type: 'GET',
                url: submit_url,
                success: function (res) {

                    var data = JSON.parse(res);

                    $('#add_tag')[0].reset();

                    $('[name="id"]').val(data.id);
                    $('[name="tag_name"]').val(data.tag_name);

                    $("#add_tag").attr("action", 'c_level/tag_controller/update_tag')

                    $('#myTag').modal('show');
                    $('.modal-title').text('Update Tag');
                    $('#save').text('Update Tag');


                }, error: function () {

                }
            });
        });
    });

    function tagkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>retailer-tag-search",
            type: 'post',
            data: {keyword: keyword},
            success: function (r) {
                $("#results_color").html(r);
            }
        });
    }
</script>
<?php
$this->load->view('c_level/tag/tagJs');
?>




