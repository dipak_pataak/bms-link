
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>Customer Bulk Upload</h1>
                    <?php
                        $page_url = $this->uri->segment(1);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Customer Bulk';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Bulk</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                    <?php
                            $user_data=access_role_permission(40);
                            $log_data=employ_role_access($id);
                                                                
                    ?>

                <div class="card mb-4 bulk_upload">
                    <div class="row">
                        <div class="col-sm-7">
                            <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span>
                        </div>
                        <div class="col-sm-5">
                            <a href="<?php echo base_url('assets/c_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo form_open_multipart('retailer-customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="file" class="attatch_file" name="upload_csv_file" id="" required> 
                                <small id="fileHelp" class="text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                            <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Submit</button>
                            <?php }  ?>

                        </div>
                        </form>                
                    </div>
                </div>
                <!--                <div class="card mb-4">
                                    <a href="<?php echo base_url('assets/c_level/csv/customer_csv_sample.csv') ?>" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download Sample File</a>
                                    <span class="text-warning">The first line in downloaded csv file should remain as it is. Please do not change the order of columns.</span><br>
                                    <div class="card-body">
                <?php echo form_open_multipart('retailer-customer-csv-upload', array('class' => 'form-vertical', 'id' => 'validate', 'name' => '')) ?>
                                        <div class="form-group text-center">
                                            <div class="col-sm-12">
                                                <input type="file" class="attatch_file" name="upload_csv_file" id="" required> 
                                                <small id="fileHelp" class="text-muted"></small>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-sm w-md m-b-5">Submit</button>
                                        </div>
                <?php echo form_close() ?>
                                    </div>
                                </div>-->
            </div>

        </div>
    </div>
</main>
