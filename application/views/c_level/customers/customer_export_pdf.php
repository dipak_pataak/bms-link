<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4 style="text-align: center;">Customer List</h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <!--dataTableExample3-->
                    <table  border="1" width="100%" style="margin-top:25px;border-collapse:collapse;">
                        <tr>
                            <th width="5%">ID</th>
                            <th width="15%">FName</th>
                            <th width="10%">LName</th>
                            <th width="10%">Side Mark</th>
                            <th width="15%">Phone</th>
                            <th width="15%">Email</th>
                            <th width="30%">Address</th>
                        </tr>
                        <?php
                        $sl = 0;
                        foreach ($customer_list as $customer) {
                            $sl++;
                            ?>
                            <tr>
                                <td><?php echo $customer->customer_id; ?></td>
                                <td><?php echo $customer->first_name; ?></td>
                                <td><?php echo @$customer->last_name; ?></td>
                                <td>
                                    <?php echo $customer->side_mark; ?>
                                </td>
                                <td>
                                    <?php echo $customer->phone; ?>
                                </td>
                                <td>
                                    <?php echo $customer->email; ?>
                                </td>
                                <td>
                                    <?php echo $customer->address . '<br>' . @$customer->city . ', ' . @$customer->state . ', ' . @$customer->zip_code . ', ' . @$customer->country_code; ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>