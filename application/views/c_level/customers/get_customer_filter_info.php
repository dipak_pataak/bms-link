<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
		padding: 0 5px;
	}
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
			flex: 0 0 35px;
			max-width: 35px;
			text-align: center;
    } 
    .phone_email_link{color: #007bff;}
	@media only screen and (min-width: 580px) and (max-width: 994px) {
		.or-filter .col-sm-2 {
			flex: 0 0 18.7%;
			max-width: 18.7%;
			padding: 0 5px;
		}
	}
	@media only screen and (max-width:580px) {
		.or-filter div {
			flex: 0 0 100%;
			max-width: 100%;
			margin: 0 0 10px;
			text-align: center;
		}
		.or-filter div:last-child {
			margin-bottom: 0px;
		}
	}
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Customer List</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Customer List</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">

                <p class="mb-0">
                    <!--                    <button class="btn btn-primary default mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                            Filter
                                        </button>-->
                </p>
                <div class="collapsexxx" id="collapseExample">
                    <div class="p-4 border mt-4">
                        <form class="form-horizontal" action="<?php echo base_url(); ?>retailer-customer-filter" method="post">
                            <fieldset>
                                <div class="row or-filter">
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control mb-3 first_name" name="first_name" value="<?php echo $first_name; ?>" placeholder="First Name">
                                    </div>
                                    <div class="or_cls">-- OR --</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control mb-3 sidemark" name="sidemark" value="<?php echo $sidemark; ?>" placeholder="Sidemark">
                                    </div>
                                    <div class="or_cls">-- OR --</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control mb-3 phone" name="phone" value="<?php echo $phone; ?>" placeholder="Phone">
                                    </div>
                                    <div class="or_cls">-- OR --</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control mb-3 address" name="address" value="<?php echo $address; ?>" placeholder="Address">
                                    </div>
                                    <div class="col-sm-3 col-md-2">
                                        <div>
                                            <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                    <button type="submit" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-12 text-right">
                            <div class="form-group row m-0 mb-3 search_div float-right">
                                <label for="keyword" class="mb-2"></label>
                                    <input type="text" class="form-control mb-2" name="keyword" id="keyword" onkeyup="customerkeyup_search()" placeholder="Search..." tabindex="">
                                    <button class="btn btn-info dropdown-toggle mb-2" type="button" data-toggle="dropdown"><i class="fa fa-list"> </i> Action
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>retailer-customer-export-csv" class="dropdown-item">Export to CSV</a></li>
                                        <li><a href="<?php echo base_url(); ?>retailer-customer-export-pdf" class="dropdown-item">Export to PDF</a></li>
                                        <li><a href="javascript:void(0)" class="dropdown-item action-delete" onClick="return action_delete(document.recordlist)" >Delete</a></li>
                                    </ul>
                            </div>          
                        </div>
                        <?php // dd($get_customer); ?>
                         <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Customer_controller/manage_action') ?>">
                            <input type="hidden" name="action">
							<div class="table-respoonsive">
                        <table class="datatable2 table table-bordered table-hover" id="result_search">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="SellectAll"/></th>
                                    <th width="4%">SL#</th>
                                    <th width="11%">Name</th>
                                    <th width="9%">Sidemark</th>
                                    <th width="14%">Phone</th>
                                    <th width="20%">Address</th>
                                    <th width="11%">Comment</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($get_customer_filter_info) {
                                    $sl = 0;
                                    foreach ($get_customer_filter_info as $customer) {
                                        $sl++;
                                        $pre = $this->db->select('remarks')->where('customer_id', $customer->customer_id)->order_by('appointment_id', 'DESC')->get('appointment_calendar')->row();
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="Id_List[]" id="Id_List[]" value="<?= $customer->customer_id; ?>" class="checkbox_list">  
                                            </td>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $customer->first_name . " " . @$customer->last_name; ?></td>
                                            <td><?php echo $customer->side_mark; ?></td>
                                            <td>
                                                <a href="tel:<?php echo $customer->phone; ?>" class="phone_email_link"><?php echo $customer->phone; ?></a>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" id="address_<?php echo $customer->customer_id; ?>" class="address phone_email_link" onclick="show_address_map(<?php echo $customer->customer_id; ?>);">
                                                    <?php echo $customer->address; ?><br>
                                                    <?php echo $customer->city; ?>, <?php echo $customer->state; ?>, <?php echo $customer->zip_code; ?>, <?php echo $customer->country_code; ?>
                                                </a>
                                            </td>
                                            <td><?php echo @$pre->remarks; ?></td>
                                            <td>
                                                <select class="form-control inner_table" name="type" id="sta" onchange="show_customer_record(<?php echo $customer->customer_id; ?>);">
                                                    <option value="website" <?= ($customer->now_status == 'website' ? 'selected' : ''); ?> >Website</option>
                                                    <option value="walkin" <?= ($customer->now_status == 'walkin' ? 'selected' : ''); ?> >Walkin</option>
                                                    <option value="scheduled" <?= ($customer->now_status == 'scheduled' ? 'selected' : ''); ?> >Scheduled</option>
                                                    <option value="re-schedule" <?= ($customer->now_status == 're-schedule' ? 'selected' : ''); ?>>Re-Scheduled</option>
                                                    <option value="cancelled" <?= ($customer->now_status == 'cancelled' ? 'selected' : ''); ?>>Cancelled</option>
                                                    <option value="active" <?= ($customer->now_status == 'active' ? 'selected' : ''); ?>>Active</option>
                                                    <option value="inactive" <?= ($customer->now_status == 'inactive' ? 'selected' : ''); ?>>Inactive</option>
                                                </select>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>customer-view/<?php echo $customer->customer_id; ?>" class="btn btn-success btn-xs default" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                    <i class="simple-icon-eye"></i>
                                                </a>
                                                <a href="<?php echo base_url(); ?>customer-edit/<?php echo $customer->customer_id; ?>" class="btn btn-warning default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="simple-icon-pencil"></i></a>
                                                <button class="btn btn-danger default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="glyph-icon simple-icon-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <?php if (empty($get_customer_filter_info)) { ?>
                                <tfoot>
                                <th colspan="8" class="text-danger">Record not found!</th>
                                </tfoot>
                            <?php } ?>
                        </table>
							</div>
						</form>   
                        <?php //echo $links;    ?>
                    </div>
                    <div class="modal fade" id="customer_address_modal_info" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Customer Address Map Show</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body" id="customer_address_info">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
//    $(document).ready(function () {
//        $(".inner_table").on("change", function () {
//            $modal = $('#appointschedule');
//            if ($(this).val() === 'schedule') {
//                $modal.modal('show');
//            }
//        });
//    });
    function show_customer_record(t) {
//    $("#appointschedule").modal('show');
        $.post("<?php echo base_url(); ?>show-customer-record/" + t, function (t) {
            $("#customer_info").html(t);
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
//        ======== its timer =========== format: 'LT'
            $("[data-header-left='true']").parent().addClass("pmd-navbar-left");
            $('#datepicker-left-header').datetimepicker({
                'format': "HH:mm", // HH:mm:ss
            });
            var st = $('#sta').val();
            $('#status').val(st);
            $("form :input").attr("autocomplete", "off");
            $("#appointschedule").modal('show');
        });
    }
        //    ========== its for customer search ======
    function customerkeyup_search() {
        var keyword = $("#keyword").val();
        $.ajax({
            url: "<?php echo base_url(); ?>retailer-customer-search",
            type: 'post',
            data:{keyword:keyword},
            success: function (r) {
//                console.log(r);
                $("#result_search").html(r);
            }
        });
    }
      //============== its for show_address_map ==========
    function show_address_map(id) {
        var address = $("#address_" + id).text();
        var location = $.trim(address)
        $.post("<?php echo base_url(); ?>retailer-show-address-map/" + id, function (t) {
            $("#customer_address_info").html(t);
            $('#customer_address_modal_info').modal('show');
            $(".modal-title").text(location);
        });
    }
</script>
<script type="text/javascript">
    $('#s1').hide();
    $('#c1').hide();
    function viewFild(id) {
        if (id == 1) {
            $('#s1').show();
            $('#c1').hide();
        } else if (id == 2) {
            $('#s1').show();
            $('#c1').show();

        } else {
            $('#s1').hide();
            $('#c1').hide();
        }
    }
</script>
