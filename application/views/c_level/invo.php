<html>
    <!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Order Recover</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style type="text/css">
            body{
                width: 80%;
                margin: 0 auto;
            }
            .welcome_header{
                background: #2f4f4f;
                height: 80px;
            }
            .welcome_header h3{
                text-align: center;
                color: #fff;
                padding: 25px 0px;
                font-size: 25px;
            }
            table {
                border-collapse: collapse;
            }

            table, td, th {
                border: 1px solid black;
            }
            .invoice-data-table td {
                padding: .50rem;
            }
            .order-item-table th, .order-item-table td{
                vertical-align: bottom;
                text-align: center;
                padding: .50rem;
            }
        </style>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <div class="welcome_header">
                    <h3 class="text-center">Thank you for your order</h3>
                </div>
            </div>


            <div class="row" style="width: 100%; float: left;" >

                <div class="form-group col-md-6 address-section" style="float: left; margin-top: 50px;">
                    <div style="width:200px; height: 120px;">
                        <img height="60px" src="<?php echo base_url('assets/c_level/uploads/appsettings/').$company_profile->logo;?>">
                    </div>
                    <span><?=$company_profile->company_name;?></span><br/>
                    <span><?=$company_profile->address;?></span><br/>
                    <span><?=$company_profile->city;?>, <br/>
                    <?=$company_profile->state;?>, <?=$company_profile->zip_code;?>, 
                    <?=$company_profile->country_code;?></span><br/>
                    <span><?=$company_profile->phone;?></span><br/>
                    <span><?=$company_profile->email;?></span><br/>
                </div>
                
                <div class="form-group col-md-5" style="float: right; margin-top: 50px;">

                    <table class="table table-bordered mb-4 invoice-data-table" border="1" cellpadding="0" cellspacing="0">
                    
                        <tr class="text-center">
                            <td style="text-align: center;padding: .50rem;">Order Date</td>
                            <td style="text-align: center;padding: .50rem;"><?=date_format(date_create($orderd->order_date),'M-d-Y');?></td>
                        </tr>

                        <tr class="text-center">
                            <td style="text-align: center;padding: .50rem;">Order Id</td>
                            <td style="text-align: center;padding: .50rem;"><?=$orderd->order_id?></td>
                        </tr>

                        <tr class="text-center">
                            <td style="text-align: center;padding: .50rem;">Customer Name</td>
                            <td style="text-align: center;padding: .50rem;"><?=$orderd->customer_name?></td>
                        </tr>

                        <tr class="text-center">
                            <td style="text-align: center;padding: .50rem;">Customer Address</td>
                            <td style="text-align: center;padding: .50rem;">
                                <span><?=$orderd->address;?></span><br/>
                                <span><?=$orderd->city;?>, <br/>
                                <?=$orderd->state;?>, <?=$orderd->zip_code;?>, 
                                <?=$orderd->country_code;?></span><br/>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td style="text-align: center;padding: .50rem;">Barcode</td>
                            <td  style="text-align: center;padding: .50rem;" width="250px">
                                <?php
                                if ($orderd->barcode != NULL) {
                                    echo '<img src="' . base_url() . $orderd->barcode . '" width="100%" style="height:50px;"/>';
                                } 
                                ?>
                            </td>
                        </tr>
                    </table>

                    <!-- <p>Please click the url, Pay for this order!</p>
                    <a href="<?php echo base_url(); ?>c_level/customer_payment/payment?order_id=<?=$orderd->order_id;?>&customer_id=<?=$customer_info->customer_id?>" >Click Here</a> -->


                </div>
            </div>


            <div class="row" style="width: 100%; float: left;">
        
                <h3>Order Details</h3>
                    <table class="table table-bordered mb-4 order-item-table" width="100%" border="1" cellpadding="0" cellspacing="0">

                        <thead>
                            <tr>
                                <th style="text-align: center;padding: .50rem;">Item.</th>
                                <th style="text-align: center;padding: .50rem;">Qty</th>
                                <th style="text-align: center;padding: .50rem;" width="400px;">Description</th>
                                <th style="text-align: center;padding: .50rem;">List</th>
                                <th style="text-align: center;padding: .50rem;">Discount(%)</th>
                                <th style="text-align: center;padding: .50rem;">Price</th>
                                <th style="text-align: center;padding: .50rem;">Notes</th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php $i = 1; ?>
                            <?php 
                            foreach ($order_details as $items): 
                                $width_fraction = $this->db->where('id',$items->width_fraction_id)->get('width_height_fractions')->row();
                                $height_fraction = $this->db->where('id',$items->height_fraction_id)->get('width_height_fractions')->row();
                               
                            ?>

                                <tr>

                                    <td style="text-align: center;padding: .50rem;"><?=$i?></td>
                                    <td style="text-align: center;padding: .50rem;"><?=$items->product_qty;?></td>
                                    <td style="text-align: center;padding: .50rem;">
                                    <strong><?=$items->product_name;?></strong><br/> 
                                    <?php 
                                    if($items->pattern_name){
                                        echo $items->pattern_name.'<br/>';
                                    }
                                    ?>
                                    W <?=$items->width;?> <?=@$width_fraction->fraction_value?>, 
                                    H <?=$items->height;?> <?=@$height_fraction->fraction_value?>, 
                                    <?=$items->color_number;?> 
                                    <?=$items->color_name;?>
                                    <?php
                                        $attributess = (json_decode($items->product_attribute));
                                
                                    ?>
                                    </td>
                                    
                                    <td style="text-align: center;padding: .50rem;"><?=$company_profile->currency;?><?=number_format($items->list_price,2)?></td>
                                    <td style="text-align: center;padding: .50rem;"><?=($items->discount)?>%</td>
                                    <td style="text-align: center;padding: .50rem;"><?=$company_profile->currency;?> <?=@number_format($items->unit_total_price,2)?> </td>
                                    <td style="text-align: center;padding: .50rem;"><?=$items->notes?></td>
                                    
                                </tr>

                                <?php $i++; ?>

                            <?php endforeach; ?>
                                
                        </tbody>
                    </table>

            </div>

            <br/>


            <div class="row" style="width: 100%; float: left; margin-top: 20px;">

                <table class="datatable2 table table-bordered mb-4 order-item-table" width="100%" border="1" cellpadding="0" cellspacing="0">

                    <thead>
                        <tr>
                           
                            <th style="text-align: center;padding: .50rem;">Sales Tax</th>
                            <th style="text-align: center;padding: .50rem;">Sub-Total</th>
                            <th style="text-align: center;padding: .50rem;">Installation Charge</th>
                            <th style="text-align: center;padding: .50rem;">Other Charge</th>
                            <th style="text-align: center;padding: .50rem;">Misc</th>
                            <th style="text-align: center;padding: .50rem;">Discount</th>
                            <th style="text-align: center;padding: .50rem;">Grand Total</th>
                            <th style="text-align: center;padding: .50rem;">Deposit</th>
                            <th style="text-align: center;padding: .50rem;">Due </th>
                        </tr>
                    </thead>

                    <tbody>
                        
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->state_tax,2)?></td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->subtotal,2)?> </td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->installation_charge,2)?> </td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->other_charge,2)?></td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->misc,2)?> </td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->invoice_discount,2)?></td>
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->grand_total,2)?> 
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->paid_amount,2)?> 
                        <td style="text-align: center;padding: .50rem;"> <?=$company_profile->currency;?> <?=number_format($orderd->due,2)?> 
                    </tbody>
                </table>

            </div>


            <!-- <div class="row" style="float: left; width: 100%; margin-top: 50px;">
                <div class="">

                    <p>
                        Regards,<br>

                        <a href="https://www.bdtask.com/">
                            BDtask
                        </a><br>
                        Supports<br>
                        Mobile: +88-01817-584639<br>
                    </p>
                    
                </div>
            </div>

            <div class="row" style="float: left; width: 100%;">
                <div class="welcome_header"></div>
            </div> -->

        </div>
    </body>

</html>
