<style type="text/css">
    ul li{
        list-style: none;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Users</h1>
                    <?php
                    $page_url = $this->uri->segment(1);
                    $get_favorite = get_c_favorite_detail($page_url);
                    $class = "notfavorite_icon";
                    $fav_title = 'Users';
                    $onclick = 'add_favorite()';
                    if (!empty($get_favorite)) {
                        $class = "favorites_icon";
                        $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                    }
                    ?>
                    <?php
                            $user_data=access_role_permission(33);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Users</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 mb-4">
                 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
                <a href="<?php echo base_url(); ?>add-user" class="btn btn-success btn-sm" style="margin: 5px;">Add User</a>
             <?php } ?>
                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
						<div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Fixed Commission</th>
                                    <th>Commission (%)</th>
                                    <th>Assigned Roles</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //dd($get_users);
                                $sl = 0;
                                foreach ($get_users as $user) {
                                    $user_roles = $this->db->select('b.role_name')->from('user_access_tbl a')
                                                    ->join('role_tbl b', 'b.id = a.role_id', 'left')
                                                    ->where('a.user_id', $user->id)->get()->result();
                                    $sl++;
                                    ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $user->first_name . " " . $user->last_name; ?></td>
                                        <td><?php echo $user->phone; ?></td>
                                        <td><?php echo $user->email; ?></td>
                                        <td><?php echo $user->fixed_commission; ?></td>
                                        <td><?php echo $user->percentage_commission; ?></td>
                                        <td>
                                            <ul>
                                                <?php foreach ($user_roles as $role){ ?>
                                                <li><?php echo $role->role_name; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                            <a href="<?php echo base_url(); ?>user-edit/<?php echo $user->id; ?>" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="simple-icon-note" aria-hidden="true"></i></a>
                                            <?php  }?>
                                            <?php if($user_data[0]->can_delete==1 or $log_data[0]->is_admin==1){ ?>
                                            <a href="<?php echo base_url(); ?>c_level/user_controller/delete_user/<?php echo $user->id; ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="simple-icon-trash" aria-hidden="true"></i></a>
                                             <?php  }?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <?php if (empty($get_users)) { ?>
                                <tfoot>
                                    <tr>
                                        <th class="text-center text-danger" colspan="6">Record not found!</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</main>
