
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="mb-3">
                    <h1>User Edit</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">User Edit</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row form-fix-width">

            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php
//                        echo $user_edit[0]['c_level_id'];
//                        echo '<pre>'; print_r($user_edit); die(); ?>
                        <form action="<?php echo base_url(); ?>user-update/<?php echo $user_edit[0]['id']; ?>" method="post">

                            <div class="form-group row">
                                <label for="first_name" class="col-sm-3 col-form-label">First Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control alphaonly" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $user_edit[0]['first_name']; ?>" required onkeyup="required_validation()"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="last_name" class="col-sm-3 col-form-label">Last Name <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control alphaonly" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $user_edit[0]['last_name']; ?>" required onkeyup="required_validation()">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Email <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="<?php echo $user_edit[0]['email']; ?>" required onkeyup="required_validation()">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Password <i class="text-danger">*</i></label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Fixed Commission</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control NumbersAndDot" name="fixed_commission" id="fixed_commission"  placeholder="Fixed Commission" value="<?php echo $user_edit[0]['fixed_commission']; ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-form-label">Commission (%)</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control NumbersAndDot percentage_valid" name="percentage_commission" id="percentage_commission"  placeholder="Commission (%)" value="<?php echo $user_edit[0]['percentage_commission']; ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                                <div class="col-sm-6">
                        <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-primary btn-sm text-white">Back</a>
                                    <input type="submit" id="add-customer" class="btn btn-success btn-sm" name="add_user" value="Update" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>

<script>
    $('.NumbersAndDot').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

    $('.percentage_valid').keyup(function(){
      if ($(this).val() > 100){
        $(this).val('100');
      }
    });

    function required_validation() {
        console.log("ihh");
        if ($("#first_name").val() != '' && $("#last_name").val() != '' && $("#email").val() !='') {
            if(isEmail($("#email").val())){
                $('#add-customer').prop('disabled', false);
                return false;
            }else{
                $('#add-customer').prop('disabled', true);
            }    
        }else{
            $('#add-customer').prop('disabled', true);
        }
    }

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    $('.alphaonly').bind('keyup blur',function(){ 
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z\s]/g,'') ); }
    );
</script>    
