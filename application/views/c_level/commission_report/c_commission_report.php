 <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/b_level/resources/css/daterangepicker.css" />
<style>
    table tr th, table tr td {
        border-color: #222 !important;
    }
    @media (min-width: 992px){
        .daterangepicker {
            top : 215px !important;
        }
    }   
    @media (min-width: 768px) and (max-width: 991px){
        .daterangepicker {
            top : 235px !important;
        }
    } 
    @media (min-width: 576px) and (max-width: 767px){
        .daterangepicker {
            top : 290px !important;
        }
    } 
    @media (max-width: 575px){
        .daterangepicker {
            top : 265px !important;
        }
    } 
       
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Commission Report</h1>
                    <?php
                        $page_url = $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3);
                        $get_favorite = get_c_favorite_detail($page_url);
                        $class = "notfavorite_icon";
                        $fav_title = 'Commission Report';
                        $onclick = 'add_favorite()';
                        if (!empty($get_favorite)) {
                            $class = "favorites_icon";
                            $onclick = 'remove_favorite(' . $get_favorite['id'] . ',this)';
                        }
                    ?>
                    <input type="hidden" name="fav_page_url" id="fav_page_url" value="<?php echo $page_url; ?>" />
                    <input type="hidden" name="fav_title" id="fav_title" value="<?php echo $fav_title; ?>" />
                    <span class="fav_class <?php echo $class; ?>" onclick="<?php echo $onclick; ?>" data-page-url="<?php echo $page_url; ?>"><span class="star-icon"></span></span>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Commission</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
   
        <div class="card mb-4">
            <div class="card-body">
                <div class="p-4 border mt-4">
                    <form class="form-horizontal" action="<?php echo base_url('c_level/Commision_report_controller/search_c'); ?>" method="post" id="customerFilterFrm">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3">
                                    <input type="text" class="form-control mb-3 employee_name" placeholder="Employee Name" name="search_employee_name" id="employee_name" value="<?=$this->session->userdata('search_employee_name')?>">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control mb-3" id="search_daterange" name="search_daterange" value="<?=$this->session->userdata('search_daterange')?>" />
                                </div>
                                <div class="col-sm-4">
                                    <div>
                                        <button type="submit" class="btn btn-sm btn-success default" id="customerFilterBtn" name="Search" value="Search">Go</button>
                                        <button type="submit" class="btn btn-sm btn-danger default" name="All" value="All">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('c_level/Commision_report_controller/search_c') ?>">
                        <input type="hidden" name="action">
                        <table class="datatable2 table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee Name</th>
                                    <th>Date</th>
                                    <th>Total Commission</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $sl = 0 + $pagenum;
                                foreach ($commission_list as $commission) {
                                    $sl++;
                                    ?>
                                    <tr>
                                        <td><?php echo $sl; ?></td>
                                        <td><?php echo $commission->full_name; ?></td>
                                        <td><?php echo $commission->created_date; ?></td>
                                        <td><?php echo round($commission->total_commission,2); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <?php if (empty($commission_list)) { ?>
                                <tfoot>
                                    <tr>
                                        <th colspan="8" class="text-center  text-danger">No record found!</th>
                                    </tr> 
                                </tfoot>
                            <?php } ?>
                        </table>
                    </form>
                    <?php echo $links; ?>  
                </div>
            </div>
        </div>
    </div>
</main>    
	

<script>
$(function() {
  $('#search_daterange').daterangepicker({
  	ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last Week': [moment().subtract(1, 'weeks'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
  });
});
</script>