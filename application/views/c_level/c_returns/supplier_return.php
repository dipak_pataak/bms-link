<?php $get_company = $this->db->query("SELECT * FROM company_profile WHERE user_id".$this->user_id)->row();?>
<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <?php
                    $user_id = $this->session->userdata('user_id');
                    $user_role_info = $this->db->select('*')->from('user_access_tbl')->where('user_id',$user_id)->get()->result();
                    $id=$user_role_info[0]->role_id;
                    $user_data = $this->db->select('*')
                                            ->from('role_permission_tbl')
                                            ->where('menu_id =',93)
                                            ->where('role_id', $id)
                                            ->get()->result();
                    $log_data = $this->db->select('*')
                                            ->from('log_info')
                                          
                                            ->where('user_id', $user_id)
                                            ->get()->result();
                                                               
                    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Stock History</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Stock History
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

         <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <form class="form-horizontal" action="<?php echo base_url(); ?>c-supplier-return-filter" method="post">
                            <fieldset>
                                <div class="row or-filter">
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="invoice_no" placeholder="Invoice No.">
                                    </div>
                                    <div class="or_cls">-- OR --</div>
                                    <div class="col-sm-2">
                                        <select class="form-control select2" name="supplier_name" data-placeholder="-- select supplier --">
                                            <option value=""></option>
                                            <?php
                                            foreach ($get_supplier as $supplier) { ?>
                                                <option value='<?php echo $supplier->supplier_id; ?>'>
                                                    <?php echo $supplier->supplier_name; ?>
                                                </option>
                                           <?php  }                                    ?>
                                        </select>
                                    </div>
                                    <!--                            -- OR --
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="material_name" placeholder="Material Name">
                                    </div>-->
                                    <div class="col-sm-3">
                                            <button type="submit" class="btn btn-sm btn-success default">Go</button>
                                        <button type="button" class="btn btn-sm btn-danger default" onclick="field_reset()">Reset</button>
                                    </div>
                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>

        </div>




        <div class="row">
            <div class="col-xl-12 mb-4">

              
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3 float-right">
                                <label for="keyword" class="mb-2"></label>
                                
                            </div>
                        </div>


                                    <div class="table-responsive">
                                        <table class="table table-bordered text-center" id="results">
                                            <thead>
                                                <tr>
                                                    <th>SL No.</th>
                                                    <th>Purchase Invoice</th>
                                                    <th>Supplier Name</th>
                                                    <th>Material</th>
                                                    <th>Qty</th>
                                                    <th>Comment</th>
                                                    <th>Returned By</th>
                                                    <th>Approved By</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                    $sl_no = 0;
                    foreach ($get_raw_material_supplier_return as $single) {

                        $this->db->select('a.*, a.raw_material_id, b.material_name');
                        $this->db->from('raw_material_return_details_tbl a');
                        $this->db->join('row_material_tbl b', 'b.id = a.raw_material_id');
                        $this->db->where('a.return_id', $single->return_id);
                        $material_info_query = $this->db->get()->result();
//                        echo '<pre>';                        print_r($material_info_query);die();

                        $sl_no++;
                        ?>
                        <tr>
                            <td><?php echo $sl_no; ?></td>
                            <td><?php echo $single->purchase_id; ?></td>                            
                            <td><?php echo $single->supplier_name; ?></td>                            
                            <td>
                                <?php
                                $sl = 0;
                                foreach ($material_info_query as $material_single) {
                                    $sl++;
                                    echo "<p>";
                                    echo $sl . ". " . $material_single->material_name;
                                    echo "</p>";
                                }
                                ?>
                            </td>                            
                            <td>
                                <?php
                                $sl = 0;
                                foreach ($material_info_query as $material_single) {
                                    echo "<p>";
                                    echo $material_single->return_qty;
                                    echo "</p>";
                                }
                                ?>
                            </td>                            
                            <td><?php echo $single->return_comments; ?></td>            
                            <td>
                                <?php echo $single->name; ?>
                            </td>
                            <td>
                                <?php
                                if ($single->is_approved == 1) {
                                    echo $single->approve_name;
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($single->is_approved == 1) {
                                    echo "Approved";
                                } elseif ($single->is_approved == 0) {
                                    echo "Pending";
                                }
                                ?>
                            </td>
                            <td class="text-right">
                                <form action="" method="post" id="return_stock_frm">
                                    <?php
                                    foreach ($material_info_query as $single_return_details) {
                                        ?>
                                        <input type="hidden" name="raw_material_id[]" value="<?php echo $single_return_details->raw_material_id; ?>" class="form-control">
                                        <input type="hidden" name="pattern_model_id[]" value="<?php echo $single_return_details->pattern_model_id; ?>" class="form-control">
                                        <input type="hidden" name="color_id[]" value="<?php echo $single_return_details->color_id; ?>" class="form-control">
                                        <input type="hidden" name="return_qty[]" value="<?php echo $single_return_details->return_qty; ?>" class="form-control">
                                    <?php } ?>
                                    <input type="hidden" name="return_id" value="<?php echo $single->return_id; ?>">
                                    <?php if($user_data[0]->can_edit==1 or $log_data[0]->is_admin==1){ ?>
                                    <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" data-toggle="tooltip" data-placement="right" title="Approve " <?php
                                    if ($single->is_approved == 1) {
                                        echo "disabled";
                                    }
                                    ?>>
                                        <i class="fa fa-check-square" aria-hidden="true"></i>
                                    </button>
                                     <?php } ?>


                                </form>                                
                            </td>
    <!--                            <td>
                            <?php
                            if ($single->is_approved == 0) {
                                ?>
                                                                                                <a href="<?php echo base_url(); ?>raw-material-return-approved/<?= $single->return_id ?>" onclick="return confirm('Are you sure want to approve it? ')" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="right" title="Approve "><i class="fa fa-check-square" aria-hidden="true"></i></a>
                            <?php } elseif ($single->is_approved == 1) { ?>
                                                                                                <a href="<?php echo base_url(); ?>raw-material-return-rejected/<?= $single->return_id ?>" onclick="return confirm('Are you sure want to reject it? ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Rejected "><i class="fa fa-window-close" aria-hidden="true"></i></a>
                            <?php } ?>
                            </td>-->
                        </tr>
                    <?php } ?>
                                            </tbody>
                                            <?php if (empty($get_raw_material_supplier_return)) { ?>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="10" class="text-center text-danger">Record not found!</th>
                                                    </tr>
                                                </tfoot>
                                            <?php } ?>
                                        </table>
                                        </div>

                  
                    </div>

                </div>
            </div>
        </div>



    </div>
</main>


<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#return_approve_btn', function () {
            var frm = $("#return_stock_frm")[0];
            var form_data = new FormData(frm);
            $.ajax({
                url: "<?php echo base_url(); ?>c_level/C_return_controller/raw_material_return_purchase_approved",
//                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    return confirm("Are you sure approve it?");
                },
                success: function (data) {
                    $('.custom-success').css({'display': 'block'});
                    $('.alert-custom-txt').html("<i class='icon fa fa-warning'></i> Return Approved successfully!");
                    setTimeout(function () {
                        $('.custom-success').css({'display': 'none'});
                    }, 2000);
                    location.reload();
                }
            });
        });
    });
</script>
