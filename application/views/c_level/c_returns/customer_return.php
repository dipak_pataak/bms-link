<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Customer Return</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Return</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
       

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                    <?php
                            $user_data=access_role_permission(92);
                            $log_data=employ_role_access($id);
                                                                
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-right">
                                
                              
                            </div>
                        </div>


                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice No</th>
                                        <th>Side Mark</th>
                                        <th>Company</th>
                                        <th>Comments</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0 + $pagenum;
                                    foreach ($customer_return as $value) {
                        $com = $this->db->where('user_id',$value->c_u_id)->get('company_profile')->row();
						$blevel_order_id = $this->db->select('order_id')->where('clevel_order_id',$value->order_id)->get('b_level_quatation_tbl')->row();

                                        $sl++; ?>
                                        <tr>
                                            td><?php echo $sl; ?></td>
				                            <td><?php echo $blevel_order_id->order_id; ?></td>
				                            <td><?php echo $value->side_mark; ?></td>
				                            <td><?php echo @$com->company_name; ?></td>
				                            <td><?php echo $value->return_comments; ?></td>
				                            <td><?php echo date('M-d-Y', strtotime($value->return_date)); ?></td>
				                            <td>
				                                <?php
				                                if ($value->is_approved == 1) {
				                                    echo "Resend";
				                                } elseif ($value->is_approved == 0) {
				                                    echo "Pending";
				                                } elseif ($value->is_approved == 2) {
				                                    echo "Cancel";
				                                }
				                                ?>
				                            </td>
				                            <td class="text-right">
                                                 <?php if($user_data[0]->can_create==1 or $log_data[0]->is_admin==1){ ?>
				                                <button type="button" class="btn btn-success btn-sm" id="return_approve_btn" onclick="show_return_resend('<?php echo $value->return_id; ?>', '<?php echo $value->order_id; ?>','<?php echo $value->returned_by; ?>');" data-toggle="tooltip" data-placement="right" title="Resend " <?php
				//                                if ($value->is_approved == 1) {
				//                                    echo "disabled";
				//                                }
				                                ?>>
				                                    <i class="fa fa-share-square" aria-hidden="true"></i>
				                                </button>
                                                <?php } ?>

				                            </td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($customer_return)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</main>


<!-- end content / right -->
<script type="text/javascript">
    function show_return_resend(return_id, order_id, returned_by) {
        $.ajax({
            url: "<?php echo base_url().'c_level/C_return_controller/show_return_resend'; ?>",
            type: 'POST',
            data: {'return_id': return_id, 'order_id': order_id, 'returned_by' : returned_by},
            success: function (data) {
//                console.log(data);
                $("#resend_info").html(data);
                $('#resend_modal_info').modal('show');
            }

        });
    }

</script>
