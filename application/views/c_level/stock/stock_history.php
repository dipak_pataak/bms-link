<?php $get_company = $this->db->query("SELECT * FROM company_profile WHERE user_id".$this->user_id)->row();?>
<script src="//code.jquery.com/jquery.min.js"></script>
<style>
    .pac-container.pac-logo {
        top: 400px !important;
    }

    .pac-container:after {
        content: none !important;
    }

    .phone-input {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .phone_type_select {
        width: 85px;
        display: inline-block;
        border-radius: 0;
        vertical-align: top;
        background: #ddd;
    }

    .phone_no_type {
        display: inline-block;
        width: calc(100% - 85px);
        margin-left: -4px;
        border-radius: 0;
        vertical-align: top;
        line-height: 19px;
    }

    #normalItem tr td {
        border: none !important;
    }

    #addItem_file tr td {
        border: none !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Stock History</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Stock History
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

         <div class="row">
            <div class="col-xl-12 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="#">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <select name="material_id" class="form-control mb-3 select2" id="material_id" data-placeholder ='-- select one --'>
                                        <option value=""></option>
                                        <?php
                                        foreach ($get_raw_materials as $material) {
                                            echo "<option value='$material->id'>$material->material_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control mb-3 datepicker" name="from_date" id="from_date" placeholder="YYYY-MM-DD">
                                </div>
                             
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control mb-3 datepicker" name="to_date" id="to_date" placeholder="YYYY-MM-DD">
                                </div>

                            </div>
                            
                            <input type="button" class="btn btn-sm btn-success default" id="go_btn" onclick="filter_result()" value="Go">
                        </form>
                    </div>
                </div>
            </div>

        </div>




        <div class="row">
            <div class="col-xl-12 mb-4">

              
                <div class="card mb-4">
                    <div class="card-body">
                        <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3 float-right">
                                <label for="keyword" class="mb-2"></label>
                                
                            </div>
                        </div>


                                    <div class="table-responsive">
                                        <table class="table table-bordered text-center" id="results">
                                            <thead>
                                                <tr>
                                                    <th>SL No.</th>
                                                    <th>Raw Material</th>
                                                    <th>Pattern</th>
                                                    <th>Color</th>
                                                    <th>In Qty(<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                                                    <th>Out Qty(<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                                                    <th>Manufacturing</th>
                                                    <!-- <th>Measurement</th> -->
                                                    <th>Date</th>
                                                    <!--<th>Action</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sl = 0;
                                                foreach ($raw_material_stock_info as $single) {
                                                    $sl++;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $sl; ?></td>
                                                        <td><?php echo $single->material_name; ?></td>
                                                        <td><?php echo ($single->pattern_name!=null?$single->pattern_name:'n/a'); ?></td>
                                                        <td><?php echo ($single->color_name!=NULL?$single->color_name:'n/a'); ?></td>
                                                        <td><?php echo $single->in_qty; ?></td>
                                                        <td><?php echo $single->out_qty; ?></td>
                                                        <td><?php echo $single->from_module; ?></td>                       
                                                        <!-- <td><?php echo $single->measurment; ?></td>                        -->
                                                        <td><?php echo $single->stock_date; ?></td>                       
                                   <!--                        <td>
                                                         <a href="#" class="btn btn-warning default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="update"><i class="fa fa-pencil"></i></a>
                                                            <button class="btn btn-danger default btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></button>
                                                        </td>-->
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                            <?php if (empty($raw_material_stock_info)) { ?>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="10" class="text-center text-danger">Record not found!</th>
                                                    </tr>
                                                </tfoot>
                                            <?php } ?>
                                        </table>
                                        </div>

                  
                    </div>

                </div>
            </div>
        </div>



    </div>
</main>

<script type="text/javascript">
    function filter_result() {
        var material_id = $("#material_id").val();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        $.ajax({
            url: "c_level/Stock_controller/filter_result/",
            type: "POST",
            data: {material_id: material_id, from_date: from_date, to_date: to_date},
            success: function (r) {
                  $("#results").html(r);
            }
        });
    }
</script>