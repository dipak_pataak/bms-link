<style type="text/css">
    .or-filter .col-sm-3.col-md-2, .or-filter .col-sm-2 {
        padding: 0 5px;
    }
    .or_cls{
        font-size: 8px;
        margin-top: 8px;
        font-weight: bold;
        flex: 0 0 35px;
        max-width: 35px;
        text-align: center;
    }
    .address{
        cursor: pointer;
    }
    .phone_email_link{color: #007bff;}


    .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{
        top: 135.594px !important;
        left: 698.767px;
        z-index: 1060;
        display: block;
    }
    @media only screen and (min-width: 580px) and (max-width: 994px) {
        .or-filter .col-sm-2 {
            flex: 0 0 18.7%;
            max-width: 18.7%;
            padding: 0 5px;
        }
    }
    @media only screen and (max-width:580px) {
        .or-filter div {
            flex: 0 0 100%;
            max-width: 100%;
            margin: 0 0 10px;
            text-align: center;
        }
        .or-filter div:last-child {
            margin-bottom: 0px;
        }
    }
</style>
<?php $get_company=$this->db->query("SELECT * FROM company_profile WHERE user_id = ".$this->session->userdata('user_id'))->row();?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <h1>Stock Availability</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>retailer-dashboard">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Stock Availability</li>
                        </ol>
                    </nav>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
   <!--      <div class="card mb-4">
            <div class="card-body">

            </div>
        </div> -->

        <div class="row">
            <div class="col-xl-12 mb-4">

                <div class="">
                    <?php
                    $error = $this->session->flashdata('error');
                    $success = $this->session->flashdata('success');
                    if ($error != '') {
                        echo $error;
                    }
                    if ($success != '') {
                        echo $success;
                    }
                    ?>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        <!-- <div id="appointschedule" class="modal fade show" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalPopoversLabel">Appointment</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="customer_info">

                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-sm-12">
                            <div class="form-group row m-0 mb-3  float-right">
                              
                                
                            </div>
                        </div>


                            <div class="table-responsive">
                                <table class="datatable2 table table-bordered table-hover" id="result_search">
                                    <thead>
                                    <tr>
                                        <th>SL No.</th>
                                        <th>Raw Material</th>
                                        <th>Pattern</th>
                                        <th>Color</th>
                                        <th>In Qty/(Sq.<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                                        <th>Out Qty/(Sq.<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                                        <th>Available Qty(<?php if(!empty($get_company->unit)) echo $get_company->unit;?>)</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $sl = 0;
                                    foreach ($get_allrecord as $single) {
                                        $sl++;
                                        ?>
                                       <tr>
                                            <td><?php echo $sl; ?></td>
                                            <td><?php echo $single->material_name; ?></td>
                                            <td><?php echo ($single->pattern_name!=null?$single->pattern_name:'n/a'); ?></td>
                                            <td><?php echo ($single->color_name!=NULL?$single->color_name:'n/a'); ?></td>
                                            <td><?php echo $single->inqty; ?></td>
                                            <td><?php echo $single->outqty; ?></td>
                                            <td>
                                                <?php echo $availableqty = $single->inqty - $single->outqty; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <?php if (empty($get_allrecord)) { ?>
                                        <tfoot>
                                        <tr>
                                            <th class="text-center text-danger" colspan="9">Record not found!</th>
                                        </tr>
                                        </tfoot>
                                    <?php } ?>
                                </table>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
