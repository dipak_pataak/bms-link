

<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Chart.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/perfect-scrollbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/jquery.barrating.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/select2.full.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/nouislider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/Sortable.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/vendor/mousetrap.min.js"></script>
<!--========== its for chart of account tree ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/treeview.js"></script>
<!--=========== its for close ================-->
<!--=========== its for new quotation ================-->
<script src="<?php echo base_url(); ?>assets/c_level/plugins/theia-sticky-sidebar-master/dist/theia-sticky-sidebar.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>
<!--=========== its for new quotation close ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/dore.script.js"></script>
<script src="<?php echo base_url(); ?>assets/c_level/js/scripts.js"></script>
<!--=========== its for add customer phone format .js ================-->
<script src="<?php echo base_url(); ?>assets/c_level/js/phone-format.js"></script>
<!--=========== its for add customer phone format .js close ================-->
<!-- date time picker -->

<script src="<?php echo base_url() ?>assets/c_level/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
         $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                showOn: "focus",
            });
            
        $("form :input").attr("autocomplete", "off");

        var uri = "<?php echo $this->uri->segment(1) ?>";
        if (uri == 'account-chart' || uri == 'voucher-debit' || uri == 'voucher-credit' || uri == 'voucher-journal' || uri == 'contra-voucher' || uri == 'voucher-approval' || uri == 'cash-book' || uri == 'bank-book' || uri == 'cash-flow' || uri == 'voucher-reports' || uri == 'general-ledger' || uri == 'profit-loss' || uri == 'trial-balance') {
            $("#remove_active").removeClass("active");
            $("#account").addClass("active");
        }
        if (uri == 'add-customer' || uri == 'customer-list') {
            $("#remove_active").removeClass("active");
            $("#customer").addClass("active");
        }
        if (uri == 'new-quotation' || uri == 'manage-order' || uri == 'manage-invoice' || uri == 'order-cancel' || uri == 'customer-return' || uri == 'track-order') {
            $("#remove_active").removeClass("active");
            $("#order").addClass("active");
        }

    });
    $("main").on('click', function () {
        $("#app-container").addClass("menu-sub-hidden sub-hidden");
    })
</script>




</body>

</html>
