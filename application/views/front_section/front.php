<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>BMS</title>
  <!-- MDB icon -->
    <?php $baseurl = 'http://soft9.bdtask.com/bmslink_apps/'; ?>
  <link rel="icon" href="" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/fontawesome/css/all.min.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/mdb.css">
  <!-- Your custom styles -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/style.css">
  <!-- Smart Wizard -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/smart_wizard/css/smart_wizard.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/iziToast.min.css">
    <link href="<?php echo base_url(); ?>assets/phone_format/css/intlTelInput.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <!-- Header Start-->
  <header class="header header--external">
    <div class="container">
      <div class="header__logo">
        <a href="<?php echo base_url() ?>">
        <img src="<?php echo base_url(); ?>assets/front_section/img/logo/bms_logo-1.gif" class="" />
        </a>
      </div>
      <div class="header__progress-bar-container">
        <!-- <ul class="header__progress-bar is-step-1 ">
          <li class="header__progress-step step-1">
            <a href="#step-1">
              <div class="header__progress-step-label">1</div>
              <div class="header__progress-step-bar"></div>
              <div class="header__progress-step-title">Select package</div>
            </a>
          </li>
          <li class="header__progress-step step-2">
            <a href="#step-2">
              <div class="header__progress-step-label">2</div>
              <div class="header__progress-step-bar"></div>
              <div class="header__progress-step-title">Contact information</div>
            </a>
          </li>
          <li class="header__progress-step step-3">
            <a href="#step-3">
              <div class="header__progress-step-label">3</div>
              <div class="header__progress-step-bar"></div>
              <div class="header__progress-step-title">Payment</div>
            </a>
          </li>
        </ul> -->
      </div>
    </div>
  </header>
  <!-- Header Ends-->

  <!-- Main Start-->
  <main>

    <section class="py-0">
      <div style="background-image:url(<?php echo base_url(); ?>assets/front_section/img/hero-bg.jpg);" class="background-holder"></div>
      <div class="container">
        <div class="row justify-content-center py-5">
          <div class="col-xl-6 color-white ">
            <h2 class="font-weight-700 mb-4 pr-3" style="padding-top:80px">Existing Customer Login</h2>
            <a class="btn btn-lg btn-outline-white hvr-sweep-left font-16 px-6 waves-effect waves-light" href="retailer">Retailer</a>
            <a class="btn btn-lg btn-outline-white hvr-sweep-right font-16 px-6 waves-effect waves-light" href="wholesaler">Wholeseller</a>
            <!-- <h3 class="h3-responsive " style="padding-bottom:7px">Run a free scan to check your listings on:</h3>
            <div class="publishers-icon-container">
              <img src="<?php echo base_url(); ?>assets/front_section/img/logo/logo-Yahoo.png" class="publisher-icon yahoo">
              <img src="<?php echo base_url(); ?>assets/front_section/img/logo/logo-Google.png" class="publisher-icon google">
              <img src="<?php echo base_url(); ?>assets/front_section/img/logo/logo-yelp.png" class="publisher-icon yelp">
              <img src="<?php echo base_url(); ?>assets/front_section/img/logo/logo-facebook.png" class="publisher-icon facebook">
              <img src="<?php echo base_url(); ?>assets/front_section/img/logo/logo-bing.png" class="publisher-icon bing" style="margin-left:20px">
              <span class="publisher-icon more">&amp; More</span>
            </div> -->
          </div>
          <div class="col-xl-6">
<div id="smartwizard">
          <ul class="header__progress-bar">
          <li class="header__progress-step step-1">
            <a href="#step-1">
              <div class="header__progress-step-label">1</div>
              <div class="header__progress-step-bar"></div>
              <div class="header__progress-step-title">Select package</div>
            </a>
          </li>
          <li class="header__progress-step step-2">
            <a href="#step-2">
              <div class="header__progress-step-label">2</div>
              <div class="header__progress-step-bar"></div>

              <div class="header__progress-step-title">Contact information</div>
            </a>
          </li>
          <li class="header__progress-step step-3">
            <a href="#step-3">
              <div class="header__progress-step-label">3</div>
              <div class="header__progress-step-bar"></div>
              <div class="header__progress-step-title">Payment</div>
            </a>
          </li>
        </ul>
  <div>
  <form id="add_update_form">
            <div id="step-1" class="">
            <div class="card card-body blue-grey lighten-5">
              <h4 class="h4-responsive font-weight-600 text-center mb-0">What kind of business are you?</h4>
              <p class="font-11 text-center">(Select One)</p>
              <ul class="nav md-pills business-type-selector">
                <li class="nav-item">

                 <a class="active" data-toggle="tab" href="#panel11" role="tab" onclick="second_step_button('wholesaler')">
                    <div class="business-type-icon">
                      <img src="<?php echo base_url(); ?>assets/front_section/img/svg/one-business-locatio.svg" />
                    </div>
                    <div class="business-type-name">wholesaler</div>
                    <div class="yext-tooltip ">
                      <span class="info-icon" data-toggle="tooltip" data-placement="bottom"
                        title="Scan a business with one storefront or office"></span>
                    </div>
                  
                    </a>
                </li>




                <li class="nav-item">
                   <a class="" data-toggle="tab" href="#panel12" role="tab" onclick="second_step_button('retailer')">
                    <div class="business-type-icon">
                      <img src="<?php echo base_url(); ?>assets/front_section/img/svg/multi-location-business.svg" />
                    </div>
                    <div class="business-type-name">Retailer</div>
                    <div class="yext-tooltip ">
                      <span class="info-icon" data-toggle="tooltip" data-placement="bottom"
                        title="Scan a business with multiple storefronts or offices"></span>
                    </div>
                  </a>
                </li>
              </ul>
              <div class="tab-content px-0 pt-3">
              <!--wholesaler-->
             <div class="tab-pane fade in show active" id="panel11" role="tabpanel">

              
<!--                 -->
<!--                   <div class=" select-outline my-3">-->
<!--                        <select class="mdb-select md-form my-3 md-outline" id="user_type_status" name="user_type_status" onchange="not_assign_b_user_list()">-->
<!--                             <option class="form-control"> </option>-->
<!--                            --><?php //foreach (get_b_user_type_status() as  $value) { ?>
<!--                            <option --><?php //if(isset($data->user_type_status) && $data->user_type_status == $value['id']){ echo "selected"; } ?><!--  value="--><?//= $value['id'] ?><!--">-->
<!--                            --><?//= $value['status'] ?>
<!--                            </option>-->
<!--                            --><?php //} ?>
<!--                            </select>-->
<!--                        <label for="user_type_status" class="">Select package</label>-->
<!--                    </div>-->

                 <div class=" select-outline my-3">
                     <select class="mdb-select md-form my-3 md-outline" name="select_b_package_id">
                         <option value="1">wholesaler</option>
                         <option value="2">wholesaler bridge</option>
                     </select>
                     <label for="select_b_package_id">Select Package</label>
                 </div>

                 <div class=" select-outline my-3">
                     <select class="mdb-select md-form my-3 md-outline" name="select_b_duration" id="select_b_duration">
                         <option value="monthly"> Monthly </option>
                         <option value="yearly"> Yearly</option>
                     </select>
                     <label for="select_b_duration">Select Duration</label>
                 </div>
            

                  <div class="md-form md-outline my-3">
                    <input type="text" id="first_name" name="first_name" class="form-control white">
                    <label for="first_name">First Name</label>
                  </div>
                  <div class="md-form md-outline my-3">
                    <input type="text" id="last_name" name="last_name" class="form-control white">
                    <label for="last_name">Last Name</label>
                  </div>
                
                    <div class="md-form md-outline my-3">
                        <input type="email" name="email" id="email" class="form-control white">
                        <label for="email">Email Address</label>
                    </div>

                    <div class="md-form md-outline mb-0 mt-3">
                    <input name="phone" class="form-control phone phone-format" type="text" id="phone"  placeholder="+1 (XXX)-XXX-XXXX">
                    </div>

                 <!-- <div class="form-group row">
                     <label for="phone" class="col-sm-3 col-form-label text-right">Phone </label>
                     <div class="col-sm-9">
                         <input name="phone" class="form-control phone phone-format" type="text" id="phone"  placeholder="+1 (XXX)-XXX-XXXX">
                     </div>
                 </div> -->
                  
                      <div class="md-form md-outline my-3">
                        <input type="password" id="password" name="password" class="form-control white">
                        <label for="password">Password</label>
                      </div>
                  <div class="md-form md-outline my-3">
                      <input type="password" id="confirm" name="confirm" class="form-control white">
                      <label for="confirm">Confirm Password</label>
                  </div>

                      <div class="md-form md-outline mt-3">
                        <input type="text" id="sub_domain" name="sub_domain"  onkeyup="sub_domain_validate()"  class="form-control white">
                          <span id="subdomain_validate_2_msg" style="color: red;"></span>
                        <label for="sub_domain">Sub Domain</label>
                      </div>
<!--                     <input type="hidden" name="id" value="--><?php //if (isset($data->id) && !empty($data->id)) : echo $data->id;  else : 0;  endif ?><!--">-->
                  <div class="text-center">
                      <button type="button"  onclick="validation_step_first('add_update_form','<?php echo base_url();?>Front_controller/first_step_validation/w')"  class="btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16 px-6">Next</button>
                  </div>

                </div>
                   
              <div class="tab-pane fade" id="panel12" role="tabpanel">


                  <div class=" select-outline my-3">
                      <select class="mdb-select md-form my-3 md-outline" name="select_c_package_id">
                          <option value="2">package Advance </option>
                          <option value="3"> package Pro</option>
                      </select>
                    <label for="select_c_package_id">Select Package</label>
                  </div>

                  <div class=" select-outline my-3">
                      <select class="mdb-select md-form my-3 md-outline" name="select_c_duration" id="select_c_duration">
                          <option value="monthly"> Monthly </option>
                          <option value="yearly"> Yearly</option>
                      </select>
                      <label for="select_c_duration">Select Duration</label>
                  </div>



                   <div class="md-form md-outline my-3">
                    <input type="text" id="first_name_2" name="first_name_2" class="form-control white">
                    <label for="first_name_2">first name</label>
                  </div>
                  <div class="md-form md-outline my-3">
                    <input type="text" id="last_name_2" name="last_name_2" class="form-control white">
                    <label for="last_name_2">Last Name</label>
                  </div>
                
                    <div class="md-form md-outline my-3">
                        <input type="email" name="email_2" id="email_2" class="form-control white">
                        <label for="email_2">Email Address</label>
                    </div>

                 <div class="md-form md-outline mb-0 mt-3">
                  <input name="phone_2" class="form-control phone_2 phone-format" type="text" id="phone_2"  placeholder="+1 (XXX)-XXX-XXXX">
                 </div>

                  <!-- <div class="form-group row">
                      <label for="phone" class="col-sm-3 col-form-label text-right">Phone </label>
                      <div class="col-sm-9">
                          <input name="phone_2" class="form-control phone_2 phone-format" type="text" id="phone_2"  placeholder="+1 (XXX)-XXX-XXXX">
                      </div>
                  </div> -->

                  
                      <div class="md-form md-outline my-3">
                        <input type="password" id="password_2" name="password_2" class="form-control white">
                        <label for="password_2">Password</label>
                      </div>
                       <div class="md-form md-outline my-3">
                        <input type="password" id="confirm_2" name="confirm_2" class="form-control white">
                        <label for="confirm_2">Confirm Password</label>
                      </div>

                   
                      <div class="md-form md-outline mt-3">
                        <input type="text" class="form-control white" id="sub_domain_2" placeholder="Sub domain" onkeyup="sub_domain_validate()" name="sub_domain_2">
                           <span id="subdomain_validate_2_msg" style="color: red;"></span>
                        <label for="sub_domain_2">Sub Domain</label>
                      </div>
<!--                     <input type="hidden" name="id" value="--><?php //if (isset($data->id) && !empty($data->id)) : echo $data->id;  else : 0;  endif ?><!--">-->
                  <div class="text-center">
                    
                      <button type="button"   onclick="validation_step_first('add_update_form','<?php echo base_url();?>Front_controller/first_step_validation/r')" class=" form-post-btn btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16 px-6">Next</button>
<!--                      <a type="button" id="next-btn6">next</a>-->
                                                  
                  </div>

                </div>
                     <!--retailer-->



              </div>
            </div>
         </div>
            <div id="step-2" class="">
              <div class="card card-body blue-grey lighten-5">
                  <div id="response_msg" style="background-color: green"></div>
                <h4 class="h4-responsive font-weight-600 text-center mb-0">Contact Information</h4>
        <!--        <form>-->

                    <div class="md-form md-outline mb-0 mt-3">
                        <input type="text" id="company" name="company" class="form-control white">
                        <label for="company">company</label>
                    </div>
                      <div class="md-form md-outline mb-0 mt-3">
                        <input type="text" id="address" name="address" class="form-control white">
                        <!-- <label for="address">Address</label> -->
                      </div>    
                      <div class="md-form md-outline mb-0 mt-3">
                        <input type="text" id="city" name="city" class="form-control white">
                        <label for="city">City</label>
                      </div>

                    <div class="divider px-2"></div>
                    <div class="md-form md-outline mb-0 mt-3">

                        <input type="text" id="state" name="state" class="form-control white">
                        <label for="state">State</label>
                    </div>


                    <div class="md-form md-outline mb-0 mt-3">
                        <input type="number" id="zip_code" name="zip_code" class="form-control white">
                        <label for="zip_code">Zip</label>
                    </div>


                    <div class="md-form md-outline mb-0 mt-3">
                        <input type="text" id="country_code" name="country_code" class="form-control white">
                        <label for="country_code">country code</label>
                    </div>

                      <div class="text-center mt-3 d-flex justify-content-center">
                          <div id="second_step_button">
                              <button type="button" id="next-btn3" onclick="add_update_details_front('add_update_form','<?php echo base_url();?>Front_controller/wholesaler_user_save')" class="btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16">Next</button>
                          </div>

                                 <a type="button" id="next-btn5" style="opacity:0">next</a>
                      </div>

        </div>
        </div>
    </form>
    <div id="step-3" class="">
    <div class="card card-body blue-grey lighten-5">
      <h4 class="h4-responsive font-weight-600 text-center mb-0">Payment</h4>
        <form id="payment_form">
                    <div class="md-form md-outline my-3">
                        <input type="" maxlength="19" required id="card_number" name="card_number" class="form-control white card_number" onkeyup="hypen_generate(this.value)" onkeypress="hypen_generate(this.value)">
                        <label for="card_number">Credit card<span style="color:red">*</span></label>
                    </div>
                    <div class="md-form md-outline my-3">
                        <input type="text" id="card_holder_name" name="card_holder_name" class="form-control white">
                        <label for="card_holder_name">Card Holder Name<span style="color:red">*</span></label>
                    </div>

            <div class="select-outline my-3">
                <select class="mdb-select md-form my-3 md-outline" id="expiry_month" name="expiry_month">
                    <option class="form-control"> </option>
                    <option value="01"> 01</option>
                    <option value="02"> 02</option>
                    <option value="03"> 03</option>
                    <option value="04"> 04</option>
                    <option value="05"> 05</option>
                    <option value="06"> 06</option>
                    <option value="07"> 07</option>
                    <option value="08"> 08</option>
                    <option value="09"> 09</option>
                    <option value="10"> 10</option>
                    <option value="11"> 11</option>
                    <option value="12"> 12</option>
                </select>
                <label for="expiry_month" class="">Expiration month<span style="color:red">*</span></label>
            </div>

            <div class="select-outline my-3">
                <select class="mdb-select md-form my-3 md-outline" id="expiry_year" name="expiry_year">
                    <option class="form-control"> </option>
                    <option value="20"> 2020</option>
                    <option value="21"> 2021</option>
                    <option value="22"> 2022</option>
                    <option value="23"> 2023</option>
                    <option value="24"> 2024</option>
                    <option value="25"> 2025</option>
                    <option value="26"> 2026</option>
                    <option value="27"> 2027</option>
                    <option value="28"> 2028</option>
                    <option value="29"> 2029</option>
                    <option value="30"> 2030</option>
                </select>
                <label for="expiry_year" class="">Expiration Year<span style="color:red">*</span></label>
            </div>



<!--            <div class="md-form md-outline my-3">-->
<!--                        <input type="text" id="expiry_year" name="expiry_year" class="form-control white">-->
<!--                        <label for="expiry_year">Expiry Year</label>-->
<!--                    </div>-->

                      <div class="md-form md-outline my-3">
                        <input type="text" maxlength="3" required id="cvv" name="cvv" class="form-control white">
                        <label for="cvv">CVV<span style="color:red">*</span></label>
                      </div>

                     <input type="hidden" id="user_id" name="user_id" value="">
                    <input type="hidden" id="package_id" name="package_id" value="">
                     <input type="hidden" id="duration" name="duration" value="">


                      <div class="text-center">
                        <button type="button" id="finish" onclick="payment_pay_now('payment_form','<?php echo base_url();?>Front_controller/payment_pay_now')"  class="btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16 px-6">Pay Now</button>
                      </div>
            </form>
    </div>
    </div>
  </div>
</div>
          </div>
        </div>
      </div>
    </section>



  </main>
  <!-- Main Ends-->

  <!-- Footer Start-->
  <footer class="footer footer--external" role="contentinfo">
    <div class="container">

      <div class="footer__legal">
        <span class="footer__legal-info">Ⓒ 2020 BMS. All Rights Reserved</span>
        <ul class="footer__external_links">
          <li id="menu-item-17150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17150">
            <a class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24747" href="" target="_blank">
              Terms & Conditions </a>
          </li>
          <li id="menu-item-17150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17150"><a
              href="" target="_blank">Privacy Policy</a></li>
          <li id="menu-item-24747" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24747">
            <a class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24747" href=""
              target="_blank">Design by WebRowdy</a>
          </li>
        </ul>
      </div>
      <!-- div class="footer__logos">
        <ul class="footer__logos__list">
          <li id="menu-item-17150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17150">
            <a class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24747">
              <img class="footer_truste" src="https://s3.amazonaws.com/www.yextstudio.com/BusinessScan/footer-trust.png"
                title="This site protected by Trustwave's Trusted Commerce program"></a>
          </li>
          <li id="menu-item-17150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17150">
            <a><img class="footer_trustwave" id="icon-trustwave"
                src="https://s3.amazonaws.com/www.yextstudio.com/BusinessScan/footer-Trustwave.png" 
                style="cursor:pointer;"></a>
          </li>
          <li>
            <img class="footer_yext_listed"
              src="https://s3.amazonaws.com/www.yextstudio.com/BusinessScan/footer-YEXT-Listed-NYSE_REV.png"
              title="Yext Listed NYSE">
          </li>
          <li>
            <a class="footer__nyc-icon" target="_blank" href="">
              <img src="https://s3.amazonaws.com/www.yextstudio.com/BusinessScan/footer-MINY.png" width="60" height="60"
                alt="Made in NY">
            </a>
          </li>
        </ul>
      </div-->
    </div>
  </footer>
  <!-- Footer Ends-->

  <!-- jQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/mdb.min.js"></script>
  <!-- Smart Wizard JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/smart_wizard/js/jquery.smartWizard.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/phone_format/js/common-phone.js"></script>
  <script src="<?php echo base_url(); ?>assets/front_section/js/iziToast.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/front_section/common.js" type="text/javascript"></script>
  <script>
    $("#card_number").focus(function() {
            $('label[for="card_number"]').addClass('active');
      })
      // blur(function() {
      //       $('label[for="card_number"]').removeClass('active');
      // });
  </script>
  <script>
      function second_step_button(button_type)
      {
        show_loader();
         if(button_type=='wholesaler')
         {
             $('#second_step_button').empty();
             $('#second_step_button').append(`<button type="button" id="next-btn3" onclick="add_update_details_front('add_update_form','<?php echo base_url();?>Front_controller/wholesaler_user_save')" class="btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16">Next</button>`);
         }
          if(button_type=='retailer')
          {
              $('#second_step_button').empty();
              $('#second_step_button').append(` <button type="button" id="next-btn4" onclick="add_update_details_front('add_update_form','<?php echo base_url();?>Front_controller/add_customer_api')" class="btn btn-outline-primary btn-capsule hvr-sweep-collapseX font-16">Next</button>`);
          }
          hide_loader();
      }
  </script>


  <script type="text/javascript">

      var mybase_url = 'http://soft9.bdtask.com/bmslink_apps/';

        $(document).ready(function(){

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
               //alert("You are on step "+stepNumber+" now");
               if(stepPosition === 'first'){
                   $("#prev-btn").addClass('disabled');
               }else if(stepPosition === 'final'){
                   $("#next-btn").addClass('disabled');
               }else{
                   $("#prev-btn").removeClass('disabled');
                   $("#next-btn").removeClass('disabled');
               }
            });

        


            // Smart Wizard
            $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'dots',
                    transitionEffect:'fade',
                    keyNavigation: false,
                    showStepURLhash: false,
                    toolbarSettings: {toolbarPosition: 'both',
                                      toolbarButtonPosition: 'end',
                                      showNextButton: false, // show/hide a Next button
                                      showPreviousButton: false // show/hide a Previous button
                                    },
                    anchorSettings: {
                        anchorClickable: false, // Enable/Disable anchor navigation
                        enableAllAnchors: false, // Activates all anchors clickable all times
                        markDoneStep: true, // add done css
                        enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                    },  
            });

            $("#next-btn").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });
            $("#next-btn2").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });
            $("#next-btn5").on("click", function() {
                // Navigate next
                $('#smartwizard').smartWizard("next");
                return true;
            });


        });
    </script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript">
  // Material Select Initialization
$(document).ready(function() {
$('.mdb-select').materialSelect();
});
    // Tooltips Initialization
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>
   <script type="text/javascript">
function add_update_details_front(form_id, url)
{ 
    var data = $('#' + form_id).serialize();
    show_loader();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data)
        { 
            if (data.status == 400)
            {
                error_message(data.msg,"");
                hide_loader();
            }
            if (data.status == 401)
            {
                error_message(data.msg,  data.url);
                hide_loader()
            }
            if (data.status == 200)
            {
                // $('#response_msg').html('<div>Registration Successfull</div>');
                success_message(data.msg);
                $('#user_id').val(data.user_id);
                $('#package_id').val(data.package_id);
                $('#duration').val(data.duration);
                $( "#next-btn5" ).trigger( "click" );
                $( "#next-btn6" ).trigger( "click" );
                hide_loader();
            }
            if (data.status == 201)
            {
                // $('#response_msg').html('<div>Registration Successfull</div>');
                var r_url = data.url;
                success_message(data.msg, 1, r_url);
                hide_loader();
            }
         
        }
    });
}

function validation_step_first(form_id, url)
{ 
    show_loader();
    var data = $('#' + form_id).serialize();
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data)
        {
            if (data.status == 400)
            {
                error_message(data.msg,"");
                hide_loader();
            }
            else if (data.status == 401)
            {
                error_message(data.msg,  data.url);
                hide_loader();
            }
            else
            {
              hide_loader();
                $('#smartwizard').smartWizard("next");
                return true;
            }
         
        }
    });
}
 </script>
 <script>
    function sub_domain_validate() 
    {
        var sub_domain = $('#sub_domain').val();
        var mailformat = /^(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,})+$/;
        if (sub_domain.match(mailformat)) {
            $('#subdomain_not_validate').text('');
            $('#subdomain_validate').text('');
            return true;
        } else {
            $('#subdomain_validate').text('');
            $('#subdomain_not_validate').text("Special characters Not allowed");
            return false;
        }
    }
</script>
<script>
 function special_character(t) {
     
        var specialChars = "<>@!#$%^&*_[]{}?:;|'\"\\/~`=abcdefghijklmnopqrstuvwxyz";
        var check = function(string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true
                }
            }
            return false;
        }
        if (check($('#phone_' + t).val()) == false) {
            // Code that needs to execute when none of the above is in the string
        } else {
            alert(specialChars + " these special character are not allows");
            $("#phone_" + t).focus();
            $("#phone_" + t).val('');
        }
    }
</script>
<script>
function error_message(msg, url = false) {
    
    iziToast.error({
        title: 'Error',
        message: msg,
        position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        onOpening: function () {   $('.form-post-btn').prop('disabled', false); },
        onOpened: function () {
            if (url) {
                window.location.href = url;
            }

        },
        onClosing: function () { $('.form-post-btn').prop('disabled', false); },
        onClosed: function () { $('.form-post-btn').prop('disabled', false); }
    });


}
</script>
<script>
function success_message(msg, status = 0, url = "") {

    iziToast.success({
        title: 'Success',
        message: msg,
        position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
        zindex: 99999999999999999999,
        onOpening: function () {
            $('.form-post-btn').prop('disabled', false);
        },
        onOpened: function () {
            if (status == 0)
            {
               // window.location.reload();
            } else if(status == 1) {
                window.location.href = url;
            }
            },
        onClosing: function () { $('.form-post-btn').prop('disabled', false); },
        onClosed: function () { $('.form-post-btn').prop('disabled', false); }
    });

}
</script>


  <script type="text/javascript">
      function payment_pay_now(form_id, url)
      {
        show_loader();
          var data = $('#' + form_id).serialize();
          $.ajax({
              url: url,
              type: 'POST',
              dataType: 'JSON',
              data: data,
              success: function (data)
              {
                  if (data.status == 400)
                  {
                      error_message(data.msg,"");
                      hide_loader();
                  }
                  if (data.status == 401)
                  {
                      error_message(data.msg,  data.url);
                      hide_loader();
                  }
                  if (data.status == 200)
                  {
                      success_message(data.msg);
                      hide_loader();
                  }
                  if (data.status == 201)
                  {
                      var r_url = data.url;
                      success_message(data.msg, 1, r_url);
                      hide_loader();
                  }

              }
          });
      }
  </script>

  <script src="https://maps.google.com/maps/api/js?key=AIzaSyCeD3LSJjBsUHiKv7IHUomkYIdbzF1b1pk&libraries=places"></script>

  <script>
     // load_country_dropdown('phone_1', country_code_phone_format); // For phone format

      //    ============= its for  google geocomplete =====================
      google.maps.event.addDomListener(window, 'load', function() {
          var places = new google.maps.places.Autocomplete(document.getElementById('address'));

          google.maps.event.addListener(places, 'place_changed', function() {
              var place = places.getPlace();
              console.log(place);
              var address = place.formatted_address;
              var latitude = place.geometry.location.lat();
              var longitude = place.geometry.location.lng();
              var geocoder = new google.maps.Geocoder;
              var latlng = {
                  lat: parseFloat(latitude),
                  lng: parseFloat(longitude)
              };
              geocoder.geocode({
                  'location': latlng
              }, function(results, status) {
                  if (status === 'OK') {
                      //console.log(results)
                      if (results[0]) {
                          //document.getElementById('location').innerHTML = results[0].formatted_address;
                          var street = "";
                          var city = "";
                          var state = "";
                          var country = "";
                          var country_code = "";
                          var zipcode = "";
                          for (var i = 0; i < results.length; i++) {
                              if (results[i].types[0] === "locality") {
                                  city = results[i].address_components[0].long_name;
                                  state = results[i].address_components[2].short_name;

                              }
                              if (results[i].types[0] === "postal_code" && zipcode == "") {
                                  zipcode = results[i].address_components[0].long_name;

                              }
                              if (results[i].types[0] === "country") {
                                  country = results[i].address_components[0].long_name;
                              }
                              if (results[i].types[0] === "country") {
                                  country_code = results[i].address_components[0].short_name;
                              }
                              if (results[i].types[0] === "route" && street == "") {
                                  for (var j = 0; j < 4; j++) {
                                      if (j == 0) {
                                          street = results[i].address_components[j].long_name;
                                      } else {
                                          street += ", " + results[i].address_components[j].long_name;
                                      }
                                  }

                              }
                              if (results[i].types[0] === "street_address") {
                                  for (var j = 0; j < 4; j++) {
                                      if (j == 0) {
                                          street = results[i].address_components[j].long_name;
                                      } else {
                                          street += ", " + results[i].address_components[j].long_name;
                                      }
                                  }

                              }
                          }
                          if (zipcode == "") {
                              if (typeof results[0].address_components[8] !== 'undefined') {
                                  zipcode = results[0].address_components[8].long_name;
                              }
                          }
                          if (country == "") {
                              if (typeof results[0].address_components[7] !== 'undefined') {
                                  country = results[0].address_components[7].long_name;
                              }
                              if (typeof results[0].address_components[7] !== 'undefined') {
                                  country_code = results[0].address_components[7].short_name;
                              }
                          }
                          if (state == "") {
                              if (typeof results[0].address_components[5] !== 'undefined') {
                                  state = results[0].address_components[5].short_name;
                              }
                          }
                          if (city == "") {
                              if (typeof results[0].address_components[5] !== 'undefined') {
                                  city = results[0].address_components[5].long_name;
                              }
                          }

                          var address = {
                              "street": street,
                              "city": city,
                              "state": state,
                              "country": country,
                              "country_code": country_code,
                              "zipcode": zipcode,
                          };
                          //document.getElementById('location').innerHTML = document.getElementById('location').innerHTML + "<br/>Street : " + address.street + "<br/>City : " + address.city + "<br/>State : " + address.state + "<br/>Country : " + address.country + "<br/>zipcode : " + address.zipcode;
                          //                        console.log(zipcode);
                          $("#city").val(city);
                          $("label[for='city']").addClass('active');
                          $("#state").val(state);
                          $("label[for='state']").addClass('active');
                          $("#zip_code").val(zipcode);
                          $("label[for='zip_code']").addClass('active');
                          $("#country_code").val(country_code);
                          $("label[for='country_code']").addClass('active');

                      } else {
                          window.alert('No results found');
                      }
                  } else {
                      window.alert('Geocoder failed due to: ' + status);
                  }
              });

          });


      });
</script>
<script>
    function hypen_generate(id) {
        var total_number = id.length;
        if (total_number == 4) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 9) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
        if (total_number == 14) {
            var gen_number = id + "-";
            $(".card_number").val(gen_number);
        }
    }
</script>


  <script src="<?php echo base_url(); ?>assets/c_level/js/phoneformat.js"></script>

  <script src="<?php echo base_url(); ?>assets/phone_format/js/intlTelInput.js"></script>
  <script src="<?php echo base_url(); ?>assets/phone_format/js/cleave.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/phone_format/js/cleave-phone.i18n.js"></script>
  <script src="<?php echo base_url(); ?>assets/phone_format/js/common-phone.js"></script>
  <script>
       country = 'US';
      //---------phone_format---------
      $(document).ready(function(){
          load_country_dropdown('phone',country);
      })

       $(document).ready(function(){
           load_country_dropdown('phone_2',country);
       })
  </script>
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #037ecc;
        border-bottom: 16px solid #037ecc;
        border-right: 16px solid #f57c00;
        border-left: 16px solid #f57c00;
        width: 100px;
        height: 100px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes  spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }

    .loader-box
    {
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        position: fixed;
        display: block;
        opacity: 0.9;
        background-color: #fff;
        z-index: 99;
        text-align: center;

    }
</style>
<div class="loader-box" id="loader" style="display: none;">
    <div class="flex-center">
    <!-- <div class="loader"></div> -->
    <img class="img-fluid" src="https://thumbs.gfycat.com/LameDifferentBalloonfish-small.gif">
</div>
</div>
<script>
    function show_loader()
    {
      console.log('Loader display');
        $('#loader').show();
    }

    function hide_loader()
    {
      console.log('Loader Hide');
        $('#loader').hide();
    }
</script>
</body>

</html>


