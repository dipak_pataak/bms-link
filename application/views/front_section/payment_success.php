<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>BMS</title>
  <!-- MDB icon -->
  <link rel="icon" href="" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/fontawesome/css/all.min.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/mdb.css">
  <!-- Your custom styles -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/style.css">
  <!-- Smart Wizard -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/smart_wizard/css/smart_wizard.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/front_section/css/iziToast.min.css">

</head>

<body>
<div class="grey lighten-4" style="height:100vh">
<div class="row h-100 align-items-center justify-content-center">
    <div class="col-md-6 col-lg-5 col-xl-4">
        <div class="card z-depth-5 text-center" style="border-radius:10px">
        <div class="d-flex justify-content-center align-items-center" style="height:250px; background:#e1ffdf">
                <img src="https://famenote.com/img/Payment-successful.png" width="200px" height="200px"/>
            </div>
            <div class="card-body">
            <h3 class="h3-responsive font-weight-600 text-success">Payment Successful</h3>
            </div>
        </div>
    </div>
</div>
</div>

  <!-- jQuery -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/js/mdb.min.js"></script>
  <!-- Smart Wizard JavaScript -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front_section/smart_wizard/js/jquery.smartWizard.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/phone_format/js/common-phone.js"></script>
  <script src="<?php echo base_url(); ?>assets/front_section/js/iziToast.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/front_section/common.js" type="text/javascript"></script>
</body>
</html>
