<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

$route['default_controller'] = 'Front_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//============ its for quickbooks ==================
$route['quickbooks-authentication']             = 'quickbooks/Authentication_controller/get_authentication';
$route['quickbooks-customer']                   = 'quickbooks/Authentication_controller/storeCustomer';
$route['quickbooks-suppliers']                  = 'quickbooks/Authentication_controller/storeSuppliers';
$route['quickbooks-invoice']                    = 'quickbooks/Authentication_controller/createInvoice';
$route['quickbooks-account']                    = 'quickbooks/Authentication_controller/createAccount';
$route['quickbooks-tax']                        = 'quickbooks/Authentication_controller/createTaxService';
$route['quickbooks-employee']                   = 'quickbooks/Authentication_controller/storeEmployee';
$route['quickbooks-category']                   = 'quickbooks/Authentication_controller/storeCategory';
$route['quickbooks-product']                    = 'quickbooks/Authentication_controller/storeProduct';
$route['quickbooks-purchase-order']             = 'quickbooks/Authentication_controller/PurchaseOrder';
$route['quickbooks-commission-list']             = 'quickbooks/Authentication_controller/ManageCommissionList';
$route['wholeseler-store-quickbooks-settings']  = 'quickbooks/Authentication_controller/storeQuickbookSetting';
$route['quickbooks-refreshtoken']               = 'quickbooks/Authentication_controller/refershAccessToken';
$route['quickbooks-revoketoken']                = 'quickbooks/Authentication_controller/revokeToken';
$route['retailer-store-quickbooks-settings']    = 'quickbooks/Authentication_controller/storeRetailerQuickbookSetting';
$route['quickbooks-tax-agency']                 = 'quickbooks/Authentication_controller/createTaxAgency';
$route['wholeseller-quickbooks-setting']        = 'b_level/Setting_controller/wholeseller_quickbooks_setting';
$route['retailer-quickbooks-setting']           = 'c_level/Setting_controller/retailer_quickbooks_setting';
$route['c-save-quickbooks-settings']            = 'c_level/Setting_controller/change_quickbooks_setting_data';

//============ its for retailer ==================
$route['login'] = 'c_level/Auth_controller';
$route['public'] = 'c_level/Auth_controller';
$route['retailer'] = 'c_level/Auth_controller';
$route['retailer-forgot-password-form'] = 'c_level/Auth_controller/c_level_forgot_password_form';
$route['retailer-forgot-password-send'] = 'c_level/Auth_controller/c_level_forgot_password_send';
$route['retailer-dashboard'] = 'c_level/Dashboard_controller/dashboard';
$route['retailer-logout'] = 'c_level/Auth_controller/c_level_logout';

/****************** START: Added On 03-02-2020 *************************/
$route['retailer-pending-invoices']     = 'c_level/Dashboard_controller/list_of_pending_invoices';
$route['retailer-pending-orders']       = 'c_level/Dashboard_controller/list_of_pending_orders';
$route['retailer-wholesaler-orders']    = 'c_level/Dashboard_controller/list_of_wholesaler_orders';
/****************** START: Added On 03-02-2020 *************************/

//$route['d-customer-form'] = 'd_level/Customer_controller/customer_form';
//============= its for settings =============
$route['retailer-account'] = 'c_level/Setting_controller/my_account';
$route['my-account-update'] = 'c_level/Setting_controller/my_account_update';
$route['company-profile'] = 'c_level/Setting_controller/company_profile';
$route['company-profile-update'] = 'c_level/Setting_controller/company_profile_update';
$route['retailer-password-change'] = 'c_level/Setting_controller/change_password';
$route['retailer-update-password'] = 'c_level/Setting_controller/update_password';
$route['payment-setting'] = 'c_level/Setting_controller/payment_setting';
$route['retailer-save-gateway'] = 'c_level/Setting_controller/c_save_gateway';
$route['retailer-gateway-edit/(:any)'] = 'c_level/Setting_controller/c_gateway_edit/$1';
$route['retailer-update-payment-gateway/(:any)'] = 'c_level/Setting_controller/update_payment_gateway/$1';
$route['retailer-gateway-delete/(:any)'] = 'c_level/Setting_controller/gateway_delete/$1';
$route['payment-gateway'] = 'c_level/Setting_controller/payment_gateway';
$route['payment-gateway-update'] = 'c_level/Setting_controller/payment_gateway_update';
$route['customer-cost-factor'] = 'c_level/Setting_controller/cost_factor';
$route['retailer-cost-factor-save'] = 'c_level/Setting_controller/cost_factor_save';
$route['iframe-code'] = 'c_level/Setting_controller/iframe_code';
$route['iframe-code-save'] = 'c_level/Setting_controller/iframe_code_save';
$route['d-customer-form'] = 'c_level/Setting_controller/d_customer_form';
$route['d-customer-ifrc-us-stateame-form'] = 'c_level/Setting_controller/d_customer_iframe_form';
$route['d-customer-iframe-form-custom/(:any)/(:any)'] = 'c_level/Setting_controller/d_customer_iframe_form_custom/$1/$2';
$route['d-customer-info-save'] = 'c_level/Setting_controller/d_customer_info_save';
//$route['profile-setting'] = 'c_level/Setting_controller/profile_setting';
$route['retailer-us-state'] = 'c_level/Setting_controller/us_state';
$route['retailer-us-state/(:any)'] = 'c_level/Setting_controller/us_state/$1';
$route['retailer-us-state-save'] = 'c_level/Setting_controller/c_us_state_save';
$route['retailer-us-state-edit/(:any)'] = 'c_level/Setting_controller/c_us_state_edit/$1';
$route['retailer-us-state-update/(:any)'] = 'c_level/Setting_controller/c_us_state_update/$1';
$route['retailer-us-state-delete/(:any)'] = 'c_level/Setting_controller/us_state_delete/$1';
$route['import-retailer-us-state-save'] = 'c_level/Setting_controller/import_c_us_state_save';

$route['retailer-state'] = 'c_level/State_controller';
$route['retailer-getStateLists'] = 'c_level/State_controller/getStateLists';
$route['retailer-import-state-save'] = 'c_level/State_controller/import_state_save';

$route['logs'] = 'c_level/Setting_controller/logs';
$route['logs/(:any)'] = 'c_level/Setting_controller/logs/$1';
$route['retailer-sms'] = 'c_level/Setting_controller/sms';
$route['retailer-sms-send']       = 'c_level/Setting_controller/sms_send';
$route['retailer-sms-csv-upload'] = 'c_level/Setting_controller/sms_csv_upload';
$route['retailer-group-sms-send'] = 'c_level/Setting_controller/group_sms_send';

$route['retailer-sms-configure'] = 'c_level/Setting_controller/sms_configure';
$route['retailer-sms-config-save'] = 'c_level/Setting_controller/sms_config_save';
$route['retailer-sms-config-verified/(:any)'] = 'c_level/Setting_controller/sms_config_verified/$1';
$route['retailer-sms-config-edit/(:any)'] = 'c_level/Setting_controller/sms_config_edit/$1';
$route['retailer-sms-config-update/(:any)'] = 'c_level/Setting_controller/sms_config_update/$1';
$route['retailer-sms-config-delete/(:any)'] = 'c_level/Setting_controller/sms_config_delete/$1';

$route['retailer-email'] = 'c_level/Setting_controller/email';
$route['retailer-mail-configure'] = 'c_level/Setting_controller/mail_configure';
$route['retailer-mail-config-save'] = 'c_level/Setting_controller/mail_config_save';
$route['retailer-email-send'] = 'c_level/Setting_controller/email_send';
//================ its for users ===============
$route['add-user'] = 'c_level/User_controller/index';
$route['user-save'] = 'c_level/User_controller/user_save';
$route['user-email-check'] = 'c_level/User_controller/user_email_check';
$route['users'] = 'c_level/User_controller/users';
$route['user-edit/(:any)'] = 'c_level/User_controller/user_edit/$1';
$route['user-update/(:any)'] = 'c_level/User_controller/user_update/$1';

//================ its for Role ===============
$route['access-role'] = 'c_level/Role_controller/access_role';
$route['role-permission'] = 'c_level/Role_controller/role_permission';
$route['role-save'] = 'c_level/Role_controller/role_save';
$route['role-list'] = 'c_level/Role_controller/role_list';
$route['role-edit/(:any)'] = 'c_level/Role_controller/role_edit/$1';
$route['role-update'] = 'c_level/Role_controller/role_update';
$route['role-delete/(:any)'] = 'c_level/Role_controller/role_delete/$1';
$route['user-role'] = 'c_level/Role_controller/user_roles';
$route['assign-user-role-save'] = 'c_level/Role_controller/assign_user_role_save';
$route['edit-user-access-role/(:any)'] = 'c_level/Role_controller/edit_user_access_role/$1';
$route['delete-user-access-role/(:any)'] = 'c_level/Role_controller/delete_user_access_role/$1';
$route['assign-user-role-update/(:any)'] = 'c_level/Role_controller/assign_user_role_update/$1';
$route['retailer-check-user-role'] = 'c_level/Role_controller/c_level_check_user_role';


// Quatation For c level : START
$route['retailer-new-quotation'] = 'c_level/quotation_controller/add_new_quotation';
$route['retailer-manage-quotation'] = 'c_level/quotation_controller/manage_quotation';
$route['retailer-manage-upcharges'] = 'c_level/quotation_controller/manageUpcharges';
$route['retailer-quotation-receipt/(:any)'] = 'c_level/quotation_controller/quotation_receipt/$1';
$route['retailer-delete-quotation/(:any)'] = 'c_level/quotation_controller/delete_quotation/$1';
$route['retailer-edit-quotation/(:any)'] = 'c_level/quotation_controller/edit_quotation/$1';
// Quatation For c level : END

// Order For c level : START
$route['retailer-synk-wholesaler/(:any)'] = 'c_level/invoice_receipt/synk_to_b/$1';
$route['retailer-synk-data-wholesaler/(:any)'] = 'c_level/invoice_receipt/synk_data_to_b/$1';
$route['retailer-invoice-receipt/(:any)'] = 'c_level/invoice_receipt/receipt/$1';
$route['retailer-bulk-order-upload'] = 'c_level/order_bulk_upload';
$route['retailer-bulk-order-save'] = 'c_level/order_bulk_upload/csv_upload_save';
$route['retailer-order-view/(:any)'] = 'c_level/order_controller/order_view/$1';
$route['retailer-custom-order-view/(:any)'] = 'c_level/customer_order_controller/order_view/$1';
$route['retailer-order-shipment/(:any)'] = 'c_level/order_controller/shipment/$1';
$route['retailer-my-receipt/(:any)'] = 'c_level/invoice_receipt/my_receipt/$1';
// Order For c level : END

// Payment For c Level : START 
$route['retailer-make-payment-wholesaler/(:any)'] = 'c_level/make_payment/payment_to_b/$1';
$route['retailer-money-receipt/(:any)'] = 'c_level/invoice_receipt/money_receipt/$1';
$route['retailer-multiple-payment-wholesaler'] = 'c_level/make_payment/multiple_payment_to_b';
$route['retailer-multiple-payment-wholesaler/(:any)'] = 'c_level/make_payment/multiple_payment_to_b/$1';
$route['retailer-payment-success/(:any)/(:any)'] = 'c_level/Customer_payment/success/$1/$2';
$route['retailer-payment-cancel/(:any)/(:any)'] = 'c_level/Customer_payment/cancel/$1/$2';
$route['retailer-payment-ipn'] = 'c_level/Customer_payment/ipn';
$route['retailer-payment-message'] = 'c_level/Customer_payment/message';
$route['retailer-payment-message/(:any)/(:any)'] = 'c_level/Customer_payment/message/$1/$2';
$route['retailer-invoice-recipt-success/(:any)/(:any)'] = 'c_level/invoice_receipt/success/$1/$2';
$route['retailer-invoice-recipt-cancel/(:any)/(:any)'] = 'c_level/invoice_receipt/cancel/$1/$2';
$route['retailer-invoice-recipt-ipn'] = 'c_level/invoice_receipt/ipn';
$route['retailer-make-payment-success/(:any)/(:any)'] = 'c_level/make_payment/success/$1/$2';
$route['retailer-make-payment-cancel/(:any)/(:any)'] = 'c_level/make_payment/cancel/$1/$2';
$route['retailer-make-payment-ipn'] = 'c_level/make_payment/ipn';
$route['retailer-make-payment-bulk-success/(:any)/(:any)'] = 'c_level/make_payment/bulk_success/$1/$2';
$route['retailer-make-payment-bulk-cancel/(:any)/(:any)'] = 'c_level/make_payment/bulk_cancel/$1/$2';
$route['retailer-make-payment-bulk-ipn'] = 'c_level/make_payment/bulk_ipn';
$route['retailer-payment-log'] = 'c_level/payment_log/get_log';
$route['retailer-payment-log/(:any)'] = 'c_level/payment_log/get_log/$1';
// Payment For c Level : END 

$route['retailer-package-edit-card'] = 'c_level/package_controller/edit_card';
$route['retailer-package-edit-card/(:any)'] = 'c_level/package_controller/edit_card/$1';
$route['retailer-package-my-subscription'] = 'c_level/package_controller/my_subscription';

//========== its for menu setup =============
$route['menu-setup'] = 'c_level/Menusetup_controller/menu_setup';
$route['retailer-menu-setup'] = 'c_level/Menusetup_controller/c_menu_setup';
$route['retailer-menu-setup/(:any)'] = 'c_level/Menusetup_controller/c_menu_setup/$1';
$route['retailer-menusetup-edit/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_edit/$1';
$route['retailer-menusetup-update/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_update/$1';
$route['retailer-menusetup-delete/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_delete/$1';
$route['retailer-menusetup-inactive/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_inactive/$1';
$route['retailer-menusetup-active/(:any)'] = 'c_level/Menusetup_controller/c_menusetup_active/$1';
$route['clevel-menu-search'] = 'c_level/Menusetup_controller/clevel_menu_search';
$route['retailer-menu-export-csv'] = 'c_level/Menusetup_controller/menu_export_csv';
$route['retailer-import-menu-save'] = 'c_level/Menusetup_controller/import_menu_update';

$route['menu-setup/(:any)'] = 'c_level/Menusetup_controller/menu_setup/$1';
$route['menusetup-save'] = 'c_level/Menusetup_controller/menusetup_save';
$route['menusetup-edit/(:any)'] = 'c_level/Menusetup_controller/menusetup_edit/$1';
$route['menusetup-update/(:any)'] = 'c_level/Menusetup_controller/menusetup_update/$1';
$route['menusetup-delete/(:any)'] = 'c_level/Menusetup_controller/menusetup_delete/$1';
$route['retailer-menu-search'] = 'c_level/Menusetup_controller/c_level_menu_search';
$route['menusetup-inactive/(:any)'] = 'c_level/Menusetup_controller/menusetup_inactive/$1';
$route['menusetup-active/(:any)'] = 'c_level/Menusetup_controller/menusetup_active/$1';

//============= its for account_chart ==============
$route['chart-of-account'] = 'c_level/Account_controller/chart_of_account';
$route['show-selected-form/(:any)'] = 'c_level/Account_controller/selectedform/$1';
$route['insert-coa'] = 'c_level/Account_controller/insert_coa';
$route['new-account-form/(:any)'] = 'c_level/Account_controller/newform/$1';
$route['account-chart'] = 'c_level/Account_controller/account_chart';
$route['voucher-debit'] = 'c_level/Account_controller/voucher_debit';
$route['debit-voucher-code/(:any)'] = 'c_level/Account_controller/debit_voucher_code/$1';
$route['create-debit-voucher'] = 'c_level/Account_controller/create_debit_voucher';
$route['voucher-credit'] = 'c_level/Account_controller/voucher_credit';
$route['create-credit-voucher'] = 'c_level/Account_controller/create_credit_voucher';
$route['voucher-journal'] = 'c_level/Account_controller/voucher_journal';
$route['create-journal-voucher'] = 'c_level/Account_controller/create_journal_voucher';
$route['update-journal-voucher'] = 'c_level/Account_controller/update_journal_voucher';
$route['contra-voucher'] = 'c_level/Account_controller/contra_voucher';
$route['create-contra-voucher'] = 'c_level/Account_controller/create_contra_voucher';
$route['update-contra-voucher'] = 'c_level/Account_controller/update_contra_voucher';
$route['voucher-approval'] = 'c_level/Account_controller/voucher_approval';
$route['voucher-edit/(:any)'] = 'c_level/Account_controller/voucher_edit/$1';
$route['update-debit-voucher'] = 'c_level/Account_controller/update_debit_voucher';
$route['update-credit-voucher'] = 'c_level/Account_controller/update_credit_voucher';
$route['isactive/(:any)/(:any)'] = 'c_level/Account_controller/isactive/$1/$2';

//============= its for account reports ==============
$route['cash-book'] = 'c_level/Account_controller/cash_book';
$route['bank-book'] = 'c_level/Account_controller/bank_book';
$route['cash-flow'] = 'c_level/Account_controller/cash_flow';
$route['retailer-cash-flow-report-search'] = 'c_level/Account_controller/cash_flow_report_search';
$route['voucher-reports'] = 'c_level/Account_controller/voucher_reports';
$route['retailer-voucher-report-serach'] = 'c_level/Account_controller/voucher_report_serach';
$route['retailer-vouchar-cash/(:any)'] = 'c_level/Account_controller/vouchar_cash/$1';
$route['general-ledger'] = 'c_level/Account_controller/general_ledger';
$route['retailer-general-led'] = 'c_level/Account_controller/general_led';
$route['retailer-accounts-report-search'] = 'c_level/Account_controller/accounts_report_search';
$route['profit-loss'] = 'c_level/Account_controller/profit_loss';
$route['retailer-profit-loss-report-search'] = 'c_level/Account_controller/profit_loss_report_search';
$route['trial-balance'] = 'c_level/Account_controller/trial_ballance';
$route['retailer-trial-balance-report'] = 'c_level/Account_controller/trial_balance_report';

//================== its for orders ================
$route['new-quotation'] = 'c_level/Order_controller/new_order';
$route['c_level/order-edit/(:any)/(:any)'] = 'c_level/Order_controller/new_order_edit/(:any)/(:any)';
$route['c_level/order-window-delete/(:any)'] = 'c_level/Order_controller/order_window_delete/$1';

$route['order-to-wholesaler'] = 'c_level/Customer_order_controller/new_order';

$route['manage-order'] = 'c_level/Order_controller/manage_order';
$route['manage-order/(:num)'] = 'c_level/Order_controller/manage_order/$1';
$route['invoice-print'] = 'c_level/Order_controller/invoice_print';
$route['quotation-edit'] = 'c_level/Order_controller/quotation_edit';
$route['manage-invoice'] = 'c_level/Order_controller/manage_invoice';
$route['manage-invoice/(:num)'] = 'c_level/Order_controller/manage_invoice/$1';
$route['order-cancel'] = 'c_level/Order_controller/order_cancel';
$route['track-order'] = 'c_level/Order_controller/track_order';

$route['my-orders'] = 'c_level/Order_controller/my_orders';
$route['my-orders/(:num)'] = 'c_level/Order_controller/my_orders/$1';

$route['retailer-customer-order-return/(:any)'] = 'c_level/Return_controller/c_customer_order_return/$1';
$route['retailer-customer-order-return-save'] = 'c_level/Return_controller/c_customer_order_return_save';
$route['customer-return'] = 'c_level/Return_controller/customer_return';
$route['retailer-show-return-resend'] = 'c_level/Return_controller/c_show_return_resend';
$route['retailer-show-return-view'] = 'c_level/Return_controller/c_show_return_view';
$route['purchase-return'] = 'c_level/Return_controller/purchase_return';
$route['purchase-return/(:any)'] = 'c_level/Return_controller/purchase_return/$1';
$route['retailer-purchase-order-return/(:any)'] = 'c_level/Return_controller/c_purchase_order_return/$1';
$route['retailer-customer-purchase-order-return-save'] = 'c_level/Return_controller/c_customer_purchase_order_return_save';
$route['retailer-order-return-detials-show/(:any)'] = 'c_level/Return_controller/c_order_return_detials_show/$1';

$route['retailer-quotation/(:any)'] = 'c_level/Order_controller/quotation/$1';
$route['retailer-quotation-label/(:any)'] = 'c_level/Order_controller/quotation_label/$1';


//============== its for customers ================
$route['add-customer'] = 'c_level/Customer_controller/add_customer';
$route['customer-save'] = 'c_level/Customer_controller/customer_save';
$route['customer-list'] = 'c_level/Customer_controller/customer_list';
$route['customer-list/(:any)'] = 'c_level/Customer_controller/customer_list/$1';
$route['customer-list-filter'] = 'c_level/Customer_controller/c_level_customer_filter';
$route['retailer-show-address-map/(:any)'] = 'c_level/Customer_controller/show_address_map/$1';
$route['customer-view/(:any)'] = 'c_level/Customer_controller/customer_view/$1';
$route['show-customer-record/(:any)'] = 'c_level/Customer_controller/show_customer_record/$1';
$route['change-customer-status'] = 'c_level/Customer_controller/change_customer_status';
$route['customer-edit/(:any)'] = 'c_level/Customer_controller/customer_edit/$1';
$route['retailer-customer-file-delete/(:any)/(:any)'] = 'c_level/Customer_controller/c_customer_file_delete/$1/$2';
$route['customer-update/(:any)'] = 'c_level/Customer_controller/customer_update/$1';
$route['retailer-customer-delete/(:any)'] = 'c_level/Customer_controller/customer_delete/$1';
$route['retailer-customer-active/(:any)/(:any)'] = 'c_level/Customer_controller/customer_active/$1/$2';
$route['state-wise-city/(:any)'] = 'c_level/Customer_controller/state_wise_city/$1';
$route['get-check-retailer-customer-unique-email'] = 'c_level/Customer_controller/get_check_unique_email';
$route['customer-comment-view/(:any)'] = 'c_level/Customer_controller/customer_comment_view/$1';
$route['out-customer-comment'] = 'c_level/Customer_controller/out_customer_comment';
$route['customer-bulk-upload'] = 'c_level/Customer_controller/customer_bulk_upload';
$route['retailer-customer-csv-upload'] = 'c_level/Customer_controller/customer_csv_upload_save';
$route['retailer-customer-search'] = 'c_level/Customer_controller/c_level_customer_search';
$route['retailer-customer-export-csv'] = 'c_level/Customer_controller/c_customer_export_csv';
$route['retailer-customer-export-pdf'] = 'c_level/Customer_controller/c_customer_export_pdf';
$route['show-old-retailer-cstomer-comment'] = 'c_level/Customer_controller/show_old_c_cstomer_comment';

//============== its for appointment-form ====================
$route['appointment-setup'] = 'c_level/Appointment_controller/appointment_setup';
$route['appointment-setup-edit'] = 'c_level/Appointment_controller/appointment_setup_edit';
$route['appointment-form'] = 'c_level/Appointment_controller/appointment_form';

//$route['appointment-form'] = 'c_level/Setting_controller/appointment_form';


$route['faq'] = 'c_level/Setting_controller/faq';

//============= its for category module ===============
$route['category-wise-subcategory/(:any)'] = 'c_level/Order_controller/category_wise_subcategory/$1';
$route['retailer-category-wise-subcategory/(:any)'] = 'c_level/Catalog_controller/category_wise_subcategory/$1';
$route['wholesaler-category-wise-subcategory/(:any)'] = 'b_level/Product_controller/category_wise_subcategory/$1';
$route['category-wise-condition/(:any)'] = 'b_level/Condition_controller/category_wise_condition/$1';
$route['category-wise-pattern/(:any)'] = 'b_level/Pattern_controller/category_wise_pattern/$1';

$route['order-logo-print/(:any)'] = 'b_level/Invoice_receipt/order_logo_print/$1';


//================= its for start wholesaler ======================
$route['admin'] = 'b_level/Auth_controller';
$route['wholesaler'] = 'b_level/Auth_controller';
$route['wholesaler-menu-search'] = 'b_level/Menusetup_controller/b_level_menu_search';
$route['wholesaler-dashboard'] = 'b_level/Dashboard';
$route['wholesaler-scan-product'] = 'b_level/Scan_product';
$route['wholesaler-logout'] = 'b_level/auth_controller/b_level_logout';
$route['wholesaler-forgot-password-form'] = 'b_level/Auth_controller/b_level_forgot_password_form';
$route['wholesaler-forgot-password-send'] = 'b_level/Auth_controller/b_level_forgot_password_send';

//=========== its for supplier module ==============
$route['add-supplier'] = 'b_level/Supplier_controller/add_supplier';
$route['supplier-list'] = 'b_level/Supplier_controller/supplier_list';
$route['supplier-invoice/(:any)'] = 'b_level/Supplier_controller/supplier_invoice/$1';
$route['supplier-edit/(:any)'] = 'b_level/Supplier_controller/supplier_edit/$1';
$route['supplier-delete/(:any)'] = 'b_level/Supplier_controller/supplier_delete/$1';
$route['supplier-filter'] = 'b_level/Supplier_controller/supplier_filter';
$route['get-check-supplier-unique-email'] = 'b_level/Supplier_controller/get_check_supplier_unique_email';
$route['wholesaler-supplier-search'] = 'b_level/Supplier_controller/b_level_supplier_search';

//============= its for customer module ==============
$route['add-wholesaler-customer'] = 'b_level/Customer_controller/add_customer';
$route['get-check-customer-unique-email'] = 'b_level/Customer_controller/get_check_unique_email';
$route['wholesaler-customer-list'] = 'b_level/Customer_controller/b_customer_list';
$route['wholesaler-customer-filter'] = 'b_level/Customer_controller/b_level_customer_filter';
$route['show-wholesaler-customer-record/(:any)'] = 'b_level/Customer_controller/show_b_customer_record/$1';
$route['wholesaler-customer-view/(:any)'] = 'b_level/Customer_controller/customer_view/$1';
$route['send-customer-comment'] = 'b_level/Customer_controller/send_customer_comment';
$route['wholesaler-customer-edit/(:any)'] = 'b_level/Customer_controller/customer_edit/$1';
$route['wholesaler-customer-file-delete/(:any)/(:any)'] = 'b_level/Customer_controller/b_customer_file_delete/$1/$2';
$route['customer-import'] = 'b_level/Customer_controller/customer_import';
$route['show-address-map/(:any)'] = 'b_level/Customer_controller/show_address_map/$1';
$route['customer-csv-upload'] = 'b_level/Customer_controller/customer_csv_upload';
$route['sales-today'] = 'b_level/Customer_controller/sales_today';
$route['sales-yesterday'] = 'b_level/Customer_controller/sales_yesterday';
$route['sales-lastweek'] = 'b_level/Customer_controller/sales_lastweek';
$route['show-customer-new-window-popup'] = 'b_level/Customer_controller/show_customer_new_window_popup';
$route['new-customer-modal'] = 'b_level/Customer_controller/new_customer_modal';
$route['wholesaler-customer-search'] = 'b_level/Customer_controller/b_level_customer_search';
$route['customer-export-csv'] = 'b_level/Customer_controller/customer_export_csv';
$route['customer-export-pdf'] = 'b_level/Customer_controller/customer_export_pdf';
$route['top-search-customer-order-info'] = 'b_level/Customer_controller/top_search_customer_order_info';
$route['retailer-top-search-customer-order-info'] = 'c_level/Customer_controller/top_search_customer_order_info';


//============== its for  new_order ===============
$route['new-order'] = 'b_level/Order_controller/new_order';
$route['order-kanban'] = 'b_level/Order_controller/order_kanban';
$route['wholesaler-order-list'] = 'b_level/Order_controller/order_list';
$route['wholesaler-order-list/(:any)'] = 'b_level/Order_controller/order_list/$1';
$route['delete-order/(:any)'] = 'b_level/Order_controller/delete_order/$1';

$route['order-view/(:any)'] = 'b_level/Order_controller/order_view/$1';
$route['customer-wise-sidemark/(:any)'] = 'b_level/Order_controller/customer_wise_sidemark/$1';
$route['customer-wise-sidemark'] = 'b_level/Order_controller/customer_wise_sidemark';

$route['order-id-generate'] = 'b_level/Order_controller/order_id_generate';
$route['wholesaler-single-order-test'] = 'b_level/Account_controller/b_single_order_test';

$route['order-customer-info-edit/(:any)'] = 'b_level/Invoice_receipt/order_customer_info_edit/$1';
$route['order-single-product-edit/(:any)'] = 'b_level/Invoice_receipt/order_single_product_edit/$1';
$route['order-delete/(:any)'] = 'b_level/Order_controller/order_delete/$1';
$route['order-edit/(:any)/(:any)'] = 'b_level/Order_controller/new_order_edit/$1/$1';
$route['wholesaler-invoice-receipt/(:any)'] = 'b_level/invoice_receipt/receipt/$1';
$route['wholesaler-retailer-receipt/(:any)'] = 'b_level/invoice_receipt/c_receipt/$1';
$route['manufacturing-invoice-receipt/(:any)'] = 'b_level/invoice_receipt/manufacturing_receipt/$1';

//=============== its for category module===============
$route['add-category'] = 'b_level/Category_controller/add_category';
$route['manage-category'] = 'b_level/Category_controller/manage_category';
$route['category-edit/(:any)'] = 'b_level/Category_controller/category_edit/$1';
$route['category-assign'] = 'b_level/Category_controller/category_assign';
$route['category-filter'] = 'b_level/Category_controller/category_filter';
$route['wholesaler-category-delete/(:any)'] = 'b_level/Category_controller/b_category_delete/$1';
// $route['wholesaler-category-search'] = 'b_level/Category_controller/b_level_category_search';
$route['wholesaler-assinged-category-product-delete'] = 'b_level/Category_controller/assinged_category_product_delete';
$route['wholesaler-getCategoryLists'] = 'b_level/Category_controller/getCategoryLists';
$route['category-csv-upload'] = 'b_level/Category_controller/category_csv_upload';
$route['export-wholesaler-category'] = 'b_level/Category_controller/export_category';

// this is row material section
$route['add-row-material'] = 'b_level/Row_materials/add_row_material';
$route['test-mail'] = 'b_level/Row_materials/sendmail';
$route['row-material-list'] = 'b_level/Row_materials/row_material_list';
$route['raw-material-csv-upload'] = 'b_level/Row_materials/raw_material_csv_upload';
$route['export-wholesaler-row-material'] = 'b_level/Row_materials/export_raw_material';
$route['edit-row-material/(:any)'] = 'b_level/Row_materials/edit_row_material/$1';
$route['delete-row-material/(:any)'] = 'b_level/Row_materials/delete_row_material/$1';
$route['wholesaler-rawmaterial-search'] = 'b_level/Row_materials/b_level_rawmaterial_search';
$route['raw-material-usage'] = 'b_level/Row_materials/raw_material_usage';
$route['raw-material-usage-save'] = 'b_level/Row_materials/raw_material_usage_save';


// this is color section
$route['color'] = 'b_level/Color_controller/add_color';
$route['import-color-save'] = 'b_level/Color_controller/import_color_save';
$route['export-wholesaler-color'] = 'b_level/Color_controller/export_color';
$route['get-color-check/(:any)'] = 'b_level/Color_controller/get_color_check/$1';
$route['wholesaler-color-filter'] = 'b_level/Color_controller/color_filter';
$route['wholesaler-color-search'] = 'b_level/Color_controller/b_level_color_search';
$route['color-list-filter'] = 'b_level/Color_controller/b_level_color_filter';
$route['wholesaler-getColorLists'] = 'b_level/Color_controller/getColorLists';

//================= its for product module ================
$route['add-product'] = 'b_level/Product_controller/add_product';
$route['product-manage'] = 'b_level/Product_controller/product_manage';
$route['product-manage/(:num)'] = 'b_level/Product_controller/product_manage/$1';
$route['product-edit/(:any)'] = 'b_level/Product_controller/product_edit/$1';
$route['delete-product/(:any)'] = 'b_level/Product_controller/delete_product/$1';
$route['product-filter'] = 'b_level/Product_controller/product_filter';
$route['product-bulk-csv-upload'] = 'b_level/Product_controller/product_bulk_csv_upload';
$route['export-wholesaler-products'] = 'b_level/Product_controller/export_products';
$route['wholesaler-product-search'] = 'b_level/Product_controller/b_level_product_search';
$route['price-model-wise-style'] = 'b_level/Product_controller/price_model_wise_style';
$route['mapping-product-attribute/(:any)'] = 'b_level/product_controller/maping_product_attribute/$1';

//========== its for Pattern_controller ==============
$route['add-pattern'] = 'b_level/Pattern_controller/add_pattern';
$route['manage-pattern'] = 'b_level/Pattern_controller/manage_pattern';
//$route['pattern-edit/(:any)'] = 'b_level/Pattern_controller/pattern_edit/$1';
$route['pattern-edit/(:any)'] = 'b_level/Pattern_controller/pattern_edit/$1';
$route['pattern-delete/(:any)'] = 'b_level/Pattern_controller/pattern_delete/$1';
$route['pattern-model-filter'] = 'b_level/Pattern_controller/pattern_model_filter';
$route['wholesaler-pattern-search'] = 'b_level/Pattern_controller/b_level_pattern_search';
$route['wholesaler-assinged-product-delete'] = 'b_level/Pattern_controller/b_assinged_product_delete';
$route['wholesaler-getPatternLists'] = 'b_level/Pattern_controller/getPatternLists';

//============ its for Attribute_controller ================
$route['add-attribute'] = 'b_level/Attribute_controller/add_attribute';
$route['manage-attribute'] = 'b_level/Attribute_controller/manage_attribute';
$route['attribute-filter'] = 'b_level/Attribute_controller/attribute_filter';

$route['attribute-edit/(:any)'] = 'b_level/Attribute_controller/attribute_edit/$1';
$route['attribute-delete/(:any)'] = 'b_level/Attribute_controller/attribute_delete/$1';
$route['attribute-type'] = 'b_level/Attribute_controller/attribute_type';
$route['wholesaler-attribute-search'] = 'b_level/Attribute_controller/b_level_attribute_search';

//============= its for Condition_controller===============
$route['add-condition'] = 'b_level/Condition_controller/add_condition';
$route['condition-save'] = 'b_level/Condition_controller/condition_save';
$route['condition-edit/(:any)'] = 'b_level/Condition_controller/condition_edit/$1';
$route['condition-update/(:any)'] = 'b_level/Condition_controller/condition_update/$1';
$route['condition-delete/(:any)'] = 'b_level/Condition_controller/condition_delete/$1';
$route['condition-filter'] = 'b_level/Condition_controller/condition_filter';

$route['wholesaler-cost-factor'] = 'b_level/Setting_controller/cost_factor';
$route['import-costfactor-save'] = 'b_level/Setting_controller/import_costfactor_save';
$route['wholesaler-edit-cost-factor/(:any)'] = 'b_level/setting_controller/edit_cust_factor/$1';
$route['wholesaler-iframe-code'] = 'b_level/Setting_controller/iframe_code_generate';
$route['d-customer-iframe-generate/(:any)/(:any)'] = 'b_level/Setting_controller/d_customer_iframe_generate/$1/$2';
$route['wholesaler-d-customer-info-save'] = 'b_level/Setting_controller/b_d_customer_info_save';
$route['wholesaler-customer-iframe-view'] = 'b_level/Setting_controller/b_customer_iframe_view';
$route['wholesaler-cost-factor-save'] = 'b_level/Setting_controller/cost_factor_save';
$route['add-tax'] = 'b_level/Condition_controller/add_tax';

$route['shipping'] = 'b_level/Setting_controller/shipping';
$route['shipping-edit/(:any)'] = 'b_level/Setting_controller/shipping_edit/$1';
$route['shipping-method-delete/(:any)'] = 'b_level/Setting_controller/shipping_method_delete/$1';

//=========== its for Pricemodel_controller ==============
$route['add-price'] = 'b_level/Pricemodel_controller/add_price';
$route['edit-price/(:any)'] = 'b_level/pricemodel_controller/edit_style/$1';
$route['save-price-style'] = 'b_level/Pricemodel_controller/save_price_style';
$route['manage-price'] = 'b_level/Pricemodel_controller/manage_price';
$route['add-group'] = 'b_level/Pricemodel_controller/add_group';
$route['group-manage'] = 'b_level/Pricemodel_controller/group_manage';
$route['edit-group/(:any)'] = 'b_level/Pricemodel_controller/edit_group/$1';
$route['price-manage-filter'] = 'b_level/Pricemodel_controller/price_manage_filter';
$route['row-column-search'] = 'b_level/Pricemodel_controller/row_column_search';

//=========== group price ==============
$route['add-group-price'] = 'b_level/Pricemodel_controller/add_group_price';
$route['edit-group-price/(:any)'] = 'b_level/pricemodel_controller/edit_group_price_style/$1';
$route['manage-group-price'] = 'b_level/Pricemodel_controller/manage_group_price';
$route['group-manage-filter'] = 'b_level/Pricemodel_controller/group_manage_filter';
$route['group-price-search'] = 'b_level/Pricemodel_controller/group_price_search';

$route['group-price-mapping'] = 'b_level/product_controller/group_price_mapping';
$route['import-group-price-mapping'] = 'b_level/product_controller/import_group_price_save';
$route['export-wholesaler-group-price-mapping'] = 'b_level/product_controller/export_group_price_mapping';
$route['sqm-price-mapping'] = 'b_level/product_controller/sqm_price_mapping';
$route['import-sqm-price-mapping'] = 'b_level/product_controller/import_sqm_price_save';
$route['export-wholesaler-sqm-price-mapping'] = 'b_level/product_controller/export_sqm_price_mapping';

//=============== its for manufacturer module ===================
$route['add-manufacturer'] = 'b_level/Manufacturer_controller/add_manufacturer';
$route['add-manufacturer/(:any)'] = 'b_level/Manufacturer_controller/add_manufacturer/$1';
$route['manage-manufacturer'] = 'b_level/Manufacturer_controller/manage_manufacturer';
$route['manufacturer-edit/(:any)'] = 'b_level/Manufacturer_controller/manufacturer_edit/$1';
$route['wholeseler_go_manufacturing/(:any)'] = 'b_level/Order_controller/go_manufacturer_stage/$1';
$route['wholesaler-manufacture-quotation/(:any)'] = 'b_level/Manufacturer_controller/quatation/$1';


//=============== its for stock module ==============
$route['stock-availability'] = 'b_level/Stock_controller/stock_availability';
$route['stock-history'] = 'b_level/Stock_controller/stock_history';

//============== its for return =============
$route['show-return-resend'] = 'b_level/Return_controller/show_return_resend';
//$route['show-return-calculation'] = 'b_level/Return_controller/show_return_calculation';
$route['wholesaler-customer-return'] = 'b_level/Return_controller/customer_return';
$route['customer-order-return/(:any)'] = 'b_level/Return_controller/customer_order_return/$1';
$route['customer-order-return-save'] = 'b_level/Return_controller/customer_order_return_save';
$route['raw-material-return-purchase/(:any)'] = 'b_level/Return_controller/raw_material_return_purchase/$1';
$route['raw-material-supplier-return'] = 'b_level/Return_controller/raw_material_supplier_return';
$route['raw-material-return-purchase-save'] = 'b_level/Return_controller/raw_material_return_purchase_save';
$route['raw-material-return-purchase-approved'] = 'b_level/Return_controller/raw_material_return_purchase_approved';
$route['product-return'] = 'b_level/Return_controller/product_return';
$route['wholesaler-supplier-return-filter'] = 'b_level/Return_controller/supplier_return_filter';
//$route['raw-material-return-approved/(:any)'] = 'b_level/Return_controller/raw_material_return_approved/$1';
//$route['raw-material-return-rejected/(:any)'] = 'b_level/Return_controller/raw_material_return_rejected/$1';
//============= its for accounts module ============
$route['wholesaler-account-chart'] = 'b_level/Account_controller/b_account_chart';

$route['purchase-entry'] = 'b_level/Purchase_controller/purchase_entry';
$route['purchase-list'] = 'b_level/Purchase_controller/purchase_list';
$route['purchase-save'] = 'b_level/Purchase_controller/purchase_save';
$route['delete-purchase/(:any)'] = 'b_level/Purchase_controller/delete_purchase/$1';
$route['edit-purchase/(:any)'] = 'b_level/Purchase_controller/edit_purchase/$1';
$route['update-purchase'] = 'b_level/Purchase_controller/update_purchase';
$route['view-purchase/(:any)'] = 'b_level/Purchase_controller/view_purchase/$1';
$route['get_row_material_supplierwise/(:any)'] = 'b_level/Purchase_controller/get_row_material_supplierwise/$1';
$route['get-material-info/(:any)'] = 'b_level/Purchase_controller/get_material_wise_color_info/$1';
$route['get-material-wise-pattern-info/(:any)'] = 'b_level/Purchase_controller/get_material_wise_pattern_info/$1';
$route['get-material-color-pattern-wise-stock-qnt/(:any)/(:any)/(:any)'] = 'b_level/Purchase_controller/get_material_color_pattern_wise_stock_qnt/$1/$2/$3';

$route['wholesaler-debit-voucher'] = 'b_level/Account_controller/debit_voucher';
$route['create-wholesaler-debit-voucher'] = 'b_level/Account_controller/create_debit_voucher';
$route['wholesaler-debit-voucher-code/(:any)'] = 'b_level/Account_controller/debit_voucher_code/$1';
$route['wholesaler-show-selected-form/(:any)'] = 'b_level/Account_controller/selectedform/$1';
$route['wholesaler-insert-coa'] = 'b_level/Account_controller/insert_coa';
$route['wholesaler-new-account-form/(:any)'] = 'b_level/Account_controller/newform/$1';
$route['wholesaler-credit-voucher'] = 'b_level/Account_controller/credit_voucher';
$route['create-wholesaler-credit-voucher'] = 'b_level/Account_controller/create_credit_voucher';
$route['wholesaler-journal-voucher'] = 'b_level/Account_controller/journal_voucher';
$route['create-wholesaler-journal-voucher'] = 'b_level/Account_controller/create_journal_voucher';
$route['wholesaler-contra-voucher'] = 'b_level/Account_controller/contra_voucher';
$route['create-wholesaler-contra-voucher'] = 'b_level/Account_controller/create_contra_voucher';
$route['wholesaler-voucher-approval'] = 'b_level/Account_controller/voucher_approval';
$route['update-wholesaler-debit-voucher'] = 'b_level/Account_controller/update_debit_voucher';
$route['update-wholesaler-credit-voucher'] = 'b_level/Account_controller/update_credit_voucher';
$route['update-wholesaler-journal-voucher'] = 'b_level/Account_controller/update_journal_voucher';
$route['update-wholesaler-contra-voucher'] = 'b_level/Account_controller/update_contra_voucher';
$route['wholesaler-isactive/(:any)/(:any)'] = 'b_level/Account_controller/isactive/$1/$2';
$route['wholesaler-voucher-edit/(:any)'] = 'b_level/Account_controller/voucher_edit/$1';
$route['wholesaler-voucher-reports'] = 'b_level/Account_controller/voucher_reports';
$route['wholesaler-vouchar-cash/(:any)'] = 'b_level/Account_controller/vouchar_cash/$1';
$route['wholesaler-voucher-report-serach'] = 'b_level/Account_controller/voucher_report_serach';


$route['wholesaler-bank-book'] = 'b_level/Account_controller/bank_book';
$route['wholesaler-cash-book'] = 'b_level/Account_controller/cash_book';
$route['wholesaler-cash-flow'] = 'b_level/Account_controller/cash_flow';
$route['wholesaler-cash-flow-report-search'] = 'b_level/Account_controller/cash_flow_report_search';
$route['wholesaler-general-ledger'] = 'b_level/Account_controller/general_ledger';
$route['wholesaler-accounts-report-search'] = 'b_level/Account_controller/b_accounts_report_search';
$route['wholesaler-general-led'] = 'b_level/Account_controller/general_led';
$route['wholesaler-profit-loss'] = 'b_level/Account_controller/profit_loss';
$route['wholesaler-profit-loss-report-search'] = 'b_level/Account_controller/b_profit_loss_report_search';
$route['wholesaler-trial-ballance'] = 'b_level/Account_controller/trial_ballance';
$route['wholesaler-trial-balance-report'] = 'b_level/Account_controller/b_trial_ballance_report';


//============= its for settings module ============
$route['wholesaler-my-account'] = 'b_level/Setting_controller/my_account';
$route['profile-setting'] = 'b_level/Setting_controller/profile_setting';
$route['mail'] = 'b_level/Setting_controller/mail';
$route['update-mail-configure'] = 'b_level/Setting_controller/update_mail_configure';
$route['sms-configure'] = 'b_level/Setting_controller/sms_configure';
$route['sms-config-save'] = 'b_level/Setting_controller/sms_config_save';
$route['sms-config-verified/(:any)'] = 'b_level/Setting_controller/sms_config_verified/$1';
$route['sms-config-edit/(:any)'] = 'b_level/Setting_controller/sms_config_edit/$1';
$route['sms-config-update/(:any)'] = 'b_level/Setting_controller/sms_config_update/$1';
$route['sms-config-delete/(:any)'] = 'b_level/Setting_controller/sms_config_delete/$1';
$route['sms'] = 'b_level/Setting_controller/sms';
$route['sms-send'] = 'b_level/Setting_controller/sms_send';
$route['sms-csv-upload'] = 'b_level/Setting_controller/sms_csv_upload';
$route['email'] = 'b_level/Setting_controller/email';
$route['email-send'] = 'b_level/Setting_controller/email_send';
$route['group-sms-send'] = 'b_level/Setting_controller/group_sms_send';
$route['gateway'] = 'b_level/Setting_controller/gateway';
$route['save-gateway'] = 'b_level/Setting_controller/save_gateway';
$route['gateway-edit/(:any)'] = 'b_level/Setting_controller/gateway_edit/$1';
$route['gateway-delete/(:any)'] = 'b_level/Setting_controller/gateway_delete/$1';
$route['update-gateway/(:any)'] = 'b_level/Setting_controller/update_gateway/$1';
$route['wholesaler-change-password'] = 'b_level/Setting_controller/change_password';
$route['wholesaler-update-password'] = 'b_level/Setting_controller/update_password';
$route['wholesaler-access-logs'] = 'b_level/Setting_controller/access_logs';
$route['wholesaler-users'] = 'b_level/Setting_controller/b_level_users';
$route['wholesaler-users'] = 'b_level/Setting_controller/b_level_users';
$route['wholesaler-users/(:any)'] = 'b_level/Setting_controller/b_level_users/$1';
$route['user-filter'] = 'b_level/Setting_controller/user_filter';

$route['uom-list'] = 'b_level/Setting_controller/uom_list';
$route['us-state'] = 'b_level/Setting_controller/us_state';
$route['us-state-save'] = 'b_level/Setting_controller/us_state_save';
$route['us-state-edit/(:any)'] = 'b_level/Setting_controller/us_state_edit/$1';
$route['us-state-update/(:any)'] = 'b_level/Setting_controller/us_state_update/$1';
$route['us-state-delete/(:any)'] = 'b_level/Setting_controller/us_state_delete/$1';
$route['import-us-state-save'] = 'b_level/Setting_controller/import_us_state_save';
$route['wholesaler-usstate-search/(:any)'] = 'b_level/Setting_controller/b_level_usstate_search/$1';

//================ its for wholesaler Role permission===============
$route['wholesaler-role-assign'] = 'b_level/role_controller/user_roles';
$route['wholesaler-check-user-role'] = 'b_level/role_controller/b_level_check_user_role';
$route['wholesaler-access-role-list'] = 'b_level/role_controller/access_role';
$route['wholesaler-role-list'] = 'b_level/role_controller/role_list';
$route['wholesaler-role-permission'] = 'b_level/role_controller/role_permission';
$route['wholesaler-menu-setup'] = 'b_level/menusetup_controller/menu_setup';
$route['menu-export-csv'] = 'b_level/menusetup_controller/menu_export_csv';
$route['import-menu-save'] = 'b_level/menusetup_controller/import_menu_update';

$route['create-user'] = 'b_level/Setting_controller/create_user';
$route['get-check-user-unique-email'] = 'b_level/Setting_controller/get_check_user_unique_email';


$route['catalog-user'] = 'b_level/Shared_catalog_controller';
$route['catalog-user/(:any)'] = 'b_level/Shared_catalog_controller/index/$1';
$route['manufacturing-receipt/(:any)'] = 'b_level/Shared_catalog_controller/manufacturing_receipt/$1';
$route['wholesaler-catalog-receipt/(:any)'] = 'b_level/Shared_catalog_controller/receipt/$1';
$route['wholesaler-request-for-catalog'] = 'b_level/Shared_catalog_controller/b_user_request_for_catalog';

$route['not-approve-catalog-request/(:any)'] = 'b_level/Shared_catalog_controller/not_approve_catalog_request/$1';
$route['wholesaler-user-catalog-product/(:any)'] = 'b_level/Shared_catalog_controller/b_user_catalog_product_list/$1';
$route['catalog-product-add-order/(:any)'] = 'b_level/Shared_catalog_controller/catalog_product_order_add/$1';
$route['wholesaler-wise-sidemark/(:any)'] = 'b_level/Shared_catalog_controller/b_user_wise_sidemark/$1';
$route['catalog-order-list'] = 'b_level/Shared_catalog_controller/catalog_order_list';
$route['wholesaler-re-order'] = 'b_level/Shared_catalog_controller/re_order_b_user_order';
$route['manufacturing-order-view/(:any)'] = 'b_level/Shared_catalog_controller/manufacturing_order_view/$1';
$route['manufacturing-catalog-order-list'] = 'b_level/Shared_catalog_controller/manufacturing_catalog_order_list';
$route['catalog-wholesaler-user-order-list'] = 'b_level/Shared_catalog_controller/catalog_b_user_order_list';
$route['catalog-wholesaler-user-order-list/(:any)'] = 'b_level/Shared_catalog_controller/catalog_b_user_order_list/$1';

$route['catalog-request'] = 'b_level/Shared_catalog_controller/catalog_request_list';
$route['catalog-request-product/(:any)'] = 'b_level/Shared_catalog_controller/catalog_request_product_list/$1';
$route['approve-catalog-request'] = 'b_level/Shared_catalog_controller/approve_catalog_request';
$route['catalog-shipment/(:any)'] = 'b_level/Shared_catalog_controller/shipment/$1';
$route['payment-wholesaler-to-wholesaler/(:any)'] = 'b_level/Shared_catalog_controller/payment_b_to_b/$1';
$route['catalog-order-view/(:any)'] = 'b_level/Shared_catalog_controller/order_view/$1';

$route['catalog-user-receipt/(:any)'] = 'b_level/Shared_catalog_controller/b_user_receipt/$1';


// this is tag section
$route['gallery_tag'] = 'b_level/Tag_controller/add_tag';
$route['wholesaler-tag-search'] = 'b_level/Tag_controller/b_level_tag_search';
$route['get-tag-check/(:any)'] = 'b_level/Tag_controller/get_tag_check/$1';
$route['import-tag-save'] = 'b_level/Tag_controller/import_tag_save';

$route['add_wholesaler_gallery_image'] = 'b_level/Image_controller/add_image';
$route['view_wholesaler_gallery_image/(:any)'] = 'b_level/Image_controller/view_image/$1';
$route['edit_wholesaler_gallery_image/(:any)'] = 'b_level/Image_controller/get_image/$1';
$route['update_wholesaler_gallery_image/(:any)'] = 'b_level/Image_controller/update_image/$1';
$route['manage_wholesaler_gallery_image'] = 'b_level/Image_controller/manage_image';
$route['export_wholesaler_gallery_image'] = 'b_level/Image_controller/export_gallery_image';
$route['import_wholesaler_gallery_image_tag'] = 'b_level/Image_controller/import_image_tag_save';
$route['manage_wholesaler_gallery_image/(:any)'] = 'b_level/Image_controller/manage_image/$1';

//retailer
$route['retailer-rma-request'] = 'c_level/C_rma_controller';
$route['retailer-order-return-save'] = 'c_level/C_rma_controller/c_customer_order_return_save';
$route['retailer-credit-card'] = 'c_level/setting_controller/card_info';
$route['retailer-scan-product'] = 'c_level/Scan_product';
$route['c_gallery_tag'] = 'c_level/Tag_controller/add_tag';
$route['c_gallery_tag/(:any)'] = 'c_level/Tag_controller/add_tag/$1';
$route['retailer-tag-search'] = 'c_level/Tag_controller/c_level_tag_search';
$route['retailer-get-tag-check/(:any)'] = 'c_level/Tag_controller/get_tag_check/$1';
$route['retailer-import-tag-save'] = 'c_level/Tag_controller/import_tag_save';

$route['retailer_gallery_image'] = 'c_level/Image_controller/manage_image';
$route['retailer_gallery_image/(:any)'] = 'c_level/Image_controller/manage_image/$1';
$route['wholeseller_images'] = 'c_level/Image_controller/manage_b_level_image';
$route['wholeseller_images/(:any)'] = 'c_level/Image_controller/manage_b_level_image/$1';
$route['add_retailer_gallery_image'] = 'c_level/Image_controller/add_image';
$route['view_retailer_gallery_image/(:any)'] = 'c_level/Image_controller/view_image/$1';
$route['edit_retailer_gallery_image/(:any)'] = 'c_level/Image_controller/get_image/$1';
$route['update_retailer_gallery_image/(:any)'] = 'c_level/Image_controller/update_image/$1';
$route['export_retailer_gallery_image'] = 'c_level/Image_controller/export_gallery_image';
$route['import_retailer_gallery_image_tag'] = 'c_level/Image_controller/import_image_tag_save';

$route['b_commission_report'] = 'b_level/Commision_report_controller/search_b/All';
$route['b_commission_report/(:any)'] = 'b_level/Commision_report_controller/b_commission_report/$1';
$route['retailer_commission_report'] = 'c_level/Commision_report_controller/search_c/All';
$route['retailer_commission_reports'] = 'c_level/Commision_report_controller/c_commission_report';
$route['retailer_commission_reports/(:any)'] = 'c_level/Commision_report_controller/c_commission_report/$1';

$route['get_colors_based_on_pattern_ids'] = 'b_level/Product_controller/get_colors_based_on_pattern_ids';


$route['import-pattern-save'] = 'b_level/Pattern_controller/import_pattern_save';
$route['export-wholesaler-pattern'] = 'b_level/Pattern_controller/export_pattern';


$route['mapping'] = 'b_level/Mapping_controller';
$route['qb_connect'] = 'Qb_oauth_endpoint/quickbook';

// this is state list section
$route['wholesaler-state'] = 'b_level/State_controller';
$route['wholesaler-getStateLists'] = 'b_level/State_controller/getStateLists';
$route['import-state-save'] = 'b_level/State_controller/import_state_save';


// CUSTOMER CATALOG
$route['customer/catalog/category/add'] = 'c_level/Catalog_controller/catalog_category_add_edit_view';
$route['customer/catalog/category/edit/(:any)'] = 'c_level/Catalog_controller/catalog_category_add_edit_view/$1';

$route['retailer-category-delete/(:any)'] = 'c_level/Catalog_controller/c_category_delete/$1';
$route['customer/catalog/category/add-update-api'] = 'c_level/Catalog_controller/catalog_category_add_update_api';
$route['customer/catalog/category/list'] = 'c_level/Catalog_controller/catalog_category_list';
$route['customer/catalog/category/list/(:any)'] = 'c_level/Catalog_controller/catalog_category_list';

$route['customer/catalog/color/add'] = 'c_level/Catalog_controller/catalog_color_add_edit_view';
$route['customer/catalog/color/edit/(:any)'] = 'c_level/Catalog_controller/catalog_color_add_edit_view/$1';
$route['customer/catalog/color/add-update-api'] = 'c_level/Catalog_controller/catalog_color_add_update_api';
$route['customer/catalog/color/list'] = 'c_level/Catalog_controller/catalog_color_list';
$route['customer/catalog/color/list/(:any)'] = 'c_level/Catalog_controller/catalog_color_list/$1';


$route['retailer-upcharge-price'] = 'c_level/Catalog_controller/retailer_upcharge_price';
$route['retailer-upcharge-price-save'] = 'c_level/Catalog_controller/retailer_upcharge_price_save';


$route['customer/catalog/product/add'] = 'c_level/Catalog_controller/catalog_product_add_view';
$route['customer/catalog/product/save'] = 'c_level/Catalog_controller/save_product_style';
$route['customer/catalog/product/edit/(:any)'] = 'c_level/Catalog_controller/catalog_product_edit_view/$1';
$route['customer/catalog/product/update-api'] = 'c_level/Catalog_controller/catalog_product_update_api';
$route['customer/catalog/product/list'] = 'c_level/Catalog_controller/catalog_product_list';
$route['customer/catalog/product/list/(:any)'] = 'c_level/Catalog_controller/catalog_product_list/$1';

$route['customer/catalog/pattern/add'] = 'c_level/Catalog_controller/catalog_pattern_add_edit_view';
$route['customer/catalog/pattern/edit/(:any)'] = 'c_level/Catalog_controller/catalog_pattern_add_edit_view/$1';
$route['customer/catalog/pattern/add-update-api'] = 'c_level/Catalog_controller/catalog_pattern_add_update_api';
$route['customer/catalog/pattern/list'] = 'c_level/Catalog_controller/catalog_pattern_list';
$route['customer/catalog/pattern/list/(:any)'] = 'c_level/Catalog_controller/catalog_pattern_list/$1';

$route['customer/catalog/attribute/add'] = 'c_level/Catalog_controller/catalog_attribute_add_edit_view';
$route['customer/catalog/attribute/edit/(:any)'] = 'c_level/Catalog_controller/catalog_attribute_add_edit_view/$1';
$route['customer/catalog/attribute/add-update-api'] = 'c_level/Catalog_controller/catalog_attribute_add_update_api';
$route['customer/catalog/attribute/list'] = 'c_level/Catalog_controller/catalog_attribute_list';
$route['customer/catalog/attribute/list/(:any)'] = 'c_level/Catalog_controller/catalog_attribute_list/$1';



$route['customer/catalog/price/add'] = 'c_level/Catalog_controller/catalog_price_add_view';
$route['customer/catalog/price/save'] = 'c_level/Catalog_controller/save_price_style';
$route['customer/catalog/price/edit/(:any)'] = 'c_level/Catalog_controller/catalog_price_edit_view/$1';
$route['customer/catalog/price/list'] = 'c_level/Catalog_controller/catalog_price_list';
$route['customer/catalog/price/list/(:any)'] = 'c_level/Catalog_controller/catalog_price_list/$1';



$route['customer/catalog/sqm-price-mapping/list'] = 'c_level/Catalog_controller/catalog_sqm_price_mapping';
$route['customer/catalog/sqm-price-mapping/list/(:any)'] = 'c_level/Catalog_controller/catalog_sqm_price_mapping/$1';

$route['customer/catalog/group-price-mapping/list'] = 'c_level/Catalog_controller/catalog_group_price_mapping';
$route['customer/catalog/group-price-mapping/list/(:any)'] = 'c_level/Catalog_controller/catalog_group_price_mapping/$1';


$route['customer/catalog/group-price/add'] = 'c_level/Catalog_controller/catalog_group_price_add_view';
$route['customer/catalog/group-price/save'] = 'c_level/Catalog_controller/save_price_style';
$route['customer/catalog/group-price/edit/(:any)'] = 'c_level/Catalog_controller/catalog_group_price_edit_view/$1';
$route['customer/catalog/group-price/list'] = 'c_level/Catalog_controller/catalog_group_price_list';
$route['customer/catalog/group-price/list/(:any)'] = 'c_level/Catalog_controller/catalog_group_price_list/$1';
$route['update-customer-menu'] = 'super_admin/Auth_controller/update_customer_menu';
$route['update-wholesaler-menu'] = 'super_admin/Auth_controller/update_wholesaler_menu';

// SUPER ADMIN
$route['super-admin'] = 'super_admin/Auth_controller';
$route['super-admin/logout'] = 'super_admin/Auth_controller/super_admin_logout';
$route['super-admin/dashboard'] = 'super_admin/Dashboard_controller';
$route['super-admin/sms-config-verified/(:any)'] = 'super_admin/Setting_controller/sms_config_verified/$1';
$route['super-admin/sms-config-edit/(:any)'] = 'super_admin/Setting_controller/sms_config_edit/$1';
$route['super-admin/sms-config-delete/(:any)'] = 'super_admin/Setting_controller/sms_config_delete/$1';
$route['super-admin/sms-config-update/(:any)'] = 'super_admin/Setting_controller/sms_config_update/$1';
$route['super-admin-menu-setup'] = 'super_admin/menusetup_controller/menu_setup';
$route['super-admin/customer/list'] = 'super_admin/Customer_controller/customer_list';
$route['super-admin/customer/add'] = 'super_admin/Customer_controller/add_customer_view';
$route['super-admin/customer/edit/(:any)'] = 'super_admin/Customer_controller/add_customer_view/$1';
$route['super-admin-role-list'] = 'super_admin/role_controller/role_list';
$route['super-admin-permission'] = 'super_admin/role_controller/role_permission';
$route['super-admin-role-permission'] = 'super_admin/role_controller/role_permission';
$route['super-admin/search/all-customer-list-api'] = 'super_admin/Customer_controller/search_customer_list_api';
$route['super-admin/menu-export-csv'] = 'super_admin/menusetup_controller/menu_export_csv';
$route['super-admin/wholesaler/list'] = 'super_admin/B_user_controller/b_user_list';
$route['super-admin-menu-search'] = 'super_admin/Menusetup_controller/super_admin_menu_search';
$route['super-admin/search/wholesaler-list-api'] = 'super_admin/B_user_controller/search_b_user_list_api';
$route['super-admin/wholesaler/add'] = 'super_admin/B_user_controller/add_edit_b_user_view';
$route['super-admin/wholesaler/edit/(:any)'] = 'super_admin/B_user_controller/add_edit_b_user_view/$1';

$route['super-admin-access-role-list'] = 'super_admin/role_controller/access_role';
$route['super-admin-role-assign'] = 'super_admin/role_controller/user_roles';
$route['super-admin-check-user-role'] = 'super_admin/role_controller/super_admin_check_user_role';

$route['super-admin-users'] = 'super_admin/Setting_controller/super_admin_users';
$route['super-admin/user-filter'] = 'super_admin/Setting_controller/user_filter';
$route['super-admin/wholesaler/company-edit/(:any)'] = 'super_admin/b_user_controller/edit_company_detail/$1';

$route['super-admin/customer/company-edit/(:any)'] = 'super_admin/customer_controller/edit_company_detail/$1';

$route['super-admin/package/list'] = 'super_admin/Package_controller';
$route['super-admin/package/edit/(:any)/(:any)'] = 'super_admin/Package_controller/edit_package_view/$1/$2';
$route['super-admin/wholesaler/package/list'] = 'super_admin/Package_controller/wholesaler';
$route['super-admin/wholesaler/package/edit/(:any)/(:any)'] = 'super_admin/Package_controller/edit_wholesaler_package_view/$1/$2';

$route['super-admin/package-attribute/list'] = 'super_admin/Package_controller/list_package_attribute';
$route['super-admin/package-attribute/edit/(:any)'] = 'super_admin/Package_controller/edit_package_attribute_view/$1/$2';
$route['super-admin/wholesaler/package-attribute/list'] = 'super_admin/Package_controller/list_wholesaler_package_attribute';
$route['super-admin/wholesaler/package-attribute/edit/(:any)'] = 'super_admin/Package_controller/edit_wholesaler_package_attribute_view/$1/$2';

$route['super-admin/orders/list'] = 'super_admin/Order_controller';
$route['super-admin/search/order-list-api'] = 'super_admin/Order_controller/search_order_list_api';
$route['super-admin/orders/receipt/(:any)'] = 'super_admin/Order_controller/single_order_receipt_view/$1';

$route['super-admin/customer-orders/list'] = 'super_admin/Order_controller/customer_order_list';
$route['super-admin/search/customer-order-list-api'] = 'super_admin/Order_controller/customer_search_order_list_api';
$route['super-admin/customer-orders/receipt/(:any)'] = 'super_admin/Order_controller/single_customer_order_receipt_view/$1';

$route['super-admin/reserve-domain/list'] = 'super_admin/Reserve_domain_controller/reserve_domain_list';
$route['super-admin/search/reserve-domain-list-api'] = 'super_admin/Reserve_domain_controller/search_reserve_domain_list_api';
$route['super-admin/reserve-domain/add'] = 'super_admin/Reserve_domain_controller/add_edit_reserve_domain_view';
$route['super-admin/reserve-domain/edit/(:any)'] = 'super_admin/Reserve_domain_controller/add_edit_reserve_domain_view/$1';


$route['super-admin/customer-catalog/list'] = 'super_admin/Catalog_controller/customer_catalog_list';
$route['super-admin/search/customer-catalog-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_list_api';
$route['super-admin/customer-catalog/catagory/list/(:any)'] = 'super_admin/Catalog_controller/catalog_category_list/$1';
$route['super-admin/search/customer-catalog-category-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_category_list_api';
$route['super-admin/customer-catalog/color/list/(:any)'] = 'super_admin/Catalog_controller/catalog_color_list/$1';
$route['super-admin/search/customer-catalog-color-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_color_list_api';
$route['super-admin/customer-catalog/pattern/list/(:any)'] = 'super_admin/Catalog_controller/catalog_pattern_list/$1';
$route['super-admin/search/customer-catalog-pattern-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_pattern_list_api';
$route['super-admin/customer-catalog/product/list/(:any)'] = 'super_admin/Catalog_controller/catalog_product_list/$1';
$route['super-admin/search/customer-catalog-product-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_product_list_api';
$route['super-admin/customer-catalog/attribute/list/(:any)'] = 'super_admin/Catalog_controller/catalog_attribute_list/$1';
$route['super-admin/search/customer-catalog-attribute-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_attribute_list_api';
$route['super-admin/customer-catalog/price/list/(:any)'] = 'super_admin/Catalog_controller/catalog_price_list/$1';
$route['super-admin/search/customer-catalog-price-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_price_list_api';
$route['super-admin/customer-catalog/group-price/list/(:any)'] = 'super_admin/Catalog_controller/catalog_group_price_list/$1';
$route['super-admin/search/customer-catalog-group-price-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_group_price_list_api';

$route['super-admin/customer-catalog/sqm-price-mapping/list/(:any)'] = 'super_admin/Catalog_controller/catalog_sqm_price_mapping_list/$1';
$route['super-admin/search/customer-catalog-sqm-price-mapping-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_sqm_price_mapping_list_api';

$route['super-admin/customer-catalog/group-price-mapping/list/(:any)'] = 'super_admin/Catalog_controller/catalog_group_price_mapping_list/$1';
$route['super-admin/search/customer-catalog-group-price-mapping-list-api'] = 'super_admin/Catalog_controller/search_customer_catalog_group_price_mapping_list_api';


$route['super-admin/wholesaler-catalog/category/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_category_list/$1';
$route['super-admin/search/wholesaler-catalog-category-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_category_list_api';

$route['super-admin/wholesaler-catalog/color/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_color_list/$1';
$route['super-admin/search/wholesaler-catalog-color-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_color_list_api';

$route['super-admin/wholesaler-catalog/pattern/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_pattern_list/$1';
$route['super-admin/search/wholesaler-catalog-pattern-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_pattern_list_api';

$route['super-admin/wholesaler-catalog/product/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_product_list/$1';
$route['super-admin/search/wholesaler-catalog-product-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_product_list_api';

$route['super-admin/wholesaler-catalog/attribute/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_attribute_list/$1';
$route['super-admin/search/wholesaler-catalog-attribute-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_attribute_list_api';

$route['super-admin/wholesaler-catalog/price/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_price_list/$1';
$route['super-admin/search/wholesaler-catalog-price-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_price_list_api';

$route['super-admin/wholesaler-catalog/group-price/list/(:any)'] = 'super_admin/Catalog_controller/b_user_catalog_group_price_list/$1';
$route['super-admin/search/wholesaler-catalog-group-price-list-api'] = 'super_admin/Catalog_controller/search_b_user_catalog_group_price_list_api';

$route['retailer-manufacturing/list'] = 'c_level/Order_controller/c_user_manufactur_list/$1';
$route['retailer-manufacturing/list/(:any)'] = 'c_level/Order_controller/c_user_manufactur_list/$1';

$route['retailer-supplier/list'] = 'c_level/Supplier_controller/supplier_list/$1';
$route['retailer-supplier/list/(:any)'] = 'c_level/Supplier_controller/supplier_list/$1';
$route['retailer-supplier/add-edit'] = 'c_level/Supplier_controller/supplier_add_edit_view/$1';
$route['retailer-supplier/add-edit/(:any)'] = 'c_level/Supplier_controller/supplier_add_edit_view/$1';

$route['retailer-stock-availability'] = 'c_level/Stock_controller/stock_availability';
$route['retailer-stock-history'] = 'c_level/Stock_controller/stock_history';

$route['retailer-customer-return'] = 'c_level/C_return_controller/customer_return';
$route['retailer-supplier-return'] = 'c_level/C_return_controller/raw_material_supplier_return';

$route['retailer-supplier-return-filter'] = 'c_level/C_return_controller/supplier_return_filter';

$route['retailer-raw-material-usage'] = 'c_level/Row_materials/raw_material_usage';
$route['retailer-raw-material-usage-save'] = 'c_level/Row_materials/raw_material_usage_save';

$route['retailer-cost-factor'] = 'c_level/Setting_controller/blevel_cost_factor';


$route['send_notification_before_one_day'] = 'c_level/Appointment_controller/send_notification_before_one_day';
$route['send_notification_before_one_hour'] = 'c_level/Appointment_controller/send_notification_before_one_hour';


$route['package'] = 'c_level/Package_controller/package';
$route['retailer-trail-upgrade-package/(:any)'] = 'c_level/Package_controller/trail_upgrade_package/$1';
$route['wholesaler-trail-upgrade-package/(:any)'] = 'b_level/Package_controller/trail_upgrade_package/$1';
$route['retailer-confirm-paid-upgrade-package/(:any)'] = 'c_level/Package_controller/confirm_paid_upgrade_package/$1';
$route['retailer-confirm-paid-upgrade-package/(:any)/(:any)'] = 'c_level/Package_controller/confirm_paid_upgrade_package/$1/$2';
$route['retailer-paid-upgrade-package'] = 'c_level/Package_controller/paid_upgrade_package';
$route['payment-success'] = 'Front_controller/payment_success';

//wholesaler package
$route['wholesaler-package'] = 'b_level/Package_controller/package'; 
$route['wholesaler-confirm-paid-upgrade-package/(:any)'] = 'b_level/Package_controller/confirm_paid_upgrade_package/$1';
$route['wholesaler-package-transaction/list'] = 'b_level/Package_controller/package_transaction';
$route['wholesaler-package-my-subscription'] = 'b_level/package_controller/my_subscription';
$route['wholesaler-credit-card'] = 'b_level/setting_controller/card_info';
$route['wholesaler-paid-upgrade-package'] = 'b_level/Package_controller/paid_upgrade_package';
$route['wholesaler-payment-failed'] = 'b_level/Package_controller/payment_failed';

$route['super-admin/tms-payment-setting'] = 'super_admin/Package_controller/tms_payment_receiver_setting';
$route['super-admin/sms-configure'] = 'super_admin/Setting_controller/sms_configure';
$route['super-admin/sms-config-save'] = 'super_admin/Setting_controller/sms_config_save';
$route['super-admin/mail'] = 'super_admin/Setting_controller/mail';
$route['super-admin/update-mail-configure'] = 'super_admin/Setting_controller/update_mail_configure';
$route['super-admin/email'] = 'super_admin/Setting_controller/email';
$route['super-admin/email-send'] = 'super_admin/Setting_controller/email_send';
$route['super-admin/sms'] = 'super_admin/Setting_controller/sms';
$route['super-admin/sms-send'] = 'super_admin/Setting_controller/sms_send';
$route['super-admin/sms-csv-upload'] = 'super_admin/Setting_controller/sms_csv_upload';
$route['super-admin/group-sms-send'] = 'super_admin/Setting_controller/group_sms_send';
$route['super-admin/wholesaler-package-transaction/list'] = 'super_admin/Package_controller/wholesaler_package_transaction';
$route['super-admin/package-transaction/list'] = 'super_admin/Package_controller/package_transaction';
$route['super-admin/transaction-list-search'] = 'super_admin/Package_controller/package_transaction_list_filtter';

$route['domain-not-found'] = 'super_admin/Auth_controller/domain_not_found';

$route['change-password'] = 'super_admin/Auth_controller/change_password';

$route['customer-uom-list'] = 'c_level/Setting_controller/customer_uom_list';
$route['customer-uom-list/(:any)'] = 'c_level/Setting_controller/customer_uom_list/$1';

$route['customer-shipping'] = 'c_level/Setting_controller/customer_shipping';
$route['customer-shipping-edit/(:any)'] = 'c_level/setting_controller/edit_shipping_method/$1';


$route['domain-not-found'] = 'super_admin/Auth_controller/domain_not_found';
$route['change-password'] = 'super_admin/Auth_controller/change_password';
$route['customer-shipping'] = 'c_level/Setting_controller/customer_shipping';
$route['super-admin/coupon/list'] = 'super_admin/Coupon_controller/coupon_list';
$route['super-admin/search/coupon-list-api'] = 'super_admin/Coupon_controller/search_coupon_list_api';
$route['super-admin/coupon/add'] = 'super_admin/Coupon_controller/add_edit_coupon_view';
$route['super-admin/coupon/edit/(:any)'] = 'super_admin/Coupon_controller/add_edit_coupon_view/$1';
$route['super-admin/import-menu-save'] = 'super_admin/menusetup_controller/import_menu_update';

$route['payment-method'] = 'c_level/Package_controller/card_list/';
$route['payment-method/(:any)'] = 'c_level/Package_controller/card_list/$1';


$route['payment-failed'] = 'c_level/Package_controller/payment_failed';

$route['package-transaction/list'] = 'c_level/Package_controller/package_transaction';


$route['change-subscription-date/(:any)/(:any)'] = 'super_admin/Package_controller/change_subscription_date/$1/$2';
$route['change-wholesaler-subscription-date/(:any)/(:any)'] = 'super_admin/Package_controller/wholesaler_change_subscription_date/$1/$2';

$route['wholesaler-connection'] = 'c_level/Customer_controller/wholesaler_connection';

//================== set route for retailer ==============
$route['retailer_go_manufacturing/(:any)'] = 'c_level/Order_controller/go_retailer_stage/$1';

//================== its for scan product ================

$route['scan-product'] = 'c_level/Scan_product_controller';

//================== its for RMA C Level ================

$route['rma-view-list'] = 'c_level/C_rma_controller/rma_view_list';
$route['rma-in-progress-list'] = 'c_level/C_rma_controller/rma_in_progress_list';
$route['rma-completed-list'] = 'c_level/C_rma_controller/rma_completed_list';

//================== its for RMA B Level ================

$route['rma-wholesaler-return-view-list'] = 'b_level/Return_controller/wholesaler_return';
$route['rma-wholesaler-return-in-progress-list'] = 'b_level/Return_controller/wholesaler_return_in_progress_list';
$route['rma-wholesaler-return-completed-list'] = 'b_level/Return_controller/wholesaler_return_completed_list';
$route['resend-retailer-data'] = 'b_level/Return_controller/resend_retailer_data';

$route['show-retailer-return-resend'] = 'b_level/Return_controller/show_retailer_return_resend';

// Added for new rma request and code by TV 10-02-2020

$route['wholesaler-rma-request'] = 'b_level/B_rma_controller';

//front
$route['Home'] = 'front_section/Front_controller'; 
$route['front-wholesaler'] ='front_section/Front_controller/add_wholesaler';

