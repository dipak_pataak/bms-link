// Queries by TV 07-01-2020

CREATE TABLE `bms_04_12_2019`.`tbl_blinds_shades_count` ( `id` INT NOT NULL AUTO_INCREMENT , `blind_length_inch` INT NOT NULL , `2_inch_slats` INT NOT NULL , `2_5_inch_slats` INT NOT NULL , `3_inch_slats` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '12', '6', '5', '4'), (NULL, '13', '6', '6', '4');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '14', '7', '7', '4'), (NULL, '15', '8', '7', '5'), (NULL, '16', '9', '7', '5'), (NULL, '17', '9', '8', '6'), (NULL, '18', '10', '8', '6');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '19', '10', '9', '7'), (NULL, '20', '11', '9', '7');


// Queries by TV 08-01-2020

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '21', '12', '9', '7'), (NULL, '22', '12', '10', '8'), (NULL, '23', '13', '11', '8'), (NULL, '24', '13', '11', '9'), (NULL, '25', '14', '12', '9'), (NULL, '26', '14', '12', '9'), (NULL, '27', '14', '12', '9'), (NULL, '28', '16', '13', '10'), (NULL, '29', '16', '14', '10'), (NULL, '30', '17', '14', '11');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '31', '17', '14', '11'), (NULL, '32', '18', '15', '12'), (NULL, '33', '18', '16', '12'), (NULL, '34', '19', '16', '12'), (NULL, '35', '20', '16', '13'), (NULL, '36', '20', '16', '13'), (NULL, '37', '21', '17', '14'), (NULL, '38', '21', '17', '14'), (NULL, '39', '22', '18', '14'), (NULL, '40', '22', '18', '15');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '41', '23', '19', '15'), (NULL, '42', '24', '19', '16'), (NULL, '43', '24', '20', '16'), (NULL, '44', '25', '20', '16'), (NULL, '45', '25', '21', '17'), (NULL, '46', '26', '21', '17'), (NULL, '47', '27', '22', '18'), (NULL, '48', '27', '22', '18'), (NULL, '49', '28', '22', '18'), (NULL, '50', '28', '22', '19');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '51', '29', '23', '19'), (NULL, '52', '29', '23', '19'), (NULL, '53', '30', '24', '20'), (NULL, '54', '31', '24', '20'), (NULL, '55', '31', '25', '21'), (NULL, '56', '32', '26', '21'), (NULL, '57', '32', '26', '21'), (NULL, '58', '33', '26', '22'), (NULL, '59', '33', '27', '22'), (NULL, '60', '34', '27', '22');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '61', '35', '28', '23'), (NULL, '62', '35', '28', '23'), (NULL, '63', '36', '28', '24'), (NULL, '64', '36', '29', '24'), (NULL, '65', '37', '29', '25'), (NULL, '66', '37', '29', '25'), (NULL, '67', '38', '30', '25'), (NULL, '68', '39', '30', '26'), (NULL, '69', '39', '30', '26'), (NULL, '70', '40', '32', '26');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '71', '40', '32', '27'), (NULL, '72', '41', '33', '27'), (NULL, '73', '42', '33', '28'), (NULL, '74', '42', '34', '28'), (NULL, '75', '43', '34', '28'), (NULL, '76', '43', '35', '29'), (NULL, '77', '44', '36', '29'), (NULL, '78', '44', '36', '30'), (NULL, '79', '45', '37', '30'), (NULL, '80', '46', '37', '30');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '81', '46', '37', '31'), (NULL, '82', '47', '38', '31'), (NULL, '83', '47', '38', '32'), (NULL, '84', '48', '39', '32'), (NULL, '85', '48', '39', '32'), (NULL, '86', '49', '40', '33'), (NULL, '87', '50', '40', '33'), (NULL, '88', '50', '41', '33'), (NULL, '89', '51', '41', '34'), (NULL, '90', '51', '42', '34');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '91', '52', '42', '35'), (NULL, '92', '52', '42', '35'), (NULL, '93', '53', '43', '35'), (NULL, '94', '54', '43', '36'), (NULL, '95', '54', '44', '36'), (NULL, '96', '55', '45', '37'), (NULL, '97', '55', '46', '37'), (NULL, '98', '56', '46', '37'), (NULL, '99', '57', '47', '0'), (NULL, '100', '57', '47', '0');

INSERT INTO `tbl_blinds_shades_count` (`id`, `blind_length_inch`, `2_inch_slats`, `2_5_inch_slats`, `3_inch_slats`) VALUES (NULL, '101', '58', '48', '0'), (NULL, '102', '58', '48', '0'), (NULL, '103', '59', '49', '0'), (NULL, '104', '59', '49', '0'), (NULL, '105', '60', '50', '0'), (NULL, '106', '61', '50', '0'), (NULL, '107', '61', '51', '0'), (NULL, '108', '62', '51', '0');


// Query by TV 20-01-2020

ALTER TABLE `qutation_details` CHANGE `order_id` `order_id` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

//Query BY TV 31-01-2020

ALTER TABLE `qutation_details` ADD `status` VARCHAR(50) NULL AFTER `created_status`;

ALTER TABLE `quatation_tbl` ADD `manufacture_order_date` DATETIME NULL AFTER `order_stage`, ADD `ship_order_date` DATETIME NULL AFTER `manufacture_order_date`;

//Query BY TV 03-02-2020

ALTER TABLE `order_return_tbl` ADD `return_status` TINYINT(4) NOT NULL DEFAULT '0' AFTER `is_approved`;
ALTER TABLE `order_return_tbl` CHANGE `return_status` `return_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0-Pending,1-Production,2-Shipping,3- Delivered';