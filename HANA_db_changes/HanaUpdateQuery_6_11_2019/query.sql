ALTER TABLE `hana_package` ADD `monthly_plan_id` VARCHAR(255) NULL DEFAULT NULL AFTER `yearly_price`, ADD `yearly_plan_id` VARCHAR(255) NULL DEFAULT NULL AFTER `monthly_plan_id`;

UPDATE `hana_package` SET `monthly_plan_id` = 'advance_monthly' WHERE `hana_package`.`id` = 2;
UPDATE `hana_package` SET `yearly_plan_id` = 'advance_yearly' WHERE `hana_package`.`id` = 2;
UPDATE `hana_package` SET `monthly_plan_id` = 'pro_monthly' WHERE `hana_package`.`id` = 3;
UPDATE `hana_package` SET `yearly_plan_id` = 'pro_yearly' WHERE `hana_package`.`id` = 3;

ALTER TABLE `user_info` ADD `package_duration` TINYINT NULL DEFAULT NULL COMMENT '1:monthly, 2:yearly' AFTER `login_token`;

UPDATE `menusetup_tbl` SET `page_url` = 'c_level/package_controller/my_subscription' WHERE `menusetup_tbl`.`id` = 95;