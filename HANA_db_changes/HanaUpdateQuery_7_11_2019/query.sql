ALTER TABLE `user_info` ADD `card_id` TINYINT NULL DEFAULT NULL AFTER `package_duration`;

ALTER TABLE `user_info` ADD `subscription_id` VARCHAR(255) NULL DEFAULT NULL AFTER `card_id`;