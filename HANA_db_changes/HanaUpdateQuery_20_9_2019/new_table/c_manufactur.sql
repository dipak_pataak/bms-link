-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2019 at 06:55 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `c_manufactur`
--

CREATE TABLE `c_manufactur` (
  `id` int(11) NOT NULL,
  `order_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `cancel_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `synk_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_manufactur`
--

INSERT INTO `c_manufactur` (`id`, `order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `cancel_comment`, `level_id`, `order_date`, `created_by`, `updated_by`, `synk_status`, `created_date`, `updated_date`) VALUES
(1, 'itit-capt-Ingo-001', 3, 'caption-Ingolstadt', '', 'assets/barcode/c/itit-capt-Ingo-001.jpg', '0', '', 0, 0, 0, 0, 0, '0', 121, 121, 0, 121, 0, '', 4, NULL, 12, '2019-09-20', 12, 0, 0, '2019-09-20', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c_manufactur`
--
ALTER TABLE `c_manufactur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c_manufactur`
--
ALTER TABLE `c_manufactur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
