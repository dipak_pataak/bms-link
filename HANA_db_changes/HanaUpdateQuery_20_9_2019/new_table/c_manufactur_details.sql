-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2019 at 06:55 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `c_manufactur_details`
--

CREATE TABLE `c_manufactur_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `list_price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `unit_total_price` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_manufactur_details`
--

INSERT INTO `c_manufactur_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `list_price`, `discount`, `unit_total_price`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`, `created_status`) VALUES
(1, 'itit-capt-Ingo-001', 'Family', '55', 9, 1, 121, 0, 121, 470, 2544, 2, 4, 0, 0, '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c_manufactur_details`
--
ALTER TABLE `c_manufactur_details`
  ADD PRIMARY KEY (`row_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c_manufactur_details`
--
ALTER TABLE `c_manufactur_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
