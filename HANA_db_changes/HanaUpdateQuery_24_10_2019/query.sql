INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'integrations', 'integrations', '', 'settings', '3', '27', '1', '0', '', '1', '0', '2', '2019-03-14 14:24:23');

UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 29;
UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 61;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'order_to_wholesaler', 'order_to_wholesaler', 'order-to-wholesaler', 'order', '0', '4', '1', '0', '', '1', '0', '48', '2018-12-12 11:47:40');








INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'return', 'return', '', 'order', '5', '4', '1', '0', '', '1', '0', '2', '2018-12-12 11:53:22');

UPDATE `menusetup_tbl` SET `parent_menu` = '98' WHERE `menusetup_tbl`.`id` = 9;
UPDATE `menusetup_tbl` SET `parent_menu` = '98' WHERE `menusetup_tbl`.`id` = 64;





INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'my_orders', 'my_orders', 'my-orders', 'order', '2', '4', '1', '0', '', '1', '0', '2', '2019-05-09 11:01:35');



INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'Wholesaler Connection', 'wholesaler_connection', 'wholesaler-connection', 'wholesaler_connection', '1', '0', '1', '0', 'simple-icon-link', '1', '0', '2', '2019-06-24 11:42:52');

UPDATE `menusetup_tbl` SET `ordering` = '6' WHERE `menusetup_tbl`.`id` = 27;
UPDATE `menusetup_tbl` SET `ordering` = '5' WHERE `menusetup_tbl`.`id` = 67;



UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 6;
UPDATE `menusetup_tbl` SET `ordering` = '3' WHERE `menusetup_tbl`.`id` = 99;

UPDATE `menusetup_tbl` SET `ordering` = '5' WHERE `menusetup_tbl`.`id` = 76;
UPDATE `menusetup_tbl` SET `ordering` = '6' WHERE `menusetup_tbl`.`id` = 87;
UPDATE `menusetup_tbl` SET `ordering` = '8' WHERE `menusetup_tbl`.`id` = 27;


INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'Scan Product', 'Scan Product', 'scan-product', 'scan_product', '7', '0', '1', '0', 'iconsmind-Statistic', '1', '0', '2', '2019-06-24 11:42:52');
UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 66;

UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 80;
UPDATE `menusetup_tbl` SET `ordering` = '3' WHERE `menusetup_tbl`.`id` = 78;
UPDATE `menusetup_tbl` SET `ordering` = '5' WHERE `menusetup_tbl`.`id` = 81;
UPDATE `menusetup_tbl` SET `ordering` = '6' WHERE `menusetup_tbl`.`id` = 79;


/********************************/

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'price_model', 'price_model', '', 'c_catalog', '4', '76', '1', '0', NULL, '1', '0', '2', '2018-12-12 12:01:35');
UPDATE `menusetup_tbl` SET `parent_menu` = '102' WHERE `menusetup_tbl`.`id` = 85;
UPDATE `menusetup_tbl` SET `parent_menu` = '102' WHERE `menusetup_tbl`.`id` = 86;
UPDATE `menusetup_tbl` SET `parent_menu` = '102' WHERE `menusetup_tbl`.`id` = 82;
UPDATE `menusetup_tbl` SET `parent_menu` = '102' WHERE `menusetup_tbl`.`id` = 83;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 83;
UPDATE `menusetup_tbl` SET `ordering` = '3' WHERE `menusetup_tbl`.`id` = 86;
UPDATE `menusetup_tbl` SET `ordering` = '4' WHERE `menusetup_tbl`.`id` = 85;
UPDATE `menusetup_tbl` SET `menu_title` = 'Group Mapping' WHERE `menusetup_tbl`.`id` = 86;
UPDATE `menusetup_tbl` SET `menu_title` = 'Group Price' WHERE `menusetup_tbl`.`id` = 83;
UPDATE `menusetup_tbl` SET `menu_title` = 'Row Column Price' WHERE `menusetup_tbl`.`id` = 82;