
UPDATE `b_menusetup_tbl` SET `menu_title` = 'My Orders', `korean_name` = 'My Orders' WHERE `b_menusetup_tbl`.`id` = 150;
ALTER TABLE `b_notification_tbl` ADD `b_user_id` INT(11) NOT NULL AFTER `created_by`, ADD `notification_for` VARCHAR(255) NOT NULL DEFAULT 'CUSTOMER' AFTER `b_user_id`;
ALTER TABLE `company_profile` ADD `unit` VARCHAR(255) NOT NULL DEFAULT 'inches' AFTER `currency`;