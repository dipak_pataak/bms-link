ALTER TABLE `b_user_catalog_products` ADD `pattern_model_id` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL AFTER `product_id`;
ALTER TABLE `b_user_catalog_products` ADD `requested_by` INT(11) NOT NULL DEFAULT '0' AFTER `request_id`;
ALTER TABLE `b_user_catalog_products` ADD `pattern_link` INT(11) NOT NULL DEFAULT '0' AFTER `product_link`;
UPDATE `b_menusetup_tbl` SET `menu_title` = 'Receive orders', `korean_name` = 'Receive orders' WHERE `b_menusetup_tbl`.`id` = 152;