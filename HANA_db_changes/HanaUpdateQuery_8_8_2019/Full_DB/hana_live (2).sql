-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 08, 2019 at 07:08 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesslog`
--

CREATE TABLE `accesslog` (
  `sl_no` bigint(20) NOT NULL,
  `action_page` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_done` text COLLATE utf8_unicode_ci,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `ip_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accesslog`
--

INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(1, 'user-save', 'insert', 'User information save', '2', 0, NULL, '2018-12-17 00:00:00'),
(2, 'user-update', 'update', 'User information Update', '2', 0, NULL, '2018-12-17 00:00:00'),
(3, 'user-update', 'update', 'User information Update', '2', 0, NULL, '2018-12-17 10:26:30'),
(4, 'customer-save', 'insert', 'customer-save', '2', 0, NULL, '2018-12-17 13:43:47'),
(5, 'customer-save', 'insert', 'customer-save', '2', 0, NULL, '2018-12-17 14:46:03'),
(6, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-17 14:56:24'),
(7, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-17 14:57:09'),
(8, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-18 05:03:37'),
(9, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-18 05:09:01'),
(10, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-18 05:10:40'),
(11, 'customer-update', 'update', 'customer-update', '2', 0, NULL, '2018-12-18 05:11:46'),
(12, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2018-12-18 08:01:17'),
(13, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2018-12-18 08:16:40'),
(14, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2018-12-18 08:19:27'),
(15, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2018-12-19 06:13:39'),
(16, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2018-12-19 06:15:42'),
(17, 'customer-save', 'insert', 'Customer information save', '4', 0, NULL, '2018-12-19 06:36:04'),
(18, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2018-12-19 06:44:25'),
(19, 'user-save', 'insert', 'User information save', '8', 0, NULL, '2018-12-19 06:56:50'),
(20, 'customer-save', 'insert', 'Customer information save', '8', 0, NULL, '2018-12-19 06:58:18'),
(21, 'appointment-setup', 'insert', 'Appointment information save', '8', 0, NULL, '2018-12-19 06:58:57'),
(22, 'user-update', 'update', 'User information Update', '8', 0, NULL, '2018-12-19 07:30:58'),
(23, 'user-save', 'insert', 'User information save', '8', 0, NULL, '2018-12-19 07:36:06'),
(24, 'user-update', 'update', 'User information Update', '8', 0, NULL, '2018-12-19 07:36:30'),
(25, 'b_level', 'insert', 'User information save', '1', 0, NULL, '2019-01-02 05:26:56'),
(26, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 06:29:51'),
(27, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 06:30:02'),
(28, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 07:04:39'),
(29, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 07:11:03'),
(30, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 07:21:42'),
(31, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 07:21:54'),
(32, 'b_level', 'update', 'User information update', '1', 0, NULL, '2019-01-02 07:35:50'),
(33, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-09 06:19:39'),
(34, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-09 06:29:19'),
(35, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-09 06:31:21'),
(36, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-09 06:44:18'),
(37, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-09 06:45:45'),
(38, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-01-09 11:48:28'),
(39, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:05:48'),
(40, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:07:10'),
(41, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:08:28'),
(42, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:34:33'),
(43, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:35:22'),
(44, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 07:35:38'),
(45, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-10 12:10:11'),
(46, 'customer_update', 'update', 'b level Customer information update', NULL, 0, NULL, '2019-01-10 13:31:43'),
(47, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 13:39:02'),
(48, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-10 14:17:07'),
(49, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-12 07:25:20'),
(50, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-01-13 12:15:56'),
(51, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-01-13 12:20:41'),
(52, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-01-13 13:28:40'),
(53, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-01-13 14:04:20'),
(54, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:05:47'),
(55, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:08:40'),
(56, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:09:16'),
(57, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:09:38'),
(58, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:19:07'),
(59, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-01-14 07:19:22'),
(60, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-01-14 07:24:19'),
(61, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-15 06:35:42'),
(62, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-01-15 07:28:05'),
(63, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 08:06:19'),
(64, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 08:20:24'),
(65, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 08:21:23'),
(66, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 08:24:40'),
(67, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 08:25:31'),
(68, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 09:22:40'),
(69, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 09:25:03'),
(70, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 09:25:32'),
(71, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 09:26:36'),
(72, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-01-15 09:31:24'),
(73, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-01-16 11:04:26'),
(74, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-01-16 11:08:18'),
(75, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 08:10:53'),
(76, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 08:12:53'),
(77, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 08:16:42'),
(78, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 09:47:31'),
(79, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 09:54:31'),
(80, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 09:56:59'),
(81, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:07:05'),
(82, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:08:17'),
(83, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:09:08'),
(84, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:17:14'),
(85, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:17:32'),
(86, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:18:38'),
(87, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:19:54'),
(88, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:23:14'),
(89, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:25:01'),
(90, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:25:11'),
(91, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:25:52'),
(92, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:26:51'),
(93, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:50:14'),
(94, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:52:33'),
(95, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:54:46'),
(96, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:58:53'),
(97, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 10:59:46'),
(98, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-11 11:02:35'),
(99, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-12 12:08:02'),
(100, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-02-25 13:23:50'),
(101, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:04:57'),
(102, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:06:43'),
(103, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:11:49'),
(104, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:12:17'),
(105, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:12:27'),
(106, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:13:30'),
(107, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:15:00'),
(108, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:16:47'),
(109, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:17:10'),
(110, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:18:14'),
(111, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:18:21'),
(112, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:19:16'),
(113, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-26 10:19:50'),
(114, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 12:06:10'),
(115, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 13:29:03'),
(116, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 13:44:09'),
(117, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 13:55:27'),
(118, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 13:55:39'),
(119, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 13:55:53'),
(120, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 14:00:16'),
(121, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 14:04:21'),
(122, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 14:04:48'),
(123, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-02-26 14:08:28'),
(124, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-02-28 10:07:50'),
(125, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-03-03 07:33:12'),
(126, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-03 09:48:44'),
(127, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-03 11:20:54'),
(128, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-03-03 11:22:29'),
(129, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-04 11:11:21'),
(130, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-03-04 12:26:02'),
(131, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-03-04 14:00:59'),
(132, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-06 12:06:49'),
(133, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-06 12:07:29'),
(134, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-06 12:09:49'),
(135, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-06 12:10:36'),
(136, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-07 04:33:23'),
(137, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-03-07 17:09:30'),
(138, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-08 02:46:34'),
(139, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-03-08 02:54:09'),
(140, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-03-08 02:55:03'),
(141, 'appointment_setup', 'insert', 'b level Appointment information save', '1', 0, NULL, '2019-03-08 02:55:45'),
(142, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 01:18:56'),
(143, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 01:21:10'),
(144, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 01:22:21'),
(145, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 01:23:22'),
(146, 'customer-save', 'insert', 'Customer information save', '16', 0, NULL, '2019-03-09 01:34:47'),
(147, 'appointment-setup', 'insert', 'Appointment information save', '16', 0, NULL, '2019-03-09 01:37:37'),
(148, 'customer-save', 'insert', 'Customer information save', '17', 0, NULL, '2019-03-09 01:40:53'),
(149, 'appointment-setup', 'insert', 'Appointment information save', '17', 0, NULL, '2019-03-09 01:42:23'),
(150, 'customer-save', 'insert', 'Customer information save', '18', 0, NULL, '2019-03-09 01:44:54'),
(151, 'appointment-setup', 'insert', 'Appointment information save', '18', 0, NULL, '2019-03-09 01:45:52'),
(152, 'customer-save', 'insert', 'Customer information save', '19', 0, NULL, '2019-03-09 01:47:58'),
(153, 'appointment-setup', 'insert', 'Appointment information save', '19', 0, NULL, '2019-03-09 01:49:04'),
(154, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 09:30:04'),
(155, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 09:31:55'),
(156, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 09:44:47'),
(157, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-09 09:53:32'),
(158, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-11 14:10:32'),
(159, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-11 15:15:05'),
(160, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-11 17:07:23'),
(161, 'b_level', 'insert', 'User information save', '1', 0, NULL, '2019-03-11 17:18:49'),
(162, 'b_level', 'insert', 'User information save', '1', 0, NULL, '2019-03-12 01:02:22'),
(163, 'b_level', 'insert', 'User information save', '1', 0, NULL, '2019-03-12 01:18:28'),
(164, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-12 17:43:22'),
(165, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-12 17:43:31'),
(166, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-12 17:47:34'),
(167, 'customer-save', 'insert', 'Customer information save', '31', 0, NULL, '2019-03-12 18:22:00'),
(168, 'appointment-setup', 'insert', 'Appointment information save', '31', 0, NULL, '2019-03-12 18:26:45'),
(169, 'user-save', 'insert', 'User information save', '2', 0, NULL, '2019-03-13 01:55:10'),
(170, 'user-save', 'insert', 'User information save', '2', 0, NULL, '2019-03-13 01:56:04'),
(171, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-13 05:44:31'),
(172, 'b_level', 'insert', 'User information save', '1', 0, NULL, '2019-03-15 00:25:15'),
(173, 'user-save', 'insert', 'User information save', '2', 0, NULL, '2019-03-15 00:32:11'),
(174, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-15 13:04:08'),
(175, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-15 13:07:19'),
(176, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-15 13:08:32'),
(177, 'customer-update', 'update', 'Customer information update', '2', 0, NULL, '2019-03-16 12:01:56'),
(178, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-17 15:10:05'),
(179, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-18 07:32:10'),
(180, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-19 01:01:26'),
(181, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-19 01:05:20'),
(182, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-19 01:05:23'),
(183, 'customer-save', 'insert', 'Customer information save', '40', 0, NULL, '2019-03-19 01:43:38'),
(184, 'customer-save', 'insert', 'Customer information save', '1', 0, NULL, '2019-03-19 01:46:04'),
(185, 'customer-save', 'insert', 'Customer information save', '40', 0, NULL, '2019-03-19 01:46:41'),
(186, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-19 01:49:55'),
(187, 'customer-save', 'insert', 'Customer information save', '1', 0, NULL, '2019-03-19 05:57:52'),
(188, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-20 05:46:13'),
(189, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-03-21 01:20:26'),
(190, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-03-21 06:46:14'),
(191, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-21 06:48:19'),
(192, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-21 06:49:53'),
(193, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-03-21 06:51:51'),
(194, 'supplier_update', 'update', 'b level Supplier information update', '1', 0, NULL, '2019-03-25 05:30:47'),
(195, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-03-26 01:07:31'),
(196, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-03-30 04:59:25'),
(197, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-03 06:43:21'),
(198, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-08 08:40:24'),
(199, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-10 04:41:40'),
(200, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-04-10 05:06:52'),
(201, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-10 05:08:25'),
(202, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-13 04:18:34'),
(203, 'customer_update', 'update', 'b level Customer information update', '1', 0, NULL, '2019-04-13 04:19:22'),
(204, 'appointment-setup', 'insert', 'Appointment information save', '2', 0, NULL, '2019-04-13 10:19:40'),
(205, 'supplier_save', 'insert', 'b level Supplier information save', '1', 0, NULL, '2019-04-18 00:31:34'),
(206, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-05-01 15:52:06'),
(207, 'customer-save', 'insert', 'Customer information save', '2', 0, NULL, '2019-05-01 17:30:31'),
(208, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-05-01 17:38:26'),
(209, 'customer-save', 'insert', 'Customer information save', '47', 0, NULL, '2019-05-01 18:23:16'),
(210, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-05-02 05:53:52'),
(211, 'customer-save', 'insert', 'Customer information save', '48', 0, NULL, '2019-05-02 05:57:57'),
(212, 'customer-save', 'insert', 'Customer information save', '48', 0, NULL, '2019-05-02 06:53:10'),
(213, 'customer-save', 'insert', 'Customer information save', '47', 0, NULL, '2019-05-02 18:25:48'),
(214, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-05-04 02:14:28'),
(215, 'customer-save', 'insert', 'Customer information save', '49', 0, NULL, '2019-05-04 02:28:38'),
(216, 'customer_save', 'insert', 'b level Customer information save', '1', 0, NULL, '2019-05-05 01:41:29'),
(217, 'customer-save', 'insert', 'Customer information save', '50', 0, NULL, '2019-05-05 01:52:15'),
(218, 'user-save', 'insert', 'User information save', '47', 0, NULL, '2019-05-14 12:25:31'),
(219, 'appointment-setup', 'insert', 'Appointment information save', '47', 0, NULL, '2019-05-14 14:13:43'),
(220, 'appointment-setup', 'insert', 'Appointment information save', '47', 0, NULL, '2019-05-14 14:22:18'),
(221, 'user-save', 'insert', 'User information save', '47', 0, NULL, '2019-05-15 04:43:34'),
(222, 'customer-save', 'insert', 'Customer information save', '47', 0, NULL, '2019-05-16 13:51:55'),
(223, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.193', '2019-05-20 16:17:52'),
(224, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 0, '162.158.118.193', '2019-05-20 16:18:35'),
(225, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 0, '162.158.118.193', '2019-05-20 16:18:47'),
(226, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 0, '162.158.118.193', '2019-05-20 16:18:56'),
(227, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 0, '162.158.118.151', '2019-05-20 16:40:22'),
(228, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 0, '162.158.118.151', '2019-05-20 16:40:34'),
(229, 'b_level', 'updated', 'attribute information updated', '1', 0, '162.158.118.35', '2019-05-20 16:40:59'),
(230, 'c-update-payment-gateway', 'insert', 'payment setting information save', '2', 0, '162.158.155.189', '2019-05-21 12:22:11'),
(231, 'supplier_save', 'insert', 'supplier information save', '1', 0, '162.158.154.134', '2019-05-21 12:29:18'),
(232, 'customer_save', 'insert', 'b level Customer information save', '1', 0, '162.158.154.68', '2019-05-21 13:28:57'),
(233, 'company-profile-update', 'updated', 'company profile information updated', '53', 0, '162.158.155.189', '2019-05-21 13:30:59'),
(234, 'my-account-update', 'insert', 'my account information updated', '53', 0, '141.101.107.229', '2019-05-21 13:32:56'),
(235, 'my-account-update', 'insert', 'my account information updated', '53', 0, '141.101.107.115', '2019-05-21 13:33:25'),
(236, 'my-account-update', 'insert', 'my account information updated', '53', 0, '141.101.107.115', '2019-05-21 13:33:40'),
(237, 'c-save-gateway', 'insert', 'payment setting information save', '53', 0, '141.101.107.229', '2019-05-21 13:37:11'),
(238, 'c-update-payment-gateway', 'insert', 'payment setting information save', '53', 0, '141.101.107.229', '2019-05-21 13:37:34'),
(239, 'c-cost-factor-save', 'insert', 'cost factor information save', '2', 0, '141.101.99.132', '2019-05-21 13:42:27'),
(240, 'user-save', 'insert', 'Color information save', '53', 0, '162.158.154.182', '2019-05-21 13:43:22'),
(241, 'my-account-update', 'insert', 'my account information updated', '54', 0, '162.158.154.182', '2019-05-21 13:44:28'),
(242, 'role-save', 'insert', 'role permission information save', '53', 0, '162.158.154.182', '2019-05-21 13:47:33'),
(243, 'assign-user-role-save', 'insert', 'assign user role information save', '53', 0, '162.158.154.182', '2019-05-21 13:48:52'),
(244, 'my-account-update', 'insert', 'my account information updated', '54', 0, '141.101.107.241', '2019-05-21 13:51:38'),
(245, 'my-account-update', 'insert', 'my account information updated', '54', 0, '162.158.158.134', '2019-05-21 13:52:25'),
(246, 'my-account-update', 'insert', 'my account information updated', '54', 0, '162.158.154.68', '2019-05-21 13:52:47'),
(247, 'c-us-state-update', 'updated', 'us state information update', '53', 0, '162.158.154.68', '2019-05-21 13:57:20'),
(248, 'customer-save', 'insert', 'Customer information save', '53', 0, '141.101.107.241', '2019-05-21 14:37:39'),
(249, 'customer-update', 'update', 'Customer information update', '53', 0, '162.158.155.237', '2019-05-21 14:39:55'),
(250, 'appointment-setup', 'insert', 'appointment information save', '53', 0, '162.158.155.237', '2019-05-21 14:42:15'),
(251, 'save_attribute', 'insert', 'attribute information save', '1', 0, '141.101.107.229', '2019-05-21 15:03:24'),
(252, 'save_attribute', 'insert', 'attribute information save', '1', 0, '141.101.107.229', '2019-05-21 15:05:01'),
(253, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '141.101.107.229', '2019-05-21 15:06:24'),
(254, 'save_price_model_mapping', 'insert', 'price model mapping information save', '1', 0, '141.101.107.229', '2019-05-21 15:11:38'),
(255, 'delete_price_model_mapping', 'deleted', 'deleted price model mapping information', '1', 0, '141.101.107.229', '2019-05-21 15:11:45'),
(256, 'customer_save', 'insert', 'b level Customer information save', '1', 0, '162.158.118.89', '2019-05-22 13:33:03'),
(257, 'customer-save', 'insert', 'Customer information save', '55', 0, '162.158.118.153', '2019-05-22 13:46:59'),
(258, 'role-save', 'insert', 'role permission information save', '53', 0, '162.158.154.68', '2019-05-23 05:18:39'),
(259, 'assign-user-role-save', 'insert', 'assign user role information save', '53', 0, '162.158.155.237', '2019-05-23 05:19:01'),
(260, 'user-update', 'updated', 'user information updated', '2', 2, '141.101.99.84', '2019-05-25 09:01:45'),
(261, 'role-save', 'insert', 'role permission information save', '2', 2, '141.101.99.84', '2019-05-25 09:04:36'),
(262, 'assign-user-role-save', 'insert', 'assign user role information save', '2', 2, '141.101.99.84', '2019-05-25 09:04:54'),
(263, 'user-save', 'insert', 'Color information save', '53', 53, '141.101.99.156', '2019-05-26 06:45:58'),
(264, 'my-account-update', 'updated', 'my account information updated', '56', 53, '141.101.99.156', '2019-05-26 06:46:39'),
(265, 'my-account-update', 'updated', 'my account information updated', '56', 53, '162.158.155.33', '2019-05-26 06:46:48'),
(266, 'payment-gateway-update', 'updated', 'payment gateway information updated', '53', 53, '162.158.154.134', '2019-05-26 06:55:02'),
(267, 'payment-gateway-update', 'updated', 'payment gateway information updated', '53', 53, '162.158.154.134', '2019-05-26 07:00:03'),
(268, 'c-update-payment-gateway', 'insert', 'payment setting information save', '53', 53, '141.101.107.229', '2019-05-26 07:01:01'),
(269, 'c-cost-factor-save', 'insert', 'cost factor information save', '48', 48, '162.158.154.68', '2019-05-26 07:02:03'),
(270, 'c-cost-factor-save', 'insert', 'cost factor information save', '48', 48, '162.158.154.68', '2019-05-26 07:02:39'),
(271, 'c-us-state-save', 'insert', 'us state information save', '53', 53, '141.101.107.229', '2019-05-26 07:15:21'),
(272, 'c-us-state-update', 'updated', 'us state information update', '53', 53, '162.158.158.134', '2019-05-26 07:15:34'),
(273, 'c_level', 'updated', 'my card information updated', '53', 53, '141.101.107.229', '2019-05-26 07:17:08'),
(274, 'c_level', 'insert', 'my card information inserted', '53', 53, '162.158.158.134', '2019-05-26 07:18:00'),
(275, 'c_level', 'deleted', 'my card information deleted', '53', 53, '141.101.107.229', '2019-05-26 07:18:25'),
(276, 'c_level', 'deleted', 'my card information deleted', '53', 53, '162.158.158.134', '2019-05-26 07:18:29'),
(277, 'c_level', 'insert', 'my card information inserted', '53', 53, '162.158.158.134', '2019-05-26 07:18:46'),
(278, 'appointment-setup', 'insert', 'appointment information save', '53', 0, '162.158.158.134', '2019-05-26 07:26:30'),
(279, 'customer-save', 'insert', 'Customer information save', '53', 53, '141.101.99.78', '2019-05-26 07:53:47'),
(280, 'product_update', 'updated', 'product information updated', '1', 0, '141.101.107.241', '2019-05-26 09:06:52'),
(281, 'save_order', 'inserted', 'order information inserted', '53', 53, '162.158.155.237', '2019-05-26 09:29:06'),
(282, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '53', 53, '162.158.155.237', '2019-05-26 09:29:07'),
(283, 'b_level', 'insert', 'user information save', '1', 1, '141.101.107.241', '2019-05-26 10:01:03'),
(284, 'my_account_update', 'insert', 'my account information save', '57', 1, '141.101.107.229', '2019-05-26 10:02:10'),
(285, 'b_level', 'update', 'assign user role information updated', '1', 1, '162.158.154.68', '2019-05-26 10:08:32'),
(286, 'my_account_update', 'insert', 'my account information save', '1', 1, '141.101.107.241', '2019-05-27 05:02:54'),
(287, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '141.101.99.156', '2019-05-27 05:14:51'),
(288, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '162.158.155.189', '2019-05-27 05:15:51'),
(289, 'role_update', 'updated', 'role permission updated', '1', 1, '141.101.99.156', '2019-05-27 05:16:27'),
(290, 'role_update', 'updated', 'role permission updated', '1', 1, '162.158.155.189', '2019-05-27 05:17:32'),
(291, 'send-customer-comment', 'insert', 'send customer comment', '57', 1, '162.158.155.189', '2019-05-27 05:18:28'),
(292, 'b_level', 'insert', 'Color information save', '1', 1, '162.158.154.182', '2019-05-27 06:32:46'),
(293, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '141.101.99.156', '2019-05-27 06:34:00'),
(294, 'product_save', 'insert', 'product information save', '1', 0, '141.101.99.84', '2019-05-27 06:54:45'),
(295, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '162.158.154.248', '2019-05-27 06:57:40'),
(296, 'save_price_model_mapping', 'insert', 'price model mapping information save', '1', 0, '141.101.99.84', '2019-05-27 07:02:09'),
(297, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '141.101.99.132', '2019-05-27 09:21:15'),
(298, 'save', 'insert', 'raw material information save', '1', 1, '141.101.99.132', '2019-05-27 09:27:46'),
(299, 'supplier-delete', 'deleted', 'supplier information deleted', '1', 1, '141.101.107.229', '2019-05-27 09:45:34'),
(300, 'customer-order-return-save', 'insert', 'customer order return information save', '1', 1, '141.101.99.156', '2019-05-27 09:49:34'),
(301, 'customer_save', 'insert', 'b level Customer information save', '1', 1, '162.158.154.248', '2019-05-27 11:48:10'),
(302, 'customer_update', 'update', 'b level Customer information update', '1', 1, '162.158.155.237', '2019-05-27 11:55:20'),
(303, 'customer_update', 'update', 'b level Customer information update', '1', 1, '162.158.155.237', '2019-05-27 11:55:54'),
(304, 'customer_update', 'update', 'b level Customer information update', '1', 1, '162.158.154.248', '2019-05-27 11:56:40'),
(305, 'my-account-update', 'updated', 'my account information updated', '47', 47, '162.158.118.193', '2019-05-27 14:37:30'),
(306, 'save_order', 'inserted', 'order information inserted', '47', 47, '162.158.118.57', '2019-05-27 15:05:59'),
(307, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.118.153', '2019-05-27 15:07:12'),
(308, 'customer_save', 'insert', 'b level Customer information save', '1', 1, '162.158.118.57', '2019-05-27 15:11:28'),
(309, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.89', '2019-05-27 15:53:02'),
(310, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.118.109', '2019-05-27 15:56:35'),
(311, 'company-profile-update', 'updated', 'company profile information updated', '59', 59, '162.158.118.237', '2019-05-27 16:04:12'),
(312, 'my-account-update', 'updated', 'my account information updated', '59', 59, '162.158.118.237', '2019-05-27 16:04:26'),
(313, 'customer-save', 'insert', 'Customer information save', '59', 59, '162.158.118.237', '2019-05-27 16:06:26'),
(314, 'c-us-state-save', 'insert', 'us state information save', '59', 59, '162.158.118.57', '2019-05-27 16:07:14'),
(315, 'c-cost-factor-save', 'insert', 'cost factor information save', '59', 59, '162.158.118.35', '2019-05-27 16:08:39'),
(316, 'save_order', 'inserted', 'order information inserted', '59', 59, '162.158.118.35', '2019-05-27 16:11:36'),
(317, 'invoice_receipt', 'updated', 'order payment information updated', '59', 59, '162.158.118.237', '2019-05-27 16:37:48'),
(318, 'invoice_receipt', 'updated', 'order payment information updated', '59', 59, '162.158.118.15', '2019-05-27 16:37:49'),
(319, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '59', 59, '162.158.118.63', '2019-05-27 16:52:35'),
(320, 'make_payment', 'updated', 'order payment information updated', '59', 59, '162.158.118.71', '2019-05-27 17:00:29'),
(321, 'make_payment', 'updated', 'order payment information updated', '59', 59, '162.158.118.71', '2019-05-27 17:01:54'),
(322, 'invoice_receipt', 'updated', 'order payment information updated', '59', 59, '162.158.118.63', '2019-05-27 17:05:48'),
(323, 'payment-gateway-update', 'updated', 'payment gateway information updated', '59', 59, '162.158.118.19', '2019-05-27 17:45:52'),
(324, 'save_order', 'inserted', 'order information inserted', '59', 59, '162.158.118.165', '2019-05-27 17:48:59'),
(325, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '59', 59, '162.158.118.35', '2019-05-27 17:55:22'),
(326, 'c_level', 'insert', 'my card information inserted', '59', 59, '162.158.118.15', '2019-05-27 17:56:01'),
(327, 'save_order', 'inserted', 'order information inserted', '59', 59, '162.158.118.87', '2019-05-28 00:47:40'),
(328, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '59', 59, '162.158.118.179', '2019-05-28 00:50:50'),
(329, 'c_level', 'insert', 'my card information inserted', '59', 59, '162.158.118.57', '2019-05-28 00:55:30'),
(330, 'customer_save', 'insert', 'b level Customer information save', '1', 1, '162.158.155.189', '2019-05-28 07:23:01'),
(331, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.155.33', '2019-05-28 07:52:21'),
(332, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.155.33', '2019-05-28 07:52:27'),
(333, 'customer_save', 'insert', 'b level Customer information save', '1', 1, '162.158.154.248', '2019-05-28 08:02:27'),
(334, 'company-profile-update', 'updated', 'company profile information updated', '61', 61, '141.101.99.132', '2019-05-28 08:09:21'),
(335, 'my-account-update', 'updated', 'my account information updated', '61', 61, '141.101.99.132', '2019-05-28 08:09:49'),
(336, 'save_order', 'inserted', 'order information inserted', '59', 59, '162.158.118.87', '2019-05-28 10:29:18'),
(337, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '59', 59, '162.158.118.87', '2019-05-28 10:29:19'),
(338, 'invoice_receipt', 'updated', 'order payment information updated', '59', 59, '162.158.118.35', '2019-05-28 10:29:37'),
(339, 'make_payment', 'updated', 'order payment information updated', '59', 59, '162.158.118.243', '2019-05-28 10:30:02'),
(340, 'product_save', 'insert', 'product information save', '1', 0, '162.158.155.237', '2019-05-29 06:09:38'),
(341, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '162.158.155.237', '2019-05-29 06:10:08'),
(342, 'customer_save', 'insert', 'Customer information save', '1', 1, '172.68.47.67', '2019-05-29 06:25:42'),
(343, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.154.134', '2019-05-29 10:11:04'),
(344, 'customer-save', 'insert', 'Customer information save', '63', 63, '162.158.154.134', '2019-05-29 10:13:22'),
(345, 'save_order', 'inserted', 'order information inserted', '63', 63, '141.101.99.84', '2019-05-29 10:14:03'),
(346, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '141.101.107.241', '2019-05-29 10:24:07'),
(347, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.58.82', '2019-05-29 15:15:02'),
(348, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.58.82', '2019-05-29 15:15:12'),
(349, 'product_update', 'updated', 'product information updated', '1', 0, '173.245.48.88', '2019-05-29 17:50:26'),
(350, 'b_level', 'insert', 'user information save', '1', 1, '162.158.58.82', '2019-05-29 18:01:48'),
(351, 'b-update-password', 'updated', 'password change successfully', '1', 1, '162.158.58.82', '2019-05-29 18:04:27'),
(352, 'customer_save', 'insert', 'Customer information save', '1', 1, '172.69.33.114', '2019-05-29 18:06:29'),
(353, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.59.11', '2019-05-29 18:08:30'),
(354, 'b_level', 'insert', 'Color information save', '1', 1, '162.158.59.11', '2019-05-29 18:58:33'),
(355, 'save_order', 'inserted', 'order information inserted', '59', 59, '162.158.58.132', '2019-05-30 05:31:12'),
(356, 'invoice_receipt', 'updated', 'order payment information updated', '59', 59, '162.158.58.188', '2019-05-30 05:31:54'),
(357, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '59', 59, '162.158.58.188', '2019-05-30 05:32:10'),
(358, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.154.182', '2019-05-30 06:21:14'),
(359, 'customer-save', 'insert', 'Customer information save', '61', 61, '162.158.154.134', '2019-05-30 07:00:25'),
(360, 'save_order', 'inserted', 'order information inserted', '61', 61, '162.158.158.134', '2019-05-30 07:00:58'),
(361, 'save_order', 'inserted', 'order information inserted', '61', 61, '108.162.215.77', '2019-05-30 07:17:14'),
(362, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:01'),
(363, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:07'),
(364, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:17'),
(365, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:23'),
(366, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:28'),
(367, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:33'),
(368, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '173.245.48.208', '2019-05-30 15:41:38'),
(369, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:43'),
(370, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:47'),
(371, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:52'),
(372, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:41:57'),
(373, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:02'),
(374, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:08'),
(375, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:29'),
(376, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:37'),
(377, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:42'),
(378, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.58.154', '2019-05-30 15:42:49'),
(379, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.58.82', '2019-05-30 15:54:03'),
(380, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:16'),
(381, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.70.102', '2019-05-30 15:54:22'),
(382, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:23'),
(383, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.70.102', '2019-05-30 15:54:27'),
(384, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:34'),
(385, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.70.102', '2019-05-30 15:54:39'),
(386, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:42'),
(387, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.70.102', '2019-05-30 15:54:45'),
(388, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:47'),
(389, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:49'),
(390, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.70.102', '2019-05-30 15:54:50'),
(391, 'delete-row-material', 'deleted', 'raw material information deleted', '1', 1, '172.69.68.150', '2019-05-30 15:54:51'),
(392, 'supplier-delete', 'deleted', 'supplier information deleted', '1', 1, '162.158.58.82', '2019-05-30 15:57:35'),
(393, 'supplier-delete', 'deleted', 'supplier information deleted', '1', 1, '162.158.58.82', '2019-05-30 15:57:48'),
(394, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.58.82', '2019-05-30 16:02:50'),
(395, 'customer-save', 'insert', 'Customer information save', '66', 66, '172.69.33.90', '2019-05-31 00:38:38'),
(396, 'payment-gateway-update', 'updated', 'payment gateway information updated', '66', 66, '162.158.59.29', '2019-05-31 00:41:49'),
(397, 'company-profile-update', 'updated', 'company profile information updated', '66', 66, '162.158.59.29', '2019-05-31 00:43:02'),
(398, 'c-cost-factor-save', 'insert', 'cost factor information save', '66', 66, '162.158.59.29', '2019-05-31 00:46:03'),
(399, 'c-us-state-save', 'insert', 'us state information save', '66', 66, '162.158.59.59', '2019-05-31 00:58:27'),
(400, 'c_level', 'insert', 'my card information inserted', '66', 66, '162.158.59.29', '2019-05-31 01:01:20'),
(401, 'my-account-update', 'updated', 'my account information updated', '66', 66, '162.158.59.29', '2019-05-31 01:01:42'),
(402, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.58.188', '2019-05-31 01:13:54'),
(403, 'save_order', 'inserted', 'order information inserted', '66', 66, '108.162.215.77', '2019-05-31 01:18:03'),
(404, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '66', 66, '108.162.215.77', '2019-05-31 01:18:04'),
(405, 'invoice_receipt', 'synchronize', 'synchronize to b level order', '61', 61, '173.245.48.208', '2019-05-31 01:36:15'),
(406, 'my-account-update', 'updated', 'my account information updated', '61', 61, '173.245.48.208', '2019-05-31 01:42:25'),
(407, 'company-profile-update', 'updated', 'company profile information updated', '61', 61, '162.158.58.132', '2019-05-31 01:46:01'),
(408, 'invoice_receipt', 'updated', 'order payment information updated', '61', 61, '172.69.33.24', '2019-05-31 02:53:39'),
(409, 'make_payment', 'updated', 'order payment information updated', '61', 61, '172.69.33.24', '2019-05-31 03:05:07'),
(410, 'save_quotation', 'inserted', 'quatation information saved', '61', 61, '162.158.58.238', '2019-05-31 03:19:45'),
(411, 'my-account-update', 'updated', 'my account information updated', '61', 61, '162.158.58.188', '2019-06-01 04:14:31'),
(412, 'c-us-state-save', 'insert', 'us state information save', '61', 61, '162.158.155.189', '2019-06-01 07:05:45'),
(413, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '162.158.154.182', '2019-06-02 04:20:28'),
(414, 'customer_update', 'update', 'Customer information update', '1', 1, '162.158.155.237', '2019-06-02 04:22:41'),
(415, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.155.237', '2019-06-02 04:25:08'),
(416, 'customer_update', 'update', 'Customer information update', '1', 1, '141.101.107.229', '2019-06-02 04:42:59'),
(417, 'delete_order', 'deleted', 'order information deleted', '61', 61, '162.158.58.132', '2019-06-02 04:55:57'),
(418, 'delete_order', 'deleted', 'order information deleted', '61', 61, '162.158.154.182', '2019-06-02 05:05:27'),
(419, 'delete_order', 'deleted', 'order information deleted', '61', 61, '162.158.154.182', '2019-06-02 05:05:32'),
(420, 'c-update-password', 'update', 'user password updated', '66', 66, '141.101.107.241', '2019-06-02 05:11:03'),
(421, 'customer-save', 'insert', 'Customer information save', '67', 67, '162.158.155.33', '2019-06-02 05:21:30'),
(422, 'save_order', 'inserted', 'order information inserted', '67', 67, '162.158.154.248', '2019-06-02 05:22:40'),
(423, 'company-profile-update', 'updated', 'company profile information updated', '67', 67, '162.158.155.189', '2019-06-02 05:22:57'),
(424, 'save_order', 'inserted', 'order information inserted', '61', 61, '162.158.155.189', '2019-06-02 11:04:31'),
(425, 'save_quotation', 'inserted', 'quatation information saved', '2', 2, '141.101.99.78', '2019-06-04 03:46:48'),
(426, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.118.19', '2019-06-06 17:48:56'),
(427, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.118.19', '2019-06-06 17:49:04'),
(428, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.118.111', '2019-06-06 17:49:11'),
(429, 'c-save-gateway', 'insert', 'payment setting information save', '66', 66, '162.158.118.131', '2019-06-08 12:53:11'),
(430, 'b-cost-factor-save', 'insert', 'cost factor information save', '1', 1, '162.158.118.75', '2019-06-08 13:52:00'),
(431, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.118.111', '2019-06-08 14:08:06'),
(432, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.119.28', '2019-06-08 14:12:40'),
(433, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.118.111', '2019-06-08 14:12:58'),
(434, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.118.57', '2019-06-08 15:43:26'),
(435, 'c-customer-order-return-save', 'insert', 'customer order return information save', '66', 66, '162.158.118.111', '2019-06-09 10:05:07'),
(436, 'return_resend_comment', 'insert', 'return resend comment information save', '66', 66, '162.158.118.15', '2019-06-09 10:10:42'),
(437, 'b_level', 'updated', 'Color information updated', '1', 1, '162.158.118.237', '2019-06-09 14:55:26'),
(438, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.237', '2019-06-09 14:58:06'),
(439, 'uom_delete', 'deleted', 'unit of measurement deleted', '1', 1, '162.158.118.57', '2019-06-10 06:05:08'),
(440, 'b_level', 'update', 'assign user role information updated', '1', 1, '162.158.118.57', '2019-06-10 06:07:22'),
(441, 'b_level', 'update', 'assign user role information updated', '1', 1, '162.158.118.57', '2019-06-10 06:07:28'),
(442, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '162.158.118.57', '2019-06-10 06:07:55'),
(443, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '141.101.105.217', '2019-06-10 07:01:09'),
(444, 'return_resend_comment', 'insert', 'return resend comment information save', '1', 1, '162.158.118.19', '2019-06-10 08:58:59'),
(445, 'return_resend_comment', 'insert', 'return resend comment information save', '1', 1, '162.158.118.19', '2019-06-10 09:01:01'),
(446, 'update_shipping_method', 'Updated', 'Updated shipping method', '1', 1, '141.101.107.229', '2019-06-10 09:09:54'),
(447, 'update_shipping_method', 'Updated', 'Updated shipping method', '1', 1, '141.101.99.84', '2019-06-10 09:10:07'),
(448, 'c-customer-order-return-save', 'insert', 'customer order return information save', '67', 67, '141.101.107.241', '2019-06-10 12:40:11'),
(449, 'save_category', 'insert', 'Category information save', '1', 1, '162.158.167.199', '2019-06-12 07:11:40'),
(450, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.20', '2019-06-12 07:12:00');
INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(451, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '162.158.167.199', '2019-06-12 07:12:12'),
(452, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '141.101.107.229', '2019-06-13 10:46:12'),
(453, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.118.111', '2019-06-17 04:56:15'),
(454, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.118.111', '2019-06-17 04:56:21'),
(455, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '162.158.118.237', '2019-06-17 05:22:54'),
(456, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.118.237', '2019-06-17 05:52:24'),
(457, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.118.27', '2019-06-17 05:53:35'),
(458, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '162.158.158.134', '2019-06-17 06:09:39'),
(459, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '162.158.118.109', '2019-06-17 06:36:52'),
(460, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.118.165', '2019-06-17 06:37:02'),
(461, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '162.158.118.111', '2019-06-17 06:44:22'),
(462, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:18:38'),
(463, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:19:12'),
(464, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.15', '2019-06-17 08:19:44'),
(465, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.15', '2019-06-17 08:20:08'),
(466, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:20:27'),
(467, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:21:00'),
(468, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:21:26'),
(469, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.15', '2019-06-17 08:23:28'),
(470, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:23:47'),
(471, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 08:24:04'),
(472, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.15', '2019-06-17 08:26:37'),
(473, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.29', '2019-06-17 08:30:35'),
(474, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.237', '2019-06-17 08:51:24'),
(475, 'customer_save', 'insert', 'Customer information save', '1', 1, '141.101.107.229', '2019-06-17 08:53:39'),
(476, 'customer_update', 'update', 'Customer information update', '1', 1, '141.101.107.115', '2019-06-17 08:53:57'),
(477, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.173', '2019-06-17 08:55:23'),
(478, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.237', '2019-06-17 08:55:23'),
(479, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.107.241', '2019-06-17 08:55:38'),
(480, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 08:56:04'),
(481, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 08:56:28'),
(482, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 08:56:46'),
(483, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 08:59:56'),
(484, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.69.134.196', '2019-06-17 08:59:57'),
(485, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:02:03'),
(486, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:02:19'),
(487, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 09:02:41'),
(488, 'save_attribute', 'insert', 'attribute information save', '1', 1, '141.101.107.115', '2019-06-17 09:03:42'),
(489, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '141.101.107.229', '2019-06-17 09:04:35'),
(490, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.158.134', '2019-06-17 09:05:51'),
(491, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.158.134', '2019-06-17 09:06:18'),
(492, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:07:05'),
(493, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:07:28'),
(494, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:07:44'),
(495, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:08:10'),
(496, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:08:27'),
(497, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:08:55'),
(498, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:09:17'),
(499, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:09:37'),
(500, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:10:00'),
(501, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:10:19'),
(502, 'save_attribute', 'insert', 'attribute information save', '1', 1, '141.101.107.229', '2019-06-17 09:13:45'),
(503, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:13:50'),
(504, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:14:07'),
(505, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '141.101.107.115', '2019-06-17 09:14:15'),
(506, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:14:21'),
(507, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:14:41'),
(508, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:14:59'),
(509, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:15:24'),
(510, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:16:13'),
(511, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:18:52'),
(512, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:19:07'),
(513, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:19:24'),
(514, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:19:40'),
(515, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:19:58'),
(516, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:20:25'),
(517, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:20:39'),
(518, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:20:56'),
(519, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 09:21:13'),
(520, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 09:21:29'),
(521, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:24:45'),
(522, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:25:08'),
(523, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:25:33'),
(524, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:25:54'),
(525, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:26:13'),
(526, 'save-price-style', 'insert', 'price model information save', '1', 1, '162.158.154.182', '2019-06-17 09:26:48'),
(527, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:28:43'),
(528, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 09:29:03'),
(529, 'product_save', 'insert', 'product information save', '1', 0, '141.101.107.229', '2019-06-17 09:33:06'),
(530, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.228', '2019-06-17 09:35:15'),
(531, 'product_save', 'insert', 'product information save', '1', 0, '141.101.99.84', '2019-06-17 09:38:35'),
(532, 'product_save', 'insert', 'product information save', '1', 0, '162.158.155.237', '2019-06-17 09:40:05'),
(533, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '141.101.99.84', '2019-06-17 09:40:38'),
(534, 'save', 'insert', 'raw material information save', '1', 1, '141.101.107.229', '2019-06-17 09:44:16'),
(535, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:43:48'),
(536, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:44:25'),
(537, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 10:44:48'),
(538, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:45:09'),
(539, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 10:45:38'),
(540, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:46:01'),
(541, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:46:25'),
(542, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 10:46:42'),
(543, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 10:47:11'),
(544, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.69.134.172', '2019-06-17 10:50:23'),
(545, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:50:23'),
(546, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.29', '2019-06-17 10:50:23'),
(547, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.128', '2019-06-17 10:50:23'),
(548, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 10:50:23'),
(549, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '172.68.144.146', '2019-06-17 10:50:35'),
(550, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '172.68.144.146', '2019-06-17 10:50:41'),
(551, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.165.95', '2019-06-17 10:50:48'),
(552, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.165.95', '2019-06-17 10:50:53'),
(553, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.95', '2019-06-17 10:51:22'),
(554, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 10:55:15'),
(555, 'supplier_save', 'insert', 'supplier information save', '1', 1, '141.101.107.115', '2019-06-17 10:55:24'),
(556, 'purchase-save', 'insert', 'purchase information save', '1', 1, '141.101.107.115', '2019-06-17 10:56:00'),
(557, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '141.101.107.115', '2019-06-17 10:56:00'),
(558, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '141.101.107.115', '2019-06-17 10:57:28'),
(559, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 11:00:41'),
(560, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:00:41'),
(561, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.249', '2019-06-17 11:00:42'),
(562, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 11:00:42'),
(563, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.254', '2019-06-17 11:00:42'),
(564, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 11:00:58'),
(565, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 11:01:14'),
(566, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 11:04:24'),
(567, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 11:04:24'),
(568, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 11:04:24'),
(569, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:06:19'),
(570, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:06:34'),
(571, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:06:48'),
(572, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:07:05'),
(573, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:07:28'),
(574, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:07:55'),
(575, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:08:07'),
(576, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:08:19'),
(577, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:08:31'),
(578, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:08:44'),
(579, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:08:56'),
(580, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:09:32'),
(581, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:09:48'),
(582, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:10:00'),
(583, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:10:23'),
(584, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:10:34'),
(585, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '162.158.154.68', '2019-06-17 11:10:46'),
(586, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:11:08'),
(587, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:11:20'),
(588, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:11:32'),
(589, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:11:46'),
(590, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:14:56'),
(591, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 11:14:56'),
(592, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 11:15:20'),
(593, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:15:30'),
(594, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:15:41'),
(595, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 11:15:52'),
(596, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 11:16:07'),
(597, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:16:21'),
(598, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 11:16:37'),
(599, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 11:17:06'),
(600, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:17:26'),
(601, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 11:20:34'),
(602, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:20:34'),
(603, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 11:20:34'),
(604, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.107.241', '2019-06-17 11:20:34'),
(605, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.107.229', '2019-06-17 11:20:34'),
(606, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.182', '2019-06-17 11:20:34'),
(607, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 11:20:34'),
(608, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.182', '2019-06-17 11:20:48'),
(609, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.182', '2019-06-17 11:20:55'),
(610, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.107.241', '2019-06-17 11:21:08'),
(611, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:21:29'),
(612, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:21:44'),
(613, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:21:56'),
(614, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:22:10'),
(615, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:22:23'),
(616, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:22:36'),
(617, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:22:50'),
(618, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:23:01'),
(619, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:23:13'),
(620, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:23:26'),
(621, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:23:39'),
(622, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:23:54'),
(623, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 11:24:08'),
(624, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:24:21'),
(625, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:24:47'),
(626, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 11:25:02'),
(627, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 11:28:07'),
(628, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 11:28:07'),
(629, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 11:28:27'),
(630, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 11:28:48'),
(631, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 11:29:01'),
(632, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 11:29:18'),
(633, 'company-profile-update', 'updated', 'company profile information updated', '68', 68, '162.158.154.248', '2019-06-17 12:02:05'),
(634, 'save_order', 'inserted', 'order information inserted', '68', 68, '162.158.158.16', '2019-06-17 12:08:29'),
(635, 'invoice_receipt', 'updated', 'order payment information updated', '68', 68, '141.101.107.241', '2019-06-17 12:08:52'),
(636, 'invoice_receipt', 'updated', 'order payment information updated', '68', 68, '141.101.107.115', '2019-06-17 12:09:07'),
(637, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 12:43:07'),
(638, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 12:44:47'),
(639, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 12:45:07'),
(640, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.173', '2019-06-17 12:45:19'),
(641, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 12:45:28'),
(642, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 12:45:43'),
(643, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '162.158.154.248', '2019-06-17 12:45:54'),
(644, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.173', '2019-06-17 12:45:55'),
(645, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 12:46:05'),
(646, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 12:46:17'),
(647, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 12:46:18'),
(648, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.183', '2019-06-17 12:46:25'),
(649, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 12:46:31'),
(650, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.183', '2019-06-17 12:46:39'),
(651, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.183', '2019-06-17 12:47:06'),
(652, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.183', '2019-06-17 12:47:36'),
(653, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.183', '2019-06-17 12:47:56'),
(654, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:48:12'),
(655, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:48:34'),
(656, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.173', '2019-06-17 12:49:14'),
(657, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:49:29'),
(658, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 12:49:38'),
(659, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.248', '2019-06-17 12:49:38'),
(660, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 12:49:51'),
(661, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:49:55'),
(662, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 12:50:14'),
(663, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 12:50:30'),
(664, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 12:50:42'),
(665, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:50:48'),
(666, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:51:04'),
(667, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:51:27'),
(668, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.165.173', '2019-06-17 12:52:50'),
(669, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 12:53:48'),
(670, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 12:53:48'),
(671, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.78', '2019-06-17 12:54:04'),
(672, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 12:54:20'),
(673, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 12:54:33'),
(674, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 12:54:46'),
(675, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.166.6', '2019-06-17 12:56:01'),
(676, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.146', '2019-06-17 12:56:01'),
(677, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.111', '2019-06-17 12:56:17'),
(678, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '172.68.144.146', '2019-06-17 12:56:22'),
(679, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.111', '2019-06-17 12:56:32'),
(680, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.237', '2019-06-17 12:56:46'),
(681, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 12:57:52'),
(682, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.179', '2019-06-17 12:59:39'),
(683, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.27', '2019-06-17 12:59:53'),
(684, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.237', '2019-06-17 12:59:53'),
(685, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.131', '2019-06-17 12:59:54'),
(686, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.179', '2019-06-17 13:00:09'),
(687, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.131', '2019-06-17 13:00:23'),
(688, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:02:29'),
(689, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:02:45'),
(690, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:02:58'),
(691, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:03:11'),
(692, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:03:23'),
(693, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.151', '2019-06-17 13:03:31'),
(694, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.131', '2019-06-17 13:03:31'),
(695, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.109', '2019-06-17 13:04:09'),
(696, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:04:22'),
(697, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.57', '2019-06-17 13:06:30'),
(698, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:06:45'),
(699, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:06:57'),
(700, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:07:11'),
(701, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:07:23'),
(702, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.109', '2019-06-17 13:07:28'),
(703, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:07:36'),
(704, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:07:47'),
(705, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:07:58'),
(706, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:08:16'),
(707, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:08:30'),
(708, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:08:43'),
(709, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:10:07'),
(710, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:10:24'),
(711, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:10:40'),
(712, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:10:53'),
(713, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:11:04'),
(714, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:11:16'),
(715, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:11:27'),
(716, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:11:49'),
(717, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 13:12:20'),
(718, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 13:12:50'),
(719, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:14:32'),
(720, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:14:32'),
(721, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:14:57'),
(722, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.84', '2019-06-17 13:15:11'),
(723, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:15:22'),
(724, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:15:35'),
(725, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:15:57'),
(726, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:16:08'),
(727, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:16:42'),
(728, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:16:53'),
(729, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.84', '2019-06-17 13:18:40'),
(730, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:19:13'),
(731, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:19:27'),
(732, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:19:38'),
(733, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:19:49'),
(734, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:19:59'),
(735, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:20:02'),
(736, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:20:14'),
(737, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:20:27'),
(738, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:20:39'),
(739, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:20:52'),
(740, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:21:19'),
(741, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:21:38'),
(742, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:21:58'),
(743, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:22:10'),
(744, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 13:22:23'),
(745, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:22:34'),
(746, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:22:51'),
(747, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:23:16'),
(748, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:23:53'),
(749, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:23:58'),
(750, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.68', '2019-06-17 13:24:05'),
(751, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:24:16'),
(752, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:24:31'),
(753, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 13:25:08'),
(754, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 13:25:44'),
(755, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 13:25:55'),
(756, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 13:27:37'),
(757, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.89', '2019-06-17 13:27:53'),
(758, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 13:29:01'),
(759, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:29:01'),
(760, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:29:25'),
(761, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:29:56'),
(762, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.111', '2019-06-17 13:31:02'),
(763, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.118.111', '2019-06-17 13:31:02'),
(764, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.182', '2019-06-17 13:31:25'),
(765, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:31:37'),
(766, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.241', '2019-06-17 13:33:01'),
(767, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 13:33:25'),
(768, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 13:33:40'),
(769, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:33:52'),
(770, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:34:08'),
(771, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:34:19'),
(772, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.115', '2019-06-17 13:34:31'),
(773, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:34:42'),
(774, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.33', '2019-06-17 13:37:35'),
(775, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.189', '2019-06-17 13:37:46'),
(776, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:38:27'),
(777, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 13:38:55'),
(778, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:39:27'),
(779, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 13:40:06'),
(780, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:40:20'),
(781, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.78', '2019-06-17 13:40:35'),
(782, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.84', '2019-06-17 13:40:52'),
(783, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:42:00'),
(784, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.107.229', '2019-06-17 13:42:12'),
(785, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.237', '2019-06-17 13:44:02'),
(786, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:44:14'),
(787, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.16', '2019-06-17 13:45:17'),
(788, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 13:45:30'),
(789, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.158.134', '2019-06-17 13:45:43'),
(790, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.158.134', '2019-06-17 13:46:53'),
(791, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.158.16', '2019-06-17 13:47:19'),
(792, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:47:22'),
(793, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '141.101.99.156', '2019-06-17 13:48:14'),
(794, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:53:37'),
(795, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:53:56'),
(796, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:54:21'),
(797, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:54:28'),
(798, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:55:10'),
(799, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:55:27'),
(800, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:56:05'),
(801, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:56:15'),
(802, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:56:22'),
(803, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:57:03'),
(804, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '141.101.99.156', '2019-06-17 13:57:47'),
(805, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:57:53'),
(806, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:58:30'),
(807, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 13:59:04'),
(808, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:01:09'),
(809, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:01:43'),
(810, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:01:50'),
(811, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.154.134', '2019-06-17 14:02:58'),
(812, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:03:19'),
(813, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:03:40'),
(814, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.154.134', '2019-06-17 14:04:16'),
(815, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.118.35', '2019-06-17 14:07:49'),
(816, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '162.158.118.151', '2019-06-17 14:08:37'),
(817, 'delete_price_style', 'deleted', 'price model information deleted', '1', 1, '172.68.144.146', '2019-06-18 03:40:41'),
(818, 'delete-product', 'deleted', 'product information deleted', '1', 0, '172.68.144.146', '2019-06-18 03:42:45'),
(819, 'delete-product', 'deleted', 'product information deleted', '1', 0, '172.68.144.146', '2019-06-18 03:42:51'),
(820, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.166.6', '2019-06-18 03:42:57'),
(821, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.166.6', '2019-06-18 03:43:44'),
(822, 'my-account-update', 'updated', 'my account information updated', '2', 2, '172.68.146.237', '2019-06-18 09:00:38'),
(823, 'my-account-update', 'updated', 'my account information updated', '2', 2, '172.69.134.112', '2019-06-18 09:01:37'),
(824, 'my-account-update', 'updated', 'my account information updated', '2', 2, '162.158.165.29', '2019-06-18 09:03:11'),
(825, 'my-account-update', 'updated', 'my account information updated', '2', 2, '162.158.166.96', '2019-06-18 09:08:07'),
(826, 'my-account-update', 'updated', 'my account information updated', '2', 2, '162.158.166.96', '2019-06-18 09:12:06'),
(827, 'product_save', 'insert', 'product information save', '1', 0, '162.158.167.247', '2019-06-18 12:58:11'),
(828, 'product_save', 'insert', 'product information save', '1', 0, '162.158.166.6', '2019-06-18 13:01:22'),
(829, 'product_save', 'insert', 'product information save', '1', 0, '162.158.166.6', '2019-06-18 13:01:55'),
(830, 'delete-product', 'deleted', 'product information deleted', '1', 0, '172.69.134.172', '2019-06-20 07:07:13'),
(831, 'delete-product', 'deleted', 'product information deleted', '1', 0, '172.69.134.172', '2019-06-20 07:07:18'),
(832, 'delete-product', 'deleted', 'product information deleted', '1', 0, '172.69.134.172', '2019-06-20 07:07:22'),
(833, 'update_price_style', 'updated', 'price model information updated', '1', 1, '162.158.166.228', '2019-06-20 10:45:30'),
(834, 'product_save', 'insert', 'product information save', '1', 0, '162.158.165.15', '2019-06-20 10:56:29'),
(835, 'product_save', 'insert', 'product information save', '1', 0, '172.69.134.172', '2019-06-20 11:02:42'),
(836, 'customer_update', 'update', 'Customer information update', '1', 1, '172.68.146.249', '2019-06-20 12:19:29'),
(837, 'b-cost-factor-save', 'insert', 'cost factor information save', '1', 1, '172.69.134.112', '2019-06-20 13:37:30'),
(838, 'raw-material-usage-save', 'deleted', 'raw material usages information save', '1', 1, '162.158.165.173', '2019-06-20 13:43:03'),
(839, 'company-profile-update', 'updated', 'company profile information updated', '66', 66, '162.158.165.131', '2019-06-20 14:17:33'),
(840, 'company-profile-update', 'updated', 'company profile information updated', '2', 2, '162.158.165.95', '2019-06-20 14:18:49'),
(841, 'product_update', 'updated', 'product information updated', '1', 0, '172.69.134.172', '2019-06-21 06:38:23'),
(842, 'product_update', 'updated', 'product information updated', '1', 0, '172.69.134.172', '2019-06-21 06:39:08'),
(843, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.167.199', '2019-06-21 12:02:26'),
(844, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.165.59', '2019-06-22 09:47:18'),
(845, 'save_attribute', 'insert', 'attribute information save', '1', 1, '162.158.166.96', '2019-06-24 07:33:13'),
(846, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.166.72', '2019-06-24 08:43:04'),
(847, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 1, '162.158.165.215', '2019-06-24 08:44:04'),
(848, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 1, '162.158.165.215', '2019-06-24 08:44:38'),
(849, 'attribute-delete', 'deleted', 'attribute information deleted', '1', 1, '162.158.165.215', '2019-06-24 08:44:43'),
(850, 'save_attribute', 'insert', 'attribute information save', '1', 1, '162.158.165.215', '2019-06-24 08:47:19'),
(851, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '162.158.165.215', '2019-06-24 08:48:03'),
(852, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.165.215', '2019-06-24 08:50:22'),
(853, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.167.73', '2019-06-24 08:51:04'),
(854, 'delete_quotation', 'deleted', 'quatation information deleted', '66', 66, '162.158.118.105', '2019-06-25 09:56:07'),
(855, 'delete_quotation', 'deleted', 'quatation information deleted', '66', 66, '162.158.118.105', '2019-06-25 09:56:12'),
(856, 'save_category', 'insert', 'Category information save', '1', 1, '162.158.118.235', '2019-06-25 10:20:20'),
(857, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.155.87', '2019-06-25 10:21:46'),
(858, 'customer_update', 'update', 'Customer information update', '1', 1, '162.158.155.87', '2019-06-25 10:24:50'),
(859, 'company-profile-update', 'updated', 'company profile information updated', '69', 69, '162.158.154.254', '2019-06-25 10:26:47'),
(860, 'save_price_model_mapping', 'insert', 'price model mapping information save', '1', 0, '162.158.155.165', '2019-06-26 06:02:05'),
(861, 'save_order', 'inserted', 'order information inserted', '69', 69, '162.158.155.159', '2019-06-26 06:09:59'),
(862, 'invoice_receipt', 'updated', 'order payment information updated', '69', 69, '162.158.155.75', '2019-06-26 06:10:53'),
(863, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '69', 69, '141.101.107.181', '2019-06-26 06:14:18'),
(864, 'make_payment', 'updated', 'order payment information updated', '69', 69, '162.158.154.32', '2019-06-26 06:15:01'),
(865, 'make_payment', 'updated', 'order payment information updated', '69', 69, '141.101.107.181', '2019-06-26 06:17:05'),
(866, 'save_order', 'inserted', 'order information inserted', '69', 69, '162.158.154.32', '2019-06-26 06:18:36'),
(867, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '69', 69, '162.158.154.110', '2019-06-26 06:18:50'),
(868, 'make_payment', 'updated', 'order payment information updated', '69', 69, '162.158.155.87', '2019-06-26 06:19:09'),
(869, 'invoice_receipt', 'updated', 'order payment information updated', '69', 69, '162.158.155.45', '2019-06-26 06:29:58'),
(870, 'user-save', 'insert', 'Color information save', '69', 69, '141.101.107.181', '2019-06-26 06:51:55'),
(871, 'role-save', 'insert', 'role permission information save', '69', 69, '141.101.99.108', '2019-06-26 06:56:03'),
(872, 'assign-user-role-save', 'insert', 'assign user role information save', '69', 69, '162.158.155.75', '2019-06-26 06:56:19'),
(873, 'role-update', 'updated', 'role permission updated done', '69', 69, '141.101.99.108', '2019-06-26 07:01:06'),
(874, 'role-update', 'updated', 'role permission updated done', '69', 69, '141.101.99.108', '2019-06-26 07:01:33'),
(875, 'role-save', 'insert', 'role permission information save', '69', 69, '141.101.99.108', '2019-06-26 07:02:48'),
(876, 'role-save', 'insert', 'role permission information save', '69', 69, '141.101.99.108', '2019-06-26 07:03:53'),
(877, 'b_level', 'insert', 'user information save', '1', 1, '141.101.98.155', '2019-06-26 09:39:57'),
(878, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '141.101.98.155', '2019-06-26 09:41:00'),
(879, 'role_save', 'insert', 'role permission save', '1', 1, '141.101.99.108', '2019-06-26 09:45:09'),
(880, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '141.101.99.108', '2019-06-26 09:46:03'),
(881, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '141.101.99.108', '2019-06-26 09:46:27'),
(882, 'role_update', 'updated', 'role permission updated', '1', 1, '141.101.99.108', '2019-06-26 09:49:49'),
(883, 'role_update', 'updated', 'role permission updated', '1', 1, '162.158.155.207', '2019-06-26 10:11:14'),
(884, 'role-update', 'updated', 'role permission updated done', '69', 69, '141.101.107.217', '2019-06-26 10:30:48'),
(885, 'role-update', 'updated', 'role permission updated done', '69', 69, '162.158.154.242', '2019-06-26 12:12:49'),
(886, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.158.98', '2019-06-26 14:41:07'),
(887, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.154.194', '2019-06-27 05:05:10'),
(888, 'company-profile-update', 'updated', 'company profile information updated', '73', 73, '162.158.158.134', '2019-06-27 05:09:35'),
(889, 'customer-save', 'insert', 'Customer information save', '73', 73, '162.158.154.32', '2019-06-27 05:12:21'),
(890, 'company-profile-update', 'updated', 'company profile information updated', '72', 72, '162.158.155.87', '2019-06-27 05:20:55'),
(891, 'c-cost-factor-save', 'insert', 'cost factor information save', '72', 72, '162.158.154.26', '2019-06-27 05:33:53'),
(892, 'customer-save', 'insert', 'Customer information save', '72', 72, '162.158.154.32', '2019-06-27 05:35:13'),
(893, 'c-us-state-save', 'insert', 'us state information save', '72', 72, '141.101.99.144', '2019-06-27 05:36:28'),
(894, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.155.117', '2019-06-27 05:41:34'),
(895, 'save_price_model_mapping', 'insert', 'price model mapping information save', '1', 0, '162.158.155.87', '2019-06-27 05:42:04'),
(896, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.155.69', '2019-06-27 05:45:05'),
(897, 'b-cost-factor-save', 'insert', 'cost factor information save', '1', 1, '162.158.155.195', '2019-06-27 06:40:14');
INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(898, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '162.158.155.75', '2019-06-27 07:06:02'),
(899, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '141.101.99.72', '2019-06-27 07:06:12'),
(900, 'b_level', 'insert', 'Color information save', '1', 1, '162.158.155.75', '2019-06-27 07:06:47'),
(901, 'save-price-style', 'insert', 'price model information save', '1', 1, '141.101.99.72', '2019-06-27 07:10:19'),
(902, 'update_price_style', 'updated', 'price model information updated', '1', 1, '141.101.99.72', '2019-06-27 07:11:08'),
(903, 'purchase-save', 'insert', 'purchase information save', '1', 1, '141.101.107.67', '2019-06-27 08:35:34'),
(904, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '141.101.107.67', '2019-06-27 08:35:34'),
(905, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '141.101.107.67', '2019-06-27 08:35:34'),
(906, 'purchase-save', 'insert', 'purchase information save', '1', 1, '162.158.154.188', '2019-06-27 08:39:16'),
(907, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '162.158.154.188', '2019-06-27 08:39:16'),
(908, 'raw-material-usage-save', 'deleted', 'raw material usages information save', '1', 1, '162.158.154.188', '2019-06-27 08:39:53'),
(909, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '162.158.154.188', '2019-06-27 08:43:50'),
(910, 'raw-material-return-purchase-approved', 'approved', 'raw material purchase  approved', '1', 1, '141.101.107.67', '2019-06-27 08:45:08'),
(911, 'delete_price_model_mapping', 'deleted', 'deleted price model mapping information', '1', 0, '162.158.154.242', '2019-06-27 08:57:47'),
(912, 'delete_price_model_mapping', 'deleted', 'deleted price model mapping information', '1', 0, '162.158.154.242', '2019-06-27 08:57:51'),
(913, 'user-save', 'insert', 'Color information save', '72', 72, '162.158.155.87', '2019-06-27 09:02:41'),
(914, 'user-save', 'insert', 'Color information save', '72', 72, '162.158.155.69', '2019-06-27 09:18:38'),
(915, 'user-save', 'insert', 'Color information save', '72', 72, '141.101.99.120', '2019-06-27 09:22:09'),
(916, 'user-save', 'insert', 'Color information save', '72', 72, '162.158.155.69', '2019-06-27 09:33:22'),
(917, 'save_order', 'inserted', 'order information inserted', '72', 72, '141.101.98.179', '2019-06-27 09:38:56'),
(918, 'user-save', 'insert', 'Color information save', '72', 72, '141.101.107.67', '2019-06-27 09:39:36'),
(919, 'customer-save', 'insert', 'Customer information save', '72', 72, '141.101.99.222', '2019-06-27 09:51:49'),
(920, 'c-customer-delete', 'deleted', 'customer deleted information', '72', 72, '162.158.154.236', '2019-06-27 09:52:01'),
(921, 'appointment-setup', 'insert', 'appointment information save', '72', 72, '141.101.99.222', '2019-06-27 09:52:48'),
(922, 'user-save', 'insert', 'Color information save', '72', 72, '141.101.107.127', '2019-06-27 10:12:08'),
(923, 'b_level', 'insert', 'user information save', '1', 1, '162.158.155.189', '2019-06-27 10:19:38'),
(924, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.155.195', '2019-06-27 12:06:54'),
(925, 'customer_update', 'update', 'Customer information update', '1', 1, '141.101.99.120', '2019-06-27 12:09:51'),
(926, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.154.32', '2019-06-27 12:12:28'),
(927, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.154.32', '2019-06-27 12:13:43'),
(928, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.154.242', '2019-06-27 12:24:41'),
(929, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.242', '2019-06-27 12:29:51'),
(930, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.230', '2019-06-27 12:34:09'),
(931, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.230', '2019-06-27 12:35:16'),
(932, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.230', '2019-06-27 12:36:06'),
(933, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.230', '2019-06-27 12:41:50'),
(934, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.158.134', '2019-06-27 12:46:47'),
(935, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '141.101.107.67', '2019-06-27 12:46:58'),
(936, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '141.101.98.155', '2019-06-27 12:47:09'),
(937, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.154.242', '2019-06-27 12:48:00'),
(938, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.32', '2019-06-27 12:48:13'),
(939, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.155.117', '2019-06-27 12:48:33'),
(940, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.154.230', '2019-06-27 12:52:44'),
(941, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.154.230', '2019-06-27 12:52:49'),
(942, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.154.242', '2019-06-27 12:57:30'),
(943, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '141.101.107.217', '2019-06-27 14:17:20'),
(944, 'customer-save', 'insert', 'Customer information save', '66', 66, '162.158.154.242', '2019-06-27 14:19:38'),
(945, 'supplier_save', 'insert', 'supplier information save', '1', 1, '141.101.107.217', '2019-06-27 14:42:52'),
(946, 'supplier_save', 'insert', 'supplier information save', '1', 1, '162.158.154.230', '2019-06-27 14:43:53'),
(947, 'delete_quotation', 'deleted', 'quatation information deleted', '66', 66, '162.158.118.17', '2019-06-27 14:51:21'),
(948, 'customer-save', 'insert', 'Customer information save', '2', 2, '162.158.150.29', '2019-06-29 04:36:39'),
(949, 'c-customer-delete', 'deleted', 'customer deleted information', '2', 2, '162.158.150.107', '2019-06-29 04:37:41'),
(950, 'customer-save', 'insert', 'Customer information save', '2', 2, '162.158.150.107', '2019-06-29 04:39:15'),
(951, 'customer-update', 'update', 'Customer information update', '66', 66, '162.158.165.59', '2019-06-29 06:13:10'),
(952, 'cancel_comment', 'cancel', 'order cancel comment', '66', 66, '162.158.165.185', '2019-06-29 06:25:06'),
(953, 'c-customer-order-return-save', 'insert', 'customer order return information save', '66', 66, '162.158.166.150', '2019-06-29 06:58:21'),
(954, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.131', '2019-06-29 07:20:19'),
(955, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.166.54', '2019-06-29 07:30:54'),
(956, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.59', '2019-06-29 07:45:37'),
(957, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.9', '2019-06-29 07:45:54'),
(958, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.221', '2019-06-29 08:28:36'),
(959, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.68.146.105', '2019-06-29 08:30:50'),
(960, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.167.175', '2019-06-29 09:01:33'),
(961, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.167.73', '2019-06-29 09:30:51'),
(962, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.135.41', '2019-06-29 09:33:18'),
(963, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.165.59', '2019-06-29 09:34:02'),
(964, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.165.59', '2019-06-29 09:34:46'),
(965, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.59', '2019-06-29 09:35:22'),
(966, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.165.59', '2019-06-29 09:35:36'),
(967, 'customer-update', 'update', 'Customer information update', '66', 66, '162.158.166.114', '2019-06-29 09:44:37'),
(968, 'save_order', 'inserted', 'order information inserted', '66', 66, '172.68.144.116', '2019-06-29 09:46:02'),
(969, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.68.144.116', '2019-06-29 09:46:20'),
(970, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.165.71', '2019-06-29 09:46:42'),
(971, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.165.71', '2019-06-29 09:46:51'),
(972, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.165.71', '2019-06-29 09:47:45'),
(973, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.165.71', '2019-06-29 09:50:41'),
(974, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.166.180', '2019-06-29 09:51:18'),
(975, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.68.146.111', '2019-06-29 09:55:39'),
(976, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.165.71', '2019-06-29 09:57:35'),
(977, 'c-cost-factor-save', 'insert', 'cost factor information save', '66', 66, '162.158.167.85', '2019-06-29 10:18:53'),
(978, 'c-cost-factor-save', 'insert', 'cost factor information save', '66', 66, '162.158.167.85', '2019-06-29 10:19:04'),
(979, 'user-save', 'insert', 'Color information save', '66', 66, '172.69.135.41', '2019-06-29 11:16:20'),
(980, 'role-save', 'insert', 'role permission information save', '66', 66, '172.69.135.41', '2019-06-29 11:18:09'),
(981, 'assign-user-role-save', 'insert', 'assign user role information save', '66', 66, '162.158.167.73', '2019-06-29 11:18:47'),
(982, 'appointment-setup', 'insert', 'appointment information save', '66', 66, '162.158.167.73', '2019-06-29 11:25:52'),
(983, 'role-update', 'updated', 'role permission updated done', '66', 66, '162.158.166.54', '2019-06-29 12:24:56'),
(984, 'role-update', 'updated', 'role permission updated done', '66', 66, '162.158.165.185', '2019-06-29 12:26:24'),
(985, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.166.54', '2019-06-29 12:37:44'),
(986, 'role_update', 'updated', 'role permission updated', '1', 1, '162.158.167.73', '2019-06-29 12:39:25'),
(987, 'assign_user_role_save', 'role assign', 'employee role assign', '1', 1, '162.158.166.54', '2019-06-29 12:39:48'),
(988, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.167.73', '2019-06-29 12:42:53'),
(989, 'customer_update', 'update', 'Customer information update', '11', 1, '172.68.144.134', '2019-06-29 12:52:05'),
(990, 'customer_update', 'update', 'Customer information update', '11', 1, '172.68.146.243', '2019-06-29 12:56:34'),
(991, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.165.9', '2019-06-29 12:58:13'),
(992, 'b_level', 'deleted', 'Customer information deleted', '11', 1, '172.68.146.243', '2019-06-29 13:44:50'),
(993, 'company-profile-update', 'updated', 'company profile information updated', '86', 86, '162.158.165.245', '2019-06-29 13:47:35'),
(994, 'import-c-us-state-save', 'insert', 'us state information import done', '86', 86, '162.158.165.245', '2019-06-29 13:50:16'),
(995, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '172.68.146.15', '2019-06-29 13:50:56'),
(996, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '162.158.165.245', '2019-06-29 13:51:02'),
(997, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '162.158.165.245', '2019-06-29 13:51:07'),
(998, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '162.158.165.245', '2019-06-29 13:51:13'),
(999, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '172.68.146.15', '2019-06-29 13:51:18'),
(1000, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '162.158.165.245', '2019-06-29 13:51:23'),
(1001, 'c-us-state-delete', 'deleted', 'us state information deleted', '86', 86, '172.68.146.15', '2019-06-29 13:51:30'),
(1002, 'import-c-us-state-save', 'insert', 'us state information import done', '86', 86, '172.68.146.15', '2019-06-29 13:52:22'),
(1003, 'c-us-state-save', 'insert', 'us state information save', '86', 86, '172.68.146.15', '2019-06-29 13:56:38'),
(1004, 'c-us-state-update', 'updated', 'us state information update', '86', 86, '172.68.146.15', '2019-06-29 13:57:03'),
(1005, 'customer-csv-upload', 'insert', 'customer csv information imported', '11', 1, '162.158.166.180', '2019-06-29 13:58:51'),
(1006, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.180', '2019-06-29 13:59:12'),
(1007, 'customer-csv-upload', 'insert', 'customer csv information imported', '11', 1, '162.158.166.198', '2019-06-29 14:01:50'),
(1008, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.180', '2019-06-29 14:02:55'),
(1009, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.180', '2019-06-29 14:03:24'),
(1010, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.198', '2019-06-29 14:03:47'),
(1011, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.243', '2019-06-29 14:03:50'),
(1012, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.198', '2019-06-29 14:04:12'),
(1013, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.180', '2019-06-29 14:04:41'),
(1014, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.243', '2019-06-29 14:05:33'),
(1015, 'customer_update', 'update', 'Customer information update', '11', 1, '162.158.166.180', '2019-06-29 14:05:56'),
(1016, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.147', '2019-06-29 14:07:45'),
(1017, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.147', '2019-06-29 14:09:04'),
(1018, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.147', '2019-06-29 14:10:14'),
(1019, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.147', '2019-06-29 14:10:54'),
(1020, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.105', '2019-06-29 14:12:37'),
(1021, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '162.158.165.207', '2019-06-29 14:13:46'),
(1022, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.146.15', '2019-06-29 14:14:52'),
(1023, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '162.158.166.12', '2019-06-29 14:15:48'),
(1024, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.68.144.182', '2019-06-29 14:17:07'),
(1025, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '172.69.135.71', '2019-06-29 14:17:20'),
(1026, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '162.158.166.198', '2019-06-29 14:17:53'),
(1027, 'customer_save', 'insert', 'Customer information save', '11', 1, '162.158.165.239', '2019-06-29 14:24:27'),
(1028, 'purchase-save', 'insert', 'purchase information save', '1', 1, '172.68.144.134', '2019-06-29 14:41:00'),
(1029, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '172.68.144.134', '2019-06-29 14:41:00'),
(1030, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '172.68.144.134', '2019-06-29 14:41:00'),
(1031, 'c-us-state-delete', 'deleted', 'us state information deleted', '66', 66, '162.158.166.198', '2019-06-29 14:52:20'),
(1032, 'c-us-state-delete', 'deleted', 'us state information deleted', '66', 66, '162.158.166.192', '2019-06-29 14:52:26'),
(1033, 'import-c-us-state-save', 'insert', 'us state information import done', '66', 66, '162.158.166.198', '2019-06-29 14:52:41'),
(1034, 'delete_user', 'deleted', 'user information deleted', '11', 1, '172.68.144.134', '2019-06-29 15:04:53'),
(1035, 'update_user', 'updated', 'user information updated ', '11', 1, '172.68.144.134', '2019-06-29 15:05:58'),
(1036, 'update_user', 'updated', 'user information updated ', '11', 1, '172.68.144.134', '2019-06-29 15:06:12'),
(1037, 'update_user', 'updated', 'user information updated ', '1', 1, '172.68.146.69', '2019-06-29 15:06:47'),
(1038, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.221', '2019-06-30 05:31:09'),
(1039, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.185', '2019-06-30 05:36:02'),
(1040, 'save_category', 'insert', 'Category information save', '1', 1, '172.68.144.134', '2019-06-30 05:37:21'),
(1041, 'category_update', 'updated', 'Category information updated', '1', 1, '172.68.144.134', '2019-06-30 05:37:31'),
(1042, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '172.68.144.134', '2019-06-30 05:37:36'),
(1043, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.185', '2019-06-30 05:37:41'),
(1044, 'save_category', 'insert', 'Category information save', '1', 1, '172.68.144.134', '2019-06-30 05:37:52'),
(1045, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '172.68.144.134', '2019-06-30 05:38:12'),
(1046, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.144.134', '2019-06-30 05:39:20'),
(1047, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '172.68.146.153', '2019-06-30 05:45:12'),
(1048, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '172.68.146.153', '2019-06-30 05:47:53'),
(1049, 'b_level', 'updated', 'Color information updated', '1', 1, '162.158.166.222', '2019-06-30 05:48:13'),
(1050, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.166.222', '2019-06-30 05:48:18'),
(1051, 'delete_price_style', 'deleted', 'price model information deleted', '1', 1, '162.158.165.9', '2019-06-30 06:03:22'),
(1052, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.166.12', '2019-06-30 06:11:31'),
(1053, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.166.12', '2019-06-30 06:13:59'),
(1054, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.166.30', '2019-06-30 06:26:29'),
(1055, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.166.30', '2019-06-30 06:26:46'),
(1056, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.166.30', '2019-06-30 06:37:14'),
(1057, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.165.9', '2019-06-30 07:20:25'),
(1058, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '172.69.135.71', '2019-06-30 10:26:20'),
(1059, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.165.15', '2019-06-30 12:04:46'),
(1060, 'delete_user', 'deleted', 'user information deleted', '1', 1, '162.158.165.185', '2019-06-30 12:22:23'),
(1061, 'uom_update', 'update', 'unit of measurement updated', '11', 1, '162.158.165.185', '2019-06-30 12:23:16'),
(1062, 'uom_delete', 'deleted', 'unit of measurement deleted', '11', 1, '162.158.165.185', '2019-06-30 12:23:26'),
(1063, 'b_level', 'insert', 'unit of measurement insert done', '11', 1, '162.158.165.185', '2019-06-30 12:23:34'),
(1064, 'update_user', 'updated', 'user information updated ', '1', 1, '162.158.165.185', '2019-06-30 12:29:06'),
(1065, 'b_level', 'deleted', 'Customer information deleted', '11', 1, '162.158.165.185', '2019-06-30 12:29:45'),
(1066, 'product_update', 'updated', 'product information updated', '1', 0, '172.68.144.38', '2019-06-30 12:39:32'),
(1067, 'product_update', 'updated', 'product information updated', '1', 0, '172.68.144.38', '2019-06-30 12:40:14'),
(1068, 'customer_update', 'update', 'Customer information update', '1', 1, '172.68.144.38', '2019-06-30 12:44:14'),
(1069, 'sms-send', 'insert', 'custom sms send', '1', 1, '172.68.144.128', '2019-06-30 12:52:08'),
(1070, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '172.68.146.147', '2019-06-30 13:38:27'),
(1071, 'customer_update', 'update', 'Customer information update', '1', 1, '172.68.144.116', '2019-06-30 13:46:26'),
(1072, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '162.158.167.223', '2019-06-30 13:48:46'),
(1073, 'raw-material-return-purchase-save', 'insert', 'raw material return puchase information save', '1', 1, '162.158.167.73', '2019-07-01 05:26:52'),
(1074, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.166.114', '2019-07-01 05:56:29'),
(1075, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.166.198', '2019-07-01 05:56:39'),
(1076, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.166.114', '2019-07-01 05:56:46'),
(1077, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '172.68.144.134', '2019-07-01 05:59:17'),
(1078, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '172.68.144.134', '2019-07-01 05:59:22'),
(1079, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.9', '2019-07-01 05:59:28'),
(1080, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.9', '2019-07-01 05:59:33'),
(1081, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.9', '2019-07-01 05:59:37'),
(1082, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '172.68.144.128', '2019-07-01 05:59:47'),
(1083, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.166.180', '2019-07-01 05:59:47'),
(1084, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.167.73', '2019-07-01 06:22:33'),
(1085, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '172.69.134.142', '2019-07-01 09:18:56'),
(1086, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.165.191', '2019-07-01 10:44:32'),
(1087, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.165.207', '2019-07-01 10:47:34'),
(1088, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.207', '2019-07-01 11:05:51'),
(1089, 'customer-csv-upload', 'insert', 'customer csv information imported', '1', 1, '162.158.165.207', '2019-07-01 11:09:40'),
(1090, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.71', '2019-07-01 11:10:12'),
(1091, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.71', '2019-07-01 11:10:16'),
(1092, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.71', '2019-07-01 11:10:20'),
(1093, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '162.158.165.71', '2019-07-01 11:10:24'),
(1094, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '172.68.146.147', '2019-07-01 11:18:19'),
(1095, 'save_category', 'insert', 'Category information save', '1', 1, '172.68.146.147', '2019-07-01 11:18:41'),
(1096, 'category_update', 'updated', 'Category information updated', '1', 1, '172.68.146.147', '2019-07-01 11:19:09'),
(1097, 'category_update', 'updated', 'Category information updated', '1', 1, '172.68.146.147', '2019-07-01 11:21:09'),
(1098, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '172.68.146.243', '2019-07-01 11:23:47'),
(1099, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '172.68.146.243', '2019-07-01 11:24:00'),
(1100, 'b_level', 'insert', 'Color information save', '1', 1, '172.68.146.147', '2019-07-01 11:26:33'),
(1101, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '162.158.165.9', '2019-07-01 11:28:14'),
(1102, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:28:32'),
(1103, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:28:36'),
(1104, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:28:40'),
(1105, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:28:43'),
(1106, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:28:48'),
(1107, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.158.165.9', '2019-07-01 11:32:16'),
(1108, 'save-price-style', 'insert', 'price model information save', '1', 1, '162.158.165.9', '2019-07-01 11:41:25'),
(1109, 'update_price_style', 'updated', 'price model information updated', '1', 1, '162.158.165.9', '2019-07-01 11:42:36'),
(1110, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.165.9', '2019-07-01 11:47:55'),
(1111, 'my_account_update', 'insert', 'my account information save', '1', 1, '162.158.165.9', '2019-07-01 11:50:49'),
(1112, 'save-price-style', 'insert', 'price model information save', '1', 1, '162.158.165.9', '2019-07-01 11:51:59'),
(1113, 'update_price_style', 'updated', 'price model information updated', '1', 1, '162.158.165.9', '2019-07-01 11:52:50'),
(1114, 'save_price_model_mapping', 'insert', 'price model mapping information save', '1', 0, '162.158.166.180', '2019-07-01 11:56:00'),
(1115, 'product_save', 'insert', 'product information save', '1', 0, '162.158.166.12', '2019-07-01 12:11:23'),
(1116, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '162.158.166.12', '2019-07-01 12:11:36'),
(1117, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '162.158.165.161', '2019-07-01 12:13:09'),
(1118, 'product_save', 'insert', 'product information save', '1', 0, '172.69.135.41', '2019-07-01 12:16:48'),
(1119, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '172.69.135.41', '2019-07-01 12:16:53'),
(1120, 'customer_save', 'insert', 'Customer information save', '1', 1, '172.68.144.134', '2019-07-01 12:32:45'),
(1121, 'company-profile-update', 'updated', 'company profile information updated', '127', 127, '162.158.167.85', '2019-07-01 12:35:16'),
(1122, 'my-account-update', 'updated', 'my account information updated', '127', 127, '172.68.146.147', '2019-07-01 12:36:11'),
(1123, 'save_order', 'inserted', 'order information inserted', '127', 127, '162.158.165.207', '2019-07-01 12:54:44'),
(1124, 'invoice_receipt', 'updated', 'order payment information updated', '127', 127, '162.158.166.180', '2019-07-01 12:59:09'),
(1125, 'save_order', 'inserted', 'order information inserted', '127', 127, '162.158.165.191', '2019-07-01 13:12:18'),
(1126, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '127', 127, '172.68.144.128', '2019-07-01 13:12:19'),
(1127, 'invoice_receipt', 'updated', 'order payment information updated', '127', 127, '172.68.144.128', '2019-07-01 13:12:50'),
(1128, 'save_order', 'inserted', 'order information inserted', '127', 127, '172.69.135.71', '2019-07-01 13:24:28'),
(1129, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '127', 127, '162.158.165.89', '2019-07-01 13:24:29'),
(1130, 'invoice_receipt', 'updated', 'order payment information updated', '127', 127, '162.158.165.89', '2019-07-01 13:25:05'),
(1131, 'invoice_receipt', 'updated', 'order payment information updated', '127', 127, '172.69.135.71', '2019-07-01 13:33:51'),
(1132, 'make_payment', 'updated', 'order payment information updated', '127', 127, '172.69.135.71', '2019-07-01 13:37:12'),
(1133, 'make_payment', 'updated', 'order payment information updated', '127', 127, '172.69.135.71', '2019-07-01 13:38:28'),
(1134, 'customer_update', 'update', 'Customer information update', '1', 1, '172.68.146.69', '2019-07-02 06:02:51'),
(1135, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '172.68.146.69', '2019-07-02 06:07:48'),
(1136, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.166.108', '2019-07-02 06:11:50'),
(1137, 'customer_save', 'insert', 'Customer information save', '1', 1, '172.69.170.41', '2019-07-03 00:30:39'),
(1138, 'customer_update', 'update', 'Customer information update', '1', 1, '172.69.170.71', '2019-07-03 00:36:10'),
(1139, 'customer_update', 'update', 'Customer information update', '1', 1, '172.69.170.77', '2019-07-03 00:36:13'),
(1140, 'customer_update', 'update', 'Customer information update', '1', 1, '162.158.118.113', '2019-07-03 00:44:35'),
(1141, 'company-profile-update', 'updated', 'company profile information updated', '128', 128, '172.69.170.77', '2019-07-03 00:46:38'),
(1142, 'user-save', 'insert', 'Color information save', '128', 128, '172.69.170.77', '2019-07-03 01:02:03'),
(1143, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.158.156', '2019-07-03 09:46:29'),
(1144, '4767', 'deleted', 'customer file deleted', '1', 1, '162.158.155.63', '2019-07-03 09:54:18'),
(1145, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.158.156', '2019-07-03 10:08:42'),
(1146, 'customer_update', 'update', 'Customer information update', '1', 1, '141.101.98.119', '2019-07-03 11:55:51'),
(1147, '4768', 'deleted', 'customer file deleted', '1', 1, '141.101.98.119', '2019-07-03 11:57:20'),
(1148, 'send-customer-comment', 'insert', 'send customer comment', '1', 1, '162.158.158.120', '2019-07-03 12:39:14'),
(1149, 'user-save', 'insert', 'Color information save', '66', 66, '162.158.118.187', '2019-07-03 17:49:38'),
(1150, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.114', '2019-07-04 06:35:56'),
(1151, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.154.92', '2019-07-04 06:38:57'),
(1152, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.98.119', '2019-07-04 06:41:13'),
(1153, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '141.101.98.119', '2019-07-04 06:41:14'),
(1154, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.154.92', '2019-07-04 06:42:16'),
(1155, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.120', '2019-07-04 06:48:21'),
(1156, 'c-customer-delete', 'deleted', 'customer deleted information', '66', 66, '162.158.158.120', '2019-07-04 06:49:06'),
(1157, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.158.120', '2019-07-04 06:52:35'),
(1158, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.158.156', '2019-07-04 06:52:41'),
(1159, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.98.89', '2019-07-04 06:54:44'),
(1160, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 06:55:01'),
(1161, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.98.89', '2019-07-04 07:00:24'),
(1162, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '141.101.98.89', '2019-07-04 07:00:25'),
(1163, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.98', '2019-07-04 07:00:38'),
(1164, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.104', '2019-07-04 07:03:21'),
(1165, 'make_payment', 'updated', 'order payment information updated', '66', 66, '141.101.98.119', '2019-07-04 07:16:16'),
(1166, 'customer_save', 'insert', 'Customer information save', '1', 1, '162.158.158.156', '2019-07-04 07:47:12'),
(1167, 'b-category-delete', 'deleted', 'category information deleted', '1', 1, '162.158.158.70', '2019-07-04 09:05:48'),
(1168, 'c-cost-factor-save', 'insert', 'cost factor information save', '66', 66, '162.158.118.159', '2019-07-04 10:53:31'),
(1169, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.118.89', '2019-07-04 10:54:12'),
(1170, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.118.159', '2019-07-04 10:54:13'),
(1171, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.118.81', '2019-07-04 10:54:24'),
(1172, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.99.168', '2019-07-04 15:03:13'),
(1173, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.154.68', '2019-07-04 15:03:14'),
(1174, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.114', '2019-07-04 15:03:40'),
(1175, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.107.199', '2019-07-04 15:06:07'),
(1176, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.120', '2019-07-04 15:06:08'),
(1177, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.99.168', '2019-07-04 15:06:16'),
(1178, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.156', '2019-07-04 15:07:06'),
(1179, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.70', '2019-07-04 15:10:23'),
(1180, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.70', '2019-07-04 15:13:24'),
(1181, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.104', '2019-07-04 15:14:30'),
(1182, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.98', '2019-07-04 15:14:46'),
(1183, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.107.199', '2019-07-04 15:15:16'),
(1184, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.155.63', '2019-07-04 15:16:40'),
(1185, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.104', '2019-07-04 15:16:40'),
(1186, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.98', '2019-07-04 15:16:48'),
(1187, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.156', '2019-07-04 15:18:34'),
(1188, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.154.68', '2019-07-04 15:18:39'),
(1189, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.70', '2019-07-04 15:19:02'),
(1190, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.70', '2019-07-04 15:19:03'),
(1191, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.70', '2019-07-04 15:19:10'),
(1192, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 15:19:27'),
(1193, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 15:19:42'),
(1194, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.70', '2019-07-04 15:20:00'),
(1195, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.104', '2019-07-04 15:21:24'),
(1196, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.114', '2019-07-04 15:21:25'),
(1197, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.114', '2019-07-04 15:21:31'),
(1198, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.98.119', '2019-07-04 15:22:16'),
(1199, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.76', '2019-07-04 15:22:53'),
(1200, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.120', '2019-07-04 15:23:40'),
(1201, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.76', '2019-07-04 15:23:41'),
(1202, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.120', '2019-07-04 15:23:48'),
(1203, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.107.199', '2019-07-04 15:26:11'),
(1204, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.114', '2019-07-04 15:26:12'),
(1205, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.70', '2019-07-04 15:26:20'),
(1206, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:26:48'),
(1207, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.107.199', '2019-07-04 15:28:36'),
(1208, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '141.101.107.199', '2019-07-04 15:28:37'),
(1209, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.107.199', '2019-07-04 15:28:42'),
(1210, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.98', '2019-07-04 15:29:23'),
(1211, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.158.70', '2019-07-04 15:30:00'),
(1212, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:30:19'),
(1213, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:30:38'),
(1214, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:35:02'),
(1215, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.156', '2019-07-04 15:38:19'),
(1216, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:38:31'),
(1217, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.104', '2019-07-04 15:38:48'),
(1218, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.156', '2019-07-04 15:39:19'),
(1219, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.158.156', '2019-07-04 15:41:24'),
(1220, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.158.76', '2019-07-04 15:41:30'),
(1221, 'delete_order', 'deleted', 'order information deleted', '66', 66, '141.101.99.168', '2019-07-04 15:42:31'),
(1222, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.155.63', '2019-07-04 15:42:37'),
(1223, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.158.156', '2019-07-04 15:43:21'),
(1224, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.70', '2019-07-04 15:43:22'),
(1225, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 15:43:49'),
(1226, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 15:44:02'),
(1227, 'delete_order', 'deleted', 'order information deleted', '66', 66, '162.158.158.76', '2019-07-04 15:44:32'),
(1228, 'delete_order', 'deleted', 'order information deleted', '66', 66, '141.101.98.89', '2019-07-04 15:44:32'),
(1229, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.155.63', '2019-07-04 15:45:58'),
(1230, 'save_order', 'inserted', 'order information inserted', '66', 66, '141.101.98.239', '2019-07-04 16:06:20'),
(1231, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.158.156', '2019-07-04 16:06:21'),
(1232, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 16:06:29'),
(1233, 'make_payment', 'updated', 'order payment information updated', '66', 66, '141.101.98.89', '2019-07-04 16:06:43'),
(1234, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.154.68', '2019-07-04 16:07:11'),
(1235, 'update_shipping_method', 'Updated', 'Updated shipping method', '1', 1, '141.101.98.239', '2019-07-04 16:08:58'),
(1236, '4765', 'deleted', 'customer file deleted', '1', 1, '172.69.170.89', '2019-07-04 16:14:20'),
(1237, 'product_save', 'insert', 'product information save', '1', 0, '172.69.170.47', '2019-07-04 16:20:39'),
(1238, 'category_update', 'updated', 'Category information updated', '1', 1, '162.158.118.171', '2019-07-04 17:46:42'),
(1239, 'category_update', 'updated', 'Category information updated', '1', 1, '162.158.118.219', '2019-07-04 17:46:54'),
(1240, 'category_update', 'updated', 'Category information updated', '1', 1, '162.158.118.229', '2019-07-04 17:47:30'),
(1241, 'c-cost-factor-save', 'insert', 'cost factor information save', '66', 66, '162.158.118.131', '2019-07-04 18:02:46'),
(1242, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.118.237', '2019-07-04 18:03:41'),
(1243, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.118.49', '2019-07-04 18:06:42'),
(1244, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.119.14', '2019-07-04 18:12:37'),
(1245, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.119.14', '2019-07-04 18:12:52'),
(1246, 'assign-user-role-save', 'insert', 'assign user role information save', '66', 66, '162.158.118.107', '2019-07-04 18:24:17'),
(1247, 'appointment-setup', 'insert', 'appointment information save', '66', 66, '162.158.118.107', '2019-07-04 18:25:06'),
(1248, 'raw-material-usage-save', 'deleted', 'raw material usages information save', '1', 1, '162.158.118.149', '2019-07-04 18:33:01'),
(1249, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.229', '2019-07-04 23:55:29'),
(1250, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.249', '2019-07-04 23:55:36'),
(1251, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.131', '2019-07-04 23:55:43'),
(1252, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.95', '2019-07-04 23:55:51'),
(1253, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.119.14', '2019-07-04 23:55:57'),
(1254, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.89', '2019-07-04 23:56:31'),
(1255, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.191', '2019-07-04 23:57:01'),
(1256, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.23', '2019-07-04 23:57:20'),
(1257, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.7', '2019-07-04 23:59:21'),
(1258, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.5', '2019-07-04 23:59:40'),
(1259, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.118.131', '2019-07-05 00:02:27'),
(1260, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.118.203', '2019-07-05 00:02:28'),
(1261, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.118.203', '2019-07-05 00:03:15'),
(1262, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.118.203', '2019-07-05 00:04:20'),
(1263, 'customer-save', 'insert', 'Customer information save', '66', 66, '162.158.118.203', '2019-07-05 00:07:58'),
(1264, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.49', '2019-07-05 00:28:29'),
(1265, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.131', '2019-07-05 00:28:37'),
(1266, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.159', '2019-07-05 00:28:45'),
(1267, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.89', '2019-07-05 00:28:52'),
(1268, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.13', '2019-07-05 00:28:58'),
(1269, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.118.203', '2019-07-05 00:31:16'),
(1270, 'delete-product', 'deleted', 'product information deleted', '1', 0, '162.158.118.89', '2019-07-05 00:35:24'),
(1271, 'appointment-setup', 'insert', 'appointment information save', '66', 66, '172.69.170.47', '2019-07-05 00:37:14'),
(1272, 'appointment-setup', 'insert', 'appointment information save', '66', 66, '172.69.6.23', '2019-07-05 00:42:28'),
(1273, 'save_order', 'inserted', 'order information inserted', '66', 66, '172.69.170.89', '2019-07-05 00:55:23'),
(1274, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.69.170.89', '2019-07-05 00:55:24'),
(1275, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.170.89', '2019-07-05 01:00:38'),
(1276, 'save_order', 'inserted', 'order information inserted', '66', 66, '172.69.170.47', '2019-07-05 01:02:37'),
(1277, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.69.170.47', '2019-07-05 01:02:38'),
(1278, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.170.89', '2019-07-05 01:02:54'),
(1279, 'save_order', 'inserted', 'order information inserted', '66', 66, '172.69.170.89', '2019-07-05 01:06:25'),
(1280, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.69.6.29', '2019-07-05 01:06:25'),
(1281, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.6.29', '2019-07-05 01:06:48'),
(1282, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.170.89', '2019-07-05 01:19:40'),
(1283, 'make_payment', 'updated', 'order payment information updated', '66', 66, '172.69.6.83', '2019-07-05 01:23:43'),
(1284, 'save_order', 'inserted', 'order information inserted', '66', 66, '172.69.6.83', '2019-07-05 01:31:15'),
(1285, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '172.69.170.89', '2019-07-05 01:31:16'),
(1286, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '172.69.6.83', '2019-07-05 01:32:26'),
(1287, 'supplier_save', 'insert', 'supplier information save', '1', 1, '162.158.118.13', '2019-07-05 02:36:06'),
(1288, 'raw-material-usage-save', 'deleted', 'raw material usages information save', '1', 1, '172.69.170.89', '2019-07-05 02:39:12'),
(1289, 'purchase-save', 'insert', 'purchase information save', '1', 1, '172.69.6.83', '2019-07-05 02:44:56'),
(1290, 'purchase-save', 'insert', 'raw material stock information save', '1', 1, '172.69.6.83', '2019-07-05 02:44:56'),
(1291, 'c-customer-order-return-save', 'insert', 'customer order return information save', '66', 66, '172.69.6.29', '2019-07-05 02:56:50'),
(1292, 'c-customer-order-return-save', 'insert', 'customer order return information save', '66', 66, '172.69.6.29', '2019-07-05 02:58:48'),
(1293, 'c-customer-order-return-save', 'insert', 'customer order return information save', '66', 66, '172.69.170.17', '2019-07-05 02:59:44'),
(1294, 'cancel_comment', 'cancel', 'order cancel comment', '66', 66, '172.69.170.17', '2019-07-05 03:03:12'),
(1295, 'return_resend_comment', 'insert', 'return resend comment information save', '1', 1, '172.69.6.23', '2019-07-05 03:12:04'),
(1296, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.158.156', '2019-07-05 14:46:00'),
(1297, 'save_order', 'inserted', 'order information inserted', '2', 2, '162.158.165.83', '2019-07-05 15:56:13'),
(1298, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '2', 2, '162.158.165.143', '2019-07-05 15:59:14'),
(1299, 'save_order', 'inserted', 'order information inserted', '2', 2, '172.69.226.96', '2019-07-05 16:34:51'),
(1300, 'save_order', 'inserted', 'order information inserted', '2', 2, '141.101.88.137', '2019-07-05 16:38:54'),
(1301, 'user-update', 'updated', 'user information updated', '66', 66, '162.158.118.219', '2019-07-06 03:26:50'),
(1302, 'save_order', 'inserted', 'order information inserted', '66', 66, '162.158.118.49', '2019-07-06 03:54:40'),
(1303, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '66', 66, '162.158.118.49', '2019-07-06 03:54:41'),
(1304, 'invoice_receipt', 'updated', 'order payment information updated', '66', 66, '162.158.118.131', '2019-07-06 03:56:49'),
(1305, 'make_payment', 'updated', 'order payment information updated', '66', 66, '162.158.118.107', '2019-07-06 03:57:07'),
(1306, 'cancel_comment', 'cancel', 'order cancel comment', '66', 66, '162.158.118.107', '2019-07-06 04:28:25'),
(1307, 'c-customer-purchase-order-return-save', 'insert', 'purchase order return information save', '66', 66, '162.158.118.189', '2019-07-06 04:34:41'),
(1308, 'category_update', 'updated', 'Category information updated', '1', 1, '141.101.98.239', '2019-07-10 11:03:16'),
(1309, 'product_update', 'updated', 'product information updated', '1', 0, '162.158.158.156', '2019-07-10 11:14:58'),
(1310, 'customer_save', 'insert', 'Customer information save', '1', 1, '182.160.105.186', '2019-07-16 11:54:58'),
(1311, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-24 14:52:13'),
(1312, 'company-profile-update', 'updated', 'company profile information updated', '135', 135, '219.91.253.114', '2019-07-24 15:09:43'),
(1313, 'save_category', 'insert', 'Category information save', '1', 1, '219.91.253.114', '2019-07-25 13:53:32'),
(1314, 'save_category', 'insert', 'Category information save', '1', 1, '219.91.253.114', '2019-07-25 13:53:42'),
(1315, 'save_category', 'insert', 'Category information save', '1', 1, '219.91.253.114', '2019-07-25 13:53:52'),
(1316, 'save_category', 'insert', 'Category information save', '1', 1, '219.91.253.114', '2019-07-25 13:54:05'),
(1317, 'save_category', 'insert', 'Category information save', '1', 1, '219.91.253.114', '2019-07-25 13:54:12');
INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(1318, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:06:43'),
(1319, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:09:00'),
(1320, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:09:43'),
(1321, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:12:38'),
(1322, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:13:24'),
(1323, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:14:24'),
(1324, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:15:50'),
(1325, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:16:32'),
(1326, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:19:36'),
(1327, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:20:49'),
(1328, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:22:47'),
(1329, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:41:26'),
(1330, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:44:13'),
(1331, 'save_attribute', 'insert', 'attribute information save', '1', 1, '219.91.253.114', '2019-07-25 14:44:55'),
(1332, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 14:50:38'),
(1333, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 14:51:31'),
(1334, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 14:51:45'),
(1335, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 14:52:13'),
(1336, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 14:52:22'),
(1337, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:04:35'),
(1338, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:07:06'),
(1339, 'b_level', 'updated', 'Color information updated', '1', 1, '219.91.253.114', '2019-07-25 15:07:18'),
(1340, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 15:07:45'),
(1341, 'b_level', 'updated', 'Color information updated', '1', 1, '219.91.253.114', '2019-07-25 15:07:59'),
(1342, 'b_level', 'updated', 'Color information updated', '1', 1, '219.91.253.114', '2019-07-25 15:08:38'),
(1343, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:09:08'),
(1344, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:14:23'),
(1345, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:14:59'),
(1346, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 15:18:11'),
(1347, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-25 15:32:01'),
(1348, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-25 15:38:28'),
(1349, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 15:41:08'),
(1350, 'product_update', 'updated', 'product information updated', '1', 0, '219.91.253.114', '2019-07-25 15:46:19'),
(1351, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 15:59:34'),
(1352, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 15:59:48'),
(1353, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:00:28'),
(1354, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:00:47'),
(1355, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:01:03'),
(1356, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:05:46'),
(1357, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-25 16:06:23'),
(1358, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-25 16:09:58'),
(1359, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:17:27'),
(1360, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 16:19:56'),
(1361, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 16:26:59'),
(1362, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 16:28:12'),
(1363, 'b_level', 'deleted', 'Color information deleted', '1', 1, '219.91.253.114', '2019-07-25 16:31:27'),
(1364, 'b_level', 'deleted', 'Color information deleted', '1', 1, '219.91.253.114', '2019-07-25 16:31:34'),
(1365, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 16:34:19'),
(1366, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-25 16:37:21'),
(1367, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 16:39:55'),
(1368, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 16:40:58'),
(1369, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-25 16:41:26'),
(1370, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-25 16:41:35'),
(1371, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 16:44:47'),
(1372, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-25 16:45:18'),
(1373, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-25 16:45:25'),
(1374, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:47:05'),
(1375, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 16:47:20'),
(1376, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:48:47'),
(1377, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 16:49:02'),
(1378, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:49:23'),
(1379, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:49:30'),
(1380, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:49:45'),
(1381, 'b_level', 'insert', 'Color information save', '1', 1, '219.91.253.114', '2019-07-25 16:49:54'),
(1382, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 16:51:33'),
(1383, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:52:23'),
(1384, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-25 16:52:37'),
(1385, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-25 17:00:03'),
(1386, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-25 17:00:45'),
(1387, 'b_level', 'updated', 'Color information updated', '1', 1, '219.91.253.114', '2019-07-25 17:01:24'),
(1388, 'b_level', 'updated', 'Color information updated', '1', 1, '219.91.253.114', '2019-07-25 17:01:33'),
(1389, 'update_price_style', 'updated', 'price model information updated', '1', 1, '219.91.253.114', '2019-07-25 17:12:00'),
(1390, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:39:26'),
(1391, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:39:51'),
(1392, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:40:10'),
(1393, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:40:27'),
(1394, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:40:47'),
(1395, 'save-price-style', 'insert', 'price model information save', '1', 1, '219.91.253.114', '2019-07-25 17:41:09'),
(1396, 'category_update', 'updated', 'Category information updated', '1', 1, '219.91.253.114', '2019-07-25 18:27:41'),
(1397, 'category_update', 'updated', 'Category information updated', '1', 1, '219.91.253.114', '2019-07-25 18:27:50'),
(1398, 'category_update', 'updated', 'Category information updated', '1', 1, '219.91.253.114', '2019-07-25 18:33:15'),
(1399, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '219.91.253.114', '2019-07-25 18:35:42'),
(1400, 'update_shipping_method', 'Updated', 'Updated shipping method', '1', 1, '219.91.253.114', '2019-07-25 18:36:07'),
(1401, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '219.91.253.114', '2019-07-25 18:38:39'),
(1402, 'sms-config-delete', 'deleted', 'sms configuration deleted', '1', 1, '219.91.253.114', '2019-07-25 18:39:09'),
(1403, 'sms-config-update', 'updated', 'sms configuration updated', '1', 1, '219.91.253.114', '2019-07-25 18:39:29'),
(1404, 'my-account-update', 'updated', 'my account information updated', '135', 135, '219.91.253.114', '2019-07-25 18:51:19'),
(1405, 'customer-save', 'insert', 'Customer information save', '135', 135, '219.91.253.114', '2019-07-25 19:02:02'),
(1406, 'import-c-us-state-save', 'insert', 'us state information import done', '135', 135, '219.91.253.114', '2019-07-25 19:03:37'),
(1407, 'save_order', 'inserted', 'order information inserted', '135', 135, '219.91.253.114', '2019-07-25 19:12:07'),
(1408, 'save_order', 'inserted', 'order information inserted', '135', 135, '219.91.253.114', '2019-07-25 19:12:12'),
(1409, 'save_order', 'inserted', 'order information inserted', '135', 135, '219.91.253.114', '2019-07-25 19:12:21'),
(1410, 'customer_save', 'insert', 'Customer information save', '1', 1, '159.89.88.224', '2019-07-25 19:27:14'),
(1411, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-25 19:43:40'),
(1412, 'company-profile-update', 'updated', 'company profile information updated', '136', 136, '219.91.253.114', '2019-07-25 19:46:18'),
(1413, 'pattern-delete', 'deleted', 'pattern information deleted', '1', 1, '219.91.253.114', '2019-07-26 06:18:17'),
(1414, 'b_level', 'updated', 'Color information updated', '1', 1, '159.89.189.55', '2019-07-26 06:27:56'),
(1415, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-26 09:51:30'),
(1416, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '192.241.205.238', '2019-07-26 10:18:09'),
(1417, 'b_level', 'insert', 'Color information save', '1', 1, '192.241.205.238', '2019-07-26 10:20:41'),
(1418, 'b_level', 'deleted', 'Color information deleted', '1', 1, '192.241.205.238', '2019-07-26 10:20:49'),
(1419, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-26 10:31:50'),
(1420, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '219.91.253.114', '2019-07-26 10:34:40'),
(1421, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-26 10:41:47'),
(1422, 'company-profile-update', 'updated', 'company profile information updated', '139', 139, '219.91.253.114', '2019-07-26 10:42:28'),
(1423, 'company-profile-update', 'updated', 'company profile information updated', '139', 139, '219.91.253.114', '2019-07-26 11:22:13'),
(1424, 'my-account-update', 'updated', 'my account information updated', '139', 139, '219.91.253.114', '2019-07-26 11:22:39'),
(1425, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 11:39:28'),
(1426, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 11:39:30'),
(1427, 'delete_order', 'deleted', 'order information deleted', '139', 139, '219.91.253.114', '2019-07-26 11:39:59'),
(1428, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 11:41:36'),
(1429, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 11:42:46'),
(1430, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 11:51:55'),
(1431, 'b_level', 'insert', 'Tag information save', '1', 1, '219.91.253.114', '2019-07-26 12:09:03'),
(1432, 'b_level', 'updated', 'Tag information updated', '1', 1, '42.111.129.76', '2019-07-26 13:41:44'),
(1433, 'b_level', 'deleted', 'Tag information deleted', '1', 1, '219.91.253.114', '2019-07-26 13:47:11'),
(1434, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 13:49:08'),
(1435, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 13:49:16'),
(1436, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 13:49:34'),
(1437, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 13:49:35'),
(1438, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 13:49:44'),
(1439, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 13:59:33'),
(1440, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 14:09:43'),
(1441, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 14:10:50'),
(1442, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 14:12:55'),
(1443, 'b_level', 'deleted', 'Tag information deleted', '1', 1, '219.91.253.114', '2019-07-26 14:20:22'),
(1444, 'b_level', 'deleted', 'Tag information deleted', '1', 1, '219.91.253.114', '2019-07-26 14:20:26'),
(1445, 'b_level', 'deleted', 'Tag information deleted', '1', 1, '219.91.253.114', '2019-07-26 14:20:29'),
(1446, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 14:28:27'),
(1447, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 14:36:22'),
(1448, 'b_level', 'updated', 'Tag information updated', '1', 1, '219.91.253.114', '2019-07-26 14:49:19'),
(1449, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '219.91.253.114', '2019-07-26 14:50:20'),
(1450, 'uom_delete', 'deleted', 'unit of measurement deleted', '1', 1, '219.91.253.114', '2019-07-26 14:51:03'),
(1451, 'uom_update', 'update', 'unit of measurement updated', '1', 1, '219.91.253.114', '2019-07-26 14:51:15'),
(1452, 'uom_update', 'update', 'unit of measurement updated', '1', 1, '219.91.253.114', '2019-07-26 14:51:21'),
(1453, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:29:58'),
(1454, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 15:34:27'),
(1455, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:35:46'),
(1456, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:38:52'),
(1457, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:39:16'),
(1458, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:40:32'),
(1459, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:53:12'),
(1460, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 15:55:02'),
(1461, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 15:55:54'),
(1462, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 16:54:18'),
(1463, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 16:56:00'),
(1464, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 16:57:17'),
(1465, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 16:57:36'),
(1466, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 17:00:22'),
(1467, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 17:06:24'),
(1468, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.102.234', '2019-07-26 17:07:13'),
(1469, 'order_controller', 'synchronize', 'synchronize to b level order', '139', 139, '157.33.102.234', '2019-07-26 17:08:52'),
(1470, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 17:09:30'),
(1471, 'save_order', 'inserted', 'order information inserted', '139', 139, '157.33.6.154', '2019-07-26 17:10:15'),
(1472, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 17:11:17'),
(1473, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 17:12:51'),
(1474, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 17:15:33'),
(1475, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 17:16:56'),
(1476, 'delete_quotation', 'deleted', 'quatation information deleted', '139', 139, '157.33.6.154', '2019-07-26 17:43:29'),
(1477, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-26 18:08:13'),
(1478, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-26 18:08:20'),
(1479, 'save_order', 'inserted', 'order information inserted', '139', 139, '219.91.253.114', '2019-07-26 18:14:55'),
(1480, 'invoice_receipt', 'updated', 'order payment information updated', '139', 139, '219.91.253.114', '2019-07-26 18:15:13'),
(1481, 'delete-product', 'deleted', 'product information deleted', '1', 0, '219.91.253.114', '2019-07-26 18:17:57'),
(1482, 'product_update', 'updated', 'product information updated', '1', 0, '219.91.253.114', '2019-07-26 18:18:10'),
(1483, 'product_update', 'updated', 'product information updated', '1', 0, '219.91.253.114', '2019-07-26 18:18:21'),
(1484, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-26 18:21:59'),
(1485, 'company-profile-update', 'updated', 'company profile information updated', '140', 140, '219.91.253.114', '2019-07-26 18:22:53'),
(1486, 'my-account-update', 'updated', 'my account information updated', '140', 140, '219.91.253.114', '2019-07-26 18:23:07'),
(1487, 'c-cost-factor-save', 'insert', 'cost factor information save', '140', 140, '219.91.253.114', '2019-07-26 18:28:27'),
(1488, 'role-save', 'insert', 'role permission information save', '140', 140, '219.91.253.114', '2019-07-26 18:37:43'),
(1489, 'user-save', 'insert', 'Color information save', '140', 140, '219.91.253.114', '2019-07-26 18:39:09'),
(1490, 'assign-user-role-save', 'insert', 'assign user role information save', '140', 140, '219.91.253.114', '2019-07-26 18:39:50'),
(1491, 'save_order', 'inserted', 'order information inserted', '141', 140, '219.91.253.114', '2019-07-26 18:55:52'),
(1492, 'save_order', 'inserted', 'order information inserted', '140', 140, '219.91.253.114', '2019-07-26 18:57:30'),
(1493, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-26 18:57:36'),
(1494, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-26 19:00:54'),
(1495, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-26 19:02:55'),
(1496, 'save_order', 'inserted', 'order information inserted', '140', 140, '219.91.253.114', '2019-07-26 19:12:45'),
(1497, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-26 19:13:06'),
(1498, 'save_order', 'inserted', 'order information inserted', '140', 140, '219.91.253.114', '2019-07-26 19:14:41'),
(1499, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '140', 140, '219.91.253.114', '2019-07-26 19:14:41'),
(1500, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-26 19:14:50'),
(1501, 'customer-save', 'insert', 'Customer information save', '140', 140, '219.91.253.114', '2019-07-27 01:40:10'),
(1502, 'save_order', 'inserted', 'order information inserted', '140', 140, '24.27.75.250', '2019-07-27 02:22:27'),
(1503, 'invoice_receipt', 'updated', 'order payment information updated', '140', 140, '219.91.253.114', '2019-07-27 03:13:43'),
(1504, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '219.91.253.114', '2019-07-27 09:43:17'),
(1505, 'b_level', 'update', 'assign user role information updated', '1', 1, '219.91.253.114', '2019-07-27 10:51:16'),
(1506, 'b_level', 'update', 'assign user role information updated', '1', 1, '219.91.253.114', '2019-07-27 10:51:38'),
(1507, 'b_level', 'update', 'assign user role information updated', '1', 1, '219.91.253.114', '2019-07-27 10:51:47'),
(1508, 'b_level', 'update', 'assign user role information updated', '1', 1, '219.91.253.114', '2019-07-27 10:51:52'),
(1509, 'c-customer-delete', 'deleted', 'customer deleted information', '140', 140, '219.91.253.114', '2019-07-27 11:39:54'),
(1510, 'customer-save', 'insert', 'Customer information save', '140', 140, '219.91.253.114', '2019-07-27 11:41:07'),
(1511, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-27 16:49:13'),
(1512, 'company-profile-update', 'updated', 'company profile information updated', '142', 142, '219.91.253.114', '2019-07-27 16:51:34'),
(1513, 'save_order', 'inserted', 'order information inserted', '142', 142, '219.91.253.114', '2019-07-27 16:54:36'),
(1514, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '142', 142, '219.91.253.114', '2019-07-27 16:54:36'),
(1515, 'invoice_receipt', 'updated', 'order payment information updated', '142', 142, '219.91.253.114', '2019-07-27 16:54:41'),
(1516, 'make_payment', 'updated', 'order payment information updated', '142', 142, '219.91.253.114', '2019-07-27 16:54:52'),
(1517, 'b_level', 'deleted', 'Customer information deleted', '1', 1, '219.91.253.114', '2019-07-27 17:13:27'),
(1518, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '219.91.253.114', '2019-07-27 17:15:03'),
(1519, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-27 17:17:17'),
(1520, 'company-profile-update', 'updated', 'company profile information updated', '143', 143, '219.91.253.114', '2019-07-27 17:19:25'),
(1521, 'payment-gateway-update', 'updated', 'payment gateway information updated', '143', 143, '219.91.253.114', '2019-07-27 17:21:29'),
(1522, 'c_level', 'insert', 'my card information inserted', '143', 143, '219.91.253.114', '2019-07-27 17:26:27'),
(1523, 'c-cost-factor-save', 'insert', 'cost factor information save', '143', 143, '219.91.253.114', '2019-07-27 17:27:25'),
(1524, 'c-cost-factor-save', 'insert', 'cost factor information save', '143', 143, '219.91.253.114', '2019-07-27 17:33:46'),
(1525, 'c-cost-factor-save', 'insert', 'cost factor information save', '143', 143, '219.91.253.114', '2019-07-27 17:34:25'),
(1526, 'import-c-us-state-save', 'insert', 'us state information import done', '143', 143, '219.91.253.114', '2019-07-27 17:35:51'),
(1527, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 17:36:21'),
(1528, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 143, '219.91.253.114', '2019-07-27 17:36:21'),
(1529, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 17:36:58'),
(1530, 'c-customer-delete', 'deleted', 'customer deleted information', '143', 143, '219.91.253.114', '2019-07-27 17:37:15'),
(1531, 'delete_order', 'deleted', 'order information deleted', '143', 143, '219.91.253.114', '2019-07-27 17:41:00'),
(1532, 'customer-save', 'insert', 'Customer information save', '143', 143, '219.91.253.114', '2019-07-27 17:54:03'),
(1533, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:01:22'),
(1534, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:02:36'),
(1535, 'delete_order', 'deleted', 'order information deleted', '143', 143, '219.91.253.114', '2019-07-27 18:03:03'),
(1536, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:04:11'),
(1537, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:04:17'),
(1538, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-27 18:06:42'),
(1539, 'company-profile-update', 'updated', 'company profile information updated', '144', 144, '219.91.253.114', '2019-07-27 18:07:36'),
(1540, 'save_order', 'inserted', 'order information inserted', '144', 144, '219.91.253.114', '2019-07-27 18:09:13'),
(1541, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '144', 144, '219.91.253.114', '2019-07-27 18:09:14'),
(1542, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '219.91.253.114', '2019-07-27 18:09:19'),
(1543, 'customer-save', 'insert', 'Customer information save', '144', 144, '219.91.253.114', '2019-07-27 18:10:47'),
(1544, 'save_order', 'inserted', 'order information inserted', '144', 144, '219.91.253.114', '2019-07-27 18:12:01'),
(1545, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '144', 144, '219.91.253.114', '2019-07-27 18:12:01'),
(1546, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '219.91.253.114', '2019-07-27 18:12:06'),
(1547, 'make_payment', 'updated', 'order payment information updated', '144', 144, '219.91.253.114', '2019-07-27 18:12:18'),
(1548, 'delete_order', 'deleted', 'order information deleted', '143', 143, '219.91.253.114', '2019-07-27 18:14:46'),
(1549, 'customer-save', 'insert', 'Customer information save', '143', 143, '219.91.253.114', '2019-07-27 18:18:58'),
(1550, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:20:13'),
(1551, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 143, '219.91.253.114', '2019-07-27 18:20:14'),
(1552, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:20:20'),
(1553, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:20:42'),
(1554, 'b-cost-factor-save', 'insert', 'cost factor information save', '1', 1, '219.91.253.114', '2019-07-27 18:21:09'),
(1555, 'b_level', 'insert', 'cost factor information save', '1', 1, '219.91.253.114', '2019-07-27 18:21:58'),
(1556, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:26:20'),
(1557, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:26:28'),
(1558, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:28:17'),
(1559, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:28:39'),
(1560, 'delete_quotation', 'deleted', 'quatation information deleted', '143', 143, '219.91.253.114', '2019-07-27 18:29:38'),
(1561, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:31:26'),
(1562, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:35:08'),
(1563, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 143, '219.91.253.114', '2019-07-27 18:35:09'),
(1564, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:35:15'),
(1565, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:35:26'),
(1566, 'delete_order', 'deleted', 'order information deleted', '143', 143, '219.91.253.114', '2019-07-27 18:35:41'),
(1567, 'save_order', 'inserted', 'order information inserted', '143', 143, '219.91.253.114', '2019-07-27 18:37:47'),
(1568, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 143, '219.91.253.114', '2019-07-27 18:37:48'),
(1569, 'user-save', 'insert', 'Color information save', '143', 143, '219.91.253.114', '2019-07-27 18:40:55'),
(1570, 'user-save', 'insert', 'Color information save', '143', 143, '219.91.253.114', '2019-07-27 18:41:28'),
(1571, 'role-save', 'insert', 'role permission information save', '143', 143, '219.91.253.114', '2019-07-27 18:43:41'),
(1572, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:44:38'),
(1573, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:44:48'),
(1574, 'save_order', 'inserted', 'order information inserted', '145', 143, '219.91.253.114', '2019-07-27 18:45:54'),
(1575, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '145', 143, '219.91.253.114', '2019-07-27 18:45:55'),
(1576, 'role-update', 'updated', 'role permission updated done', '143', 143, '219.91.253.114', '2019-07-27 18:47:25'),
(1577, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:48:30'),
(1578, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:48:37'),
(1579, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 18:50:48'),
(1580, 'user-save', 'insert', 'Color information save', '143', 143, '219.91.253.114', '2019-07-27 18:52:15'),
(1581, 'user-save', 'insert', 'Color information save', '143', 143, '219.91.253.114', '2019-07-27 18:52:53'),
(1582, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:53:09'),
(1583, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 18:53:20'),
(1584, 'role-update', 'updated', 'role permission updated done', '143', 143, '219.91.253.114', '2019-07-27 18:57:57'),
(1585, 'role-delete', 'update', 'assign user role information updated', '143', 143, '219.91.253.114', '2019-07-27 18:58:51'),
(1586, 'role-save', 'insert', 'role permission information save', '143', 143, '219.91.253.114', '2019-07-27 19:00:32'),
(1587, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 19:00:47'),
(1588, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 19:00:55'),
(1589, 'assign-user-role-save', 'insert', 'assign user role information save', '143', 143, '219.91.253.114', '2019-07-27 19:01:09'),
(1590, 'save_order', 'inserted', 'order information inserted', '147', 143, '219.91.253.114', '2019-07-27 19:02:26'),
(1591, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '147', 143, '219.91.253.114', '2019-07-27 19:02:27'),
(1592, 'save_order', 'inserted', 'order information inserted', '148', 143, '219.91.253.114', '2019-07-27 19:03:33'),
(1593, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '148', 143, '219.91.253.114', '2019-07-27 19:03:33'),
(1594, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 19:04:07'),
(1595, 'invoice_receipt', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 19:04:30'),
(1596, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 19:10:37'),
(1597, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-27 19:10:37'),
(1598, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-28 00:24:46'),
(1599, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-28 00:24:46'),
(1600, 'b_level', 'updated', 'Tag information updated', '1', 1, '219.91.253.114', '2019-07-28 00:34:28'),
(1601, 'save_order', 'inserted', 'order information inserted', '143', 143, '117.208.11.197', '2019-07-28 11:00:19'),
(1602, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 08:48:10'),
(1603, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 08:52:23'),
(1604, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 08:54:20'),
(1605, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 08:55:48'),
(1606, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:03:39'),
(1607, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:04:11'),
(1608, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:05:13'),
(1609, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:06:42'),
(1610, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:07:12'),
(1611, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:07:25'),
(1612, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:08:11'),
(1613, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:16:31'),
(1614, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:17:00'),
(1615, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.227.171', '2019-07-29 09:23:17'),
(1616, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:27:36'),
(1617, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:29:53'),
(1618, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:32:36'),
(1619, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:33:45'),
(1620, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:46:20'),
(1621, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:52:03'),
(1622, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:52:26'),
(1623, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-29 09:53:32'),
(1624, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:54:11'),
(1625, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:55:20'),
(1626, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:56:30'),
(1627, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 09:56:51'),
(1628, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-29 09:57:55'),
(1629, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-29 09:58:03'),
(1630, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:00:54'),
(1631, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-29 10:03:02'),
(1632, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-29 10:03:09'),
(1633, 'pattern_update', 'Updated', 'pattern information updated', '1', 1, '219.91.253.114', '2019-07-29 10:03:14'),
(1634, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-29 10:03:35'),
(1635, 'pattern_save', 'insert', 'Pattern information save', '1', 1, '219.91.253.114', '2019-07-29 10:04:00'),
(1636, 'email-send', 'insert', 'custom email send', '1', 1, '162.243.33.234', '2019-07-29 10:04:37'),
(1637, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-29 10:07:54'),
(1638, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-29 10:09:27'),
(1639, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 10:19:35'),
(1640, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 143, '219.91.253.114', '2019-07-29 10:25:07'),
(1641, 'make_payment', 'updated', 'order payment information updated', '143', 143, '219.91.253.114', '2019-07-29 10:25:35'),
(1642, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 10:27:37'),
(1643, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 10:27:48'),
(1644, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 10:28:16'),
(1645, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '219.91.253.114', '2019-07-29 10:35:04'),
(1646, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:36:04'),
(1647, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:36:05'),
(1648, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:36:28'),
(1649, 'email-send', 'insert', 'custom email send', '1', 1, '123.136.187.7', '2019-07-29 10:38:06'),
(1650, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 10:44:35'),
(1651, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '219.91.253.114', '2019-07-29 10:45:31'),
(1652, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:45:56'),
(1653, 'email-send', 'insert', 'custom email send', '1', 1, '219.91.253.114', '2019-07-29 10:46:30'),
(1654, 'customer_save', 'insert', 'Customer information save', '1', 1, '219.91.253.114', '2019-07-29 10:48:45'),
(1655, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-29 10:50:46'),
(1656, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '219.91.253.114', '2019-07-29 10:51:55'),
(1657, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 10:56:21'),
(1658, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 10:56:54'),
(1659, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 10:59:18'),
(1660, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 11:00:11'),
(1661, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 11:04:00'),
(1662, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 11:08:24'),
(1663, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '123.136.187.7', '2019-07-29 11:13:14'),
(1664, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:00:22'),
(1665, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:00:47'),
(1666, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:01:05'),
(1667, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:01:17'),
(1668, 'import-color-save', 'insert', 'color csv information imported done', '1', 1, '162.243.51.250', '2019-07-31 02:05:38'),
(1669, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:10:00'),
(1670, 'b_level', 'deleted', 'Color information deleted', '1', 1, '162.243.51.250', '2019-07-31 02:10:13'),
(1671, 'b_level', 'insert', 'Color information save', '1', 1, '162.243.51.250', '2019-07-31 02:11:29'),
(1672, 'customer-save', 'insert', 'Customer information save', '143', 143, '162.243.51.250', '2019-07-31 02:17:04'),
(1673, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 03:32:45'),
(1674, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:32:59'),
(1675, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 03:33:59'),
(1676, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:34:13'),
(1677, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:46:41'),
(1678, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:47:54'),
(1679, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:48:38'),
(1680, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:48:51'),
(1681, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:48:59'),
(1682, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:49:27'),
(1683, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:50:18'),
(1684, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:52:51'),
(1685, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:53:18'),
(1686, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:53:46'),
(1687, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:54:15'),
(1688, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 03:55:16'),
(1689, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:10:09'),
(1690, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:10:45'),
(1691, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:10:53'),
(1692, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:22:19'),
(1693, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:22:38'),
(1694, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:24:18'),
(1695, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:24:35'),
(1696, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:25:21'),
(1697, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:25:34'),
(1698, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:25:48'),
(1699, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:26:01'),
(1700, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:26:13'),
(1701, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:26:53'),
(1702, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:27:08'),
(1703, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:28:43'),
(1704, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:28:51'),
(1705, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:29:16'),
(1706, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:29:27'),
(1707, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:29:43'),
(1708, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:29:59'),
(1709, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:30:22'),
(1710, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '1.22.233.8', '2019-07-31 04:30:28'),
(1711, 'c_level', 'insert', 'Test email send', '144', 144, '1.22.233.8', '2019-07-31 04:30:42'),
(1712, 'customer-save', 'insert', 'Customer information save', '144', 144, '1.22.233.8', '2019-07-31 04:32:24'),
(1713, 'c-email-send', 'insert', 'custom email send', '144', 144, '1.22.233.8', '2019-07-31 04:52:24'),
(1714, 'c-email-send', 'insert', 'custom email send', '144', 144, '1.22.233.8', '2019-07-31 04:52:43'),
(1715, 'c-email-send', 'insert', 'custom email send', '144', 144, '1.22.233.8', '2019-07-31 04:52:47'),
(1716, 'c-email-send', 'insert', 'custom email send', '144', 144, '1.22.233.8', '2019-07-31 04:53:17'),
(1717, 'update-mail-configure', 'insert', 'Email configuration save', '1', 1, '1.22.233.8', '2019-07-31 04:55:43'),
(1718, 'b_level', 'insert', 'Test email send', '1', 1, '1.22.233.8', '2019-07-31 04:56:04'),
(1719, 'b_level', 'insert', 'Test email send', '1', 1, '1.22.233.8', '2019-07-31 04:59:39'),
(1720, 'email-send', 'insert', 'custom email send', '1', 1, '1.22.233.8', '2019-07-31 05:01:50'),
(1721, 'email-send', 'insert', 'custom email send', '1', 1, '1.22.233.8', '2019-07-31 05:06:00'),
(1722, 'email-send', 'insert', 'custom email send', '1', 1, '1.22.233.8', '2019-07-31 05:06:10'),
(1723, 'email-send', 'insert', 'custom email send', '1', 1, '1.22.233.8', '2019-07-31 05:07:51'),
(1724, 'c-sms-config-save', 'insert', 'sms configuration insert', '144', 144, '123.136.187.107', '2019-07-31 12:11:07'),
(1725, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 12:14:23'),
(1726, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 12:15:23'),
(1727, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 12:16:04'),
(1728, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 12:16:41'),
(1729, 'product_save', 'insert', 'product information save', '1', 0, '219.91.253.114', '2019-07-31 12:23:43'),
(1730, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '219.91.253.114', '2019-07-31 12:23:59'),
(1731, 'customer-update', 'update', 'Customer information update', '144', 144, '123.136.187.107', '2019-07-31 12:25:29'),
(1732, 'customer-update', 'update', 'Customer information update', '144', 144, '123.136.187.107', '2019-07-31 12:25:39'),
(1733, 'save_order', 'inserted', 'order information inserted', '144', 144, '123.136.187.107', '2019-07-31 12:26:12'),
(1734, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:31:05'),
(1735, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:45:03'),
(1736, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:49:03'),
(1737, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:49:27'),
(1738, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:50:12'),
(1739, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:51:09'),
(1740, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:53:18'),
(1741, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:54:44'),
(1742, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:55:19'),
(1743, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:56:10'),
(1744, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:57:21'),
(1745, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 12:57:50'),
(1746, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 13:00:58'),
(1747, 'c-sms-config-update', 'updated', 'sms configuration updated', '144', 144, '123.136.187.107', '2019-07-31 13:01:34'),
(1748, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 13:02:30'),
(1749, 'c-mail-config-save', 'insert', 'Email configuration save', '144', 144, '123.136.187.107', '2019-07-31 13:09:16'),
(1750, 'c_level', 'insert', 'Test email send', '144', 144, '123.136.187.107', '2019-07-31 13:09:44'),
(1751, 'invoice_receipt', 'updated', 'order payment information updated', '144', 144, '123.136.187.107', '2019-07-31 13:10:44'),
(1752, 'delete_order', 'deleted', 'order information deleted', '144', 144, '123.136.187.107', '2019-07-31 13:16:04');
INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(1753, 'save_order', 'inserted', 'order information inserted', '144', 144, '123.136.187.107', '2019-07-31 13:17:47'),
(1754, 'b_level', 'deleted', 'Tag information deleted', '1', 1, '174.128.181.216', '2019-08-05 04:31:20'),
(1755, 'product_save', 'insert', 'product information save', '1', 0, '127.0.0.1', '2019-08-05 15:46:18'),
(1756, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '1', 0, '127.0.0.1', '2019-08-05 15:47:22'),
(1757, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '1', 0, '127.0.0.1', '2019-08-05 15:47:50'),
(1758, 'save_category', 'insert', 'Category information save', '150', 150, '127.0.0.1', '2019-08-05 16:36:58'),
(1759, 'pattern_save', 'insert', 'Pattern information save', '150', 150, '127.0.0.1', '2019-08-05 16:37:13'),
(1760, 'b_level', 'insert', 'Color information save', '150', 150, '127.0.0.1', '2019-08-05 16:38:07'),
(1761, 'b_level', 'updated', 'Color information updated', '150', 150, '127.0.0.1', '2019-08-05 16:39:05'),
(1762, 'product_save', 'insert', 'product information save', '150', 0, '127.0.0.1', '2019-08-05 16:43:00'),
(1763, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '150', 0, '127.0.0.1', '2019-08-05 16:43:24'),
(1764, 'save_order', 'inserted', 'order information inserted', '143', 143, '127.0.0.1', '2019-08-05 16:54:04'),
(1765, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '143', 1, '127.0.0.1', '2019-08-05 16:54:12'),
(1766, 'make_payment', 'updated', 'order payment information updated', '143', 143, '127.0.0.1', '2019-08-05 16:54:15'),
(1767, 'product_update', 'updated', 'product information updated', '150', 0, '127.0.0.1', '2019-08-06 14:31:18'),
(1768, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 14:33:25'),
(1769, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-06 14:37:32'),
(1770, 'customer_save', 'insert', 'Customer information save', '150', 150, '127.0.0.1', '2019-08-06 15:20:14'),
(1771, 'customer_save', 'insert', 'Customer information save', '150', 150, '127.0.0.1', '2019-08-06 15:20:22'),
(1772, 'b_level', 'deleted', 'Customer information deleted', '150', 150, '127.0.0.1', '2019-08-06 15:20:29'),
(1773, 'b_level', 'insert', 'Color information save', '150', 150, '127.0.0.1', '2019-08-06 15:53:28'),
(1774, 'category_update', 'updated', 'Category information updated', '150', 150, '127.0.0.1', '2019-08-06 17:19:24'),
(1775, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 18:34:25'),
(1776, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 18:34:29'),
(1777, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 18:34:33'),
(1778, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 18:35:03'),
(1779, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-06 18:35:07'),
(1780, 'product_save', 'insert', 'product information save', '150', 0, '127.0.0.1', '2019-08-06 19:06:30'),
(1781, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-06 19:09:17'),
(1782, 'delete_sqm_price_model_mapping', 'deleted', 'deleted sqm price model mapping information', '150', 0, '127.0.0.1', '2019-08-07 10:27:34'),
(1783, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '150', 0, '127.0.0.1', '2019-08-07 10:27:43'),
(1784, 'b_level', 'insert', 'Color information save', '150', 150, '127.0.0.1', '2019-08-07 10:32:22'),
(1785, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-07 11:49:28'),
(1786, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-07 11:57:07'),
(1787, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 11:59:10'),
(1788, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 12:01:10'),
(1789, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 12:01:17'),
(1790, 'company-profile-update', 'updated', 'company profile information updated', '152', 152, '127.0.0.1', '2019-08-07 12:08:45'),
(1791, 'customer-save', 'insert', 'Customer information save', '152', 152, '127.0.0.1', '2019-08-07 12:22:53'),
(1792, 'save_order', 'inserted', 'order information inserted', '152', 152, '127.0.0.1', '2019-08-07 12:23:46'),
(1793, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '152', 150, '127.0.0.1', '2019-08-07 12:24:07'),
(1794, 'make_payment', 'updated', 'order payment information updated', '152', 152, '127.0.0.1', '2019-08-07 12:24:12'),
(1795, 'save_order', 'inserted', 'order information inserted', '152', 152, '127.0.0.1', '2019-08-07 15:34:41'),
(1796, 'invoice_receipt', 'updated', 'order payment information updated', '152', 152, '127.0.0.1', '2019-08-07 15:34:55'),
(1797, 'invoice_receipt', 'synchronize', 'synchronize to BMS Decor order', '152', 150, '127.0.0.1', '2019-08-07 15:35:22'),
(1798, 'make_payment', 'updated', 'order payment information updated', '152', 152, '127.0.0.1', '2019-08-07 15:35:44'),
(1799, 'company_profile_update', 'updated', 'Company profile information updated', '150', 150, '127.0.0.1', '2019-08-07 16:14:32'),
(1800, 'my_account_update', 'insert', 'my account information save', '150', 150, '127.0.0.1', '2019-08-07 16:15:41'),
(1801, 'email-send', 'insert', 'custom email send', '150', 150, '127.0.0.1', '2019-08-07 16:22:53'),
(1802, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 16:47:07'),
(1803, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 17:57:43'),
(1804, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 17:59:27'),
(1805, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 18:00:42'),
(1806, 'company_profile_update', 'updated', 'Company profile information updated', '1', 1, '127.0.0.1', '2019-08-07 18:01:02'),
(1807, 'pattern_update', 'Updated', 'pattern information updated', '150', 150, '127.0.0.1', '2019-08-08 15:16:23'),
(1808, 'pattern_save', 'insert', 'Pattern information save', '150', 150, '127.0.0.1', '2019-08-08 15:16:35'),
(1809, 'product_update', 'updated', 'product information updated', '150', 0, '127.0.0.1', '2019-08-08 15:16:47');

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa`
--

CREATE TABLE `acc_coa` (
  `row_id` bigint(20) NOT NULL,
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa`
--

INSERT INTO `acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(1, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 135, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(2, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(3, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 135, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(4, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 135, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(5, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(6, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 135, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(7, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(8, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(9, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(10, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(11, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 135, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(12, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(13, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 135, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(14, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(15, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 135, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(16, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 135, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(17, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 135, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(18, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(19, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 135, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(20, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 135, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(21, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 135, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(22, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(23, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 135, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(24, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(25, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 135, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(26, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 135, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(27, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 135, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(28, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 135, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(29, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(30, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 135, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(31, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 135, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(32, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 135, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(33, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 135, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(34, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 135, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(35, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 135, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(36, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 135, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(37, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 135, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(38, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 135, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(39, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 135, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(40, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 135, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(41, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 135, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(42, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(43, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(44, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 135, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(45, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(46, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(47, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 135, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(48, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 135, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(49, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 135, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(50, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 135, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(51, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(52, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(53, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 135, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(54, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 135, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(55, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 135, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(56, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 135, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(58, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 136, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(59, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(60, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 136, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(61, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 136, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(62, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(63, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 136, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(64, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(65, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(66, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(67, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(68, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 136, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(69, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(70, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 136, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(71, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(72, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 136, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(73, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 136, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(74, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 136, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(75, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(76, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 136, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(77, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 136, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(78, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 136, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(79, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(80, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 136, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(81, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(82, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 136, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(83, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 136, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(84, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 136, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(85, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 136, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(86, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(87, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 136, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(88, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 136, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(89, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 136, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(90, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 136, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(91, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 136, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(92, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 136, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(93, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 136, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(94, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 136, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(95, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 136, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(96, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 136, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(97, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 136, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(98, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 136, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(99, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(100, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(101, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 136, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(102, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(103, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(104, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 136, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(105, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 136, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(106, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 136, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(107, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 136, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(108, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(109, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(110, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 136, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(111, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 136, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(112, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 136, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(113, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 136, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(119, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 137, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(120, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(121, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 137, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(122, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 137, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(123, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(124, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 137, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(125, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(126, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(127, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(128, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(129, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 137, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(130, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(131, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 137, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(132, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(133, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 137, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(134, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 137, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(135, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 137, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(136, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(137, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 137, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(138, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 137, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(139, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 137, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(140, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(141, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 137, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(142, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(143, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 137, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(144, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 137, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(145, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 137, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(146, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 137, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(147, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(148, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 137, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(149, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 137, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(150, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 137, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(151, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 137, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(152, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 137, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(153, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 137, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(154, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 137, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(155, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 137, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(156, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 137, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(157, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 137, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(158, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 137, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(159, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 137, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(160, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(161, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(162, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 137, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(163, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(164, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(165, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 137, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(166, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 137, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(167, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 137, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(168, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 137, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(169, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(170, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(171, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 137, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(172, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 137, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(173, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 137, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(174, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 137, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(175, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 138, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(176, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(177, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 138, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(178, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 138, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(179, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(180, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 138, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(181, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(182, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(183, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(184, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(185, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 138, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(186, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(187, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 138, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(188, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(189, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 138, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(190, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 138, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(191, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 138, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(192, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(193, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 138, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(194, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 138, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(195, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 138, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(196, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(197, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 138, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(198, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(199, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 138, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(200, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 138, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(201, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 138, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(202, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 138, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(203, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(204, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 138, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(205, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 138, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(206, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 138, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(207, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 138, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(208, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 138, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(209, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 138, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(210, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 138, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(211, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 138, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(212, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 138, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(213, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 138, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(214, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 138, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(215, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 138, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(216, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(217, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(218, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 138, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(219, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(220, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(221, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 138, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(222, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 138, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(223, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 138, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(224, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 138, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(225, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(226, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(227, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 138, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(228, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 138, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(229, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 138, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(230, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 138, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(231, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 139, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(232, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(233, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 139, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(234, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 139, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(235, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(236, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 139, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(237, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(238, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(239, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(240, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(241, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 139, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(242, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(243, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 139, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(244, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(245, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 139, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(246, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 139, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(247, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 139, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(248, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(249, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 139, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(250, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 139, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(251, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 139, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(252, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(253, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 139, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(254, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(255, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 139, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(256, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 139, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(257, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 139, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(258, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 139, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(259, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(260, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 139, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(261, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 139, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(262, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 139, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(263, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 139, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(264, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 139, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(265, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 139, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(266, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 139, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(267, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 139, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(268, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 139, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(269, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 139, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(270, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 139, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(271, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 139, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(272, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(273, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(274, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 139, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(275, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(276, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(277, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 139, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(278, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 139, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(279, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 139, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(280, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 139, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(281, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(282, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(283, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 139, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(284, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 139, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(285, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 139, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(286, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 139, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(288, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 140, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(289, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(290, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 140, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(291, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 140, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(292, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(293, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 140, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(294, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(295, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(296, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(297, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(298, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 140, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(299, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(300, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 140, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(301, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(302, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 140, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(303, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 140, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(304, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 140, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(305, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(306, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 140, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(307, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 140, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(308, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 140, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(309, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(310, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 140, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(311, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(312, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 140, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(313, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 140, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(314, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 140, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(315, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 140, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(316, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(317, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 140, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(318, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 140, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(319, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 140, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(320, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 140, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(321, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 140, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(322, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 140, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(323, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 140, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(324, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 140, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(325, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 140, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(326, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 140, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(327, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 140, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(328, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 140, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(329, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(330, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(331, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 140, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(332, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(333, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(334, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 140, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(335, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 140, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(336, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 140, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(337, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 140, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(338, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(339, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(340, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 140, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(341, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 140, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(342, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 140, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(343, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 140, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(347, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 142, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01');
INSERT INTO `acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(348, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(349, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 142, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(350, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 142, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(351, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(352, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 142, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(353, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(354, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(355, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(356, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(357, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 142, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(358, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(359, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 142, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(360, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(361, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 142, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(362, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 142, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(363, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 142, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(364, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(365, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 142, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(366, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 142, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(367, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 142, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(368, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(369, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 142, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(370, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(371, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 142, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(372, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 142, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(373, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 142, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(374, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 142, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(375, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(376, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 142, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(377, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 142, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(378, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 142, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(379, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 142, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(380, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 142, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(381, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 142, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(382, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 142, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(383, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 142, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(384, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 142, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(385, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 142, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(386, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 142, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(387, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 142, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(388, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(389, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(390, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 142, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(391, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(392, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(393, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 142, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(394, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 142, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(395, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 142, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(396, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 142, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(397, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(398, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(399, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 142, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(400, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 142, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(401, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 142, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(402, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 142, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(403, '102030101', 'CUS-0016-Rajiv Pastula', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '142', '2019-07-27 00:00:00', '', '0000-00-00 00:00:00'),
(404, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 143, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(405, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(406, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 143, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(407, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 143, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(408, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(409, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 143, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(410, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(411, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(412, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(413, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(414, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 143, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(415, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(416, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 143, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(417, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(418, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 143, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(419, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 143, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(420, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 143, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(421, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(422, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 143, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(423, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 143, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(424, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 143, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(425, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(426, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 143, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(427, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(428, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 143, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(429, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 143, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(430, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 143, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(431, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 143, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(432, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(433, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 143, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(434, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 143, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(435, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 143, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(436, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 143, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(437, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 143, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(438, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 143, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(439, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 143, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(440, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 143, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(441, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 143, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(442, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 143, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(443, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 143, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(444, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 143, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(445, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(446, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(447, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 143, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(448, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(449, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(450, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 143, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(451, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 143, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(452, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 143, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(453, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 143, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(454, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(455, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(456, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 143, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(457, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 143, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(458, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 143, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(459, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 143, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(461, '1020301-1', 'CUS-0021-Rajiv Pastula1', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 143, '143', '2019-07-27 00:00:00', '144', '2019-07-31 00:00:00'),
(462, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 144, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(463, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(464, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 144, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(465, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 144, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(466, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(467, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 144, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(468, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(469, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(470, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(471, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(472, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 144, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(473, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(474, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 144, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(475, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(476, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 144, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(477, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 144, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(478, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 144, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(479, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(480, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 144, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(481, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 144, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(482, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 144, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(483, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(484, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 144, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(485, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(486, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 144, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(487, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 144, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(488, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 144, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(489, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 144, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(490, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(491, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 144, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(492, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 144, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(493, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 144, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(494, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 144, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(495, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 144, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(496, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 144, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(497, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 144, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(498, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 144, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(499, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 144, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(500, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 144, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(501, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 144, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(502, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 144, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(503, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(504, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(505, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 144, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(506, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(507, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(508, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 144, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(509, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 144, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(510, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 144, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(511, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 144, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(512, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(513, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(514, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 144, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(515, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 144, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(516, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 144, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(517, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 144, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(519, '1020301-1', 'CUS-0021-Rajiv Pastula1', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 144, '144', '2019-07-27 00:00:00', '144', '2019-07-31 00:00:00'),
(520, '1020301-2', 'CUS-0022-Daniel Park', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 143, '143', '2019-07-27 00:00:00', '', '0000-00-00 00:00:00'),
(521, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 149, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(522, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(523, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 149, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(524, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 149, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(525, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(526, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 149, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(527, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(528, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(529, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(530, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(531, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 149, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(532, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(533, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 149, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(534, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(535, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 149, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(536, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 149, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(537, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 149, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(538, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(539, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 149, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(540, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 149, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(541, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 149, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(542, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(543, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 149, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(544, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(545, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 149, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(546, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 149, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(547, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 149, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(548, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 149, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(549, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(550, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 149, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(551, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 149, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(552, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 149, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(553, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 149, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(554, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 149, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(555, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 149, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(556, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 149, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(557, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 149, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(558, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 149, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(559, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 149, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(560, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 149, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(561, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 149, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(562, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(563, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(564, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 149, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(565, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(566, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(567, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 149, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(568, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 149, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(569, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 149, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(570, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 149, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(571, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(572, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(573, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 149, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(574, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 149, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(575, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 149, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(576, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 149, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(577, '1020301-3', 'CUS-0024-Rajiv Pastula', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 143, '143', '2019-07-31 00:00:00', '', '0000-00-00 00:00:00'),
(578, '1020301-2', 'CUS-0025-Moses Cameron', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 144, '144', '2019-07-31 00:00:00', '', '0000-00-00 00:00:00'),
(579, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 151, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(580, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(581, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 151, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(582, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 151, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(583, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(584, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 151, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(585, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(586, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(587, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(588, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(589, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 151, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(590, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(591, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 151, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(592, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(593, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 151, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(594, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 151, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(595, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 151, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(596, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(597, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 151, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(598, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 151, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(599, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 151, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(600, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(601, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 151, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(602, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(603, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 151, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(604, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 151, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(605, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 151, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(606, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 151, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(607, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(608, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 151, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(609, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 151, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(610, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 151, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(611, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 151, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(612, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 151, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(613, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 151, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(614, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 151, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(615, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 151, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(616, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 151, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(617, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 151, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(618, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 151, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(619, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 151, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(620, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(621, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(622, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 151, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(623, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(624, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(625, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 151, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(626, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 151, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(627, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 151, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(628, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 151, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(629, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(630, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(631, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 151, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(632, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 151, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(633, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 151, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(634, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 151, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(635, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 152, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
(636, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(637, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 152, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(638, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 152, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(639, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(640, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 152, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(641, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(642, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(643, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(644, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(645, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 152, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(646, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
(647, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 152, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
(648, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
(649, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 152, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
(650, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 152, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
(651, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 152, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
(652, '1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
(653, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 152, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
(654, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 152, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(655, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 152, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(656, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(657, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 152, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(658, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(659, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 152, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
(660, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 152, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
(661, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 152, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(662, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 152, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(663, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(664, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 152, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(665, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 152, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
(666, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 152, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
(667, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 152, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
(668, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 152, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(669, '402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 152, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
(670, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 152, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
(671, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 152, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(672, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 152, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(673, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 152, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(674, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 152, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
(675, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 152, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(676, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(677, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(678, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 152, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(679, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(680, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(681, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 152, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(682, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 152, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(683, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 152, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(684, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 152, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21');
INSERT INTO `acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(685, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(686, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(687, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 152, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
(688, '502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 152, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
(689, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 152, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(690, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 152, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53'),
(691, '1020301-1', 'CUS-0028-d_user first', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 152, '152', '2019-08-07 00:00:00', '', '0000-00-00 00:00:00'),
(692, '1020302', 'CUS-0029-new_d user', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '152', '2019-08-07 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa_default`
--

CREATE TABLE `acc_coa_default` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa_default`
--

INSERT INTO `acc_coa_default` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
('1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 0, '2', '2019-01-01 09:43:30', '', '1971-01-01 00:00:01'),
('101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
('10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
('10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
('1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
('10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
('1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
('1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
('1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
('1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
('10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
('102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2018-07-07 11:23:00'),
('10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, '48', '2019-05-22 11:43:01', 'admin', '2015-10-15 15:57:55'),
('1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '53', '2019-05-22 11:19:12', 'admin', '2016-05-23 12:05:43'),
('1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, '53', '2019-05-22 11:39:19', 'admin', '2015-10-15 15:32:42'),
('102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '48', '2019-05-22 11:42:43', 'admin', '2015-10-15 15:32:52'),
('102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '53', '2019-05-22 11:39:32', '', '1971-01-01 00:00:01'),
('1020103', 'Credit Card-TMS', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '59', '2019-05-28 05:48:01', '', '1971-01-01 00:00:01'),
('1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '48', '2019-05-22 11:46:54', '', '1971-01-01 00:00:01'),
('1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
('102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 0, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
('10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
('1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
('2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
('201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, '53', '2019-05-22 11:33:54', 'admin', '2015-10-15 19:43:51'),
('20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, '53', '2019-05-22 11:33:45', 'admin', '2015-10-15 19:45:35'),
('202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
('20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
('3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
('301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
('302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, '53', '2019-05-22 11:25:47', 'admin', '2016-09-25 11:04:09'),
('30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, '53', '2019-05-22 11:26:28', 'admin', '2016-09-25 11:04:35'),
('30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, '53', '2019-05-22 11:25:55', 'admin', '2016-09-25 11:04:19'),
('4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
('402', 'Other Expenses', 'Expence', 1, 1, 0, 1, 'E', 0, 0, '0.00', 0, '53', '2019-05-22 11:25:27', 'admin', '2015-10-15 18:37:42'),
('40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, '53', '2019-05-22 11:22:07', 'anwarul', '2013-07-17 14:21:47'),
('4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
('40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
('4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
('40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, '53', '2019-05-22 11:24:21', 'admin', '2015-10-15 19:02:51'),
('40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
('4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
('4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
('40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
('40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
('4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
('5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
('501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
('50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
('502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
('50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
('50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
('5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, '2', '2018-12-22 14:01:15', '', '1971-01-01 00:00:01'),
('502020101', 'BMS Window Decor', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 0, '53', '2019-05-22 11:20:18', '', '1971-01-01 00:00:01'),
('50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
('5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 0, '53', '2019-05-22 11:33:19', 'admin', '2016-09-28 13:23:53');

-- --------------------------------------------------------

--
-- Table structure for table `acc_customer_income`
--

CREATE TABLE `acc_customer_income` (
  `ID` int(11) NOT NULL,
  `Customer_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_glsummarybalance`
--

CREATE TABLE `acc_glsummarybalance` (
  `ID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `FYear` int(11) DEFAULT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_income_expence`
--

CREATE TABLE `acc_income_expence` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Student_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Paymode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Perpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci NOT NULL,
  `StoreID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `IsApprove` tinyint(4) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_temp`
--

CREATE TABLE `acc_temp` (
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Debit` decimal(18,2) NOT NULL,
  `Credit` decimal(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_transaction`
--

CREATE TABLE `acc_transaction` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `StoreID` int(11) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_transaction`
--

INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `StoreID`, `IsPosted`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES
(9, '1564250423QUWVC-002', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564250423QUWVC-002', '0.00', '2239.80', NULL, '1', 143, '143', '2019-07-27 18:02:36', NULL, NULL, '1'),
(10, '1564250423QUWVC-002', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564250423QUWVC-002', '2239.80', '0.00', NULL, '1', 143, '143', '2019-07-27 18:02:36', NULL, NULL, '1'),
(11, '1564250615AXH9C-002', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564250615AXH9C-002', '0.00', '824.71', NULL, '1', 143, '143', '2019-07-27 18:04:17', NULL, NULL, '1'),
(12, '1564250615AXH9C-002', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564250615AXH9C-002', '824.71', '0.00', NULL, '1', 143, '143', '2019-07-27 18:04:17', NULL, NULL, '1'),
(13, '1564251108PDU2C-004', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564251108PDU2C-004', '0.00', '376.50', NULL, '1', 144, '144', '2019-07-27 18:12:06', NULL, NULL, '1'),
(14, '1564251108PDU2C-004', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251108PDU2C-004', '376.50', '0.00', NULL, '1', 144, '144', '2019-07-27 18:12:06', NULL, NULL, '1'),
(15, '1564251121r0Esb-003', 'INV', '2019-07-27', '1020101', 'Paid for invoice #1564251121r0Esb-003', '0.00', '366.81', NULL, '1', 144, '144', '2019-07-27 18:12:18', NULL, NULL, '1'),
(16, '1564251121r0Esb-003', 'INV', '2019-07-27', '502020101', 'Amount received for invoice #1564251121r0Esb-003', '366.81', '0.00', NULL, '1', 144, '144', '2019-07-27 18:12:18', NULL, NULL, '1'),
(17, '1564251588FVQHC-005', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564251588FVQHC-005', '0.00', '600.00', NULL, '1', 143, '143', '2019-07-27 18:20:20', NULL, NULL, '1'),
(18, '1564251588FVQHC-005', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251588FVQHC-005', '600.00', '0.00', NULL, '1', 143, '143', '2019-07-27 18:20:20', NULL, NULL, '1'),
(19, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020101', 'Paid for invoice #1564251614bQbOb-004', '0.00', '655.18', NULL, '1', 143, '143', '2019-07-27 18:20:42', NULL, NULL, '1'),
(20, '1564251614bQbOb-004', 'INV', '2019-07-27', '502020101', 'Amount received for invoice #1564251614bQbOb-004', '655.18', '0.00', NULL, '1', 143, '143', '2019-07-27 18:20:42', NULL, NULL, '1'),
(21, '1564251785ARPSC-006', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564251785ARPSC-006', '0.00', '1506.19', NULL, '1', 143, '143', '2019-07-27 18:26:28', NULL, NULL, '1'),
(22, '1564251785ARPSC-006', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251785ARPSC-006', '1506.19', '0.00', NULL, '1', 143, '143', '2019-07-27 18:26:28', NULL, NULL, '1'),
(23, '1564252028E4DAC-007', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564252028E4DAC-007', '0.00', '1506.19', NULL, '1', 143, '143', '2019-07-27 18:28:39', NULL, NULL, '1'),
(24, '1564252028E4DAC-007', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564252028E4DAC-007', '1506.19', '0.00', NULL, '1', 143, '143', '2019-07-27 18:28:39', NULL, NULL, '1'),
(25, '1564252489UEBDC-007', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564252489UEBDC-007', '0.00', '324.43', NULL, '1', 143, '143', '2019-07-27 18:35:15', NULL, NULL, '1'),
(26, '1564252489UEBDC-007', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564252489UEBDC-007', '324.43', '0.00', NULL, '1', 143, '143', '2019-07-27 18:35:15', NULL, NULL, '1'),
(27, '1564252509yG6Fb-005', 'INV', '2019-07-27', '1020101', 'Paid for invoice #1564252509yG6Fb-005', '0.00', '324.43', NULL, '1', 143, '143', '2019-07-27 18:35:26', NULL, NULL, '1'),
(28, '1564252509yG6Fb-005', 'INV', '2019-07-27', '502020101', 'Amount received for invoice #1564252509yG6Fb-005', '324.43', '0.00', NULL, '1', 143, '143', '2019-07-27 18:35:26', NULL, NULL, '1'),
(29, '1564253146BWSAC-009', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564253146BWSAC-009', '0.00', '727.98', NULL, '1', 143, '143', '2019-07-27 18:50:48', NULL, NULL, '1'),
(30, '1564253146BWSAC-009', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564253146BWSAC-009', '727.98', '0.00', NULL, '1', 143, '143', '2019-07-27 18:50:48', NULL, NULL, '1'),
(31, '1564254122USGHC-010', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564254122USGHC-010', '0.00', '506.50', NULL, '1', 143, '143', '2019-07-27 19:04:07', NULL, NULL, '1'),
(32, '1564254122USGHC-010', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564254122USGHC-010', '506.50', '0.00', NULL, '1', 143, '143', '2019-07-27 19:04:07', NULL, NULL, '1'),
(33, '1564254204FUMFC-011', 'INV', '2019-07-27', '1020301-1', 'Customer 1020301-1 paid for invoice #1564254204FUMFC-011', '0.00', '457.36', NULL, '1', 143, '143', '2019-07-27 19:04:30', NULL, NULL, '1'),
(34, '1564254204FUMFC-011', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564254204FUMFC-011', '457.36', '0.00', NULL, '1', 143, '143', '2019-07-27 19:04:30', NULL, NULL, '1'),
(35, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020101', 'Paid for invoice #1564251614bQbOb-004', '0.00', '655.19', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(36, '1564251614bQbOb-004', 'INV', '2019-07-27', '502020101', 'Amount received for invoice #1564251614bQbOb-004', '655.19', '0.00', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(37, '1564252668pWO0b-006', 'INV', '2019-07-27', '1020101', 'Paid for invoice #1564252668pWO0b-006', '0.00', '1212.46', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(38, '1564252668pWO0b-006', 'INV', '2019-07-27', '502020101', 'Amount received for invoice #1564252668pWO0b-006', '1212.46', '0.00', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(39, '1564251614bQbOb-004', 'INV', '2019-07-28', '1020101', 'Paid for invoice #1564251614bQbOb-004', '0.00', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(40, '1564251614bQbOb-004', 'INV', '2019-07-28', '502020101', 'Amount received for invoice #1564251614bQbOb-004', '0.00', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(41, '1564252668pWO0b-006', 'INV', '2019-07-28', '1020101', 'Paid for invoice #1564252668pWO0b-006', '0.00', '1212.45', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(42, '1564252668pWO0b-006', 'INV', '2019-07-28', '502020101', 'Amount received for invoice #1564252668pWO0b-006', '1212.45', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(43, '1564395907fl8bb-007', 'INV', '2019-07-29', '1020101', 'Paid for invoice #1564395907fl8bb-007', '0.00', '97.86', NULL, '1', 143, '143', '2019-07-29 10:25:35', NULL, NULL, '1'),
(44, '1564395907fl8bb-007', 'INV', '2019-07-29', '502020101', 'Amount received for invoice #1564395907fl8bb-007', '97.86', '0.00', NULL, '1', 143, '143', '2019-07-29 10:25:35', NULL, NULL, '1'),
(45, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '562.50', NULL, '1', 143, '144', '2019-07-31 12:31:05', NULL, NULL, '1'),
(46, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '562.50', '0.00', NULL, '1', 143, '144', '2019-07-31 12:31:05', NULL, NULL, '1'),
(47, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '50.00', NULL, '1', 143, '144', '2019-07-31 12:45:03', NULL, NULL, '1'),
(48, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '50.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:45:03', NULL, NULL, '1'),
(49, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:49:03', NULL, NULL, '1'),
(50, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:49:03', NULL, NULL, '1'),
(51, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:49:27', NULL, NULL, '1'),
(52, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:49:27', NULL, NULL, '1'),
(53, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:50:12', NULL, NULL, '1'),
(54, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:50:12', NULL, NULL, '1'),
(55, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:51:09', NULL, NULL, '1'),
(56, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:51:09', NULL, NULL, '1'),
(57, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:53:18', NULL, NULL, '1'),
(58, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:53:18', NULL, NULL, '1'),
(59, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:54:44', NULL, NULL, '1'),
(60, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:54:44', NULL, NULL, '1'),
(61, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:55:19', NULL, NULL, '1'),
(62, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:55:19', NULL, NULL, '1'),
(63, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:56:10', NULL, NULL, '1'),
(64, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:56:10', NULL, NULL, '1'),
(65, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:57:21', NULL, NULL, '1'),
(66, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:57:21', NULL, NULL, '1'),
(67, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 12:57:50', NULL, NULL, '1'),
(68, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 12:57:50', NULL, NULL, '1'),
(69, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '10.00', NULL, '1', 143, '144', '2019-07-31 13:02:30', NULL, NULL, '1'),
(70, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '10.00', '0.00', NULL, '1', 143, '144', '2019-07-31 13:02:30', NULL, NULL, '1'),
(71, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020301-1', 'Customer 1020301-1 paid for invoice #1564575953YHUUC-013', '0.00', '20.00', NULL, '1', 143, '144', '2019-07-31 13:10:44', NULL, NULL, '1'),
(72, '1564575953YHUUC-013', 'INV', '2019-07-31', '1020101', 'Amount received for invoice #1564575953YHUUC-013', '20.00', '0.00', NULL, '1', 143, '144', '2019-07-31 13:10:44', NULL, NULL, '1'),
(73, '1565004252lYLjb-009', 'INV', '2019-08-05', '1020101', 'Paid for invoice #1565004252lYLjb-009', '0.00', '9.74', NULL, '1', 143, '143', '2019-08-05 16:54:16', NULL, NULL, '1'),
(74, '1565004252lYLjb-009', 'INV', '2019-08-05', '502020101', 'Amount received for invoice #1565004252lYLjb-009', '9.74', '0.00', NULL, '1', 143, '143', '2019-08-05 16:54:16', NULL, NULL, '1'),
(75, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Paid for invoice #156500641433GQB-002', '0.00', '1.00', NULL, '1', 0, '150', '2019-08-05 17:37:24', NULL, NULL, '1'),
(76, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Paid for invoice #156500641433GQB-002', '0.00', '1.00', NULL, '1', 0, '1', '2019-08-05 17:38:57', NULL, NULL, '1'),
(77, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Paid for invoice #156500641433GQB-002', '0.00', '18.00', NULL, '1', 0, '1', '2019-08-05 17:40:01', NULL, NULL, '1'),
(78, '1565160847vpO7b-016', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565160847vpO7b-016', '0.00', '43392.30', NULL, '1', 152, '152', '2019-08-07 12:24:12', NULL, NULL, '1'),
(79, '1565160847vpO7b-016', 'INV', '2019-08-07', '502020101', 'Amount received for invoice #1565160847vpO7b-016', '43392.30', '0.00', NULL, '1', 152, '152', '2019-08-07 12:24:12', NULL, NULL, '1'),
(80, '1565161008PG0SB-004', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565161008PG0SB-004', '0.00', '100.00', NULL, '1', 0, '1', '2019-08-07 12:28:04', NULL, NULL, '1'),
(81, '1565172271RTPFC-016', 'INV', '2019-08-07', '1020302', 'Customer 1020302 paid for invoice #1565172271RTPFC-016', '0.00', '348.09', NULL, '1', 0, '152', '2019-08-07 15:34:55', NULL, NULL, '1'),
(82, '1565172271RTPFC-016', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565172271RTPFC-016', '348.09', '0.00', NULL, '1', 0, '152', '2019-08-07 15:34:55', NULL, NULL, '1'),
(83, '1565172322hiHqb-017', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565172322hiHqb-017', '0.00', '313.28', NULL, '1', 152, '152', '2019-08-07 15:35:44', NULL, NULL, '1'),
(84, '1565172322hiHqb-017', 'INV', '2019-08-07', '502020101', 'Amount received for invoice #1565172322hiHqb-017', '313.28', '0.00', NULL, '1', 152, '152', '2019-08-07 15:35:44', NULL, NULL, '1'),
(85, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '350304915580.60', NULL, '1', 0, '1', '2019-08-07 18:13:06', NULL, NULL, '1'),
(86, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '24977000.00', NULL, '1', 0, '150', '2019-08-07 18:41:54', NULL, NULL, '1'),
(87, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '100.00', NULL, '1', 0, '150', '2019-08-07 18:46:31', NULL, NULL, '1'),
(88, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '100.00', NULL, '1', 0, '150', '2019-08-07 18:49:25', NULL, NULL, '1'),
(89, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '288720500.00', NULL, '1', 0, '150', '2019-08-07 18:54:17', NULL, NULL, '1'),
(90, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '1.00', NULL, '1', 0, '150', '2019-08-07 19:02:12', NULL, NULL, '1'),
(91, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Paid for invoice #1565180117RILXB-002', '0.00', '577401000.00', NULL, '1', 0, '150', '2019-08-07 19:03:30', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_calendar`
--

CREATE TABLE `appointment_calendar` (
  `appointment_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `c_level_id` int(11) NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `c_level_staff_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_by` int(11) NOT NULL,
  `level_from` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_tbl`
--

CREATE TABLE `attribute_tbl` (
  `attribute_id` int(11) NOT NULL,
  `attribute_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_type` tinyint(4) DEFAULT NULL COMMENT '1=text,2=option',
  `category_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_tbl`
--

INSERT INTO `attribute_tbl` (`attribute_id`, `attribute_name`, `attribute_type`, `category_id`, `position`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(1, 'Multi Blinds', 2, 1, 1, 1, 1, '2019-07-25', '2019-07-25'),
(2, 'Mount', 2, 1, 2, 1, 1, '2019-07-25', '2019-07-25'),
(3, 'Hold Down', 2, 1, 3, 1, 0, '2019-07-25', '0000-00-00'),
(4, 'Tilt Type', 2, 1, 4, 1, 0, '2019-07-25', '0000-00-00'),
(5, 'Tilt Position', 2, 1, 5, 1, 0, '2019-07-25', '0000-00-00'),
(6, 'Lift Type', 2, 1, 6, 1, 0, '2019-07-25', '0000-00-00'),
(7, 'Lift Position', 2, 1, 7, 1, 0, '2019-07-25', '0000-00-00'),
(8, 'Routless', 2, 1, 8, 1, 0, '2019-07-25', '0000-00-00'),
(9, 'Valance Size', 2, 1, 9, 1, 0, '2019-07-25', '0000-00-00'),
(10, 'Common Valance', 2, 1, 10, 1, 0, '2019-07-25', '0000-00-00'),
(11, 'Valance Clips', 2, 1, 11, 1, 0, '2019-07-25', '0000-00-00'),
(12, 'Custom Valance Returned', 2, 1, 12, 1, 1, '2019-07-25', '2019-07-25'),
(13, 'Tile Cut Outs', 2, 1, 13, 1, 0, '2019-07-25', '0000-00-00'),
(14, 'Custome Cord Length', 2, 1, 14, 1, 0, '2019-07-25', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `attr_options`
--

CREATE TABLE `attr_options` (
  `att_op_id` int(11) NOT NULL,
  `option_name` text NOT NULL,
  `option_type` tinyint(4) DEFAULT NULL COMMENT '1=text,2=option',
  `attribute_id` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `price_type` tinyint(4) DEFAULT NULL COMMENT '1=$,2=%',
  `op_condition` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_options`
--

INSERT INTO `attr_options` (`att_op_id`, `option_name`, `option_type`, `attribute_id`, `price`, `price_type`, `op_condition`) VALUES
(1, 'No', 0, 1, 0, 1, ''),
(2, '2 on One', 3, 1, 5, 2, ''),
(3, '3 on One', 3, 1, 10, 2, ''),
(4, 'Inside Mount(IB)', 0, 2, 0, 1, '-1/2'),
(5, 'Outside Mount(OB)', 0, 2, 0, 1, ''),
(6, 'Yes', 0, 3, 0, 1, ''),
(7, 'No', 0, 3, 0, 1, ''),
(8, 'Cord Tilter', 0, 4, 0, 1, ''),
(9, 'Wand Tilter', 0, 4, 0, 1, ''),
(10, 'Left', 0, 5, 0, 1, ''),
(11, 'Right', 0, 5, 0, 1, ''),
(12, 'Cord Lift', 0, 6, 0, 1, ''),
(13, 'Cordless Lift', 0, 6, 40, 1, ''),
(14, 'Right', 0, 7, 0, 1, ''),
(15, 'Left', 0, 7, 0, 1, ''),
(16, 'No', 0, 8, 0, 1, ''),
(17, 'Yes', 0, 8, 20, 2, ''),
(18, 'Default', 0, 9, 0, 1, '-1/8'),
(19, 'Entry', 1, 9, 0, 1, ''),
(20, 'No', 0, 10, 0, 1, ''),
(21, 'Yes', 1, 10, 0, 1, ''),
(22, 'Standard -C Clip', 0, 11, 0, 1, ''),
(23, 'High Position Hidden', 0, 11, 0, 1, ''),
(24, 'Low Position Hidden', 0, 11, 0, 1, ''),
(25, 'No Returns-IB', 0, 12, 0, 1, ''),
(26, '1/2', 0, 12, 4, 1, '1'),
(27, 'Custom Size returns', 0, 12, 4, 1, ''),
(28, 'No', 0, 13, 0, 1, ''),
(29, 'Bottom Left Cutout', 5, 13, 0, 1, '-1/8'),
(30, 'Bottom Right Cutout', 5, 13, 0, 1, '-1/8'),
(31, 'Both Bottom Cutout', 5, 13, 0, 1, '-1/4'),
(32, 'No', 0, 14, 0, 1, ''),
(33, 'Custom Cord Length', 1, 14, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `attr_options_option_option_tbl`
--

CREATE TABLE `attr_options_option_option_tbl` (
  `att_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_name` text NOT NULL,
  `att_op_op_op_type` int(11) NOT NULL,
  `att_op_op_id` int(11) NOT NULL,
  `op_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `att_op_op_op_price_type` varchar(4) DEFAULT NULL,
  `att_op_op_op_price` float DEFAULT NULL,
  `att_op_op_op_condition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attr_options_option_tbl`
--

CREATE TABLE `attr_options_option_tbl` (
  `op_op_id` int(11) NOT NULL,
  `op_op_name` text NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=text,2=option',
  `att_op_op_price_type` varchar(5) DEFAULT NULL,
  `att_op_op_price` float DEFAULT NULL,
  `att_op_op_condition` text NOT NULL,
  `option_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_options_option_tbl`
--

INSERT INTO `attr_options_option_tbl` (`op_op_id`, `op_op_name`, `type`, `att_op_op_price_type`, `att_op_op_price`, `att_op_op_condition`, `option_id`, `attribute_id`) VALUES
(1, 'Left', 1, '1', 0, '-3/8', 2, 1),
(2, 'Right', 1, '1', 0, '-1/4', 2, 1),
(3, 'Left', 0, '1', 0, '-1/4', 3, 1),
(4, 'Center', 0, '1', 0, '-1/4', 3, 1),
(5, 'Right', 0, '1', 0, '-1/4', 3, 1),
(6, 'Width-Tile to wall', 0, '1', 0, '', 29, 13),
(7, 'Height-tile height ', 0, '1', 0, '', 29, 13),
(8, 'Width-Tile to wall', 1, '1', 0, '', 30, 13),
(9, 'Height-tile height ', 1, '1', 0, '', 30, 13),
(10, 'Width-Tile to wall', 1, '1', 0, '', 31, 13),
(11, 'Height-tile height ', 1, '1', 0, '', 31, 13);

-- --------------------------------------------------------

--
-- Table structure for table `attr_op_op_op_op_tbl`
--

CREATE TABLE `attr_op_op_op_op_tbl` (
  `att_op_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_op_name` text NOT NULL,
  `att_op_op_op_op_type` int(11) NOT NULL,
  `att_op_op_op_op_price_type` int(11) NOT NULL,
  `att_op_op_op_op_price` float NOT NULL,
  `att_op_op_op_op_condition` text NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_id` int(11) NOT NULL,
  `op_op_id` int(11) NOT NULL,
  `op_op_op_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_acc_coa`
--

CREATE TABLE `b_acc_coa` (
  `row_id` int(11) NOT NULL,
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_acc_coa`
--

INSERT INTO `b_acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(1, '4021403', 'AC', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:33:55', '', '1971-01-01 00:00:01'),
(2, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:43', '', '1971-01-01 00:00:01'),
(3, '10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2013-09-18 15:29:35'),
(4, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
(5, '102020103', 'Advance House Rent', 'Advance', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-10-02 16:55:38', 'admin', '2016-10-02 16:57:32'),
(6, '10202', 'Advance, Deposit And Pre-payments', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2015-12-31 16:46:24'),
(7, '4020602', 'Advertisement and Publicity', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:51:44', '', '1971-01-01 00:00:01'),
(8, '1010410', 'Air Cooler', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-05-23 12:13:55', '', '1971-01-01 00:00:01'),
(9, '4020603', 'AIT Against Advertisement', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:52:09', '', '1971-01-01 00:00:01'),
(10, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '2018-12-22 13:40:24', '', '1971-01-01 00:00:01'),
(11, '1010204', 'Attendance Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:49:31', '', '1971-01-01 00:00:01'),
(12, '40216', 'Audit Fee', 'Other Expenses', 2, 0, 0, 0, 'E', 0, 0, '0.00', 0, '1', '2019-04-19 01:39:11', '', '1971-01-01 00:00:01'),
(13, '4021002', 'Bank Charge', 'Financial Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:21:03', '', '1971-01-01 00:00:01'),
(14, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, '1', '2019-05-22 11:55:29', 'admin', '2016-09-25 11:04:19'),
(15, '102010202', 'Bank Of America', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:50:32', 'admin', '2015-10-15 15:32:52'),
(16, '1010104', 'Book Shelf', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:46:11', '', '1971-01-01 00:00:01'),
(17, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:45:37', '', '1971-01-01 00:00:01'),
(18, '10201020301', 'Branch 1', 'Standard Bank', 5, 1, 1, 1, 'A', 0, 0, '0.00', 0, '2', '2018-07-19 13:44:33', '', '1971-01-01 00:00:01'),
(19, '4020604', 'Business Development Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:52:29', '', '1971-01-01 00:00:01'),
(20, '4020606', 'Campaign Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:52:57', 'admin', '2016-09-19 14:52:48'),
(21, '4020502', 'Campus Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:46:53', 'admin', '2017-04-27 17:02:39'),
(22, '1020103', 'Capital One Master Card', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:49:42', '', '1971-01-01 00:00:01'),
(23, '40212', 'Car Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:28:43', '', '1971-01-01 00:00:01'),
(24, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, '1', '2019-05-04 07:07:56', 'admin', '2015-10-15 15:57:55'),
(25, '1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 0, 'A', 0, 0, '0.00', 0, '2', '2018-07-19 13:43:59', 'admin', '2015-10-15 15:32:42'),
(26, '1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-21 12:33:17', 'admin', '2016-05-23 12:05:43'),
(27, '30101', 'Cash Sale', 'Store Income', 1, 1, 1, 0, 'I', 0, 0, '0.00', 0, '2', '2018-07-08 07:51:26', '', '1971-01-01 00:00:01'),
(28, '1010207', 'CCTV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:51:24', '', '1971-01-01 00:00:01'),
(29, '102020102', 'CEO Current A/C', 'Advance', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-09-25 11:54:54', '', '1971-01-01 00:00:01'),
(30, '1010101', 'Class Room Chair', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:45:29', '', '1971-01-01 00:00:01'),
(31, '4021407', 'Close Circuit Cemera', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:35:35', '', '1971-01-01 00:00:01'),
(32, '4020601', 'Commision on Admission', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:51:21', 'admin', '2016-09-19 14:42:54'),
(33, '1010206', 'Computer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:51:09', '', '1971-01-01 00:00:01'),
(34, '4021410', 'Computer (R)', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-03-24 12:38:52', 'Zoherul', '2016-03-24 12:41:40'),
(35, '1010102', 'Computer Table', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:45:44', '', '1971-01-01 00:00:01'),
(36, '301020401', 'Continuing Registration fee - UoL (Income)', 'Registration Fee (UOL) Income', 4, 1, 1, 0, 'I', 0, 0, '0.00', 0, 'admin', '2015-10-15 17:40:40', '', '1971-01-01 00:00:01'),
(37, '4020904', 'Contratuall Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:12:34', '', '1971-01-01 00:00:01'),
(38, '403', 'Cost of Sale', 'Expence', 0, 1, 1, 0, 'E', 0, 0, '0.00', 0, '2', '2018-07-08 10:37:16', '', '1971-01-01 00:00:01'),
(39, '30102', 'Credit Sale', 'Store Income', 1, 1, 1, 0, 'I', 0, 0, '0.00', 0, '2', '2018-07-08 07:51:34', '', '1971-01-01 00:00:01'),
(40, '4020709', 'Cultural Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'nasmud', '2017-04-29 12:45:10', '', '1971-01-01 00:00:01'),
(41, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '2019-01-01 09:50:28', 'admin', '2018-07-07 11:23:00'),
(42, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(48, '1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, '', '2018-12-22 13:58:22', 'admin', '2018-07-07 12:31:42'),
(49, '40100002', 'cw-Chichawatni', 'Store Expenses', 2, 0, 0, 0, 'E', 0, 0, '0.00', 0, '1', '2019-07-24 17:21:50', '', '1971-01-01 00:00:01'),
(50, '1020202', 'Deposit', 'Advance, Deposit And Pre-payments', 3, 1, 0, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:40:42', '', '1971-01-01 00:00:01'),
(51, '4020605', 'Design & Printing Expense', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:55:00', '', '1971-01-01 00:00:01'),
(52, '4020404', 'Dish Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:58:21', '', '1971-01-01 00:00:01'),
(53, '40215', 'Dividend', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 0, 'admin', '2016-09-25 14:07:55', '', '1971-01-01 00:00:01'),
(54, '4020403', 'Drinking Water Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:58:10', '', '1971-01-01 00:00:01'),
(55, '1010211', 'DSLR Camera', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:53:17', 'admin', '2016-01-02 16:23:25'),
(56, '4020908', 'Earned Leave', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:38', '', '1971-01-01 00:00:01'),
(57, '4020607', 'Education Fair Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:53:42', '', '1971-01-01 00:00:01'),
(58, '1010602', 'Electric Equipment', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:44:51', '', '1971-01-01 00:00:01'),
(59, '1010203', 'Electric Kettle', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:49:07', '', '1971-01-01 00:00:01'),
(60, '10106', 'Electrical Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:43:44', '', '1971-01-01 00:00:01'),
(61, '4020407', 'Electricity Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:59:31', '', '1971-01-01 00:00:01'),
(62, '10202010501', 'employ', 'Salary', 5, 1, 0, 0, 'A', 0, 0, '0.00', 0, 'admin', '2018-07-05 11:47:10', '', '1971-01-01 00:00:01'),
(63, '5020202', 'Employee', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, '', '2019-01-10 07:48:45', '', '1971-01-01 00:00:01'),
(64, '40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 0, 'admin', '2013-07-08 16:21:26', 'anwarul', '2013-07-17 14:21:47'),
(65, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(66, '4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(67, '4020903', 'Faculty,Staff Salary & Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:12:21', '', '1971-01-01 00:00:01'),
(68, '4021404', 'Fax Machine', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:34:15', '', '1971-01-01 00:00:01'),
(69, '4020905', 'Festival & Incentive Bonus', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:12:48', '', '1971-01-01 00:00:01'),
(70, '1010103', 'File Cabinet', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:46:02', '', '1971-01-01 00:00:01'),
(71, '40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
(72, '1010403', 'Fire Extingushier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:39:32', '', '1971-01-01 00:00:01'),
(73, '4021408', 'Furniture', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:35:47', '', '1971-01-01 00:00:01'),
(74, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
(75, '4020406', 'Gas Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:59:20', '', '1971-01-01 00:00:01'),
(76, '20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
(77, '10105', 'Generator', 'Non Current Assets', 2, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:52:55', 'admin', '2016-05-23 12:05:18'),
(78, '4021414', 'Generator Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-06-16 10:21:05', '', '1971-01-01 00:00:01'),
(79, '40213', 'Generator Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:29:29', '', '1971-01-01 00:00:01'),
(80, '10103', 'Groceries and Cutleries', 'Non Current Assets', 2, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:53:10', '', '1971-01-01 00:00:01'),
(81, '1010408', 'Gym Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:46:03', '', '1971-01-01 00:00:01'),
(82, '4020907', 'Honorarium', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:26', '', '1971-01-01 00:00:01'),
(83, '40205', 'House Rent', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-08-24 10:26:56', '', '1971-01-01 00:00:01'),
(84, '40100001', 'HP-Hasilpur', 'Academic Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, '2', '2018-07-29 03:44:23', '', '1971-01-01 00:00:01'),
(85, '4020702', 'HR Recruitment Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-09-25 12:55:49', '', '1971-01-01 00:00:01'),
(86, '4020703', 'Incentive on Admission', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-09-25 12:56:09', '', '1971-01-01 00:00:01'),
(87, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', '', '1971-01-01 00:00:01'),
(88, '30204', 'Income from Photocopy & Printing', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, '1', '2019-05-22 11:55:43', 'admin', '2016-09-25 11:04:28'),
(89, '5020302', 'Income Tax Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-19 11:18:17', 'admin', '2016-09-28 13:18:35'),
(90, '102020302', 'Insurance Premium', 'Prepayment', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-09-19 13:10:57', '', '1971-01-01 00:00:01'),
(91, '4021001', 'Interest on Loan', 'Financial Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:20:53', 'admin', '2016-09-19 14:53:34'),
(92, '4020401', 'Internet Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:56:55', 'admin', '2015-10-15 18:57:32'),
(93, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '2', '2018-07-07 15:21:58', '', '1971-01-01 00:00:01'),
(94, '10205010101', 'Jahangir', 'Hasan', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '2', '2018-07-07 10:40:56', '', '1971-01-01 00:00:01'),
(95, '1010210', 'LCD TV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:52:27', '', '1971-01-01 00:00:01'),
(96, '30103', 'Lease Sale', 'Store Income', 1, 1, 1, 0, 'I', 0, 0, '0.00', 0, '2', '2018-07-08 07:51:52', '', '1971-01-01 00:00:01'),
(97, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
(98, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:59', '', '1971-01-01 00:00:01'),
(99, '4020707', 'Library Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2017-01-10 15:34:54', '', '1971-01-01 00:00:01'),
(100, '4021409', 'Lift', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:36:12', '', '1971-01-01 00:00:01'),
(101, '50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
(102, '4020608', 'Marketing & Promotion Exp.', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:53:59', '', '1971-01-01 00:00:01'),
(103, '4020901', 'Medical Allowance', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:11:33', '', '1971-01-01 00:00:01'),
(104, '1010411', 'Metal Ditector', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'Zoherul', '2016-08-22 10:55:22', '', '1971-01-01 00:00:01'),
(105, '4021413', 'Micro Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-05-12 14:53:51', '', '1971-01-01 00:00:01'),
(106, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, 'anwarul', '2014-02-06 15:26:31', 'admin', '2016-09-25 11:04:35'),
(107, '4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:53', '', '1971-01-01 00:00:01'),
(108, '4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-09-25 12:54:39', '', '1971-01-01 00:00:01'),
(109, '40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
(110, '1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-29 10:43:30', '', '1971-01-01 00:00:01'),
(111, '1010212', 'Network Accessories', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-02 16:23:32', '', '1971-01-01 00:00:01'),
(112, '4020408', 'News Paper Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-01-02 15:55:57', '', '1971-01-01 00:00:01'),
(113, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, '', '1971-01-01 00:00:01', 'admin', '2015-10-15 15:29:11'),
(114, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
(115, '1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:40:02', '', '1971-01-01 00:00:01'),
(116, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
(117, '4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:33:15', '', '1971-01-01 00:00:01'),
(118, '30201', 'Office Stationary (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, 'anwarul', '2013-07-17 15:21:06', 'admin', '2016-09-25 11:04:50'),
(119, '402', 'Other Expenses', 'Expence', 1, 1, 0, 0, 'E', 0, 0, '0.00', 0, '2', '2018-07-07 14:00:16', 'admin', '2015-10-15 18:37:42'),
(120, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, '1', '2019-05-22 11:55:15', 'admin', '2016-09-25 11:04:09'),
(121, '40211', 'Others (Non Academic Expenses)', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'Obaidul', '2014-12-03 16:05:42', 'admin', '2015-10-15 19:22:09'),
(122, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-29 10:43:16', '', '1971-01-01 00:00:01'),
(123, '4020910', 'Outstanding Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-04-24 11:56:50', '', '1971-01-01 00:00:01'),
(124, '4021405', 'Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:34:31', '', '1971-01-01 00:00:01'),
(125, '4021412', 'PABX-Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-04-24 14:40:18', '', '1971-01-01 00:00:01'),
(126, '4020902', 'Part-time Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:12:06', '', '1971-01-01 00:00:01'),
(127, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:50:11', '', '1971-01-01 00:00:01'),
(128, '1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
(129, '4021411', 'Photocopy Machine Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'Zoherul', '2016-04-24 12:40:02', 'admin', '2017-04-27 17:03:17'),
(130, '1020203', 'Prepayment', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:40:51', 'admin', '2015-12-31 16:49:58'),
(131, '1010201', 'Printer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:47:15', '', '1971-01-01 00:00:01'),
(132, '40202', 'Printing and Stationary', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 0, 'admin', '2013-07-08 16:21:45', 'admin', '2016-09-19 14:39:32'),
(133, '1010208', 'Projector', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:51:44', '', '1971-01-01 00:00:01'),
(134, '40206', 'Promonational Expence', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-07-11 13:48:57', 'anwarul', '2013-07-17 14:23:03'),
(135, '40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:32:46', '', '1971-01-01 00:00:01'),
(136, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
(137, '20102', 'Retained Earnings', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, '1', '2019-05-22 11:54:13', 'admin', '2016-09-25 14:05:06'),
(138, '4020708', 'River Cruse', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2017-04-24 15:35:25', '', '1971-01-01 00:00:01'),
(139, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 0, 'admin', '2018-07-05 11:46:44', '', '1971-01-01 00:00:01'),
(140, '40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-12-12 11:22:58', '', '1971-01-01 00:00:01'),
(141, '404', 'Sale Discount', 'Expence', 1, 1, 1, 0, 'E', 0, 0, '0.00', 0, '2', '2018-07-19 10:15:11', '', '1971-01-01 00:00:01'),
(142, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:41:30', '', '1971-01-01 00:00:01'),
(143, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, '1', '2019-05-22 11:54:03', 'admin', '2015-10-15 19:45:35'),
(144, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, '1', '2019-05-22 11:54:20', 'admin', '2015-10-15 19:43:51'),
(145, '50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:50:30', '', '1971-01-01 00:00:01'),
(146, '40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-11-21 14:13:01', 'admin', '2015-10-15 19:02:51'),
(147, '4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:13:13', '', '1971-01-01 00:00:01'),
(148, '50102', 'Sponsors Loan', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:48:02', '', '1971-01-01 00:00:01'),
(149, '4020706', 'Sports Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'nasmud', '2016-11-09 13:16:53', '', '1971-01-01 00:00:01'),
(150, '102010204', 'State Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-05-22 11:51:03', '', '1971-01-01 00:00:01'),
(151, '401', 'Store Expenses', 'Expence', 1, 1, 0, 0, 'E', 0, 0, '0.00', 0, '2', '2018-07-07 13:38:59', 'admin', '2015-10-15 17:58:46'),
(152, '301', 'Store Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
(153, '1010601', 'Sub Station', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:44:11', '', '1971-01-01 00:00:01'),
(154, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, '1', '2019-05-21 12:29:57', '', '1971-01-01 00:00:01'),
(155, '4020704', 'TB Care Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-10-08 13:03:04', '', '1971-01-01 00:00:01'),
(156, '30206', 'TB Care Income', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, 'admin', '2016-10-08 13:00:56', '', '1971-01-01 00:00:01'),
(157, '4020501', 'TDS on House Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:44:07', 'admin', '2016-09-19 14:40:16'),
(158, '502030201', 'TDS Payable House Rent', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-19 11:19:42', 'admin', '2016-09-28 13:19:37'),
(159, '502030203', 'TDS Payable on Advertisement Bill', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-28 13:20:51', '', '1971-01-01 00:00:01'),
(160, '502030202', 'TDS Payable on Salary', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 0, 'admin', '2016-09-28 13:20:17', '', '1971-01-01 00:00:01'),
(161, '4021402', 'Tea Kettle', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:33:45', '', '1971-01-01 00:00:01'),
(162, '4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:57:59', '', '1971-01-01 00:00:01'),
(163, '1010209', 'Telephone Set & PABX', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:51:57', 'admin', '2016-10-02 17:10:40'),
(164, '40203', 'Travelling & Conveyance', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 0, 'admin', '2013-07-08 16:22:06', 'admin', '2015-10-15 18:45:13'),
(165, '4021406', 'TV', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:35:07', '', '1971-01-01 00:00:01'),
(166, '1010205', 'UPS', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:50:38', '', '1971-01-01 00:00:01'),
(167, '40204', 'Utility Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, 'anwarul', '2013-07-11 16:20:24', 'admin', '2016-01-02 15:55:22'),
(168, '4020503', 'VAT on House Rent Exp', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:49:22', 'admin', '2016-09-25 14:00:52'),
(169, '5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, 'admin', '2015-10-15 19:51:11', 'admin', '2016-09-28 13:23:53'),
(170, '1010409', 'Vehicle A/C', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'Zoherul', '2016-05-12 12:13:21', '', '1971-01-01 00:00:01'),
(171, '1010405', 'Voltage Stablizer', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-03-27 10:40:59', '', '1971-01-01 00:00:01'),
(172, '1010105', 'Waiting Sofa - Steel', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2015-10-15 15:46:29', '', '1971-01-01 00:00:01'),
(173, '4020405', 'WASA Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2015-10-15 18:58:51', '', '1971-01-01 00:00:01'),
(174, '1010402', 'Water Purifier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, 'admin', '2016-01-29 11:14:11', '', '1971-01-01 00:00:01'),
(175, '4020705', 'Website Development Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, 'admin', '2016-10-15 12:42:47', '', '1971-01-01 00:00:01'),
(177, '1020301-2', 'CUS-0017-James Park', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-07-27 00:00:00', '', '0000-00-00 00:00:00'),
(178, '1020301-3', 'CUS-0019-Rajiv Pastula', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-07-27 00:00:00', '', '0000-00-00 00:00:00'),
(179, '1020301-4', 'CUS-0023-Rajiv Pastula', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '1', '2019-07-29 00:00:00', '', '0000-00-00 00:00:00'),
(180, '1020301-5', 'CUS-0026-first customer', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '150', '2019-08-06 00:00:00', '', '0000-00-00 00:00:00'),
(181, '1020301-6', 'CUS-0027-first customer', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '150', '2019-08-06 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `b_acc_transaction`
--

CREATE TABLE `b_acc_transaction` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `StoreID` int(11) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_acc_transaction`
--

INSERT INTO `b_acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `StoreID`, `IsPosted`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES
(3, '1564251121r0Esb-003', 'INV', '2019-07-27', '1020301-3', 'Customer 1020301-3 paid for invoice #1564251121r0Esb-003', '0.00', '366.81', NULL, '1', 144, '144', '2019-07-27 18:12:18', NULL, NULL, '1'),
(4, '1564251121r0Esb-003', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251121r0Esb-003', '366.81', '0.00', NULL, '1', 144, '144', '2019-07-27 18:12:18', NULL, NULL, '1'),
(5, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564251614bQbOb-004', '0.00', '655.18', NULL, '1', 143, '143', '2019-07-27 18:20:42', NULL, NULL, '1'),
(6, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251614bQbOb-004', '655.18', '0.00', NULL, '1', 143, '143', '2019-07-27 18:20:42', NULL, NULL, '1'),
(7, '1564252509yG6Fb-005', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564252509yG6Fb-005', '0.00', '324.43', NULL, '1', 143, '143', '2019-07-27 18:35:26', NULL, NULL, '1'),
(8, '1564252509yG6Fb-005', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564252509yG6Fb-005', '324.43', '0.00', NULL, '1', 143, '143', '2019-07-27 18:35:26', NULL, NULL, '1'),
(9, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564251614bQbOb-004', '0.00', '655.19', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(10, '1564251614bQbOb-004', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564251614bQbOb-004', '655.19', '0.00', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(11, '1564252668pWO0b-006', 'INV', '2019-07-27', '1020301-2', 'Customer 1020301-2 paid for invoice #1564252668pWO0b-006', '0.00', '1212.46', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(12, '1564252668pWO0b-006', 'INV', '2019-07-27', '1020101', 'Amount received for invoice #1564252668pWO0b-006', '1212.46', '0.00', NULL, '1', 143, '143', '2019-07-27 19:10:37', NULL, NULL, '1'),
(13, '1564251614bQbOb-004', 'INV', '2019-07-28', '1020301-2', 'Customer 1020301-2 paid for invoice #1564251614bQbOb-004', '0.00', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(14, '1564251614bQbOb-004', 'INV', '2019-07-28', '1020101', 'Amount received for invoice #1564251614bQbOb-004', '0.00', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(15, '1564252668pWO0b-006', 'INV', '2019-07-28', '1020301-2', 'Customer 1020301-2 paid for invoice #1564252668pWO0b-006', '0.00', '1212.45', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(16, '1564252668pWO0b-006', 'INV', '2019-07-28', '1020101', 'Amount received for invoice #1564252668pWO0b-006', '1212.45', '0.00', NULL, '1', 143, '143', '2019-07-28 00:24:46', NULL, NULL, '1'),
(17, '1564395907fl8bb-007', 'INV', '2019-07-29', '1020301-2', 'Customer 1020301-2 paid for invoice #1564395907fl8bb-007', '0.00', '97.86', NULL, '1', 143, '143', '2019-07-29 10:25:35', NULL, NULL, '1'),
(18, '1564395907fl8bb-007', 'INV', '2019-07-29', '1020101', 'Amount received for invoice #1564395907fl8bb-007', '97.86', '0.00', NULL, '1', 143, '143', '2019-07-29 10:25:35', NULL, NULL, '1'),
(19, '1565004252lYLjb-009', 'INV', '2019-08-05', '1020301-2', 'Customer 1020301-2 paid for invoice #1565004252lYLjb-009', '0.00', '9.74', NULL, '1', 143, '143', '2019-08-05 16:54:16', NULL, NULL, '1'),
(20, '1565004252lYLjb-009', 'INV', '2019-08-05', '1020101', 'Amount received for invoice #1565004252lYLjb-009', '9.74', '0.00', NULL, '1', 143, '143', '2019-08-05 16:54:16', NULL, NULL, '1'),
(21, '156500641433GQB-002', 'INV', '2019-08-05', '4021403', 'Customer 4021403 paid for invoice #156500641433GQB-002', '0.00', '1.00', NULL, '1', 0, '150', '2019-08-05 17:37:24', NULL, NULL, '1'),
(22, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Amount received for invoice #156500641433GQB-002', '1.00', '0.00', NULL, '1', 0, '150', '2019-08-05 17:37:24', NULL, NULL, '1'),
(23, '156500641433GQB-002', 'INV', '2019-08-05', '4021403', 'Customer 4021403 paid for invoice #156500641433GQB-002', '0.00', '1.00', NULL, '1', 0, '1', '2019-08-05 17:38:57', NULL, NULL, '1'),
(24, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Amount received for invoice #156500641433GQB-002', '1.00', '0.00', NULL, '1', 0, '1', '2019-08-05 17:38:57', NULL, NULL, '1'),
(25, '156500641433GQB-002', 'INV', '2019-08-05', '4021403', 'Customer 4021403 paid for invoice #156500641433GQB-002', '0.00', '18.00', NULL, '1', 0, '1', '2019-08-05 17:40:01', NULL, NULL, '1'),
(26, '156500641433GQB-002', 'INV', '2019-08-05', '1020101', 'Amount received for invoice #156500641433GQB-002', '18.00', '0.00', NULL, '1', 0, '1', '2019-08-05 17:40:01', NULL, NULL, '1'),
(27, '1565160847vpO7b-016', 'INV', '2019-08-07', '1020301-6', 'Customer 1020301-6 paid for invoice #1565160847vpO7b-016', '0.00', '43392.30', NULL, '1', 152, '152', '2019-08-07 12:24:12', NULL, NULL, '1'),
(28, '1565160847vpO7b-016', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565160847vpO7b-016', '43392.30', '0.00', NULL, '1', 152, '152', '2019-08-07 12:24:12', NULL, NULL, '1'),
(29, '1565161008PG0SB-004', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565161008PG0SB-004', '0.00', '100.00', NULL, '1', 0, '1', '2019-08-07 12:28:04', NULL, NULL, '1'),
(30, '1565161008PG0SB-004', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565161008PG0SB-004', '100.00', '0.00', NULL, '1', 0, '1', '2019-08-07 12:28:04', NULL, NULL, '1'),
(31, '1565172322hiHqb-017', 'INV', '2019-08-07', '1020301-6', 'Customer 1020301-6 paid for invoice #1565172322hiHqb-017', '0.00', '313.28', NULL, '1', 152, '152', '2019-08-07 15:35:44', NULL, NULL, '1'),
(32, '1565172322hiHqb-017', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565172322hiHqb-017', '313.28', '0.00', NULL, '1', 152, '152', '2019-08-07 15:35:44', NULL, NULL, '1'),
(33, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '350304915580.60', NULL, '1', 0, '1', '2019-08-07 18:13:06', NULL, NULL, '1'),
(34, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '350304915580.60', '0.00', NULL, '1', 0, '1', '2019-08-07 18:13:06', NULL, NULL, '1'),
(35, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '24977000.00', NULL, '1', 0, '150', '2019-08-07 18:41:54', NULL, NULL, '1'),
(36, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '24977000.00', '0.00', NULL, '1', 0, '150', '2019-08-07 18:41:54', NULL, NULL, '1'),
(37, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '100.00', NULL, '1', 0, '150', '2019-08-07 18:46:31', NULL, NULL, '1'),
(38, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '100.00', '0.00', NULL, '1', 0, '150', '2019-08-07 18:46:31', NULL, NULL, '1'),
(39, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '100.00', NULL, '1', 0, '150', '2019-08-07 18:49:25', NULL, NULL, '1'),
(40, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '100.00', '0.00', NULL, '1', 0, '150', '2019-08-07 18:49:25', NULL, NULL, '1'),
(41, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '288720500.00', NULL, '1', 0, '150', '2019-08-07 18:54:17', NULL, NULL, '1'),
(42, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '288720500.00', '0.00', NULL, '1', 0, '150', '2019-08-07 18:54:17', NULL, NULL, '1'),
(43, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '1.00', NULL, '1', 0, '150', '2019-08-07 19:02:12', NULL, NULL, '1'),
(44, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '1.00', '0.00', NULL, '1', 0, '150', '2019-08-07 19:02:12', NULL, NULL, '1'),
(45, '1565180117RILXB-002', 'INV', '2019-08-07', '4021403', 'Customer 4021403 paid for invoice #1565180117RILXB-002', '0.00', '577401000.00', NULL, '1', 0, '150', '2019-08-07 19:03:30', NULL, NULL, '1'),
(46, '1565180117RILXB-002', 'INV', '2019-08-07', '1020101', 'Amount received for invoice #1565180117RILXB-002', '577401000.00', '0.00', NULL, '1', 0, '150', '2019-08-07 19:03:30', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `b_cost_factor_tbl`
--

CREATE TABLE `b_cost_factor_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `dealer_cost_factor` float NOT NULL,
  `individual_cost_factor` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_cost_factor_tbl`
--

INSERT INTO `b_cost_factor_tbl` (`id`, `customer_id`, `product_id`, `dealer_cost_factor`, `individual_cost_factor`, `created_by`, `create_date`, `updated_by`, `update_date`) VALUES
(1, 21, 1, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(2, 21, 2, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(3, 21, 3, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(4, 21, 4, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(5, 21, 5, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(6, 18, 1, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(7, 18, 2, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(8, 18, 3, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(9, 18, 4, 80, 0, 1, '2019-07-27', 0, '0000-00-00'),
(10, 18, 5, 80, 0, 1, '2019-07-27', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `b_level_quatation_attributes`
--

CREATE TABLE `b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_level_quatation_attributes`
--

INSERT INTO `b_level_quatation_attributes` (`row_id`, `fk_od_id`, `order_id`, `product_id`, `product_attribute`) VALUES
(4, 4, '1564250954DY0yb-002', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(5, 5, '1564251121r0Esb-003', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(6, 6, '1564251614bQbOb-004', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(7, 7, '1564252509yG6Fb-005', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(8, 8, '1564252668pWO0b-006', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(9, 9, '1564252668pWO0b-006', 3, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(10, 10, '1564395907fl8bb-007', 2, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(11, 11, '1565003748FB0YB-008', 8, '[]'),
(12, 12, '1565004252lYLjb-009', 8, '[]'),
(13, 13, '15650065938E9ZB-010', 7, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(14, 14, '15650065938E9ZB-010', 8, '[]'),
(15, 15, '1565085043U7P9B-011', 9, '[]'),
(16, 16, '15650906487NXLB-012', 9, '[]'),
(19, 19, '1565098656YJJ6B-013', 9, '[]'),
(20, 20, '1565098656YJJ6B-013', 10, '[]'),
(21, 21, '1565155210YKFBB-014', 9, '[]'),
(22, 22, '1565155210YKFBB-014', 10, '[]'),
(23, 23, '1565159292JRL2B-015', 9, '[]'),
(24, 24, '1565159292JRL2B-015', 10, '[]'),
(25, 25, '1565160847vpO7b-016', 9, '[]'),
(26, 26, '1565160847vpO7b-016', 10, '[]'),
(27, 27, '1565172322hiHqb-017', 10, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `b_level_quatation_tbl`
--

CREATE TABLE `b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_level_quatation_tbl`
--

INSERT INTO `b_level_quatation_tbl` (`id`, `order_id`, `clevel_order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `level_id`, `order_date`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(4, '1564250954DY0yb-002', '1564250897CWPIC-003', 21, 'Rajiv-1004', '', 'assets/barcode/b/1564250954DY0yb-002.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 645.928, 596.7, 0, 645.928, 0, '1', 1, 144, '2019-07-27', 144, 0, '2019-07-27', '0000-00-00', 0),
(5, '1564251121r0Esb-003', '1564251108PDU2C-004', 21, 'Rajiv-1000', '', 'assets/barcode/b/1564251121r0Esb-003.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 733.61, 677.7, 366.81, 366.8, 0, '1', 4, 144, '2019-07-27', 144, 0, '2019-07-27', '0000-00-00', 1),
(6, '1564251614bQbOb-004', '1564251588FVQHC-005', 18, 'Daniel-1000', '', 'assets/barcode/b/1564251614bQbOb-004.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 1310.37, 1210.5, 1310.37, 0, 0, '1', 2, 143, '2019-07-27', 143, 0, '2019-07-27', '0000-00-00', 1),
(7, '1564252509yG6Fb-005', '1564252489UEBDC-007', 18, 'Jessica-1000', '', 'assets/barcode/b/1564252509yG6Fb-005.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 648.851, 599.4, 324.43, 324.42, 0, '1', 3, 143, '2019-07-27', 143, 0, '2019-07-27', '0000-00-00', 1),
(8, '1564252668pWO0b-006', '1564252649DBBLC-008', 18, 'Daniel-1000', '', 'assets/barcode/b/1564252668pWO0b-006.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 2424.91, 2240.1, 2424.9, 0, 0, '1', 2, 143, '2019-07-27', 143, 0, '2019-07-27', '0000-00-00', 1),
(9, '1564395907fl8bb-007', '1564311596CURLC-012', 18, 'Daniel-1000', '', 'assets/barcode/b/1564395907fl8bb-007.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 195.716, 180.8, 97.86, 97.86, 0, '1', 3, 143, '2019-07-28', 143, 0, '2019-07-28', '0000-00-00', 1),
(10, '1565003748FB0YB-008', NULL, 18, 'James-1000', '', 'assets/barcode/b/1565003748FB0YB-008.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 18, 18, 0, 18, 0, '1', 4, 1, '2019-08-05', 1, 0, '2019-08-05', '0000-00-00', 1),
(11, '1565004252lYLjb-009', '15650042321GXNC-014', 18, 'Daniel-1000', '', 'assets/barcode/b/1565004252lYLjb-009.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 19.485, 18, 9.74, 9.74, 0, '1', 3, 1, '2019-08-05', 143, 0, '2019-08-05', '0000-00-00', 1),
(12, '15650065938E9ZB-010', NULL, 18, 'James-1000', '', 'assets/barcode/b/15650065938E9ZB-010.jpg', '0', '', NULL, 0, NULL, NULL, 20, NULL, 100.6, 120.6, 0, 100.6, 0, '1', 1, 1, '2019-08-05', 1, 0, '2019-08-05', '0000-00-00', 1),
(13, '1565085043U7P9B-011', NULL, 29, 'first-indore', '', 'assets/barcode/b/1565085043U7P9B-011.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 2250, 2250, 0, 2250, 0, '1', 4, 150, '2019-08-06', 150, 0, '2019-08-06', '0000-00-00', 1),
(14, '15650906487NXLB-012', NULL, 29, 'first-indore', '', 'assets/barcode/b/15650906487NXLB-012.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 178.2, 178.2, 0, 178.2, 0, '1', 1, 150, '2019-08-06', 150, 0, '2019-08-06', '0000-00-00', 1),
(16, '1565098656YJJ6B-013', NULL, 29, 'first-indore', '', 'assets/barcode/b/1565098656YJJ6B-013.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 803.07, 803.07, 0, 803.07, 0, '1', 1, 150, '2019-08-06', 150, 0, '2019-08-06', '0000-00-00', 1),
(17, '1565155210YKFBB-014', NULL, 29, 'first-indore', '', 'assets/barcode/b/1565155210YKFBB-014.jpg', '0', '', NULL, 0, NULL, NULL, 519697000, NULL, 36.56, 519697000, 0, 36.56, 0, '1', 1, 150, '2019-08-07', 150, 0, '2019-08-07', '0000-00-00', 1),
(18, '1565159292JRL2B-015', NULL, 29, 'first-indore', '', 'assets/barcode/b/1565159292JRL2B-015.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 21606.4, 21606.4, 0, 21606.4, 0, '1', 1, 150, '2019-08-07', 150, 0, '2019-08-07', '0000-00-00', 1),
(19, '1565160847vpO7b-016', '1565160813LAHMC-015', 29, 'd_user-Indore', '', 'assets/barcode/b/1565160847vpO7b-016.jpg', '0', '', 0, 0, 0, 0, 0, '0', 86784.6, 86784.6, 43392.3, 43392.3, 0, '1', 3, 150, '2019-08-07', 152, 0, '2019-08-07', '0000-00-00', 1),
(20, '1565172322hiHqb-017', '1565172271RTPFC-016', 29, 'new_d-Indore', '', 'assets/barcode/b/1565172322hiHqb-017.jpg', '0', '', 0, 0, 0, 0, 0, '0', 626.562, 626.562, 313.28, 313.28, 0, '1', 3, 150, '2019-08-07', 152, 0, '2019-08-07', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_level_qutation_details`
--

CREATE TABLE `b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_level_qutation_details`
--

INSERT INTO `b_level_qutation_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `unit_total_price`, `list_price`, `discount`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`) VALUES
(4, '1564250954DY0yb-002', 'Study', '1', 1, 1, 596.7, 663, 10, 1, 30, 70, 70, 0, 0, 'Testing'),
(5, '1564251121r0Esb-003', 'Dining', '1', 1, 1, 677.7, 753, 10, 1, 31, 70, 80, 0, 4, ''),
(6, '1564251614bQbOb-004', 'Study', '1', 1, 1, 1210.5, 1345, 10, 1, 29, 102, 106, 0, 0, 'Testing'),
(7, '1564252509yG6Fb-005', 'Dining', '1', 1, 1, 599.4, 666, 10, 1, 29, 80, 60, 4, 4, ''),
(8, '1564252668pWO0b-006', 'Guest Bath', '1', 1, 1, 677.7, 753, 10, 1, 30, 70, 80, 4, 4, 'Testing'),
(9, '1564252668pWO0b-006', 'Living', '3', 1, 1, 1562.4, 1736, 10, 5, 91, 88, 90, 8, 1, ''),
(10, '1564395907fl8bb-007', 'Guest', '2', 1, 1, 180.8, 904, 80, 4, 56, 66, 78, 4, 4, 'test'),
(11, '1565003748FB0YB-008', 'Guest', '8', 1, 1, 18, 20, 10, 9, 191, 10, 20, 3, 2, 'asas'),
(12, '1565004252lYLjb-009', 'Dining', '8', 1, 1, 18, 20, 10, 9, 193, 10, 18, 2, 3, 'test'),
(13, '15650065938E9ZB-010', 'Living', '7', 1, 1, 102.6, 114, 10, 9, 191, 10, 20, 1, 3, 'manufacturing order'),
(14, '15650065938E9ZB-010', 'Living', '8', 1, 1, 18, 20, 10, 9, 191, 100, 20, 1, 2, 'manufacturing order'),
(15, '1565085043U7P9B-011', 'Guest', '9', 6, 1, 2250, 2500, 10, 14, 232, 500, 500, 0, 0, 'asas'),
(16, '15650906487NXLB-012', 'Dining', '9', 6, 1, 178.2, 198, 10, 14, 232, 100, 198, 0, 0, 'test'),
(19, '1565098656YJJ6B-013', 'Dining', '9', 6, 1, 176.51, 196.12, 10, 14, 232, 98, 200, 1, 0, 'test'),
(20, '1565098656YJJ6B-013', 'Dining', '10', 6, 1, 626.56, 696.18, 10, 14, 232, 100, 100, 1, 1, 'test'),
(21, '1565155210YKFBB-014', 'Dining', '9', 6, 1, 519697000, 577441000, 10, 14, 232, 100, 100, 1, 1, 'first order'),
(22, '1565155210YKFBB-014', 'Dining', '10', 6, 1, 626.56, 696.18, 10, 14, 232, 100, 100, 1, 1, 'second product'),
(23, '1565159292JRL2B-015', 'Dining', '9', 6, 1, 21600, 24000, 10, 14, 232, 100, 100, 0, 0, 'first pro'),
(24, '1565159292JRL2B-015', 'Living', '10', 6, 1, 6.41, 7.12, 10, 14, 232, 10, 10, 1, 1, 'sec pro'),
(25, '1565160847vpO7b-016', 'Dining', '9', 6, 1, 86778.2, 96420.2, 10, 14, 232, 200, 200, 2, 1, 'cus first order'),
(26, '1565160847vpO7b-016', 'Dining', '10', 6, 1, 6.408, 7.12, 10, 14, 232, 10, 10, 1, 1, 'cus second order'),
(27, '1565172322hiHqb-017', 'Living', '10', 6, 1, 626.562, 696.18, 10, 14, 232, 100, 100, 1, 1, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `b_menusetup_tbl`
--

CREATE TABLE `b_menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(255) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(2) NOT NULL COMMENT '1 = left, 2 = system, 3 = top',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_menusetup_tbl`
--

INSERT INTO `b_menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `created_by`, `create_date`) VALUES
(1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 1, '2019-01-03 11:57:31'),
(2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 1, '2019-01-03 11:58:27'),
(3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 1, '2019-01-03 11:58:54'),
(5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 1, '2019-01-03 12:05:04'),
(6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 1, '2019-01-03 12:05:35'),
(7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 1, '2019-01-03 12:06:25'),
(8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 1, '2019-01-03 12:07:13'),
(9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 1, '2019-01-03 12:07:50'),
(10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 1, '2019-01-03 12:08:44'),
(11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 1, '2019-01-03 12:10:09'),
(13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 1, '2019-01-03 12:11:34'),
(14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 1, '2019-01-03 12:12:45'),
(15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 1, '2019-01-03 12:13:34'),
(16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 1, '2019-01-03 12:14:03'),
(17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 1, '2019-01-03 12:16:24'),
(20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 1, '2019-01-03 12:19:38'),
(21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 1, '2019-01-03 12:22:13'),
(22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 1, '2019-01-03 12:23:15'),
(23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 1, '2019-01-03 12:23:51'),
(24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 1, '2019-01-03 12:24:44'),
(28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 1, '2019-01-03 12:27:45'),
(29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 1, '2019-01-03 12:28:40'),
(30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 1, '2019-01-03 12:29:43'),
(31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 1, '2019-01-03 12:30:30'),
(35, 'production', 'Production', '', 'production', 6, 0, 1, 0, '', 1, 1, '2019-01-05 04:49:38'),
(36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 1, '2019-01-05 04:51:43'),
(37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 1, '2019-01-05 04:52:34'),
(38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 1, '2019-01-05 05:14:50'),
(39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 1, '2019-01-05 05:15:44'),
(40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 1, '2019-01-05 05:16:47'),
(41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 1, '2019-01-05 05:18:56'),
(42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 1, '2019-01-05 05:19:33'),
(46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 1, '2019-01-05 05:22:57'),
(47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 1, '2019-01-05 05:23:32'),
(48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 1, '2019-01-05 05:24:09'),
(50, 'account', 'Account', '', 'account', 7, 0, 1, 0, '', 1, 1, '2019-01-05 05:44:12'),
(51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 1, '2019-01-05 05:47:13'),
(52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 1, '2019-01-05 05:49:17'),
(53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 1, '2019-01-05 05:55:38'),
(54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 1, '2019-01-05 05:56:55'),
(55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:02:46'),
(56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:06:32'),
(57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:10:23'),
(58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:12:25'),
(59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:20:28'),
(60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 1, '2019-01-05 06:21:59'),
(61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 1, '2019-01-05 06:22:31'),
(62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:24:15'),
(63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:27:28'),
(64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:28:03'),
(65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:28:50'),
(66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:30:04'),
(67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 1, '2019-01-05 06:30:35'),
(68, 'setting', 'Setting', '', 'setting', 8, 0, 1, 0, '', 1, 1, '2019-01-05 06:36:53'),
(69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-01-05 06:50:57'),
(70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-01-05 06:51:54'),
(71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 1, '2019-01-05 06:52:47'),
(72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 1, '2019-01-05 06:53:45'),
(73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 1, '2019-01-05 06:54:27'),
(74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 1, '2019-01-05 06:54:53'),
(75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-01-05 06:55:44'),
(76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-01-15 08:56:56'),
(78, 'color', 'Color', 'color', 'catalog', 3, 10, 1, 0, '', 1, 1, '2019-01-19 11:44:02'),
(79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 1, '2019-01-19 11:59:03'),
(83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 1, '2019-02-08 05:48:21'),
(84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 1, '2019-02-08 05:52:51'),
(85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 1, '2019-02-09 08:56:38'),
(86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 1, '2019-02-09 08:57:58'),
(87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-02-18 13:02:52'),
(88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 1, '2019-02-28 04:54:19'),
(89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 1, '2019-02-28 04:59:10'),
(90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 1, '2019-02-28 04:59:52'),
(91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 1, '2019-02-28 05:00:45'),
(92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 1, '2019-02-28 05:01:25'),
(93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 1, '2019-02-28 05:03:24'),
(94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 1, '2019-02-28 05:39:56'),
(95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 1, '2019-03-09 04:34:18'),
(96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 1, '2019-04-17 13:23:20'),
(104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 1, '2019-05-09 08:30:36'),
(105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 1, '2019-05-09 08:31:59'),
(106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 1, '2019-05-09 08:34:08'),
(107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 1, '2019-05-09 08:38:46'),
(108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 1, '2019-05-09 08:40:00'),
(109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 1, '2019-05-09 08:40:54'),
(110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 1, '2019-05-09 08:41:32'),
(111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 1, '2019-05-09 08:42:08'),
(112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 1, '2019-05-09 08:43:03'),
(113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 1, '2019-05-11 03:26:42'),
(114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 1, '2019-05-11 03:27:56'),
(115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 1, '2019-05-11 03:28:45'),
(116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:29:55'),
(117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:30:54'),
(118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:31:29'),
(119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:32:10'),
(120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:32:51'),
(121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 1, '2019-05-11 03:34:26'),
(122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 1, '2019-05-11 03:35:17'),
(123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:35:53'),
(124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:36:36'),
(125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:37:24'),
(126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:38:07'),
(127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:38:31'),
(128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 1, '2019-05-11 03:39:17'),
(129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 1, '2019-05-11 03:40:13'),
(130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 1, '2019-05-11 03:40:38'),
(131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 1, '2019-05-11 03:41:14'),
(132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 1, '2019-05-15 03:49:02'),
(133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 1, '2019-05-15 03:54:22'),
(134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 1, '2019-05-15 03:56:19'),
(135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 1, '2019-05-18 10:38:14'),
(136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 1, '2019-05-19 11:59:07'),
(137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 1, '2019-05-20 04:03:56'),
(138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 1, '2019-05-21 06:36:51'),
(139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 1, '2019-06-01 11:20:26'),
(140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 1, '2019-07-25 13:50:15'),
(141, 'gallery', 'Gallery', '#', 'Gallery', 5, 0, 1, 0, '', 1, 1, '2019-07-25 13:50:50'),
(142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 1, '2019-07-25 13:51:44'),
(143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 1, '2019-07-25 13:52:24'),
(144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 1, '2019-07-25 13:52:48'),
(145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 1, '2019-07-25 13:53:09'),
(146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 1, '2019-07-29 08:46:27'),
(147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 51, 0, 1, 0, NULL, 1, 1, '2019-08-05 10:52:57'),
(148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 1, '2019-08-05 10:53:34'),
(149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 1, '2019-08-05 10:54:12'),
(150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 1, '2019-08-05 10:54:12'),
(151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 1, '2019-08-05 10:54:26'),
(152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 1, '2019-08-05 10:54:26'),
(153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 1, '2019-08-05 10:54:26');

-- --------------------------------------------------------

--
-- Table structure for table `b_notification_tbl`
--

CREATE TABLE `b_notification_tbl` (
  `id` int(11) NOT NULL,
  `notification_text` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `go_to_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `b_user_id` int(11) NOT NULL,
  `notification_for` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CUSTOMER',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_notification_tbl`
--

INSERT INTO `b_notification_tbl` (`id`, `notification_text`, `go_to_url`, `created_by`, `b_user_id`, `notification_for`, `date`) VALUES
(2, 'Payment has been received for 1564251121r0Esb-003', 'b_level/invoice_receipt/c_receipt/1564251121r0Esb-003', 144, 0, 'CUSTOMER', '2019-07-27'),
(3, 'Payment has been received for 1564251614bQbOb-004', 'b_level/invoice_receipt/c_receipt/1564251614bQbOb-004', 143, 0, 'CUSTOMER', '2019-07-27'),
(4, 'Payment has been received for 1564252509yG6Fb-005', 'b_level/invoice_receipt/c_receipt/1564252509yG6Fb-005', 143, 0, 'CUSTOMER', '2019-07-27'),
(5, 'Payment has been received for 1564251614bQbOb-004', 'b_level/invoice_receipt/c_receipt/1564251614bQbOb-004', 143, 0, 'CUSTOMER', '2019-07-27'),
(6, 'Payment has been received for 1564252668pWO0b-006', 'b_level/invoice_receipt/c_receipt/1564252668pWO0b-006', 143, 0, 'CUSTOMER', '2019-07-27'),
(7, 'Payment has been received for 1564251614bQbOb-004', 'b_level/invoice_receipt/c_receipt/1564251614bQbOb-004', 143, 0, 'CUSTOMER', '2019-07-28'),
(8, 'Payment has been received for 1564252668pWO0b-006', 'b_level/invoice_receipt/c_receipt/1564252668pWO0b-006', 143, 0, 'CUSTOMER', '2019-07-28'),
(9, 'Payment has been received for 1564395907fl8bb-007', 'b_level/invoice_receipt/c_receipt/1564395907fl8bb-007', 143, 0, 'CUSTOMER', '2019-07-29'),
(10, 'Payment has been received for 1565004252lYLjb-009', 'b_level/invoice_receipt/c_receipt/1565004252lYLjb-009', 143, 0, 'CUSTOMER', '2019-08-05'),
(14, 'You have a new request for catalog', 'catalog-request', 150, 1, 'B_USER', '2019-08-06'),
(15, 'Your \"new request\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-06'),
(16, 'You have a new request for catalog', 'catalog-request', 150, 1, 'B_USER', '2019-08-07'),
(17, 'Your \"first request for catalog\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-07'),
(18, 'Payment has been received for 1565160847vpO7b-016', 'b_level/invoice_receipt/c_receipt/1565160847vpO7b-016', 152, 150, 'CUSTOMER', '2019-08-07'),
(19, 'Payment has been received for 1565172322hiHqb-017', 'b_level/invoice_receipt/c_receipt/1565172322hiHqb-017', 152, 150, 'CUSTOMER', '2019-08-07'),
(20, 'You received new order for 15651740954KLSB-005', 'b_level/Shared_catalog_controller/b_user_receipt/15651740954KLSB-005', 150, 1, 'B_USER', '2019-08-07'),
(21, 'You received new order for 15651768584MOGB-002', 'b_level/Shared_catalog_controller/b_user_receipt/15651768584MOGB-002', 150, 1, 'B_USER', '2019-08-07'),
(22, 'You have a new request for catalog', 'catalog-request', 1, 150, 'B_USER', '2019-08-07'),
(23, 'Your \"hii\" request has been approved', 'b-user-catalog-product/150', 150, 1, 'B_USER', '2019-08-07'),
(24, 'You have a new request for catalog', 'catalog-request', 150, 1, 'B_USER', '2019-08-07'),
(25, 'Your \"hii\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-07'),
(26, 'You received new order for 1565180117RILXB-002', 'b_level/Shared_catalog_controller/b_user_receipt/1565180117RILXB-002', 150, 1, 'B_USER', '2019-08-07'),
(27, 'You have a new request for catalog', 'catalog-request', 150, 1, 'B_USER', '2019-08-08'),
(28, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(29, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(30, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(31, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(32, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(33, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(34, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(35, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(36, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(37, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(38, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(39, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(40, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(41, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(42, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(43, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(44, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(45, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(46, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(47, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(48, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(49, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08'),
(50, 'Your \"First request for product\" request has been approved', 'b-user-catalog-product/1', 1, 150, 'B_USER', '2019-08-08');

-- --------------------------------------------------------

--
-- Table structure for table `b_role_permission_tbl`
--

CREATE TABLE `b_role_permission_tbl` (
  `id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `can_access` tinyint(1) NOT NULL,
  `can_create` tinyint(1) NOT NULL,
  `can_edit` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_role_tbl`
--

CREATE TABLE `b_role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_quatation_attributes`
--

CREATE TABLE `b_to_b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_quatation_tbl`
--

CREATE TABLE `b_to_b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_user_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_to_b_level_quatation_tbl`
--

INSERT INTO `b_to_b_level_quatation_tbl` (`id`, `order_id`, `clevel_order_id`, `b_user_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `level_id`, `order_date`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(29, '1565270424FIGSB-002', '1565155210YKFBB-014', 1, 'first-indore', '', 'assets/barcode/b/1565270424FIGSB-002.jpg', '0', '0', NULL, 0, 0, 0, 0, '0', 1316, 1316, 0, 1316, 0, '1', 1, 150, '2019-08-08', 150, 0, '2019-08-08', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_qutation_details`
--

CREATE TABLE `b_to_b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_to_b_level_qutation_details`
--

INSERT INTO `b_to_b_level_qutation_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `unit_total_price`, `list_price`, `discount`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`) VALUES
(40, '1565270424FIGSB-002', 'Dining', '8', 6, 1, 20, 20, 0, 9, 232, 100, 100, 1, 1, 'first order'),
(41, '1565270424FIGSB-002', 'Dining', '7', 6, 1, 1296, 1296, 0, 9, 232, 100, 100, 1, 1, 'second product');

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_shipment_data`
--

CREATE TABLE `b_to_b_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `b_user_name` varchar(250) DEFAULT NULL,
  `b_user_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_access_tbl`
--

CREATE TABLE `b_user_access_tbl` (
  `role_acc_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_user_access_tbl`
--

INSERT INTO `b_user_access_tbl` (`role_acc_id`, `role_id`, `user_id`, `created_by`) VALUES
(4, 4, '28', '1'),
(5, 6, '29', '1'),
(6, 5, '34', '1'),
(7, 4, '57', '1'),
(8, 4, '64', '1'),
(9, 6, '64', '1'),
(14, 7, '71', '1'),
(15, 7, '11', '1');

-- --------------------------------------------------------

--
-- Table structure for table `b_user_catalog_products`
--

CREATE TABLE `b_user_catalog_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pattern_model_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_link` int(11) NOT NULL DEFAULT '0',
  `pattern_link` int(11) NOT NULL DEFAULT '0',
  `request_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL DEFAULT '0',
  `create_date` date NOT NULL,
  `approve_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:approve,0:not approve by b2 user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `b_user_catalog_products`
--

INSERT INTO `b_user_catalog_products` (`id`, `product_id`, `pattern_model_id`, `product_link`, `pattern_link`, `request_id`, `requested_by`, `create_date`, `approve_status`) VALUES
(4, 8, '9', 9, 14, 1, 150, '2019-08-08', 1),
(5, 8, '10', 0, 0, 1, 150, '2019-08-08', 0),
(6, 8, '11', 0, 0, 1, 150, '2019-08-08', 0),
(7, 7, '9', 10, 14, 1, 150, '2019-08-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_user_catalog_request`
--

CREATE TABLE `b_user_catalog_request` (
  `request_id` int(11) NOT NULL,
  `b_user_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not approved ,1:approved'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `b_user_catalog_request`
--

INSERT INTO `b_user_catalog_request` (`request_id`, `b_user_id`, `requested_by`, `remark`, `create_date`, `status`) VALUES
(1, 1, 150, 'First request for product', '2019-08-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_category` int(11) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`category_id`, `category_name`, `parent_category`, `description`, `status`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(1, 'Blinds', 0, '', 1, 1, NULL, '2019-07-25', '0000-00-00'),
(2, 'Shades', 0, '', 0, 1, 1, '2019-07-25', '2019-07-25'),
(3, 'Shutters', 0, '', 0, 1, 1, '2019-07-25', '2019-07-25'),
(4, 'Arch', 0, '', 0, 1, 1, '2019-07-25', '2019-07-25'),
(5, 'Misc', 0, '', 1, 1, NULL, '2019-07-25', '0000-00-00'),
(6, 'Blinds', 0, 'test category', 1, 150, 150, '2019-08-05', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `city_state_tbl`
--

CREATE TABLE `city_state_tbl` (
  `id` int(11) NOT NULL,
  `state_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `color_tbl`
--

CREATE TABLE `color_tbl` (
  `id` int(11) NOT NULL,
  `pattern_id` int(10) DEFAULT NULL,
  `color_name` text NOT NULL,
  `color_number` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color_tbl`
--

INSERT INTO `color_tbl` (`id`, `pattern_id`, `color_name`, `color_number`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(191, 9, 'White White', 'P01-S', 1, 0, '2019-07-29', '0000-00-00'),
(192, 9, 'Antique White', 'P02-S', 1, 0, '2019-07-29', '0000-00-00'),
(193, 9, 'Alabaster', 'P03-S', 1, 0, '2019-07-29', '0000-00-00'),
(194, 10, 'White White', 'P01-G', 1, 0, '2019-07-29', '0000-00-00'),
(195, 10, 'Antique White', 'P02-G', 1, 0, '2019-07-29', '0000-00-00'),
(196, 10, 'Alabaster', 'P03-G', 1, 0, '2019-07-29', '0000-00-00'),
(197, 11, 'White White', 'P01-B', 1, 0, '2019-07-29', '0000-00-00'),
(198, 11, 'Antique White', 'P02-B', 1, 0, '2019-07-29', '0000-00-00'),
(199, 11, 'Alabaster', 'P03-B', 1, 0, '2019-07-29', '0000-00-00'),
(200, 12, 'White White', 'P01-2.5', 1, 0, '2019-07-29', '0000-00-00'),
(201, 12, 'Antique White', 'P02-2.5', 1, 0, '2019-07-29', '0000-00-00'),
(202, 12, 'Alabaster', 'P03-2.5', 1, 0, '2019-07-29', '0000-00-00'),
(203, 13, 'Blanco', '1124', 1, 0, '2019-07-29', '0000-00-00'),
(204, 13, 'Vivid White', '1104', 1, 0, '2019-07-29', '0000-00-00'),
(205, 13, 'Pure White', '1101', 1, 0, '2019-07-29', '0000-00-00'),
(206, 13, 'Bone', '2910', 1, 0, '2019-07-29', '0000-00-00'),
(207, 13, 'Alabaster', '1123', 1, 0, '2019-07-29', '0000-00-00'),
(208, 13, 'Smoky Grey', '206', 1, 0, '2019-07-29', '0000-00-00'),
(209, 13, 'Slat Natural', '1107', 1, 0, '2019-07-29', '0000-00-00'),
(210, 13, 'Sugar Maple', '1109', 1, 0, '2019-07-29', '0000-00-00'),
(211, 13, 'Sunrise Oak', '1111', 1, 0, '2019-07-29', '0000-00-00'),
(212, 13, ' Mink', '1112', 1, 0, '2019-07-29', '0000-00-00'),
(213, 13, 'Cherry Wood', '202', 1, 0, '2019-07-29', '0000-00-00'),
(214, 13, 'Golden Oak', '1110', 1, 0, '2019-07-29', '0000-00-00'),
(215, 13, 'Warm Cherry', '1118', 1, 0, '2019-07-29', '0000-00-00'),
(216, 13, 'Evening Oak', '1115', 1, 0, '2019-07-29', '0000-00-00'),
(217, 13, 'Toasted Walnut', '1121', 1, 0, '2019-07-29', '0000-00-00'),
(218, 13, 'Warm Chestnut', '1122', 1, 0, '2019-07-29', '0000-00-00'),
(219, 13, 'Cherry', '1116', 1, 0, '2019-07-29', '0000-00-00'),
(220, 13, 'Mahogany', '203', 1, 0, '2019-07-29', '0000-00-00'),
(221, 13, 'Dark Mahogany', '205', 1, 0, '2019-07-29', '0000-00-00'),
(222, 13, 'Kona', '1125', 1, 0, '2019-07-29', '0000-00-00'),
(223, 9, 'White White', 'P01-WiVi', 1, 0, '2019-07-29', '0000-00-00'),
(224, 9, 'Antique White', 'P02-WiVi', 1, 0, '2019-07-29', '0000-00-00'),
(225, 0, 'Kona', '1125', 1, 0, '2019-07-31', '0000-00-00'),
(226, 0, 'Kona', '1125', 1, 0, '2019-07-31', '0000-00-00'),
(227, 9, 'Kona', '1125', 1, 0, '2019-07-31', '0000-00-00'),
(228, 9, 'Kona', '1125', 1, 0, '2019-07-31', '0000-00-00'),
(229, 9, 'green', '43', 1, 0, '2019-07-31', '0000-00-00'),
(230, 9, 'red', '1255', 1, 0, '2019-07-31', '0000-00-00'),
(231, 9, 'white', '423', 1, 0, '2019-07-31', '0000-00-00'),
(232, 14, 'test color', 'test color', 150, 150, '2019-08-05', '2019-08-05'),
(233, 10, 'red', 'red', 150, 0, '2019-08-06', '0000-00-00'),
(234, 11, 'red', 'red', 150, 0, '2019-08-06', '0000-00-00'),
(235, 12, 'red', 'red', 150, 0, '2019-08-06', '0000-00-00'),
(236, 13, 'red', 'red', 150, 0, '2019-08-06', '0000-00-00'),
(237, 14, 'red', 'red', 150, 0, '2019-08-06', '0000-00-00'),
(238, 9, 'demo color', 'demo color', 150, 0, '2019-08-07', '0000-00-00'),
(239, 10, 'demo color', 'demo color', 150, 0, '2019-08-07', '0000-00-00'),
(240, 11, 'demo color', 'demo color', 150, 0, '2019-08-07', '0000-00-00'),
(241, 12, 'demo color', 'demo color', 150, 0, '2019-08-07', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `company_profile`
--

CREATE TABLE `company_profile` (
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inches',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `setting_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_profile`
--

INSERT INTO `company_profile` (`company_id`, `user_id`, `company_name`, `email`, `phone`, `picture`, `email_notification`, `sms_notification`, `currency`, `unit`, `logo`, `address`, `city`, `state`, `zip_code`, `country_code`, `created_by`, `created_at`, `updated_by`, `updated_date`, `status`, `setting_status`) VALUES
(3, 1, 'BMS Window Decor', 'info@bmswindowdecor.com', '+1 (972) 820-0007', NULL, '2020101', '2020101', '$', 'inches', 'BMS-Logos_Decor-150.png', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 1, NULL, 1, '2019-01-27 13:14:51', 1, 0),
(108, 143, 'Legacy Blinds Mfg', 'legacyblinds@aol.com', '+1 (972) 820-0007', NULL, '2020101', '2020101', '$', 'inches', '16d3c2a8a11411d2888c83fcc815bf13.png', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 1, '2019-07-27', 143, NULL, 1, 1),
(109, 144, 'Legacy blinds', 'admin@gmail.com', '+1 (478) 287-1258', NULL, '2020101', '2020101', '$', 'inches', '0a5dd89d552a9f42f812eeb7d9408108.png', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 1, '2019-07-27', 144, NULL, 1, 1),
(110, 149, 'TechnoPlus Inc', 'rajiv@technoplusinc.com', '+1 (478) 287-1258', NULL, NULL, NULL, NULL, 'inches', NULL, '1004 Ridge Hollow Trail', 'Irving', 'TX', '75063', 'US', 1, '2019-07-29', NULL, NULL, 1, 0),
(111, 150, 'todayinfra_main', 'todayinfra@mailinator.com', '+1 (212) 121-2121', NULL, NULL, NULL, '₩', 'cm', 'ebay_logo.png', 'Indore', 'Indore', 'MP', '452004', 'IN', 150, NULL, 150, NULL, 1, 0),
(113, 152, 'todayinfra', 'first_customer@customer.com', '+912121212121', NULL, '2020101', '2020101', '₩', 'inches', '', 'indore', 'indore', 'mp', '12345', 'India', 150, '2019-08-06', 152, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_commet_tbl`
--

CREATE TABLE `customer_commet_tbl` (
  `id` int(11) NOT NULL,
  `comment_from` int(11) NOT NULL,
  `comment_to` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_visited` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_file_tbl`
--

CREATE TABLE `customer_file_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_user_id` int(11) NOT NULL,
  `file_upload` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customer_id` int(11) NOT NULL,
  `customer_user_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_customer_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `customer_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'business=c,personal=d',
  `side_mark` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL COMMENT 'Only  level id',
  `now_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL COMMENT 'C level user ID',
  `create_date` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customer_id`, `customer_user_id`, `customer_no`, `company_customer_id`, `first_name`, `last_name`, `company`, `street_no`, `address`, `city`, `state`, `zip_code`, `country_code`, `phone`, `email`, `reference`, `customer_type`, `side_mark`, `level_id`, `now_status`, `created_by`, `create_date`, `update_by`, `update_date`) VALUES
(17, NULL, 'CUS-0016-Rajiv Pastula', 'PAT-17', 'Rajiv', 'Pastula', 'PaTaaK', '1004', '1004 Ridge Hollow Trail', 'Irving', 'TX', '75063', 'US', '+1 (478) 287-1258', 'rajiv@pataak.com', '', 'personal', 'Rajiv-1004', 142, NULL, '142', '2019-07-27', 0, '0000-00-00'),
(18, '143', 'CUS-0017-James Park', 'LEG-18', 'James', 'Park', 'Legacy Blinds Mfg', '1000', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (972) 820-0007', 'legacyblinds@aol.com', '', 'business', 'James-1000', 1, NULL, '1', '2019-07-27', 0, '0000-00-00'),
(20, NULL, 'CUS-0018-Jessica Park', '-19', 'Jessica', 'Park', '', '1000', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (214) 837-0214', 'info@bmsdecor.com', 'James', 'personal', 'Jessica-1000', 143, NULL, '143', '2019-07-27', 0, '0000-00-00'),
(21, '144', 'CUS-0019-Rajiv Pastula', 'LEG-21', 'Rajiv', 'Pastula', 'Legacy blinds', '1000', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (478) 287-1258', 'admin@gmail.com', '', 'business', 'Rajiv-1000', 1, NULL, '1', '2019-07-27', 0, '0000-00-00'),
(22, NULL, 'CUS-0020-Rajiv Pastula', '-22', 'Rajiv', 'Pastula', '', '1004', '1004 Ridge Hollow Trail', 'Irving', 'TX', '75063', 'US', '+1 (478) 287-1258', 'pastularajiv@gmail.com', '', 'personal', 'Rajiv-1004', 144, NULL, '144', '2019-07-27', 0, '0000-00-00'),
(23, NULL, 'CUS-0021-Rajiv Pastula1', '-23', 'Rajiv', 'Pastula1', '', '1000', '1000 3rd Avenue', 'New York', 'US', '10022', 'US', '+1 (478) 287-1258', 'demoinsys@gmail.com', '', 'personal', 'Rajiv-1000', 144, NULL, '144', '2019-07-27', 144, '2019-07-31'),
(24, NULL, 'CUS-0022-Daniel Park', '-24', 'Daniel', 'Park', '', '1000', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (214) 837-0214', 'danielpark@gmail.com', '', 'personal', 'Daniel-1000', 143, NULL, '143', '2019-07-27', 0, '0000-00-00'),
(25, '149', 'CUS-0023-Rajiv Pastula', 'TEC-25', 'Rajiv', 'Pastula', 'TechnoPlus Inc', '1004', '1004 Ridge Hollow Trail', 'Irving', 'TX', '75063', 'US', '+1 (478) 287-1258', 'rajiv@technoplusinc.com', '', 'business', 'Rajiv-1004', 1, NULL, '1', '2019-07-29', 0, '0000-00-00'),
(26, NULL, 'CUS-0024-Rajiv Pastula', 'ABC-26', 'Rajiv', 'Pastula', 'ABC', 'DSW', 'DSW Designer Shoe Warehouse', 'New York', 'US', '10123', 'US', '+1 (234) 234-2343', 'rajivpastula45@gmail.com', '', 'personal', 'Rajiv-DSW', 143, NULL, '143', '2019-07-31', 0, '0000-00-00'),
(27, NULL, 'CUS-0025-Moses Cameron', 'ACE-27', 'Moses', 'Cameron', 'Acevedo and Sherman Inc', 'Voluptatem', 'Voluptatem do eius a', 'Error mollitia eveni', 'Repudiandae illum q', '46579', 'Voluptatum quis sunt', '+1 (614) 921-1069', 'demoinsys@gmail.com', 'Et non maiores ullam', 'personal', 'Moses-Voluptatem', 144, NULL, '144', '2019-07-31', 0, '0000-00-00'),
(29, '152', 'CUS-0027-first customer', 'TOD-29', 'first', 'customer', 'todayinfra', 'indore', 'indore', 'indore', 'mp', '12345', 'India', '+912121212121', 'first_customer@customer.com', '', 'business', 'first-indore', 150, NULL, '150', '2019-08-06', 0, '0000-00-00'),
(30, NULL, 'CUS-0028-d_user first', 'TES-30', 'd_user', 'first', 'test company', 'Indore', 'Indore', 'Indore', 'MP', '452004', 'India', '+1 (212) 121-2121', 'd_user@mailinator.com', '', 'personal', 'd_user-Indore', 152, NULL, '152', '2019-08-07', 0, '0000-00-00'),
(31, NULL, 'CUS-0029-new_d user', 'TES-31', 'new_d', 'user', 'test', 'Indore', 'Indore', 'Indore', 'MP', '452004', 'IN', '+1 (212) 121-2121', 'new_d_user@mailinator.com', '', 'personal', 'new_d-Indore', 152, NULL, '152', '2019-08-07', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_phone_type_tbl`
--

CREATE TABLE `customer_phone_type_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_user_id` int(11) NOT NULL,
  `phone_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_phone_type_tbl`
--

INSERT INTO `customer_phone_type_tbl` (`id`, `customer_id`, `customer_user_id`, `phone_type`, `phone`) VALUES
(19, 18, 143, 'Phone', '+1 (972) 820-0007'),
(21, 20, 0, 'Phone', '+1 (214) 837-0214'),
(22, 21, 144, 'Phone', '+1 (478) 287-1258'),
(23, 22, 0, 'Phone', '+1 (478) 287-1258'),
(25, 24, 0, 'Phone', '+1 (214) 837-0214'),
(26, 25, 149, 'Phone', '+1 (478) 287-1258'),
(27, 26, 0, 'Phone', '+1 (234) 234-2343'),
(28, 27, 0, 'Phone', '+1 (614) 921-1069'),
(30, 23, 0, 'Phone', '+1 (478) 287-1258'),
(32, 29, 152, 'Phone', '+912121212121'),
(33, 30, 0, 'Phone', '+1 (212) 121-2121'),
(34, 31, 0, 'Phone', '+1 (212) 121-2121');

-- --------------------------------------------------------

--
-- Table structure for table `customer_query`
--

CREATE TABLE `customer_query` (
  `row_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `c_level_id` int(11) NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_email_tbl`
--

CREATE TABLE `custom_email_tbl` (
  `id` int(11) NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_email_tbl`
--

INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'demoinsys@gmail.com', 'admin@gmail.com', '', '2019-07-29 09:56:31', 1),
(0, 'demoinsys@gmail.com', 'demoinsys@gmail.com', '', '2019-07-29 09:56:32', 1),
(0, 'demoinsys@gmail.com', 'admin@gmail.com', '<p>test</p>', '2019-07-29 09:56:52', 1),
(0, 'demoinsys@gmail.com', 'demoinsys@gmail.com', '<p>test</p>', '2019-07-29 09:56:52', 1),
(0, 'demoinsys@gmail.com', 'admin@gmail.com', '<p>Hi&nbsp;</p><p><br></p><p>Testing bms</p>', '2019-07-29 10:00:55', 1),
(0, 'demoinsys@gmail.com', 'pastularajiv@gmail.com', '<p>Hi&nbsp;</p><p><br></p><p>Testing bms</p>', '2019-07-29 10:00:56', 1),
(0, 'demoinsys@gmail.com', 'admin@gmail.com', '<p>Hello</p>', '2019-07-29 10:04:38', 1),
(0, 'demoinsys@gmail.com', 'dipak@pataak.com', '<p>Hello</p>', '2019-07-29 10:04:39', 1),
(0, 'info@bmsdecor.com', 'admin@gmail.com', '<p>test</p>', '2019-07-29 10:38:06', 1),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', '<p>test</p>', '2019-07-29 10:38:06', 1),
(0, 'info@bmsdecor.com', 'admin@gmail.com', '<p>Testing EMail</p>', '2019-07-29 10:46:32', 1),
(0, 'info@bmsdecor.com', 'pastularajiv@gmail.com', '<p>Testing EMail</p>', '2019-07-29 10:46:32', 1),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 04:22:39', 144),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 04:25:22', 144),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 04:29:59', 144),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 04:30:43', 144);
INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', '<p>test<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAHhCAYAAACxyrMlAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxOTowNzoyNSAxNjowNTo0NaRq/x8AAP94SURBVHhe7P0HvC3JVR8K18nnppk7SdIoM5JQQAKBhJAQIAQIhEQ2yfnZn/GzscEJB7DNs/0ZjLP9Q+/xHD8HDH4YMCY9TBAyEiiggFAeaaTRaHK4+Z578vnWf1Wt6lW5uvc+915pzv/ePrtq5aqurtXV3bv3wgHBjMCPft2/Nbc961bznf/ymxzl+sHIpswXE1xPirbUxojcst3uqn6DA6tfJ0Y9HmLGpmP5Dl9d4yP24z49mGCpRWueUZTo6H9CIHOQt5YjgoZt0ZilFfrTgXI8jlHkN9Cj3vJdQ8H+Av3zIObA77Ap6BBlL4sLZmtjx+zt7JsFKjNIl0sqjMQgqo60cnzFLCxRdc/WGYFuCuyzQSSyLYCA8pNgIXKS81nSzaJfuG9kKixQsPd/4EHz3p//QN/BfIT5odTdEb25V2YX8ChK9ptoTMQpc4Rpj7kkHoalFq15RlGiwilhZOJxWFyimSQ3mShAvBxPmdMFUq/br6FfqyaJ3T7wR0TSK0rJZnd7L594AkQG92kj0j4FuLi6SIlnIUw8NcBUYC6yreH8dCEOOvFTgghGwg390clH8K6f+X1z8ZHLrvY4R6WDS5igMjeM9l1QCMnTWlTXclwlxMVYqW6E2A2BDLIajti2lpcAlTltA4FMVjxHFBp9YjLDlle28KysjCJWbBRBOl1qLd8lwH7Dx3C+gUJNMkKvKJ2I7+8fmJ0tyhoycWtdP5krIoqUEOzHAe+jxZUlc0B2upCIFfRA7jSZRVfSKjjR5DQLe0xOPufuO29+5z/8rtnfRZTXHo+PVVi7jbP3Qp+FefQ225iHoQo48fT46IqjkcaYmZfw1B4/SmZK4gQ48VRQj2eaT0ErKXhM9d1hP0w8I9Dn3opRF2PVg8SBK0Kiyz3vu18ZdIqKYpYo8fAlQk0sIZCxxhI1Sx4PiRfTeXNKLziJyb4P8picfIAP/fqd5h3/9T2udoReTBkbRaVJxkagy34kNJeYnBFli4ux7YqvCquKrB7NZm17eYlRcThhfBQ95siih0+cTePIbqln+RGxYKOEg95z0R7fGXQlNi/QthegIQ62iOASG068kXySxMPQ0lSioo+dxQ/M4tKiWVrqXPUoEeh2aPQDQdN+q+87eJQtQoFcA8RnSj7otHf+1O+Zj/32JxzlcYiRnX6twZNTEX2NCaUinc7+YLFOWWCEqAJp9Sgqmax4j5mCQEBuGrGwYj0GHSIa7vWUxApWHSJuXTgA256ceFjbFgtgibqIhZfpEVaoiLNvWwywQ4knZRBhISRy3JEc5nuserrgdG3SiQwJMj56wGpIPLaaQcVwhRWvekRUq8yUfIDtjR3zK//wjeb9v/JhRzlCDaV91UZbsyXRdfBeBXS3JBZs1RXaPhyUYEmneZ7J7FQmoDRMMEjGivUIOzhRfOAEemE5v+pJLLYJXYAWa8604qmDVXr0JthmFPRAzrGw6tnb2jP72/t21ePAKV8lHtaPjWDVQztqaXmJVz7V2wVO137k5dh7xUQMZ9JvWjfMF14ijwpLDImFRNQRZ04+ABLQb77ht817fvZ9VN521KuHT5f7PZOjLCnOtdkZY03SDAEUVR1DfYz1YuXp7wjFkmjf2EplRri2IAWrU9GMWa4uZMyD/IRbhMRim5AlMRTdF0uyMRI5EMrKnlsWGdAj4+Ancu8gRIFsQZ2M5BE8ZMBA9oWWXXWyfsbQwb59Km5pFZfbHDHdZV7VxhoZ0aiwgMFOXdSG0CFZYWvtrEjEmEvyAXY2d8ybfux3zM/8jV8yG2c3HPUzHNkePgy0HbUk5hFq00aHk8lxxIoFQ5bc6cWJlaSbKx4gEkE10eo20yEYQTSQI5F48LSv5Et8JBbbhCyJ4ej48CIl2RiJXF1xlP3eGAhB4okAUssUVpW7lHgO9txDBgxKKPhAYuI6IWMMvkFapsRjv4eUehvUhlIRBbZoNrQVOqQjEanqjSFdohEIDJhb8gFwlvjABx8y//6P/aR5x0++x5x/4ILjHCHT91cXYwM4BHkvUpQNGTOF0FJ2/JIY01s2ImTFO2zYZNEQjNjQ8SQq8KqHMo9OPG30STFydnvVE7myIjie22O/R4ZgJ34nHOmg2mOGL7dt71Pyke/0QMsuXwIbkTHxvU+rHlxu44cMZEe5yTrQV6Ua4nk+tNGCSFMS5XoBkVEUZY2nyBYjAxr9hoM3fP2/M9tXdlytDOyc9VNr5o6XP8O84o99obnhiScdZ74YGf78MMLtTBGycsaCIrXsl0wMqNsHUommQAIvUpR1jPAjlS/oWzL9LfADVMSY3jOuvIif1lI0zAxuKoIRKw4N9aXlBZrU1CWfHAJGQSpHJlpAlnlXQ9f1JJTYSwgeAacsNqBDxoooQVeUNyD0uBFgpbN1edfs7+7R/BZpLrgbbZrMVUfAB7lcW18hUXuvhznBhK2V62A1ZbofsXTwLogsoFHyEehKpTOgQ0s+MW5/wRPN01/8FHPrHTebk7ec4NdJrKytZK9Rj8FnfvIpaCtyy369izLMLlLdK/bLAh2QyytLZu3EqllaX3YcQlHVMegjEInlM/oDKVbOoCJi6X02BMXUU7HhWVzoEYx1HKiMeW9p2V7EOCgdTomLhJAlgZYjJ9BCxUkob6lTLESHjBVRgoHOuHkHiQerHsx9aeKBLdrUgcYlXd8/MMuryzTnLXOZObqfoifkaoAapGE+2woxFTBL9vPJh+3bYhVeVwo9Sg5XLfkIsCJaph2wtLTIZbvjjvCZiQOzSGd52N+nn3ajedrnPdk865WfZU7dllsFu2EYfuQHc0QbqrFyHqURb8n0t6HPcDJeJ0bFhmdxoSIIKHZOHG3BqocfrwavNJMESAh5UoZWhJbNTkR5Ywm1x2dDZmBHgkF13LyzQFl98/IW9cm+n7O8BWR/ti2nIfRX+cIUi+NglVY9nuz7yFFGhONtUCFQUz4tQxNKGJIPpHs0BP0hw2oozZSrnXyOcISnvPB28+wvu8M87fOfYk7ffgNR1BCkYjAg49GZGa2W5BgZvkZptA9kKjVspK4yCgUbnsyFgpBAsbPiqKtVDyM3IwR6sREHRfbFgmgWWhYxNIxkqT3+KjIDKxLK6nROnaSLxLG9uU3bDj8eLbAW8FceNMBf2iJ/mGJxlQff68Gqx7vWg7EWDsSIH5llglfTzBGrqMYdnypY08WWxxBHzs9R8jnCNQNWRM979bPNK//UF5kVuSxHozEYkPHojOpD1ZVieQVmFfiWTH8r+gzHD8UipYKNgBwfdqjq49OxQx33KaD6Iq16gne4xcd41YCDIvtiQbSIrHxKLJrt8VeRGViRUFGnOGMOIF1cndnf2+dVD68RlJotLioXtCpCBZuTw/SKBwxWsOqRfY7kMChZ5MJxMvxR4DOZBZTBnKyCkiQ0hAvw+S1RH6wPpUjIMdQp0xGOcHWxu7XLX07+b3/5583Hf+dus7ezFx4YQYUQ1YeqK8XyMQr8llqMqnyPj1hmog6u9lRfHlo14KDIJTPTkFor2u9xXJABeWBFQj12S/C6+E7Prq0nc6gkHvxNE48gfJMBCVilKuStA01RFlBSSTIY7MjGCCojoPUCX7jkaC87hqaVUMg4Sj5HuPY4c89Z8yv/8DfN23/83Y7ShhrDXSjJD3QqtYx2iOQQ6PDZb8NKpx/7uG8PnLWCUZCL/qrMEkKFpv0WCjKpF4UeuzlAj3UPOLnjhAibfpMBduEBP9mBzT147PT0Ax/8kIF/k4GT64D/8mkNzl9gU/tWW4AssRNOT9xIurH/GsgIHCWfI1wXwKWNd//M75tf/6dvMhtnr6SDtTi6HaMy+plV4TeY/ciYGW2ZFJo6JNC/6nGFnBzRsupZYgVeHoVQuWqqx09Gpumlx24OXo8KmGHpw7/JwHV1eLXU/QAP0TghyaxMQB0nB0sruJwcKBUBqdB+AZzHWJqrgNRCqkKR0YFIl5NN130l6TRbi3GUfI5wXeEjb7rLvPlfv9XV8ugZ9oKabMBrGSX+GL9AIt8xs/T5oLPy2lcUvBFXyBklWtNXU4DgZUJh1Krqo2xbpDZTSlzthtezBXxhl38kjk6K8HQbUwPbrkIfXHK7w+8VWr7gV2T510kb+x1cltCLo9Lu5VXRYM/rluS9wAQkuo5QGXoeyMQNv0fJ5wjXHT72lk+YN/7oW/ieEEMN4nA8u1pjkOf4A6mlbNGUigQS+Z7E0xMKCeGMmi+55eQ9rWwMfpqumgKEjK/527ZIVTJGeuzmwHr4Yw3gCtv+7oHZ2abxJxNtzh3RNNmLEgOX2nDJrZZ4wPHc2qU2MYwn5ZxGoJtDU6CCRHeEsRGiR8nnCNclPvwbd5q73vpJV8uhPcJLEgm9Yaoyf1hE/JZ4DmN0Wj8UF1iLY+tx1C2DP4PwfG1bhB4EGSM9dnNgvUiZss82Vj002fO9ntg26lV/B/Yhg8IJQqIey8S7F3xOPKGuF8vJT0Wgq705xL4EgWhJKMRR8jnCdQncrH3bf/xdc+6+c46SHAYWWaIiR/ywWlBWGCuRlW/Mym0fDgf2nWLFVQ9DMSKZLj+1M3ABGxqsoXRYiSdFhtpj1wGiXpwLkTL17S5+JG53l4q1fg5hp9sD2kX7/Gg13uHG3+lRQC0x17RPAq5zs6J6nodA014BXlcKnYYS0b7EAxwlnyNct7j02GXzW//qbWZ/L36apvPAqMLZqJhiVtUVMRU/L1o10OBqWEn9JccU+WBQ7PfTABsarEXVMkbI4CMVz1NzpBwSbZ7QB4qu4V5PMQkHRjQGBq961BysbXtkiQSvR0zEaP8nosEUnxOoYBAlK163w4h22iFeFHHEo+RzhOsaD935sHnwww+5WoTC4PfkiD9UXamgD1RYDqFEXp6oM/kQwA4eMqDDFUdsv+IY0bYw860Q/qpqHZ0y3maCgoGeVRoh0Y4IUsWKcm/XPVodTu9WqBCGf78BnSQtryybxeVFXvUUVQp2BpekSzIl/STxdCJvr+QlgjitiNMI9WwRSWJVukfJ5wjXNfBDhR/89TtdDVCjN4MSt65VQEvJ8afY9jo9yjzJ4nJb56pnCqBeMqEmxeEvocdlLJPTIVrZVMjhNwbQ5I43BqwcW+HfxikBmoF2RAiqaCNVdjbdqkfPmkonhLJAOrg/tIhX6BCpqlIBUhb05wGY0ZtgaNpIR5G4to3EEyMYNlqXGfiK7hGOcJ3j7rffw5fgAujB7BCQVCUUdbWMvoBZFb5mlsWIU2BWTcegmYjPoZfyP49tERFdtdtPSxB8jkOJ9hiPZXI6RCubKnBokseLiZGESm/FTzQjQszHqocfrd6R3+pxKAZnGW4eNfvUP4v4kTiKp/iEW9EWYcHt55oMId/aAVCXLYeWfhHOYMt+gESQKvz9IEs8Sj5HuO6xdXnLfOx/3UWlYCQHKHPGoW3HSdBHNZoCMyCXDTiIABKPfodYBSQEuS7ZLthJMbDXYzyWyehgji6bKnBAhh6SIW+WLHDsEAkhBFYsuGS2s7lrJ2eZoYt6liGJB5fYcC+OfxpbAopn+UoM3L/gV2RKELUe9amJp20/tRx+BzWvfZR8jvBpgY+/4x6zhxvBQPkosCjyHaPA9+QO+2WRTk7LB0AyeGAKZ9O9q54eswEaCgm7x0EsE9VRjZPGAObaYowprIK8Bh5qw/vb+KexZSVVNKYYVGQKqeAhA/9Ytp6LIxUNTjolZgadZucCsd/2oaMikMKQeCILShTUo+RzPYB2Cm5S4swJb3rmjcqgHcHi0qOXzYWHLrpaCDW8K6hLjbHRJxuiqRML4KzeFbHqySNUKk/oBWj5RDczMY61D+RMFO2kDL/CkceWo7kOGGEuAb+/bW9P/TQ2Ias3EFFCX9tPt+rBvZ7o0eqSf9YrMQuQZlvd8ch0WwKxPcX+oFiwQgHEnKOfVLhGwEC/6WmnzZNf8ERz+/OfaE494aQ5fuO6WcbP7NKOwiWAy2c2zCMff8zc9dt3mwc+VHji63EC/CT7y//YS80LXvNcR7FIBm9mNFuSYxT5Dhm+hbNS5AOOGckkKjFB6n6GwIRLf2nDPY1lPD3VMDLuKHbQOoF+Zmqc0X5bPS/BCcGxJAnhF0HlQQO8E3CbjpUsCk5Btl1NKx2yv315l5LPXuXLu4MhLrkqL3QontVjq2ZxxT7h5vdh5km8yMoowGxTSwQyzWi3bAqcVW8kfezAszKMx13ywe/GyFNDaPrOVWwLBvrq8RVzx8ufYV767S82t37WzXap3oGP/c4n+M3P25e3HeXxBawGX/CazzZf8qde7igWyeDNjOZgKo34PfoWzkqRDxAz4ifiOX2h0VBgL66OIxM/Q47hGh6lQcXWQlIbsbyqZ1LPePuA02mrRoJySNDn6toKX5/Bo884Idvd3uUn3arJp+JwcGETDx4w2LqEn8YuHYeDMS5p27RT8GXSFTqmfeIBX8sQhmrEmCfEdKYZMWluUfg3qQ4WpQsC6AAU83GVfLA8/vZ//o3mCc+6hesXH71s/uv3/ndz5fwm1w8Tz3jJU81zX/1s88yXPs2cuOV4d9LROHvvOfPr/+LN5lO/d5+jPH6AffeMlz7VfO33f5WjZAZ5diSrqTTi9+kDllE/UhxTyWTFY6KWx8VyV4ev/KonqNhaSGojJ080S84wx9oHvL0aIgmpqkMDPz2tkw/etwZaMfkUnA5kW4ILJBwknuQJN0asQYht045ZPU6rHuwjueSmZELxWLkBiPdMEbHZSEeqiXch9PiIMbIppSDsEuAaAxPLhLl4PMjHAh3gcl8Ffg8TGNCnn3yD+UNv+BbzLf/g9ea2Z91q3vM/3md+8e//mvnpv/oL5r9938+bn/8//qd58795m/n42z5prlzY5C+4lc4HbnrqafO6v/mV5thNxxzl8YP9fZpkNq7FSY/dF/XjzXGVUFY+Jjbk03s9oRTXcooTYFN0xtgU+6QzWi1WqF62KqAgMJCHEhL77vZ+NfHgr9cYVBn7lGzwcwk+8QTCsXik3EKPeOSPkZlDc2Jd9nPIGmsAMRX0runK5zhNos/64mea5375s83bfvxd5t733u84hwMMlO/4599gbn/+k7h+/sGL5if+3M8cysrnlmfeZF75J15mPutlT+ebkW/59+8w7/nv76te5sMluVuecbN52uc/xbz4Gz7HnLz1hOOEOP/gBUpev2jOP3DBUT7zgQniKS+83XzD33st15NBmx3Flsh/I36fPtC63OYYit8S9QhUB2bPqseXAn4HInlbpb85O2NtAwVTISIJV/WrB5wk4g/lXr3ywQoHl92yK5+M05Ckata02by0a/Z3dfKxMoFeZBdV7BNorJ2gVQ/tJzwlJwjFI+UWYnEJK0bObElWY556c8DhnvpX8Llf/wLzp//rHzWv+UuvMk+nyXbKZajrEXhi7fO/+UXmj//b7zDPfuVncbve+Ia3mHf8xLub95dwZo8HCyD7n77rp8xH3/xxvtQQ48Yn3WC+4A98bt/A+QzEmGMhJ9uvPy7x4KMkmpziBaqpVvq9nkHS01O1OpR8085Y2wDpjFaDDnUOEsDqMby1YNmsrC3bsd1rDHKR7FBNmfxbPVvxqsfKRGYCCA/xIvktLVPicQkTf0PdmqUMquKOmTrpw9XW68Q1Sz43P+10cNnrMyH33Pz0m8w30pn5q/7MKxzFmLdTInnfL33I1fqxeXHL/MLf/VXzK//wjWbr0pajDvjsL7vDnLz5uKs9zpE9QMr3ehJMOsD6lCA1JvFAFmfTfMWNWfiTkQwqHVDyTVUngElWb1XFBnuAkpIifSIJ4MSNtxW7qhF2c2qAoBNCUfR0yYPk+AulW3uchCzsWEmkHUHzZP8g+cBO2i1auhNV8ZQpHjyn1EGB0EhM1euAhHXNkg/eVKyBnfjpDNzb+aa//7X8QAGSKg7W9//Kh81b/9M7+T7OVGD18+N/5qf5sWueABxOUOJ5ybd9nqtdYzRnh/kAJygLy/a1KsG2Em2eZ19tj8lMb/iRL1zSki3R5w0/gUxy2Gii8ZvQ9IYJU8uobYX4K/hUNJlkl1fJv6JjW1lbMqu4H6njdu2wcbvN+fYyrY1sD34HfYlftlAOjzQvsz6fKLrZwicjBxT7Dl8rxfq0YvD3SphI/1F3dCEXMeQNLuMj1MlbwJUIrHr4UhnrZXw5Y+4jAGJfWbFPzCLcEBEh4WdQlAHDMdWvgvaYZHQLDmAV/Jmg24KY1aav+j2fG28/ZZ7wnNvM8179bPOcL73DUY35nf/wDvPYPfa3WzBAzt133jz8sUe5HgMHAi7V4RFHjX1aRt/9rk8N34SP0HvP58mf80Rz8pYTvqO2N7bNJ995r6uluPH2G8y3/MjrzU1PudFRDNv88T/70+biw5ccZTY85YVPMn/gH30dTwaCzYub5t/8of9SvJz3zC98Gj+Ng138ibffw9fLn3DHreapL34y7wf04+XHNswDH3rY3Pu++4v9lgNWefiO0o2UdNdPrRPlwGxe2DLnHrhg7vv9+825++d7Pwpnxk990e3mG3/4dY7iIJOQIB7hgJIZXi1lhTp+7ZfhTUQvnSzrOk5GPtYRNo5EjP0l3EdwNK3Ack5Y+F5uBNJXnwRuPPi+iwPzdNKgDTfdJVG0L5sP1jHuRB73bPZ39yjBLfPXICB3QH225cb02rEV2vcoZe75IAzS30ouTQ++NOCRbV/e4WNCNW8AkfPaRCcdXA5FTKlMREE1sp+QSo6EwQFbjZJo0u1Fm3nET0vH5qaiGQYJXPXkg/shX/7dX9wcrO/9hQ+Y3/iXb3a1EPjC4Xf+y2/iCVAD34L/ie/5WXPpkegllA49yefJn/Mk8x3/4ht9fFi1vPnfvN28+2d/n+sxMCm+9q+92jz/qz7bUahf6YB84xvebN778x90lPng+V/1HPPV3/flfBYs+K9/4b+b+z+Q/wLq9/7Sn+Kn+nBT9Zd+6NfNbc+6xXzhd7yYz2RjfOq995tf/Hu/2vXwxYu/8YW8D/lsOAO8quS9P/9+WvW9a27fo+Lk87m3m2/64dc7ikM8ekuTCoFFORmg5IQ6R/8glip4SssW8SESh6f1kXjk0Miac8QsbxKspUq3pZDEsY8VBOnSJ1/JICM4bvyElgOJrR6j1RS1Ex4xVpBUkExwr4dTGe0jeXx6FQmJhlnzgYMAiCUNAmNo+8qu2dncyX+hlNTkI9cEJFv4xvHDCdgLOUVAFQWalKgEjkI76Vc2I2i2U21oMAYvriSEonKGMRiZAKucnz0OEb25jnduAbCxRxNqDD4Liy7njQESzzf90Nf6xIOB/iv/6DeLiQd4CungiT2Nh+58xLzvlz/savPDh379o+Yjv/mxoA+f+Nm3uVIKJGMAB9pXfO+XmJf/kZdkEw/wtM97svm2f/oN5uRt+SfsNC4+Qqu5SjdjEnnpt73YfPVfeZWjXEUUDiAfLvOd0PShMhmF8DgUnOHjNkRJBkKQO4ywiz5zoPGHMYjjBJc3sRrH5UJcruSVZOsYd/qDXNn7tLam9pB4kCBxTC/kDgHlKBcNYuU3aSPxQJiFoFSOMOZ6u1mVkFi2GiF2UoCIceyySXEMpuh4hMpXPflsnNkwD330UbNx7oqjWJy977x55K5H/Va7XIUkc/Ghi+bsvedpME2/n6KBx75f+9dfbdZPrjmKMe/8qffyZF8CVlJf8Re+lC9tCRDbh37jo7zaOAy8+2ffZ3bVmR4u+RXh9jMmiRM3HefY8NMEn/jde/h+FJ6sw1mg4NZn3mxegqfoGnjsnrOcgB775Blz1+/cbd7//37IfJj66bFPng1OCvAI/e0veKKrHQI6DwIv1ikfY1CrGJjZNhKPnZ5Ay5mb6KICZ3GEYR+bK0gSweSOS2d834iOC+SVIbk4xIcEZFwxBujY0CO5ZFCEKGawvbXDP32QWCzIB6DY+ZI3VFk+UkLVkVQxRMzwYQxE228odbRa2yJoDbD0pku66FF0pxhKJ1YvQ5ylGlc9+Xz0LZ/gtwp88Fc/4igWuMT2E3/+v/vtXT/9XsdJgUeSf+mHf8P89F/7BXOGJsKpcMc6v1ftm3/odeb0k4d7NvD/jp98j6vl8ZQX3c4TtgbuD937vgdcbf545K7HzEd/+xOuhqTZ/8TbB3/tTvNTf+Xnzc/9wC+bX/0nbzL/jcq/9s/+l+NavOj1LzBrJ4dkmsOFBy+a//GDv0L78ef481f/6f8yv/xDv27+n7/4c2TvTUHifeWffJkrXQ30HxK9oiMs1pExJCR8YsWDDT2X8xnP47PDGRxh14tmdOykiXsiC/wQAx6wQELyCSin4z41+JDEHzDHNrrkg2a5XfcLpcMTbg4dLnA1BZe67fvbMnvIVfFRMhfeZyNwGIMGl0rKIzBYFChKyrSIuiSB0xP1nIkUbcmrf9mNzr7DL3Y5UM9jcMgWPw0XA/cS+Pc3Wh1XAXzgvsWXftfLg8tXOIvHfZ7drfQ7NgI8bomHJmJsXtoy5+8/72rzB/rvbf/5XdxHgL1J2wbeCP0///FvmnO0WpRBjlXjh9/4MX67ggDftXjSc8uX8gDsv0c/ccZsuffMYV8uUxy4/v6RN91l7n7np5gOPO1zn8xfnp07+o6AQaxTXiNUqRho2c7wYxJWPS0z88M4T5D2Gg1VjC1sOK5w+ZVXQTRmWQ/HquPLGGToMoA6C9mqRqAngGi8qiJYUfsXLw5N7OVsxXAyePovq+BIVVNZpiXiL5cCmWmTWujGWw6K44CTh7x6OcJ+Z1c9+VxPQAL5kj/1RUESwarlTf/nb/NEWgN+Tx838GM89JFHRr0G5qan3mie8JxbXa0PeBJw7Fuua+355LuGZAGcuLl93wdA3F/0h7/AfN0PvsZ8yw+/znzLj3wdryD1gyBITGNWZ9ORDnhPiVl9x4bCaIUqYms4D8NJVNHLXN0rYx12A5ERcciKZxmX4igJ2cRDNKxCdvb5ezZ42GB/l04AlxcMFhTb/PCBpbM8bXiXGx4owIYkhmS25x4ysA8gUFJx/SeAZ+vdXg7El0lxssYrMaYTfKEO9kmJx7+1WoOqoFRNEVOFZkHLoECvamAsAstt20lwpEI6suWQUSFAuOUsxOM6+dzwxFPmc746fEU/HhveoyV6C7iPsnoivTyFxNALPKjwJ/7DHzR/5Me+1bzsD37+qFXc/R98iA/w3sej5SGKHOJkiaRcA17787of+EqOG68Qes6X3GGeSiscPHqNR+DxnScNfGt9rkjGeDroxx0GKbr1W4IRPxZHXe71HD66W8UYpMfpeZAaxihu1GMVZN9egO8PLXIS4YcV1hb56Temo06TPO4b4XFmbPJdJDzMsLJOdFpFy8MNXnZ9xawfX+VLY/aejnPugASFmqcMrDIgQwkRD1DAV06paSYRQNKxiccjkZkyFmBENlV01SKUK6/idXri8FpcG4vHdfLJ4dZn3mRe8UdfWp2sBbkn6y6fDR+kKAHmP+drhsSHV/HoBxdaePijj/KZWPzgxhTETa0NpeOnj/F3mvCG7hiYaPw1foXSI9lXBXE4HcdJKNKhMAO4Z6j/i14Oy33FLlgD25VmiANjAquOlbUVThBItnhYhzcaG+DxWx2ERhue0MQmdSSqQXaoM83Jguc88l/UcXK261Y9TOtph5NBIuMv2JIvvepBKTPMQzj+cGi5pKOPtcSGZvaAI7FFQWKzDm8hMFWOw3IC4fFw6kfJh4AXdMo9FOBzv+4F5tY7wgcJYuDGozzKrIGDoQcYvPjiJx5QwEoLK5lt9eRZCxcfvsiX0i49tuEohw8chF9LKx48ZIHkjEkFT7i96f/+HfNfvvtnzL/+zv9s/n//2381H1MPRMwdyZhPD4KU0o9Rui3hiJ8THybMw0Zfy0IpqoHQp1oFxgq+J4P2YuzjUhjGL2+7EzbouzKesNzF5154FQB+tvWVgZ52kAzEoIsxjlWaBuhNOxE/We0ALRtVZILIkHIQMX4kXhO64YRH6SiQnri8bpIP79RrADzijSfA8KVWAa5R497FsdP45n4eGPD6xrqg9CbqHPC483/7vl8wP/PXf9H89r9/+6jvKMkP4j129xn+vBrA+/huu2O4z4XV189+/y+Zd//07/N3m/C2BFx2LH3Jd/5I+6vag43uHWltFEqW4lVngLm57zMUSs2v7QKc4GEVgruP+Nza2KMTLmzDfZ2u7cpQxtsNLG2bE5H0J5Ic+wINCZ6a00rzmIOk1fYnE2iFRSeTWPWA3jVHKRn4C9KOBJC104oO4ChsUSNrL0RWM6uXi8Nq8x2zrKEOQNWpC65h8gkb2brPcBjAJauf/r6f5+8Uven/+h3zqJrIkUS+6A+9xNUyoF68i87w4yfibnnG6b5xRMCgxqR973sf4Cf3xuCzvsj+VAN+YO5q4diN6/w0nOCT7743+32sFSXDmDJYJyBxM5PfhnLLtuKXRJPHfjVmil2jz1Ao5Wpzi8EC5ngCx4qCTvDwpdqDyoMwWbAR/+HgStSdTKdPXDLz3wEkYuuQVFfVWB77Bg8aIDpmaX4JgUy03pEAKnbAyrMLnLICQ9hZkYregEGbL1dqnVaHEjjZyOZoGtcs+cT3BnDz/2oDj2vrL6m+9T++M3jY4IWvfa658UnluPD+MqxeNG555s3m2A3lFdM8gDOy533lczj+Cw/N591xPeD7YNVTdYsnv9C+vsijY6B2ITeCS4hlG7pjTI9BzW5HV86IgveIHFZdrRb4SMBUYI6Ofb6ktYbvAqE6zln45pxBV0qwvYcfiuOVUL2ToRMkHgLiwYMMOCHmez0RP4tApqBQtDM8hZdGO84WyLKNh3iPLMTGGuOWtTsCuGbJB9+Q14MO91nw6PINNNnf9LTT5nNe+zx+c/OhIhqYeIP03e8YLqXhAYBv/2ffyD8aVcKb/+3b+cfdBJDVX1adN3AJ4VV/5ovNjZSs733/g9XvIs0beCpub3vw96xXPJO/H4V37WHDL62+/m+9JnjBKrB2YnhrxPwQju6OsV5Eqtuw1nLm+DWxq7PqqQNuQlcdgY9EyRSOfX5wAI9hdwLTRTZegi+hW0kQJ5XSw0FPKwO8qgkNMh/H2OIq3lpNlZifQyBTUKiQ86wCJ0MWUkZ6JCIrrloZqR6iydr8p41rlnwe+OBDwU1+TGJ4t9i38/b15qv+wpfy8/xXG//vP3xj8NYEvP0AL0MtAauPd/308O63lfUV89TPe7KrzR94q8ILv/Z5XL77Hffw59UCLvHpp/luecZN5pt/+HW8z2TfffarhjeVC25++mlXOhw0x3rnwTAXOF8tl8UT8rnFWjeUcufm2KNlEasKPP3mX9RZQZIkCtb5Xg+teuz9n6iToeJIrJ0xgaSIy214eo6zUw3Q9zaCShODNPlxakO0GTuDAkOqGcnZUTOsujSJoaaXgE4+XOmq48EPP2ze+wsfDBIQ3quGy2/4kiMGUeuxSO4HPcBQVNUc5EY9wDciowG6fXnb/M5/fCcPXgF+NfTWzyo//fZ7P/d+84Ff/Qi/5Rc28Qh1ct9jDsD75171v7/C4B1TWPHgabkaltSTOvhSbBFRH+C18TngjQa/+k/fZC48fNFR7KPXt95xCz+IcOIW+/64j7/1k8F+fSGtYrEqmh8aA6P7AMiJjrOdk265LyaeuaEcATgpV1FawXcg7yMPSUBYBaUJxtlK6AXr6Feypy+lc1ezERRUMWMCfnD84rL2uBcU98tCMpBGxY+HhGvhSMLNSMwPkfF4qGZjSAg1DMLXLPkAv/Wv3mp+40ffYi5Gjyzj7AOvg2l92RNPpOCG95lPneMN7xyrfZNf7OKFpCKfu+GJR4U/9MaP8pNwkMODCfgpA/6mdgG/+Ya3mHvebX/zB0+FveRb5/tDb+s3rJlv/cdf718DdOZTZ83lx+pPleGeFFYrZ6kNeJy8BDzuDTm09dz95/kVQSXc//4Hzc/+jV82H/i1j5gt0tO4j3h419sv/H9/lfct3iAM4Mx27cQcknFm0u4e8xmM1lUKwyGkQIQem8VLbj3KTZSN5DmKOgf/o0xAmDasUDBGYm2uBSSnUAD6FT8Uh8ew+cRSgTWFVDJB8wOveqBbduOM2WJdMEQiqW5eDXd9FIiEhAhOhjtfVJwIK7zX5tAdWOrgqv+eTw64LIMfmMPKB2fMG+c2+Ifl8B6yGjBocbYt361BU/Ddm9oSHveR+D1NJMLyj5B8pgtwMJzEmbxj4f1kmJz1iigGngb7uh/8av7RM6wSfvJ7fpYT3azAk3df8T1fwl9EFbz35z9g3vijb/bx5XCC9PCoKIBVWekRaHzz/NiNx7hPcOBtnN3oevoOP7qHV+ngC384gcClVP17QHe8/Bl82fKed93LiXwWIC78ns83/4PX+30HJM2PCYnAgJRVEQYUO5EkQkObgVVPNvn0u66gLOU5gYiqNB3kZp4QTRMxlAL6Bat5XGrHcc2sxGDFA8Kj2XHz0jafgGK8yJUTr8UytghoaxhOWPDz05yRXALm1QRCZCWZSI74fypRO67njl5f6BdBd3xlwesi+XwmAQ8cvOYvv8o858vu4Jds4m3Ps+CJz73NfO1f/4rkh/PwRu973n2fq33mI0g+DjxwafgGIzgezYXRnZILggLFzkn2HkX2R9Qy6HdfQFki4PiKoraNEwpxO3SZ0IiCQvLBlQx+rxt8JQbrHjA+7A/F7XKZaaRS0orpOGHFS3rxNm7EUQSzKnyFrJQnIkaq0IeNdsBVSzxj/EiQXTp9hq/pZbfPRGDF88s//Bu8MnnWFz+Tf1LgVMcPtMXAzfwv/hNfaL7jn39jknjuec99vD3egeMBZ8lYSSQ3l4HCMTDLsZ3T7bWXC5ExS0AN5E3P1+Foa4GCrWDCxb1GvveTTP51D+hX3KPhez3oY4jTltNyrADiGyv44d1wGTCrwlfISnmiGwj0oYcE2KMTDyvZYjfG6kiQTZ1xho9WPocEnH294Kufa17zl76MLwXiy6QfedPHzCffdZ/ZKtxTwZnXM17yNH6aDT9rcOz0sWRSxf0n/PT31fx+z/WA3MonBx7N9Ic/cYU07D5GOuAbh4Bj56SY1lAX2CTpKoJO3bpYnpulMlFx6oYVMh1J6FbX8EqhNsY6LpnhjQXDuG97gCx0/Kono1KzgsSFl5bi+Cv+lAuT27EUJXIMChWt9Ky2+QFaNr9r8hjjoxvTjB4ln0PGE559i3n1n/9S8xT3xUvc0zpz7zlzkZLH5sUtmiQP+PtENzzhpLmZVjt4+28Nb/vxd5nf+Y+/e0iD6PpFb/LxoP7ByOazaPSVO0Dz3Zanejh2ToppDXVB9pJbp25ZLOUUZZmhuGWjGaSxj1IXeKW8NhIJkgh/zYJctuZV8LGfNy/jXg/0LV2jFifGB04KVuWBmJww02pWGtwcM46zbj5ELNvqJGCM/W7Ydzj0uM/hKPlcBeCM6jmvepZ59Z97pVkb8ebqGL//Cx80v/l/vYUSWPmhh89Y0AhH8sFvBvWCn87B6KbuwjDPj/TG8HfsnBTTGuqCw1n1pJyqSd0Bnb4HhMGPVge8UlkbyQdPueK3fVCuTmxkBm8h2MK9HtrkXo+gGSMJ4GlXfC3Crnqi46ojXqDKLTF1qHXzA3Jy1Q4ikA7UWmJjoV8dNNX20T2fqwCcyX3wf37E/MR3/wz/fPiVC8MTYT3AaglfZH3jG978+Ew8BExEq8f6EzcfGnJ80CjHxMRbcKQMB1ANOanAfgeSxNOJsouUUw1nRKwtTDLlleramNTkHl71tJh46NM9f68n7OBmjDgZ2d+3P9eAtxng1+wSwEor3gpKTB1qM1BCO4wErEJ/8Dlx6GWB/TOPxAMcJZ+rCDx2/T//yZvMf/7T/828+2d+v+vVOA9/9BHzE3/uZ82b/83bytejHwfAD4qdvO2kq9WR9JIjYH5CAsLZMp8ldxw58+hxTKYJZjKcKo8yN9E31CapeqUObZoxZR8V5R35gOSQeA527dNygqYXldXwlQreP7FSNfNZ8apEXb3DACEro4jRsBIOh+5E5gFrLkw6k+FiQ4xHl92uIfCdI3wXBr/+eeqJp/hlhtgdOxs7/Ps+n3j7J+0vltYe/XycYO3kqvniP/Ey86LXPd9R8kh6qtV1xLeX5FLB0pHhyS3bDlPv9ZRFQk7TlBegQlM4D7xQfxK07yasDFY9+DmEHUosya+8OjNySXXr0g6vWnilZFl1uJ2K/Y23nfgvP2vl0o4n9PlwnzlIc1qGEr4mOCPuw3MyNqPe68ZgKmPUkeJdUwOrRKaOks91AD77ouU/zsCwM3Adms/ojpKOB76s+vV/77Xm1meEj53HSHpsRBf6JIT/BT1P7rSLA3TKyqfMTjlVU55JhapgGVZtwjSmfTcxyKDPsMrf2rIvB/WeSUSksDLawW8B0YkaVkpMx59amGqn4rs8eA/j6vpSeEWhMh2WOQpdQg0ENmKDtoHJ2wYKfmvdEWMwUWmEY/UkHm+lYO7ostt1ANzTwQtK8R0hvFsO94iOEk+I4zefMKdvP+VqeczaYzh75u+a0IZyjCn253/JLcRhjhLYnmzfK7YspF5Q40tv2AfCok8pSnLCygg0pguzBJ14qIx3HeJXSoMvlBYSD6gt84wuoQoCR6lXoQTUVMyjIz9ENivGKqwYXrShc5R8jvBpgTu+6Gn8QtUSsmO8MvCroKMWScMmInsI83Ek9jrt9pwd5lA2H3KaYWiBzpgFI8ULmGiF1JD88Vs/YkNbQmLa3XG/14P9I8xSf6ukgtLBvjEr8b2eSuLpQrdgDwZjKMnmGyjtnMHnYBMIawkiVm5ciwUvWjFnQceXKx3hCNct8Cjsc1/9bFe7CnAHDg4yTIJ4P94SJyNLL8xTCXKrp9ZBWWaHnM4QJmFm22ygZQX8ugy6D32t+xs0rHpwWZp7t+XGCeAvb2SM9+cKVj3MAtEVQjRNC0hQ7E+CV7YFqTIphyrTIjPyMnYbhjrZgUhDZ8A1/EmFIxyhF3d80dOrv3SbHetdB0AGBT1cAlpeoo2SEBJRD3K5p4bekDuP7UmY2m0ebKBlpcML9V3ysAFAfe9XPZqf7WvyY/8P2DP8YmF+mg7ZJ5N4QEmpBcT2J8NaKdvKNjCLWBI2Q7spJUCNTcaz7JpOAUfJ5wjXNfCWYfyq7bUGP2hKBxcObKyA8AQbLtuUEszYez3143bgdh3fsVCHEkS6bNfABlpWOr2wWNqHuEfjVz1VkAH73wOX2/Ajccv4vZ7M93ogq+VbwHjQ8u2YIniHw2pnXhB7oc0OLw12FlN0CEfJ5wjXNZ7w2bea21/wRFdLkR33Ew+GMpxB+sCUJeaRX+y9oSEJyYl0kpQqMVVYhIFbl5uOudjlhrcsjfBE9tCHsrphTepnfMk6+ZXSoK9tHHFiQAUP8eDrDLiXhESkEch2QPbzZLA+n9J0+I4HUx2pPVAaXjpEkjB6dDIQtaPkc4TrFvzLrX/2le6Hxq4CRh5IIo55EAlIVkPZVU8BdZcDtzu0iW2YCV1GRniKRHV1b2uvYsoyksRAddCw6uFf942eJI3Fa4BsLvGMSg+s35N0ZgVHa4sldIgkGKkj4rHaUfI5wnUJPMX0Jd/1cv5piRJGjP8Z4LzQR48/viS3TAmIJjp/cp5e4WH0xn9Y7Zyf3ZalEZ6UKIpS5Sfctvd45cNPuAl80U7m2cRDH3i8Gr/VgxMDkWF5W+wCy45RyIH0+1Y7glFpzQHWOzyMaQvCGGHWbxX5o+RzhOsSn/2qZ5lnvfwZrjYCHQdHFg29XrPB1SCa6DgJ0RYfvG17VqLXL2OE8Ci7NdRmF8a0oHJau1j1ZOEm81iJ6iAhRDwkgifcZNUTi9bANqSQQXd6IP1x650Oy4E5VDrtjwkD6JBPvDd0jpLPEa47fNYXPd18zV97NT9iXcLYY2carJfm/KoQnJUTWHWJ6EhCtCLCEdc25/zy306UhDP0UXavFlxQ+IjjQ5/ube+b/cyqx68iIiXsM0+iCn4oDgkIuScSrWKw4T4jdKQHho1njOcOBMF12kYchxBGYFITKh10lHyOcF3hWa98pvmK7/lSV7uWmHCERgeat6AORCQhXg1FSSrGBO9dmKvd5iw2zltRmhi5VY+fzCNFHRaXaTnKbzOwpG54M5H9sbBvMBtrpDI+vDlq0UJfqxACb64+KySExJ4m1If4UfI5wvWDF33dC8zr/9ZrzIlbjjtKHsUDqMhooKKnJ7IW9BNYWTUQHYMfUkAioiSkL9VBYIRLi06F0XaraFkb4Y1Es9LUL+hT3OfBK6gkYUP2YMFpKEWmx4aIgEer8Tqd3ldWsR1bVIXxYDutATTWPssj4SDxoFyHjcGWBR1qWbAttyUoMso4Sj5HuOY4/ZQbzWu+78vNl/3plzvKtYY9ikYeSz6JFA/OHDDB0qSaJqH5YmxbrifgQYOwAa6iaFyMG4k6dSpWPb0ITMT2InD+K8gweXLiyQwE9oU/7ZUOi2KTwoxomskxG2MZ7KPkc4RrCjxY8Aff8C3m+V/5nK5HqudwLB0KWpfRqnCNwlk+v0+OPoPvsdTQ0SHz7zOyWDXa7xETZEnar3rwpVLqX8jxulB1jaXJnxBYdWDVs9i56gkkWuLCj3YTyMzqTTyt3cwG3UrHKxEyeiwaFFL0jSr0sl2BF8xYNAXKgNrRTyoc4arjSc99Av+O0TNf/nRz6zNudlSHytFRHevuhLDv4FJIDFoC+yo6S8G/2UP/Wc+SBkTEJMZYwQng0EQMPBGUgknerT+ANbxa+4y5DjlPRUCFWBg1Xgg2gy3TBCQeJIwLj2yaHfykNvoXwiJLxcBTUBmwfmKFL2/GXyrVSFQLtjyIz2FEcVs1+ssNyzRKoO0nYo7gA9bCCiQ2cNQaoiAuyEc1KNmUA0AyL93yUVKL9a568rGPntpX1vPZovZeCvpaY1wXZQEL12vzgDk0kcGThjPGJ+/0ByuaW55+k3nGS55qnvdVz66+p605sBsYrV5QAHnM/uK2ErJ6ipi4K/i3EEs2CSVgWt5A1exE2GgwPSUtVOjzXAmdweMIv+mzsWPHE3cwPu1HC1DBpUxeTTcGN7i1FgXQppSSkGlCdaWp6GtgVsIH4T4jxOQ41Ib63HFVkg9eafHkF95ubn/BE8ytz7zFnLzluFm/Yc2srK3YBISBRWGMDKUINlOzFbFqXseFVLVEm23nPAAzw1lKBiPd4Cxz0qDD7nNFgN26NmLfLq8tm/VT62b1OB6b7vAwMu4Yo9UnNTqE3PvuQSI6xb830thnc2hbjHZTJzqNDVOnwhIPJW1S5CpuRA8JDPLadF5NSxSQU/RqxGR+h51u5CNNwC4jvy1VEk8jFaVp88CUlh9a8sF+xy8FvvB1zzdf/Me/0E0+1wEyrZ3ScXnMz9KnE/pGkBKiotR6B3rVRZf/PnhTI2zirQZF8TnGloBs4wSE+9/OtpYuCHzPHsjsFhRaxqgpO1t7/MOKHkRjNa2Lsmo2ijiRwn0e+Vn6bowQ9WCHIxWjmGsILMd6JbcZ20wi+ZJKL7i5tjgzDiX5HD99zLz0219snvcVzzYnbz3hqNcJotbOqyMt5mvt0wXBzxBnYLlOBh8l8cIBWVNhzOV+j11HelLV4QAkHqzwsrcUAoMWPkYUZr0NQ2DzdAjjfjpeIYN/1gf9DXzPy1kORYZHINEQRw7d3zPmyqUds3lxyxFJDQ3TugU7SD442cXrdPSDBlW3NWbMsx1McIyqYUKOr23wFIz7NnYfVc3FfcBQ93xK0NN8dalev38kJHwiFN8MD61U84Mw5pl8KJLbn/9E801//2vNsRvWHfE6Q9TaUY1vYr7WPl0wOvmMRFVlgj1GomeTD2OETb6Z7coJanZG+BgD/OQADmmexsjHMDnMwWHWRNtuItERCn6zBysf/LQ88of9VVOC1s3YQdux6lk7vhzwmy5rAjGPO9URW4ZzfL9TiOnNgNgwVhQZ9nIWrOMUG6IskPGRc5s3lQ0wi46U2Ye1E6vmK7/nS823/eOvv34Tz6Giv9OPcP1hyt6Lr3JdD1iioPg9ZvSJ26mHi3qvgZtINDrarnooeVLGQZlPjaUdWlfK4OmNgO/16B+ia7isC8Q8NuuIBT2QmVXgWxDT8atiGt2CDug87sBeRWpcJFrSHnpX0OkHim6bS/JZWV82X/kXv8x83jd8Dt9gPsIR5oXqcO4Y61lEemrNMw40uxc1p8Y2C8invfDGobmf/rbbzBjZnlmav7dH6zaeNAkUOueRnMGoWVDBqgc/k40VEFSacdQEmsohtL/ilS35Zqr97+XnDuk/jdowwDVNpyJx9cWmJGG/tSnMnHzWTq6Z1/3NrzLPu5q/sT8n9HXuEWaB7ePPvJ5WJ9bjcWjdMaRRfGJDnNgwIdu3KEwIPBtvuRFFTlmFgdh41UPJx8eJD9KDqlcv2aEJFz9nwffgGr4mgUNyhiP7QbXk22Uk5IWSyMxg4yOtu3hky0HGEbqAu0FLD8Q+ONWZkg/OMl72hz7fPOsVz3SUIxxhfigdCDMhMjp51VObxA8l8AZowmm5xYTO331ZXLRfcZgz4H+WpkN3d9c9GCHhEVFsBhFH4WO+tb+ltMgPXnShJpbl5RViaq1nx+aFPpBRGK4ZzwUFcWwuq/gEk9kYbF6UHIRXgojrzWGm5PPC1z7XvOQPfK6rHeEIDaiBd31ghoBaB10J10Mf8IRCiYhOHpGEqquhznibYi0BCmF/f59XPjoeqAVzaskOCS1S4sEX2Lti7pHR0NfRXBEfiZmSXdIvsXqQ9SWoGUZXxrs3Y6wyApx8RqkEEaVNFZNtcvK56amnzRf9kZfyAD7CRGT2iJzAzLyRuUOxm9lKqLCuC0yOr3akXotG007odQs5LYuJHgkI75SrJqEKZm2y1V8w+7zqGawFceKPEKIw+YFdotm3GTjiLEhspEZLbvI9OC3xQEe2LIQBp6VNI2cslonBB3g+ArlNFGz0R9drmJw5vvA7XmxO3Tbf7/DwTcLD3tS/sJsKG+nEW9ZuXrSy5Wx0bnictrDhKSF+3JbOIEnUbSjPeVM+62jx82hqTTM7F0y+ZHUoMdM+cKUWqnLUJJ2EOBE1DIPd5bsixCxyxePWrXrELg0z3lq9jctsuNyG2DE2m+gQGRAGwDG5coIMA3uHyfhTVAyhxeBabwESQgWdvgX+OEe5sAXIEusYnXwQ0FNfdLt54dc+z1E+gzGyM69XzL0ZHQYTkeuhL1UM/VN2hNoBfw3a2DPXAl1iTkiSj14NHUbTxCZ5o5OlfXsy42lom50AXQgWqgxZaIDPqx4t14mkXQEh5Lb6OnbvW5OLCzS9KUg1Ik9HFDeqvJEDX9Zbz6DSwXWI5zA6+eBJFLy94AjXN4bxMHFkXENczxHLZDwah9CorkmCMNk1NZVXQ0v2J6il6bDXbTMjGOhjAqR27OGSW9S3aJ6nRHZslaZ3SlhLS0v28ermKpzgRPCBLfAYqA8Vka1CCbA87hHBuDiQsqYJmsb7wb71Rn9iWh4QdFsv6gabGJ18Tt520tz2rFtc7QjzxAz7kTGr/vxxSBHNaHYm9XjiEFzlzh/WCHV0h9UQxJc2+beGxtwbytiMSVj14ISWn1CLzGI1ZAv2A3wUbXWwhEtuXncgF9EWGST8L1Y0lLhLEB9/dgQhGCFaAkzIFqBmW9rFIMGabA5j5TMYnXye/uKnNH/m+AiPb4w6CK4WghgmBhQcsCMw5/azuavcp+IOkyw/rk2rjVGJiFAKeW8v5YCC+0AasRRWTLwqo+TTswrkE3tXBuL5VwPVrsSjjFixmnAEEmU/ja2EKr+mGGOM7BwxOvnc8YpnPq6fcLtG+2kSDiXWQ+6Aw+7fWexPftBg7uhrxbz7Eva0Tbkv1JOEcrFAh1c9tGl9FOVhFrLMNJ8IVBT7tDBaWkYShIIjD2YC5PyzbLKRXyn3wBm2H1kvHuDG21h06bYM67ZNCSJC24REHG6jswgeNjjCrEDnT8Qo1d6LM4eBa+e5jolx1Sajq9hU3qMd/kaFNKM9SUL2y6tuSlFPTdd0+VU6MTgpUUuJB92cPhZFSDrLsuqpOGGW5sfJxfMyx0vF7hjMahfiXSpNISUwMgYNiSdvQnEr+2Z08lk7uepKR7iekNu/HVci5o5DdznFgdOZJbbWmf3VQE/8kJmlnTOB+mhhgVZCS5SE3GtuOJ5MQHZ1k36pFECNV0TIS6h4tk0O1uaBWV5Z4p/XZvuBnIXI2j8hIlESySSeOcDH0ERZqjuupiAJxA2fAdLlg0kE4IJQxZLPx+/1s8cFZO9/euDwo53BQ+mgvUpdbN3Q34q/SaHMKX6YsabsSgaJBwkIiQg/O8EYhAgLfK+ndK8GP6kQcoYa7udT2uHkU0LeagYkWEw73UbyKKrPaHc84FCcqn3RACRLwz6lO4PaVQNHyeczHp0j4bBwjd0LZgqjdAReJdjY6y24lt2c9Q0ibVjUIPnwl0DdagjAq3TwHjc87aYBPi7F4dFrvSISH6Dw49UrNqnFj1c7twNCNhvwVol3GIkHqkX1GexW0eNQ+rMzBoi1RZVUTnjYhQmOkk8JmY5s74jpOCzb1+LS25TWdGlMacsc2n8tHzQIwp9DW+aNMKRCgI6MfkQCQuJg0OAES49RzI97tOrZ33VfLqUtSBAoEo2/VBpBSVkkhAFgFRNPF6yFGNfkeCv6zDDmGp8yVrBbc3eUfK4V5joIrg9cr02ycR1CdFetwXVHk8OYMf5QvcMYRGjDagcPC6yuLZsVrGCQ3InO7H1jdrb3OMEAkiBclS/T5b5UOpQq8Dbxp6LRNCYCEpWlNBOP5zcd9CNriqOxRQ/bx/NBZN8Vheq3oXuyGP0z2iPFP32Raeb8Wk6WImPdtkfFNXDmdsO8EujAUkIVeY0usU5bAUjHqo1QVqK86il13eh4nEKn3mjzQEFpkq0SEmP91jEM8SAB7ungp7HxSAKmFPudngWzs7lrNi5s2X7HpoAa5FbXl+39HipDSya5UJoQh0UCTCKH/D3QRMEC8YCdnJlD3hpwFVd1n/Ld0tykCx7mTrz5AOz4cuNgewSK8hyNLY612yVv7XsxKmgV3zIVQg5HyaeETDPn13KyFBnrtj0qLsehj7ldOqoEOrCUUEVeoynWaScA6QxqIwwoUTxNVUSnyeGQcTfjO/QgMoiN2HcF2x0u+5AYCiNtAclnd8e9RHTRJh40z7ZwwWxe2eUExC8aRZZyfAZVsUpaO77iCM4z+P0hkGxZWDjikkHy9rtMKIYSNbfahlWjP9Ws5z6vNTrj6BErtJRxlHxyyDRxvq0ma5HBbvujYnMcfNAomHn10whyYCvBhg7QIdIpFIF0rNpIZRFHn5WSdodJHCpLlLyCbifayGhmwtx9zcEgHqPOAXMLnoDDigiX3+zj1tJhNlHxgwv4krvEIX3bHZfYS8ErFsfTuwxjYOvKjtnd2nXjQZ31O2j5GHbKpAaxUEGyENPMgN1acDEqcYwNseX2KPnEiJp3OK0lq1P8FITyZEV1xZlXP5UgQ1bqu4YOkU6hEMNQnaBMqPZXT7tIZmXFPeHF8tbetGiuc3Q3yl4qE+ge5kTDBHdRiv7Y1QY2/kN9qV+lo7V7ABt1HeGKBwA/zX3xzIa5cmmbf7Au5zdnlW3wH5dsWSgjqZ2NgijmvBMa7ACVGKaG13J7lHw0VNMOr5XOcuSgy19BKE9WVCnSaJhp9VMJcmBl/DbQJdZpy4PkrcpYxQHzSD7L+K4L2ZHDZno01zk6G1abP+x9nzowfmeagyaoLi4Zc+n8ptna2LGrrmhajUdJ4AJLOICFIskZmhEqh3Y9hwpdh3sQBxRwggBij3IePZrJPbXHO9Dlwb44DEQOZvE3SvfQGzYehxXSzHZrR+112I/XFHPoj97EI5+y9YPsT4ozVqr7DKQl8cwV8JBvSMDJixAQf3mzaQfKqM8XEp9sR8kHoJ7AyRQ65HCRejh8nyHiL+V9WuAahDxqXuvEp2HPzxWlFcssK5m+JAT7U3ZoHFdqQ1NCaVXLuZ7U5FQJFNk8XMX2S7xFINnBBpJlYGkyBpvDFuNxn3yuTtLJ46r4zTj5jL50Og9kjtFZ8Rnb4zM2jI+/DhutBINLpPlE5IyPjrNfAZKhNNWSRqm4+k0riBLsYIVit8AuMML2MPfhr1aM+3AOyMT1uEw+0tVXdw52zsKPfmQUyjYa1ok9OgFVxEdauooYH5nVqBx8o0xa4eu3f64eDmPV4xHtriQJjXaRU0jHBChZ05PaBGvRFpiRhFNpjmKUkrXo2xDx5zAuDTp4Z1xL8LhLPoV+OGSEXucRw8w2YODadIbHYbmfdT4rHLejA7ZX0D+DMWPjeu7zAKWJNEDGlE1CNMX16HchtJOPPprMWUXp9faZG8T4K1sVDYHABhfmd4ktQeDMIeg6K/C4ST5Jf8SdczUw1WekVzejuA1//NPFc0W/727MO8QK5usK1uwRdxWb8GmD3sTThYYpTkLuslwbs8RFuqwOP/GlMdq6TA+3/Fuvp/GI7Eo72YbbPPiYL612JNYJyDrLYRD4jE8+SX90ddA8ETqb1XVdv9+6SM51ErgOMLRmWrv6JqgWPrP6NIsRTYwvrc11zI3ZXSTLCyHMelm9WeISXWuYa/CXd5SFJB1vqgcZWZCyJvjpuzHGO9EyWeiCz+jv+eR3gPu8anAO6WOSa6XU1u8TzrH45Y41FOyF5D7/QIM9oFvQgsWHP90Q6Wo/dJt0gvSBLyUOXzL9DMKI9ug5Y2zimc/JgCDjm0g2vHJc+OmGC2c3zeblHX6haQjRg6GgRrFj3nXyzEhXHCIPSaQe/+qlnmYPygMos7JvWyOgRlt13p6hjwtmA7Izbz8Gzmdk8ilGeNVDHxxO7jalVzcRcQvCNRt8eSI3DitKIUvVao4IDfaAbkGLoZ/HKUKaJ4vSZDclYCo+3pPPLIkHmF/yafvmWDNi2H+bGztmZ2svc3IyKIg6Szgxv/JhMSs7aCh5gTBjNzloQ4KkvyBEW07Wo8dZAcpu1oUybYuD1GdU8qlGdk3Ctk7571T/Tq+tHklkFHpCwADhyVKjoBiS2/41emJhdAs60eHPKEBjrqsegIqcfPByUkeueLgOUYi2qy/oLJ4mQohizvATe7UDwHTG+SN/MuQkmhhUezWcJP5E8xyPDWyengYWe7ESmppSGKmpjJBDTlaD43NCLdlZUIovRiWGz4jk04zomoRsnfLfqf6dXls9ksgodIUAIWyLOM5o1GDgVBRDlqpVdIAGO8QIYRadMD5F4zCSDxIPn72PD6uIOZqaBhVAaYjgBOZgb9/s7Ow5ynjgTdLzwZQes5fB8LMNvGKDCXdIaIhlxw6g6zW5+QLWxdvhYNQh5hqba/N1k3zE6pgd04zkcELtgHXMf6fG4PTa6hmJiNQdQiTI+Yf+5IZzWB/nsMEe0C0oovR3hI4AKtzO0uDrtlkQVJf6J4QXAPqBDcRcenhpFrCTvmi1lPQhJmy8C213d4/7VgvJePIkV+B1EsliisHbwJdX7P2LGKXdpBHY1kZq+cz1I4uzExvpAeVP/LQ3kil+5QE0bidtLKvto0w+wstt8qEFHUDSZGe3iFhewHr44wQiORTLZmsOFUiM539lN9CNzaBOsiF5UL4myadkIY69hK4IZg9zAganvjQ1DtLrU42k6tUyKoLxxJyK9jvtjgcYIcxnqaOMD4Da3Fc9Goo8MUSPRH9WgyWw3bZxkcD4QBmvbzrYO6CkQ6se/xMEYNgPga+qgpW0l+pWVpdonxA10utBoBLrV3YzZLPuSIfViImfeUAiQnLFJidnAVie/iT2Ius5Z8qYsD0pJy/gAzQ9y9EqyrRCnqphbdDfrH+lXzAVkgcjk5LPKIURaHVDt9/DCrCKwakvzRBH317JCCnSKPcFYZCx4RzUJiE7OQyIFAt2BA12iBHCU5OPqFyN5DMhvABZ/VmN5sA2Owy7LoMkEg5PyPg9HqJjxbNHCShe9QBB1fmypuzYwr7AqmcKIlchobKLIZfoRkBTsOHgRFv3drHRiojKsM1t5f8QYjGFjPUcyaoGYFItOK/jhOgjFs+YdchzQv0u51mk3MHW6OQz/y8mDig1Y5THwwuvgMFh4npiLLMmnkluy+YcqOYIkoTsDosUM3YEFVYenQr+dGi0A6ti22PrWXTZrQhND88jqzuLwRoqA1D6CefY+NoIpg9MxEg+gPC3adXjocwFln0lTD5LlHiWl+zltzFIxGNCYR/3+tGnXTz+CWg/EhB+BI9/JI/I/FaFwGbGQUTiaiY+JmXUBxDTD17aDwXZjGlCSg3Vq44d8pYFMVcvXa775DPK2+GFVoB1mHU7MZb+7s0IEmmS27ypCBnjbocFk3fGFlgge1ZuRzt4VsZODIhgQvAXG5xOxXwAjon+4HscRbCQLQaIVPTElACTNH3UvrHeitm3UcO57G1vF9hm2hYZl5xsaJPEA7r3TwWMhT1a9exsp/d6EqtMUIkHdZq3V1eXmDIGiW1AEwud5ELogMQZgsc9t/PA7HICshsnY6IzP3agqgEn4yCnPsAyDqwTlFJZYmXMOrmBk7pIKXlkrVcw2L1ukk/chNFeDiesAqyzossJsXiVLt2MEJEmuLWIFFM7qXGp4lP2nd6HcmYIDKqRkRq0aEnNuWD28KcOJYIJHXEuuUtuA0sJdZhsgmykZjoNO7FJYYxU4uSIS0gRJMnIVCFTgNrFHqBtb+35y2/F+JlgqdbMArvGj+/xgwaJQh5FsZiRiZVFuvxYoYyJEGgw/UfiwSW5/f19uxpiltMmU1mXGeP+C6cJLIP/sp7YjhQcOTHtxPLpFCg6zqBko4TB9nWXfCZZP5yQstDLxiImxMMqXXoZISJNcGmhFPM2HLUpRyAGhkc8KfEkzyUIcKEIz9ZyyWm/ZfLjvFwisBNb06oBwHBMLtIf/EQykk845waVPCKRDo1IaKhUdX3bM1Kun7P6IFYNh7Di9BeFZD4BvXFpkgA+kg6+jMk2nH/3EcLNI2ISEyFIq2v9DxpURTSzEDeLNP0MAo3me3vcT/QH94LsfTAkIVuGlWw/Zmj5tx3kZiASEOcaRM65sn2b5bjPMcjZqWHwcV1ddpuEqxCOddHpaGQ8gXhTNy8w0y5RuqkZR6nKKGSYAym1FcOztEwibwk4kwxYqhMSFYFiSHGZf5M/UCcULQzI2GrCC4YaVf2CjqCoWzUaYhCtKHXYwz7Z3d4LHq8uqrkOx9QliQeXP1dX+1Y9VZGYWZgf235CgdY0y9KRTbvioZMbl4RwSdImIcerGNUsqxEZF/jruYrvSIkNL5JzXLBPACenEVDLQgqDj2mPlFwvKPfV3GBdXDVHowCVngO1CKfLdmxRYaThjPgYCyO9hfJjlQk82dGKh+eGWY31oGC26s0zDykmwmC54qPHPfUjzmP39ocn3IpqatBqGXy3pwc94VwTZAJDn2DDam6ZVnWrx1bM6voyP1TBfEpEqjs8ZFjaTUoZFMiCwYb8AXQ/1w143RpEqG/3eXz6Jp9mj8wG25/Sqx0YISoYJz7SeAvOXNOqEhgTQSjb66wFZ4AzhsY4wyJtz0rnENY1QjHuzgYNYrP3AKVxvrQkP9Nethhz7D7ArsDJQG4i1mhGmjefYrShOlrS4NtHsg848ayuLXMSWl5ZMvxwnEtSAltiLS5lUWLl2pyVLdtueLboEirj0zP5zNDgFob+HOFkZDyDD4emfirAlKZeHWX1gr8SIubYsAL5scq9cHbxgQ1nopjw3Fzp0OlciXVqKAwaVV3PzEuN9zsRIxzhshLEqyoZJuZcXHJLzikijAhlRozzxNJFFd0oEsJ/l6QxBldoNbRGiWhldZn6wF5yjBNRFg12gEBW4skbADXHSXZNLNTYdwKxj+3aJh+Johdj5UdgMD3Sych4EvGmfirAlJF+A5BuWV1xXHGMq1S2rl3lBky7Dk1mqNZBquAlqeCvt/erJ+hWzQjO4HYuGPzPHgm6EpPpHm1yyS2PmDHsy/SnCkJMinIwH6JqbJynLmmOI5XkRMOrITz4IquhJS5zN/YkoRhxmxN1EEKiUBLRidD24k2AMK9d8pFI4s4qQUc+J6SdMtLJrOIT9EeqZFG2oTiu2PSXqihUmW3bCmNkW8CQwyQZHtedHpzYtHjGauXlq1Y6XEyLvYYF++6z1kTJN8b1RiRSkVVPSb073pkb1m8Akl46UQvbGAi4oujzRn8k0eBlqlgFra6v2NUQP/pn+aLrP1toyIHdY0pawVBKUsRuHcj2r+6B3AZcm+Rj4+vHWPkGho4SpJQqRooDI8UdBq1Af5oxVisd4DlMdNOFrG1NDARsRe7RDOiMkMS0pP8ly071eaPptiIwa8ihfsNatzNa9exBmDp1QoB41D3ZtQ4jQujDhPhiWBMIuLRpKIdURK0WgiQhmFlaXrJJiFZEKIPGl+zGHMQZtGKoQXQHfX5W0W9A3AMlHF7y0VHG2xiMla8gdT8hoAnxZFWadgaBCS4TsI2qIcWk4lifqXxoT2N22+MQ6FPFX3Lz6PTgxEbF44VHaRHGyl8boC/lOyylBFIDdLDyyc2nM/XA6Fhy3mAk3DDF2rJDNUhiOlG0b1R7IO8SEe4F8WpozSYirIaYh0t22qgKK3aGqmw1SOv0BrBuRjnd5wmhiPHJR7eits0Dc7BTDmmC8aujQhi0Ev2RBiHOKlW9gTnqIHGCqXzZQpGjGapMh5grxSB6hhWT8gcMHSAlsw1MVPOYRf9a+q7Brnoa1jNs7Bv/uHuEUbH2ClflEES8hRgVk5Iep5dCkhBm62VKQmvHVnlFtITXEFGYSELMF0TF2L9vITF82W0xvH5sZA64NpfdCNyWXGsFMzbWd1qCMqeKeapUbQ3MRGxkDH3iyt8Y+062qaIExpgHdOJJL7mliO1n/SWX3Pqj6pd0iHx06Y92olDRTVkDpTTxNEFKmPRwv2dqQsdZfYxZuoAxqTETUAzUMuReyOwNslggg5JoFpeWaBWE7wyt8veH+Ek58PhEYAC6It56ACveUiH+jkMyD2f8miSfQlssXGCzoKw+0fAEtaJK1dbATMRGxhCIF3Ur/jqQ1xlDddBMV67KgxsJxPJZfSLyL7R6ZKWyGJWYMxinnkrP6N5DT0KyAYn9Dof+uz2tzsmwoYJ7b4t08q7VO9yGGK1wdXBYYcEub9RpSDZ4xRRWQyt8b4hWQ3gvHvicpKCRgRiRne8g5EAtqLQQGYxBthCT+LiqyUecFlFltlG23/ScxwxqWVRtWeZElwEC/aKxyF9RLgOS7RJ3QmNMA1a+pJXSY4qvZ0z4HzcbgVIkfaBJwJWmYrK+yi6qSAgtzhIfVj1TAJ84W9e7Y9Z+qiIybu/dzIBisNH+nmOjcqYkCR3s75vF5UW+J4RLcst8bwj30pCE6kGAO8cwPcQuJxxsTB0wl+TjnTS2KpoCZdTtTzQ8Qa0eRxtF3RFG+0StlJcdYR8oi6ecpmklMAzQUCu55KZ13KcgrgeQ0V4VCjFCNMQURdaZ7HHIMLIdJsg+3tqM+z1jL7mJKJ5yw08zAJNanVPKtVvJoWirkzxWgbTjrc69//MG8aDHAXUi3zsj7/LgB96cgEQ0fHkVCcpuQPh4dAZVJkHp2w3eVV1XChj9YlF8kWwemHj1I0BdbYY4J6g2VaoCatDGGBFLIprVtcSANcZHUTZiULXLrBPij+FPABxWFiFPap6aqnqAhQN0iQaeFcsIR6SKOQbzq0JjfRSM5cj++HHMEaoeVaaGEqQiajh+d3cOzPY2Jj5LD+EICd2+QRzvcZOXiMYiemoogXUS24ScspIbijnlkcjadUAcc3BRA6ZuvBgXPzmOEwC4xPy8vbnLK1L7MIftEPl5B/zwnX+xKfH5M9dphQNdqDJ/D1JE6GrvIHRVL7tl0RVwCKiU1ercKiaqNlWqApXE04ls2FmjlhiwsnJ5lEUjDlW7zGqhpsIggFKX/QjDCqqt3ZJgflVodh8eCDveGHUL3fZHQNu0bzRwlQBlz8KRl4hOiZF1corZWCwgPqhM8VpGYq0Sx1xADpF48KN768dWKMnYaRyrGSQjpuHxdfVvgVY/S1gJEQ/3hxaXl9gGr4SCBrBxVx4AilCTfe6frKgAOtF2bZNPK+AIugPyGGlQY6JqU60qgGFRQUdMWZECEeSA1WFfwKKd8iPMMgb5sZoKFVW5K4ExXxRzDHy0omjxe5DamIfVAU1r3e4GQZmTMPns72FzyafTFsSwQcfej2ByAJirodPVAFIIdfIWRtt1KOpNNdiBAxrRSDJ4HY+97Oac0QfquMwGniPZzSUalPlJOUpA/KTcKslRpzOPf54WWgNEP4uYiZ1X2jK4RsmHIi62KI+2+EiDGhNVm2pVATsQimgYB7vpX2GMbIy6bsjNTShZODn+qOlEArFoS1X49po4UNao2RL0yBSlHBknij0HZx3KR19QMyF2gZ9OyCMfly1SQ6nAqx5bHIWx8lAIdfIWRtt1yOpN2pcdgF302cI+rWIW+H5O6Vjje0CUgPA0h89LSD604WlPe5mUGMTnBxSOYzUkX161CQyfiXnsNsQBhmdOb/A1SD64NdWPoJ1FtCWy6DOexUQ1h9kTTxEZZtZbw4fAi2XlQ2KnSS/Y1CaSjBVwtURQ1wyHmIRLbsVeJ3KBEyCQ6VGIQY3hxBMDR/ocMT9r1hL/VUZ5goq+T1KEtFltuCQ0FoG3nOuMyVAsH69Qx0QEnbw1hypTwfWH36pwXun/Mq1ckET8ikcBFModDPu1Ahr1RFigxLJ+bNWsU6JZP7bMl+b4F3zdd7SWVvBbQ6v8e0P85VXKDJywYLEnvmb8Ka5S8kFvyAa0I9XSZfRJZTFBTbx1qRaE3O4so2F8jK71ldFo+BB4sQ55Fum0C4SiFUUaKjG35SbmL5KR2ojrsdeSGVCW7LfRwow+RgQSi2I+w6IHm5vbFCLpoNPtWTjOuvlEIBNDaR9lRJvo0TksuwmoYXxbBA3Um0bVcMjEKj4GJHImkEDwYMExSiq4VMeaJLhMJwDHeLVDCQgrHV7tuEt26yv8FoXsq3xyTiZizslHoou3NsZp9EkVMVJ1tLeCcDYRaFTYzRgiZtFX1ciAttggwaVeuyQXihYUnVzp4o5HpB5bQx1fZsS6J4sCWZBlF3XyDFDLbsqcPCJ5V637mILSCLJfLO31FkthMuOk1YkpbWKdQDG1oild4ZBCakUBRsSQlGnjpMPEgZ2gaBiMkInEPTw4YxGrO9cWxFzDl08pyfgEQkDC4S+o0opHnwlIooEP3A9apVWS/eXVJbZpk5Q1469hheF0Y0LysW7zWz/Ga46TTjBBfZK3gkIz8VTQ1IwEivKdIQRiWZ2B2GmS0S1LgpCdNfEIcKx2+1YYp5OXrtsg7pTAIowy0SUsQphV0HnukzoSE49/nU6A1HBMgUrurL2EbKg5ojKZ1YnQIxMjqwO/smXQ5aco1B+lDoM3Fw9etbO8vMQrHiSbGNiX6X60sEmIjkCS4bdr494QVk/0aX95lTaVzPoQCs955dOD4jloBpCUbQJGqmvxTpUQGSVrq8NaQaSpqQSsLylFaBqyCMSyOgPRlzpss0gil1cEFZscTC2IfA4Y4EUbJSVCkVXRiTFCtBOpxfn7AHCMql5zTnDxMnidjneuonA7LYzLJi1MdLjsJuoa8T7KiDSR1wmpY+1CnnWiAON4Y8R+svLFYMZGGYH6GckGD4Xgh+mKIDeIK5+AhhjsasetlNZwbwhPymE1tGhXdsRL9ykI8RbiGiSfFsrBdmFG9cnI+LOkjkAKImOaMMiO0QoRaGbNTPPCsolC3gIGcZftDiE+YOizcHJXRJf/AKlG2wZJjHcUID3gZwdMBmYjH8FTbty50UYoxSXf7WlhVLPEp/2w8JXQUs5uLaJSHK1WlPTagGZdW55Ea2F3Z48/eaWZC1jZ8CcTjHIMnGSw2iF7SDz41dU1SkIruCTHJxX4QUGMj7y+h3Nx9ZKPc5iHMIsCdcyoPjMiv0MoHQEVRDo0vdDga5qxiqaClUhkG4p5dkNpBMqWLKf4LreCYjWyLDMl5sQQhWwNLwWEOlMstJC1KUQKHBMPf7fHtcKiLy6cAGAibE2cVXZDt4axqlNcQSenp3vLIxGse0S/YYWCiX5vjxJL1qjt472dfbNDyYefdsvJKVfh5bh6DB4kJvd+sJLFfaM1XJZbpdUQv8oH8WaSJOqKdrjJR5zRFvklKOZUzKiuMclM5H+oRowSMiKdmizUJdsQyLIToiUUyEV4diCXVwI1GawlkBzL21oRfNJHW/YgiNBjrwWxgeM93gTexwzOJqtWFLMsRUTCwaqHz5J1gxRYPDFkEw4mxdIKVMiJ6hR4I4O1kt1CODPFAZvxliBx0OPxgBPP+vqK2d3d55UNf5dHAXXwtrd3+VFqnHjpn1hAKfa0S4nKoieGFNi3fIWBGmrvDa3wagj3mvjYQ5JCgsuYP5TkYwOy/mQD7ODTlAmIjU6ENjPJlFMKbQylJjJinZosGMoWNCsGwcqye0312q7ICVikQ45Bcm1RGvD0F2eK2YNfAXJdrrNCIRG+Sv66/WQxaPrSdGMJmqZco9Lv9mTiKqD1oEEzhpxA0WR/XDHGymvUW+gQOEAljDXnH5M7VhSY0AGsMDav7JgdSjI8xu3Eara3dom+7VYiK3yZE8kIRr1dV8BTh1gdgeDUZwLm+336gw3fKeIn5ShR8uPaSJKIIUpCo18sujvxFerATI0cFeWAiWplOIOp3RGelOjY+MK9VdGewsoy7ESeoNd+ucJgCv44Vihhv5Gtwe0XEpVDecBS8HeZJjz+Mh5THFQFxYBXgwj6cByhw0AiMuqQs7KpDffZi4x804Q71LELME1sbdv7CFZx0A7sREYPDuxDBjhrz0HtyjpyAmpoeDYXbK1lU6kzWvKA1umRDxAo5GOMYwLQ9/iCqL2khcl9gRLPHicbzuk8xu3lULzFesX9pg9WRxsbWAVRIlhb4lUQZJCskLjw1CJWKZubO/zC0eIl6gqqQ5nMSWL0LzWljZMQeNd18umMbFQDZkC+p0Z4d6Jj42X5RKliZRor6yMrXzCSUVdIlZiCP7SlXCBMPrFMuj9CwnJ0WUKzUUzUuxEZqiDL7j7krFwi3auuEek0TUDACWEC2d3f4wmPJxOmW2ZgJ6gA9pLb8vICP+6bazb2boacIiek5hNmD3+aNuOpqCUv0Hq9Oh5eoXBcEZK4aKLG5SxcRtNTtVxS29mlEwIiY2XJ39khYPUBQ7j3v7m5SwW3cqINu2+XdGAXl8iwX7a2d/hN2HNPPgIyy+MGMVHMkoiuz+TTiGhUwHNAvodGROFEx8QdyJYrKQrspu9E4PAST1Cj4ZSaHCiSfLRMa3+gBD2sfAJRV5k+ggFlMTCeIstmYkORYWXKNkZC6XSpQ8gJYuLY3sGZsrtE43ZAYich2OSDty+XHrHuQk4vmktYxP3JutHyJBBVuzBFx6MWm0LULOqzA1qdUJKgxMIrBgXsC1lZoG8hG0gQa3trj79gin3HKx+i4ddjV1aWrS79xwpoc3ObitbWGDT3aWSSw6U/iPVQ7vlk0WoXGiFbBM3KsA8F3l/icFwUPCjwaatNBLLlSh4ZdodWRuAqJR5UaFy4Y8ANkVReU0JuGclJHClC92okHrAq7C4UbUwxrHS61Z0g9g3/FgxNfDxxEB2sHjsY+9gP0GtOUrOC4yqMWz0WIoEpYY3WqcWmoMMEeIKmDsSqJdeBINnv89h7LbEEkgtksB1z73TDZTaseABed9CGFQ9fnlYGYlvzArtEEqX/VyX5oCFxxzLAkE1BkyPWoSLwmXWeEIpw+7UbiTtfSTgpIhGpNrQsEqH2QaKRUVcIuRVWExBnlaxeSMRY4wPPVpmNpJNV7UZbGxJVqc4Axu2BPjRjK8K9+8spl2JbIDL3u60SbAkvEeWkNRXdQXcIOhEJp9u0wlgdngcmeSKQ2tISvkMTJoZukA4meN5/mPAdgjJtOEGw90ZtpAP3cHEoyUcaUGxIxNCyinz1UQyiLzKv3hZleHmuOXhCwsnDiYh0h8aASFgGXxYZRt1XyA1qsSLX8SdmWHhqwk51UEPi0fPdbKsdIHKcxJElTUJlD0xzQjqj1SIF+12Q8ujQfa0lkHRaT7l1ASbiTaE6biNZQVG+grE6Vr5PKxsmEatvKOgAJ36bASsgISQfJVPotnFoGJlL8kHMestCCagib9cv+iIMpDoaVLTKxCI3hBPrlE6hlKyNipWIZeUz8MSBW5QVVJlZk1XgAOIx7wb+pDPGGjL2ulw0hMCu7gMPNExvZTTnnAYwceGS2550asUYWJrNKqSD5DPTPqg30fodYb/RjLnB+pjuyV5yW7TJe0oHQoU2uSeUg7z0FBtWVxXRQ8EhX3azTbMLuqGh1wt0TGFcKSVGotdQSeQ1mFHkBoAYbyhb0ng4RWujYSliFSU9Y5DIympipBPLZ/U9Ui4oOH7kklvpmMVB1n+cjY0ig4oQx+lLMbg1dpOXewYoG664rCNQXODEw/cGKkjZiNPer0hj7kerDeD3JWyCE+uUngnexyzOSBf9h7GsrpL1oUO+JjJpj8mu1lsDh5B80CzZBB2RHDJ0VHF0NY5GVqKikpWPwcyqxGCnLtYHsmHtidUCFFuKRWnPGCSyspqodFCMWboeVoCQIPJ4tJQTC238iinA1fUGJCaziKRUVXzOgkFfSghObw5ZR3nvoOY54wAbuImNJ6G4NtYohc+rHlEsNC0HaLTcIap9SNWuqTb8HAaGuFstGJALE+cai3g8vbMNfnyj7OpiOKjTJucxXoc3ECcAalNV6aymv5cI6aPW7XOPKY/wTUVfYwpSGTJImuxbohmqeRkTZVS6PuCMMpoHu6KNTS1gNVqB8ydu8VmUBxPGnYDoJIgZGEa4Sy1F2kREiy6oo2/ByWtoChLO0vKC/e2SVJTRP9ojQVWNTcQ29XHsk6CD5XWfr1tkhVNiQBnlwAI3okUNxyymBnwHhFGzF/Bs3y/hcd61Zdt+1R8sW7EVs7SqBicebDw+KG6nyPf7RIk+LVf+HC4GF4jLFR3Ql3qc8GTvgqNJmGn4C/IB2kBl+8VSux9QLyFYGekyKeGtFHu7e/yTCNq/NohLbvhC6tbmDj94UAOnC/vfxhpHxgxbrILkZkg+g1rLwGEln1GBt6Qjtq7ygHZlP7JZwErx/qA/0jPN1la6POGURbvg7hXTWahrBwUnTchC+ZNitT0QUgJKnZAxJqiwYqC7cNMb9x/scLXtCPSoAtbyivu1RgeUIedlA6USMkJEiqmyG4e+TfsXMrxhYqeCPXxyv4dTQOyUERITkaxOGTIFLC5Q3/HTae7RW+pvfLmUaswHunyRPTwejCfdnOluQLyvZwZJPvbQv7Txt+jpk/cDX/bjcA4VoXlXcx8cC33iFTNYCdqn/2z/yuBE/Dw28AVMtIEGCVh42ACy4NmWpIAZUrP+nM8BIDh9NuCsZI3Z5Fjyw0/IERPtwL7lsU7twb/EsZC0MRGRT+JNSD7uFRsKLQM2wOkYFaBHQyvDBgkbopUBEkeu1bBDde+hK7GP+BvGhKTVma5OKQ5FRhtQFVdYBdhrxzbeImbwF7fUmuIouBQgINUC0nAW6QMH5i4fpGoCd67wWnf+TgRBW/aLj8B3CRkhImmq71uaSHhCcf07eNXSoNk6x08DhL/h7WYM34YctBmPkNghUoQc+ugzPNJr2zG0AX0skOkvMF1wDkl5n9fVQDAMMHlTAsJ7y/iSIdp0iHGkpomC/26y5h9zoxMiP2FLrEpR07BPeIzzm6l3mcb7xYpkwaaUPYZXsD7b07yVi/1AD5rcjtWloR3gsQBtiRJtXsBBlwGcsJHxmFxFd/JxxOrBVcGooBgdGg0RHGpIODrpjIlD+hs9uneAs3TZL1SIDEXVAUVGHVoNlzrQ7Xh1Bs7KmdayO9GvYFCvGJrRB9qE8YTkjhcm8osRD+TgsskAv00vbcWHd9nlOxWK+w11TAZ4ZQw+hdYFxO8KPMHs7rmzdEtNkNgdCEWXnbHgsEf8+KY7nqoS2gBJkGIyE2Piy0mSqNi82pAJHmNke3vP7Gztevq8kTQfFPqPfYpX2eAH13AMMofi6RknCFNixRjf2rTvYMNJZAkdZgdAuGAqJqMdOImw7bA/ow1aN0qiztHo5LOTST4eGUu9O31UEB4dWiMMI1ZeDhOmxWMBC9DHK+h5cmGqRdXuCKclUb7vgclxxbWjZXOEzxxC9YqxGf1o8C6iPzjD3drCeIRxmkhdQsDxEbjr8h0KcU2RpB8xmSDxAOOOnBD2uDigBGqTUHKcJLYHQtFtZzw45HEGK69YSacAdxkomGj64+Mz9bg9VxGIBO4XaCm8SwkI9zJw/h7HxKsTYGKoOFnVqtyP9B9vnV5bx3vWbN8m3ZuFCFmLNn5bRgLa4Xtvw0mxRtV8jpkxAjF9uoB9j1f6HDu+ypfX0Fd97SC05Jz/+SSfioXSIBzl1KNTa4JxDNRCqBNgA4A93PDbDQ7iDGZtlmNggCzSxLiCp2SYwOQyOv3mkKoWjM3gowQxiUkOZ4U4ODGiV1fs4YNJATK8O7v8h0JcUyQ5QvC2YHsD2NbnARwffhXHdfqT2LcE/BVWMlSF2RjDfFZO42N1Vb1iJYCt84kTFQdzkeFALbQhlz6vNuKWADiZxApuc2Ob+DYBocnoZ1m5TkXozyZs9CveZWeTTi6iEiSWMCbEiZjxFmq8xRr1GIGXlkutL7Lw4YogIW6seI4dW6Ua+itOswX0NteZGn/ZDdeBtYYux/ERj39e1VUFNZUifJidndBpmDt6MUo8ccAV5EWHAGAXZxGYJJNE3OmnKUYCaAfMI/EcFiQO+RxaA0rUNgDkAqsMsR4iT0X/0oFJqwesgjCpBnIlJQ8IYAtbEuuhb23iyb+ZuQ9lxTABubNbH9KgJ77j7mQy/uT6WbnlMUITLs7MIapYCfjXMgODqhwohlYwBpfoeLLUmof5AV7EU9AFjog249c98dMBNvnYBHzsJCVgrTwj4AcJbfoYKYPjpnlknxOBwxQ/0kENXbxJGzLoq7BTFWbxT5g9+dRQCnoKpjS0A3z2gzjF/gg/Y7oB16DlEpzW63ZXFbRMfmW6r80Xo21ODiJVrJoiJiY8JHh9XPYhtOxrioyjAxOVTTyh/DjUdXGLZHfXXoZDewLITlUmRERIoXVXy+jhR76wGqi1BbyUr4IKWKEcn8jRJ1YZVwtFT4qB4xxveEaSxyP7uDS9Psfkw1dOkHhgbA72ElCn4rQE+wUJiPfGFD8yJirg+3Uklx0CmjbVv8P40+SKQ7D0dr3DH+Qjgh4h6gFZfoiBNsyPo2wUBQcrsImB3zGuRqPovoZJQaSemr4dEwdJ8ts9TQyWAz8Dme3iMpt812IaAutF4Jxu8OWIgowJVPVYsogERQAfZJQf920knia8auQrwDj7NUs1jNFDm/kxfDpIcBLIuvRn5g3GYZT+c78y4RDA/qxxTkK0b4M4ejaYiWnRBuv4sOUIzFDbWMh86zDTNZpZY+nGIRlHMugJXkRmDQMJohtFhwNDSrC6hEkFxDlA7I62N0kJGJTERNVMRgC7sv9+jBVKzGRtznSIjAZWWfAbIIoriZuRUjQw1u09jrocUExOnlyywdOiK/dBpOMm1wCdppdIAE1CH+BLyMyTrmgacoB8bsNHssMOFzP5a7S3Okf19lUnJjxwgNOpPsy0T+bY0JwpXom4cg5z7mcG/PH9H+ryou+i45QBCjYknuSH00ZiFl3GJANWaZRqRthOrHb1gMsqZQzKiZmIgKPCXm7rTWgxJinxMYPLb/bykCMqU3mrGarWoQYgiS6v4Fvu7bj093sGUDBeNW/Dru5L3BCxTPF4UOix65ERRmxY9Wxe2eF9e/zkatloR0BQhRguuY2MbkZgTGLp46o9QKAd8sXkM2vzMmavr+Qzh/3XawITdg5zCKEIeETv4eGDLIrOU4ZQ8InEM3XlM0UnwWgjVmGiWgL91BImbcyd4e5t+CswVtzPEo/H6JZ52AmSjjP+DpMjOuStZqixHh3iSDw99634C9I5Gfcamxr0fighZ6Gt1fIcoSZMzrYo+eBzbW3ZESNIU+UzA2YTfxnvERoBbQ7mJ4EUsYvwSpwSxtpGXHijAu/DuM2uHpNLyPrOEA8n+ZBFPvh7e2BUBAMmqnFYOEsTTLUzBfCKR69xkPsIigGEjJIY7neMPfeaW5tHG5p4K7qiJJMedilWDXi0lk9GATe8RX3Y6wNYRDFwUokVD86Oxx0dwGiFBGiHfYJvaEcUYuCmxyO+s4EriK328FOZrizobVFv8kmkag5ImHV6g2gAq2Q8tixPFsYI3VCNnduaBp48Q5/yO9NkSmw3P2nHlGbJ1IXvL9kVcofjBpAG1o+t0Jhf4tXh1cB8kw9ZEmO4DFLFhPbNq0vkktu87I0B/OKyG0+QlhQhjKoZIwmMebx6rm0eZWyG558qijju5ODDB/oVyZ3XgR0OcyI4IpDQx19yGyVcBLcDT77pS28akZser/juCWzV20MnRdR/MXrsywMvPcDktk9tw+taGCUH2mBPEB3AWMGKgZNPtnMVpLMyYkg+eHEqJmuUexsvyWfW5nA7dvfcd39C52NtIwVgvl5bW+XPcWN+OvpnrRIoUATLmyNVAaEuwUF0hEo35m2vF/CLwzQdq7aV0lbZimgKhBgpXscIY2iR/TcBXX6GnvTijcEociURWMQBPe4gHCVcBfsl/9m50bnBh2y9aLXHn8FPQC5Uja1L2/ZVNxTDlTNXzJXHLptdV7/ayPZrCSVZMmJXeiMaQKKQnleT7T22MY3JYZ4RjQCFPSn5cLj0p3SMJ/3BCm4rQIs0RKfj0Ax3wvkPu2eYnLtC6xYcRDvF+9BhzPqUVk1Eryp1pvWnkNEVmQwrACZgHNDjjumW1X7IcZX1D579GI8OpdxFkB5frb66cu6K2bqwRQnnirn48CWzR6uOZVziWS3cc4kxqcFluHVxHRUBdBMSD5+gIGGPGivzArUCHU//M7utE6J4TRowPvlwQ3sbW5AVst4OHVfFSQWxf1p/4zdJRoU1QvhQmlsxCpZsM3kfjExDpNttbpLfSUpZsKXIlD6rBW+SJzZM28j5RdT6UDa+dXHL7GzsmpNPPGmW1uxltpVjy+bYDce4XISYnNToMuZjjqyMna/n3Q7Yoxjsvespxucc0ATMftktA94vmbaBdE2a7Jzyx9hBMyuSRieEPnSoiOUJ1tuoGB1YM3ofqVq75DBLJHxCWR0nM7ZTwVsqmGP+VFek13ujXq96xror9dX2xg4nn5NPOGHwOzZm/8AsrSyZ3a095l1tjO7G6hggwGBL5rrE6J6YL1yfHUryAdC8eJsFOXvFjf4Em+JdNSQOQ0ISY2uDTkEPK3/6OBzAcGRcSCErEhqD0NBMwFoS939z/VTcqANz9DyKjNFoWUJC4G/j0+fUDQ+3cNnZLAGPd6PfxvZdDXvbu2ZhadHsXLFJ6MRtJ8zaqVV7g74HnWI1wIRsAvRG3E/BlvQ56cQbDrpekPxhIRebb3Cy0R/ecjzarjJGP+22VXm23Bo6CB5jngWj+6NDAaHVfhuDQXZG+x6JbepH9PyorlJB6fhWu7+PMrJVkfhI7X6MNmwV4i/EoS+TL2jWkGkf9gnGB14keq2xtbVLSSF9lJbDjmIvAfM8uml9HT+fYNsXAzT8QnG9y/Jcfg1YobMxtVx+5DJrLi4vmVVKPLjvs0yxrN+4boUOEbkuwpiRn4zOxQ0dXi36JWNGhtqF70ytra3k2ArORi6QBFVDRaAdxZcWJyZzgdgHa/hpt/Wr9LQbxYXQZjrCEKPd7D+UpnVhiu72D0HMDDZDf+ZgqguFYzYPFxTHaIt2D3bZCLTayIiP0O7HyLAChVkHmvIbh2HH8rWDjAt7Xhg2lCMbEV6rm+CL/UxqMo53GHBVwt72Hm/AzuVt9g82fr1149ENs7y2aNZvONzEA39jmxPo+E6Leg9V3jBhW+nmMTw2kE7Y/YYNDqIgnE98DO6H0oBW8HMC99mwidfRyUcaJMkmbZSYPkSUXDcA8XjFz2boD+/DiHcYkEHTDRcbh+d2nt+6MLJRSpx9um3uGGU0jYInvalwplKr4f5pTiyHhgX/RT8dQxzrvDBqPHqQUtQ/u3QWvvHYhrly1j7dhserIXPyCSfNIq0kl48tm7UbjyV68wKa0WwKCUjiEEgNYQ2hlYMEB5fd+G0QtcY0g5kFNgEihHiM8OZpQrnKgH/ZBFEYE1Y+hcYcdhvF/kgfsRo++bimTmGaMK4iMGij8Z8AbMgECScGC5QMsQVb7IESH6k5HqOM54VxOeoyTXTB/YOGXekuiNVEIXO1vuVdgp1YbAxBvHMOy/YJHtt1hC7YIOyPJxDoA9/Z2aCks3JixSytLlJ9hx8yOHHbSRY5RkkH22E8JDJaLxJGRN3Nd7q8fzBGSoqjApoA8iv3AwH85VLgt3YlqsTp7olxyPTHTJfdPJThqWeL0nmypYRxKKvhhjRxJtqdB0oTm4TkucW+JAk36IB08hh4XYA5+zFWcxxGOSgL45r9pYcvm4OdfXPl3KYlOlE5GAVshf5E5CaudfLBmxo4dlu1mBCSjItce8CzN9cdoQtOmD5k9XlAseKLoyvHVviS2sEe0SBGZ0672+0vkrKoLY6a+rTeGNjYbHkW8Dsa52BnEsgvLmW6Ygb9gfE4IPH42JmMeCeKWaJr1nySzwQgHr15JIR+ZO15WA7uI44/4OYD7GQ8VaSThcTbHQ6PElcmIJHaeQUGR1li0cjc4WGUk4IwkTcvbvGlHZSXjy+bPTrj3rq4bdlEw8Zlt5VM1YB9wz9OR7qyn64GOFRyiJ+w3t3fo2I0SDTAkq0BjDc7sYTC9tIRFTpsWLggJBant3l+k0gHZpWSz+aFTXP81uPmxK0naHLcM5vntqxQBjAjpsZgqh6AfuC3dne3WUE7xX6iyR9JPXuz/xABf/Br38OnfItf97DEmCbyGJnaLzWovohNz5x8YFu2UuBaxstqFBlttFUVV30EL/a8ipDfxldRdQJKVkN0bQ3XfnEGhK0fMGX1DxlDoB0oC2N/YbWzc4XOpKmp+O7I+ql1c+ymYzz54VvzaBOvamXHdvtNgS7l9+8d8iCRFkuo8Mc/CxHHjjj0JuhsY7zykf3PJ0KW1IDTZyWaOORJQ/o4dvMxfmv2FeyHLSx7jD1BIObJ205YuQjO2ig415OBJ93QD9mEMRJQhx28Xy2wNUuAnYA/+OV9GjSjt4dozqC/sTTGe5d6L7K2xOvB+OQzqNpNQ2fhkoxHUyAPrVZXVRKRMKLEjgMp2HeHCXKEE4vd0ROaC97+500DtjBo+KSlwy7biI0cFrr95Fo2UHe298zlhy5Tmc70aAK54cmn+Fc5geU1+9ju5Uc3OLFz2/Lm+kG66Mr8zzPMB7kQ+YyWzmb34gkt539EG2HLjhE0htQ69YogG/pYx9NtbJsMw/blR2lf0fGFBw1i1MLONVMwa8gA7GO1ENwn7EVGhZMAnfTw4/CSjA8Zdl/Sypj8Bl9pkZ1aCANcvcUQuzO3RVQjJ/jp8pg428ontkeOUxcKwiwKlNGvpiQrSiDj0sphQtyLlzG/haQ1Ma7ERgLwaMPEq8diDOjz+CwamiO6/aSCQhEqnpraPHOFYt83q8dXzYlbj/OBorF+YpVvcl96ZKM6gXVBHJMhDI/tHVzamA/ituWwTRN5jc9oCqTACgcnPsPsYGmjoMWdmT1K0Btn7YMf66ePmaUVyzh+C+0nNYlBdULYk/ViYMzwSoUm7eqBkkMhAJjBShvfx2LMI9AaEDZt+EkITqC+Ga7s6xYIR7YewCZO9GaCd6i9MyHCwoTkU7MXQ8v2yM8E5aTDH/YT/7QBbdE+mxmxewxSexbds+qx2vyX/hTnByvGEPtIpnpiFhEWc7KHjm4/oaCPMwLeB7ZPiQcT2+qptaB9AvzmzcoN62afVgw7G/b+z2i4ACQObHCFFcPOrn3l/BRoezXgMtbOzq7Z5ZOIyiBpGSoANvksnU6AxP6o5KNEfXz0gbgXF5f48idWocduPG5O3ILX6dgO62l7DNEZq1cDQsbPD/QdgwqVIMBCX+DnGba3aCVyGKsfmHQbnh5EcoA/m9in9FI5RtjE5TycUExri8TTF9NsKx+HwNU4/3OCctbwq8ND9+JsEAuSUQMyA21XA/sQE1j7cpvV5r85QxqKJ1r45LcmgEiOvIgVuDro8hMG1AqPL6udXOf7OkDchehf/PAanrZapAyBLzYe0MEzChRAKQ7ss21KCvhtnd4DUmzl7OVgE4+dVKpvB+k1WAFWVjAEN925J5LTIWLCOnHbcd5PLAcebNPHmHChNlanFzxpU9/yparOfcjoDAYJaHtzx+6/KWcprs+ymwOS+Q7eiUd+pr9MtA/wgauo/iSjCNljsuVRMjO6p2J3gcugcthQEahiCTU2LochQYwZl0DLLczhdS/FHwVj2NTBdmrGAMW3xYEA+zirw0TJdfxxsoeOIYwKBiEpNVUcVk6umKXlJU4wGmgzVntXLm3zAwf8lA8R8R6xFnwM9Kcch5XiycWdcWLyyu1Lb49rfYAd2IPtbbwHLWcYGGu4ANi3l4nswxldr8HK+M3G6eSmhjpFpwfo3106OdnapOOio7keUUC1+Ngs9ckmTdrYj6UxwgA93iqAHR4jFL/8/DfGufTzvPsN+xbH1OYVOokj4/AdYh6erc25rHwQSLGzZ4ZurN7CYgwtWRBxQOyUICgBYeN9W2lLj03oYx5E0mGbiT1JOHYQ8Z+aQcDxB9FUgQcO+cM16O7JZVakYUQII26KZ4B24YkqfIN+hw4KNAvHBH6xdIsmFn5iC009wG1w+ucu+eTgY6gGk2GSA6ws5Z6MTMBVMxXgoMY+wr7ChEUWLWPOkPhkk8nlMq0QMVbwa60CkWEElQHS7gAkVxC/ZkCc6GOsijcxaVN02dgbaLbLMa1pm4A2kegI/CZv0PU2AnbiJ5tXKPGQXZ6taRveXjAWfYrwi7GBBIQTWhvH/Pfw6BeLbvCBkqosLvjvO8+AcigBJyM2qhEAKbip30Mm7GXqbB44jtZj249rEsYqSh6p1uM99tdlmGRCsYYSseEX7cDLMe1LVO2rOOaKLnNJi2fCPk0klx++ZE4+6RQfgFhVAmjb9tkr9no+7bdjNx9nuiCJoRhUO1pOdNSnK7QSw8slHZXHSQsYC5gAcUljb8+upIaDO4MOmyWUVBEDYr3C98aor46v8E9sAzxGoJhRFhLGE8aWTIA97b6aQP+ijXwVgE4UduVKAIi9cN0QIzZRajt8Y2ysri2ZZToGoYa+9fL4TMKxBIkfsvZnsvc4ESzqdwcX/JYwuBpK2kQSigOPdWKuup8Lt3H1jXVB3GcWljhz8hHt5Ul3ZNuuA4lIvK2dgVIKpkZXlPZgQpAt34EKpAM9DDo8wIBP9C/UrDlnVCNDCkD8UKSh4GYDkbLtoIOA4l+mA8G2o9WQPLJahXCGPkUp1JzmnUCKPEypjVcubPJltbVbTgRn7RqF0CyKzKpWABkj6FtMMjhR6elbjAs8ymofPrH3pYp6/eEwesXhDzeU7RNadiJZoeSDN6MvLdPJShwP+t5+MHCSKTTGyDirECc9NtMwh2MQVxx2d/kk0J589UP2bQ5B15BcLUx5nHsJ77RDv8oxyFQCFSAxmLQrYY6fkg3uT+GRe+agDZHvErRYCHUfOEJZB+PDtWPJJlK0A/FAR+wFoRFR91M6vIc4Riefy0g+kQrsLzWTT9lNltOIalTQQKTAk6Sixfakieg8O2iogP/2g/ksQgWUcS2ddcD0cEY0MqQYbCdASck5I3YqYSmyezmRYnXKbQgHTwzdhECmpODBvWrRlO0DLGKi5tXClR2zc3HHLNLBjBdVrkWv5a+6LDInBOpUbN/SRv2JG832EKC+Rf9SJ4Jvu9+ekGCT/XG1kw7Dhkorrl2+HCUxIC4AEwsmSh4rGPNoAxjuD+QxCfm97D7GQ9quDIDUYy+jijL6lRaT9ooDbYh9zGVnNtfw7805uYY4w+9v7lNciYAd6luqC5CoIIbLoXgaEfsDtwKCMaJ8x63ScWheGF+sNSDlhJqAJFOcaOHhB/StbkMedgyVMD75uDMmAUp8qYq2kJqi6KjIKKNLpSKEZve6jXsIHZr0mu/kgtWGM88O5IpOGOU9lzIgG+7qxsCBvCv6UuQwrAmc3TyTEdqtCDLspL1Iq57dc1tmYZkGNP07oIN0Hd8locmyiqL5lt8CWC0cO7ZbYnvoh5RWOxgT8QI6xUK4fQf/eOAAq4I4Fj4mlPGBb/fBCq2OlpeXSWbk04QJsM/gKGqJVGt9BERqUOC4nZ7EPcpMYjMF2yW5pmjGMcfn+jfud7HJZPqT8AniO4cCOULGqEPIyVvzVCr4eUTH5IwgTsuO/AkfG26IU2kuyQdLdVwBQbnL2CiPFqNUCsKWHB5gU5CqVx02wWKBrK6EOzGV1UgZdfkMSDYUVzVXrJqrMIt2SyARTPMYorhyvnP2Cp91sSbR1m85UT/7yrro8FvAyEOlHw2zs3klbWeAJwZKIpt42s2SotEVw+pCFm1fXV2mlY+b6CdDPEZGpFoPqKAGpTSokqlEMlVNUQg7QbfTGHWBnNmesC1KQVlYbmit3/YASZp2fIQ++dIubZZnrU9IPulvrzevq05pCWG0WlEBU9iAsQdPWbzA6bDvRQJZXQn7NC8vSIl1+RQsFshmbNZsFXgZK+6zAhIZpGzyWaEz7n2aNHcubXHXrN6wZpZoMswi66LDbwasVWq47KJppi0KurOYHEBWlCFcxsETmPqx7vjIHcQHXTQfl+HkwYS5Q8WYBKSh5IYiFLQBi5yZVIqQJYaAiEysAXK63Y4FHQEQ8q3sQDF4wSSrWRSTDxWPH1/lJ/bkXieIh5N8RlicuelVAwNTSj2trYtUuAVWUYMZmpsOkkA3aygk+lpWNgWLBbIFe0DJZoaekkrKIdL9Ywl4ugzAcPUTZ+6gKuj3IpCOg8m4G2k+hNKdxUwKsuYMarvoLyQePPCQ7TvGoCtAn+NpJ1x2GzldEOCnoJMjV8JSH01oM0WdEqO0n2N6Tr9HxqPKnB1ivrCv5ZQ8z+2EagLcDFVllYrHjq24N0wg+RCBhFtPCWQBB7KxD01gYopYRLZJqBooM0vHjdbIi9S5mqUlFTlEwEAHyhbqsUhQ0QiJvhaSsxAR7g8vG1SiWgGRkFRDvZQSw+slYu6SGw1UOX70pBlMhGIkQEIoIlSnktgOd8+AUGEcnK6YmGomBaYT9FneLvqLj308GZk4dRqartqOlU8flBJvBST+CQXxUnta6NKJw83F0Os41i3q5SMTap5bQClmgTpeAGvb/psnQjeuoo9VfoiCCvhj/49PPjpkXyZLtlHlbS4oGqt7ylHrGkBDwrH0gYGtiIpQlpWVHSSHkt0YvpCiLjtUAhmNmKjqRZ0KVW9DQUMISDwYqhUEumIsMZgglVQ1uCy5HRRGY+pEWoNMJrBdMowuxEMD+3jQwNEGOKVI15sjZX6Sjx1oSCfpzcLHRP8SZEgxxHfishNdakO4ZUz0n9fjFtkiQWoh1aInNI9Y2UHOMUIfBeFZwAnFbTIOuOyq8kk0x2VMWvkIrCH9lNshYei5CHmGUDW3bxDHWhGIzJMHNlttoyAo5KyNhGglizpZ4oCsPMMqinoiB8QMVxeyZg1IOVn5IsER3Uct+cgjoBa6XIbyoNChm1fsAqtO1C0hmNwrtq1v/FwDl+xRz08cyRZ+KCrpycoTD3rI1CFbiEFXtDNSAyvBoO9QkZ0fGgEppC0uIDE57Cm9zRtl+3nqeOh97zZ1bKbjeyDge0t4jN9GYXVmSj6HCumroEGaODDyVAci1A/6oiaDOfSHN0tqg5XcplAgD0iY4aBNUGQMrIDtCbYQ89WQssNDMwGqxzoMO5YIKTcrDwTEUIpr9Ef2G8Z3sg9ZyBWZGQuEEIlUqq3L6BDREKu8SWEOGOwqgzXbTgG7iPsp2rEg8YayJYUgon1Zpt/JAUTP6g6lBAWWkHGWHqBgZn4oBCTIsKohcQe7zRfl3yHB9Zlym8FU7zAeb3mE54aqolxjDMn3yURidPLhhtIf2RjyOQ+wA1scEBKlFlIVFDPLZ4hQCKHylhcpIyMvpKqZSKA5aAsGhZywHFHbFRkZVnr8BPuWIHVFihTBGbhSC+QFASOU4poi4UO+7Oih+AEyNBHNiXdjhIGsv07dErRNa2oo6WIWjoeDHIkHb9+QHc2qNV3IMX8h+zQr69siIawliFgindUoMuaJhoMJ/uX4sJuUpiHt7RBF68IoCqRgXy2HMwLm8ZALHljRGL/yiRo1t7iTDhOCJYa1AioCw8Rr/wlEJVANKh1Q8lJUpDKUUC62LBRb1GVLQERMOrFd7DPZBN6OMhTXE0U+4rC5qtuKCJi2IjpcC/gWwSW3DF/gbbjPimg/OoyIr0Q0S+xD3mZECZkpNJ/6kL897x5z5X3aoc+X3GiGwHep7DiyaqFq2RDvOcfO60aoMueBXARqfAGOnZPUEH4oF9ZmhbavtyyKDAs5bOONkeh6zjSIujKD8cNvblDE8cnHAfFiXpgcpvSkbIyBoFnYqsgIocoP9rh/MRIVIaSieThZmXtl64ITth/5+AI4eWyqWATHxJPFIIX9JBug7UDeVzRQjxUZxGAli5xqgEBgaLEiqUoIfi1jhc8siNTFxqFhSNhZkYZuDtpeqpqhpkIhHF800Td4/ROjpRsAyQdTRO6+rlivgNgdUnZsNYX6kfrsioJFcpJC01uKPHUKyj4G+HOyjDDPy9hQdptAxP0W6Y6FxGHtKE+xYWLxd8XWlt2bMhanJh8bOlzZ0rBNg9UebScS5ioFZckRMwcRaYgBLEZ//OZooxDodlhwIiJZk/YyvNPzt4a9DNccgkoEUfQgYdcAqMlWRCBgU04g3zAgBxE3ScEnG8SHTUBysewoVOIRVtV8lRmibq/ALSsE8GKqf3jVUzAXkRXsO95C1DU8SKRDyqJbsAyJKjXVNs5DGpurC+KWB2Bh/JHtGsC59cnGbQIdnWwBQKg2cnZo81fwG0E0DhEjX06nySqJqYaLm8PPFNunYKz50W1wbkc5ByIFX1UB5JrkKZ0OA7FOnRJYffjTBon1SAYy1OZ4H1RtVJgpi5JGT0AaTj5KNwMa9rAPca9hZWmZbXCiqSGypw/COnygCRohDugUbIsVJLoDsdDi6Ae8ig2/M8P7MNMv7lVbAcTG2tqKvWeUCSJrLhU7FFTdENO+P6wMz20OLAUtOvqAmA3JE5/sPo2hKyotRGZDyyP6gyBh2e7QxqwTX2WWrzF38mU3AB1iF+UtwBVPIeqfhNcBEaZNFYczXxVAnHhEdiiUISJerEOnBG/L22gbEvmaJMu4DaCzB7u5esxPUGXGLLevKvJZkLzTdIQITXsQoDZhZNMAq84PVlTBErrPqSJ9qXZqVwW1rba9gkRbsQ7qPLxElC+7uX7Eh9+UfRT9RvL8pm4S0vtRywTIElsYpyTSVQ3e73mJRH9M4mGQJux3jq3AVzeCvaM2haiNUuvy1SU0Dkl3KAKXXPgoc/fZ6vjk4w3QBptiaIBMO/qflbcFt/XAyQZqxf1BfrChrLahkELLeZGE0AetNrRVU/NIRDPwMlxzzScFbEDMzyIjoPXCzf0TQgMsRn/sZnWzYEFbzMP55RK1kWa+hriCaA2wP3NRseBYolmRTFFQGG+rYWgWOH3pg+CwIRLvLxSJ6EQHEEGuuLGM27IoMkq6muIjyiKvX0C0r7Vuop8QanAWKjpOItjakD2itwpa47mFWLXlLrNphIuxoRLLeXDswqWTG1fqB+lC3S+3kn8K4isgNkCyfFBgs1XbLtkisAx2iit7JIQQCashH0PEEzXfwQHVQ3NLoposLNt8l3BUe4VfRCRU1pO9l3Jy8HaGP27LoMmSfw5UwBIdK5/swWaVHIIKQ1O4HNugKkjMs5RxSM1NsFXRGGFIrPhNVxj2KTc5dESG2YVjSpi4Lq/2SgJWzbBBki1EnppDvyRAknofU2BF3VGG68LCrZkbulc6W2+9oL1A7av5aaKiPLTDnuzJ1oNAzjUpTEok4QccgXm2Mv2ymzLCH7ltBDg+bK7O9mWLIOZ5i3eKMHrgjXCtCi2aiHtGwmF4NtcIQSXkK7LqAqKinfigupYpwgmKvGwpypwYgR0u+FoeBbaQLSsSoCr2KRJPOIgdvPhgAZDaQBnAdNd/qORkuuD0BVG1ExWtToMilohGBPQfHm/FY9YHSOSOXjyuSEA2viGM5OOURMVvoDseIFVFUpBprQytX5eMQdK9CoFcpgMYEsEgHNba4BMn988O4pKvGtI4gCmWRgU/BUETW85EcMqLRUkXy/QFWatPbJioJiYQm8SnkJP1Z7Q5poJm8yaFCgJ5EHJgZiqlKQOVoAhZPkGaPyScIen0gGWdfFlHuAUJxUolqQQHipIgwxZSUSti2qes1EAI+LYgJE+uwPbL/vDI8VhA3350+wxR1mJOh8GyBUKWsUDJx9rOHFIMsRn6t9/v4QeKqJboOlmvy7UIrEicyo3/qn4TpMU71VVb6JKjY43/OtPYmF7GkGaGfwFaBrKYpJQCZkqmSgOigrqKdRaOI9EZNFGSbfw9n1qDGhDVrLpERNByOXk7IRM1xyQIOWAnhBCaXRCxYAH6E3WE1LDxD5y5tngQQ8toSNOtCg4AuxNjuRp8SFVAIBQSClPpj/j1NA/FLEEpSVGR8igI+Kd7PN8W7L+sSh4srDXQt33aotpqdh1sxRZzqLCFVRGxqDD3+VdHBwFtj6m+ECJ9xNpC+iJR0YNYlkUZQSFZct5HGU5z7A7pkJVxxZBCHF7QRvyPBWYBnMo2DYGmbkNuS5AlRhhitE8Typai6EbJT7rsxoZ7YiXUQxxQl7McTjp4P1BGUEgBOUucCJmFImMBhfoE/bK/vecrohZqMddvgB38aCNXqhAR3ugP61Qh0kNpoBBcxddj5BqgofRlayIj6Ek446aO4f3t/yXibbBCqGXt4OmvfbY/7IEBLGMFZ0CHgQxbtBqanbDfKi/azDhhaeoSm3ysAOvSH+4TAdh683BCWpbANmxxApR2y0gQC0Gp6W2AjC4HVSw2sQOhjxrSiHogWnoLMCXoLIoeQjiRMC9EOpgP6YMvwpLc5Hs+8BGH4/wHWwttOcstTQZF/SxxAqKjjkto/BJ13fJi8DPOC1TfvbJrNh+5bHbdL27K3uCi2gDYspuzP7jJQuQZQaUGK1QUr9ogZjDjZAAR+9EHFhaNdGLEpwzg3hVKAjY4WJWSrQ2Ad05C/M/J5AS74a1wrYicCLWZn/zVA2QWkI09WvTgfk80I1g4/2EoVKIOsPd7FqlvHG8QqABCTrBLvhfK2Ai72I+8oWxJHnbFYjkBDxXp/0yXlSA+9NZGv6RGTmtCyBWIh8iTcqLHqfQzoKSVNgRTsDod4FqnibMbm5SxFmjeHfJW20AokZP3IXJ2JAn+7yTxwcWhIZZDf4kkl5X5w5V7gAzMIANcUroSgwaqkmz2KMng6FxcWzILq7QReffSrtk5t2nDJN7SiRWzcsM67RyxZQ3yztP23ae1PJA1cjSA770RwsPIlgMdiCmChICSSLMpL0N9ouSdqOe3Rg3bd/JeVOmU1GF3eWnBrKwsJj76RmpFqKbPAcuHq1QVLPpiysOrat8texl+sO9dEcljd3fPbG3t8phlsogFNoYK/OOeGH5vf2Vlmb+NLvQBYY31ieT7IbBtkct9DD92LUKxjKECAkkYiVSz7h0RonqcBygGPoBVlX6gkVXXzpRAIQYhtyPpkwH8/BOhRx9avluogp/q4IeDqIrTOHxigw8p818oRkud0cnnzOVNs0TOkHzs0CxhlFmGaEhI+GuDJ7iBmtilakQZCafdY4dCwIG8c27L7F7e8fXV0+vM3j6zaXcM0fZ39s3yyRWzfMOa+s0Z5aHgrBTD3uYuJbwdsrlqFvF22MBUSStC1b2jRIygmio1MXZyhjwSDxJQrtvy6HBSFckw3QFVA2t1uI6RqDiCP6hzyPjJJR0BHhbY3N4127v7XE71hYDPwTGOvbW1ZbOE315xOyAOy04xkcHE/oCkXSTrT/wcVATus4y2hMVgM4pXu64Zq+4QhZyNRFULRczeBjVQizZwobJtZwsDSLfYYxvHiSVgPHLJ8QfbA8HrUmX0ZTfrT86WcwCnzI0h0vKFQJ0LOU5vzhcsqAhRRRkBa8v667RDbV5YorPJi9tml5KAWbY9if2I1Q6veKg3D5B46IDnxHMjJR5uj2xhUUOTY5EDSmR7F7fMEk0Kuxe2PRM7G/+aUAZVkSA1xXRQVIugUgfr0h9u+lhwt6oz9aINYRYFLIoig/5QUhsFb8ekrQu0zFzQMqScSTHY74qvAT5yh38q1UMUZHOzAQFtRd/bZAXewNUa8pcxEKsQEelP2NVbzZBwyhIh7HSorSst6yxrTEgRuYwu4VAoEO/SHw8xazcZK25zV3lkO3w4L+5DIsGf0cknH7SYZLNViBQO7FzC8ciYE1KG1Qm3K0g557IINJgWGzuUAHYu4d12dnAjGdAykCzajZPy3oFZOq4SjzjCR+RTSPEWAyuexfVVvryHSyHcZ1nJCM6g2LUaYU2gqZ6TEPLQYtzchryG1uXJEt2IenFZLdINFMUGRlGEgShsEkSbEBuPV9Fghi1ORkuf+OJmEFVKqqiBPuRHrGmzkzAAYXRqaC3GEo1nvkznRKrSZTMDnHv+IHnbqxplD2WOhdiyR+Pwb0DBgiKJhJYK4ysgYzZEbNVCbBcv9c2A1GPqZPSE34RtkZ6PpCTxyBaA1EZfdnvs8qZZWVqKdlBqIrbaNVkCiV4B3eaU4LQQzMLKotmjFcfOeTxEYFu+evM6P2Cw/egVrqNDcJliEdfLb1m3lywyv5evbbfCgS4mkJ2zV8zyqVUeOTtnt8zKrcc8PwsynNpuUHQlFQ1QZDf0BDkxTPMYiku0ulxZXuZyiBmMMyyjbqXYowSlmS/aocGEmp02wqYHlS5g5bKzu2c2N3cpJgkKWz0u9Dnu9eCV9yOnhSryXn1nBWi5HWylVq3qYCCWaLUoseiO9QDaSMAmRpEXwd9CKAMSYo6l6U97l4hAOZBck0aB9GHCxoKKNYjxQucsA6TM94RcGeKuiNLo5PPopU2zSpMuo6CZJ1fcRKyugIq+pxgriy3QmeDeBh4koMTjsHJ6je/lbJ+5wkkJycmfqlNPL1GiWFqnCdQlH227MxwviFej7J7dNMuU7GBvjxLgyi355JPuybo35moRV8aHtl23QqgJkKHSCLM+BiZWFivLS2aZTm6GYdn0blEQq5/0xD2Yg9KnYjOapkAKr5LoTjBGQMLZ2t41m1u74a+QwpyrasuWROmfhvDaGn5rZVj5zATnL16NpKdkDg2foZUcQmog72KpIWHrmTrn0LMtE+ebgFbz8PoDMyemEYSMQi4GRpHhoDwVnLZiEUjb7PigiihSfWg3KvikP0J3PHlACphw2Q0Ziwrs3EKqEbkNpTBJnzDo2X8eU4xFwOWH/c09m3jQZ1RfuWmNkwvu6yxS0llA3tnbIx4VcLmCRiDk9+nAR11f6OgKh4Qw8co/uzynT9qDSIQHe/t2VWVF7cY6THIQTgVKRPQVyZelnqAgwCSMOdkiProRqxxskBYTVoxHl6JYahWRmK0O/1LYCOxWghi1+rp/shDRokAILe5VAt2A0wXRwMaLbhonQQuVOSna3rZ9gcSDIYzLbnOB8mdR3iNMzDJCiFgqOlBlzyatGNOs2IBzKF7spmsWWb/AIOKRk9MWsQUyGRsWmiERxJuDKs4bQcJNXNJfblTIGH/ZjVY+K5gEnVasHNTHWHYGWyoSflGuwCgM+yIQDib7XUok/Fg1DkxazeBeDp48W1yixLO6yMlpn1Yki+tLLHuwu0fbgVk+tcJJCuUcstQckWi7566Y5Zuw2jkw+5d2aHLZJ/trlp03n0cky1X6Y8nC9MMlQihVRUZI28vacMTV1SVevve3K9yzQdzCgD1XDNDto4xmnKN8WOEJKh6YBHC/58rm9hAbPoOOUSAeWPv0D6tO/H5PMCVE9ich8e0IM9tODVSa2QTr0h8vqydLj8FSMOECwhJ6xmlp1ZdQIxvsK7KXMW+Rd+Hh41YGcjRGZAtyLMJ/aFIEn8ZL0Be+bIV97PRpWSjgxHpk8uFHrXGGT1pZxRHWQs+jwhiQURtII206cdHiS26XtvkJNzzphjr6bW9rj+/3LN+4br/ns0xyl3fsColOO7EaWj5t7wn5tTghG40i7u/sUaLB49p0PkqdwyzsOfq/fBPZw0qK7O0+smGWbzlu4+mE7msuDn8cQluoxQm7LO0QijsbA6Qc6DLRPnyyRKfeq3iMvILIBSGkiO1UTkExY7lsuxS8fKAoFaUdGy5i7GmRQkYRXxDd2dnn5GNXNQ65hpE+eyce+v/Y6gonf4wx4U9BrBZMTEB2Uq9ADLJaOSiwNVd76W5KYEQs5LXTdtFWUUHiiVWKCOJwvqiejaTXqJPjj9i2IHageCJnRVARQiH5uP3sefTpL7tNSz5beMAr/I7PCAtlb6PCCMRDzWl2slqun/B4M19GQx2CSL54nBr3fmh1s7exY1c91CkQweoIKyGsiIrIsJDkFldogK4uU40EqLN4X1ERyU90di8gQS3yyqoGFld+hroQXQMTWL5SDZBoFQRL+rlBgGG4TGfeq7TpIRlISoUDyFgHqdQkQnnsDaioM6yNkiGn3eFnhpRTtY/kgy+WbtJqfVGfnOjQch1B/GO06sEDH3JZtxct6XBy05VQN+Q4QIAZqRfPKiBw6z7niaBdgHMS+xrEatFaiG4oSVQQ0HeOIfzudikFKQrC/eM+BYonclYEFSHY5AO6EifQXKYILENtsLQJyefsxnDjPQk0Ay/SlO03lkp26GqQeI8GegYPEyDR7DyGp9pA4B7kBLRCqxE8BIDVygG/zo0m0Bso8RxfYZ0sKo736Wx1b3OPV00eOXmi7Z7ZoNiWzQJNFPLNYrw2H3Hxx4k0MbX3tBXIifF4iVGwV3RTCQDDcJXaw5NfLBdUqRIHo/mK58mRuR4k7WUblZTBjGwvMQa9CcEIOlSxat68smO2d/c4EQV+lb6OFK3Cr5Yi+YDeE2GPjIAnG6dgR2oeU3qvrBNC9IryTYEU3C4HVnc2yiZSTtweQfoz4KGu1Er6NcRR6HYkBhUPcgObToY9BuogbkvaNutz8rHy2kIfoCdbAYFIQ7YHmIt4Q9mSCFIbKDWwJP0ROwkwaauNZXDmuLdPKw0kXKL43kePLpjd89s28YBGpKWTq2ahlHggw7opcPDztMbf43FCFXn4WqTEBwEkHoD/Qpd8H9CZb//Zq3jnCHgDbAuHDRA+t9cJelq0DaCadHwN1Ld2oDo5fMimKxIM4PkhPLnAr0G3l8E2hv5JkPEhpGHTtYnoUEX/8WVZXoJL4qG/FD+Od2mbbh9kMFTweDY2q5MCdL31gn2Rwli9QcNq9ehrjXgrQgvojumAt69s5E0Mve513BaizMmhTypF7EXXW1uM4EQxJ6AQTwGjk09p/0hwbD+o9CAUZFX6I9sA5ritDi9Jf9hGRYXvndAKZue8fVPB3ra9p4ODeYcSz/4mLrlRxQdDn/iyJ51diqOlkyucfPhtjhqOH0OmMzsxOVBxgdQxd+SUxBQ2vLuLy5oowE0ntaNiNpDx7g8Rpcp8r+8LQdGXNY1L3PmuSlBFD9CwsV/0MdrOgsKhLQ5MWA5K0pJ9YRpYnWLnjf/VIe5kG5BSRqPThBVboGGM+2foS/rj+l+6DbBywybAilOjJNcL9jlJeYSSEuvR0P0wxk0VysZgHyW7YZ+IKy86sAkJdzbMyUwMHJoDqJILOZAJMegPSuNXPioK8e/NBZUWEm1bC0kEIQTELALJPhVOPLh0tn3uCj/VhifZkIT2r+yY3YuUeK7YRMQrCWr70uk1TjJ8XJMePiXx4DFo7xOfUnawpMJ0RiSbUGi44nElBzGT0eBT1tz+tpNmqie+LdcC+rIJEt2gYpHIeDhqxJCq4/oNsCfqtB+IYFtOBQ6K/kSBcb/bot8CJIRx4DM5bAWIT72FKHNGoWFCexExdBe/DBSbI+IDfarlGKoiq56sXAO8m9QmyHehlogRjssqVJCq2IeMwmgbAq809EA22Wj4LiBuZZyFsEraZqKpmTMAJyF46ARbfELCYD/5/RhTMR7joCzNYnTykTZ6kwmhhlBY13g/WLJDQkggEl4yqHSAOhcPBeDLongkmldAuOxG//A2g/2NXX6SjU8l6f/SDav8dNviMfvINe7tMO3EypB4Iv8DKWIImOx49ImVzz65LEhbCLMyeNGGASQXyYIbSfjNIyJkZTwcJ/zIbimo3TQqcbnTBhZGh9B5c/VJKDt3cE4yENWqOqMtoZG1mxAGZOU1iLFH49kN17xcxECy5YUyLjUX2q8he0Y2MReZDYWqSDTLGCEag8NQumLKk5pxCiTBpA3sC42kOgYz2PGWRZVZh24yvt914viqWV9f5u+I7e7ucxm0ZTy1W/Djk4nnSSEUzg2t8SsfAYxlDAoGlghaSlgjBJU6tG6gkhDK8KLU2fvUyTtIPHTA8p6gZLRIHY4yT4TYIEw9vIwkg3syO/ssjzISD777g0t2w+UiCxRtdSgFYLLliQQ/zLJOPjZ2qJaBCHog6BR2QJAgD3JsTGZpvtfnSHoL4Iiajy2F4kZCiqPJWXC70dcOXo/+5AbtKIixIpwTJSMqslXBnWqLLcR2RZXVheig5SJWFtjneFwaE0cxHjLi7Smni5R9+JJnB7y+21ooy/VacFJ9ogHQIt6cK/dhTXmm3fBRg9Wzq5ppcJ6nGwjhzE2BNFuAxLO+TifQVMYrmeSY29iw3xVbW6WTbSSgCMFKlQ2iPlhGTUmEILHxyUdOqzLQZFl82r/DxkgIZRRFi4wQWsyL4iyP2rF71iYe3hm0A1Zvst/b4WTkADk8No2nx4SOVuG1N/y2AdCcOD7sNvxLwBOdSFn5ADQJ4PJbwFCCXCR9e5aqh5DA8bQ8NpAGcgLPpz/2Xw3gOglXFIpsvbCyuOwz1EYbyaFqwzFdh7ia37rRUNJsEeGx5jaGY2o5bONhxzQSEC6hBYB9bK46wEaC71flcs/0WEoQi22rXrIhngnbQ/T50zZ12CoQl3obj9ktJCAzkkjnCfvFYlvGE5InaK47zise95074uFnNuwYwZ+OIFhWyeh+d5+jk0/sUsKw9KE20BQiolS1nKZpuodi5GSEluMxaO8hMe6ex9sIqIyOoA5fuXHd4JdJ8XAB39/B3qBPnJHvb+2ZPZwF8M/oOKvOgftwm/zLgOzZiX2QDyBE8rG3uUNx7NlVlr+aZ/XZDi2JcU/KWhOIAYJ74IBriqwhZGvS12gbPgZovmW6UBSlA05YdFif2ofEI/d8+o0VULTBPchtHRO7yCRyGUUtK2wML70JWIb+aNlJcAZgG79aiuTj7TWMwz805UvjkJ0a0xSdHAI7HQZFPhEVQtzxBRTtjMZ8rAj4JgCZ483REkgbO9uqgRUN5sAdfMGdNtzruUJzIN4LiPIenWRv79rVEF44WwK7DWLgPxaqqDH+S6aXaNJ2ZQEmcyBrKCL2O1OSrtivm4PVxgpn7xIeJrBvLUDH4KWd/DYCEuFEw49WE8t9x4cvqxETDxsYnA0gOTGlBZKIhHTV75NAhpId7fz9bfxSKlXhi/1hsxq8MsLLH2nllT1jJdriDZRMA7tJlaAoMZMNW6JnpQb6UNHD8MONzbWV3JusgU6nGTE1DTfRI8ldrQRzOpndQZBIHLc/rDycvjaD1c6V7R3+gil+7DEHyGsO+pu/37O+4igOkXpijQyx74ybkCQRZgRL0I1SKJC96REeyhAjcIYtMhreR9UoRjcZw+GXBDFXHFtfNtvb9jthq6vL/Ou3fBJDGy634TwWtye2KTFhnCAx5cA5gGOTAKUB1GuO5EOnAsqjk89jnHzspaoinEV2Tebt9WWrYcNpIwjLKfXqCvQhL9Zwhr1zdtMcbNtr44vHl/k+D16jg18IXTxJByISDlY81On7+JE4EkQSWjy1ahZOEF9dlsuD+PZ/AtC4DVTQbZEetTr2Lx4+wCmHtMOC5LA36T/fG6JBgkt/1i59IvRjK/bekVbziIhStQYGrg3HQqtIWfHhB2fcruY+tAEN4eM/4j2gQb8SvdlAPi3CWhiaMOsaZbAk/eFPZ7gUOaNgWuvYvaGhuAX9GrxKpCtVtk5/LtPqHGevmEg0ICcUzcEkgwkHkwomm0LOGuAc+jCUPIq2qoLkIo1KJ9cyr1UFQkpYkU1tO2OGaTSPtqGVk7htSUR4wj106JYRomqMBjvBcZorNjd3+BIbLq1t00nvFr5qQoZwH2iFTspxHxFvSV9fI1m86YWAlmtf9oujAYU2zGr06cj8IWVyMOH1OpiMBc6SwJkaDEodIUSyFVgzoa0+baUTNGvQxmW0vcvufW1uyYkJkC+10cSNV+YsnaKDcffA7J6htlLSAQ8vEV3CpbkiSN86dv/JcClolgvhSRI3feRuAgeqIurEeFci+dDqztb5gyAFUcCfhm2BEEU8qmMCwwJyHa8EIofeRnbGIX7kBHW8z40nTOZZgVhOIw5lQEWpAPhpTroObB3yXMtDRxA1fUJ0Fjm9OAbIYBLBp+cpIZ4bCNoWxgv6HhNPcxqI2WwbfwZGEJPb/8x1zgN+jMS9TO2KUTXQxjj/hEQhjGnGcDqhvMzDIYWuzeAn6zHP4Ok2PHhg3zBC0x6/IWORP3F/Gy9SxoMpNvm49pMhGTZYIck+x1+Q8cmX0x2Rufh0zPGv17m8ZY0QYsX0jM/HQ46G9JPzmNEMaGgo6zsifyiBQForOkCXyWQItnZppcNvqxYQDUlm5fS6WTpGZwCPXjH7dAYAOjoWb5Xmx1GRpBxQ4pgY6cQaY5AdQgx0Yn1RwKdXSMXqsAqBDio6GMD58K6UQqDrwH1IKy4kntMn13jlEkIHTaWcEQcMbAxDDqkiZ8FShFiwqXiVIPEB7THRDW02Ag5mLJKx+gzExDcRS2Es8f1BcK3mUCKUlAAWCqTL8kokgJKPLBG0sZLhMcDsOBbo1QxK7fGIBZoKFYxre1OaBOKewPEn4OMQCYPmuSt0MiOraMx9eBIOj/J7L2BRkSWGPx6WZGmaLeVJyQewSqFq0RAH6DwTYrmcrRhe24klia6uzhAR3MvBfRW8zcCvLnh1s8RfFsXKCDf8eSlJ7KXTx/iSHC7DDfCBjAKLa52aPvlO5JsgYScfqMU2ht1hkdOpYG9/j5+SuvnUulnGfTTqP3y3UcPvI/6IHTqgf3EPCxDnBVELHWHJvpapGjsUsHcdwiEDQxg3hndpH/hppNEFOOpBXsK9zkjA12pt4ISFTU1lJfnAvBXiv/xnYNqSGNHGwvimYawN53+S63nEe3iIo+OfTnfzIC6xYfVjx5R9dB/fhdyn/tjEnMj9QkwxQlWmcV2I+MSJ0CAn0ywg5Rkvu4VDJEHAHLwP5A7XXqSsxZYzpmIS6njg4IBWNHgnGwM9QZ3MnziOqDv4MhvzbOJBUhre1+asZvwBMRl2mBYzCvpAhVWB01LKXCwZ4wYSO8cHzfEHDII8QJF4brCJB0kHL7Lc2nF95ButoQnWOHzjSauV7l/OLAgVdZ0f/tsG5JJmJ7DWtE3W6XKirRcUHLnLnAMmDlyT393DCVO+BbFnHPZLS0vhgx4ipKppHJaS91JGageIreSlpiK03hvxfGOYDZmYK83obWEOfOnbnUTu0VwnbzhAAsHYunBpi49570MK1F3F+z1qLOphKWV12tIPuJItQYEZkjMCMVhE5Ow5tGwtxHK2Tn/xoBodoPLzB9wLSDw4+0MHupUNHpJA/yHx8Ms+8Q43seo+BFKNyB6+CQIqg5YhJ7QarJzTcAa13cSvgvBZRiOrExKx7F7mxHOMByduVG/u7JkrlNBFUmz7Ov8b6gLIqDFZQaxJEJIiDySci1FSpDI2JcIY5IYtJyewMvIvlPMLAE3UCPhBhcE1+iMbhmMtlkidgeSBHxhkFv1pbRCETu77QOzcAVWLofUDrQznwm/SHrvBp2wW2vbULUbfuBLULF1rqNj8YIvgSMItbdgPavcGwNNs/NQb9RweqZYrVRhXWBnZBw9gJUSpn4v9rxijVz6PRSsfj4KVUcYBVrBaXboZIb7PpOwwcL+GMvreWSQeOgRwtk11vKkAP9TGj1jzG6rBWzRLeFR5jZKSrHgiP83YYvmmQh2huqvlbGZoTdc5AR7oClTlhwtoIN506pi71LZvtqh/MHBxlhOqZIwKyU166BP8Ki5spv2T0Rcoli0Wh3qCilWGWLJyqXTgybFFKokiVieBxGLqog3lCJME9svWduGtGA5xbBjna6srfFO5PAXk6Ykt95lH0isOUxoeomQ5RU5ydv+HimrjHFM3ob8zukQxJnByiRUQjm0MkUt4UGvHJiMxYj8QSGiVa3JyQx/C9ec7oJGTUXshST6R9ihjAlbqO6tiZASZJA1DLZLhDqSJki+3ocngryza387BTTWQaBLFtrC2jOtBlPYp8Tg7kbkyYvluxTwGdWWoZDOiN10nAoogfelIuJ+AS2S4x4PBhzN0JB1cakP3WRQ8KrItegWzSonedT+hoE9IR+lgYwzKHjRCqdhT6YgJ5JSML2qBvkAGZJqLODCucbkNj8jqyxwt4JIHHnFPMTawGK0YZrPf30KNWTo+D5k2x/T5KNTMylNcGhX5qRHiqga+hCoJCF2HBLRD8ygnIALNqvw5eMEn1rf0iUFGbN9F9OmLRAR7POBPNlV01T6QMHYgb/yvA5ETrlJreF/IkhSDQsmIDr9GhzoSqxzuDWo5r26ozK/JQaKhZMQ/wobZEDRnSpvziBmQxWaLqjAOojaoDyVdnAmJndQwtwUblXXiwYoHlyxt4sEynaXdFkHZiCVA592G/ZdwB3mJw4I13DZveI+Bl9jTEEsGLlbebJW3AFliASoIbS/sk7AMRNUEvPKnjg8nTrZsi6OhAs1CbE+1X7feh9D/1Eikr7EqwHZVETahCL03ZukzPOmG+7t4+wHmaQyXkzQ/4vFsvgRn/+fDqjgW1viVzyW78hmlpDDSXeAo0Iwb13EEgsQPHMiNcUpGfJ9HbJV8CQo2A2QV68irKGrBb6sLEgT8VDinzpfaVOJBjkbS4cRTOvaUoXxItA8oWJw9rbqHDQK5oFIZxZ3IxyCw3KoXMUBCxT6uO+lG7qSWUbCPCcE+bIC3XVjlYlucDfT9yvIyX9s/4JufIWJXRXvNfTOnTiG0PLWQi2SsTfQbLlNiJYD7ZehufD9GvpR59TA449Kh+3aX4JZpBYT5kzrz4iV3CQ6/+O8CkL8kQR8qRinSJ6+IiD9p5YOdOGZIsTxFK1sAMZYzSDSIJyKIfWhXxCTEdYIm4X1u/Aul2JB4mOg2B1W0UHwpKpJFQqgja4OhqJmiImVpWQQCQ0VK2NATuo4uLiee3TTxaOUmrBB8BCq+Ao5s/RD1eEuRcrOyEbHITxj9YHVqpl/Bx6jYhzjtItroT232S2zQgc/7zxKFnYhFCOV4islAS1076CjmEYnt4gWzRokHxwSAy1I4gcIWT22HBUze8C7bYQPDCu3epXkTl9gxh4N26uQqJWH7wFGIqJ4EiY6kv7SNTj49gHu+lMbZIw6OYAVSOHHeHMkDjdANgYAWiupSHUhU4lhos0esJTs4jpInKELCAxwx18QYoi9biIjjihG1DiXEOoGyLdh/thYnmxg7OLAyiWeTEo/++YOSgZJdhmP6yz5eON7JdYiaV6+iXxIiMgb15pEQkmoWosYbNZOTTam5IlgD+o8CRazFXsvY4AmFjvzgJaQVFEMJGEWpEJ1iYyAm9TYrUhv2/odcisJqE70nyWA+XmNY6/pfCfNsew445nH5fXvbJSCi4RLc6soynaS6k/gMdMRxeS7JZ2g4DgQq8casAYNQAibTnwwrRWzD+RNyzBZ+SLQQ2YQVEXN8MYktNySEJ1seiksfOlRHrUMEnTAXfZ33hvvXTjYauISzQstsfriABh3G1uYuJZ5dlXgiQ9q2IqdwTHw3gHOPn4FzvTggtu/MNDBC2okVhoqFyAxFv5XgZXQz603NGEw9wAQOXhxvfMzFEMcRIGofsW4F0YC3XXAUo1OsBFHPbVOgW9+yiTpOlJbdPR5catvBa2eYwaQ5Y9g/OqbadjWAQ39nb0hAGNT4sTl8Vwyv4Bk6Y+gUHxuTQv5Mycc2HEHIZukBrFACIedUstDCXLYHXd4GUSSmCCKfcgiKEctxmf7wZknchUM3Oxm3lRFKcYn+2FofdNNY322W6GshrwckiANrbXnJ3IIVDz+GaxMPzvT8uRfRxK5sPcDewj8aulyHNWlHDG27IFLAoKX3TREjHdREB89ukwHSE4hWZCQEb0rMgYNjHp+eF6p4C8NGf2kW8avOsRBDUugxw/LXH3xTWqBBihUPkjYuM9nL0XhK047fUc0Tp0Wlkftl4m6cCk5AuARHCUiOY05Aq2oFpGIKwwsbPfqBg0fcAwdsqKSZoY9yUgKFWrejuPliHi1ZIgo93tdN24xQytf6lBl1HUekjxEmBzglrHjWVpbMTVjxUEPxQAvu72xR8lHfbR4Fa9o5kA/ajzh4+U3WluQ/p8Na6N4/GQZIrVY2j5acAdEpGQ9spg6sWnqPBflj0z1sgK8FCmohou/xJmucxY869DOi7LHUJqBgnskZvZqpsYAPPMG6cICHAWjeoHlxb3fbHOztmK0rm2Zv67JZXF4xa8dvoM9lLtMfkqWR7m5oSjzcZ3RcYNvmBwx2OfGsux9YwwMHePoL5Sx0PzQbObIXMuIjLRSh22NP5RyB2oMHVeR7QLySJtrG5o7ZpvnCvy6L4G3QpxRBBH1C8rkSdqYgQxtluIK+EAcZLrVUFL8oWmC0TA8YJAOdTgNtnZDYuychxgNByeMloWsr9lIbzm5wP2CDDiq8Nqf0+zBtuJOFKC7sT1w/X14q/YZPL6xuHF3VomOWZHItDWRzik0lBy3XNBojdIJdgss+W1s7vPoZs4v41Sm0k0ceVgnYZc4v6VRNF2INyWKh3TBI8jja36XCvrl0/jGzuHnerO5cNg8/+KB58NEL5uyli2abUvT29oHBM0eYPO0qHG9kXzcnlo15wq23mFtvPm1ufPIzzPHTt5mF5TV+ug3Agwa4FL1JxwQePcZKqDv5jEKnkYpYzUJ/r5KMEvLXEZwBO/7cSeQKJW90E/HwMlKcrPKcYf8PnwCXLWF88rlIyUdDaY8y1AGeujqN+jVRS77HZEagqeNhJRP5hoEsu5/IqO1JzQrfRJBPPHhdDh4wkCd7+pFPOBoYcnjUF2dO45PPIC+RdVkgoaYcCcgBV5TVjFzX1JwkAdeEY4TOcNmMb3zjFfe5ODKAN5ylYiKtojMsdkt/RjenEG+6tgNCYUjwmOFxs2/2t6+YrQuPmIuP3Gfuu+fj5uFHz5lzl3fM+Z1ls3rr08zpJ95ulvGW+p1z5uDSY+bM/Q+YhQuPmmOru+aW255gDhZXzM0nl83NdzzPPHBh3Zy/tGnO3nOfuXFhw3zB859rnvSCzzdrJ26kBLXCT7XhJwVwaRonT9KPoI09ASij2DlzRY+58FiQiktD7g/mClyeRwKyP6tAKyA6IZIELbLBpytMSz5OY5RiB6w9+ttpOBBrNKPZyga/HZJPfyEKilV7CbPu3XOVWKzBOz1jBjcQ48SDs7vtHVznThVAsQNogJVyshkfOaws29/w6R993YIerNGjpmXixgFj+E2MElYIHdvHX929uM5ZT76xjuvzxcN+bHhwPUXHwyrHLRhMWs4BrWr2rlww25fOmIfv/qh54P77zKNnz5uzV/bN9vot5vTTnmVueNItZv1g01x+6G5z9/vebt7/1t827/vgXebcxR1zgFXM8ZNmaWXBLC6t0CqGxh+Vl9dWzfbGtlnc2SXdLfPiZ9xkXvLyl5nPf83rzd0bp8zFT3zcfN5znmqe/cLPMzfc+iRe5fBlTppwkXwwhvFONDyAMPk+WgBnYx6mKugxX0w+sTIJ4GcZ5JgG216Ccyev/B9/YMlecgNGJ5+HL0QrnxkwOKZSZxSJmCekBpjSslvht1XHKVftERN8t18IeemajdyeZHuKrkXiFc8eGcDBtY1X5oDgpMs+I05ZMAHOlNoYYdDBa7RUS/xhB4Qymq7RFWKXUANhAJjocHDv4NH3yqTnPVMBJxa4p2e/XBrFlAkxJpW9dCBRzjhk0OS2t232Ns7TauZ+88CnPmEeefQMJZrL5twOTXC3PsXc/NSnmrVjq2bn4sPmoY+/39z5jt82737775mHzl4y2wv2/g1+XGKREi26BpfY9vd27bsdcbmN2o6xjgmRX6G5SJMmLWJuPHHMLKysmNOnTlCyWzJPXN0yf/xPfqdZeOaXmQfu+oj5ii98sXnSs15E6y2seHAfiOKhvkQy2tpyv/1lGxFAt7Teh4pbF5wZPebDYWUr6Lt4uNk+li/gUv87gStXbAJCYhp00OvUI1S/6slncOZKHd6zIgnREjy5ZTfDb6kAkZcUEatlU/d+/rJD2waDhGwytHuZ/zrFnL4kHnm4APcPrtBZtE08TiiLjLWcgwLQXiQ2nCWVMcJghOZobvH9QVJBV3jT25BHGBgOZlxywyUgefw961ER0Tfra/ZV+f6w13z3mUNPtyQIlCLrqO7ThL17xWxeeMxceOh+8+iZR8z5s2fN/Y9eMBsL62b9CU8zJ286TZP8vrl45kHz8MfeZz78nnebD3/o4+bR8xf4gZidfRw1tJpZXjZ7tPrgt9ZTosETaWgjEq4G5R6KCzw+F6d+pL/0HxMmn6Qv4LU5xhxfXTE33nza3HDyuHnGqWXzJ7/3L5n7Lh8ztx3bN6/88q8mv3hoY4F/wRfHnayIWqufbm5dcCb0mE6bYQnoU83joqujq3Fs23tARKT/G5SAdvAmBEwyDKx87D6Za/IRQ+IGiHZ98FFCkV1gWHLEbPjQ/JYoMGaVU7Pnea5g+yrUqOkHiIzxX36+t4408biHC6r3eDJRVUilMYDhZl/VEV/6yRjrhVJtWqkJtLuu4WCGNjQxBIcSuu4KHjZw9QRRKJDHrl1bp7N8mj3kqdhelLpG3Hh+RpDfm7i/bQ62LpvN84+Zc48+TEnmjHns0cfMQ+evmN1jN5jV07ea9ZMnaE2xa7bOPWjupURz1wc+ZO6++x7z4GNnzMbGjtkiM0t00oKVCsbO9vYOf+LFqrikaBONPbOWb96DLwnIjzcrRptNQPZMHZOi3XCmvrxs++jU2oq5/fYnmFXqvP/t219vbn7Rq83e+UfN13z1683u0holH5vM8bLN0iXQTJcUoCT7lZqYYipshq3Y7tPJx/b14MDeA5JLkjgpgs4WLsHRiQH6WZI9S9MOcXukD3HyiZUljoGuJCqeqkFkmCGpKRCAWRV+CCuYFXfElqmATxXbR6FWy0YAFtYaZJGqPTZ26OxsfWUpeqrNrnjSxJOx2HDSigHDDaueZTq9tEOvrgFuHJVg3Mh1iHVKxkvI+pwSSA/C4NgL/cEBzD9nQfsNtECqEMrwaOzyfMMl54F/Xs1s0oLmkjn38H1m48I5c+axR8zDj50157doxXXyJnPshtNmicbg8tKeufLYveZTH3m/+diHPmTuv/9B89jZC+bcpS2zhZUJLofRmERywct/cY8LqwucCGJlw5MgtQWJhk8OaUDgchoN6vAcDO2luvuI4IzQJ+xh4kT/4hNXAG56xrPN5YOT5oZzHzanb3uC+bpXvsi87Bv/tDlz793m9a99nVk+eZovZSIGfvCATKn5dSSUVs0AwgUKMtN8h+C+9bAVSRWax0VfFzm8eHXBrCAB8T88EUhzDJIz9y+LjU8+D3VddlMmG9ar7IgZVguajhxz0d6AVlC3sMyiCDFq6lkeEd2u4b9AzUYOdk/hIoPV5d1KhR47kMGBe3xt2Zw+scYDALs+TTwFa+PIRaAN+H4PLm30jDyIJJGNdaohumJ0LOYSRIQgJhvYYJ1KXBkC5ledxA8bZMNxRBLBvkbCxyWRkYd8HmTj4GCXzmbwtNlZfrz54mMPm4cefNCcuXjFbCysGbN+yiytHzerx4+b5YMrZvPcA+aBu+40H7/zQ+a+T1LSeehRc2mb7Kysm2WKa39vx+xSm/Yp4B1e1SDhULJZwErGtsFu+1ynArUQdWoitRETv+qmLuTE0aeLC/ZLpDffuGL+8g/+HfOffuUD5sG3/JR56tOfar7xy19mXvr6P2wuPPyg+ZrXfZNZW1tlWTwdukubfwvIKGgdNMgVPYigd1vFxRTvMcJhJRU79yQOfN0WMC9hP+GXUfHdKPQn7GG84vKkrIDmmHyUmYrFLmdVUwULRO6yrREpYCDX0OqphO0IvDR1aJhIwPKBY7XnC8ZSMi5NIPGsmJtO0qQACtnsSjwRKZGoqJaA5IOzylp/ZlkjfBwOKIARMcSias9ZOAF3SCsBz1AgpojRgYtHWfO/4eOUIjL2NxLPsOLsxwKlg4NdSgxbG2b70nlz5eIZc/7h+82DDz1sHru8ZbbNutlbXTNLJ240p06fMssLV8yFB+8x93/0/ebuD33YfPTOu82nHjlndpbWzPL6ulmhOHbJHn7mBEl0G2VKKHv4mg5WaMvLtIrYYt98Oc1mHkRBn/ZoQrN1K3jCCyghWnzB0G1ugqTts25eNd/z1/+K+cl3nDMf/sV/b577nKebb/uaV5onvPTrzFOOG/P5X/Qqs7a+qlY/wwliC5BL9yFBk9hQJJNREVRY3ZCQbBuk4labGr4uBfQ0PkiT/iMR80900/EOTpCAZks+kWrBUrcDJ5jKlw13247hfXVYKPgpanpGh+0MBnWtH+91QmQ+9UYU+s+JZ10SD65l7/PDBbhObZ9qEygL+aKFqCSMPCAGFXziWQYMxhyK5jr9zAwJNIByXomjJ0TfbSwcOyJirl+92CAvBzEmbkxcfgxrkzCn6pDAO7hwOal0yDP9YM+YvW2zs3HJ7G6cMxtnHjX33/tx8/DZC+byzqK5YlaMOX6jOX7zLeb4iZNmZecx8+gnPmQ+8aHfMx/+/febj378PnNukxLF6gmzQmOOJyzyvrtLkzKNQ3wDHj/NzMESD5fPEBzagMl7j+QAfmCA/uGLyHiA', '2019-07-31 04:52:26', 144);
INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'info@bmsdecor.com', 'rajivpastula45@gmail.com', '<p>test<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAHhCAYAAACxyrMlAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxOTowNzoyNSAxNjowNTo0NaRq/x8AAP94SURBVHhe7P0HvC3JVR8K18nnppk7SdIoM5JQQAKBhJAQIAQIhEQ2yfnZn/GzscEJB7DNs/0ZjLP9Q+/xHD8HDH4YMCY9TBAyEiiggFAeaaTRaHK4+Z578vnWf1Wt6lW5uvc+915pzv/ePrtq5aqurtXV3bv3wgHBjMCPft2/Nbc961bznf/ymxzl+sHIpswXE1xPirbUxojcst3uqn6DA6tfJ0Y9HmLGpmP5Dl9d4yP24z49mGCpRWueUZTo6H9CIHOQt5YjgoZt0ZilFfrTgXI8jlHkN9Cj3vJdQ8H+Av3zIObA77Ap6BBlL4sLZmtjx+zt7JsFKjNIl0sqjMQgqo60cnzFLCxRdc/WGYFuCuyzQSSyLYCA8pNgIXKS81nSzaJfuG9kKixQsPd/4EHz3p//QN/BfIT5odTdEb25V2YX8ChK9ptoTMQpc4Rpj7kkHoalFq15RlGiwilhZOJxWFyimSQ3mShAvBxPmdMFUq/br6FfqyaJ3T7wR0TSK0rJZnd7L594AkQG92kj0j4FuLi6SIlnIUw8NcBUYC6yreH8dCEOOvFTgghGwg390clH8K6f+X1z8ZHLrvY4R6WDS5igMjeM9l1QCMnTWlTXclwlxMVYqW6E2A2BDLIajti2lpcAlTltA4FMVjxHFBp9YjLDlle28KysjCJWbBRBOl1qLd8lwH7Dx3C+gUJNMkKvKJ2I7+8fmJ0tyhoycWtdP5krIoqUEOzHAe+jxZUlc0B2upCIFfRA7jSZRVfSKjjR5DQLe0xOPufuO29+5z/8rtnfRZTXHo+PVVi7jbP3Qp+FefQ225iHoQo48fT46IqjkcaYmZfw1B4/SmZK4gQ48VRQj2eaT0ErKXhM9d1hP0w8I9Dn3opRF2PVg8SBK0Kiyz3vu18ZdIqKYpYo8fAlQk0sIZCxxhI1Sx4PiRfTeXNKLziJyb4P8picfIAP/fqd5h3/9T2udoReTBkbRaVJxkagy34kNJeYnBFli4ux7YqvCquKrB7NZm17eYlRcThhfBQ95siih0+cTePIbqln+RGxYKOEg95z0R7fGXQlNi/QthegIQ62iOASG068kXySxMPQ0lSioo+dxQ/M4tKiWVrqXPUoEeh2aPQDQdN+q+87eJQtQoFcA8RnSj7otHf+1O+Zj/32JxzlcYiRnX6twZNTEX2NCaUinc7+YLFOWWCEqAJp9Sgqmax4j5mCQEBuGrGwYj0GHSIa7vWUxApWHSJuXTgA256ceFjbFgtgibqIhZfpEVaoiLNvWwywQ4knZRBhISRy3JEc5nuserrgdG3SiQwJMj56wGpIPLaaQcVwhRWvekRUq8yUfIDtjR3zK//wjeb9v/JhRzlCDaV91UZbsyXRdfBeBXS3JBZs1RXaPhyUYEmneZ7J7FQmoDRMMEjGivUIOzhRfOAEemE5v+pJLLYJXYAWa8604qmDVXr0JthmFPRAzrGw6tnb2jP72/t21ePAKV8lHtaPjWDVQztqaXmJVz7V2wVO137k5dh7xUQMZ9JvWjfMF14ijwpLDImFRNQRZ04+ABLQb77ht817fvZ9VN521KuHT5f7PZOjLCnOtdkZY03SDAEUVR1DfYz1YuXp7wjFkmjf2EplRri2IAWrU9GMWa4uZMyD/IRbhMRim5AlMRTdF0uyMRI5EMrKnlsWGdAj4+Ancu8gRIFsQZ2M5BE8ZMBA9oWWXXWyfsbQwb59Km5pFZfbHDHdZV7VxhoZ0aiwgMFOXdSG0CFZYWvtrEjEmEvyAXY2d8ybfux3zM/8jV8yG2c3HPUzHNkePgy0HbUk5hFq00aHk8lxxIoFQ5bc6cWJlaSbKx4gEkE10eo20yEYQTSQI5F48LSv5Et8JBbbhCyJ4ej48CIl2RiJXF1xlP3eGAhB4okAUssUVpW7lHgO9txDBgxKKPhAYuI6IWMMvkFapsRjv4eUehvUhlIRBbZoNrQVOqQjEanqjSFdohEIDJhb8gFwlvjABx8y//6P/aR5x0++x5x/4ILjHCHT91cXYwM4BHkvUpQNGTOF0FJ2/JIY01s2ImTFO2zYZNEQjNjQ8SQq8KqHMo9OPG30STFydnvVE7myIjie22O/R4ZgJ34nHOmg2mOGL7dt71Pyke/0QMsuXwIbkTHxvU+rHlxu44cMZEe5yTrQV6Ua4nk+tNGCSFMS5XoBkVEUZY2nyBYjAxr9hoM3fP2/M9tXdlytDOyc9VNr5o6XP8O84o99obnhiScdZ74YGf78MMLtTBGycsaCIrXsl0wMqNsHUommQAIvUpR1jPAjlS/oWzL9LfADVMSY3jOuvIif1lI0zAxuKoIRKw4N9aXlBZrU1CWfHAJGQSpHJlpAlnlXQ9f1JJTYSwgeAacsNqBDxoooQVeUNyD0uBFgpbN1edfs7+7R/BZpLrgbbZrMVUfAB7lcW18hUXuvhznBhK2V62A1ZbofsXTwLogsoFHyEehKpTOgQ0s+MW5/wRPN01/8FHPrHTebk7ec4NdJrKytZK9Rj8FnfvIpaCtyy369izLMLlLdK/bLAh2QyytLZu3EqllaX3YcQlHVMegjEInlM/oDKVbOoCJi6X02BMXUU7HhWVzoEYx1HKiMeW9p2V7EOCgdTomLhJAlgZYjJ9BCxUkob6lTLESHjBVRgoHOuHkHiQerHsx9aeKBLdrUgcYlXd8/MMuryzTnLXOZObqfoifkaoAapGE+2woxFTBL9vPJh+3bYhVeVwo9Sg5XLfkIsCJaph2wtLTIZbvjjvCZiQOzSGd52N+nn3ajedrnPdk865WfZU7dllsFu2EYfuQHc0QbqrFyHqURb8n0t6HPcDJeJ0bFhmdxoSIIKHZOHG3BqocfrwavNJMESAh5UoZWhJbNTkR5Ywm1x2dDZmBHgkF13LyzQFl98/IW9cm+n7O8BWR/ti2nIfRX+cIUi+NglVY9nuz7yFFGhONtUCFQUz4tQxNKGJIPpHs0BP0hw2oozZSrnXyOcISnvPB28+wvu8M87fOfYk7ffgNR1BCkYjAg49GZGa2W5BgZvkZptA9kKjVspK4yCgUbnsyFgpBAsbPiqKtVDyM3IwR6sREHRfbFgmgWWhYxNIxkqT3+KjIDKxLK6nROnaSLxLG9uU3bDj8eLbAW8FceNMBf2iJ/mGJxlQff68Gqx7vWg7EWDsSIH5llglfTzBGrqMYdnypY08WWxxBHzs9R8jnCNQNWRM979bPNK//UF5kVuSxHozEYkPHojOpD1ZVieQVmFfiWTH8r+gzHD8UipYKNgBwfdqjq49OxQx33KaD6Iq16gne4xcd41YCDIvtiQbSIrHxKLJrt8VeRGViRUFGnOGMOIF1cndnf2+dVD68RlJotLioXtCpCBZuTw/SKBwxWsOqRfY7kMChZ5MJxMvxR4DOZBZTBnKyCkiQ0hAvw+S1RH6wPpUjIMdQp0xGOcHWxu7XLX07+b3/5583Hf+dus7ezFx4YQYUQ1YeqK8XyMQr8llqMqnyPj1hmog6u9lRfHlo14KDIJTPTkFor2u9xXJABeWBFQj12S/C6+E7Prq0nc6gkHvxNE48gfJMBCVilKuStA01RFlBSSTIY7MjGCCojoPUCX7jkaC87hqaVUMg4Sj5HuPY4c89Z8yv/8DfN23/83Y7ShhrDXSjJD3QqtYx2iOQQ6PDZb8NKpx/7uG8PnLWCUZCL/qrMEkKFpv0WCjKpF4UeuzlAj3UPOLnjhAibfpMBduEBP9mBzT147PT0Ax/8kIF/k4GT64D/8mkNzl9gU/tWW4AssRNOT9xIurH/GsgIHCWfI1wXwKWNd//M75tf/6dvMhtnr6SDtTi6HaMy+plV4TeY/ciYGW2ZFJo6JNC/6nGFnBzRsupZYgVeHoVQuWqqx09Gpumlx24OXo8KmGHpw7/JwHV1eLXU/QAP0TghyaxMQB0nB0sruJwcKBUBqdB+AZzHWJqrgNRCqkKR0YFIl5NN130l6TRbi3GUfI5wXeEjb7rLvPlfv9XV8ugZ9oKabMBrGSX+GL9AIt8xs/T5oLPy2lcUvBFXyBklWtNXU4DgZUJh1Krqo2xbpDZTSlzthtezBXxhl38kjk6K8HQbUwPbrkIfXHK7w+8VWr7gV2T510kb+x1cltCLo9Lu5VXRYM/rluS9wAQkuo5QGXoeyMQNv0fJ5wjXHT72lk+YN/7oW/ieEEMN4nA8u1pjkOf4A6mlbNGUigQS+Z7E0xMKCeGMmi+55eQ9rWwMfpqumgKEjK/527ZIVTJGeuzmwHr4Yw3gCtv+7oHZ2abxJxNtzh3RNNmLEgOX2nDJrZZ4wPHc2qU2MYwn5ZxGoJtDU6CCRHeEsRGiR8nnCNclPvwbd5q73vpJV8uhPcJLEgm9Yaoyf1hE/JZ4DmN0Wj8UF1iLY+tx1C2DP4PwfG1bhB4EGSM9dnNgvUiZss82Vj002fO9ntg26lV/B/Yhg8IJQqIey8S7F3xOPKGuF8vJT0Wgq705xL4EgWhJKMRR8jnCdQncrH3bf/xdc+6+c46SHAYWWaIiR/ywWlBWGCuRlW/Mym0fDgf2nWLFVQ9DMSKZLj+1M3ABGxqsoXRYiSdFhtpj1wGiXpwLkTL17S5+JG53l4q1fg5hp9sD2kX7/Gg13uHG3+lRQC0x17RPAq5zs6J6nodA014BXlcKnYYS0b7EAxwlnyNct7j02GXzW//qbWZ/L36apvPAqMLZqJhiVtUVMRU/L1o10OBqWEn9JccU+WBQ7PfTABsarEXVMkbI4CMVz1NzpBwSbZ7QB4qu4V5PMQkHRjQGBq961BysbXtkiQSvR0zEaP8nosEUnxOoYBAlK163w4h22iFeFHHEo+RzhOsaD935sHnwww+5WoTC4PfkiD9UXamgD1RYDqFEXp6oM/kQwA4eMqDDFUdsv+IY0bYw860Q/qpqHZ0y3maCgoGeVRoh0Y4IUsWKcm/XPVodTu9WqBCGf78BnSQtryybxeVFXvUUVQp2BpekSzIl/STxdCJvr+QlgjitiNMI9WwRSWJVukfJ5wjXNfBDhR/89TtdDVCjN4MSt65VQEvJ8afY9jo9yjzJ4nJb56pnCqBeMqEmxeEvocdlLJPTIVrZVMjhNwbQ5I43BqwcW+HfxikBmoF2RAiqaCNVdjbdqkfPmkonhLJAOrg/tIhX6BCpqlIBUhb05wGY0ZtgaNpIR5G4to3EEyMYNlqXGfiK7hGOcJ3j7rffw5fgAujB7BCQVCUUdbWMvoBZFb5mlsWIU2BWTcegmYjPoZfyP49tERFdtdtPSxB8jkOJ9hiPZXI6RCubKnBokseLiZGESm/FTzQjQszHqocfrd6R3+pxKAZnGW4eNfvUP4v4kTiKp/iEW9EWYcHt55oMId/aAVCXLYeWfhHOYMt+gESQKvz9IEs8Sj5HuO6xdXnLfOx/3UWlYCQHKHPGoW3HSdBHNZoCMyCXDTiIABKPfodYBSQEuS7ZLthJMbDXYzyWyehgji6bKnBAhh6SIW+WLHDsEAkhBFYsuGS2s7lrJ2eZoYt6liGJB5fYcC+OfxpbAopn+UoM3L/gV2RKELUe9amJp20/tRx+BzWvfZR8jvBpgY+/4x6zhxvBQPkosCjyHaPA9+QO+2WRTk7LB0AyeGAKZ9O9q54eswEaCgm7x0EsE9VRjZPGAObaYowprIK8Bh5qw/vb+KexZSVVNKYYVGQKqeAhA/9Ytp6LIxUNTjolZgadZucCsd/2oaMikMKQeCILShTUo+RzPYB2Cm5S4swJb3rmjcqgHcHi0qOXzYWHLrpaCDW8K6hLjbHRJxuiqRML4KzeFbHqySNUKk/oBWj5RDczMY61D+RMFO2kDL/CkceWo7kOGGEuAb+/bW9P/TQ2Ias3EFFCX9tPt+rBvZ7o0eqSf9YrMQuQZlvd8ch0WwKxPcX+oFiwQgHEnKOfVLhGwEC/6WmnzZNf8ERz+/OfaE494aQ5fuO6WcbP7NKOwiWAy2c2zCMff8zc9dt3mwc+VHji63EC/CT7y//YS80LXvNcR7FIBm9mNFuSYxT5Dhm+hbNS5AOOGckkKjFB6n6GwIRLf2nDPY1lPD3VMDLuKHbQOoF+Zmqc0X5bPS/BCcGxJAnhF0HlQQO8E3CbjpUsCk5Btl1NKx2yv315l5LPXuXLu4MhLrkqL3QontVjq2ZxxT7h5vdh5km8yMoowGxTSwQyzWi3bAqcVW8kfezAszKMx13ywe/GyFNDaPrOVWwLBvrq8RVzx8ufYV767S82t37WzXap3oGP/c4n+M3P25e3HeXxBawGX/CazzZf8qde7igWyeDNjOZgKo34PfoWzkqRDxAz4ifiOX2h0VBgL66OIxM/Q47hGh6lQcXWQlIbsbyqZ1LPePuA02mrRoJySNDn6toKX5/Bo884Idvd3uUn3arJp+JwcGETDx4w2LqEn8YuHYeDMS5p27RT8GXSFTqmfeIBX8sQhmrEmCfEdKYZMWluUfg3qQ4WpQsC6AAU83GVfLA8/vZ//o3mCc+6hesXH71s/uv3/ndz5fwm1w8Tz3jJU81zX/1s88yXPs2cuOV4d9LROHvvOfPr/+LN5lO/d5+jPH6AffeMlz7VfO33f5WjZAZ5diSrqTTi9+kDllE/UhxTyWTFY6KWx8VyV4ev/KonqNhaSGojJ080S84wx9oHvL0aIgmpqkMDPz2tkw/etwZaMfkUnA5kW4ILJBwknuQJN0asQYht045ZPU6rHuwjueSmZELxWLkBiPdMEbHZSEeqiXch9PiIMbIppSDsEuAaAxPLhLl4PMjHAh3gcl8Ffg8TGNCnn3yD+UNv+BbzLf/g9ea2Z91q3vM/3md+8e//mvnpv/oL5r9938+bn/8//qd58795m/n42z5prlzY5C+4lc4HbnrqafO6v/mV5thNxxzl8YP9fZpkNq7FSY/dF/XjzXGVUFY+Jjbk03s9oRTXcooTYFN0xtgU+6QzWi1WqF62KqAgMJCHEhL77vZ+NfHgr9cYVBn7lGzwcwk+8QTCsXik3EKPeOSPkZlDc2Jd9nPIGmsAMRX0runK5zhNos/64mea5375s83bfvxd5t733u84hwMMlO/4599gbn/+k7h+/sGL5if+3M8cysrnlmfeZF75J15mPutlT+ebkW/59+8w7/nv76te5sMluVuecbN52uc/xbz4Gz7HnLz1hOOEOP/gBUpev2jOP3DBUT7zgQniKS+83XzD33st15NBmx3Flsh/I36fPtC63OYYit8S9QhUB2bPqseXAn4HInlbpb85O2NtAwVTISIJV/WrB5wk4g/lXr3ywQoHl92yK5+M05Ckata02by0a/Z3dfKxMoFeZBdV7BNorJ2gVQ/tJzwlJwjFI+UWYnEJK0bObElWY556c8DhnvpX8Llf/wLzp//rHzWv+UuvMk+nyXbKZajrEXhi7fO/+UXmj//b7zDPfuVncbve+Ia3mHf8xLub95dwZo8HCyD7n77rp8xH3/xxvtQQ48Yn3WC+4A98bt/A+QzEmGMhJ9uvPy7x4KMkmpziBaqpVvq9nkHS01O1OpR8085Y2wDpjFaDDnUOEsDqMby1YNmsrC3bsd1rDHKR7FBNmfxbPVvxqsfKRGYCCA/xIvktLVPicQkTf0PdmqUMquKOmTrpw9XW68Q1Sz43P+10cNnrMyH33Pz0m8w30pn5q/7MKxzFmLdTInnfL33I1fqxeXHL/MLf/VXzK//wjWbr0pajDvjsL7vDnLz5uKs9zpE9QMr3ehJMOsD6lCA1JvFAFmfTfMWNWfiTkQwqHVDyTVUngElWb1XFBnuAkpIifSIJ4MSNtxW7qhF2c2qAoBNCUfR0yYPk+AulW3uchCzsWEmkHUHzZP8g+cBO2i1auhNV8ZQpHjyn1EGB0EhM1euAhHXNkg/eVKyBnfjpDNzb+aa//7X8QAGSKg7W9//Kh81b/9M7+T7OVGD18+N/5qf5sWueABxOUOJ5ybd9nqtdYzRnh/kAJygLy/a1KsG2Em2eZ19tj8lMb/iRL1zSki3R5w0/gUxy2Gii8ZvQ9IYJU8uobYX4K/hUNJlkl1fJv6JjW1lbMqu4H6njdu2wcbvN+fYyrY1sD34HfYlftlAOjzQvsz6fKLrZwicjBxT7Dl8rxfq0YvD3SphI/1F3dCEXMeQNLuMj1MlbwJUIrHr4UhnrZXw5Y+4jAGJfWbFPzCLcEBEh4WdQlAHDMdWvgvaYZHQLDmAV/Jmg24KY1aav+j2fG28/ZZ7wnNvM8179bPOcL73DUY35nf/wDvPYPfa3WzBAzt133jz8sUe5HgMHAi7V4RFHjX1aRt/9rk8N34SP0HvP58mf80Rz8pYTvqO2N7bNJ995r6uluPH2G8y3/MjrzU1PudFRDNv88T/70+biw5ccZTY85YVPMn/gH30dTwaCzYub5t/8of9SvJz3zC98Gj+Ng138ibffw9fLn3DHreapL34y7wf04+XHNswDH3rY3Pu++4v9lgNWefiO0o2UdNdPrRPlwGxe2DLnHrhg7vv9+825++d7Pwpnxk990e3mG3/4dY7iIJOQIB7hgJIZXi1lhTp+7ZfhTUQvnSzrOk5GPtYRNo5EjP0l3EdwNK3Ack5Y+F5uBNJXnwRuPPi+iwPzdNKgDTfdJVG0L5sP1jHuRB73bPZ39yjBLfPXICB3QH225cb02rEV2vcoZe75IAzS30ouTQ++NOCRbV/e4WNCNW8AkfPaRCcdXA5FTKlMREE1sp+QSo6EwQFbjZJo0u1Fm3nET0vH5qaiGQYJXPXkg/shX/7dX9wcrO/9hQ+Y3/iXb3a1EPjC4Xf+y2/iCVAD34L/ie/5WXPpkegllA49yefJn/Mk8x3/4ht9fFi1vPnfvN28+2d/n+sxMCm+9q+92jz/qz7bUahf6YB84xvebN778x90lPng+V/1HPPV3/flfBYs+K9/4b+b+z+Q/wLq9/7Sn+Kn+nBT9Zd+6NfNbc+6xXzhd7yYz2RjfOq995tf/Hu/2vXwxYu/8YW8D/lsOAO8quS9P/9+WvW9a27fo+Lk87m3m2/64dc7ikM8ekuTCoFFORmg5IQ6R/8glip4SssW8SESh6f1kXjk0Miac8QsbxKspUq3pZDEsY8VBOnSJ1/JICM4bvyElgOJrR6j1RS1Ex4xVpBUkExwr4dTGe0jeXx6FQmJhlnzgYMAiCUNAmNo+8qu2dncyX+hlNTkI9cEJFv4xvHDCdgLOUVAFQWalKgEjkI76Vc2I2i2U21oMAYvriSEonKGMRiZAKucnz0OEb25jnduAbCxRxNqDD4Liy7njQESzzf90Nf6xIOB/iv/6DeLiQd4CungiT2Nh+58xLzvlz/savPDh379o+Yjv/mxoA+f+Nm3uVIKJGMAB9pXfO+XmJf/kZdkEw/wtM97svm2f/oN5uRt+SfsNC4+Qqu5SjdjEnnpt73YfPVfeZWjXEUUDiAfLvOd0PShMhmF8DgUnOHjNkRJBkKQO4ywiz5zoPGHMYjjBJc3sRrH5UJcruSVZOsYd/qDXNn7tLam9pB4kCBxTC/kDgHlKBcNYuU3aSPxQJiFoFSOMOZ6u1mVkFi2GiF2UoCIceyySXEMpuh4hMpXPflsnNkwD330UbNx7oqjWJy977x55K5H/Va7XIUkc/Ghi+bsvedpME2/n6KBx75f+9dfbdZPrjmKMe/8qffyZF8CVlJf8Re+lC9tCRDbh37jo7zaOAy8+2ffZ3bVmR4u+RXh9jMmiRM3HefY8NMEn/jde/h+FJ6sw1mg4NZn3mxegqfoGnjsnrOcgB775Blz1+/cbd7//37IfJj66bFPng1OCvAI/e0veKKrHQI6DwIv1ikfY1CrGJjZNhKPnZ5Ay5mb6KICZ3GEYR+bK0gSweSOS2d834iOC+SVIbk4xIcEZFwxBujY0CO5ZFCEKGawvbXDP32QWCzIB6DY+ZI3VFk+UkLVkVQxRMzwYQxE228odbRa2yJoDbD0pku66FF0pxhKJ1YvQ5ylGlc9+Xz0LZ/gtwp88Fc/4igWuMT2E3/+v/vtXT/9XsdJgUeSf+mHf8P89F/7BXOGJsKpcMc6v1ftm3/odeb0k4d7NvD/jp98j6vl8ZQX3c4TtgbuD937vgdcbf545K7HzEd/+xOuhqTZ/8TbB3/tTvNTf+Xnzc/9wC+bX/0nbzL/jcq/9s/+l+NavOj1LzBrJ4dkmsOFBy+a//GDv0L78ef481f/6f8yv/xDv27+n7/4c2TvTUHifeWffJkrXQ30HxK9oiMs1pExJCR8YsWDDT2X8xnP47PDGRxh14tmdOykiXsiC/wQAx6wQELyCSin4z41+JDEHzDHNrrkg2a5XfcLpcMTbg4dLnA1BZe67fvbMnvIVfFRMhfeZyNwGIMGl0rKIzBYFChKyrSIuiSB0xP1nIkUbcmrf9mNzr7DL3Y5UM9jcMgWPw0XA/cS+Pc3Wh1XAXzgvsWXftfLg8tXOIvHfZ7drfQ7NgI8bomHJmJsXtoy5+8/72rzB/rvbf/5XdxHgL1J2wbeCP0///FvmnO0WpRBjlXjh9/4MX67ggDftXjSc8uX8gDsv0c/ccZsuffMYV8uUxy4/v6RN91l7n7np5gOPO1zn8xfnp07+o6AQaxTXiNUqRho2c7wYxJWPS0z88M4T5D2Gg1VjC1sOK5w+ZVXQTRmWQ/HquPLGGToMoA6C9mqRqAngGi8qiJYUfsXLw5N7OVsxXAyePovq+BIVVNZpiXiL5cCmWmTWujGWw6K44CTh7x6OcJ+Z1c9+VxPQAL5kj/1RUESwarlTf/nb/NEWgN+Tx838GM89JFHRr0G5qan3mie8JxbXa0PeBJw7Fuua+355LuGZAGcuLl93wdA3F/0h7/AfN0PvsZ8yw+/znzLj3wdryD1gyBITGNWZ9ORDnhPiVl9x4bCaIUqYms4D8NJVNHLXN0rYx12A5ERcciKZxmX4igJ2cRDNKxCdvb5ezZ42GB/l04AlxcMFhTb/PCBpbM8bXiXGx4owIYkhmS25x4ysA8gUFJx/SeAZ+vdXg7El0lxssYrMaYTfKEO9kmJx7+1WoOqoFRNEVOFZkHLoECvamAsAstt20lwpEI6suWQUSFAuOUsxOM6+dzwxFPmc746fEU/HhveoyV6C7iPsnoivTyFxNALPKjwJ/7DHzR/5Me+1bzsD37+qFXc/R98iA/w3sej5SGKHOJkiaRcA17787of+EqOG68Qes6X3GGeSiscPHqNR+DxnScNfGt9rkjGeDroxx0GKbr1W4IRPxZHXe71HD66W8UYpMfpeZAaxihu1GMVZN9egO8PLXIS4YcV1hb56Temo06TPO4b4XFmbPJdJDzMsLJOdFpFy8MNXnZ9xawfX+VLY/aejnPugASFmqcMrDIgQwkRD1DAV06paSYRQNKxiccjkZkyFmBENlV01SKUK6/idXri8FpcG4vHdfLJ4dZn3mRe8UdfWp2sBbkn6y6fDR+kKAHmP+drhsSHV/HoBxdaePijj/KZWPzgxhTETa0NpeOnj/F3mvCG7hiYaPw1foXSI9lXBXE4HcdJKNKhMAO4Z6j/i14Oy33FLlgD25VmiANjAquOlbUVThBItnhYhzcaG+DxWx2ERhue0MQmdSSqQXaoM83Jguc88l/UcXK261Y9TOtph5NBIuMv2JIvvepBKTPMQzj+cGi5pKOPtcSGZvaAI7FFQWKzDm8hMFWOw3IC4fFw6kfJh4AXdMo9FOBzv+4F5tY7wgcJYuDGozzKrIGDoQcYvPjiJx5QwEoLK5lt9eRZCxcfvsiX0i49tuEohw8chF9LKx48ZIHkjEkFT7i96f/+HfNfvvtnzL/+zv9s/n//2381H1MPRMwdyZhPD4KU0o9Rui3hiJ8THybMw0Zfy0IpqoHQp1oFxgq+J4P2YuzjUhjGL2+7EzbouzKesNzF5154FQB+tvWVgZ52kAzEoIsxjlWaBuhNOxE/We0ALRtVZILIkHIQMX4kXhO64YRH6SiQnri8bpIP79RrADzijSfA8KVWAa5R497FsdP45n4eGPD6xrqg9CbqHPC483/7vl8wP/PXf9H89r9/+6jvKMkP4j129xn+vBrA+/huu2O4z4XV189+/y+Zd//07/N3m/C2BFx2LH3Jd/5I+6vag43uHWltFEqW4lVngLm57zMUSs2v7QKc4GEVgruP+Nza2KMTLmzDfZ2u7cpQxtsNLG2bE5H0J5Ic+wINCZ6a00rzmIOk1fYnE2iFRSeTWPWA3jVHKRn4C9KOBJC104oO4ChsUSNrL0RWM6uXi8Nq8x2zrKEOQNWpC65h8gkb2brPcBjAJauf/r6f5+8Uven/+h3zqJrIkUS+6A+9xNUyoF68i87w4yfibnnG6b5xRMCgxqR973sf4Cf3xuCzvsj+VAN+YO5q4diN6/w0nOCT7743+32sFSXDmDJYJyBxM5PfhnLLtuKXRJPHfjVmil2jz1Ao5Wpzi8EC5ngCx4qCTvDwpdqDyoMwWbAR/+HgStSdTKdPXDLz3wEkYuuQVFfVWB77Bg8aIDpmaX4JgUy03pEAKnbAyrMLnLICQ9hZkYregEGbL1dqnVaHEjjZyOZoGtcs+cT3BnDz/2oDj2vrL6m+9T++M3jY4IWvfa658UnluPD+MqxeNG555s3m2A3lFdM8gDOy533lczj+Cw/N591xPeD7YNVTdYsnv9C+vsijY6B2ITeCS4hlG7pjTI9BzW5HV86IgveIHFZdrRb4SMBUYI6Ofb6ktYbvAqE6zln45pxBV0qwvYcfiuOVUL2ToRMkHgLiwYMMOCHmez0RP4tApqBQtDM8hZdGO84WyLKNh3iPLMTGGuOWtTsCuGbJB9+Q14MO91nw6PINNNnf9LTT5nNe+zx+c/OhIhqYeIP03e8YLqXhAYBv/2ffyD8aVcKb/+3b+cfdBJDVX1adN3AJ4VV/5ovNjZSs733/g9XvIs0beCpub3vw96xXPJO/H4V37WHDL62+/m+9JnjBKrB2YnhrxPwQju6OsV5Eqtuw1nLm+DWxq7PqqQNuQlcdgY9EyRSOfX5wAI9hdwLTRTZegi+hW0kQJ5XSw0FPKwO8qgkNMh/H2OIq3lpNlZifQyBTUKiQ86wCJ0MWUkZ6JCIrrloZqR6iydr8p41rlnwe+OBDwU1+TGJ4t9i38/b15qv+wpfy8/xXG//vP3xj8NYEvP0AL0MtAauPd/308O63lfUV89TPe7KrzR94q8ILv/Z5XL77Hffw59UCLvHpp/luecZN5pt/+HW8z2TfffarhjeVC25++mlXOhw0x3rnwTAXOF8tl8UT8rnFWjeUcufm2KNlEasKPP3mX9RZQZIkCtb5Xg+teuz9n6iToeJIrJ0xgaSIy214eo6zUw3Q9zaCShODNPlxakO0GTuDAkOqGcnZUTOsujSJoaaXgE4+XOmq48EPP2ze+wsfDBIQ3quGy2/4kiMGUeuxSO4HPcBQVNUc5EY9wDciowG6fXnb/M5/fCcPXgF+NfTWzyo//fZ7P/d+84Ff/Qi/5Rc28Qh1ct9jDsD75171v7/C4B1TWPHgabkaltSTOvhSbBFRH+C18TngjQa/+k/fZC48fNFR7KPXt95xCz+IcOIW+/64j7/1k8F+fSGtYrEqmh8aA6P7AMiJjrOdk265LyaeuaEcATgpV1FawXcg7yMPSUBYBaUJxtlK6AXr6Feypy+lc1ezERRUMWMCfnD84rL2uBcU98tCMpBGxY+HhGvhSMLNSMwPkfF4qGZjSAg1DMLXLPkAv/Wv3mp+40ffYi5Gjyzj7AOvg2l92RNPpOCG95lPneMN7xyrfZNf7OKFpCKfu+GJR4U/9MaP8pNwkMODCfgpA/6mdgG/+Ya3mHvebX/zB0+FveRb5/tDb+s3rJlv/cdf718DdOZTZ83lx+pPleGeFFYrZ6kNeJy8BDzuDTm09dz95/kVQSXc//4Hzc/+jV82H/i1j5gt0tO4j3h419sv/H9/lfct3iAM4Mx27cQcknFm0u4e8xmM1lUKwyGkQIQem8VLbj3KTZSN5DmKOgf/o0xAmDasUDBGYm2uBSSnUAD6FT8Uh8ew+cRSgTWFVDJB8wOveqBbduOM2WJdMEQiqW5eDXd9FIiEhAhOhjtfVJwIK7zX5tAdWOrgqv+eTw64LIMfmMPKB2fMG+c2+Ifl8B6yGjBocbYt361BU/Ddm9oSHveR+D1NJMLyj5B8pgtwMJzEmbxj4f1kmJz1iigGngb7uh/8av7RM6wSfvJ7fpYT3azAk3df8T1fwl9EFbz35z9g3vijb/bx5XCC9PCoKIBVWekRaHzz/NiNx7hPcOBtnN3oevoOP7qHV+ngC384gcClVP17QHe8/Bl82fKed93LiXwWIC78ns83/4PX+30HJM2PCYnAgJRVEQYUO5EkQkObgVVPNvn0u66gLOU5gYiqNB3kZp4QTRMxlAL6Bat5XGrHcc2sxGDFA8Kj2XHz0jafgGK8yJUTr8UytghoaxhOWPDz05yRXALm1QRCZCWZSI74fypRO67njl5f6BdBd3xlwesi+XwmAQ8cvOYvv8o858vu4Jds4m3Ps+CJz73NfO1f/4rkh/PwRu973n2fq33mI0g+DjxwafgGIzgezYXRnZILggLFzkn2HkX2R9Qy6HdfQFki4PiKoraNEwpxO3SZ0IiCQvLBlQx+rxt8JQbrHjA+7A/F7XKZaaRS0orpOGHFS3rxNm7EUQSzKnyFrJQnIkaq0IeNdsBVSzxj/EiQXTp9hq/pZbfPRGDF88s//Bu8MnnWFz+Tf1LgVMcPtMXAzfwv/hNfaL7jn39jknjuec99vD3egeMBZ8lYSSQ3l4HCMTDLsZ3T7bWXC5ExS0AN5E3P1+Foa4GCrWDCxb1GvveTTP51D+hX3KPhez3oY4jTltNyrADiGyv44d1wGTCrwlfISnmiGwj0oYcE2KMTDyvZYjfG6kiQTZ1xho9WPocEnH294Kufa17zl76MLwXiy6QfedPHzCffdZ/ZKtxTwZnXM17yNH6aDT9rcOz0sWRSxf0n/PT31fx+z/WA3MonBx7N9Ic/cYU07D5GOuAbh4Bj56SY1lAX2CTpKoJO3bpYnpulMlFx6oYVMh1J6FbX8EqhNsY6LpnhjQXDuG97gCx0/Kono1KzgsSFl5bi+Cv+lAuT27EUJXIMChWt9Ky2+QFaNr9r8hjjoxvTjB4ln0PGE559i3n1n/9S8xT3xUvc0zpz7zlzkZLH5sUtmiQP+PtENzzhpLmZVjt4+28Nb/vxd5nf+Y+/e0iD6PpFb/LxoP7ByOazaPSVO0Dz3Zanejh2ToppDXVB9pJbp25ZLOUUZZmhuGWjGaSxj1IXeKW8NhIJkgh/zYJctuZV8LGfNy/jXg/0LV2jFifGB04KVuWBmJww02pWGtwcM46zbj5ELNvqJGCM/W7Ydzj0uM/hKPlcBeCM6jmvepZ59Z97pVkb8ebqGL//Cx80v/l/vYUSWPmhh89Y0AhH8sFvBvWCn87B6KbuwjDPj/TG8HfsnBTTGuqCw1n1pJyqSd0Bnb4HhMGPVge8UlkbyQdPueK3fVCuTmxkBm8h2MK9HtrkXo+gGSMJ4GlXfC3Crnqi46ojXqDKLTF1qHXzA3Jy1Q4ikA7UWmJjoV8dNNX20T2fqwCcyX3wf37E/MR3/wz/fPiVC8MTYT3AaglfZH3jG978+Ew8BExEq8f6EzcfGnJ80CjHxMRbcKQMB1ANOanAfgeSxNOJsouUUw1nRKwtTDLlleramNTkHl71tJh46NM9f68n7OBmjDgZ2d+3P9eAtxng1+wSwEor3gpKTB1qM1BCO4wErEJ/8Dlx6GWB/TOPxAMcJZ+rCDx2/T//yZvMf/7T/828+2d+v+vVOA9/9BHzE3/uZ82b/83bytejHwfAD4qdvO2kq9WR9JIjYH5CAsLZMp8ldxw58+hxTKYJZjKcKo8yN9E31CapeqUObZoxZR8V5R35gOSQeA527dNygqYXldXwlQreP7FSNfNZ8apEXb3DACEro4jRsBIOh+5E5gFrLkw6k+FiQ4xHl92uIfCdI3wXBr/+eeqJp/hlhtgdOxs7/Ps+n3j7J+0vltYe/XycYO3kqvniP/Ey86LXPd9R8kh6qtV1xLeX5FLB0pHhyS3bDlPv9ZRFQk7TlBegQlM4D7xQfxK07yasDFY9+DmEHUosya+8OjNySXXr0g6vWnilZFl1uJ2K/Y23nfgvP2vl0o4n9PlwnzlIc1qGEr4mOCPuw3MyNqPe68ZgKmPUkeJdUwOrRKaOks91AD77ouU/zsCwM3Adms/ojpKOB76s+vV/77Xm1meEj53HSHpsRBf6JIT/BT1P7rSLA3TKyqfMTjlVU55JhapgGVZtwjSmfTcxyKDPsMrf2rIvB/WeSUSksDLawW8B0YkaVkpMx59amGqn4rs8eA/j6vpSeEWhMh2WOQpdQg0ENmKDtoHJ2wYKfmvdEWMwUWmEY/UkHm+lYO7ostt1ANzTwQtK8R0hvFsO94iOEk+I4zefMKdvP+VqeczaYzh75u+a0IZyjCn253/JLcRhjhLYnmzfK7YspF5Q40tv2AfCok8pSnLCygg0pguzBJ14qIx3HeJXSoMvlBYSD6gt84wuoQoCR6lXoQTUVMyjIz9ENivGKqwYXrShc5R8jvBpgTu+6Gn8QtUSsmO8MvCroKMWScMmInsI83Ek9jrt9pwd5lA2H3KaYWiBzpgFI8ULmGiF1JD88Vs/YkNbQmLa3XG/14P9I8xSf6ukgtLBvjEr8b2eSuLpQrdgDwZjKMnmGyjtnMHnYBMIawkiVm5ciwUvWjFnQceXKx3hCNct8Cjsc1/9bFe7CnAHDg4yTIJ4P94SJyNLL8xTCXKrp9ZBWWaHnM4QJmFm22ygZQX8ugy6D32t+xs0rHpwWZp7t+XGCeAvb2SM9+cKVj3MAtEVQjRNC0hQ7E+CV7YFqTIphyrTIjPyMnYbhjrZgUhDZ8A1/EmFIxyhF3d80dOrv3SbHetdB0AGBT1cAlpeoo2SEBJRD3K5p4bekDuP7UmY2m0ebKBlpcML9V3ysAFAfe9XPZqf7WvyY/8P2DP8YmF+mg7ZJ5N4QEmpBcT2J8NaKdvKNjCLWBI2Q7spJUCNTcaz7JpOAUfJ5wjXNfCWYfyq7bUGP2hKBxcObKyA8AQbLtuUEszYez3143bgdh3fsVCHEkS6bNfABlpWOr2wWNqHuEfjVz1VkAH73wOX2/Ajccv4vZ7M93ogq+VbwHjQ8u2YIniHw2pnXhB7oc0OLw12FlN0CEfJ5wjXNZ7w2bea21/wRFdLkR33Ew+GMpxB+sCUJeaRX+y9oSEJyYl0kpQqMVVYhIFbl5uOudjlhrcsjfBE9tCHsrphTepnfMk6+ZXSoK9tHHFiQAUP8eDrDLiXhESkEch2QPbzZLA+n9J0+I4HUx2pPVAaXjpEkjB6dDIQtaPkc4TrFvzLrX/2le6Hxq4CRh5IIo55EAlIVkPZVU8BdZcDtzu0iW2YCV1GRniKRHV1b2uvYsoyksRAddCw6uFf942eJI3Fa4BsLvGMSg+s35N0ZgVHa4sldIgkGKkj4rHaUfI5wnUJPMX0Jd/1cv5piRJGjP8Z4LzQR48/viS3TAmIJjp/cp5e4WH0xn9Y7Zyf3ZalEZ6UKIpS5Sfctvd45cNPuAl80U7m2cRDH3i8Gr/VgxMDkWF5W+wCy45RyIH0+1Y7glFpzQHWOzyMaQvCGGHWbxX5o+RzhOsSn/2qZ5lnvfwZrjYCHQdHFg29XrPB1SCa6DgJ0RYfvG17VqLXL2OE8Ci7NdRmF8a0oHJau1j1ZOEm81iJ6iAhRDwkgifcZNUTi9bANqSQQXd6IP1x650Oy4E5VDrtjwkD6JBPvDd0jpLPEa47fNYXPd18zV97NT9iXcLYY2carJfm/KoQnJUTWHWJ6EhCtCLCEdc25/zy306UhDP0UXavFlxQ+IjjQ5/ube+b/cyqx68iIiXsM0+iCn4oDgkIuScSrWKw4T4jdKQHho1njOcOBMF12kYchxBGYFITKh10lHyOcF3hWa98pvmK7/lSV7uWmHCERgeat6AORCQhXg1FSSrGBO9dmKvd5iw2zltRmhi5VY+fzCNFHRaXaTnKbzOwpG54M5H9sbBvMBtrpDI+vDlq0UJfqxACb64+KySExJ4m1If4UfI5wvWDF33dC8zr/9ZrzIlbjjtKHsUDqMhooKKnJ7IW9BNYWTUQHYMfUkAioiSkL9VBYIRLi06F0XaraFkb4Y1Es9LUL+hT3OfBK6gkYUP2YMFpKEWmx4aIgEer8Tqd3ldWsR1bVIXxYDutATTWPssj4SDxoFyHjcGWBR1qWbAttyUoMso4Sj5HuOY4/ZQbzWu+78vNl/3plzvKtYY9ikYeSz6JFA/OHDDB0qSaJqH5YmxbrifgQYOwAa6iaFyMG4k6dSpWPb0ITMT2InD+K8gweXLiyQwE9oU/7ZUOi2KTwoxomskxG2MZ7KPkc4RrCjxY8Aff8C3m+V/5nK5HqudwLB0KWpfRqnCNwlk+v0+OPoPvsdTQ0SHz7zOyWDXa7xETZEnar3rwpVLqX8jxulB1jaXJnxBYdWDVs9i56gkkWuLCj3YTyMzqTTyt3cwG3UrHKxEyeiwaFFL0jSr0sl2BF8xYNAXKgNrRTyoc4arjSc99Av+O0TNf/nRz6zNudlSHytFRHevuhLDv4FJIDFoC+yo6S8G/2UP/Wc+SBkTEJMZYwQng0EQMPBGUgknerT+ANbxa+4y5DjlPRUCFWBg1Xgg2gy3TBCQeJIwLj2yaHfykNvoXwiJLxcBTUBmwfmKFL2/GXyrVSFQLtjyIz2FEcVs1+ssNyzRKoO0nYo7gA9bCCiQ2cNQaoiAuyEc1KNmUA0AyL93yUVKL9a568rGPntpX1vPZovZeCvpaY1wXZQEL12vzgDk0kcGThjPGJ+/0ByuaW55+k3nGS55qnvdVz66+p605sBsYrV5QAHnM/uK2ErJ6ipi4K/i3EEs2CSVgWt5A1exE2GgwPSUtVOjzXAmdweMIv+mzsWPHE3cwPu1HC1DBpUxeTTcGN7i1FgXQppSSkGlCdaWp6GtgVsIH4T4jxOQ41Ib63HFVkg9eafHkF95ubn/BE8ytz7zFnLzluFm/Yc2srK3YBISBRWGMDKUINlOzFbFqXseFVLVEm23nPAAzw1lKBiPd4Cxz0qDD7nNFgN26NmLfLq8tm/VT62b1OB6b7vAwMu4Yo9UnNTqE3PvuQSI6xb830thnc2hbjHZTJzqNDVOnwhIPJW1S5CpuRA8JDPLadF5NSxSQU/RqxGR+h51u5CNNwC4jvy1VEk8jFaVp88CUlh9a8sF+xy8FvvB1zzdf/Me/0E0+1wEyrZ3ScXnMz9KnE/pGkBKiotR6B3rVRZf/PnhTI2zirQZF8TnGloBs4wSE+9/OtpYuCHzPHsjsFhRaxqgpO1t7/MOKHkRjNa2Lsmo2ijiRwn0e+Vn6bowQ9WCHIxWjmGsILMd6JbcZ20wi+ZJKL7i5tjgzDiX5HD99zLz0219snvcVzzYnbz3hqNcJotbOqyMt5mvt0wXBzxBnYLlOBh8l8cIBWVNhzOV+j11HelLV4QAkHqzwsrcUAoMWPkYUZr0NQ2DzdAjjfjpeIYN/1gf9DXzPy1kORYZHINEQRw7d3zPmyqUds3lxyxFJDQ3TugU7SD442cXrdPSDBlW3NWbMsx1McIyqYUKOr23wFIz7NnYfVc3FfcBQ93xK0NN8dalev38kJHwiFN8MD61U84Mw5pl8KJLbn/9E801//2vNsRvWHfE6Q9TaUY1vYr7WPl0wOvmMRFVlgj1GomeTD2OETb6Z7coJanZG+BgD/OQADmmexsjHMDnMwWHWRNtuItERCn6zBysf/LQ88of9VVOC1s3YQdux6lk7vhzwmy5rAjGPO9URW4ZzfL9TiOnNgNgwVhQZ9nIWrOMUG6IskPGRc5s3lQ0wi46U2Ye1E6vmK7/nS823/eOvv34Tz6Giv9OPcP1hyt6Lr3JdD1iioPg9ZvSJ26mHi3qvgZtINDrarnooeVLGQZlPjaUdWlfK4OmNgO/16B+ia7isC8Q8NuuIBT2QmVXgWxDT8atiGt2CDug87sBeRWpcJFrSHnpX0OkHim6bS/JZWV82X/kXv8x83jd8Dt9gPsIR5oXqcO4Y61lEemrNMw40uxc1p8Y2C8invfDGobmf/rbbzBjZnlmav7dH6zaeNAkUOueRnMGoWVDBqgc/k40VEFSacdQEmsohtL/ilS35Zqr97+XnDuk/jdowwDVNpyJx9cWmJGG/tSnMnHzWTq6Z1/3NrzLPu5q/sT8n9HXuEWaB7ePPvJ5WJ9bjcWjdMaRRfGJDnNgwIdu3KEwIPBtvuRFFTlmFgdh41UPJx8eJD9KDqlcv2aEJFz9nwffgGr4mgUNyhiP7QbXk22Uk5IWSyMxg4yOtu3hky0HGEbqAu0FLD8Q+ONWZkg/OMl72hz7fPOsVz3SUIxxhfigdCDMhMjp51VObxA8l8AZowmm5xYTO331ZXLRfcZgz4H+WpkN3d9c9GCHhEVFsBhFH4WO+tb+ltMgPXnShJpbl5RViaq1nx+aFPpBRGK4ZzwUFcWwuq/gEk9kYbF6UHIRXgojrzWGm5PPC1z7XvOQPfK6rHeEIDaiBd31ghoBaB10J10Mf8IRCiYhOHpGEqquhznibYi0BCmF/f59XPjoeqAVzaskOCS1S4sEX2Lti7pHR0NfRXBEfiZmSXdIvsXqQ9SWoGUZXxrs3Y6wyApx8RqkEEaVNFZNtcvK56amnzRf9kZfyAD7CRGT2iJzAzLyRuUOxm9lKqLCuC0yOr3akXotG007odQs5LYuJHgkI75SrJqEKZm2y1V8w+7zqGawFceKPEKIw+YFdotm3GTjiLEhspEZLbvI9OC3xQEe2LIQBp6VNI2cslonBB3g+ArlNFGz0R9drmJw5vvA7XmxO3Tbf7/DwTcLD3tS/sJsKG+nEW9ZuXrSy5Wx0bnictrDhKSF+3JbOIEnUbSjPeVM+62jx82hqTTM7F0y+ZHUoMdM+cKUWqnLUJJ2EOBE1DIPd5bsixCxyxePWrXrELg0z3lq9jctsuNyG2DE2m+gQGRAGwDG5coIMA3uHyfhTVAyhxeBabwESQgWdvgX+OEe5sAXIEusYnXwQ0FNfdLt54dc+z1E+gzGyM69XzL0ZHQYTkeuhL1UM/VN2hNoBfw3a2DPXAl1iTkiSj14NHUbTxCZ5o5OlfXsy42lom50AXQgWqgxZaIDPqx4t14mkXQEh5Lb6OnbvW5OLCzS9KUg1Ik9HFDeqvJEDX9Zbz6DSwXWI5zA6+eBJFLy94AjXN4bxMHFkXENczxHLZDwah9CorkmCMNk1NZVXQ0v2J6il6bDXbTMjGOhjAqR27OGSW9S3aJ6nRHZslaZ3SlhLS0v28ermKpzgRPCBLfAYqA8Vka1CCbA87hHBuDiQsqYJmsb7wb71Rn9iWh4QdFsv6gabGJ18Tt520tz2rFtc7QjzxAz7kTGr/vxxSBHNaHYm9XjiEFzlzh/WCHV0h9UQxJc2+beGxtwbytiMSVj14ISWn1CLzGI1ZAv2A3wUbXWwhEtuXncgF9EWGST8L1Y0lLhLEB9/dgQhGCFaAkzIFqBmW9rFIMGabA5j5TMYnXye/uKnNH/m+AiPb4w6CK4WghgmBhQcsCMw5/azuavcp+IOkyw/rk2rjVGJiFAKeW8v5YCC+0AasRRWTLwqo+TTswrkE3tXBuL5VwPVrsSjjFixmnAEEmU/ja2EKr+mGGOM7BwxOvnc8YpnPq6fcLtG+2kSDiXWQ+6Aw+7fWexPftBg7uhrxbz7Eva0Tbkv1JOEcrFAh1c9tGl9FOVhFrLMNJ8IVBT7tDBaWkYShIIjD2YC5PyzbLKRXyn3wBm2H1kvHuDG21h06bYM67ZNCSJC24REHG6jswgeNjjCrEDnT8Qo1d6LM4eBa+e5jolx1Sajq9hU3qMd/kaFNKM9SUL2y6tuSlFPTdd0+VU6MTgpUUuJB92cPhZFSDrLsuqpOGGW5sfJxfMyx0vF7hjMahfiXSpNISUwMgYNiSdvQnEr+2Z08lk7uepKR7iekNu/HVci5o5DdznFgdOZJbbWmf3VQE/8kJmlnTOB+mhhgVZCS5SE3GtuOJ5MQHZ1k36pFECNV0TIS6h4tk0O1uaBWV5Z4p/XZvuBnIXI2j8hIlESySSeOcDH0ERZqjuupiAJxA2fAdLlg0kE4IJQxZLPx+/1s8cFZO9/euDwo53BQ+mgvUpdbN3Q34q/SaHMKX6YsabsSgaJBwkIiQg/O8EYhAgLfK+ndK8GP6kQcoYa7udT2uHkU0LeagYkWEw73UbyKKrPaHc84FCcqn3RACRLwz6lO4PaVQNHyeczHp0j4bBwjd0LZgqjdAReJdjY6y24lt2c9Q0ibVjUIPnwl0DdagjAq3TwHjc87aYBPi7F4dFrvSISH6Dw49UrNqnFj1c7twNCNhvwVol3GIkHqkX1GexW0eNQ+rMzBoi1RZVUTnjYhQmOkk8JmY5s74jpOCzb1+LS25TWdGlMacsc2n8tHzQIwp9DW+aNMKRCgI6MfkQCQuJg0OAES49RzI97tOrZ33VfLqUtSBAoEo2/VBpBSVkkhAFgFRNPF6yFGNfkeCv6zDDmGp8yVrBbc3eUfK4V5joIrg9cr02ycR1CdFetwXVHk8OYMf5QvcMYRGjDagcPC6yuLZsVrGCQ3InO7H1jdrb3OMEAkiBclS/T5b5UOpQq8Dbxp6LRNCYCEpWlNBOP5zcd9CNriqOxRQ/bx/NBZN8Vheq3oXuyGP0z2iPFP32Raeb8Wk6WImPdtkfFNXDmdsO8EujAUkIVeY0usU5bAUjHqo1QVqK86il13eh4nEKn3mjzQEFpkq0SEmP91jEM8SAB7ungp7HxSAKmFPudngWzs7lrNi5s2X7HpoAa5FbXl+39HipDSya5UJoQh0UCTCKH/D3QRMEC8YCdnJlD3hpwFVd1n/Ld0tykCx7mTrz5AOz4cuNgewSK8hyNLY612yVv7XsxKmgV3zIVQg5HyaeETDPn13KyFBnrtj0qLsehj7ldOqoEOrCUUEVeoynWaScA6QxqIwwoUTxNVUSnyeGQcTfjO/QgMoiN2HcF2x0u+5AYCiNtAclnd8e9RHTRJh40z7ZwwWxe2eUExC8aRZZyfAZVsUpaO77iCM4z+P0hkGxZWDjikkHy9rtMKIYSNbfahlWjP9Ws5z6vNTrj6BErtJRxlHxyyDRxvq0ma5HBbvujYnMcfNAomHn10whyYCvBhg7QIdIpFIF0rNpIZRFHn5WSdodJHCpLlLyCbifayGhmwtx9zcEgHqPOAXMLnoDDigiX3+zj1tJhNlHxgwv4krvEIX3bHZfYS8ErFsfTuwxjYOvKjtnd2nXjQZ31O2j5GHbKpAaxUEGyENPMgN1acDEqcYwNseX2KPnEiJp3OK0lq1P8FITyZEV1xZlXP5UgQ1bqu4YOkU6hEMNQnaBMqPZXT7tIZmXFPeHF8tbetGiuc3Q3yl4qE+ge5kTDBHdRiv7Y1QY2/kN9qV+lo7V7ABt1HeGKBwA/zX3xzIa5cmmbf7Au5zdnlW3wH5dsWSgjqZ2NgijmvBMa7ACVGKaG13J7lHw0VNMOr5XOcuSgy19BKE9WVCnSaJhp9VMJcmBl/DbQJdZpy4PkrcpYxQHzSD7L+K4L2ZHDZno01zk6G1abP+x9nzowfmeagyaoLi4Zc+n8ptna2LGrrmhajUdJ4AJLOICFIskZmhEqh3Y9hwpdh3sQBxRwggBij3IePZrJPbXHO9Dlwb44DEQOZvE3SvfQGzYehxXSzHZrR+112I/XFHPoj97EI5+y9YPsT4ozVqr7DKQl8cwV8JBvSMDJixAQf3mzaQfKqM8XEp9sR8kHoJ7AyRQ65HCRejh8nyHiL+V9WuAahDxqXuvEp2HPzxWlFcssK5m+JAT7U3ZoHFdqQ1NCaVXLuZ7U5FQJFNk8XMX2S7xFINnBBpJlYGkyBpvDFuNxn3yuTtLJ46r4zTj5jL50Og9kjtFZ8Rnb4zM2jI+/DhutBINLpPlE5IyPjrNfAZKhNNWSRqm4+k0riBLsYIVit8AuMML2MPfhr1aM+3AOyMT1uEw+0tVXdw52zsKPfmQUyjYa1ok9OgFVxEdauooYH5nVqBx8o0xa4eu3f64eDmPV4xHtriQJjXaRU0jHBChZ05PaBGvRFpiRhFNpjmKUkrXo2xDx5zAuDTp4Z1xL8LhLPoV+OGSEXucRw8w2YODadIbHYbmfdT4rHLejA7ZX0D+DMWPjeu7zAKWJNEDGlE1CNMX16HchtJOPPprMWUXp9faZG8T4K1sVDYHABhfmd4ktQeDMIeg6K/C4ST5Jf8SdczUw1WekVzejuA1//NPFc0W/727MO8QK5usK1uwRdxWb8GmD3sTThYYpTkLuslwbs8RFuqwOP/GlMdq6TA+3/Fuvp/GI7Eo72YbbPPiYL612JNYJyDrLYRD4jE8+SX90ddA8ETqb1XVdv9+6SM51ErgOMLRmWrv6JqgWPrP6NIsRTYwvrc11zI3ZXSTLCyHMelm9WeISXWuYa/CXd5SFJB1vqgcZWZCyJvjpuzHGO9EyWeiCz+jv+eR3gPu8anAO6WOSa6XU1u8TzrH45Y41FOyF5D7/QIM9oFvQgsWHP90Q6Wo/dJt0gvSBLyUOXzL9DMKI9ug5Y2zimc/JgCDjm0g2vHJc+OmGC2c3zeblHX6haQjRg6GgRrFj3nXyzEhXHCIPSaQe/+qlnmYPygMos7JvWyOgRlt13p6hjwtmA7Izbz8Gzmdk8ilGeNVDHxxO7jalVzcRcQvCNRt8eSI3DitKIUvVao4IDfaAbkGLoZ/HKUKaJ4vSZDclYCo+3pPPLIkHmF/yafvmWDNi2H+bGztmZ2svc3IyKIg6Szgxv/JhMSs7aCh5gTBjNzloQ4KkvyBEW07Wo8dZAcpu1oUybYuD1GdU8qlGdk3Ctk7571T/Tq+tHklkFHpCwADhyVKjoBiS2/41emJhdAs60eHPKEBjrqsegIqcfPByUkeueLgOUYi2qy/oLJ4mQohizvATe7UDwHTG+SN/MuQkmhhUezWcJP5E8xyPDWyengYWe7ESmppSGKmpjJBDTlaD43NCLdlZUIovRiWGz4jk04zomoRsnfLfqf6dXls9ksgodIUAIWyLOM5o1GDgVBRDlqpVdIAGO8QIYRadMD5F4zCSDxIPn72PD6uIOZqaBhVAaYjgBOZgb9/s7Ow5ynjgTdLzwZQes5fB8LMNvGKDCXdIaIhlxw6g6zW5+QLWxdvhYNQh5hqba/N1k3zE6pgd04zkcELtgHXMf6fG4PTa6hmJiNQdQiTI+Yf+5IZzWB/nsMEe0C0oovR3hI4AKtzO0uDrtlkQVJf6J4QXAPqBDcRcenhpFrCTvmi1lPQhJmy8C213d4/7VgvJePIkV+B1EsliisHbwJdX7P2LGKXdpBHY1kZq+cz1I4uzExvpAeVP/LQ3kil+5QE0bidtLKvto0w+wstt8qEFHUDSZGe3iFhewHr44wQiORTLZmsOFUiM539lN9CNzaBOsiF5UL4myadkIY69hK4IZg9zAganvjQ1DtLrU42k6tUyKoLxxJyK9jvtjgcYIcxnqaOMD4Da3Fc9Goo8MUSPRH9WgyWw3bZxkcD4QBmvbzrYO6CkQ6se/xMEYNgPga+qgpW0l+pWVpdonxA10utBoBLrV3YzZLPuSIfViImfeUAiQnLFJidnAVie/iT2Ius5Z8qYsD0pJy/gAzQ9y9EqyrRCnqphbdDfrH+lXzAVkgcjk5LPKIURaHVDt9/DCrCKwakvzRBH317JCCnSKPcFYZCx4RzUJiE7OQyIFAt2BA12iBHCU5OPqFyN5DMhvABZ/VmN5sA2Owy7LoMkEg5PyPg9HqJjxbNHCShe9QBB1fmypuzYwr7AqmcKIlchobKLIZfoRkBTsOHgRFv3drHRiojKsM1t5f8QYjGFjPUcyaoGYFItOK/jhOgjFs+YdchzQv0u51mk3MHW6OQz/y8mDig1Y5THwwuvgMFh4npiLLMmnkluy+YcqOYIkoTsDosUM3YEFVYenQr+dGi0A6ti22PrWXTZrQhND88jqzuLwRoqA1D6CefY+NoIpg9MxEg+gPC3adXjocwFln0lTD5LlHiWl+zltzFIxGNCYR/3+tGnXTz+CWg/EhB+BI9/JI/I/FaFwGbGQUTiaiY+JmXUBxDTD17aDwXZjGlCSg3Vq44d8pYFMVcvXa775DPK2+GFVoB1mHU7MZb+7s0IEmmS27ypCBnjbocFk3fGFlgge1ZuRzt4VsZODIhgQvAXG5xOxXwAjon+4HscRbCQLQaIVPTElACTNH3UvrHeitm3UcO57G1vF9hm2hYZl5xsaJPEA7r3TwWMhT1a9exsp/d6EqtMUIkHdZq3V1eXmDIGiW1AEwud5ELogMQZgsc9t/PA7HICshsnY6IzP3agqgEn4yCnPsAyDqwTlFJZYmXMOrmBk7pIKXlkrVcw2L1ukk/chNFeDiesAqyzossJsXiVLt2MEJEmuLWIFFM7qXGp4lP2nd6HcmYIDKqRkRq0aEnNuWD28KcOJYIJHXEuuUtuA0sJdZhsgmykZjoNO7FJYYxU4uSIS0gRJMnIVCFTgNrFHqBtb+35y2/F+JlgqdbMArvGj+/xgwaJQh5FsZiRiZVFuvxYoYyJEGgw/UfiwSW5/f19uxpiltMmU1mXGeP+C6cJLIP/sp7YjhQcOTHtxPLpFCg6zqBko4TB9nWXfCZZP5yQstDLxiImxMMqXXoZISJNcGmhFPM2HLUpRyAGhkc8KfEkzyUIcKEIz9ZyyWm/ZfLjvFwisBNb06oBwHBMLtIf/EQykk845waVPCKRDo1IaKhUdX3bM1Kun7P6IFYNh7Di9BeFZD4BvXFpkgA+kg6+jMk2nH/3EcLNI2ISEyFIq2v9DxpURTSzEDeLNP0MAo3me3vcT/QH94LsfTAkIVuGlWw/Zmj5tx3kZiASEOcaRM65sn2b5bjPMcjZqWHwcV1ddpuEqxCOddHpaGQ8gXhTNy8w0y5RuqkZR6nKKGSYAym1FcOztEwibwk4kwxYqhMSFYFiSHGZf5M/UCcULQzI2GrCC4YaVf2CjqCoWzUaYhCtKHXYwz7Z3d4LHq8uqrkOx9QliQeXP1dX+1Y9VZGYWZgf235CgdY0y9KRTbvioZMbl4RwSdImIcerGNUsqxEZF/jruYrvSIkNL5JzXLBPACenEVDLQgqDj2mPlFwvKPfV3GBdXDVHowCVngO1CKfLdmxRYaThjPgYCyO9hfJjlQk82dGKh+eGWY31oGC26s0zDykmwmC54qPHPfUjzmP39ocn3IpqatBqGXy3pwc94VwTZAJDn2DDam6ZVnWrx1bM6voyP1TBfEpEqjs8ZFjaTUoZFMiCwYb8AXQ/1w143RpEqG/3eXz6Jp9mj8wG25/Sqx0YISoYJz7SeAvOXNOqEhgTQSjb66wFZ4AzhsY4wyJtz0rnENY1QjHuzgYNYrP3AKVxvrQkP9Nethhz7D7ArsDJQG4i1mhGmjefYrShOlrS4NtHsg848ayuLXMSWl5ZMvxwnEtSAltiLS5lUWLl2pyVLdtueLboEirj0zP5zNDgFob+HOFkZDyDD4emfirAlKZeHWX1gr8SIubYsAL5scq9cHbxgQ1nopjw3Fzp0OlciXVqKAwaVV3PzEuN9zsRIxzhshLEqyoZJuZcXHJLzikijAhlRozzxNJFFd0oEsJ/l6QxBldoNbRGiWhldZn6wF5yjBNRFg12gEBW4skbADXHSXZNLNTYdwKxj+3aJh+Johdj5UdgMD3Sych4EvGmfirAlJF+A5BuWV1xXHGMq1S2rl3lBky7Dk1mqNZBquAlqeCvt/erJ+hWzQjO4HYuGPzPHgm6EpPpHm1yyS2PmDHsy/SnCkJMinIwH6JqbJynLmmOI5XkRMOrITz4IquhJS5zN/YkoRhxmxN1EEKiUBLRidD24k2AMK9d8pFI4s4qQUc+J6SdMtLJrOIT9EeqZFG2oTiu2PSXqihUmW3bCmNkW8CQwyQZHtedHpzYtHjGauXlq1Y6XEyLvYYF++6z1kTJN8b1RiRSkVVPSb073pkb1m8Akl46UQvbGAi4oujzRn8k0eBlqlgFra6v2NUQP/pn+aLrP1toyIHdY0pawVBKUsRuHcj2r+6B3AZcm+Rj4+vHWPkGho4SpJQqRooDI8UdBq1Af5oxVisd4DlMdNOFrG1NDARsRe7RDOiMkMS0pP8ly071eaPptiIwa8ihfsNatzNa9exBmDp1QoB41D3ZtQ4jQujDhPhiWBMIuLRpKIdURK0WgiQhmFlaXrJJiFZEKIPGl+zGHMQZtGKoQXQHfX5W0W9A3AMlHF7y0VHG2xiMla8gdT8hoAnxZFWadgaBCS4TsI2qIcWk4lifqXxoT2N22+MQ6FPFX3Lz6PTgxEbF44VHaRHGyl8boC/lOyylBFIDdLDyyc2nM/XA6Fhy3mAk3DDF2rJDNUhiOlG0b1R7IO8SEe4F8WpozSYirIaYh0t22qgKK3aGqmw1SOv0BrBuRjnd5wmhiPHJR7eits0Dc7BTDmmC8aujQhi0Ev2RBiHOKlW9gTnqIHGCqXzZQpGjGapMh5grxSB6hhWT8gcMHSAlsw1MVPOYRf9a+q7Brnoa1jNs7Bv/uHuEUbH2ClflEES8hRgVk5Iep5dCkhBm62VKQmvHVnlFtITXEFGYSELMF0TF2L9vITF82W0xvH5sZA64NpfdCNyWXGsFMzbWd1qCMqeKeapUbQ3MRGxkDH3iyt8Y+062qaIExpgHdOJJL7mliO1n/SWX3Pqj6pd0iHx06Y92olDRTVkDpTTxNEFKmPRwv2dqQsdZfYxZuoAxqTETUAzUMuReyOwNslggg5JoFpeWaBWE7wyt8veH+Ek58PhEYAC6It56ACveUiH+jkMyD2f8miSfQlssXGCzoKw+0fAEtaJK1dbATMRGxhCIF3Ur/jqQ1xlDddBMV67KgxsJxPJZfSLyL7R6ZKWyGJWYMxinnkrP6N5DT0KyAYn9Dof+uz2tzsmwoYJ7b4t08q7VO9yGGK1wdXBYYcEub9RpSDZ4xRRWQyt8b4hWQ3gvHvicpKCRgRiRne8g5EAtqLQQGYxBthCT+LiqyUecFlFltlG23/ScxwxqWVRtWeZElwEC/aKxyF9RLgOS7RJ3QmNMA1a+pJXSY4qvZ0z4HzcbgVIkfaBJwJWmYrK+yi6qSAgtzhIfVj1TAJ84W9e7Y9Z+qiIybu/dzIBisNH+nmOjcqYkCR3s75vF5UW+J4RLcst8bwj30pCE6kGAO8cwPcQuJxxsTB0wl+TjnTS2KpoCZdTtTzQ8Qa0eRxtF3RFG+0StlJcdYR8oi6ecpmklMAzQUCu55KZ13KcgrgeQ0V4VCjFCNMQURdaZ7HHIMLIdJsg+3tqM+z1jL7mJKJ5yw08zAJNanVPKtVvJoWirkzxWgbTjrc69//MG8aDHAXUi3zsj7/LgB96cgEQ0fHkVCcpuQPh4dAZVJkHp2w3eVV1XChj9YlF8kWwemHj1I0BdbYY4J6g2VaoCatDGGBFLIprVtcSANcZHUTZiULXLrBPij+FPABxWFiFPap6aqnqAhQN0iQaeFcsIR6SKOQbzq0JjfRSM5cj++HHMEaoeVaaGEqQiajh+d3cOzPY2Jj5LD+EICd2+QRzvcZOXiMYiemoogXUS24ScspIbijnlkcjadUAcc3BRA6ZuvBgXPzmOEwC4xPy8vbnLK1L7MIftEPl5B/zwnX+xKfH5M9dphQNdqDJ/D1JE6GrvIHRVL7tl0RVwCKiU1ercKiaqNlWqApXE04ls2FmjlhiwsnJ5lEUjDlW7zGqhpsIggFKX/QjDCqqt3ZJgflVodh8eCDveGHUL3fZHQNu0bzRwlQBlz8KRl4hOiZF1corZWCwgPqhM8VpGYq0Sx1xADpF48KN768dWKMnYaRyrGSQjpuHxdfVvgVY/S1gJEQ/3hxaXl9gGr4SCBrBxVx4AilCTfe6frKgAOtF2bZNPK+AIugPyGGlQY6JqU60qgGFRQUdMWZECEeSA1WFfwKKd8iPMMgb5sZoKFVW5K4ExXxRzDHy0omjxe5DamIfVAU1r3e4GQZmTMPns72FzyafTFsSwQcfej2ByAJirodPVAFIIdfIWRtt1KOpNNdiBAxrRSDJ4HY+97Oac0QfquMwGniPZzSUalPlJOUpA/KTcKslRpzOPf54WWgNEP4uYiZ1X2jK4RsmHIi62KI+2+EiDGhNVm2pVATsQimgYB7vpX2GMbIy6bsjNTShZODn+qOlEArFoS1X49po4UNao2RL0yBSlHBknij0HZx3KR19QMyF2gZ9OyCMfly1SQ6nAqx5bHIWx8lAIdfIWRtt1yOpN2pcdgF302cI+rWIW+H5O6Vjje0CUgPA0h89LSD604WlPe5mUGMTnBxSOYzUkX161CQyfiXnsNsQBhmdOb/A1SD64NdWPoJ1FtCWy6DOexUQ1h9kTTxEZZtZbw4fAi2XlQ2KnSS/Y1CaSjBVwtURQ1wyHmIRLbsVeJ3KBEyCQ6VGIQY3hxBMDR/ocMT9r1hL/VUZ5goq+T1KEtFltuCQ0FoG3nOuMyVAsH69Qx0QEnbw1hypTwfWH36pwXun/Mq1ckET8ikcBFModDPu1Ahr1RFigxLJ+bNWsU6JZP7bMl+b4F3zdd7SWVvBbQ6v8e0P85VXKDJywYLEnvmb8Ka5S8kFvyAa0I9XSZfRJZTFBTbx1qRaE3O4so2F8jK71ldFo+BB4sQ55Fum0C4SiFUUaKjG35SbmL5KR2ojrsdeSGVCW7LfRwow+RgQSi2I+w6IHm5vbFCLpoNPtWTjOuvlEIBNDaR9lRJvo0TksuwmoYXxbBA3Um0bVcMjEKj4GJHImkEDwYMExSiq4VMeaJLhMJwDHeLVDCQgrHV7tuEt26yv8FoXsq3xyTiZizslHoou3NsZp9EkVMVJ1tLeCcDYRaFTYzRgiZtFX1ciAttggwaVeuyQXihYUnVzp4o5HpB5bQx1fZsS6J4sCWZBlF3XyDFDLbsqcPCJ5V637mILSCLJfLO31FkthMuOk1YkpbWKdQDG1oild4ZBCakUBRsSQlGnjpMPEgZ2gaBiMkInEPTw4YxGrO9cWxFzDl08pyfgEQkDC4S+o0opHnwlIooEP3A9apVWS/eXVJbZpk5Q1469hheF0Y0LysW7zWz/Ga46TTjBBfZK3gkIz8VTQ1IwEivKdIQRiWZ2B2GmS0S1LgpCdNfEIcKx2+1YYp5OXrtsg7pTAIowy0SUsQphV0HnukzoSE49/nU6A1HBMgUrurL2EbKg5ojKZ1YnQIxMjqwO/smXQ5aco1B+lDoM3Fw9etbO8vMQrHiSbGNiX6X60sEmIjkCS4bdr494QVk/0aX95lTaVzPoQCs955dOD4jloBpCUbQJGqmvxTpUQGSVrq8NaQaSpqQSsLylFaBqyCMSyOgPRlzpss0gil1cEFZscTC2IfA4Y4EUbJSVCkVXRiTFCtBOpxfn7AHCMql5zTnDxMnidjneuonA7LYzLJi1MdLjsJuoa8T7KiDSR1wmpY+1CnnWiAON4Y8R+svLFYMZGGYH6GckGD4Xgh+mKIDeIK5+AhhjsasetlNZwbwhPymE1tGhXdsRL9ykI8RbiGiSfFsrBdmFG9cnI+LOkjkAKImOaMMiO0QoRaGbNTPPCsolC3gIGcZftDiE+YOizcHJXRJf/AKlG2wZJjHcUID3gZwdMBmYjH8FTbty50UYoxSXf7WlhVLPEp/2w8JXQUs5uLaJSHK1WlPTagGZdW55Ea2F3Z48/eaWZC1jZ8CcTjHIMnGSw2iF7SDz41dU1SkIruCTHJxX4QUGMj7y+h3Nx9ZKPc5iHMIsCdcyoPjMiv0MoHQEVRDo0vdDga5qxiqaClUhkG4p5dkNpBMqWLKf4LreCYjWyLDMl5sQQhWwNLwWEOlMstJC1KUQKHBMPf7fHtcKiLy6cAGAibE2cVXZDt4axqlNcQSenp3vLIxGse0S/YYWCiX5vjxJL1qjt472dfbNDyYefdsvJKVfh5bh6DB4kJvd+sJLFfaM1XJZbpdUQv8oH8WaSJOqKdrjJR5zRFvklKOZUzKiuMclM5H+oRowSMiKdmizUJdsQyLIToiUUyEV4diCXVwI1GawlkBzL21oRfNJHW/YgiNBjrwWxgeM93gTexwzOJqtWFLMsRUTCwaqHz5J1gxRYPDFkEw4mxdIKVMiJ6hR4I4O1kt1CODPFAZvxliBx0OPxgBPP+vqK2d3d55UNf5dHAXXwtrd3+VFqnHjpn1hAKfa0S4nKoieGFNi3fIWBGmrvDa3wagj3mvjYQ5JCgsuYP5TkYwOy/mQD7ODTlAmIjU6ENjPJlFMKbQylJjJinZosGMoWNCsGwcqye0312q7ICVikQ45Bcm1RGvD0F2eK2YNfAXJdrrNCIRG+Sv66/WQxaPrSdGMJmqZco9Lv9mTiKqD1oEEzhpxA0WR/XDHGymvUW+gQOEAljDXnH5M7VhSY0AGsMDav7JgdSjI8xu3Eara3dom+7VYiK3yZE8kIRr1dV8BTh1gdgeDUZwLm+336gw3fKeIn5ShR8uPaSJKIIUpCo18sujvxFerATI0cFeWAiWplOIOp3RGelOjY+MK9VdGewsoy7ESeoNd+ucJgCv44Vihhv5Gtwe0XEpVDecBS8HeZJjz+Mh5THFQFxYBXgwj6cByhw0AiMuqQs7KpDffZi4x804Q71LELME1sbdv7CFZx0A7sREYPDuxDBjhrz0HtyjpyAmpoeDYXbK1lU6kzWvKA1umRDxAo5GOMYwLQ9/iCqL2khcl9gRLPHicbzuk8xu3lULzFesX9pg9WRxsbWAVRIlhb4lUQZJCskLjw1CJWKZubO/zC0eIl6gqqQ5nMSWL0LzWljZMQeNd18umMbFQDZkC+p0Z4d6Jj42X5RKliZRor6yMrXzCSUVdIlZiCP7SlXCBMPrFMuj9CwnJ0WUKzUUzUuxEZqiDL7j7krFwi3auuEek0TUDACWEC2d3f4wmPJxOmW2ZgJ6gA9pLb8vICP+6bazb2boacIiek5hNmD3+aNuOpqCUv0Hq9Oh5eoXBcEZK4aKLG5SxcRtNTtVxS29mlEwIiY2XJ39khYPUBQ7j3v7m5SwW3cqINu2+XdGAXl8iwX7a2d/hN2HNPPgIyy+MGMVHMkoiuz+TTiGhUwHNAvodGROFEx8QdyJYrKQrspu9E4PAST1Cj4ZSaHCiSfLRMa3+gBD2sfAJRV5k+ggFlMTCeIstmYkORYWXKNkZC6XSpQ8gJYuLY3sGZsrtE43ZAYich2OSDty+XHrHuQk4vmktYxP3JutHyJBBVuzBFx6MWm0LULOqzA1qdUJKgxMIrBgXsC1lZoG8hG0gQa3trj79gin3HKx+i4ddjV1aWrS79xwpoc3ObitbWGDT3aWSSw6U/iPVQ7vlk0WoXGiFbBM3KsA8F3l/icFwUPCjwaatNBLLlSh4ZdodWRuAqJR5UaFy4Y8ANkVReU0JuGclJHClC92okHrAq7C4UbUwxrHS61Z0g9g3/FgxNfDxxEB2sHjsY+9gP0GtOUrOC4yqMWz0WIoEpYY3WqcWmoMMEeIKmDsSqJdeBINnv89h7LbEEkgtksB1z73TDZTaseABed9CGFQ9fnlYGYlvzArtEEqX/VyX5oCFxxzLAkE1BkyPWoSLwmXWeEIpw+7UbiTtfSTgpIhGpNrQsEqH2QaKRUVcIuRVWExBnlaxeSMRY4wPPVpmNpJNV7UZbGxJVqc4Axu2BPjRjK8K9+8spl2JbIDL3u60SbAkvEeWkNRXdQXcIOhEJp9u0wlgdngcmeSKQ2tISvkMTJoZukA4meN5/mPAdgjJtOEGw90ZtpAP3cHEoyUcaUGxIxNCyinz1UQyiLzKv3hZleHmuOXhCwsnDiYh0h8aASFgGXxYZRt1XyA1qsSLX8SdmWHhqwk51UEPi0fPdbKsdIHKcxJElTUJlD0xzQjqj1SIF+12Q8ujQfa0lkHRaT7l1ASbiTaE6biNZQVG+grE6Vr5PKxsmEatvKOgAJ36bASsgISQfJVPotnFoGJlL8kHMestCCagib9cv+iIMpDoaVLTKxCI3hBPrlE6hlKyNipWIZeUz8MSBW5QVVJlZk1XgAOIx7wb+pDPGGjL2ulw0hMCu7gMPNExvZTTnnAYwceGS2550asUYWJrNKqSD5DPTPqg30fodYb/RjLnB+pjuyV5yW7TJe0oHQoU2uSeUg7z0FBtWVxXRQ8EhX3azTbMLuqGh1wt0TGFcKSVGotdQSeQ1mFHkBoAYbyhb0ng4RWujYSliFSU9Y5DIympipBPLZ/U9Ui4oOH7kklvpmMVB1n+cjY0ig4oQx+lLMbg1dpOXewYoG664rCNQXODEw/cGKkjZiNPer0hj7kerDeD3JWyCE+uUngnexyzOSBf9h7GsrpL1oUO+JjJpj8mu1lsDh5B80CzZBB2RHDJ0VHF0NY5GVqKikpWPwcyqxGCnLtYHsmHtidUCFFuKRWnPGCSyspqodFCMWboeVoCQIPJ4tJQTC238iinA1fUGJCaziKRUVXzOgkFfSghObw5ZR3nvoOY54wAbuImNJ6G4NtYohc+rHlEsNC0HaLTcIap9SNWuqTb8HAaGuFstGJALE+cai3g8vbMNfnyj7OpiOKjTJucxXoc3ECcAalNV6aymv5cI6aPW7XOPKY/wTUVfYwpSGTJImuxbohmqeRkTZVS6PuCMMpoHu6KNTS1gNVqB8ydu8VmUBxPGnYDoJIgZGEa4Sy1F2kREiy6oo2/ByWtoChLO0vKC/e2SVJTRP9ojQVWNTcQ29XHsk6CD5XWfr1tkhVNiQBnlwAI3okUNxyymBnwHhFGzF/Bs3y/hcd61Zdt+1R8sW7EVs7SqBicebDw+KG6nyPf7RIk+LVf+HC4GF4jLFR3Ql3qc8GTvgqNJmGn4C/IB2kBl+8VSux9QLyFYGekyKeGtFHu7e/yTCNq/NohLbvhC6tbmDj94UAOnC/vfxhpHxgxbrILkZkg+g1rLwGEln1GBt6Qjtq7ygHZlP7JZwErx/qA/0jPN1la6POGURbvg7hXTWahrBwUnTchC+ZNitT0QUgJKnZAxJqiwYqC7cNMb9x/scLXtCPSoAtbyivu1RgeUIedlA6USMkJEiqmyG4e+TfsXMrxhYqeCPXxyv4dTQOyUERITkaxOGTIFLC5Q3/HTae7RW+pvfLmUaswHunyRPTwejCfdnOluQLyvZwZJPvbQv7Txt+jpk/cDX/bjcA4VoXlXcx8cC33iFTNYCdqn/2z/yuBE/Dw28AVMtIEGCVh42ACy4NmWpIAZUrP+nM8BIDh9NuCsZI3Z5Fjyw0/IERPtwL7lsU7twb/EsZC0MRGRT+JNSD7uFRsKLQM2wOkYFaBHQyvDBgkbopUBEkeu1bBDde+hK7GP+BvGhKTVma5OKQ5FRhtQFVdYBdhrxzbeImbwF7fUmuIouBQgINUC0nAW6QMH5i4fpGoCd67wWnf+TgRBW/aLj8B3CRkhImmq71uaSHhCcf07eNXSoNk6x08DhL/h7WYM34YctBmPkNghUoQc+ugzPNJr2zG0AX0skOkvMF1wDkl5n9fVQDAMMHlTAsJ7y/iSIdp0iHGkpomC/26y5h9zoxMiP2FLrEpR07BPeIzzm6l3mcb7xYpkwaaUPYZXsD7b07yVi/1AD5rcjtWloR3gsQBtiRJtXsBBlwGcsJHxmFxFd/JxxOrBVcGooBgdGg0RHGpIODrpjIlD+hs9uneAs3TZL1SIDEXVAUVGHVoNlzrQ7Xh1Bs7KmdayO9GvYFCvGJrRB9qE8YTkjhcm8osRD+TgsskAv00vbcWHd9nlOxWK+w11TAZ4ZQw+hdYFxO8KPMHs7rmzdEtNkNgdCEWXnbHgsEf8+KY7nqoS2gBJkGIyE2Piy0mSqNi82pAJHmNke3vP7Gztevq8kTQfFPqPfYpX2eAH13AMMofi6RknCFNixRjf2rTvYMNJZAkdZgdAuGAqJqMdOImw7bA/ow1aN0qiztHo5LOTST4eGUu9O31UEB4dWiMMI1ZeDhOmxWMBC9DHK+h5cmGqRdXuCKclUb7vgclxxbWjZXOEzxxC9YqxGf1o8C6iPzjD3drCeIRxmkhdQsDxEbjr8h0KcU2RpB8xmSDxAOOOnBD2uDigBGqTUHKcJLYHQtFtZzw45HEGK69YSacAdxkomGj64+Mz9bg9VxGIBO4XaCm8SwkI9zJw/h7HxKsTYGKoOFnVqtyP9B9vnV5bx3vWbN8m3ZuFCFmLNn5bRgLa4Xtvw0mxRtV8jpkxAjF9uoB9j1f6HDu+ypfX0Fd97SC05Jz/+SSfioXSIBzl1KNTa4JxDNRCqBNgA4A93PDbDQ7iDGZtlmNggCzSxLiCp2SYwOQyOv3mkKoWjM3gowQxiUkOZ4U4ODGiV1fs4YNJATK8O7v8h0JcUyQ5QvC2YHsD2NbnARwffhXHdfqT2LcE/BVWMlSF2RjDfFZO42N1Vb1iJYCt84kTFQdzkeFALbQhlz6vNuKWADiZxApuc2Ob+DYBocnoZ1m5TkXozyZs9CveZWeTTi6iEiSWMCbEiZjxFmq8xRr1GIGXlkutL7Lw4YogIW6seI4dW6Ua+itOswX0NteZGn/ZDdeBtYYux/ERj39e1VUFNZUifJidndBpmDt6MUo8ccAV5EWHAGAXZxGYJJNE3OmnKUYCaAfMI/EcFiQO+RxaA0rUNgDkAqsMsR4iT0X/0oFJqwesgjCpBnIlJQ8IYAtbEuuhb23iyb+ZuQ9lxTABubNbH9KgJ77j7mQy/uT6WbnlMUITLs7MIapYCfjXMgODqhwohlYwBpfoeLLUmof5AV7EU9AFjog249c98dMBNvnYBHzsJCVgrTwj4AcJbfoYKYPjpnlknxOBwxQ/0kENXbxJGzLoq7BTFWbxT5g9+dRQCnoKpjS0A3z2gzjF/gg/Y7oB16DlEpzW63ZXFbRMfmW6r80Xo21ODiJVrJoiJiY8JHh9XPYhtOxrioyjAxOVTTyh/DjUdXGLZHfXXoZDewLITlUmRERIoXVXy+jhR76wGqi1BbyUr4IKWKEcn8jRJ1YZVwtFT4qB4xxveEaSxyP7uDS9Psfkw1dOkHhgbA72ElCn4rQE+wUJiPfGFD8yJirg+3Uklx0CmjbVv8P40+SKQ7D0dr3DH+Qjgh4h6gFZfoiBNsyPo2wUBQcrsImB3zGuRqPovoZJQaSemr4dEwdJ8ts9TQyWAz8Dme3iMpt812IaAutF4Jxu8OWIgowJVPVYsogERQAfZJQf920knia8auQrwDj7NUs1jNFDm/kxfDpIcBLIuvRn5g3GYZT+c78y4RDA/qxxTkK0b4M4ejaYiWnRBuv4sOUIzFDbWMh86zDTNZpZY+nGIRlHMugJXkRmDQMJohtFhwNDSrC6hEkFxDlA7I62N0kJGJTERNVMRgC7sv9+jBVKzGRtznSIjAZWWfAbIIoriZuRUjQw1u09jrocUExOnlyywdOiK/dBpOMm1wCdppdIAE1CH+BLyMyTrmgacoB8bsNHssMOFzP5a7S3Okf19lUnJjxwgNOpPsy0T+bY0JwpXom4cg5z7mcG/PH9H+ryou+i45QBCjYknuSH00ZiFl3GJANWaZRqRthOrHb1gMsqZQzKiZmIgKPCXm7rTWgxJinxMYPLb/bykCMqU3mrGarWoQYgiS6v4Fvu7bj093sGUDBeNW/Dru5L3BCxTPF4UOix65ERRmxY9Wxe2eF9e/zkatloR0BQhRguuY2MbkZgTGLp46o9QKAd8sXkM2vzMmavr+Qzh/3XawITdg5zCKEIeETv4eGDLIrOU4ZQ8InEM3XlM0UnwWgjVmGiWgL91BImbcyd4e5t+CswVtzPEo/H6JZ52AmSjjP+DpMjOuStZqixHh3iSDw99634C9I5Gfcamxr0fighZ6Gt1fIcoSZMzrYo+eBzbW3ZESNIU+UzA2YTfxnvERoBbQ7mJ4EUsYvwSpwSxtpGXHijAu/DuM2uHpNLyPrOEA8n+ZBFPvh7e2BUBAMmqnFYOEsTTLUzBfCKR69xkPsIigGEjJIY7neMPfeaW5tHG5p4K7qiJJMedilWDXi0lk9GATe8RX3Y6wNYRDFwUokVD86Oxx0dwGiFBGiHfYJvaEcUYuCmxyO+s4EriK328FOZrizobVFv8kmkag5ImHV6g2gAq2Q8tixPFsYI3VCNnduaBp48Q5/yO9NkSmw3P2nHlGbJ1IXvL9kVcofjBpAG1o+t0Jhf4tXh1cB8kw9ZEmO4DFLFhPbNq0vkktu87I0B/OKyG0+QlhQhjKoZIwmMebx6rm0eZWyG558qijju5ODDB/oVyZ3XgR0OcyI4IpDQx19yGyVcBLcDT77pS28akZser/juCWzV20MnRdR/MXrsywMvPcDktk9tw+taGCUH2mBPEB3AWMGKgZNPtnMVpLMyYkg+eHEqJmuUexsvyWfW5nA7dvfcd39C52NtIwVgvl5bW+XPcWN+OvpnrRIoUATLmyNVAaEuwUF0hEo35m2vF/CLwzQdq7aV0lbZimgKhBgpXscIY2iR/TcBXX6GnvTijcEociURWMQBPe4gHCVcBfsl/9m50bnBh2y9aLXHn8FPQC5Uja1L2/ZVNxTDlTNXzJXHLptdV7/ayPZrCSVZMmJXeiMaQKKQnleT7T22MY3JYZ4RjQCFPSn5cLj0p3SMJ/3BCm4rQIs0RKfj0Ax3wvkPu2eYnLtC6xYcRDvF+9BhzPqUVk1Eryp1pvWnkNEVmQwrACZgHNDjjumW1X7IcZX1D579GI8OpdxFkB5frb66cu6K2bqwRQnnirn48CWzR6uOZVziWS3cc4kxqcFluHVxHRUBdBMSD5+gIGGPGivzArUCHU//M7utE6J4TRowPvlwQ3sbW5AVst4OHVfFSQWxf1p/4zdJRoU1QvhQmlsxCpZsM3kfjExDpNttbpLfSUpZsKXIlD6rBW+SJzZM28j5RdT6UDa+dXHL7GzsmpNPPGmW1uxltpVjy+bYDce4XISYnNToMuZjjqyMna/n3Q7Yoxjsvespxucc0ATMftktA94vmbaBdE2a7Jzyx9hBMyuSRieEPnSoiOUJ1tuoGB1YM3ofqVq75DBLJHxCWR0nM7ZTwVsqmGP+VFek13ujXq96xror9dX2xg4nn5NPOGHwOzZm/8AsrSyZ3a095l1tjO7G6hggwGBL5rrE6J6YL1yfHUryAdC8eJsFOXvFjf4Em+JdNSQOQ0ISY2uDTkEPK3/6OBzAcGRcSCErEhqD0NBMwFoS939z/VTcqANz9DyKjNFoWUJC4G/j0+fUDQ+3cNnZLAGPd6PfxvZdDXvbu2ZhadHsXLFJ6MRtJ8zaqVV7g74HnWI1wIRsAvRG3E/BlvQ56cQbDrpekPxhIRebb3Cy0R/ecjzarjJGP+22VXm23Bo6CB5jngWj+6NDAaHVfhuDQXZG+x6JbepH9PyorlJB6fhWu7+PMrJVkfhI7X6MNmwV4i/EoS+TL2jWkGkf9gnGB14keq2xtbVLSSF9lJbDjmIvAfM8uml9HT+fYNsXAzT8QnG9y/Jcfg1YobMxtVx+5DJrLi4vmVVKPLjvs0yxrN+4boUOEbkuwpiRn4zOxQ0dXi36JWNGhtqF70ytra3k2ArORi6QBFVDRaAdxZcWJyZzgdgHa/hpt/Wr9LQbxYXQZjrCEKPd7D+UpnVhiu72D0HMDDZDf+ZgqguFYzYPFxTHaIt2D3bZCLTayIiP0O7HyLAChVkHmvIbh2HH8rWDjAt7Xhg2lCMbEV6rm+CL/UxqMo53GHBVwt72Hm/AzuVt9g82fr1149ENs7y2aNZvONzEA39jmxPo+E6Leg9V3jBhW+nmMTw2kE7Y/YYNDqIgnE98DO6H0oBW8HMC99mwidfRyUcaJMkmbZSYPkSUXDcA8XjFz2boD+/DiHcYkEHTDRcbh+d2nt+6MLJRSpx9um3uGGU0jYInvalwplKr4f5pTiyHhgX/RT8dQxzrvDBqPHqQUtQ/u3QWvvHYhrly1j7dhserIXPyCSfNIq0kl48tm7UbjyV68wKa0WwKCUjiEEgNYQ2hlYMEB5fd+G0QtcY0g5kFNgEihHiM8OZpQrnKgH/ZBFEYE1Y+hcYcdhvF/kgfsRo++bimTmGaMK4iMGij8Z8AbMgECScGC5QMsQVb7IESH6k5HqOM54VxOeoyTXTB/YOGXekuiNVEIXO1vuVdgp1YbAxBvHMOy/YJHtt1hC7YIOyPJxDoA9/Z2aCks3JixSytLlJ9hx8yOHHbSRY5RkkH22E8JDJaLxJGRN3Nd7q8fzBGSoqjApoA8iv3AwH85VLgt3YlqsTp7olxyPTHTJfdPJThqWeL0nmypYRxKKvhhjRxJtqdB0oTm4TkucW+JAk36IB08hh4XYA5+zFWcxxGOSgL45r9pYcvm4OdfXPl3KYlOlE5GAVshf5E5CaudfLBmxo4dlu1mBCSjItce8CzN9cdoQtOmD5k9XlAseKLoyvHVviS2sEe0SBGZ0672+0vkrKoLY6a+rTeGNjYbHkW8Dsa52BnEsgvLmW6Ygb9gfE4IPH42JmMeCeKWaJr1nySzwQgHr15JIR+ZO15WA7uI44/4OYD7GQ8VaSThcTbHQ6PElcmIJHaeQUGR1li0cjc4WGUk4IwkTcvbvGlHZSXjy+bPTrj3rq4bdlEw8Zlt5VM1YB9wz9OR7qyn64GOFRyiJ+w3t3fo2I0SDTAkq0BjDc7sYTC9tIRFTpsWLggJBant3l+k0gHZpWSz+aFTXP81uPmxK0naHLcM5vntqxQBjAjpsZgqh6AfuC3dne3WUE7xX6iyR9JPXuz/xABf/Br38OnfItf97DEmCbyGJnaLzWovohNz5x8YFu2UuBaxstqFBlttFUVV30EL/a8ipDfxldRdQJKVkN0bQ3XfnEGhK0fMGX1DxlDoB0oC2N/YbWzc4XOpKmp+O7I+ql1c+ymYzz54VvzaBOvamXHdvtNgS7l9+8d8iCRFkuo8Mc/CxHHjjj0JuhsY7zykf3PJ0KW1IDTZyWaOORJQ/o4dvMxfmv2FeyHLSx7jD1BIObJ205YuQjO2ig415OBJ93QD9mEMRJQhx28Xy2wNUuAnYA/+OV9GjSjt4dozqC/sTTGe5d6L7K2xOvB+OQzqNpNQ2fhkoxHUyAPrVZXVRKRMKLEjgMp2HeHCXKEE4vd0ROaC97+500DtjBo+KSlwy7biI0cFrr95Fo2UHe298zlhy5Tmc70aAK54cmn+Fc5geU1+9ju5Uc3OLFz2/Lm+kG66Mr8zzPMB7kQ+YyWzmb34gkt539EG2HLjhE0htQ69YogG/pYx9NtbJsMw/blR2lf0fGFBw1i1MLONVMwa8gA7GO1ENwn7EVGhZMAnfTw4/CSjA8Zdl/Sypj8Bl9pkZ1aCANcvcUQuzO3RVQjJ/jp8pg428ontkeOUxcKwiwKlNGvpiQrSiDj0sphQtyLlzG/haQ1Ma7ERgLwaMPEq8diDOjz+CwamiO6/aSCQhEqnpraPHOFYt83q8dXzYlbj/OBorF+YpVvcl96ZKM6gXVBHJMhDI/tHVzamA/ituWwTRN5jc9oCqTACgcnPsPsYGmjoMWdmT1K0Btn7YMf66ePmaUVyzh+C+0nNYlBdULYk/ViYMzwSoUm7eqBkkMhAJjBShvfx2LMI9AaEDZt+EkITqC+Ga7s6xYIR7YewCZO9GaCd6i9MyHCwoTkU7MXQ8v2yM8E5aTDH/YT/7QBbdE+mxmxewxSexbds+qx2vyX/hTnByvGEPtIpnpiFhEWc7KHjm4/oaCPMwLeB7ZPiQcT2+qptaB9AvzmzcoN62afVgw7G/b+z2i4ACQObHCFFcPOrn3l/BRoezXgMtbOzq7Z5ZOIyiBpGSoANvksnU6AxP6o5KNEfXz0gbgXF5f48idWocduPG5O3ILX6dgO62l7DNEZq1cDQsbPD/QdgwqVIMBCX+DnGba3aCVyGKsfmHQbnh5EcoA/m9in9FI5RtjE5TycUExri8TTF9NsKx+HwNU4/3OCctbwq8ND9+JsEAuSUQMyA21XA/sQE1j7cpvV5r85QxqKJ1r45LcmgEiOvIgVuDro8hMG1AqPL6udXOf7OkDchehf/PAanrZapAyBLzYe0MEzChRAKQ7ss21KCvhtnd4DUmzl7OVgE4+dVKpvB+k1WAFWVjAEN925J5LTIWLCOnHbcd5PLAcebNPHmHChNlanFzxpU9/yparOfcjoDAYJaHtzx+6/KWcprs+ymwOS+Q7eiUd+pr9MtA/wgauo/iSjCNljsuVRMjO6p2J3gcugcthQEahiCTU2LochQYwZl0DLLczhdS/FHwVj2NTBdmrGAMW3xYEA+zirw0TJdfxxsoeOIYwKBiEpNVUcVk6umKXlJU4wGmgzVntXLm3zAwf8lA8R8R6xFnwM9Kcch5XiycWdcWLyyu1Lb49rfYAd2IPtbbwHLWcYGGu4ANi3l4nswxldr8HK+M3G6eSmhjpFpwfo3106OdnapOOio7keUUC1+Ngs9ckmTdrYj6UxwgA93iqAHR4jFL/8/DfGufTzvPsN+xbH1OYVOokj4/AdYh6erc25rHwQSLGzZ4ZurN7CYgwtWRBxQOyUICgBYeN9W2lLj03oYx5E0mGbiT1JOHYQ8Z+aQcDxB9FUgQcO+cM16O7JZVakYUQII26KZ4B24YkqfIN+hw4KNAvHBH6xdIsmFn5iC009wG1w+ucu+eTgY6gGk2GSA6ws5Z6MTMBVMxXgoMY+wr7ChEUWLWPOkPhkk8nlMq0QMVbwa60CkWEElQHS7gAkVxC/ZkCc6GOsijcxaVN02dgbaLbLMa1pm4A2kegI/CZv0PU2AnbiJ5tXKPGQXZ6taRveXjAWfYrwi7GBBIQTWhvH/Pfw6BeLbvCBkqosLvjvO8+AcigBJyM2qhEAKbip30Mm7GXqbB44jtZj249rEsYqSh6p1uM99tdlmGRCsYYSseEX7cDLMe1LVO2rOOaKLnNJi2fCPk0klx++ZE4+6RQfgFhVAmjb9tkr9no+7bdjNx9nuiCJoRhUO1pOdNSnK7QSw8slHZXHSQsYC5gAcUljb8+upIaDO4MOmyWUVBEDYr3C98aor46v8E9sAzxGoJhRFhLGE8aWTIA97b6aQP+ijXwVgE4UduVKAIi9cN0QIzZRajt8Y2ysri2ZZToGoYa+9fL4TMKxBIkfsvZnsvc4ESzqdwcX/JYwuBpK2kQSigOPdWKuup8Lt3H1jXVB3GcWljhz8hHt5Ul3ZNuuA4lIvK2dgVIKpkZXlPZgQpAt34EKpAM9DDo8wIBP9C/UrDlnVCNDCkD8UKSh4GYDkbLtoIOA4l+mA8G2o9WQPLJahXCGPkUp1JzmnUCKPEypjVcubPJltbVbTgRn7RqF0CyKzKpWABkj6FtMMjhR6elbjAs8ymofPrH3pYp6/eEwesXhDzeU7RNadiJZoeSDN6MvLdPJShwP+t5+MHCSKTTGyDirECc9NtMwh2MQVxx2d/kk0J589UP2bQ5B15BcLUx5nHsJ77RDv8oxyFQCFSAxmLQrYY6fkg3uT+GRe+agDZHvErRYCHUfOEJZB+PDtWPJJlK0A/FAR+wFoRFR91M6vIc4Riefy0g+kQrsLzWTT9lNltOIalTQQKTAk6Sixfakieg8O2iogP/2g/ksQgWUcS2ddcD0cEY0MqQYbCdASck5I3YqYSmyezmRYnXKbQgHTwzdhECmpODBvWrRlO0DLGKi5tXClR2zc3HHLNLBjBdVrkWv5a+6LDInBOpUbN/SRv2JG832EKC+Rf9SJ4Jvu9+ekGCT/XG1kw7Dhkorrl2+HCUxIC4AEwsmSh4rGPNoAxjuD+QxCfm97D7GQ9quDIDUYy+jijL6lRaT9ooDbYh9zGVnNtfw7805uYY4w+9v7lNciYAd6luqC5CoIIbLoXgaEfsDtwKCMaJ8x63ScWheGF+sNSDlhJqAJFOcaOHhB/StbkMedgyVMD75uDMmAUp8qYq2kJqi6KjIKKNLpSKEZve6jXsIHZr0mu/kgtWGM88O5IpOGOU9lzIgG+7qxsCBvCv6UuQwrAmc3TyTEdqtCDLspL1Iq57dc1tmYZkGNP07oIN0Hd8locmyiqL5lt8CWC0cO7ZbYnvoh5RWOxgT8QI6xUK4fQf/eOAAq4I4Fj4mlPGBb/fBCq2OlpeXSWbk04QJsM/gKGqJVGt9BERqUOC4nZ7EPcpMYjMF2yW5pmjGMcfn+jfud7HJZPqT8AniO4cCOULGqEPIyVvzVCr4eUTH5IwgTsuO/AkfG26IU2kuyQdLdVwBQbnL2CiPFqNUCsKWHB5gU5CqVx02wWKBrK6EOzGV1UgZdfkMSDYUVzVXrJqrMIt2SyARTPMYorhyvnP2Cp91sSbR1m85UT/7yrro8FvAyEOlHw2zs3klbWeAJwZKIpt42s2SotEVw+pCFm1fXV2mlY+b6CdDPEZGpFoPqKAGpTSokqlEMlVNUQg7QbfTGHWBnNmesC1KQVlYbmit3/YASZp2fIQ++dIubZZnrU9IPulvrzevq05pCWG0WlEBU9iAsQdPWbzA6bDvRQJZXQn7NC8vSIl1+RQsFshmbNZsFXgZK+6zAhIZpGzyWaEz7n2aNHcubXHXrN6wZpZoMswi66LDbwasVWq47KJppi0KurOYHEBWlCFcxsETmPqx7vjIHcQHXTQfl+HkwYS5Q8WYBKSh5IYiFLQBi5yZVIqQJYaAiEysAXK63Y4FHQEQ8q3sQDF4wSSrWRSTDxWPH1/lJ/bkXieIh5N8RlicuelVAwNTSj2trYtUuAVWUYMZmpsOkkA3aygk+lpWNgWLBbIFe0DJZoaekkrKIdL9Ywl4ugzAcPUTZ+6gKuj3IpCOg8m4G2k+hNKdxUwKsuYMarvoLyQePPCQ7TvGoCtAn+NpJ1x2GzldEOCnoJMjV8JSH01oM0WdEqO0n2N6Tr9HxqPKnB1ivrCv5ZQ8z+2EagLcDFVllYrHjq24N0wg+RCBhFtPCWQBB7KxD01gYopYRLZJqBooM0vHjdbIi9S5mqUlFTlEwEAHyhbqsUhQ0QiJvhaSsxAR7g8vG1SiWgGRkFRDvZQSw+slYu6SGw1UOX70pBlMhGIkQEIoIlSnktgOd8+AUGEcnK6YmGomBaYT9FneLvqLj308GZk4dRqartqOlU8flBJvBST+CQXxUnta6NKJw83F0Os41i3q5SMTap5bQClmgTpeAGvb/psnQjeuoo9VfoiCCvhj/49PPjpkXyZLtlHlbS4oGqt7ylHrGkBDwrH0gYGtiIpQlpWVHSSHkt0YvpCiLjtUAhmNmKjqRZ0KVW9DQUMISDwYqhUEumIsMZgglVQ1uCy5HRRGY+pEWoNMJrBdMowuxEMD+3jQwNEGOKVI15sjZX6Sjx1oSCfpzcLHRP8SZEgxxHfishNdakO4ZUz0n9fjFtkiQWoh1aInNI9Y2UHOMUIfBeFZwAnFbTIOuOyq8kk0x2VMWvkIrCH9lNshYei5CHmGUDW3bxDHWhGIzJMHNlttoyAo5KyNhGglizpZ4oCsPMMqinoiB8QMVxeyZg1IOVn5IsER3Uct+cgjoBa6XIbyoNChm1fsAqtO1C0hmNwrtq1v/FwDl+xRz08cyRZ+KCrpycoTD3rI1CFbiEFXtDNSAyvBoO9QkZ0fGgEppC0uIDE57Cm9zRtl+3nqeOh97zZ1bKbjeyDge0t4jN9GYXVmSj6HCumroEGaODDyVAci1A/6oiaDOfSHN0tqg5XcplAgD0iY4aBNUGQMrIDtCbYQ89WQssNDMwGqxzoMO5YIKTcrDwTEUIpr9Ef2G8Z3sg9ZyBWZGQuEEIlUqq3L6BDREKu8SWEOGOwqgzXbTgG7iPsp2rEg8YayJYUgon1Zpt/JAUTP6g6lBAWWkHGWHqBgZn4oBCTIsKohcQe7zRfl3yHB9Zlym8FU7zAeb3mE54aqolxjDMn3yURidPLhhtIf2RjyOQ+wA1scEBKlFlIVFDPLZ4hQCKHylhcpIyMvpKqZSKA5aAsGhZywHFHbFRkZVnr8BPuWIHVFihTBGbhSC+QFASOU4poi4UO+7Oih+AEyNBHNiXdjhIGsv07dErRNa2oo6WIWjoeDHIkHb9+QHc2qNV3IMX8h+zQr69siIawliFgindUoMuaJhoMJ/uX4sJuUpiHt7RBF68IoCqRgXy2HMwLm8ZALHljRGL/yiRo1t7iTDhOCJYa1AioCw8Rr/wlEJVANKh1Q8lJUpDKUUC62LBRb1GVLQERMOrFd7DPZBN6OMhTXE0U+4rC5qtuKCJi2IjpcC/gWwSW3DF/gbbjPimg/OoyIr0Q0S+xD3mZECZkpNJ/6kL897x5z5X3aoc+X3GiGwHep7DiyaqFq2RDvOcfO60aoMueBXARqfAGOnZPUEH4oF9ZmhbavtyyKDAs5bOONkeh6zjSIujKD8cNvblDE8cnHAfFiXpgcpvSkbIyBoFnYqsgIocoP9rh/MRIVIaSieThZmXtl64ITth/5+AI4eWyqWATHxJPFIIX9JBug7UDeVzRQjxUZxGAli5xqgEBgaLEiqUoIfi1jhc8siNTFxqFhSNhZkYZuDtpeqpqhpkIhHF800Td4/ROjpRsAyQdTRO6+rlivgNgdUnZsNYX6kfrsioJFcpJC01uKPHUKyj4G+HOyjDDPy9hQdptAxP0W6Y6FxGHtKE+xYWLxd8XWlt2bMhanJh8bOlzZ0rBNg9UebScS5ioFZckRMwcRaYgBLEZ//OZooxDodlhwIiJZk/YyvNPzt4a9DNccgkoEUfQgYdcAqMlWRCBgU04g3zAgBxE3ScEnG8SHTUBysewoVOIRVtV8lRmibq/ALSsE8GKqf3jVUzAXkRXsO95C1DU8SKRDyqJbsAyJKjXVNs5DGpurC+KWB2Bh/JHtGsC59cnGbQIdnWwBQKg2cnZo81fwG0E0DhEjX06nySqJqYaLm8PPFNunYKz50W1wbkc5ByIFX1UB5JrkKZ0OA7FOnRJYffjTBon1SAYy1OZ4H1RtVJgpi5JGT0AaTj5KNwMa9rAPca9hZWmZbXCiqSGypw/COnygCRohDugUbIsVJLoDsdDi6Ae8ig2/M8P7MNMv7lVbAcTG2tqKvWeUCSJrLhU7FFTdENO+P6wMz20OLAUtOvqAmA3JE5/sPo2hKyotRGZDyyP6gyBh2e7QxqwTX2WWrzF38mU3AB1iF+UtwBVPIeqfhNcBEaZNFYczXxVAnHhEdiiUISJerEOnBG/L22gbEvmaJMu4DaCzB7u5esxPUGXGLLevKvJZkLzTdIQITXsQoDZhZNMAq84PVlTBErrPqSJ9qXZqVwW1rba9gkRbsQ7qPLxElC+7uX7Eh9+UfRT9RvL8pm4S0vtRywTIElsYpyTSVQ3e73mJRH9M4mGQJux3jq3AVzeCvaM2haiNUuvy1SU0Dkl3KAKXXPgoc/fZ6vjk4w3QBptiaIBMO/qflbcFt/XAyQZqxf1BfrChrLahkELLeZGE0AetNrRVU/NIRDPwMlxzzScFbEDMzyIjoPXCzf0TQgMsRn/sZnWzYEFbzMP55RK1kWa+hriCaA2wP3NRseBYolmRTFFQGG+rYWgWOH3pg+CwIRLvLxSJ6EQHEEGuuLGM27IoMkq6muIjyiKvX0C0r7Vuop8QanAWKjpOItjakD2itwpa47mFWLXlLrNphIuxoRLLeXDswqWTG1fqB+lC3S+3kn8K4isgNkCyfFBgs1XbLtkisAx2iit7JIQQCashH0PEEzXfwQHVQ3NLoposLNt8l3BUe4VfRCRU1pO9l3Jy8HaGP27LoMmSfw5UwBIdK5/swWaVHIIKQ1O4HNugKkjMs5RxSM1NsFXRGGFIrPhNVxj2KTc5dESG2YVjSpi4Lq/2SgJWzbBBki1EnppDvyRAknofU2BF3VGG68LCrZkbulc6W2+9oL1A7av5aaKiPLTDnuzJ1oNAzjUpTEok4QccgXm2Mv2ymzLCH7ltBDg+bK7O9mWLIOZ5i3eKMHrgjXCtCi2aiHtGwmF4NtcIQSXkK7LqAqKinfigupYpwgmKvGwpypwYgR0u+FoeBbaQLSsSoCr2KRJPOIgdvPhgAZDaQBnAdNd/qORkuuD0BVG1ExWtToMilohGBPQfHm/FY9YHSOSOXjyuSEA2viGM5OOURMVvoDseIFVFUpBprQytX5eMQdK9CoFcpgMYEsEgHNba4BMn988O4pKvGtI4gCmWRgU/BUETW85EcMqLRUkXy/QFWatPbJioJiYQm8SnkJP1Z7Q5poJm8yaFCgJ5EHJgZiqlKQOVoAhZPkGaPyScIen0gGWdfFlHuAUJxUolqQQHipIgwxZSUSti2qes1EAI+LYgJE+uwPbL/vDI8VhA3350+wxR1mJOh8GyBUKWsUDJx9rOHFIMsRn6t9/v4QeKqJboOlmvy7UIrEicyo3/qn4TpMU71VVb6JKjY43/OtPYmF7GkGaGfwFaBrKYpJQCZkqmSgOigrqKdRaOI9EZNFGSbfw9n1qDGhDVrLpERNByOXk7IRM1xyQIOWAnhBCaXRCxYAH6E3WE1LDxD5y5tngQQ8toSNOtCg4AuxNjuRp8SFVAIBQSClPpj/j1NA/FLEEpSVGR8igI+Kd7PN8W7L+sSh4srDXQt33aotpqdh1sxRZzqLCFVRGxqDD3+VdHBwFtj6m+ECJ9xNpC+iJR0YNYlkUZQSFZct5HGU5z7A7pkJVxxZBCHF7QRvyPBWYBnMo2DYGmbkNuS5AlRhhitE8Typai6EbJT7rsxoZ7YiXUQxxQl7McTjp4P1BGUEgBOUucCJmFImMBhfoE/bK/vecrohZqMddvgB38aCNXqhAR3ugP61Qh0kNpoBBcxddj5BqgofRlayIj6Ek446aO4f3t/yXibbBCqGXt4OmvfbY/7IEBLGMFZ0CHgQxbtBqanbDfKi/azDhhaeoSm3ysAOvSH+4TAdh683BCWpbANmxxApR2y0gQC0Gp6W2AjC4HVSw2sQOhjxrSiHogWnoLMCXoLIoeQjiRMC9EOpgP6YMvwpLc5Hs+8BGH4/wHWwttOcstTQZF/SxxAqKjjkto/BJ13fJi8DPOC1TfvbJrNh+5bHbdL27K3uCi2gDYspuzP7jJQuQZQaUGK1QUr9ogZjDjZAAR+9EHFhaNdGLEpwzg3hVKAjY4WJWSrQ2Ad05C/M/J5AS74a1wrYicCLWZn/zVA2QWkI09WvTgfk80I1g4/2EoVKIOsPd7FqlvHG8QqABCTrBLvhfK2Ai72I+8oWxJHnbFYjkBDxXp/0yXlSA+9NZGv6RGTmtCyBWIh8iTcqLHqfQzoKSVNgRTsDod4FqnibMbm5SxFmjeHfJW20AokZP3IXJ2JAn+7yTxwcWhIZZDf4kkl5X5w5V7gAzMIANcUroSgwaqkmz2KMng6FxcWzILq7QReffSrtk5t2nDJN7SiRWzcsM67RyxZQ3yztP23ae1PJA1cjSA770RwsPIlgMdiCmChICSSLMpL0N9ouSdqOe3Rg3bd/JeVOmU1GF3eWnBrKwsJj76RmpFqKbPAcuHq1QVLPpiysOrat8texl+sO9dEcljd3fPbG3t8phlsogFNoYK/OOeGH5vf2Vlmb+NLvQBYY31ieT7IbBtkct9DD92LUKxjKECAkkYiVSz7h0RonqcBygGPoBVlX6gkVXXzpRAIQYhtyPpkwH8/BOhRx9avluogp/q4IeDqIrTOHxigw8p818oRkud0cnnzOVNs0TOkHzs0CxhlFmGaEhI+GuDJ7iBmtilakQZCafdY4dCwIG8c27L7F7e8fXV0+vM3j6zaXcM0fZ39s3yyRWzfMOa+s0Z5aHgrBTD3uYuJbwdsrlqFvF22MBUSStC1b2jRIygmio1MXZyhjwSDxJQrtvy6HBSFckw3QFVA2t1uI6RqDiCP6hzyPjJJR0BHhbY3N4127v7XE71hYDPwTGOvbW1ZbOE315xOyAOy04xkcHE/oCkXSTrT/wcVATus4y2hMVgM4pXu64Zq+4QhZyNRFULRczeBjVQizZwobJtZwsDSLfYYxvHiSVgPHLJ8QfbA8HrUmX0ZTfrT86WcwCnzI0h0vKFQJ0LOU5vzhcsqAhRRRkBa8v667RDbV5YorPJi9tml5KAWbY9if2I1Q6veKg3D5B46IDnxHMjJR5uj2xhUUOTY5EDSmR7F7fMEk0Kuxe2PRM7G/+aUAZVkSA1xXRQVIugUgfr0h9u+lhwt6oz9aINYRYFLIoig/5QUhsFb8ekrQu0zFzQMqScSTHY74qvAT5yh38q1UMUZHOzAQFtRd/bZAXewNUa8pcxEKsQEelP2NVbzZBwyhIh7HSorSst6yxrTEgRuYwu4VAoEO/SHw8xazcZK25zV3lkO3w4L+5DIsGf0cknH7SYZLNViBQO7FzC8ciYE1KG1Qm3K0g557IINJgWGzuUAHYu4d12dnAjGdAykCzajZPy3oFZOq4SjzjCR+RTSPEWAyuexfVVvryHSyHcZ1nJCM6g2LUaYU2gqZ6TEPLQYtzchryG1uXJEt2IenFZLdINFMUGRlGEgShsEkSbEBuPV9Fghi1ORkuf+OJmEFVKqqiBPuRHrGmzkzAAYXRqaC3GEo1nvkznRKrSZTMDnHv+IHnbqxplD2WOhdiyR+Pwb0DBgiKJhJYK4ysgYzZEbNVCbBcv9c2A1GPqZPSE34RtkZ6PpCTxyBaA1EZfdnvs8qZZWVqKdlBqIrbaNVkCiV4B3eaU4LQQzMLKotmjFcfOeTxEYFu+evM6P2Cw/egVrqNDcJliEdfLb1m3lywyv5evbbfCgS4mkJ2zV8zyqVUeOTtnt8zKrcc8PwsynNpuUHQlFQ1QZDf0BDkxTPMYiku0ulxZXuZyiBmMMyyjbqXYowSlmS/aocGEmp02wqYHlS5g5bKzu2c2N3cpJgkKWz0u9Dnu9eCV9yOnhSryXn1nBWi5HWylVq3qYCCWaLUoseiO9QDaSMAmRpEXwd9CKAMSYo6l6U97l4hAOZBck0aB9GHCxoKKNYjxQucsA6TM94RcGeKuiNLo5PPopU2zSpMuo6CZJ1fcRKyugIq+pxgriy3QmeDeBh4koMTjsHJ6je/lbJ+5wkkJycmfqlNPL1GiWFqnCdQlH227MxwviFej7J7dNMuU7GBvjxLgyi355JPuybo35moRV8aHtl23QqgJkKHSCLM+BiZWFivLS2aZTm6GYdn0blEQq5/0xD2Yg9KnYjOapkAKr5LoTjBGQMLZ2t41m1u74a+QwpyrasuWROmfhvDaGn5rZVj5zATnL16NpKdkDg2foZUcQmog72KpIWHrmTrn0LMtE+ebgFbz8PoDMyemEYSMQi4GRpHhoDwVnLZiEUjb7PigiihSfWg3KvikP0J3PHlACphw2Q0Ziwrs3EKqEbkNpTBJnzDo2X8eU4xFwOWH/c09m3jQZ1RfuWmNkwvu6yxS0llA3tnbIx4VcLmCRiDk9+nAR11f6OgKh4Qw8co/uzynT9qDSIQHe/t2VWVF7cY6THIQTgVKRPQVyZelnqAgwCSMOdkiProRqxxskBYTVoxHl6JYahWRmK0O/1LYCOxWghi1+rp/shDRokAILe5VAt2A0wXRwMaLbhonQQuVOSna3rZ9gcSDIYzLbnOB8mdR3iNMzDJCiFgqOlBlzyatGNOs2IBzKF7spmsWWb/AIOKRk9MWsQUyGRsWmiERxJuDKs4bQcJNXNJfblTIGH/ZjVY+K5gEnVasHNTHWHYGWyoSflGuwCgM+yIQDib7XUok/Fg1DkxazeBeDp48W1yixLO6yMlpn1Yki+tLLHuwu0fbgVk+tcJJCuUcstQckWi7566Y5Zuw2jkw+5d2aHLZJ/trlp03n0cky1X6Y8nC9MMlQihVRUZI28vacMTV1SVevve3K9yzQdzCgD1XDNDto4xmnKN8WOEJKh6YBHC/58rm9hAbPoOOUSAeWPv0D6tO/H5PMCVE9ich8e0IM9tODVSa2QTr0h8vqydLj8FSMOECwhJ6xmlp1ZdQIxvsK7KXMW+Rd+Hh41YGcjRGZAtyLMJ/aFIEn8ZL0Be+bIV97PRpWSjgxHpk8uFHrXGGT1pZxRHWQs+jwhiQURtII206cdHiS26XtvkJNzzphjr6bW9rj+/3LN+4br/ns0xyl3fsColOO7EaWj5t7wn5tTghG40i7u/sUaLB49p0PkqdwyzsOfq/fBPZw0qK7O0+smGWbzlu4+mE7msuDn8cQluoxQm7LO0QijsbA6Qc6DLRPnyyRKfeq3iMvILIBSGkiO1UTkExY7lsuxS8fKAoFaUdGy5i7GmRQkYRXxDd2dnn5GNXNQ65hpE+eyce+v/Y6gonf4wx4U9BrBZMTEB2Uq9ADLJaOSiwNVd76W5KYEQs5LXTdtFWUUHiiVWKCOJwvqiejaTXqJPjj9i2IHageCJnRVARQiH5uP3sefTpL7tNSz5beMAr/I7PCAtlb6PCCMRDzWl2slqun/B4M19GQx2CSL54nBr3fmh1s7exY1c91CkQweoIKyGsiIrIsJDkFldogK4uU40EqLN4X1ERyU90di8gQS3yyqoGFld+hroQXQMTWL5SDZBoFQRL+rlBgGG4TGfeq7TpIRlISoUDyFgHqdQkQnnsDaioM6yNkiGn3eFnhpRTtY/kgy+WbtJqfVGfnOjQch1B/GO06sEDH3JZtxct6XBy05VQN+Q4QIAZqRfPKiBw6z7niaBdgHMS+xrEatFaiG4oSVQQ0HeOIfzudikFKQrC/eM+BYonclYEFSHY5AO6EifQXKYILENtsLQJyefsxnDjPQk0Ay/SlO03lkp26GqQeI8GegYPEyDR7DyGp9pA4B7kBLRCqxE8BIDVygG/zo0m0Bso8RxfYZ0sKo736Wx1b3OPV00eOXmi7Z7ZoNiWzQJNFPLNYrw2H3Hxx4k0MbX3tBXIifF4iVGwV3RTCQDDcJXaw5NfLBdUqRIHo/mK58mRuR4k7WUblZTBjGwvMQa9CcEIOlSxat68smO2d/c4EQV+lb6OFK3Cr5Yi+YDeE2GPjIAnG6dgR2oeU3qvrBNC9IryTYEU3C4HVnc2yiZSTtweQfoz4KGu1Er6NcRR6HYkBhUPcgObToY9BuogbkvaNutz8rHy2kIfoCdbAYFIQ7YHmIt4Q9mSCFIbKDWwJP0ROwkwaauNZXDmuLdPKw0kXKL43kePLpjd89s28YBGpKWTq2ahlHggw7opcPDztMbf43FCFXn4WqTEBwEkHoD/Qpd8H9CZb//Zq3jnCHgDbAuHDRA+t9cJelq0DaCadHwN1Ld2oDo5fMimKxIM4PkhPLnAr0G3l8E2hv5JkPEhpGHTtYnoUEX/8WVZXoJL4qG/FD+Od2mbbh9kMFTweDY2q5MCdL31gn2Rwli9QcNq9ehrjXgrQgvojumAt69s5E0Mve513BaizMmhTypF7EXXW1uM4EQxJ6AQTwGjk09p/0hwbD+o9CAUZFX6I9sA5ritDi9Jf9hGRYXvndAKZue8fVPB3ra9p4ODeYcSz/4mLrlRxQdDn/iyJ51diqOlkyucfPhtjhqOH0OmMzsxOVBxgdQxd+SUxBQ2vLuLy5oowE0ntaNiNpDx7g8Rpcp8r+8LQdGXNY1L3PmuSlBFD9CwsV/0MdrOgsKhLQ5MWA5K0pJ9YRpYnWLnjf/VIe5kG5BSRqPThBVboGGM+2foS/rj+l+6DbBywybAilOjJNcL9jlJeYSSEuvR0P0wxk0VysZgHyW7YZ+IKy86sAkJdzbMyUwMHJoDqJILOZAJMegPSuNXPioK8e/NBZUWEm1bC0kEIQTELALJPhVOPLh0tn3uCj/VhifZkIT2r+yY3YuUeK7YRMQrCWr70uk1TjJ8XJMePiXx4DFo7xOfUnawpMJ0RiSbUGi44nElBzGT0eBT1tz+tpNmqie+LdcC+rIJEt2gYpHIeDhqxJCq4/oNsCfqtB+IYFtOBQ6K/kSBcb/bot8CJIRx4DM5bAWIT72FKHNGoWFCexExdBe/DBSbI+IDfarlGKoiq56sXAO8m9QmyHehlogRjssqVJCq2IeMwmgbAq809EA22Wj4LiBuZZyFsEraZqKpmTMAJyF46ARbfELCYD/5/RhTMR7joCzNYnTykTZ6kwmhhlBY13g/WLJDQkggEl4yqHSAOhcPBeDLongkmldAuOxG//A2g/2NXX6SjU8l6f/SDav8dNviMfvINe7tMO3EypB4Iv8DKWIImOx49ImVzz65LEhbCLMyeNGGASQXyYIbSfjNIyJkZTwcJ/zIbimo3TQqcbnTBhZGh9B5c/VJKDt3cE4yENWqOqMtoZG1mxAGZOU1iLFH49kN17xcxECy5YUyLjUX2q8he0Y2MReZDYWqSDTLGCEag8NQumLKk5pxCiTBpA3sC42kOgYz2PGWRZVZh24yvt914viqWV9f5u+I7e7ucxm0ZTy1W/Djk4nnSSEUzg2t8SsfAYxlDAoGlghaSlgjBJU6tG6gkhDK8KLU2fvUyTtIPHTA8p6gZLRIHY4yT4TYIEw9vIwkg3syO/ssjzISD777g0t2w+UiCxRtdSgFYLLliQQ/zLJOPjZ2qJaBCHog6BR2QJAgD3JsTGZpvtfnSHoL4Iiajy2F4kZCiqPJWXC70dcOXo/+5AbtKIixIpwTJSMqslXBnWqLLcR2RZXVheig5SJWFtjneFwaE0cxHjLi7Smni5R9+JJnB7y+21ooy/VacFJ9ogHQIt6cK/dhTXmm3fBRg9Wzq5ppcJ6nGwjhzE2BNFuAxLO+TifQVMYrmeSY29iw3xVbW6WTbSSgCMFKlQ2iPlhGTUmEILHxyUdOqzLQZFl82r/DxkgIZRRFi4wQWsyL4iyP2rF71iYe3hm0A1Zvst/b4WTkADk8No2nx4SOVuG1N/y2AdCcOD7sNvxLwBOdSFn5ADQJ4PJbwFCCXCR9e5aqh5DA8bQ8NpAGcgLPpz/2Xw3gOglXFIpsvbCyuOwz1EYbyaFqwzFdh7ia37rRUNJsEeGx5jaGY2o5bONhxzQSEC6hBYB9bK46wEaC71flcs/0WEoQi22rXrIhngnbQ/T50zZ12CoQl3obj9ktJCAzkkjnCfvFYlvGE5InaK47zise95074uFnNuwYwZ+OIFhWyeh+d5+jk0/sUsKw9KE20BQiolS1nKZpuodi5GSEluMxaO8hMe6ex9sIqIyOoA5fuXHd4JdJ8XAB39/B3qBPnJHvb+2ZPZwF8M/oOKvOgftwm/zLgOzZiX2QDyBE8rG3uUNx7NlVlr+aZ/XZDi2JcU/KWhOIAYJ74IBriqwhZGvS12gbPgZovmW6UBSlA05YdFif2ofEI/d8+o0VULTBPchtHRO7yCRyGUUtK2wML70JWIb+aNlJcAZgG79aiuTj7TWMwz805UvjkJ0a0xSdHAI7HQZFPhEVQtzxBRTtjMZ8rAj4JgCZ483REkgbO9uqgRUN5sAdfMGdNtzruUJzIN4LiPIenWRv79rVEF44WwK7DWLgPxaqqDH+S6aXaNJ2ZQEmcyBrKCL2O1OSrtivm4PVxgpn7xIeJrBvLUDH4KWd/DYCEuFEw49WE8t9x4cvqxETDxsYnA0gOTGlBZKIhHTV75NAhpId7fz9bfxSKlXhi/1hsxq8MsLLH2nllT1jJdriDZRMA7tJlaAoMZMNW6JnpQb6UNHD8MONzbWV3JusgU6nGTE1DTfRI8ldrQRzOpndQZBIHLc/rDycvjaD1c6V7R3+gil+7DEHyGsO+pu/37O+4igOkXpijQyx74ybkCQRZgRL0I1SKJC96REeyhAjcIYtMhreR9UoRjcZw+GXBDFXHFtfNtvb9jthq6vL/Ou3fBJDGy634TwWtye2KTFhnCAx5cA5gGOTAKUB1GuO5EOnAsqjk89jnHzspaoinEV2Tebt9WWrYcNpIwjLKfXqCvQhL9Zwhr1zdtMcbNtr44vHl/k+D16jg18IXTxJByISDlY81On7+JE4EkQSWjy1ahZOEF9dlsuD+PZ/AtC4DVTQbZEetTr2Lx4+wCmHtMOC5LA36T/fG6JBgkt/1i59IvRjK/bekVbziIhStQYGrg3HQqtIWfHhB2fcruY+tAEN4eM/4j2gQb8SvdlAPi3CWhiaMOsaZbAk/eFPZ7gUOaNgWuvYvaGhuAX9GrxKpCtVtk5/LtPqHGevmEg0ICcUzcEkgwkHkwomm0LOGuAc+jCUPIq2qoLkIo1KJ9cyr1UFQkpYkU1tO2OGaTSPtqGVk7htSUR4wj106JYRomqMBjvBcZorNjd3+BIbLq1t00nvFr5qQoZwH2iFTspxHxFvSV9fI1m86YWAlmtf9oujAYU2zGr06cj8IWVyMOH1OpiMBc6SwJkaDEodIUSyFVgzoa0+baUTNGvQxmW0vcvufW1uyYkJkC+10cSNV+YsnaKDcffA7J6htlLSAQ8vEV3CpbkiSN86dv/JcClolgvhSRI3feRuAgeqIurEeFci+dDqztb5gyAFUcCfhm2BEEU8qmMCwwJyHa8EIofeRnbGIX7kBHW8z40nTOZZgVhOIw5lQEWpAPhpTroObB3yXMtDRxA1fUJ0Fjm9OAbIYBLBp+cpIZ4bCNoWxgv6HhNPcxqI2WwbfwZGEJPb/8x1zgN+jMS9TO2KUTXQxjj/hEQhjGnGcDqhvMzDIYWuzeAn6zHP4Ok2PHhg3zBC0x6/IWORP3F/Gy9SxoMpNvm49pMhGTZYIck+x1+Q8cmX0x2Rufh0zPGv17m8ZY0QYsX0jM/HQ46G9JPzmNEMaGgo6zsifyiBQForOkCXyWQItnZppcNvqxYQDUlm5fS6WTpGZwCPXjH7dAYAOjoWb5Xmx1GRpBxQ4pgY6cQaY5AdQgx0Yn1RwKdXSMXqsAqBDio6GMD58K6UQqDrwH1IKy4kntMn13jlEkIHTaWcEQcMbAxDDqkiZ8FShFiwqXiVIPEB7THRDW02Ag5mLJKx+gzExDcRS2Es8f1BcK3mUCKUlAAWCqTL8kokgJKPLBG0sZLhMcDsOBbo1QxK7fGIBZoKFYxre1OaBOKewPEn4OMQCYPmuSt0MiOraMx9eBIOj/J7L2BRkSWGPx6WZGmaLeVJyQewSqFq0RAH6DwTYrmcrRhe24klia6uzhAR3MvBfRW8zcCvLnh1s8RfFsXKCDf8eSlJ7KXTx/iSHC7DDfCBjAKLa52aPvlO5JsgYScfqMU2ht1hkdOpYG9/j5+SuvnUulnGfTTqP3y3UcPvI/6IHTqgf3EPCxDnBVELHWHJvpapGjsUsHcdwiEDQxg3hndpH/hppNEFOOpBXsK9zkjA12pt4ISFTU1lJfnAvBXiv/xnYNqSGNHGwvimYawN53+S63nEe3iIo+OfTnfzIC6xYfVjx5R9dB/fhdyn/tjEnMj9QkwxQlWmcV2I+MSJ0CAn0ywg5Rkvu4VDJEHAHLwP5A7XXqSsxZYzpmIS6njg4IBWNHgnGwM9QZ3MnziOqDv4MhvzbOJBUhre1+asZvwBMRl2mBYzCvpAhVWB01LKXCwZ4wYSO8cHzfEHDII8QJF4brCJB0kHL7Lc2nF95ButoQnWOHzjSauV7l/OLAgVdZ0f/tsG5JJmJ7DWtE3W6XKirRcUHLnLnAMmDlyT393DCVO+BbFnHPZLS0vhgx4ipKppHJaS91JGageIreSlpiK03hvxfGOYDZmYK83obWEOfOnbnUTu0VwnbzhAAsHYunBpi49570MK1F3F+z1qLOphKWV12tIPuJItQYEZkjMCMVhE5Ow5tGwtxHK2Tn/xoBodoPLzB9wLSDw4+0MHupUNHpJA/yHx8Ms+8Q43seo+BFKNyB6+CQIqg5YhJ7QarJzTcAa13cSvgvBZRiOrExKx7F7mxHOMByduVG/u7JkrlNBFUmz7Ov8b6gLIqDFZQaxJEJIiDySci1FSpDI2JcIY5IYtJyewMvIvlPMLAE3UCPhBhcE1+iMbhmMtlkidgeSBHxhkFv1pbRCETu77QOzcAVWLofUDrQznwm/SHrvBp2wW2vbULUbfuBLULF1rqNj8YIvgSMItbdgPavcGwNNs/NQb9RweqZYrVRhXWBnZBw9gJUSpn4v9rxijVz6PRSsfj4KVUcYBVrBaXboZIb7PpOwwcL+GMvreWSQeOgRwtk11vKkAP9TGj1jzG6rBWzRLeFR5jZKSrHgiP83YYvmmQh2huqvlbGZoTdc5AR7oClTlhwtoIN506pi71LZvtqh/MHBxlhOqZIwKyU166BP8Ki5spv2T0Rcoli0Wh3qCilWGWLJyqXTgybFFKokiVieBxGLqog3lCJME9svWduGtGA5xbBjna6srfFO5PAXk6Ykt95lH0isOUxoeomQ5RU5ydv+HimrjHFM3ob8zukQxJnByiRUQjm0MkUt4UGvHJiMxYj8QSGiVa3JyQx/C9ec7oJGTUXshST6R9ihjAlbqO6tiZASZJA1DLZLhDqSJki+3ocngryza387BTTWQaBLFtrC2jOtBlPYp8Tg7kbkyYvluxTwGdWWoZDOiN10nAoogfelIuJ+AS2S4x4PBhzN0JB1cakP3WRQ8KrItegWzSonedT+hoE9IR+lgYwzKHjRCqdhT6YgJ5JSML2qBvkAGZJqLODCucbkNj8jqyxwt4JIHHnFPMTawGK0YZrPf30KNWTo+D5k2x/T5KNTMylNcGhX5qRHiqga+hCoJCF2HBLRD8ygnIALNqvw5eMEn1rf0iUFGbN9F9OmLRAR7POBPNlV01T6QMHYgb/yvA5ETrlJreF/IkhSDQsmIDr9GhzoSqxzuDWo5r26ozK/JQaKhZMQ/wobZEDRnSpvziBmQxWaLqjAOojaoDyVdnAmJndQwtwUblXXiwYoHlyxt4sEynaXdFkHZiCVA592G/ZdwB3mJw4I13DZveI+Bl9jTEEsGLlbebJW3AFliASoIbS/sk7AMRNUEvPKnjg8nTrZsi6OhAs1CbE+1X7feh9D/1Eikr7EqwHZVETahCL03ZukzPOmG+7t4+wHmaQyXkzQ/4vFsvgRn/+fDqjgW1viVzyW78hmlpDDSXeAo0Iwb13EEgsQPHMiNcUpGfJ9HbJV8CQo2A2QV68irKGrBb6sLEgT8VDinzpfaVOJBjkbS4cRTOvaUoXxItA8oWJw9rbqHDQK5oFIZxZ3IxyCw3KoXMUBCxT6uO+lG7qSWUbCPCcE+bIC3XVjlYlucDfT9yvIyX9s/4JufIWJXRXvNfTOnTiG0PLWQi2SsTfQbLlNiJYD7ZehufD9GvpR59TA449Kh+3aX4JZpBYT5kzrz4iV3CQ6/+O8CkL8kQR8qRinSJ6+IiD9p5YOdOGZIsTxFK1sAMZYzSDSIJyKIfWhXxCTEdYIm4X1u/Aul2JB4mOg2B1W0UHwpKpJFQqgja4OhqJmiImVpWQQCQ0VK2NATuo4uLiee3TTxaOUmrBB8BCq+Ao5s/RD1eEuRcrOyEbHITxj9YHVqpl/Bx6jYhzjtItroT232S2zQgc/7zxKFnYhFCOV4islAS1076CjmEYnt4gWzRokHxwSAy1I4gcIWT22HBUze8C7bYQPDCu3epXkTl9gxh4N26uQqJWH7wFGIqJ4EiY6kv7SNTj49gHu+lMbZIw6OYAVSOHHeHMkDjdANgYAWiupSHUhU4lhos0esJTs4jpInKELCAxwx18QYoi9biIjjihG1DiXEOoGyLdh/thYnmxg7OLAyiWeTEo/++YOSgZJdhmP6yz5eON7JdYiaV6+iXxIiMgb15pEQkmoWosYbNZOTTam5IlgD+o8CRazFXsvY4AmFjvzgJaQVFEMJGEWpEJ1iYyAm9TYrUhv2/odcisJqE70nyWA+XmNY6/pfCfNsew445nH5fXvbJSCi4RLc6soynaS6k/gMdMRxeS7JZ2g4DgQq8casAYNQAibTnwwrRWzD+RNyzBZ+SLQQ2YQVEXN8MYktNySEJ1seiksfOlRHrUMEnTAXfZ33hvvXTjYauISzQstsfriABh3G1uYuJZ5dlXgiQ9q2IqdwTHw3gHOPn4FzvTggtu/MNDBC2okVhoqFyAxFv5XgZXQz603NGEw9wAQOXhxvfMzFEMcRIGofsW4F0YC3XXAUo1OsBFHPbVOgW9+yiTpOlJbdPR5catvBa2eYwaQ5Y9g/OqbadjWAQ39nb0hAGNT4sTl8Vwyv4Bk6Y+gUHxuTQv5Mycc2HEHIZukBrFACIedUstDCXLYHXd4GUSSmCCKfcgiKEctxmf7wZknchUM3Oxm3lRFKcYn+2FofdNNY322W6GshrwckiANrbXnJ3IIVDz+GaxMPzvT8uRfRxK5sPcDewj8aulyHNWlHDG27IFLAoKX3TREjHdREB89ukwHSE4hWZCQEb0rMgYNjHp+eF6p4C8NGf2kW8avOsRBDUugxw/LXH3xTWqBBihUPkjYuM9nL0XhK047fUc0Tp0Wlkftl4m6cCk5AuARHCUiOY05Aq2oFpGIKwwsbPfqBg0fcAwdsqKSZoY9yUgKFWrejuPliHi1ZIgo93tdN24xQytf6lBl1HUekjxEmBzglrHjWVpbMTVjxUEPxQAvu72xR8lHfbR4Fa9o5kA/ajzh4+U3WluQ/p8Na6N4/GQZIrVY2j5acAdEpGQ9spg6sWnqPBflj0z1sgK8FCmohou/xJmucxY869DOi7LHUJqBgnskZvZqpsYAPPMG6cICHAWjeoHlxb3fbHOztmK0rm2Zv67JZXF4xa8dvoM9lLtMfkqWR7m5oSjzcZ3RcYNvmBwx2OfGsux9YwwMHePoL5Sx0PzQbObIXMuIjLRSh22NP5RyB2oMHVeR7QLySJtrG5o7ZpvnCvy6L4G3QpxRBBH1C8rkSdqYgQxtluIK+EAcZLrVUFL8oWmC0TA8YJAOdTgNtnZDYuychxgNByeMloWsr9lIbzm5wP2CDDiq8Nqf0+zBtuJOFKC7sT1w/X14q/YZPL6xuHF3VomOWZHItDWRzik0lBy3XNBojdIJdgss+W1s7vPoZs4v41Sm0k0ceVgnYZc4v6VRNF2INyWKh3TBI8jja36XCvrl0/jGzuHnerO5cNg8/+KB58NEL5uyli2abUvT29oHBM0eYPO0qHG9kXzcnlo15wq23mFtvPm1ufPIzzPHTt5mF5TV+ug3Agwa4FL1JxwQePcZKqDv5jEKnkYpYzUJ/r5KMEvLXEZwBO/7cSeQKJW90E/HwMlKcrPKcYf8PnwCXLWF88rlIyUdDaY8y1AGeujqN+jVRS77HZEagqeNhJRP5hoEsu5/IqO1JzQrfRJBPPHhdDh4wkCd7+pFPOBoYcnjUF2dO45PPIC+RdVkgoaYcCcgBV5TVjFzX1JwkAdeEY4TOcNmMb3zjFfe5ODKAN5ylYiKtojMsdkt/RjenEG+6tgNCYUjwmOFxs2/2t6+YrQuPmIuP3Gfuu+fj5uFHz5lzl3fM+Z1ls3rr08zpJ95ulvGW+p1z5uDSY+bM/Q+YhQuPmmOru+aW255gDhZXzM0nl83NdzzPPHBh3Zy/tGnO3nOfuXFhw3zB859rnvSCzzdrJ26kBLXCT7XhJwVwaRonT9KPoI09ASij2DlzRY+58FiQiktD7g/mClyeRwKyP6tAKyA6IZIELbLBpytMSz5OY5RiB6w9+ttpOBBrNKPZyga/HZJPfyEKilV7CbPu3XOVWKzBOz1jBjcQ48SDs7vtHVznThVAsQNogJVyshkfOaws29/w6R993YIerNGjpmXixgFj+E2MElYIHdvHX929uM5ZT76xjuvzxcN+bHhwPUXHwyrHLRhMWs4BrWr2rlww25fOmIfv/qh54P77zKNnz5uzV/bN9vot5vTTnmVueNItZv1g01x+6G5z9/vebt7/1t827/vgXebcxR1zgFXM8ZNmaWXBLC6t0CqGxh+Vl9dWzfbGtlnc2SXdLfPiZ9xkXvLyl5nPf83rzd0bp8zFT3zcfN5znmqe/cLPMzfc+iRe5fBlTppwkXwwhvFONDyAMPk+WgBnYx6mKugxX0w+sTIJ4GcZ5JgG216Ccyev/B9/YMlecgNGJ5+HL0QrnxkwOKZSZxSJmCekBpjSslvht1XHKVftERN8t18IeemajdyeZHuKrkXiFc8eGcDBtY1X5oDgpMs+I05ZMAHOlNoYYdDBa7RUS/xhB4Qymq7RFWKXUANhAJjocHDv4NH3yqTnPVMBJxa4p2e/XBrFlAkxJpW9dCBRzjhk0OS2t232Ns7TauZ+88CnPmEeefQMJZrL5twOTXC3PsXc/NSnmrVjq2bn4sPmoY+/39z5jt82737775mHzl4y2wv2/g1+XGKREi26BpfY9vd27bsdcbmN2o6xjgmRX6G5SJMmLWJuPHHMLKysmNOnTlCyWzJPXN0yf/xPfqdZeOaXmQfu+oj5ii98sXnSs15E6y2seHAfiOKhvkQy2tpyv/1lGxFAt7Teh4pbF5wZPebDYWUr6Lt4uNk+li/gUv87gStXbAJCYhp00OvUI1S/6slncOZKHd6zIgnREjy5ZTfDb6kAkZcUEatlU/d+/rJD2waDhGwytHuZ/zrFnL4kHnm4APcPrtBZtE08TiiLjLWcgwLQXiQ2nCWVMcJghOZobvH9QVJBV3jT25BHGBgOZlxywyUgefw961ER0Tfra/ZV+f6w13z3mUNPtyQIlCLrqO7ThL17xWxeeMxceOh+8+iZR8z5s2fN/Y9eMBsL62b9CU8zJ286TZP8vrl45kHz8MfeZz78nnebD3/o4+bR8xf4gZidfRw1tJpZXjZ7tPrgt9ZTosETaWgjEq4G5R6KCzw+F6d+pL/0HxMmn6Qv4LU5xhxfXTE33nza3HDyuHnGqWXzJ7/3L5n7Lh8ztx3bN6/88q8mv3hoY4F/wRfHnayIWqufbm5dcCb0mE6bYQnoU83joqujq3Fs23tARKT/G5SAdvAmBEwyDKx87D6Za/IRQ+IGiHZ98FFCkV1gWHLEbPjQ/JYoMGaVU7Pnea5g+yrUqOkHiIzxX36+t4408biHC6r3eDJRVUilMYDhZl/VEV/6yRjrhVJtWqkJtLuu4WCGNjQxBIcSuu4KHjZw9QRRKJDHrl1bp7N8mj3kqdhelLpG3Hh+RpDfm7i/bQ62LpvN84+Zc48+TEnmjHns0cfMQ+evmN1jN5jV07ea9ZMnaE2xa7bOPWjupURz1wc+ZO6++x7z4GNnzMbGjtkiM0t00oKVCsbO9vYOf+LFqrikaBONPbOWb96DLwnIjzcrRptNQPZMHZOi3XCmvrxs++jU2oq5/fYnmFXqvP/t219vbn7Rq83e+UfN13z1683u0holH5vM8bLN0iXQTJcUoCT7lZqYYipshq3Y7tPJx/b14MDeA5JLkjgpgs4WLsHRiQH6WZI9S9MOcXukD3HyiZUljoGuJCqeqkFkmCGpKRCAWRV+CCuYFXfElqmATxXbR6FWy0YAFtYaZJGqPTZ26OxsfWUpeqrNrnjSxJOx2HDSigHDDaueZTq9tEOvrgFuHJVg3Mh1iHVKxkvI+pwSSA/C4NgL/cEBzD9nQfsNtECqEMrwaOzyfMMl54F/Xs1s0oLmkjn38H1m48I5c+axR8zDj50157doxXXyJnPshtNmicbg8tKeufLYveZTH3m/+diHPmTuv/9B89jZC+bcpS2zhZUJLofRmERywct/cY8LqwucCGJlw5MgtQWJhk8OaUDgchoN6vAcDO2luvuI4IzQJ+xh4kT/4hNXAG56xrPN5YOT5oZzHzanb3uC+bpXvsi87Bv/tDlz793m9a99nVk+eZovZSIGfvCATKn5dSSUVs0AwgUKMtN8h+C+9bAVSRWax0VfFzm8eHXBrCAB8T88EUhzDJIz9y+LjU8+D3VddlMmG9ar7IgZVguajhxz0d6AVlC3sMyiCDFq6lkeEd2u4b9AzUYOdk/hIoPV5d1KhR47kMGBe3xt2Zw+scYDALs+TTwFa+PIRaAN+H4PLm30jDyIJJGNdaohumJ0LOYSRIQgJhvYYJ1KXBkC5ledxA8bZMNxRBLBvkbCxyWRkYd8HmTj4GCXzmbwtNlZfrz54mMPm4cefNCcuXjFbCysGbN+yiytHzerx4+b5YMrZvPcA+aBu+40H7/zQ+a+T1LSeehRc2mb7Kysm2WKa39vx+xSm/Yp4B1e1SDhULJZwErGtsFu+1ynArUQdWoitRETv+qmLuTE0aeLC/ZLpDffuGL+8g/+HfOffuUD5sG3/JR56tOfar7xy19mXvr6P2wuPPyg+ZrXfZNZW1tlWTwdukubfwvIKGgdNMgVPYigd1vFxRTvMcJhJRU79yQOfN0WMC9hP+GXUfHdKPQn7GG84vKkrIDmmHyUmYrFLmdVUwULRO6yrREpYCDX0OqphO0IvDR1aJhIwPKBY7XnC8ZSMi5NIPGsmJtO0qQACtnsSjwRKZGoqJaA5IOzylp/ZlkjfBwOKIARMcSias9ZOAF3SCsBz1AgpojRgYtHWfO/4eOUIjL2NxLPsOLsxwKlg4NdSgxbG2b70nlz5eIZc/7h+82DDz1sHru8ZbbNutlbXTNLJ240p06fMssLV8yFB+8x93/0/ebuD33YfPTOu82nHjlndpbWzPL6ulmhOHbJHn7mBEl0G2VKKHv4mg5WaMvLtIrYYt98Oc1mHkRBn/ZoQrN1K3jCCyghWnzB0G1ugqTts25eNd/z1/+K+cl3nDMf/sV/b577nKebb/uaV5onvPTrzFOOG/P5X/Qqs7a+qlY/wwliC5BL9yFBk9hQJJNREVRY3ZCQbBuk4labGr4uBfQ0PkiT/iMR80900/EOTpCAZks+kWrBUrcDJ5jKlw13247hfXVYKPgpanpGh+0MBnWtH+91QmQ+9UYU+s+JZ10SD65l7/PDBbhObZ9qEygL+aKFqCSMPCAGFXziWQYMxhyK5jr9zAwJNIByXomjJ0TfbSwcOyJirl+92CAvBzEmbkxcfgxrkzCn6pDAO7hwOal0yDP9YM+YvW2zs3HJ7G6cMxtnHjX33/tx8/DZC+byzqK5YlaMOX6jOX7zLeb4iZNmZecx8+gnPmQ+8aHfMx/+/febj378PnNukxLF6gmzQmOOJyzyvrtLkzKNQ3wDHj/NzMESD5fPEBzagMl7j+QAfmCA/uGLyHiA', '2019-07-31 04:52:26', 144);
INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', '<p>test<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAHhCAYAAACxyrMlAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxOTowNzoyNSAxNjowNTo0NaRq/x8AAP94SURBVHhe7P0HvC3JVR8K18nnppk7SdIoM5JQQAKBhJAQIAQIhEQ2yfnZn/GzscEJB7DNs/0ZjLP9Q+/xHD8HDH4YMCY9TBAyEiiggFAeaaTRaHK4+Z578vnWf1Wt6lW5uvc+915pzv/ePrtq5aqurtXV3bv3wgHBjMCPft2/Nbc961bznf/ymxzl+sHIpswXE1xPirbUxojcst3uqn6DA6tfJ0Y9HmLGpmP5Dl9d4yP24z49mGCpRWueUZTo6H9CIHOQt5YjgoZt0ZilFfrTgXI8jlHkN9Cj3vJdQ8H+Av3zIObA77Ap6BBlL4sLZmtjx+zt7JsFKjNIl0sqjMQgqo60cnzFLCxRdc/WGYFuCuyzQSSyLYCA8pNgIXKS81nSzaJfuG9kKixQsPd/4EHz3p//QN/BfIT5odTdEb25V2YX8ChK9ptoTMQpc4Rpj7kkHoalFq15RlGiwilhZOJxWFyimSQ3mShAvBxPmdMFUq/br6FfqyaJ3T7wR0TSK0rJZnd7L594AkQG92kj0j4FuLi6SIlnIUw8NcBUYC6yreH8dCEOOvFTgghGwg390clH8K6f+X1z8ZHLrvY4R6WDS5igMjeM9l1QCMnTWlTXclwlxMVYqW6E2A2BDLIajti2lpcAlTltA4FMVjxHFBp9YjLDlle28KysjCJWbBRBOl1qLd8lwH7Dx3C+gUJNMkKvKJ2I7+8fmJ0tyhoycWtdP5krIoqUEOzHAe+jxZUlc0B2upCIFfRA7jSZRVfSKjjR5DQLe0xOPufuO29+5z/8rtnfRZTXHo+PVVi7jbP3Qp+FefQ225iHoQo48fT46IqjkcaYmZfw1B4/SmZK4gQ48VRQj2eaT0ErKXhM9d1hP0w8I9Dn3opRF2PVg8SBK0Kiyz3vu18ZdIqKYpYo8fAlQk0sIZCxxhI1Sx4PiRfTeXNKLziJyb4P8picfIAP/fqd5h3/9T2udoReTBkbRaVJxkagy34kNJeYnBFli4ux7YqvCquKrB7NZm17eYlRcThhfBQ95siih0+cTePIbqln+RGxYKOEg95z0R7fGXQlNi/QthegIQ62iOASG068kXySxMPQ0lSioo+dxQ/M4tKiWVrqXPUoEeh2aPQDQdN+q+87eJQtQoFcA8RnSj7otHf+1O+Zj/32JxzlcYiRnX6twZNTEX2NCaUinc7+YLFOWWCEqAJp9Sgqmax4j5mCQEBuGrGwYj0GHSIa7vWUxApWHSJuXTgA256ceFjbFgtgibqIhZfpEVaoiLNvWwywQ4knZRBhISRy3JEc5nuserrgdG3SiQwJMj56wGpIPLaaQcVwhRWvekRUq8yUfIDtjR3zK//wjeb9v/JhRzlCDaV91UZbsyXRdfBeBXS3JBZs1RXaPhyUYEmneZ7J7FQmoDRMMEjGivUIOzhRfOAEemE5v+pJLLYJXYAWa8604qmDVXr0JthmFPRAzrGw6tnb2jP72/t21ePAKV8lHtaPjWDVQztqaXmJVz7V2wVO137k5dh7xUQMZ9JvWjfMF14ijwpLDImFRNQRZ04+ABLQb77ht817fvZ9VN521KuHT5f7PZOjLCnOtdkZY03SDAEUVR1DfYz1YuXp7wjFkmjf2EplRri2IAWrU9GMWa4uZMyD/IRbhMRim5AlMRTdF0uyMRI5EMrKnlsWGdAj4+Ancu8gRIFsQZ2M5BE8ZMBA9oWWXXWyfsbQwb59Km5pFZfbHDHdZV7VxhoZ0aiwgMFOXdSG0CFZYWvtrEjEmEvyAXY2d8ybfux3zM/8jV8yG2c3HPUzHNkePgy0HbUk5hFq00aHk8lxxIoFQ5bc6cWJlaSbKx4gEkE10eo20yEYQTSQI5F48LSv5Et8JBbbhCyJ4ej48CIl2RiJXF1xlP3eGAhB4okAUssUVpW7lHgO9txDBgxKKPhAYuI6IWMMvkFapsRjv4eUehvUhlIRBbZoNrQVOqQjEanqjSFdohEIDJhb8gFwlvjABx8y//6P/aR5x0++x5x/4ILjHCHT91cXYwM4BHkvUpQNGTOF0FJ2/JIY01s2ImTFO2zYZNEQjNjQ8SQq8KqHMo9OPG30STFydnvVE7myIjie22O/R4ZgJ34nHOmg2mOGL7dt71Pyke/0QMsuXwIbkTHxvU+rHlxu44cMZEe5yTrQV6Ua4nk+tNGCSFMS5XoBkVEUZY2nyBYjAxr9hoM3fP2/M9tXdlytDOyc9VNr5o6XP8O84o99obnhiScdZ74YGf78MMLtTBGycsaCIrXsl0wMqNsHUommQAIvUpR1jPAjlS/oWzL9LfADVMSY3jOuvIif1lI0zAxuKoIRKw4N9aXlBZrU1CWfHAJGQSpHJlpAlnlXQ9f1JJTYSwgeAacsNqBDxoooQVeUNyD0uBFgpbN1edfs7+7R/BZpLrgbbZrMVUfAB7lcW18hUXuvhznBhK2V62A1ZbofsXTwLogsoFHyEehKpTOgQ0s+MW5/wRPN01/8FHPrHTebk7ec4NdJrKytZK9Rj8FnfvIpaCtyy369izLMLlLdK/bLAh2QyytLZu3EqllaX3YcQlHVMegjEInlM/oDKVbOoCJi6X02BMXUU7HhWVzoEYx1HKiMeW9p2V7EOCgdTomLhJAlgZYjJ9BCxUkob6lTLESHjBVRgoHOuHkHiQerHsx9aeKBLdrUgcYlXd8/MMuryzTnLXOZObqfoifkaoAapGE+2woxFTBL9vPJh+3bYhVeVwo9Sg5XLfkIsCJaph2wtLTIZbvjjvCZiQOzSGd52N+nn3ajedrnPdk865WfZU7dllsFu2EYfuQHc0QbqrFyHqURb8n0t6HPcDJeJ0bFhmdxoSIIKHZOHG3BqocfrwavNJMESAh5UoZWhJbNTkR5Ywm1x2dDZmBHgkF13LyzQFl98/IW9cm+n7O8BWR/ti2nIfRX+cIUi+NglVY9nuz7yFFGhONtUCFQUz4tQxNKGJIPpHs0BP0hw2oozZSrnXyOcISnvPB28+wvu8M87fOfYk7ffgNR1BCkYjAg49GZGa2W5BgZvkZptA9kKjVspK4yCgUbnsyFgpBAsbPiqKtVDyM3IwR6sREHRfbFgmgWWhYxNIxkqT3+KjIDKxLK6nROnaSLxLG9uU3bDj8eLbAW8FceNMBf2iJ/mGJxlQff68Gqx7vWg7EWDsSIH5llglfTzBGrqMYdnypY08WWxxBHzs9R8jnCNQNWRM979bPNK//UF5kVuSxHozEYkPHojOpD1ZVieQVmFfiWTH8r+gzHD8UipYKNgBwfdqjq49OxQx33KaD6Iq16gne4xcd41YCDIvtiQbSIrHxKLJrt8VeRGViRUFGnOGMOIF1cndnf2+dVD68RlJotLioXtCpCBZuTw/SKBwxWsOqRfY7kMChZ5MJxMvxR4DOZBZTBnKyCkiQ0hAvw+S1RH6wPpUjIMdQp0xGOcHWxu7XLX07+b3/5583Hf+dus7ezFx4YQYUQ1YeqK8XyMQr8llqMqnyPj1hmog6u9lRfHlo14KDIJTPTkFor2u9xXJABeWBFQj12S/C6+E7Prq0nc6gkHvxNE48gfJMBCVilKuStA01RFlBSSTIY7MjGCCojoPUCX7jkaC87hqaVUMg4Sj5HuPY4c89Z8yv/8DfN23/83Y7ShhrDXSjJD3QqtYx2iOQQ6PDZb8NKpx/7uG8PnLWCUZCL/qrMEkKFpv0WCjKpF4UeuzlAj3UPOLnjhAibfpMBduEBP9mBzT147PT0Ax/8kIF/k4GT64D/8mkNzl9gU/tWW4AssRNOT9xIurH/GsgIHCWfI1wXwKWNd//M75tf/6dvMhtnr6SDtTi6HaMy+plV4TeY/ciYGW2ZFJo6JNC/6nGFnBzRsupZYgVeHoVQuWqqx09Gpumlx24OXo8KmGHpw7/JwHV1eLXU/QAP0TghyaxMQB0nB0sruJwcKBUBqdB+AZzHWJqrgNRCqkKR0YFIl5NN130l6TRbi3GUfI5wXeEjb7rLvPlfv9XV8ugZ9oKabMBrGSX+GL9AIt8xs/T5oLPy2lcUvBFXyBklWtNXU4DgZUJh1Krqo2xbpDZTSlzthtezBXxhl38kjk6K8HQbUwPbrkIfXHK7w+8VWr7gV2T510kb+x1cltCLo9Lu5VXRYM/rluS9wAQkuo5QGXoeyMQNv0fJ5wjXHT72lk+YN/7oW/ieEEMN4nA8u1pjkOf4A6mlbNGUigQS+Z7E0xMKCeGMmi+55eQ9rWwMfpqumgKEjK/527ZIVTJGeuzmwHr4Yw3gCtv+7oHZ2abxJxNtzh3RNNmLEgOX2nDJrZZ4wPHc2qU2MYwn5ZxGoJtDU6CCRHeEsRGiR8nnCNclPvwbd5q73vpJV8uhPcJLEgm9Yaoyf1hE/JZ4DmN0Wj8UF1iLY+tx1C2DP4PwfG1bhB4EGSM9dnNgvUiZss82Vj002fO9ntg26lV/B/Yhg8IJQqIey8S7F3xOPKGuF8vJT0Wgq705xL4EgWhJKMRR8jnCdQncrH3bf/xdc+6+c46SHAYWWaIiR/ywWlBWGCuRlW/Mym0fDgf2nWLFVQ9DMSKZLj+1M3ABGxqsoXRYiSdFhtpj1wGiXpwLkTL17S5+JG53l4q1fg5hp9sD2kX7/Gg13uHG3+lRQC0x17RPAq5zs6J6nodA014BXlcKnYYS0b7EAxwlnyNct7j02GXzW//qbWZ/L36apvPAqMLZqJhiVtUVMRU/L1o10OBqWEn9JccU+WBQ7PfTABsarEXVMkbI4CMVz1NzpBwSbZ7QB4qu4V5PMQkHRjQGBq961BysbXtkiQSvR0zEaP8nosEUnxOoYBAlK163w4h22iFeFHHEo+RzhOsaD935sHnwww+5WoTC4PfkiD9UXamgD1RYDqFEXp6oM/kQwA4eMqDDFUdsv+IY0bYw860Q/qpqHZ0y3maCgoGeVRoh0Y4IUsWKcm/XPVodTu9WqBCGf78BnSQtryybxeVFXvUUVQp2BpekSzIl/STxdCJvr+QlgjitiNMI9WwRSWJVukfJ5wjXNfBDhR/89TtdDVCjN4MSt65VQEvJ8afY9jo9yjzJ4nJb56pnCqBeMqEmxeEvocdlLJPTIVrZVMjhNwbQ5I43BqwcW+HfxikBmoF2RAiqaCNVdjbdqkfPmkonhLJAOrg/tIhX6BCpqlIBUhb05wGY0ZtgaNpIR5G4to3EEyMYNlqXGfiK7hGOcJ3j7rffw5fgAujB7BCQVCUUdbWMvoBZFb5mlsWIU2BWTcegmYjPoZfyP49tERFdtdtPSxB8jkOJ9hiPZXI6RCubKnBokseLiZGESm/FTzQjQszHqocfrd6R3+pxKAZnGW4eNfvUP4v4kTiKp/iEW9EWYcHt55oMId/aAVCXLYeWfhHOYMt+gESQKvz9IEs8Sj5HuO6xdXnLfOx/3UWlYCQHKHPGoW3HSdBHNZoCMyCXDTiIABKPfodYBSQEuS7ZLthJMbDXYzyWyehgji6bKnBAhh6SIW+WLHDsEAkhBFYsuGS2s7lrJ2eZoYt6liGJB5fYcC+OfxpbAopn+UoM3L/gV2RKELUe9amJp20/tRx+BzWvfZR8jvBpgY+/4x6zhxvBQPkosCjyHaPA9+QO+2WRTk7LB0AyeGAKZ9O9q54eswEaCgm7x0EsE9VRjZPGAObaYowprIK8Bh5qw/vb+KexZSVVNKYYVGQKqeAhA/9Ytp6LIxUNTjolZgadZucCsd/2oaMikMKQeCILShTUo+RzPYB2Cm5S4swJb3rmjcqgHcHi0qOXzYWHLrpaCDW8K6hLjbHRJxuiqRML4KzeFbHqySNUKk/oBWj5RDczMY61D+RMFO2kDL/CkceWo7kOGGEuAb+/bW9P/TQ2Ias3EFFCX9tPt+rBvZ7o0eqSf9YrMQuQZlvd8ch0WwKxPcX+oFiwQgHEnKOfVLhGwEC/6WmnzZNf8ERz+/OfaE494aQ5fuO6WcbP7NKOwiWAy2c2zCMff8zc9dt3mwc+VHji63EC/CT7y//YS80LXvNcR7FIBm9mNFuSYxT5Dhm+hbNS5AOOGckkKjFB6n6GwIRLf2nDPY1lPD3VMDLuKHbQOoF+Zmqc0X5bPS/BCcGxJAnhF0HlQQO8E3CbjpUsCk5Btl1NKx2yv315l5LPXuXLu4MhLrkqL3QontVjq2ZxxT7h5vdh5km8yMoowGxTSwQyzWi3bAqcVW8kfezAszKMx13ywe/GyFNDaPrOVWwLBvrq8RVzx8ufYV767S82t37WzXap3oGP/c4n+M3P25e3HeXxBawGX/CazzZf8qde7igWyeDNjOZgKo34PfoWzkqRDxAz4ifiOX2h0VBgL66OIxM/Q47hGh6lQcXWQlIbsbyqZ1LPePuA02mrRoJySNDn6toKX5/Bo884Idvd3uUn3arJp+JwcGETDx4w2LqEn8YuHYeDMS5p27RT8GXSFTqmfeIBX8sQhmrEmCfEdKYZMWluUfg3qQ4WpQsC6AAU83GVfLA8/vZ//o3mCc+6hesXH71s/uv3/ndz5fwm1w8Tz3jJU81zX/1s88yXPs2cuOV4d9LROHvvOfPr/+LN5lO/d5+jPH6AffeMlz7VfO33f5WjZAZ5diSrqTTi9+kDllE/UhxTyWTFY6KWx8VyV4ev/KonqNhaSGojJ080S84wx9oHvL0aIgmpqkMDPz2tkw/etwZaMfkUnA5kW4ILJBwknuQJN0asQYht045ZPU6rHuwjueSmZELxWLkBiPdMEbHZSEeqiXch9PiIMbIppSDsEuAaAxPLhLl4PMjHAh3gcl8Ffg8TGNCnn3yD+UNv+BbzLf/g9ea2Z91q3vM/3md+8e//mvnpv/oL5r9938+bn/8//qd58795m/n42z5prlzY5C+4lc4HbnrqafO6v/mV5thNxxzl8YP9fZpkNq7FSY/dF/XjzXGVUFY+Jjbk03s9oRTXcooTYFN0xtgU+6QzWi1WqF62KqAgMJCHEhL77vZ+NfHgr9cYVBn7lGzwcwk+8QTCsXik3EKPeOSPkZlDc2Jd9nPIGmsAMRX0runK5zhNos/64mea5375s83bfvxd5t733u84hwMMlO/4599gbn/+k7h+/sGL5if+3M8cysrnlmfeZF75J15mPutlT+ebkW/59+8w7/nv76te5sMluVuecbN52uc/xbz4Gz7HnLz1hOOEOP/gBUpev2jOP3DBUT7zgQniKS+83XzD33st15NBmx3Flsh/I36fPtC63OYYit8S9QhUB2bPqseXAn4HInlbpb85O2NtAwVTISIJV/WrB5wk4g/lXr3ywQoHl92yK5+M05Ckata02by0a/Z3dfKxMoFeZBdV7BNorJ2gVQ/tJzwlJwjFI+UWYnEJK0bObElWY556c8DhnvpX8Llf/wLzp//rHzWv+UuvMk+nyXbKZajrEXhi7fO/+UXmj//b7zDPfuVncbve+Ia3mHf8xLub95dwZo8HCyD7n77rp8xH3/xxvtQQ48Yn3WC+4A98bt/A+QzEmGMhJ9uvPy7x4KMkmpziBaqpVvq9nkHS01O1OpR8085Y2wDpjFaDDnUOEsDqMby1YNmsrC3bsd1rDHKR7FBNmfxbPVvxqsfKRGYCCA/xIvktLVPicQkTf0PdmqUMquKOmTrpw9XW68Q1Sz43P+10cNnrMyH33Pz0m8w30pn5q/7MKxzFmLdTInnfL33I1fqxeXHL/MLf/VXzK//wjWbr0pajDvjsL7vDnLz5uKs9zpE9QMr3ehJMOsD6lCA1JvFAFmfTfMWNWfiTkQwqHVDyTVUngElWb1XFBnuAkpIifSIJ4MSNtxW7qhF2c2qAoBNCUfR0yYPk+AulW3uchCzsWEmkHUHzZP8g+cBO2i1auhNV8ZQpHjyn1EGB0EhM1euAhHXNkg/eVKyBnfjpDNzb+aa//7X8QAGSKg7W9//Kh81b/9M7+T7OVGD18+N/5qf5sWueABxOUOJ5ybd9nqtdYzRnh/kAJygLy/a1KsG2Em2eZ19tj8lMb/iRL1zSki3R5w0/gUxy2Gii8ZvQ9IYJU8uobYX4K/hUNJlkl1fJv6JjW1lbMqu4H6njdu2wcbvN+fYyrY1sD34HfYlftlAOjzQvsz6fKLrZwicjBxT7Dl8rxfq0YvD3SphI/1F3dCEXMeQNLuMj1MlbwJUIrHr4UhnrZXw5Y+4jAGJfWbFPzCLcEBEh4WdQlAHDMdWvgvaYZHQLDmAV/Jmg24KY1aav+j2fG28/ZZ7wnNvM8179bPOcL73DUY35nf/wDvPYPfa3WzBAzt133jz8sUe5HgMHAi7V4RFHjX1aRt/9rk8N34SP0HvP58mf80Rz8pYTvqO2N7bNJ995r6uluPH2G8y3/MjrzU1PudFRDNv88T/70+biw5ccZTY85YVPMn/gH30dTwaCzYub5t/8of9SvJz3zC98Gj+Ng138ibffw9fLn3DHreapL34y7wf04+XHNswDH3rY3Pu++4v9lgNWefiO0o2UdNdPrRPlwGxe2DLnHrhg7vv9+825++d7Pwpnxk990e3mG3/4dY7iIJOQIB7hgJIZXi1lhTp+7ZfhTUQvnSzrOk5GPtYRNo5EjP0l3EdwNK3Ack5Y+F5uBNJXnwRuPPi+iwPzdNKgDTfdJVG0L5sP1jHuRB73bPZ39yjBLfPXICB3QH225cb02rEV2vcoZe75IAzS30ouTQ++NOCRbV/e4WNCNW8AkfPaRCcdXA5FTKlMREE1sp+QSo6EwQFbjZJo0u1Fm3nET0vH5qaiGQYJXPXkg/shX/7dX9wcrO/9hQ+Y3/iXb3a1EPjC4Xf+y2/iCVAD34L/ie/5WXPpkegllA49yefJn/Mk8x3/4ht9fFi1vPnfvN28+2d/n+sxMCm+9q+92jz/qz7bUahf6YB84xvebN778x90lPng+V/1HPPV3/flfBYs+K9/4b+b+z+Q/wLq9/7Sn+Kn+nBT9Zd+6NfNbc+6xXzhd7yYz2RjfOq995tf/Hu/2vXwxYu/8YW8D/lsOAO8quS9P/9+WvW9a27fo+Lk87m3m2/64dc7ikM8ekuTCoFFORmg5IQ6R/8glip4SssW8SESh6f1kXjk0Miac8QsbxKspUq3pZDEsY8VBOnSJ1/JICM4bvyElgOJrR6j1RS1Ex4xVpBUkExwr4dTGe0jeXx6FQmJhlnzgYMAiCUNAmNo+8qu2dncyX+hlNTkI9cEJFv4xvHDCdgLOUVAFQWalKgEjkI76Vc2I2i2U21oMAYvriSEonKGMRiZAKucnz0OEb25jnduAbCxRxNqDD4Liy7njQESzzf90Nf6xIOB/iv/6DeLiQd4CungiT2Nh+58xLzvlz/savPDh379o+Yjv/mxoA+f+Nm3uVIKJGMAB9pXfO+XmJf/kZdkEw/wtM97svm2f/oN5uRt+SfsNC4+Qqu5SjdjEnnpt73YfPVfeZWjXEUUDiAfLvOd0PShMhmF8DgUnOHjNkRJBkKQO4ywiz5zoPGHMYjjBJc3sRrH5UJcruSVZOsYd/qDXNn7tLam9pB4kCBxTC/kDgHlKBcNYuU3aSPxQJiFoFSOMOZ6u1mVkFi2GiF2UoCIceyySXEMpuh4hMpXPflsnNkwD330UbNx7oqjWJy977x55K5H/Va7XIUkc/Ghi+bsvedpME2/n6KBx75f+9dfbdZPrjmKMe/8qffyZF8CVlJf8Re+lC9tCRDbh37jo7zaOAy8+2ffZ3bVmR4u+RXh9jMmiRM3HefY8NMEn/jde/h+FJ6sw1mg4NZn3mxegqfoGnjsnrOcgB775Blz1+/cbd7//37IfJj66bFPng1OCvAI/e0veKKrHQI6DwIv1ikfY1CrGJjZNhKPnZ5Ay5mb6KICZ3GEYR+bK0gSweSOS2d834iOC+SVIbk4xIcEZFwxBujY0CO5ZFCEKGawvbXDP32QWCzIB6DY+ZI3VFk+UkLVkVQxRMzwYQxE228odbRa2yJoDbD0pku66FF0pxhKJ1YvQ5ylGlc9+Xz0LZ/gtwp88Fc/4igWuMT2E3/+v/vtXT/9XsdJgUeSf+mHf8P89F/7BXOGJsKpcMc6v1ftm3/odeb0k4d7NvD/jp98j6vl8ZQX3c4TtgbuD937vgdcbf545K7HzEd/+xOuhqTZ/8TbB3/tTvNTf+Xnzc/9wC+bX/0nbzL/jcq/9s/+l+NavOj1LzBrJ4dkmsOFBy+a//GDv0L78ef481f/6f8yv/xDv27+n7/4c2TvTUHifeWffJkrXQ30HxK9oiMs1pExJCR8YsWDDT2X8xnP47PDGRxh14tmdOykiXsiC/wQAx6wQELyCSin4z41+JDEHzDHNrrkg2a5XfcLpcMTbg4dLnA1BZe67fvbMnvIVfFRMhfeZyNwGIMGl0rKIzBYFChKyrSIuiSB0xP1nIkUbcmrf9mNzr7DL3Y5UM9jcMgWPw0XA/cS+Pc3Wh1XAXzgvsWXftfLg8tXOIvHfZ7drfQ7NgI8bomHJmJsXtoy5+8/72rzB/rvbf/5XdxHgL1J2wbeCP0///FvmnO0WpRBjlXjh9/4MX67ggDftXjSc8uX8gDsv0c/ccZsuffMYV8uUxy4/v6RN91l7n7np5gOPO1zn8xfnp07+o6AQaxTXiNUqRho2c7wYxJWPS0z88M4T5D2Gg1VjC1sOK5w+ZVXQTRmWQ/HquPLGGToMoA6C9mqRqAngGi8qiJYUfsXLw5N7OVsxXAyePovq+BIVVNZpiXiL5cCmWmTWujGWw6K44CTh7x6OcJ+Z1c9+VxPQAL5kj/1RUESwarlTf/nb/NEWgN+Tx838GM89JFHRr0G5qan3mie8JxbXa0PeBJw7Fuua+355LuGZAGcuLl93wdA3F/0h7/AfN0PvsZ8yw+/znzLj3wdryD1gyBITGNWZ9ORDnhPiVl9x4bCaIUqYms4D8NJVNHLXN0rYx12A5ERcciKZxmX4igJ2cRDNKxCdvb5ezZ42GB/l04AlxcMFhTb/PCBpbM8bXiXGx4owIYkhmS25x4ysA8gUFJx/SeAZ+vdXg7El0lxssYrMaYTfKEO9kmJx7+1WoOqoFRNEVOFZkHLoECvamAsAstt20lwpEI6suWQUSFAuOUsxOM6+dzwxFPmc746fEU/HhveoyV6C7iPsnoivTyFxNALPKjwJ/7DHzR/5Me+1bzsD37+qFXc/R98iA/w3sej5SGKHOJkiaRcA17787of+EqOG68Qes6X3GGeSiscPHqNR+DxnScNfGt9rkjGeDroxx0GKbr1W4IRPxZHXe71HD66W8UYpMfpeZAaxihu1GMVZN9egO8PLXIS4YcV1hb56Temo06TPO4b4XFmbPJdJDzMsLJOdFpFy8MNXnZ9xawfX+VLY/aejnPugASFmqcMrDIgQwkRD1DAV06paSYRQNKxiccjkZkyFmBENlV01SKUK6/idXri8FpcG4vHdfLJ4dZn3mRe8UdfWp2sBbkn6y6fDR+kKAHmP+drhsSHV/HoBxdaePijj/KZWPzgxhTETa0NpeOnj/F3mvCG7hiYaPw1foXSI9lXBXE4HcdJKNKhMAO4Z6j/i14Oy33FLlgD25VmiANjAquOlbUVThBItnhYhzcaG+DxWx2ERhue0MQmdSSqQXaoM83Jguc88l/UcXK261Y9TOtph5NBIuMv2JIvvepBKTPMQzj+cGi5pKOPtcSGZvaAI7FFQWKzDm8hMFWOw3IC4fFw6kfJh4AXdMo9FOBzv+4F5tY7wgcJYuDGozzKrIGDoQcYvPjiJx5QwEoLK5lt9eRZCxcfvsiX0i49tuEohw8chF9LKx48ZIHkjEkFT7i96f/+HfNfvvtnzL/+zv9s/n//2381H1MPRMwdyZhPD4KU0o9Rui3hiJ8THybMw0Zfy0IpqoHQp1oFxgq+J4P2YuzjUhjGL2+7EzbouzKesNzF5154FQB+tvWVgZ52kAzEoIsxjlWaBuhNOxE/We0ALRtVZILIkHIQMX4kXhO64YRH6SiQnri8bpIP79RrADzijSfA8KVWAa5R497FsdP45n4eGPD6xrqg9CbqHPC483/7vl8wP/PXf9H89r9/+6jvKMkP4j129xn+vBrA+/huu2O4z4XV189+/y+Zd//07/N3m/C2BFx2LH3Jd/5I+6vag43uHWltFEqW4lVngLm57zMUSs2v7QKc4GEVgruP+Nza2KMTLmzDfZ2u7cpQxtsNLG2bE5H0J5Ic+wINCZ6a00rzmIOk1fYnE2iFRSeTWPWA3jVHKRn4C9KOBJC104oO4ChsUSNrL0RWM6uXi8Nq8x2zrKEOQNWpC65h8gkb2brPcBjAJauf/r6f5+8Uven/+h3zqJrIkUS+6A+9xNUyoF68i87w4yfibnnG6b5xRMCgxqR973sf4Cf3xuCzvsj+VAN+YO5q4diN6/w0nOCT7743+32sFSXDmDJYJyBxM5PfhnLLtuKXRJPHfjVmil2jz1Ao5Wpzi8EC5ngCx4qCTvDwpdqDyoMwWbAR/+HgStSdTKdPXDLz3wEkYuuQVFfVWB77Bg8aIDpmaX4JgUy03pEAKnbAyrMLnLICQ9hZkYregEGbL1dqnVaHEjjZyOZoGtcs+cT3BnDz/2oDj2vrL6m+9T++M3jY4IWvfa658UnluPD+MqxeNG555s3m2A3lFdM8gDOy533lczj+Cw/N591xPeD7YNVTdYsnv9C+vsijY6B2ITeCS4hlG7pjTI9BzW5HV86IgveIHFZdrRb4SMBUYI6Ofb6ktYbvAqE6zln45pxBV0qwvYcfiuOVUL2ToRMkHgLiwYMMOCHmez0RP4tApqBQtDM8hZdGO84WyLKNh3iPLMTGGuOWtTsCuGbJB9+Q14MO91nw6PINNNnf9LTT5nNe+zx+c/OhIhqYeIP03e8YLqXhAYBv/2ffyD8aVcKb/+3b+cfdBJDVX1adN3AJ4VV/5ovNjZSs733/g9XvIs0beCpub3vw96xXPJO/H4V37WHDL62+/m+9JnjBKrB2YnhrxPwQju6OsV5Eqtuw1nLm+DWxq7PqqQNuQlcdgY9EyRSOfX5wAI9hdwLTRTZegi+hW0kQJ5XSw0FPKwO8qgkNMh/H2OIq3lpNlZifQyBTUKiQ86wCJ0MWUkZ6JCIrrloZqR6iydr8p41rlnwe+OBDwU1+TGJ4t9i38/b15qv+wpfy8/xXG//vP3xj8NYEvP0AL0MtAauPd/308O63lfUV89TPe7KrzR94q8ILv/Z5XL77Hffw59UCLvHpp/luecZN5pt/+HW8z2TfffarhjeVC25++mlXOhw0x3rnwTAXOF8tl8UT8rnFWjeUcufm2KNlEasKPP3mX9RZQZIkCtb5Xg+teuz9n6iToeJIrJ0xgaSIy214eo6zUw3Q9zaCShODNPlxakO0GTuDAkOqGcnZUTOsujSJoaaXgE4+XOmq48EPP2ze+wsfDBIQ3quGy2/4kiMGUeuxSO4HPcBQVNUc5EY9wDciowG6fXnb/M5/fCcPXgF+NfTWzyo//fZ7P/d+84Ff/Qi/5Rc28Qh1ct9jDsD75171v7/C4B1TWPHgabkaltSTOvhSbBFRH+C18TngjQa/+k/fZC48fNFR7KPXt95xCz+IcOIW+/64j7/1k8F+fSGtYrEqmh8aA6P7AMiJjrOdk265LyaeuaEcATgpV1FawXcg7yMPSUBYBaUJxtlK6AXr6Feypy+lc1ezERRUMWMCfnD84rL2uBcU98tCMpBGxY+HhGvhSMLNSMwPkfF4qGZjSAg1DMLXLPkAv/Wv3mp+40ffYi5Gjyzj7AOvg2l92RNPpOCG95lPneMN7xyrfZNf7OKFpCKfu+GJR4U/9MaP8pNwkMODCfgpA/6mdgG/+Ya3mHvebX/zB0+FveRb5/tDb+s3rJlv/cdf718DdOZTZ83lx+pPleGeFFYrZ6kNeJy8BDzuDTm09dz95/kVQSXc//4Hzc/+jV82H/i1j5gt0tO4j3h419sv/H9/lfct3iAM4Mx27cQcknFm0u4e8xmM1lUKwyGkQIQem8VLbj3KTZSN5DmKOgf/o0xAmDasUDBGYm2uBSSnUAD6FT8Uh8ew+cRSgTWFVDJB8wOveqBbduOM2WJdMEQiqW5eDXd9FIiEhAhOhjtfVJwIK7zX5tAdWOrgqv+eTw64LIMfmMPKB2fMG+c2+Ifl8B6yGjBocbYt361BU/Ddm9oSHveR+D1NJMLyj5B8pgtwMJzEmbxj4f1kmJz1iigGngb7uh/8av7RM6wSfvJ7fpYT3azAk3df8T1fwl9EFbz35z9g3vijb/bx5XCC9PCoKIBVWekRaHzz/NiNx7hPcOBtnN3oevoOP7qHV+ngC384gcClVP17QHe8/Bl82fKed93LiXwWIC78ns83/4PX+30HJM2PCYnAgJRVEQYUO5EkQkObgVVPNvn0u66gLOU5gYiqNB3kZp4QTRMxlAL6Bat5XGrHcc2sxGDFA8Kj2XHz0jafgGK8yJUTr8UytghoaxhOWPDz05yRXALm1QRCZCWZSI74fypRO67njl5f6BdBd3xlwesi+XwmAQ8cvOYvv8o858vu4Jds4m3Ps+CJz73NfO1f/4rkh/PwRu973n2fq33mI0g+DjxwafgGIzgezYXRnZILggLFzkn2HkX2R9Qy6HdfQFki4PiKoraNEwpxO3SZ0IiCQvLBlQx+rxt8JQbrHjA+7A/F7XKZaaRS0orpOGHFS3rxNm7EUQSzKnyFrJQnIkaq0IeNdsBVSzxj/EiQXTp9hq/pZbfPRGDF88s//Bu8MnnWFz+Tf1LgVMcPtMXAzfwv/hNfaL7jn39jknjuec99vD3egeMBZ8lYSSQ3l4HCMTDLsZ3T7bWXC5ExS0AN5E3P1+Foa4GCrWDCxb1GvveTTP51D+hX3KPhez3oY4jTltNyrADiGyv44d1wGTCrwlfISnmiGwj0oYcE2KMTDyvZYjfG6kiQTZ1xho9WPocEnH294Kufa17zl76MLwXiy6QfedPHzCffdZ/ZKtxTwZnXM17yNH6aDT9rcOz0sWRSxf0n/PT31fx+z/WA3MonBx7N9Ic/cYU07D5GOuAbh4Bj56SY1lAX2CTpKoJO3bpYnpulMlFx6oYVMh1J6FbX8EqhNsY6LpnhjQXDuG97gCx0/Kono1KzgsSFl5bi+Cv+lAuT27EUJXIMChWt9Ky2+QFaNr9r8hjjoxvTjB4ln0PGE559i3n1n/9S8xT3xUvc0zpz7zlzkZLH5sUtmiQP+PtENzzhpLmZVjt4+28Nb/vxd5nf+Y+/e0iD6PpFb/LxoP7ByOazaPSVO0Dz3Zanejh2ToppDXVB9pJbp25ZLOUUZZmhuGWjGaSxj1IXeKW8NhIJkgh/zYJctuZV8LGfNy/jXg/0LV2jFifGB04KVuWBmJww02pWGtwcM46zbj5ELNvqJGCM/W7Ydzj0uM/hKPlcBeCM6jmvepZ59Z97pVkb8ebqGL//Cx80v/l/vYUSWPmhh89Y0AhH8sFvBvWCn87B6KbuwjDPj/TG8HfsnBTTGuqCw1n1pJyqSd0Bnb4HhMGPVge8UlkbyQdPueK3fVCuTmxkBm8h2MK9HtrkXo+gGSMJ4GlXfC3Crnqi46ojXqDKLTF1qHXzA3Jy1Q4ikA7UWmJjoV8dNNX20T2fqwCcyX3wf37E/MR3/wz/fPiVC8MTYT3AaglfZH3jG978+Ew8BExEq8f6EzcfGnJ80CjHxMRbcKQMB1ANOanAfgeSxNOJsouUUw1nRKwtTDLlleramNTkHl71tJh46NM9f68n7OBmjDgZ2d+3P9eAtxng1+wSwEor3gpKTB1qM1BCO4wErEJ/8Dlx6GWB/TOPxAMcJZ+rCDx2/T//yZvMf/7T/828+2d+v+vVOA9/9BHzE3/uZ82b/83bytejHwfAD4qdvO2kq9WR9JIjYH5CAsLZMp8ldxw58+hxTKYJZjKcKo8yN9E31CapeqUObZoxZR8V5R35gOSQeA527dNygqYXldXwlQreP7FSNfNZ8apEXb3DACEro4jRsBIOh+5E5gFrLkw6k+FiQ4xHl92uIfCdI3wXBr/+eeqJp/hlhtgdOxs7/Ps+n3j7J+0vltYe/XycYO3kqvniP/Ey86LXPd9R8kh6qtV1xLeX5FLB0pHhyS3bDlPv9ZRFQk7TlBegQlM4D7xQfxK07yasDFY9+DmEHUosya+8OjNySXXr0g6vWnilZFl1uJ2K/Y23nfgvP2vl0o4n9PlwnzlIc1qGEr4mOCPuw3MyNqPe68ZgKmPUkeJdUwOrRKaOks91AD77ouU/zsCwM3Adms/ojpKOB76s+vV/77Xm1meEj53HSHpsRBf6JIT/BT1P7rSLA3TKyqfMTjlVU55JhapgGVZtwjSmfTcxyKDPsMrf2rIvB/WeSUSksDLawW8B0YkaVkpMx59amGqn4rs8eA/j6vpSeEWhMh2WOQpdQg0ENmKDtoHJ2wYKfmvdEWMwUWmEY/UkHm+lYO7ostt1ANzTwQtK8R0hvFsO94iOEk+I4zefMKdvP+VqeczaYzh75u+a0IZyjCn253/JLcRhjhLYnmzfK7YspF5Q40tv2AfCok8pSnLCygg0pguzBJ14qIx3HeJXSoMvlBYSD6gt84wuoQoCR6lXoQTUVMyjIz9ENivGKqwYXrShc5R8jvBpgTu+6Gn8QtUSsmO8MvCroKMWScMmInsI83Ek9jrt9pwd5lA2H3KaYWiBzpgFI8ULmGiF1JD88Vs/YkNbQmLa3XG/14P9I8xSf6ukgtLBvjEr8b2eSuLpQrdgDwZjKMnmGyjtnMHnYBMIawkiVm5ciwUvWjFnQceXKx3hCNct8Cjsc1/9bFe7CnAHDg4yTIJ4P94SJyNLL8xTCXKrp9ZBWWaHnM4QJmFm22ygZQX8ugy6D32t+xs0rHpwWZp7t+XGCeAvb2SM9+cKVj3MAtEVQjRNC0hQ7E+CV7YFqTIphyrTIjPyMnYbhjrZgUhDZ8A1/EmFIxyhF3d80dOrv3SbHetdB0AGBT1cAlpeoo2SEBJRD3K5p4bekDuP7UmY2m0ebKBlpcML9V3ysAFAfe9XPZqf7WvyY/8P2DP8YmF+mg7ZJ5N4QEmpBcT2J8NaKdvKNjCLWBI2Q7spJUCNTcaz7JpOAUfJ5wjXNfCWYfyq7bUGP2hKBxcObKyA8AQbLtuUEszYez3143bgdh3fsVCHEkS6bNfABlpWOr2wWNqHuEfjVz1VkAH73wOX2/Ajccv4vZ7M93ogq+VbwHjQ8u2YIniHw2pnXhB7oc0OLw12FlN0CEfJ5wjXNZ7w2bea21/wRFdLkR33Ew+GMpxB+sCUJeaRX+y9oSEJyYl0kpQqMVVYhIFbl5uOudjlhrcsjfBE9tCHsrphTepnfMk6+ZXSoK9tHHFiQAUP8eDrDLiXhESkEch2QPbzZLA+n9J0+I4HUx2pPVAaXjpEkjB6dDIQtaPkc4TrFvzLrX/2le6Hxq4CRh5IIo55EAlIVkPZVU8BdZcDtzu0iW2YCV1GRniKRHV1b2uvYsoyksRAddCw6uFf942eJI3Fa4BsLvGMSg+s35N0ZgVHa4sldIgkGKkj4rHaUfI5wnUJPMX0Jd/1cv5piRJGjP8Z4LzQR48/viS3TAmIJjp/cp5e4WH0xn9Y7Zyf3ZalEZ6UKIpS5Sfctvd45cNPuAl80U7m2cRDH3i8Gr/VgxMDkWF5W+wCy45RyIH0+1Y7glFpzQHWOzyMaQvCGGHWbxX5o+RzhOsSn/2qZ5lnvfwZrjYCHQdHFg29XrPB1SCa6DgJ0RYfvG17VqLXL2OE8Ci7NdRmF8a0oHJau1j1ZOEm81iJ6iAhRDwkgifcZNUTi9bANqSQQXd6IP1x650Oy4E5VDrtjwkD6JBPvDd0jpLPEa47fNYXPd18zV97NT9iXcLYY2carJfm/KoQnJUTWHWJ6EhCtCLCEdc25/zy306UhDP0UXavFlxQ+IjjQ5/ube+b/cyqx68iIiXsM0+iCn4oDgkIuScSrWKw4T4jdKQHho1njOcOBMF12kYchxBGYFITKh10lHyOcF3hWa98pvmK7/lSV7uWmHCERgeat6AORCQhXg1FSSrGBO9dmKvd5iw2zltRmhi5VY+fzCNFHRaXaTnKbzOwpG54M5H9sbBvMBtrpDI+vDlq0UJfqxACb64+KySExJ4m1If4UfI5wvWDF33dC8zr/9ZrzIlbjjtKHsUDqMhooKKnJ7IW9BNYWTUQHYMfUkAioiSkL9VBYIRLi06F0XaraFkb4Y1Es9LUL+hT3OfBK6gkYUP2YMFpKEWmx4aIgEer8Tqd3ldWsR1bVIXxYDutATTWPssj4SDxoFyHjcGWBR1qWbAttyUoMso4Sj5HuOY4/ZQbzWu+78vNl/3plzvKtYY9ikYeSz6JFA/OHDDB0qSaJqH5YmxbrifgQYOwAa6iaFyMG4k6dSpWPb0ITMT2InD+K8gweXLiyQwE9oU/7ZUOi2KTwoxomskxG2MZ7KPkc4RrCjxY8Aff8C3m+V/5nK5HqudwLB0KWpfRqnCNwlk+v0+OPoPvsdTQ0SHz7zOyWDXa7xETZEnar3rwpVLqX8jxulB1jaXJnxBYdWDVs9i56gkkWuLCj3YTyMzqTTyt3cwG3UrHKxEyeiwaFFL0jSr0sl2BF8xYNAXKgNrRTyoc4arjSc99Av+O0TNf/nRz6zNudlSHytFRHevuhLDv4FJIDFoC+yo6S8G/2UP/Wc+SBkTEJMZYwQng0EQMPBGUgknerT+ANbxa+4y5DjlPRUCFWBg1Xgg2gy3TBCQeJIwLj2yaHfykNvoXwiJLxcBTUBmwfmKFL2/GXyrVSFQLtjyIz2FEcVs1+ssNyzRKoO0nYo7gA9bCCiQ2cNQaoiAuyEc1KNmUA0AyL93yUVKL9a568rGPntpX1vPZovZeCvpaY1wXZQEL12vzgDk0kcGThjPGJ+/0ByuaW55+k3nGS55qnvdVz66+p605sBsYrV5QAHnM/uK2ErJ6ipi4K/i3EEs2CSVgWt5A1exE2GgwPSUtVOjzXAmdweMIv+mzsWPHE3cwPu1HC1DBpUxeTTcGN7i1FgXQppSSkGlCdaWp6GtgVsIH4T4jxOQ41Ib63HFVkg9eafHkF95ubn/BE8ytz7zFnLzluFm/Yc2srK3YBISBRWGMDKUINlOzFbFqXseFVLVEm23nPAAzw1lKBiPd4Cxz0qDD7nNFgN26NmLfLq8tm/VT62b1OB6b7vAwMu4Yo9UnNTqE3PvuQSI6xb830thnc2hbjHZTJzqNDVOnwhIPJW1S5CpuRA8JDPLadF5NSxSQU/RqxGR+h51u5CNNwC4jvy1VEk8jFaVp88CUlh9a8sF+xy8FvvB1zzdf/Me/0E0+1wEyrZ3ScXnMz9KnE/pGkBKiotR6B3rVRZf/PnhTI2zirQZF8TnGloBs4wSE+9/OtpYuCHzPHsjsFhRaxqgpO1t7/MOKHkRjNa2Lsmo2ijiRwn0e+Vn6bowQ9WCHIxWjmGsILMd6JbcZ20wi+ZJKL7i5tjgzDiX5HD99zLz0219snvcVzzYnbz3hqNcJotbOqyMt5mvt0wXBzxBnYLlOBh8l8cIBWVNhzOV+j11HelLV4QAkHqzwsrcUAoMWPkYUZr0NQ2DzdAjjfjpeIYN/1gf9DXzPy1kORYZHINEQRw7d3zPmyqUds3lxyxFJDQ3TugU7SD442cXrdPSDBlW3NWbMsx1McIyqYUKOr23wFIz7NnYfVc3FfcBQ93xK0NN8dalev38kJHwiFN8MD61U84Mw5pl8KJLbn/9E801//2vNsRvWHfE6Q9TaUY1vYr7WPl0wOvmMRFVlgj1GomeTD2OETb6Z7coJanZG+BgD/OQADmmexsjHMDnMwWHWRNtuItERCn6zBysf/LQ88of9VVOC1s3YQdux6lk7vhzwmy5rAjGPO9URW4ZzfL9TiOnNgNgwVhQZ9nIWrOMUG6IskPGRc5s3lQ0wi46U2Ye1E6vmK7/nS823/eOvv34Tz6Giv9OPcP1hyt6Lr3JdD1iioPg9ZvSJ26mHi3qvgZtINDrarnooeVLGQZlPjaUdWlfK4OmNgO/16B+ia7isC8Q8NuuIBT2QmVXgWxDT8atiGt2CDug87sBeRWpcJFrSHnpX0OkHim6bS/JZWV82X/kXv8x83jd8Dt9gPsIR5oXqcO4Y61lEemrNMw40uxc1p8Y2C8invfDGobmf/rbbzBjZnlmav7dH6zaeNAkUOueRnMGoWVDBqgc/k40VEFSacdQEmsohtL/ilS35Zqr97+XnDuk/jdowwDVNpyJx9cWmJGG/tSnMnHzWTq6Z1/3NrzLPu5q/sT8n9HXuEWaB7ePPvJ5WJ9bjcWjdMaRRfGJDnNgwIdu3KEwIPBtvuRFFTlmFgdh41UPJx8eJD9KDqlcv2aEJFz9nwffgGr4mgUNyhiP7QbXk22Uk5IWSyMxg4yOtu3hky0HGEbqAu0FLD8Q+ONWZkg/OMl72hz7fPOsVz3SUIxxhfigdCDMhMjp51VObxA8l8AZowmm5xYTO331ZXLRfcZgz4H+WpkN3d9c9GCHhEVFsBhFH4WO+tb+ltMgPXnShJpbl5RViaq1nx+aFPpBRGK4ZzwUFcWwuq/gEk9kYbF6UHIRXgojrzWGm5PPC1z7XvOQPfK6rHeEIDaiBd31ghoBaB10J10Mf8IRCiYhOHpGEqquhznibYi0BCmF/f59XPjoeqAVzaskOCS1S4sEX2Lti7pHR0NfRXBEfiZmSXdIvsXqQ9SWoGUZXxrs3Y6wyApx8RqkEEaVNFZNtcvK56amnzRf9kZfyAD7CRGT2iJzAzLyRuUOxm9lKqLCuC0yOr3akXotG007odQs5LYuJHgkI75SrJqEKZm2y1V8w+7zqGawFceKPEKIw+YFdotm3GTjiLEhspEZLbvI9OC3xQEe2LIQBp6VNI2cslonBB3g+ArlNFGz0R9drmJw5vvA7XmxO3Tbf7/DwTcLD3tS/sJsKG+nEW9ZuXrSy5Wx0bnictrDhKSF+3JbOIEnUbSjPeVM+62jx82hqTTM7F0y+ZHUoMdM+cKUWqnLUJJ2EOBE1DIPd5bsixCxyxePWrXrELg0z3lq9jctsuNyG2DE2m+gQGRAGwDG5coIMA3uHyfhTVAyhxeBabwESQgWdvgX+OEe5sAXIEusYnXwQ0FNfdLt54dc+z1E+gzGyM69XzL0ZHQYTkeuhL1UM/VN2hNoBfw3a2DPXAl1iTkiSj14NHUbTxCZ5o5OlfXsy42lom50AXQgWqgxZaIDPqx4t14mkXQEh5Lb6OnbvW5OLCzS9KUg1Ik9HFDeqvJEDX9Zbz6DSwXWI5zA6+eBJFLy94AjXN4bxMHFkXENczxHLZDwah9CorkmCMNk1NZVXQ0v2J6il6bDXbTMjGOhjAqR27OGSW9S3aJ6nRHZslaZ3SlhLS0v28ermKpzgRPCBLfAYqA8Vka1CCbA87hHBuDiQsqYJmsb7wb71Rn9iWh4QdFsv6gabGJ18Tt520tz2rFtc7QjzxAz7kTGr/vxxSBHNaHYm9XjiEFzlzh/WCHV0h9UQxJc2+beGxtwbytiMSVj14ISWn1CLzGI1ZAv2A3wUbXWwhEtuXncgF9EWGST8L1Y0lLhLEB9/dgQhGCFaAkzIFqBmW9rFIMGabA5j5TMYnXye/uKnNH/m+AiPb4w6CK4WghgmBhQcsCMw5/azuavcp+IOkyw/rk2rjVGJiFAKeW8v5YCC+0AasRRWTLwqo+TTswrkE3tXBuL5VwPVrsSjjFixmnAEEmU/ja2EKr+mGGOM7BwxOvnc8YpnPq6fcLtG+2kSDiXWQ+6Aw+7fWexPftBg7uhrxbz7Eva0Tbkv1JOEcrFAh1c9tGl9FOVhFrLMNJ8IVBT7tDBaWkYShIIjD2YC5PyzbLKRXyn3wBm2H1kvHuDG21h06bYM67ZNCSJC24REHG6jswgeNjjCrEDnT8Qo1d6LM4eBa+e5jolx1Sajq9hU3qMd/kaFNKM9SUL2y6tuSlFPTdd0+VU6MTgpUUuJB92cPhZFSDrLsuqpOGGW5sfJxfMyx0vF7hjMahfiXSpNISUwMgYNiSdvQnEr+2Z08lk7uepKR7iekNu/HVci5o5DdznFgdOZJbbWmf3VQE/8kJmlnTOB+mhhgVZCS5SE3GtuOJ5MQHZ1k36pFECNV0TIS6h4tk0O1uaBWV5Z4p/XZvuBnIXI2j8hIlESySSeOcDH0ERZqjuupiAJxA2fAdLlg0kE4IJQxZLPx+/1s8cFZO9/euDwo53BQ+mgvUpdbN3Q34q/SaHMKX6YsabsSgaJBwkIiQg/O8EYhAgLfK+ndK8GP6kQcoYa7udT2uHkU0LeagYkWEw73UbyKKrPaHc84FCcqn3RACRLwz6lO4PaVQNHyeczHp0j4bBwjd0LZgqjdAReJdjY6y24lt2c9Q0ibVjUIPnwl0DdagjAq3TwHjc87aYBPi7F4dFrvSISH6Dw49UrNqnFj1c7twNCNhvwVol3GIkHqkX1GexW0eNQ+rMzBoi1RZVUTnjYhQmOkk8JmY5s74jpOCzb1+LS25TWdGlMacsc2n8tHzQIwp9DW+aNMKRCgI6MfkQCQuJg0OAES49RzI97tOrZ33VfLqUtSBAoEo2/VBpBSVkkhAFgFRNPF6yFGNfkeCv6zDDmGp8yVrBbc3eUfK4V5joIrg9cr02ycR1CdFetwXVHk8OYMf5QvcMYRGjDagcPC6yuLZsVrGCQ3InO7H1jdrb3OMEAkiBclS/T5b5UOpQq8Dbxp6LRNCYCEpWlNBOP5zcd9CNriqOxRQ/bx/NBZN8Vheq3oXuyGP0z2iPFP32Raeb8Wk6WImPdtkfFNXDmdsO8EujAUkIVeY0usU5bAUjHqo1QVqK86il13eh4nEKn3mjzQEFpkq0SEmP91jEM8SAB7ungp7HxSAKmFPudngWzs7lrNi5s2X7HpoAa5FbXl+39HipDSya5UJoQh0UCTCKH/D3QRMEC8YCdnJlD3hpwFVd1n/Ld0tykCx7mTrz5AOz4cuNgewSK8hyNLY612yVv7XsxKmgV3zIVQg5HyaeETDPn13KyFBnrtj0qLsehj7ldOqoEOrCUUEVeoynWaScA6QxqIwwoUTxNVUSnyeGQcTfjO/QgMoiN2HcF2x0u+5AYCiNtAclnd8e9RHTRJh40z7ZwwWxe2eUExC8aRZZyfAZVsUpaO77iCM4z+P0hkGxZWDjikkHy9rtMKIYSNbfahlWjP9Ws5z6vNTrj6BErtJRxlHxyyDRxvq0ma5HBbvujYnMcfNAomHn10whyYCvBhg7QIdIpFIF0rNpIZRFHn5WSdodJHCpLlLyCbifayGhmwtx9zcEgHqPOAXMLnoDDigiX3+zj1tJhNlHxgwv4krvEIX3bHZfYS8ErFsfTuwxjYOvKjtnd2nXjQZ31O2j5GHbKpAaxUEGyENPMgN1acDEqcYwNseX2KPnEiJp3OK0lq1P8FITyZEV1xZlXP5UgQ1bqu4YOkU6hEMNQnaBMqPZXT7tIZmXFPeHF8tbetGiuc3Q3yl4qE+ge5kTDBHdRiv7Y1QY2/kN9qV+lo7V7ABt1HeGKBwA/zX3xzIa5cmmbf7Au5zdnlW3wH5dsWSgjqZ2NgijmvBMa7ACVGKaG13J7lHw0VNMOr5XOcuSgy19BKE9WVCnSaJhp9VMJcmBl/DbQJdZpy4PkrcpYxQHzSD7L+K4L2ZHDZno01zk6G1abP+x9nzowfmeagyaoLi4Zc+n8ptna2LGrrmhajUdJ4AJLOICFIskZmhEqh3Y9hwpdh3sQBxRwggBij3IePZrJPbXHO9Dlwb44DEQOZvE3SvfQGzYehxXSzHZrR+112I/XFHPoj97EI5+y9YPsT4ozVqr7DKQl8cwV8JBvSMDJixAQf3mzaQfKqM8XEp9sR8kHoJ7AyRQ65HCRejh8nyHiL+V9WuAahDxqXuvEp2HPzxWlFcssK5m+JAT7U3ZoHFdqQ1NCaVXLuZ7U5FQJFNk8XMX2S7xFINnBBpJlYGkyBpvDFuNxn3yuTtLJ46r4zTj5jL50Og9kjtFZ8Rnb4zM2jI+/DhutBINLpPlE5IyPjrNfAZKhNNWSRqm4+k0riBLsYIVit8AuMML2MPfhr1aM+3AOyMT1uEw+0tVXdw52zsKPfmQUyjYa1ok9OgFVxEdauooYH5nVqBx8o0xa4eu3f64eDmPV4xHtriQJjXaRU0jHBChZ05PaBGvRFpiRhFNpjmKUkrXo2xDx5zAuDTp4Z1xL8LhLPoV+OGSEXucRw8w2YODadIbHYbmfdT4rHLejA7ZX0D+DMWPjeu7zAKWJNEDGlE1CNMX16HchtJOPPprMWUXp9faZG8T4K1sVDYHABhfmd4ktQeDMIeg6K/C4ST5Jf8SdczUw1WekVzejuA1//NPFc0W/727MO8QK5usK1uwRdxWb8GmD3sTThYYpTkLuslwbs8RFuqwOP/GlMdq6TA+3/Fuvp/GI7Eo72YbbPPiYL612JNYJyDrLYRD4jE8+SX90ddA8ETqb1XVdv9+6SM51ErgOMLRmWrv6JqgWPrP6NIsRTYwvrc11zI3ZXSTLCyHMelm9WeISXWuYa/CXd5SFJB1vqgcZWZCyJvjpuzHGO9EyWeiCz+jv+eR3gPu8anAO6WOSa6XU1u8TzrH45Y41FOyF5D7/QIM9oFvQgsWHP90Q6Wo/dJt0gvSBLyUOXzL9DMKI9ug5Y2zimc/JgCDjm0g2vHJc+OmGC2c3zeblHX6haQjRg6GgRrFj3nXyzEhXHCIPSaQe/+qlnmYPygMos7JvWyOgRlt13p6hjwtmA7Izbz8Gzmdk8ilGeNVDHxxO7jalVzcRcQvCNRt8eSI3DitKIUvVao4IDfaAbkGLoZ/HKUKaJ4vSZDclYCo+3pPPLIkHmF/yafvmWDNi2H+bGztmZ2svc3IyKIg6Szgxv/JhMSs7aCh5gTBjNzloQ4KkvyBEW07Wo8dZAcpu1oUybYuD1GdU8qlGdk3Ctk7571T/Tq+tHklkFHpCwADhyVKjoBiS2/41emJhdAs60eHPKEBjrqsegIqcfPByUkeueLgOUYi2qy/oLJ4mQohizvATe7UDwHTG+SN/MuQkmhhUezWcJP5E8xyPDWyengYWe7ESmppSGKmpjJBDTlaD43NCLdlZUIovRiWGz4jk04zomoRsnfLfqf6dXls9ksgodIUAIWyLOM5o1GDgVBRDlqpVdIAGO8QIYRadMD5F4zCSDxIPn72PD6uIOZqaBhVAaYjgBOZgb9/s7Ow5ynjgTdLzwZQes5fB8LMNvGKDCXdIaIhlxw6g6zW5+QLWxdvhYNQh5hqba/N1k3zE6pgd04zkcELtgHXMf6fG4PTa6hmJiNQdQiTI+Yf+5IZzWB/nsMEe0C0oovR3hI4AKtzO0uDrtlkQVJf6J4QXAPqBDcRcenhpFrCTvmi1lPQhJmy8C213d4/7VgvJePIkV+B1EsliisHbwJdX7P2LGKXdpBHY1kZq+cz1I4uzExvpAeVP/LQ3kil+5QE0bidtLKvto0w+wstt8qEFHUDSZGe3iFhewHr44wQiORTLZmsOFUiM539lN9CNzaBOsiF5UL4myadkIY69hK4IZg9zAganvjQ1DtLrU42k6tUyKoLxxJyK9jvtjgcYIcxnqaOMD4Da3Fc9Goo8MUSPRH9WgyWw3bZxkcD4QBmvbzrYO6CkQ6se/xMEYNgPga+qgpW0l+pWVpdonxA10utBoBLrV3YzZLPuSIfViImfeUAiQnLFJidnAVie/iT2Ius5Z8qYsD0pJy/gAzQ9y9EqyrRCnqphbdDfrH+lXzAVkgcjk5LPKIURaHVDt9/DCrCKwakvzRBH317JCCnSKPcFYZCx4RzUJiE7OQyIFAt2BA12iBHCU5OPqFyN5DMhvABZ/VmN5sA2Owy7LoMkEg5PyPg9HqJjxbNHCShe9QBB1fmypuzYwr7AqmcKIlchobKLIZfoRkBTsOHgRFv3drHRiojKsM1t5f8QYjGFjPUcyaoGYFItOK/jhOgjFs+YdchzQv0u51mk3MHW6OQz/y8mDig1Y5THwwuvgMFh4npiLLMmnkluy+YcqOYIkoTsDosUM3YEFVYenQr+dGi0A6ti22PrWXTZrQhND88jqzuLwRoqA1D6CefY+NoIpg9MxEg+gPC3adXjocwFln0lTD5LlHiWl+zltzFIxGNCYR/3+tGnXTz+CWg/EhB+BI9/JI/I/FaFwGbGQUTiaiY+JmXUBxDTD17aDwXZjGlCSg3Vq44d8pYFMVcvXa775DPK2+GFVoB1mHU7MZb+7s0IEmmS27ypCBnjbocFk3fGFlgge1ZuRzt4VsZODIhgQvAXG5xOxXwAjon+4HscRbCQLQaIVPTElACTNH3UvrHeitm3UcO57G1vF9hm2hYZl5xsaJPEA7r3TwWMhT1a9exsp/d6EqtMUIkHdZq3V1eXmDIGiW1AEwud5ELogMQZgsc9t/PA7HICshsnY6IzP3agqgEn4yCnPsAyDqwTlFJZYmXMOrmBk7pIKXlkrVcw2L1ukk/chNFeDiesAqyzossJsXiVLt2MEJEmuLWIFFM7qXGp4lP2nd6HcmYIDKqRkRq0aEnNuWD28KcOJYIJHXEuuUtuA0sJdZhsgmykZjoNO7FJYYxU4uSIS0gRJMnIVCFTgNrFHqBtb+35y2/F+JlgqdbMArvGj+/xgwaJQh5FsZiRiZVFuvxYoYyJEGgw/UfiwSW5/f19uxpiltMmU1mXGeP+C6cJLIP/sp7YjhQcOTHtxPLpFCg6zqBko4TB9nWXfCZZP5yQstDLxiImxMMqXXoZISJNcGmhFPM2HLUpRyAGhkc8KfEkzyUIcKEIz9ZyyWm/ZfLjvFwisBNb06oBwHBMLtIf/EQykk845waVPCKRDo1IaKhUdX3bM1Kun7P6IFYNh7Di9BeFZD4BvXFpkgA+kg6+jMk2nH/3EcLNI2ISEyFIq2v9DxpURTSzEDeLNP0MAo3me3vcT/QH94LsfTAkIVuGlWw/Zmj5tx3kZiASEOcaRM65sn2b5bjPMcjZqWHwcV1ddpuEqxCOddHpaGQ8gXhTNy8w0y5RuqkZR6nKKGSYAym1FcOztEwibwk4kwxYqhMSFYFiSHGZf5M/UCcULQzI2GrCC4YaVf2CjqCoWzUaYhCtKHXYwz7Z3d4LHq8uqrkOx9QliQeXP1dX+1Y9VZGYWZgf235CgdY0y9KRTbvioZMbl4RwSdImIcerGNUsqxEZF/jruYrvSIkNL5JzXLBPACenEVDLQgqDj2mPlFwvKPfV3GBdXDVHowCVngO1CKfLdmxRYaThjPgYCyO9hfJjlQk82dGKh+eGWY31oGC26s0zDykmwmC54qPHPfUjzmP39ocn3IpqatBqGXy3pwc94VwTZAJDn2DDam6ZVnWrx1bM6voyP1TBfEpEqjs8ZFjaTUoZFMiCwYb8AXQ/1w143RpEqG/3eXz6Jp9mj8wG25/Sqx0YISoYJz7SeAvOXNOqEhgTQSjb66wFZ4AzhsY4wyJtz0rnENY1QjHuzgYNYrP3AKVxvrQkP9Nethhz7D7ArsDJQG4i1mhGmjefYrShOlrS4NtHsg848ayuLXMSWl5ZMvxwnEtSAltiLS5lUWLl2pyVLdtueLboEirj0zP5zNDgFob+HOFkZDyDD4emfirAlKZeHWX1gr8SIubYsAL5scq9cHbxgQ1nopjw3Fzp0OlciXVqKAwaVV3PzEuN9zsRIxzhshLEqyoZJuZcXHJLzikijAhlRozzxNJFFd0oEsJ/l6QxBldoNbRGiWhldZn6wF5yjBNRFg12gEBW4skbADXHSXZNLNTYdwKxj+3aJh+Johdj5UdgMD3Sych4EvGmfirAlJF+A5BuWV1xXHGMq1S2rl3lBky7Dk1mqNZBquAlqeCvt/erJ+hWzQjO4HYuGPzPHgm6EpPpHm1yyS2PmDHsy/SnCkJMinIwH6JqbJynLmmOI5XkRMOrITz4IquhJS5zN/YkoRhxmxN1EEKiUBLRidD24k2AMK9d8pFI4s4qQUc+J6SdMtLJrOIT9EeqZFG2oTiu2PSXqihUmW3bCmNkW8CQwyQZHtedHpzYtHjGauXlq1Y6XEyLvYYF++6z1kTJN8b1RiRSkVVPSb073pkb1m8Akl46UQvbGAi4oujzRn8k0eBlqlgFra6v2NUQP/pn+aLrP1toyIHdY0pawVBKUsRuHcj2r+6B3AZcm+Rj4+vHWPkGho4SpJQqRooDI8UdBq1Af5oxVisd4DlMdNOFrG1NDARsRe7RDOiMkMS0pP8ly071eaPptiIwa8ihfsNatzNa9exBmDp1QoB41D3ZtQ4jQujDhPhiWBMIuLRpKIdURK0WgiQhmFlaXrJJiFZEKIPGl+zGHMQZtGKoQXQHfX5W0W9A3AMlHF7y0VHG2xiMla8gdT8hoAnxZFWadgaBCS4TsI2qIcWk4lifqXxoT2N22+MQ6FPFX3Lz6PTgxEbF44VHaRHGyl8boC/lOyylBFIDdLDyyc2nM/XA6Fhy3mAk3DDF2rJDNUhiOlG0b1R7IO8SEe4F8WpozSYirIaYh0t22qgKK3aGqmw1SOv0BrBuRjnd5wmhiPHJR7eits0Dc7BTDmmC8aujQhi0Ev2RBiHOKlW9gTnqIHGCqXzZQpGjGapMh5grxSB6hhWT8gcMHSAlsw1MVPOYRf9a+q7Brnoa1jNs7Bv/uHuEUbH2ClflEES8hRgVk5Iep5dCkhBm62VKQmvHVnlFtITXEFGYSELMF0TF2L9vITF82W0xvH5sZA64NpfdCNyWXGsFMzbWd1qCMqeKeapUbQ3MRGxkDH3iyt8Y+062qaIExpgHdOJJL7mliO1n/SWX3Pqj6pd0iHx06Y92olDRTVkDpTTxNEFKmPRwv2dqQsdZfYxZuoAxqTETUAzUMuReyOwNslggg5JoFpeWaBWE7wyt8veH+Ek58PhEYAC6It56ACveUiH+jkMyD2f8miSfQlssXGCzoKw+0fAEtaJK1dbATMRGxhCIF3Ur/jqQ1xlDddBMV67KgxsJxPJZfSLyL7R6ZKWyGJWYMxinnkrP6N5DT0KyAYn9Dof+uz2tzsmwoYJ7b4t08q7VO9yGGK1wdXBYYcEub9RpSDZ4xRRWQyt8b4hWQ3gvHvicpKCRgRiRne8g5EAtqLQQGYxBthCT+LiqyUecFlFltlG23/ScxwxqWVRtWeZElwEC/aKxyF9RLgOS7RJ3QmNMA1a+pJXSY4qvZ0z4HzcbgVIkfaBJwJWmYrK+yi6qSAgtzhIfVj1TAJ84W9e7Y9Z+qiIybu/dzIBisNH+nmOjcqYkCR3s75vF5UW+J4RLcst8bwj30pCE6kGAO8cwPcQuJxxsTB0wl+TjnTS2KpoCZdTtTzQ8Qa0eRxtF3RFG+0StlJcdYR8oi6ecpmklMAzQUCu55KZ13KcgrgeQ0V4VCjFCNMQURdaZ7HHIMLIdJsg+3tqM+z1jL7mJKJ5yw08zAJNanVPKtVvJoWirkzxWgbTjrc69//MG8aDHAXUi3zsj7/LgB96cgEQ0fHkVCcpuQPh4dAZVJkHp2w3eVV1XChj9YlF8kWwemHj1I0BdbYY4J6g2VaoCatDGGBFLIprVtcSANcZHUTZiULXLrBPij+FPABxWFiFPap6aqnqAhQN0iQaeFcsIR6SKOQbzq0JjfRSM5cj++HHMEaoeVaaGEqQiajh+d3cOzPY2Jj5LD+EICd2+QRzvcZOXiMYiemoogXUS24ScspIbijnlkcjadUAcc3BRA6ZuvBgXPzmOEwC4xPy8vbnLK1L7MIftEPl5B/zwnX+xKfH5M9dphQNdqDJ/D1JE6GrvIHRVL7tl0RVwCKiU1ercKiaqNlWqApXE04ls2FmjlhiwsnJ5lEUjDlW7zGqhpsIggFKX/QjDCqqt3ZJgflVodh8eCDveGHUL3fZHQNu0bzRwlQBlz8KRl4hOiZF1corZWCwgPqhM8VpGYq0Sx1xADpF48KN768dWKMnYaRyrGSQjpuHxdfVvgVY/S1gJEQ/3hxaXl9gGr4SCBrBxVx4AilCTfe6frKgAOtF2bZNPK+AIugPyGGlQY6JqU60qgGFRQUdMWZECEeSA1WFfwKKd8iPMMgb5sZoKFVW5K4ExXxRzDHy0omjxe5DamIfVAU1r3e4GQZmTMPns72FzyafTFsSwQcfej2ByAJirodPVAFIIdfIWRtt1KOpNNdiBAxrRSDJ4HY+97Oac0QfquMwGniPZzSUalPlJOUpA/KTcKslRpzOPf54WWgNEP4uYiZ1X2jK4RsmHIi62KI+2+EiDGhNVm2pVATsQimgYB7vpX2GMbIy6bsjNTShZODn+qOlEArFoS1X49po4UNao2RL0yBSlHBknij0HZx3KR19QMyF2gZ9OyCMfly1SQ6nAqx5bHIWx8lAIdfIWRtt1yOpN2pcdgF302cI+rWIW+H5O6Vjje0CUgPA0h89LSD604WlPe5mUGMTnBxSOYzUkX161CQyfiXnsNsQBhmdOb/A1SD64NdWPoJ1FtCWy6DOexUQ1h9kTTxEZZtZbw4fAi2XlQ2KnSS/Y1CaSjBVwtURQ1wyHmIRLbsVeJ3KBEyCQ6VGIQY3hxBMDR/ocMT9r1hL/VUZ5goq+T1KEtFltuCQ0FoG3nOuMyVAsH69Qx0QEnbw1hypTwfWH36pwXun/Mq1ckET8ikcBFModDPu1Ahr1RFigxLJ+bNWsU6JZP7bMl+b4F3zdd7SWVvBbQ6v8e0P85VXKDJywYLEnvmb8Ka5S8kFvyAa0I9XSZfRJZTFBTbx1qRaE3O4so2F8jK71ldFo+BB4sQ55Fum0C4SiFUUaKjG35SbmL5KR2ojrsdeSGVCW7LfRwow+RgQSi2I+w6IHm5vbFCLpoNPtWTjOuvlEIBNDaR9lRJvo0TksuwmoYXxbBA3Um0bVcMjEKj4GJHImkEDwYMExSiq4VMeaJLhMJwDHeLVDCQgrHV7tuEt26yv8FoXsq3xyTiZizslHoou3NsZp9EkVMVJ1tLeCcDYRaFTYzRgiZtFX1ciAttggwaVeuyQXihYUnVzp4o5HpB5bQx1fZsS6J4sCWZBlF3XyDFDLbsqcPCJ5V637mILSCLJfLO31FkthMuOk1YkpbWKdQDG1oild4ZBCakUBRsSQlGnjpMPEgZ2gaBiMkInEPTw4YxGrO9cWxFzDl08pyfgEQkDC4S+o0opHnwlIooEP3A9apVWS/eXVJbZpk5Q1469hheF0Y0LysW7zWz/Ga46TTjBBfZK3gkIz8VTQ1IwEivKdIQRiWZ2B2GmS0S1LgpCdNfEIcKx2+1YYp5OXrtsg7pTAIowy0SUsQphV0HnukzoSE49/nU6A1HBMgUrurL2EbKg5ojKZ1YnQIxMjqwO/smXQ5aco1B+lDoM3Fw9etbO8vMQrHiSbGNiX6X60sEmIjkCS4bdr494QVk/0aX95lTaVzPoQCs955dOD4jloBpCUbQJGqmvxTpUQGSVrq8NaQaSpqQSsLylFaBqyCMSyOgPRlzpss0gil1cEFZscTC2IfA4Y4EUbJSVCkVXRiTFCtBOpxfn7AHCMql5zTnDxMnidjneuonA7LYzLJi1MdLjsJuoa8T7KiDSR1wmpY+1CnnWiAON4Y8R+svLFYMZGGYH6GckGD4Xgh+mKIDeIK5+AhhjsasetlNZwbwhPymE1tGhXdsRL9ykI8RbiGiSfFsrBdmFG9cnI+LOkjkAKImOaMMiO0QoRaGbNTPPCsolC3gIGcZftDiE+YOizcHJXRJf/AKlG2wZJjHcUID3gZwdMBmYjH8FTbty50UYoxSXf7WlhVLPEp/2w8JXQUs5uLaJSHK1WlPTagGZdW55Ea2F3Z48/eaWZC1jZ8CcTjHIMnGSw2iF7SDz41dU1SkIruCTHJxX4QUGMj7y+h3Nx9ZKPc5iHMIsCdcyoPjMiv0MoHQEVRDo0vdDga5qxiqaClUhkG4p5dkNpBMqWLKf4LreCYjWyLDMl5sQQhWwNLwWEOlMstJC1KUQKHBMPf7fHtcKiLy6cAGAibE2cVXZDt4axqlNcQSenp3vLIxGse0S/YYWCiX5vjxJL1qjt472dfbNDyYefdsvJKVfh5bh6DB4kJvd+sJLFfaM1XJZbpdUQv8oH8WaSJOqKdrjJR5zRFvklKOZUzKiuMclM5H+oRowSMiKdmizUJdsQyLIToiUUyEV4diCXVwI1GawlkBzL21oRfNJHW/YgiNBjrwWxgeM93gTexwzOJqtWFLMsRUTCwaqHz5J1gxRYPDFkEw4mxdIKVMiJ6hR4I4O1kt1CODPFAZvxliBx0OPxgBPP+vqK2d3d55UNf5dHAXXwtrd3+VFqnHjpn1hAKfa0S4nKoieGFNi3fIWBGmrvDa3wagj3mvjYQ5JCgsuYP5TkYwOy/mQD7ODTlAmIjU6ENjPJlFMKbQylJjJinZosGMoWNCsGwcqye0312q7ICVikQ45Bcm1RGvD0F2eK2YNfAXJdrrNCIRG+Sv66/WQxaPrSdGMJmqZco9Lv9mTiKqD1oEEzhpxA0WR/XDHGymvUW+gQOEAljDXnH5M7VhSY0AGsMDav7JgdSjI8xu3Eara3dom+7VYiK3yZE8kIRr1dV8BTh1gdgeDUZwLm+336gw3fKeIn5ShR8uPaSJKIIUpCo18sujvxFerATI0cFeWAiWplOIOp3RGelOjY+MK9VdGewsoy7ESeoNd+ucJgCv44Vihhv5Gtwe0XEpVDecBS8HeZJjz+Mh5THFQFxYBXgwj6cByhw0AiMuqQs7KpDffZi4x804Q71LELME1sbdv7CFZx0A7sREYPDuxDBjhrz0HtyjpyAmpoeDYXbK1lU6kzWvKA1umRDxAo5GOMYwLQ9/iCqL2khcl9gRLPHicbzuk8xu3lULzFesX9pg9WRxsbWAVRIlhb4lUQZJCskLjw1CJWKZubO/zC0eIl6gqqQ5nMSWL0LzWljZMQeNd18umMbFQDZkC+p0Z4d6Jj42X5RKliZRor6yMrXzCSUVdIlZiCP7SlXCBMPrFMuj9CwnJ0WUKzUUzUuxEZqiDL7j7krFwi3auuEek0TUDACWEC2d3f4wmPJxOmW2ZgJ6gA9pLb8vICP+6bazb2boacIiek5hNmD3+aNuOpqCUv0Hq9Oh5eoXBcEZK4aKLG5SxcRtNTtVxS29mlEwIiY2XJ39khYPUBQ7j3v7m5SwW3cqINu2+XdGAXl8iwX7a2d/hN2HNPPgIyy+MGMVHMkoiuz+TTiGhUwHNAvodGROFEx8QdyJYrKQrspu9E4PAST1Cj4ZSaHCiSfLRMa3+gBD2sfAJRV5k+ggFlMTCeIstmYkORYWXKNkZC6XSpQ8gJYuLY3sGZsrtE43ZAYich2OSDty+XHrHuQk4vmktYxP3JutHyJBBVuzBFx6MWm0LULOqzA1qdUJKgxMIrBgXsC1lZoG8hG0gQa3trj79gin3HKx+i4ddjV1aWrS79xwpoc3ObitbWGDT3aWSSw6U/iPVQ7vlk0WoXGiFbBM3KsA8F3l/icFwUPCjwaatNBLLlSh4ZdodWRuAqJR5UaFy4Y8ANkVReU0JuGclJHClC92okHrAq7C4UbUwxrHS61Z0g9g3/FgxNfDxxEB2sHjsY+9gP0GtOUrOC4yqMWz0WIoEpYY3WqcWmoMMEeIKmDsSqJdeBINnv89h7LbEEkgtksB1z73TDZTaseABed9CGFQ9fnlYGYlvzArtEEqX/VyX5oCFxxzLAkE1BkyPWoSLwmXWeEIpw+7UbiTtfSTgpIhGpNrQsEqH2QaKRUVcIuRVWExBnlaxeSMRY4wPPVpmNpJNV7UZbGxJVqc4Axu2BPjRjK8K9+8spl2JbIDL3u60SbAkvEeWkNRXdQXcIOhEJp9u0wlgdngcmeSKQ2tISvkMTJoZukA4meN5/mPAdgjJtOEGw90ZtpAP3cHEoyUcaUGxIxNCyinz1UQyiLzKv3hZleHmuOXhCwsnDiYh0h8aASFgGXxYZRt1XyA1qsSLX8SdmWHhqwk51UEPi0fPdbKsdIHKcxJElTUJlD0xzQjqj1SIF+12Q8ujQfa0lkHRaT7l1ASbiTaE6biNZQVG+grE6Vr5PKxsmEatvKOgAJ36bASsgISQfJVPotnFoGJlL8kHMestCCagib9cv+iIMpDoaVLTKxCI3hBPrlE6hlKyNipWIZeUz8MSBW5QVVJlZk1XgAOIx7wb+pDPGGjL2ulw0hMCu7gMPNExvZTTnnAYwceGS2550asUYWJrNKqSD5DPTPqg30fodYb/RjLnB+pjuyV5yW7TJe0oHQoU2uSeUg7z0FBtWVxXRQ8EhX3azTbMLuqGh1wt0TGFcKSVGotdQSeQ1mFHkBoAYbyhb0ng4RWujYSliFSU9Y5DIympipBPLZ/U9Ui4oOH7kklvpmMVB1n+cjY0ig4oQx+lLMbg1dpOXewYoG664rCNQXODEw/cGKkjZiNPer0hj7kerDeD3JWyCE+uUngnexyzOSBf9h7GsrpL1oUO+JjJpj8mu1lsDh5B80CzZBB2RHDJ0VHF0NY5GVqKikpWPwcyqxGCnLtYHsmHtidUCFFuKRWnPGCSyspqodFCMWboeVoCQIPJ4tJQTC238iinA1fUGJCaziKRUVXzOgkFfSghObw5ZR3nvoOY54wAbuImNJ6G4NtYohc+rHlEsNC0HaLTcIap9SNWuqTb8HAaGuFstGJALE+cai3g8vbMNfnyj7OpiOKjTJucxXoc3ECcAalNV6aymv5cI6aPW7XOPKY/wTUVfYwpSGTJImuxbohmqeRkTZVS6PuCMMpoHu6KNTS1gNVqB8ydu8VmUBxPGnYDoJIgZGEa4Sy1F2kREiy6oo2/ByWtoChLO0vKC/e2SVJTRP9ojQVWNTcQ29XHsk6CD5XWfr1tkhVNiQBnlwAI3okUNxyymBnwHhFGzF/Bs3y/hcd61Zdt+1R8sW7EVs7SqBicebDw+KG6nyPf7RIk+LVf+HC4GF4jLFR3Ql3qc8GTvgqNJmGn4C/IB2kBl+8VSux9QLyFYGekyKeGtFHu7e/yTCNq/NohLbvhC6tbmDj94UAOnC/vfxhpHxgxbrILkZkg+g1rLwGEln1GBt6Qjtq7ygHZlP7JZwErx/qA/0jPN1la6POGURbvg7hXTWahrBwUnTchC+ZNitT0QUgJKnZAxJqiwYqC7cNMb9x/scLXtCPSoAtbyivu1RgeUIedlA6USMkJEiqmyG4e+TfsXMrxhYqeCPXxyv4dTQOyUERITkaxOGTIFLC5Q3/HTae7RW+pvfLmUaswHunyRPTwejCfdnOluQLyvZwZJPvbQv7Txt+jpk/cDX/bjcA4VoXlXcx8cC33iFTNYCdqn/2z/yuBE/Dw28AVMtIEGCVh42ACy4NmWpIAZUrP+nM8BIDh9NuCsZI3Z5Fjyw0/IERPtwL7lsU7twb/EsZC0MRGRT+JNSD7uFRsKLQM2wOkYFaBHQyvDBgkbopUBEkeu1bBDde+hK7GP+BvGhKTVma5OKQ5FRhtQFVdYBdhrxzbeImbwF7fUmuIouBQgINUC0nAW6QMH5i4fpGoCd67wWnf+TgRBW/aLj8B3CRkhImmq71uaSHhCcf07eNXSoNk6x08DhL/h7WYM34YctBmPkNghUoQc+ugzPNJr2zG0AX0skOkvMF1wDkl5n9fVQDAMMHlTAsJ7y/iSIdp0iHGkpomC/26y5h9zoxMiP2FLrEpR07BPeIzzm6l3mcb7xYpkwaaUPYZXsD7b07yVi/1AD5rcjtWloR3gsQBtiRJtXsBBlwGcsJHxmFxFd/JxxOrBVcGooBgdGg0RHGpIODrpjIlD+hs9uneAs3TZL1SIDEXVAUVGHVoNlzrQ7Xh1Bs7KmdayO9GvYFCvGJrRB9qE8YTkjhcm8osRD+TgsskAv00vbcWHd9nlOxWK+w11TAZ4ZQw+hdYFxO8KPMHs7rmzdEtNkNgdCEWXnbHgsEf8+KY7nqoS2gBJkGIyE2Piy0mSqNi82pAJHmNke3vP7Gztevq8kTQfFPqPfYpX2eAH13AMMofi6RknCFNixRjf2rTvYMNJZAkdZgdAuGAqJqMdOImw7bA/ow1aN0qiztHo5LOTST4eGUu9O31UEB4dWiMMI1ZeDhOmxWMBC9DHK+h5cmGqRdXuCKclUb7vgclxxbWjZXOEzxxC9YqxGf1o8C6iPzjD3drCeIRxmkhdQsDxEbjr8h0KcU2RpB8xmSDxAOOOnBD2uDigBGqTUHKcJLYHQtFtZzw45HEGK69YSacAdxkomGj64+Mz9bg9VxGIBO4XaCm8SwkI9zJw/h7HxKsTYGKoOFnVqtyP9B9vnV5bx3vWbN8m3ZuFCFmLNn5bRgLa4Xtvw0mxRtV8jpkxAjF9uoB9j1f6HDu+ypfX0Fd97SC05Jz/+SSfioXSIBzl1KNTa4JxDNRCqBNgA4A93PDbDQ7iDGZtlmNggCzSxLiCp2SYwOQyOv3mkKoWjM3gowQxiUkOZ4U4ODGiV1fs4YNJATK8O7v8h0JcUyQ5QvC2YHsD2NbnARwffhXHdfqT2LcE/BVWMlSF2RjDfFZO42N1Vb1iJYCt84kTFQdzkeFALbQhlz6vNuKWADiZxApuc2Ob+DYBocnoZ1m5TkXozyZs9CveZWeTTi6iEiSWMCbEiZjxFmq8xRr1GIGXlkutL7Lw4YogIW6seI4dW6Ua+itOswX0NteZGn/ZDdeBtYYux/ERj39e1VUFNZUifJidndBpmDt6MUo8ccAV5EWHAGAXZxGYJJNE3OmnKUYCaAfMI/EcFiQO+RxaA0rUNgDkAqsMsR4iT0X/0oFJqwesgjCpBnIlJQ8IYAtbEuuhb23iyb+ZuQ9lxTABubNbH9KgJ77j7mQy/uT6WbnlMUITLs7MIapYCfjXMgODqhwohlYwBpfoeLLUmof5AV7EU9AFjog249c98dMBNvnYBHzsJCVgrTwj4AcJbfoYKYPjpnlknxOBwxQ/0kENXbxJGzLoq7BTFWbxT5g9+dRQCnoKpjS0A3z2gzjF/gg/Y7oB16DlEpzW63ZXFbRMfmW6r80Xo21ODiJVrJoiJiY8JHh9XPYhtOxrioyjAxOVTTyh/DjUdXGLZHfXXoZDewLITlUmRERIoXVXy+jhR76wGqi1BbyUr4IKWKEcn8jRJ1YZVwtFT4qB4xxveEaSxyP7uDS9Psfkw1dOkHhgbA72ElCn4rQE+wUJiPfGFD8yJirg+3Uklx0CmjbVv8P40+SKQ7D0dr3DH+Qjgh4h6gFZfoiBNsyPo2wUBQcrsImB3zGuRqPovoZJQaSemr4dEwdJ8ts9TQyWAz8Dme3iMpt812IaAutF4Jxu8OWIgowJVPVYsogERQAfZJQf920knia8auQrwDj7NUs1jNFDm/kxfDpIcBLIuvRn5g3GYZT+c78y4RDA/qxxTkK0b4M4ejaYiWnRBuv4sOUIzFDbWMh86zDTNZpZY+nGIRlHMugJXkRmDQMJohtFhwNDSrC6hEkFxDlA7I62N0kJGJTERNVMRgC7sv9+jBVKzGRtznSIjAZWWfAbIIoriZuRUjQw1u09jrocUExOnlyywdOiK/dBpOMm1wCdppdIAE1CH+BLyMyTrmgacoB8bsNHssMOFzP5a7S3Okf19lUnJjxwgNOpPsy0T+bY0JwpXom4cg5z7mcG/PH9H+ryou+i45QBCjYknuSH00ZiFl3GJANWaZRqRthOrHb1gMsqZQzKiZmIgKPCXm7rTWgxJinxMYPLb/bykCMqU3mrGarWoQYgiS6v4Fvu7bj093sGUDBeNW/Dru5L3BCxTPF4UOix65ERRmxY9Wxe2eF9e/zkatloR0BQhRguuY2MbkZgTGLp46o9QKAd8sXkM2vzMmavr+Qzh/3XawITdg5zCKEIeETv4eGDLIrOU4ZQ8InEM3XlM0UnwWgjVmGiWgL91BImbcyd4e5t+CswVtzPEo/H6JZ52AmSjjP+DpMjOuStZqixHh3iSDw99634C9I5Gfcamxr0fighZ6Gt1fIcoSZMzrYo+eBzbW3ZESNIU+UzA2YTfxnvERoBbQ7mJ4EUsYvwSpwSxtpGXHijAu/DuM2uHpNLyPrOEA8n+ZBFPvh7e2BUBAMmqnFYOEsTTLUzBfCKR69xkPsIigGEjJIY7neMPfeaW5tHG5p4K7qiJJMedilWDXi0lk9GATe8RX3Y6wNYRDFwUokVD86Oxx0dwGiFBGiHfYJvaEcUYuCmxyO+s4EriK328FOZrizobVFv8kmkag5ImHV6g2gAq2Q8tixPFsYI3VCNnduaBp48Q5/yO9NkSmw3P2nHlGbJ1IXvL9kVcofjBpAG1o+t0Jhf4tXh1cB8kw9ZEmO4DFLFhPbNq0vkktu87I0B/OKyG0+QlhQhjKoZIwmMebx6rm0eZWyG558qijju5ODDB/oVyZ3XgR0OcyI4IpDQx19yGyVcBLcDT77pS28akZser/juCWzV20MnRdR/MXrsywMvPcDktk9tw+taGCUH2mBPEB3AWMGKgZNPtnMVpLMyYkg+eHEqJmuUexsvyWfW5nA7dvfcd39C52NtIwVgvl5bW+XPcWN+OvpnrRIoUATLmyNVAaEuwUF0hEo35m2vF/CLwzQdq7aV0lbZimgKhBgpXscIY2iR/TcBXX6GnvTijcEociURWMQBPe4gHCVcBfsl/9m50bnBh2y9aLXHn8FPQC5Uja1L2/ZVNxTDlTNXzJXHLptdV7/ayPZrCSVZMmJXeiMaQKKQnleT7T22MY3JYZ4RjQCFPSn5cLj0p3SMJ/3BCm4rQIs0RKfj0Ax3wvkPu2eYnLtC6xYcRDvF+9BhzPqUVk1Eryp1pvWnkNEVmQwrACZgHNDjjumW1X7IcZX1D579GI8OpdxFkB5frb66cu6K2bqwRQnnirn48CWzR6uOZVziWS3cc4kxqcFluHVxHRUBdBMSD5+gIGGPGivzArUCHU//M7utE6J4TRowPvlwQ3sbW5AVst4OHVfFSQWxf1p/4zdJRoU1QvhQmlsxCpZsM3kfjExDpNttbpLfSUpZsKXIlD6rBW+SJzZM28j5RdT6UDa+dXHL7GzsmpNPPGmW1uxltpVjy+bYDce4XISYnNToMuZjjqyMna/n3Q7Yoxjsvespxucc0ATMftktA94vmbaBdE2a7Jzyx9hBMyuSRieEPnSoiOUJ1tuoGB1YM3ofqVq75DBLJHxCWR0nM7ZTwVsqmGP+VFek13ujXq96xror9dX2xg4nn5NPOGHwOzZm/8AsrSyZ3a095l1tjO7G6hggwGBL5rrE6J6YL1yfHUryAdC8eJsFOXvFjf4Em+JdNSQOQ0ISY2uDTkEPK3/6OBzAcGRcSCErEhqD0NBMwFoS939z/VTcqANz9DyKjNFoWUJC4G/j0+fUDQ+3cNnZLAGPd6PfxvZdDXvbu2ZhadHsXLFJ6MRtJ8zaqVV7g74HnWI1wIRsAvRG3E/BlvQ56cQbDrpekPxhIRebb3Cy0R/ecjzarjJGP+22VXm23Bo6CB5jngWj+6NDAaHVfhuDQXZG+x6JbepH9PyorlJB6fhWu7+PMrJVkfhI7X6MNmwV4i/EoS+TL2jWkGkf9gnGB14keq2xtbVLSSF9lJbDjmIvAfM8uml9HT+fYNsXAzT8QnG9y/Jcfg1YobMxtVx+5DJrLi4vmVVKPLjvs0yxrN+4boUOEbkuwpiRn4zOxQ0dXi36JWNGhtqF70ytra3k2ArORi6QBFVDRaAdxZcWJyZzgdgHa/hpt/Wr9LQbxYXQZjrCEKPd7D+UpnVhiu72D0HMDDZDf+ZgqguFYzYPFxTHaIt2D3bZCLTayIiP0O7HyLAChVkHmvIbh2HH8rWDjAt7Xhg2lCMbEV6rm+CL/UxqMo53GHBVwt72Hm/AzuVt9g82fr1149ENs7y2aNZvONzEA39jmxPo+E6Leg9V3jBhW+nmMTw2kE7Y/YYNDqIgnE98DO6H0oBW8HMC99mwidfRyUcaJMkmbZSYPkSUXDcA8XjFz2boD+/DiHcYkEHTDRcbh+d2nt+6MLJRSpx9um3uGGU0jYInvalwplKr4f5pTiyHhgX/RT8dQxzrvDBqPHqQUtQ/u3QWvvHYhrly1j7dhserIXPyCSfNIq0kl48tm7UbjyV68wKa0WwKCUjiEEgNYQ2hlYMEB5fd+G0QtcY0g5kFNgEihHiM8OZpQrnKgH/ZBFEYE1Y+hcYcdhvF/kgfsRo++bimTmGaMK4iMGij8Z8AbMgECScGC5QMsQVb7IESH6k5HqOM54VxOeoyTXTB/YOGXekuiNVEIXO1vuVdgp1YbAxBvHMOy/YJHtt1hC7YIOyPJxDoA9/Z2aCks3JixSytLlJ9hx8yOHHbSRY5RkkH22E8JDJaLxJGRN3Nd7q8fzBGSoqjApoA8iv3AwH85VLgt3YlqsTp7olxyPTHTJfdPJThqWeL0nmypYRxKKvhhjRxJtqdB0oTm4TkucW+JAk36IB08hh4XYA5+zFWcxxGOSgL45r9pYcvm4OdfXPl3KYlOlE5GAVshf5E5CaudfLBmxo4dlu1mBCSjItce8CzN9cdoQtOmD5k9XlAseKLoyvHVviS2sEe0SBGZ0672+0vkrKoLY6a+rTeGNjYbHkW8Dsa52BnEsgvLmW6Ygb9gfE4IPH42JmMeCeKWaJr1nySzwQgHr15JIR+ZO15WA7uI44/4OYD7GQ8VaSThcTbHQ6PElcmIJHaeQUGR1li0cjc4WGUk4IwkTcvbvGlHZSXjy+bPTrj3rq4bdlEw8Zlt5VM1YB9wz9OR7qyn64GOFRyiJ+w3t3fo2I0SDTAkq0BjDc7sYTC9tIRFTpsWLggJBant3l+k0gHZpWSz+aFTXP81uPmxK0naHLcM5vntqxQBjAjpsZgqh6AfuC3dne3WUE7xX6iyR9JPXuz/xABf/Br38OnfItf97DEmCbyGJnaLzWovohNz5x8YFu2UuBaxstqFBlttFUVV30EL/a8ipDfxldRdQJKVkN0bQ3XfnEGhK0fMGX1DxlDoB0oC2N/YbWzc4XOpKmp+O7I+ql1c+ymYzz54VvzaBOvamXHdvtNgS7l9+8d8iCRFkuo8Mc/CxHHjjj0JuhsY7zykf3PJ0KW1IDTZyWaOORJQ/o4dvMxfmv2FeyHLSx7jD1BIObJ205YuQjO2ig415OBJ93QD9mEMRJQhx28Xy2wNUuAnYA/+OV9GjSjt4dozqC/sTTGe5d6L7K2xOvB+OQzqNpNQ2fhkoxHUyAPrVZXVRKRMKLEjgMp2HeHCXKEE4vd0ROaC97+500DtjBo+KSlwy7biI0cFrr95Fo2UHe298zlhy5Tmc70aAK54cmn+Fc5geU1+9ju5Uc3OLFz2/Lm+kG66Mr8zzPMB7kQ+YyWzmb34gkt539EG2HLjhE0htQ69YogG/pYx9NtbJsMw/blR2lf0fGFBw1i1MLONVMwa8gA7GO1ENwn7EVGhZMAnfTw4/CSjA8Zdl/Sypj8Bl9pkZ1aCANcvcUQuzO3RVQjJ/jp8pg428ontkeOUxcKwiwKlNGvpiQrSiDj0sphQtyLlzG/haQ1Ma7ERgLwaMPEq8diDOjz+CwamiO6/aSCQhEqnpraPHOFYt83q8dXzYlbj/OBorF+YpVvcl96ZKM6gXVBHJMhDI/tHVzamA/ituWwTRN5jc9oCqTACgcnPsPsYGmjoMWdmT1K0Btn7YMf66ePmaUVyzh+C+0nNYlBdULYk/ViYMzwSoUm7eqBkkMhAJjBShvfx2LMI9AaEDZt+EkITqC+Ga7s6xYIR7YewCZO9GaCd6i9MyHCwoTkU7MXQ8v2yM8E5aTDH/YT/7QBbdE+mxmxewxSexbds+qx2vyX/hTnByvGEPtIpnpiFhEWc7KHjm4/oaCPMwLeB7ZPiQcT2+qptaB9AvzmzcoN62afVgw7G/b+z2i4ACQObHCFFcPOrn3l/BRoezXgMtbOzq7Z5ZOIyiBpGSoANvksnU6AxP6o5KNEfXz0gbgXF5f48idWocduPG5O3ILX6dgO62l7DNEZq1cDQsbPD/QdgwqVIMBCX+DnGba3aCVyGKsfmHQbnh5EcoA/m9in9FI5RtjE5TycUExri8TTF9NsKx+HwNU4/3OCctbwq8ND9+JsEAuSUQMyA21XA/sQE1j7cpvV5r85QxqKJ1r45LcmgEiOvIgVuDro8hMG1AqPL6udXOf7OkDchehf/PAanrZapAyBLzYe0MEzChRAKQ7ss21KCvhtnd4DUmzl7OVgE4+dVKpvB+k1WAFWVjAEN925J5LTIWLCOnHbcd5PLAcebNPHmHChNlanFzxpU9/yparOfcjoDAYJaHtzx+6/KWcprs+ymwOS+Q7eiUd+pr9MtA/wgauo/iSjCNljsuVRMjO6p2J3gcugcthQEahiCTU2LochQYwZl0DLLczhdS/FHwVj2NTBdmrGAMW3xYEA+zirw0TJdfxxsoeOIYwKBiEpNVUcVk6umKXlJU4wGmgzVntXLm3zAwf8lA8R8R6xFnwM9Kcch5XiycWdcWLyyu1Lb49rfYAd2IPtbbwHLWcYGGu4ANi3l4nswxldr8HK+M3G6eSmhjpFpwfo3106OdnapOOio7keUUC1+Ngs9ckmTdrYj6UxwgA93iqAHR4jFL/8/DfGufTzvPsN+xbH1OYVOokj4/AdYh6erc25rHwQSLGzZ4ZurN7CYgwtWRBxQOyUICgBYeN9W2lLj03oYx5E0mGbiT1JOHYQ8Z+aQcDxB9FUgQcO+cM16O7JZVakYUQII26KZ4B24YkqfIN+hw4KNAvHBH6xdIsmFn5iC009wG1w+ucu+eTgY6gGk2GSA6ws5Z6MTMBVMxXgoMY+wr7ChEUWLWPOkPhkk8nlMq0QMVbwa60CkWEElQHS7gAkVxC/ZkCc6GOsijcxaVN02dgbaLbLMa1pm4A2kegI/CZv0PU2AnbiJ5tXKPGQXZ6taRveXjAWfYrwi7GBBIQTWhvH/Pfw6BeLbvCBkqosLvjvO8+AcigBJyM2qhEAKbip30Mm7GXqbB44jtZj249rEsYqSh6p1uM99tdlmGRCsYYSseEX7cDLMe1LVO2rOOaKLnNJi2fCPk0klx++ZE4+6RQfgFhVAmjb9tkr9no+7bdjNx9nuiCJoRhUO1pOdNSnK7QSw8slHZXHSQsYC5gAcUljb8+upIaDO4MOmyWUVBEDYr3C98aor46v8E9sAzxGoJhRFhLGE8aWTIA97b6aQP+ijXwVgE4UduVKAIi9cN0QIzZRajt8Y2ysri2ZZToGoYa+9fL4TMKxBIkfsvZnsvc4ESzqdwcX/JYwuBpK2kQSigOPdWKuup8Lt3H1jXVB3GcWljhz8hHt5Ul3ZNuuA4lIvK2dgVIKpkZXlPZgQpAt34EKpAM9DDo8wIBP9C/UrDlnVCNDCkD8UKSh4GYDkbLtoIOA4l+mA8G2o9WQPLJahXCGPkUp1JzmnUCKPEypjVcubPJltbVbTgRn7RqF0CyKzKpWABkj6FtMMjhR6elbjAs8ymofPrH3pYp6/eEwesXhDzeU7RNadiJZoeSDN6MvLdPJShwP+t5+MHCSKTTGyDirECc9NtMwh2MQVxx2d/kk0J589UP2bQ5B15BcLUx5nHsJ77RDv8oxyFQCFSAxmLQrYY6fkg3uT+GRe+agDZHvErRYCHUfOEJZB+PDtWPJJlK0A/FAR+wFoRFR91M6vIc4Riefy0g+kQrsLzWTT9lNltOIalTQQKTAk6Sixfakieg8O2iogP/2g/ksQgWUcS2ddcD0cEY0MqQYbCdASck5I3YqYSmyezmRYnXKbQgHTwzdhECmpODBvWrRlO0DLGKi5tXClR2zc3HHLNLBjBdVrkWv5a+6LDInBOpUbN/SRv2JG832EKC+Rf9SJ4Jvu9+ekGCT/XG1kw7Dhkorrl2+HCUxIC4AEwsmSh4rGPNoAxjuD+QxCfm97D7GQ9quDIDUYy+jijL6lRaT9ooDbYh9zGVnNtfw7805uYY4w+9v7lNciYAd6luqC5CoIIbLoXgaEfsDtwKCMaJ8x63ScWheGF+sNSDlhJqAJFOcaOHhB/StbkMedgyVMD75uDMmAUp8qYq2kJqi6KjIKKNLpSKEZve6jXsIHZr0mu/kgtWGM88O5IpOGOU9lzIgG+7qxsCBvCv6UuQwrAmc3TyTEdqtCDLspL1Iq57dc1tmYZkGNP07oIN0Hd8locmyiqL5lt8CWC0cO7ZbYnvoh5RWOxgT8QI6xUK4fQf/eOAAq4I4Fj4mlPGBb/fBCq2OlpeXSWbk04QJsM/gKGqJVGt9BERqUOC4nZ7EPcpMYjMF2yW5pmjGMcfn+jfud7HJZPqT8AniO4cCOULGqEPIyVvzVCr4eUTH5IwgTsuO/AkfG26IU2kuyQdLdVwBQbnL2CiPFqNUCsKWHB5gU5CqVx02wWKBrK6EOzGV1UgZdfkMSDYUVzVXrJqrMIt2SyARTPMYorhyvnP2Cp91sSbR1m85UT/7yrro8FvAyEOlHw2zs3klbWeAJwZKIpt42s2SotEVw+pCFm1fXV2mlY+b6CdDPEZGpFoPqKAGpTSokqlEMlVNUQg7QbfTGHWBnNmesC1KQVlYbmit3/YASZp2fIQ++dIubZZnrU9IPulvrzevq05pCWG0WlEBU9iAsQdPWbzA6bDvRQJZXQn7NC8vSIl1+RQsFshmbNZsFXgZK+6zAhIZpGzyWaEz7n2aNHcubXHXrN6wZpZoMswi66LDbwasVWq47KJppi0KurOYHEBWlCFcxsETmPqx7vjIHcQHXTQfl+HkwYS5Q8WYBKSh5IYiFLQBi5yZVIqQJYaAiEysAXK63Y4FHQEQ8q3sQDF4wSSrWRSTDxWPH1/lJ/bkXieIh5N8RlicuelVAwNTSj2trYtUuAVWUYMZmpsOkkA3aygk+lpWNgWLBbIFe0DJZoaekkrKIdL9Ywl4ugzAcPUTZ+6gKuj3IpCOg8m4G2k+hNKdxUwKsuYMarvoLyQePPCQ7TvGoCtAn+NpJ1x2GzldEOCnoJMjV8JSH01oM0WdEqO0n2N6Tr9HxqPKnB1ivrCv5ZQ8z+2EagLcDFVllYrHjq24N0wg+RCBhFtPCWQBB7KxD01gYopYRLZJqBooM0vHjdbIi9S5mqUlFTlEwEAHyhbqsUhQ0QiJvhaSsxAR7g8vG1SiWgGRkFRDvZQSw+slYu6SGw1UOX70pBlMhGIkQEIoIlSnktgOd8+AUGEcnK6YmGomBaYT9FneLvqLj308GZk4dRqartqOlU8flBJvBST+CQXxUnta6NKJw83F0Os41i3q5SMTap5bQClmgTpeAGvb/psnQjeuoo9VfoiCCvhj/49PPjpkXyZLtlHlbS4oGqt7ylHrGkBDwrH0gYGtiIpQlpWVHSSHkt0YvpCiLjtUAhmNmKjqRZ0KVW9DQUMISDwYqhUEumIsMZgglVQ1uCy5HRRGY+pEWoNMJrBdMowuxEMD+3jQwNEGOKVI15sjZX6Sjx1oSCfpzcLHRP8SZEgxxHfishNdakO4ZUz0n9fjFtkiQWoh1aInNI9Y2UHOMUIfBeFZwAnFbTIOuOyq8kk0x2VMWvkIrCH9lNshYei5CHmGUDW3bxDHWhGIzJMHNlttoyAo5KyNhGglizpZ4oCsPMMqinoiB8QMVxeyZg1IOVn5IsER3Uct+cgjoBa6XIbyoNChm1fsAqtO1C0hmNwrtq1v/FwDl+xRz08cyRZ+KCrpycoTD3rI1CFbiEFXtDNSAyvBoO9QkZ0fGgEppC0uIDE57Cm9zRtl+3nqeOh97zZ1bKbjeyDge0t4jN9GYXVmSj6HCumroEGaODDyVAci1A/6oiaDOfSHN0tqg5XcplAgD0iY4aBNUGQMrIDtCbYQ89WQssNDMwGqxzoMO5YIKTcrDwTEUIpr9Ef2G8Z3sg9ZyBWZGQuEEIlUqq3L6BDREKu8SWEOGOwqgzXbTgG7iPsp2rEg8YayJYUgon1Zpt/JAUTP6g6lBAWWkHGWHqBgZn4oBCTIsKohcQe7zRfl3yHB9Zlym8FU7zAeb3mE54aqolxjDMn3yURidPLhhtIf2RjyOQ+wA1scEBKlFlIVFDPLZ4hQCKHylhcpIyMvpKqZSKA5aAsGhZywHFHbFRkZVnr8BPuWIHVFihTBGbhSC+QFASOU4poi4UO+7Oih+AEyNBHNiXdjhIGsv07dErRNa2oo6WIWjoeDHIkHb9+QHc2qNV3IMX8h+zQr69siIawliFgindUoMuaJhoMJ/uX4sJuUpiHt7RBF68IoCqRgXy2HMwLm8ZALHljRGL/yiRo1t7iTDhOCJYa1AioCw8Rr/wlEJVANKh1Q8lJUpDKUUC62LBRb1GVLQERMOrFd7DPZBN6OMhTXE0U+4rC5qtuKCJi2IjpcC/gWwSW3DF/gbbjPimg/OoyIr0Q0S+xD3mZECZkpNJ/6kL897x5z5X3aoc+X3GiGwHep7DiyaqFq2RDvOcfO60aoMueBXARqfAGOnZPUEH4oF9ZmhbavtyyKDAs5bOONkeh6zjSIujKD8cNvblDE8cnHAfFiXpgcpvSkbIyBoFnYqsgIocoP9rh/MRIVIaSieThZmXtl64ITth/5+AI4eWyqWATHxJPFIIX9JBug7UDeVzRQjxUZxGAli5xqgEBgaLEiqUoIfi1jhc8siNTFxqFhSNhZkYZuDtpeqpqhpkIhHF800Td4/ROjpRsAyQdTRO6+rlivgNgdUnZsNYX6kfrsioJFcpJC01uKPHUKyj4G+HOyjDDPy9hQdptAxP0W6Y6FxGHtKE+xYWLxd8XWlt2bMhanJh8bOlzZ0rBNg9UebScS5ioFZckRMwcRaYgBLEZ//OZooxDodlhwIiJZk/YyvNPzt4a9DNccgkoEUfQgYdcAqMlWRCBgU04g3zAgBxE3ScEnG8SHTUBysewoVOIRVtV8lRmibq/ALSsE8GKqf3jVUzAXkRXsO95C1DU8SKRDyqJbsAyJKjXVNs5DGpurC+KWB2Bh/JHtGsC59cnGbQIdnWwBQKg2cnZo81fwG0E0DhEjX06nySqJqYaLm8PPFNunYKz50W1wbkc5ByIFX1UB5JrkKZ0OA7FOnRJYffjTBon1SAYy1OZ4H1RtVJgpi5JGT0AaTj5KNwMa9rAPca9hZWmZbXCiqSGypw/COnygCRohDugUbIsVJLoDsdDi6Ae8ig2/M8P7MNMv7lVbAcTG2tqKvWeUCSJrLhU7FFTdENO+P6wMz20OLAUtOvqAmA3JE5/sPo2hKyotRGZDyyP6gyBh2e7QxqwTX2WWrzF38mU3AB1iF+UtwBVPIeqfhNcBEaZNFYczXxVAnHhEdiiUISJerEOnBG/L22gbEvmaJMu4DaCzB7u5esxPUGXGLLevKvJZkLzTdIQITXsQoDZhZNMAq84PVlTBErrPqSJ9qXZqVwW1rba9gkRbsQ7qPLxElC+7uX7Eh9+UfRT9RvL8pm4S0vtRywTIElsYpyTSVQ3e73mJRH9M4mGQJux3jq3AVzeCvaM2haiNUuvy1SU0Dkl3KAKXXPgoc/fZ6vjk4w3QBptiaIBMO/qflbcFt/XAyQZqxf1BfrChrLahkELLeZGE0AetNrRVU/NIRDPwMlxzzScFbEDMzyIjoPXCzf0TQgMsRn/sZnWzYEFbzMP55RK1kWa+hriCaA2wP3NRseBYolmRTFFQGG+rYWgWOH3pg+CwIRLvLxSJ6EQHEEGuuLGM27IoMkq6muIjyiKvX0C0r7Vuop8QanAWKjpOItjakD2itwpa47mFWLXlLrNphIuxoRLLeXDswqWTG1fqB+lC3S+3kn8K4isgNkCyfFBgs1XbLtkisAx2iit7JIQQCashH0PEEzXfwQHVQ3NLoposLNt8l3BUe4VfRCRU1pO9l3Jy8HaGP27LoMmSfw5UwBIdK5/swWaVHIIKQ1O4HNugKkjMs5RxSM1NsFXRGGFIrPhNVxj2KTc5dESG2YVjSpi4Lq/2SgJWzbBBki1EnppDvyRAknofU2BF3VGG68LCrZkbulc6W2+9oL1A7av5aaKiPLTDnuzJ1oNAzjUpTEok4QccgXm2Mv2ymzLCH7ltBDg+bK7O9mWLIOZ5i3eKMHrgjXCtCi2aiHtGwmF4NtcIQSXkK7LqAqKinfigupYpwgmKvGwpypwYgR0u+FoeBbaQLSsSoCr2KRJPOIgdvPhgAZDaQBnAdNd/qORkuuD0BVG1ExWtToMilohGBPQfHm/FY9YHSOSOXjyuSEA2viGM5OOURMVvoDseIFVFUpBprQytX5eMQdK9CoFcpgMYEsEgHNba4BMn988O4pKvGtI4gCmWRgU/BUETW85EcMqLRUkXy/QFWatPbJioJiYQm8SnkJP1Z7Q5poJm8yaFCgJ5EHJgZiqlKQOVoAhZPkGaPyScIen0gGWdfFlHuAUJxUolqQQHipIgwxZSUSti2qes1EAI+LYgJE+uwPbL/vDI8VhA3350+wxR1mJOh8GyBUKWsUDJx9rOHFIMsRn6t9/v4QeKqJboOlmvy7UIrEicyo3/qn4TpMU71VVb6JKjY43/OtPYmF7GkGaGfwFaBrKYpJQCZkqmSgOigrqKdRaOI9EZNFGSbfw9n1qDGhDVrLpERNByOXk7IRM1xyQIOWAnhBCaXRCxYAH6E3WE1LDxD5y5tngQQ8toSNOtCg4AuxNjuRp8SFVAIBQSClPpj/j1NA/FLEEpSVGR8igI+Kd7PN8W7L+sSh4srDXQt33aotpqdh1sxRZzqLCFVRGxqDD3+VdHBwFtj6m+ECJ9xNpC+iJR0YNYlkUZQSFZct5HGU5z7A7pkJVxxZBCHF7QRvyPBWYBnMo2DYGmbkNuS5AlRhhitE8Typai6EbJT7rsxoZ7YiXUQxxQl7McTjp4P1BGUEgBOUucCJmFImMBhfoE/bK/vecrohZqMddvgB38aCNXqhAR3ugP61Qh0kNpoBBcxddj5BqgofRlayIj6Ek446aO4f3t/yXibbBCqGXt4OmvfbY/7IEBLGMFZ0CHgQxbtBqanbDfKi/azDhhaeoSm3ysAOvSH+4TAdh683BCWpbANmxxApR2y0gQC0Gp6W2AjC4HVSw2sQOhjxrSiHogWnoLMCXoLIoeQjiRMC9EOpgP6YMvwpLc5Hs+8BGH4/wHWwttOcstTQZF/SxxAqKjjkto/BJ13fJi8DPOC1TfvbJrNh+5bHbdL27K3uCi2gDYspuzP7jJQuQZQaUGK1QUr9ogZjDjZAAR+9EHFhaNdGLEpwzg3hVKAjY4WJWSrQ2Ad05C/M/J5AS74a1wrYicCLWZn/zVA2QWkI09WvTgfk80I1g4/2EoVKIOsPd7FqlvHG8QqABCTrBLvhfK2Ai72I+8oWxJHnbFYjkBDxXp/0yXlSA+9NZGv6RGTmtCyBWIh8iTcqLHqfQzoKSVNgRTsDod4FqnibMbm5SxFmjeHfJW20AokZP3IXJ2JAn+7yTxwcWhIZZDf4kkl5X5w5V7gAzMIANcUroSgwaqkmz2KMng6FxcWzILq7QReffSrtk5t2nDJN7SiRWzcsM67RyxZQ3yztP23ae1PJA1cjSA770RwsPIlgMdiCmChICSSLMpL0N9ouSdqOe3Rg3bd/JeVOmU1GF3eWnBrKwsJj76RmpFqKbPAcuHq1QVLPpiysOrat8texl+sO9dEcljd3fPbG3t8phlsogFNoYK/OOeGH5vf2Vlmb+NLvQBYY31ieT7IbBtkct9DD92LUKxjKECAkkYiVSz7h0RonqcBygGPoBVlX6gkVXXzpRAIQYhtyPpkwH8/BOhRx9avluogp/q4IeDqIrTOHxigw8p818oRkud0cnnzOVNs0TOkHzs0CxhlFmGaEhI+GuDJ7iBmtilakQZCafdY4dCwIG8c27L7F7e8fXV0+vM3j6zaXcM0fZ39s3yyRWzfMOa+s0Z5aHgrBTD3uYuJbwdsrlqFvF22MBUSStC1b2jRIygmio1MXZyhjwSDxJQrtvy6HBSFckw3QFVA2t1uI6RqDiCP6hzyPjJJR0BHhbY3N4127v7XE71hYDPwTGOvbW1ZbOE315xOyAOy04xkcHE/oCkXSTrT/wcVATus4y2hMVgM4pXu64Zq+4QhZyNRFULRczeBjVQizZwobJtZwsDSLfYYxvHiSVgPHLJ8QfbA8HrUmX0ZTfrT86WcwCnzI0h0vKFQJ0LOU5vzhcsqAhRRRkBa8v667RDbV5YorPJi9tml5KAWbY9if2I1Q6veKg3D5B46IDnxHMjJR5uj2xhUUOTY5EDSmR7F7fMEk0Kuxe2PRM7G/+aUAZVkSA1xXRQVIugUgfr0h9u+lhwt6oz9aINYRYFLIoig/5QUhsFb8ekrQu0zFzQMqScSTHY74qvAT5yh38q1UMUZHOzAQFtRd/bZAXewNUa8pcxEKsQEelP2NVbzZBwyhIh7HSorSst6yxrTEgRuYwu4VAoEO/SHw8xazcZK25zV3lkO3w4L+5DIsGf0cknH7SYZLNViBQO7FzC8ciYE1KG1Qm3K0g557IINJgWGzuUAHYu4d12dnAjGdAykCzajZPy3oFZOq4SjzjCR+RTSPEWAyuexfVVvryHSyHcZ1nJCM6g2LUaYU2gqZ6TEPLQYtzchryG1uXJEt2IenFZLdINFMUGRlGEgShsEkSbEBuPV9Fghi1ORkuf+OJmEFVKqqiBPuRHrGmzkzAAYXRqaC3GEo1nvkznRKrSZTMDnHv+IHnbqxplD2WOhdiyR+Pwb0DBgiKJhJYK4ysgYzZEbNVCbBcv9c2A1GPqZPSE34RtkZ6PpCTxyBaA1EZfdnvs8qZZWVqKdlBqIrbaNVkCiV4B3eaU4LQQzMLKotmjFcfOeTxEYFu+evM6P2Cw/egVrqNDcJliEdfLb1m3lywyv5evbbfCgS4mkJ2zV8zyqVUeOTtnt8zKrcc8PwsynNpuUHQlFQ1QZDf0BDkxTPMYiku0ulxZXuZyiBmMMyyjbqXYowSlmS/aocGEmp02wqYHlS5g5bKzu2c2N3cpJgkKWz0u9Dnu9eCV9yOnhSryXn1nBWi5HWylVq3qYCCWaLUoseiO9QDaSMAmRpEXwd9CKAMSYo6l6U97l4hAOZBck0aB9GHCxoKKNYjxQucsA6TM94RcGeKuiNLo5PPopU2zSpMuo6CZJ1fcRKyugIq+pxgriy3QmeDeBh4koMTjsHJ6je/lbJ+5wkkJycmfqlNPL1GiWFqnCdQlH227MxwviFej7J7dNMuU7GBvjxLgyi355JPuybo35moRV8aHtl23QqgJkKHSCLM+BiZWFivLS2aZTm6GYdn0blEQq5/0xD2Yg9KnYjOapkAKr5LoTjBGQMLZ2t41m1u74a+QwpyrasuWROmfhvDaGn5rZVj5zATnL16NpKdkDg2foZUcQmog72KpIWHrmTrn0LMtE+ebgFbz8PoDMyemEYSMQi4GRpHhoDwVnLZiEUjb7PigiihSfWg3KvikP0J3PHlACphw2Q0Ziwrs3EKqEbkNpTBJnzDo2X8eU4xFwOWH/c09m3jQZ1RfuWmNkwvu6yxS0llA3tnbIx4VcLmCRiDk9+nAR11f6OgKh4Qw8co/uzynT9qDSIQHe/t2VWVF7cY6THIQTgVKRPQVyZelnqAgwCSMOdkiProRqxxskBYTVoxHl6JYahWRmK0O/1LYCOxWghi1+rp/shDRokAILe5VAt2A0wXRwMaLbhonQQuVOSna3rZ9gcSDIYzLbnOB8mdR3iNMzDJCiFgqOlBlzyatGNOs2IBzKF7spmsWWb/AIOKRk9MWsQUyGRsWmiERxJuDKs4bQcJNXNJfblTIGH/ZjVY+K5gEnVasHNTHWHYGWyoSflGuwCgM+yIQDib7XUok/Fg1DkxazeBeDp48W1yixLO6yMlpn1Yki+tLLHuwu0fbgVk+tcJJCuUcstQckWi7566Y5Zuw2jkw+5d2aHLZJ/trlp03n0cky1X6Y8nC9MMlQihVRUZI28vacMTV1SVevve3K9yzQdzCgD1XDNDto4xmnKN8WOEJKh6YBHC/58rm9hAbPoOOUSAeWPv0D6tO/H5PMCVE9ich8e0IM9tODVSa2QTr0h8vqydLj8FSMOECwhJ6xmlp1ZdQIxvsK7KXMW+Rd+Hh41YGcjRGZAtyLMJ/aFIEn8ZL0Be+bIV97PRpWSjgxHpk8uFHrXGGT1pZxRHWQs+jwhiQURtII206cdHiS26XtvkJNzzphjr6bW9rj+/3LN+4br/ns0xyl3fsColOO7EaWj5t7wn5tTghG40i7u/sUaLB49p0PkqdwyzsOfq/fBPZw0qK7O0+smGWbzlu4+mE7msuDn8cQluoxQm7LO0QijsbA6Qc6DLRPnyyRKfeq3iMvILIBSGkiO1UTkExY7lsuxS8fKAoFaUdGy5i7GmRQkYRXxDd2dnn5GNXNQ65hpE+eyce+v/Y6gonf4wx4U9BrBZMTEB2Uq9ADLJaOSiwNVd76W5KYEQs5LXTdtFWUUHiiVWKCOJwvqiejaTXqJPjj9i2IHageCJnRVARQiH5uP3sefTpL7tNSz5beMAr/I7PCAtlb6PCCMRDzWl2slqun/B4M19GQx2CSL54nBr3fmh1s7exY1c91CkQweoIKyGsiIrIsJDkFldogK4uU40EqLN4X1ERyU90di8gQS3yyqoGFld+hroQXQMTWL5SDZBoFQRL+rlBgGG4TGfeq7TpIRlISoUDyFgHqdQkQnnsDaioM6yNkiGn3eFnhpRTtY/kgy+WbtJqfVGfnOjQch1B/GO06sEDH3JZtxct6XBy05VQN+Q4QIAZqRfPKiBw6z7niaBdgHMS+xrEatFaiG4oSVQQ0HeOIfzudikFKQrC/eM+BYonclYEFSHY5AO6EifQXKYILENtsLQJyefsxnDjPQk0Ay/SlO03lkp26GqQeI8GegYPEyDR7DyGp9pA4B7kBLRCqxE8BIDVygG/zo0m0Bso8RxfYZ0sKo736Wx1b3OPV00eOXmi7Z7ZoNiWzQJNFPLNYrw2H3Hxx4k0MbX3tBXIifF4iVGwV3RTCQDDcJXaw5NfLBdUqRIHo/mK58mRuR4k7WUblZTBjGwvMQa9CcEIOlSxat68smO2d/c4EQV+lb6OFK3Cr5Yi+YDeE2GPjIAnG6dgR2oeU3qvrBNC9IryTYEU3C4HVnc2yiZSTtweQfoz4KGu1Er6NcRR6HYkBhUPcgObToY9BuogbkvaNutz8rHy2kIfoCdbAYFIQ7YHmIt4Q9mSCFIbKDWwJP0ROwkwaauNZXDmuLdPKw0kXKL43kePLpjd89s28YBGpKWTq2ahlHggw7opcPDztMbf43FCFXn4WqTEBwEkHoD/Qpd8H9CZb//Zq3jnCHgDbAuHDRA+t9cJelq0DaCadHwN1Ld2oDo5fMimKxIM4PkhPLnAr0G3l8E2hv5JkPEhpGHTtYnoUEX/8WVZXoJL4qG/FD+Od2mbbh9kMFTweDY2q5MCdL31gn2Rwli9QcNq9ehrjXgrQgvojumAt69s5E0Mve513BaizMmhTypF7EXXW1uM4EQxJ6AQTwGjk09p/0hwbD+o9CAUZFX6I9sA5ritDi9Jf9hGRYXvndAKZue8fVPB3ra9p4ODeYcSz/4mLrlRxQdDn/iyJ51diqOlkyucfPhtjhqOH0OmMzsxOVBxgdQxd+SUxBQ2vLuLy5oowE0ntaNiNpDx7g8Rpcp8r+8LQdGXNY1L3PmuSlBFD9CwsV/0MdrOgsKhLQ5MWA5K0pJ9YRpYnWLnjf/VIe5kG5BSRqPThBVboGGM+2foS/rj+l+6DbBywybAilOjJNcL9jlJeYSSEuvR0P0wxk0VysZgHyW7YZ+IKy86sAkJdzbMyUwMHJoDqJILOZAJMegPSuNXPioK8e/NBZUWEm1bC0kEIQTELALJPhVOPLh0tn3uCj/VhifZkIT2r+yY3YuUeK7YRMQrCWr70uk1TjJ8XJMePiXx4DFo7xOfUnawpMJ0RiSbUGi44nElBzGT0eBT1tz+tpNmqie+LdcC+rIJEt2gYpHIeDhqxJCq4/oNsCfqtB+IYFtOBQ6K/kSBcb/bot8CJIRx4DM5bAWIT72FKHNGoWFCexExdBe/DBSbI+IDfarlGKoiq56sXAO8m9QmyHehlogRjssqVJCq2IeMwmgbAq809EA22Wj4LiBuZZyFsEraZqKpmTMAJyF46ARbfELCYD/5/RhTMR7joCzNYnTykTZ6kwmhhlBY13g/WLJDQkggEl4yqHSAOhcPBeDLongkmldAuOxG//A2g/2NXX6SjU8l6f/SDav8dNviMfvINe7tMO3EypB4Iv8DKWIImOx49ImVzz65LEhbCLMyeNGGASQXyYIbSfjNIyJkZTwcJ/zIbimo3TQqcbnTBhZGh9B5c/VJKDt3cE4yENWqOqMtoZG1mxAGZOU1iLFH49kN17xcxECy5YUyLjUX2q8he0Y2MReZDYWqSDTLGCEag8NQumLKk5pxCiTBpA3sC42kOgYz2PGWRZVZh24yvt914viqWV9f5u+I7e7ucxm0ZTy1W/Djk4nnSSEUzg2t8SsfAYxlDAoGlghaSlgjBJU6tG6gkhDK8KLU2fvUyTtIPHTA8p6gZLRIHY4yT4TYIEw9vIwkg3syO/ssjzISD777g0t2w+UiCxRtdSgFYLLliQQ/zLJOPjZ2qJaBCHog6BR2QJAgD3JsTGZpvtfnSHoL4Iiajy2F4kZCiqPJWXC70dcOXo/+5AbtKIixIpwTJSMqslXBnWqLLcR2RZXVheig5SJWFtjneFwaE0cxHjLi7Smni5R9+JJnB7y+21ooy/VacFJ9ogHQIt6cK/dhTXmm3fBRg9Wzq5ppcJ6nGwjhzE2BNFuAxLO+TifQVMYrmeSY29iw3xVbW6WTbSSgCMFKlQ2iPlhGTUmEILHxyUdOqzLQZFl82r/DxkgIZRRFi4wQWsyL4iyP2rF71iYe3hm0A1Zvst/b4WTkADk8No2nx4SOVuG1N/y2AdCcOD7sNvxLwBOdSFn5ADQJ4PJbwFCCXCR9e5aqh5DA8bQ8NpAGcgLPpz/2Xw3gOglXFIpsvbCyuOwz1EYbyaFqwzFdh7ia37rRUNJsEeGx5jaGY2o5bONhxzQSEC6hBYB9bK46wEaC71flcs/0WEoQi22rXrIhngnbQ/T50zZ12CoQl3obj9ktJCAzkkjnCfvFYlvGE5InaK47zise95074uFnNuwYwZ+OIFhWyeh+d5+jk0/sUsKw9KE20BQiolS1nKZpuodi5GSEluMxaO8hMe6ex9sIqIyOoA5fuXHd4JdJ8XAB39/B3qBPnJHvb+2ZPZwF8M/oOKvOgftwm/zLgOzZiX2QDyBE8rG3uUNx7NlVlr+aZ/XZDi2JcU/KWhOIAYJ74IBriqwhZGvS12gbPgZovmW6UBSlA05YdFif2ofEI/d8+o0VULTBPchtHRO7yCRyGUUtK2wML70JWIb+aNlJcAZgG79aiuTj7TWMwz805UvjkJ0a0xSdHAI7HQZFPhEVQtzxBRTtjMZ8rAj4JgCZ483REkgbO9uqgRUN5sAdfMGdNtzruUJzIN4LiPIenWRv79rVEF44WwK7DWLgPxaqqDH+S6aXaNJ2ZQEmcyBrKCL2O1OSrtivm4PVxgpn7xIeJrBvLUDH4KWd/DYCEuFEw49WE8t9x4cvqxETDxsYnA0gOTGlBZKIhHTV75NAhpId7fz9bfxSKlXhi/1hsxq8MsLLH2nllT1jJdriDZRMA7tJlaAoMZMNW6JnpQb6UNHD8MONzbWV3JusgU6nGTE1DTfRI8ldrQRzOpndQZBIHLc/rDycvjaD1c6V7R3+gil+7DEHyGsO+pu/37O+4igOkXpijQyx74ybkCQRZgRL0I1SKJC96REeyhAjcIYtMhreR9UoRjcZw+GXBDFXHFtfNtvb9jthq6vL/Ou3fBJDGy634TwWtye2KTFhnCAx5cA5gGOTAKUB1GuO5EOnAsqjk89jnHzspaoinEV2Tebt9WWrYcNpIwjLKfXqCvQhL9Zwhr1zdtMcbNtr44vHl/k+D16jg18IXTxJByISDlY81On7+JE4EkQSWjy1ahZOEF9dlsuD+PZ/AtC4DVTQbZEetTr2Lx4+wCmHtMOC5LA36T/fG6JBgkt/1i59IvRjK/bekVbziIhStQYGrg3HQqtIWfHhB2fcruY+tAEN4eM/4j2gQb8SvdlAPi3CWhiaMOsaZbAk/eFPZ7gUOaNgWuvYvaGhuAX9GrxKpCtVtk5/LtPqHGevmEg0ICcUzcEkgwkHkwomm0LOGuAc+jCUPIq2qoLkIo1KJ9cyr1UFQkpYkU1tO2OGaTSPtqGVk7htSUR4wj106JYRomqMBjvBcZorNjd3+BIbLq1t00nvFr5qQoZwH2iFTspxHxFvSV9fI1m86YWAlmtf9oujAYU2zGr06cj8IWVyMOH1OpiMBc6SwJkaDEodIUSyFVgzoa0+baUTNGvQxmW0vcvufW1uyYkJkC+10cSNV+YsnaKDcffA7J6htlLSAQ8vEV3CpbkiSN86dv/JcClolgvhSRI3feRuAgeqIurEeFci+dDqztb5gyAFUcCfhm2BEEU8qmMCwwJyHa8EIofeRnbGIX7kBHW8z40nTOZZgVhOIw5lQEWpAPhpTroObB3yXMtDRxA1fUJ0Fjm9OAbIYBLBp+cpIZ4bCNoWxgv6HhNPcxqI2WwbfwZGEJPb/8x1zgN+jMS9TO2KUTXQxjj/hEQhjGnGcDqhvMzDIYWuzeAn6zHP4Ok2PHhg3zBC0x6/IWORP3F/Gy9SxoMpNvm49pMhGTZYIck+x1+Q8cmX0x2Rufh0zPGv17m8ZY0QYsX0jM/HQ46G9JPzmNEMaGgo6zsifyiBQForOkCXyWQItnZppcNvqxYQDUlm5fS6WTpGZwCPXjH7dAYAOjoWb5Xmx1GRpBxQ4pgY6cQaY5AdQgx0Yn1RwKdXSMXqsAqBDio6GMD58K6UQqDrwH1IKy4kntMn13jlEkIHTaWcEQcMbAxDDqkiZ8FShFiwqXiVIPEB7THRDW02Ag5mLJKx+gzExDcRS2Es8f1BcK3mUCKUlAAWCqTL8kokgJKPLBG0sZLhMcDsOBbo1QxK7fGIBZoKFYxre1OaBOKewPEn4OMQCYPmuSt0MiOraMx9eBIOj/J7L2BRkSWGPx6WZGmaLeVJyQewSqFq0RAH6DwTYrmcrRhe24klia6uzhAR3MvBfRW8zcCvLnh1s8RfFsXKCDf8eSlJ7KXTx/iSHC7DDfCBjAKLa52aPvlO5JsgYScfqMU2ht1hkdOpYG9/j5+SuvnUulnGfTTqP3y3UcPvI/6IHTqgf3EPCxDnBVELHWHJvpapGjsUsHcdwiEDQxg3hndpH/hppNEFOOpBXsK9zkjA12pt4ISFTU1lJfnAvBXiv/xnYNqSGNHGwvimYawN53+S63nEe3iIo+OfTnfzIC6xYfVjx5R9dB/fhdyn/tjEnMj9QkwxQlWmcV2I+MSJ0CAn0ywg5Rkvu4VDJEHAHLwP5A7XXqSsxZYzpmIS6njg4IBWNHgnGwM9QZ3MnziOqDv4MhvzbOJBUhre1+asZvwBMRl2mBYzCvpAhVWB01LKXCwZ4wYSO8cHzfEHDII8QJF4brCJB0kHL7Lc2nF95ButoQnWOHzjSauV7l/OLAgVdZ0f/tsG5JJmJ7DWtE3W6XKirRcUHLnLnAMmDlyT393DCVO+BbFnHPZLS0vhgx4ipKppHJaS91JGageIreSlpiK03hvxfGOYDZmYK83obWEOfOnbnUTu0VwnbzhAAsHYunBpi49570MK1F3F+z1qLOphKWV12tIPuJItQYEZkjMCMVhE5Ow5tGwtxHK2Tn/xoBodoPLzB9wLSDw4+0MHupUNHpJA/yHx8Ms+8Q43seo+BFKNyB6+CQIqg5YhJ7QarJzTcAa13cSvgvBZRiOrExKx7F7mxHOMByduVG/u7JkrlNBFUmz7Ov8b6gLIqDFZQaxJEJIiDySci1FSpDI2JcIY5IYtJyewMvIvlPMLAE3UCPhBhcE1+iMbhmMtlkidgeSBHxhkFv1pbRCETu77QOzcAVWLofUDrQznwm/SHrvBp2wW2vbULUbfuBLULF1rqNj8YIvgSMItbdgPavcGwNNs/NQb9RweqZYrVRhXWBnZBw9gJUSpn4v9rxijVz6PRSsfj4KVUcYBVrBaXboZIb7PpOwwcL+GMvreWSQeOgRwtk11vKkAP9TGj1jzG6rBWzRLeFR5jZKSrHgiP83YYvmmQh2huqvlbGZoTdc5AR7oClTlhwtoIN506pi71LZvtqh/MHBxlhOqZIwKyU166BP8Ki5spv2T0Rcoli0Wh3qCilWGWLJyqXTgybFFKokiVieBxGLqog3lCJME9svWduGtGA5xbBjna6srfFO5PAXk6Ykt95lH0isOUxoeomQ5RU5ydv+HimrjHFM3ob8zukQxJnByiRUQjm0MkUt4UGvHJiMxYj8QSGiVa3JyQx/C9ec7oJGTUXshST6R9ihjAlbqO6tiZASZJA1DLZLhDqSJki+3ocngryza387BTTWQaBLFtrC2jOtBlPYp8Tg7kbkyYvluxTwGdWWoZDOiN10nAoogfelIuJ+AS2S4x4PBhzN0JB1cakP3WRQ8KrItegWzSonedT+hoE9IR+lgYwzKHjRCqdhT6YgJ5JSML2qBvkAGZJqLODCucbkNj8jqyxwt4JIHHnFPMTawGK0YZrPf30KNWTo+D5k2x/T5KNTMylNcGhX5qRHiqga+hCoJCF2HBLRD8ygnIALNqvw5eMEn1rf0iUFGbN9F9OmLRAR7POBPNlV01T6QMHYgb/yvA5ETrlJreF/IkhSDQsmIDr9GhzoSqxzuDWo5r26ozK/JQaKhZMQ/wobZEDRnSpvziBmQxWaLqjAOojaoDyVdnAmJndQwtwUblXXiwYoHlyxt4sEynaXdFkHZiCVA592G/ZdwB3mJw4I13DZveI+Bl9jTEEsGLlbebJW3AFliASoIbS/sk7AMRNUEvPKnjg8nTrZsi6OhAs1CbE+1X7feh9D/1Eikr7EqwHZVETahCL03ZukzPOmG+7t4+wHmaQyXkzQ/4vFsvgRn/+fDqjgW1viVzyW78hmlpDDSXeAo0Iwb13EEgsQPHMiNcUpGfJ9HbJV8CQo2A2QV68irKGrBb6sLEgT8VDinzpfaVOJBjkbS4cRTOvaUoXxItA8oWJw9rbqHDQK5oFIZxZ3IxyCw3KoXMUBCxT6uO+lG7qSWUbCPCcE+bIC3XVjlYlucDfT9yvIyX9s/4JufIWJXRXvNfTOnTiG0PLWQi2SsTfQbLlNiJYD7ZehufD9GvpR59TA449Kh+3aX4JZpBYT5kzrz4iV3CQ6/+O8CkL8kQR8qRinSJ6+IiD9p5YOdOGZIsTxFK1sAMZYzSDSIJyKIfWhXxCTEdYIm4X1u/Aul2JB4mOg2B1W0UHwpKpJFQqgja4OhqJmiImVpWQQCQ0VK2NATuo4uLiee3TTxaOUmrBB8BCq+Ao5s/RD1eEuRcrOyEbHITxj9YHVqpl/Bx6jYhzjtItroT232S2zQgc/7zxKFnYhFCOV4islAS1076CjmEYnt4gWzRokHxwSAy1I4gcIWT22HBUze8C7bYQPDCu3epXkTl9gxh4N26uQqJWH7wFGIqJ4EiY6kv7SNTj49gHu+lMbZIw6OYAVSOHHeHMkDjdANgYAWiupSHUhU4lhos0esJTs4jpInKELCAxwx18QYoi9biIjjihG1DiXEOoGyLdh/thYnmxg7OLAyiWeTEo/++YOSgZJdhmP6yz5eON7JdYiaV6+iXxIiMgb15pEQkmoWosYbNZOTTam5IlgD+o8CRazFXsvY4AmFjvzgJaQVFEMJGEWpEJ1iYyAm9TYrUhv2/odcisJqE70nyWA+XmNY6/pfCfNsew445nH5fXvbJSCi4RLc6soynaS6k/gMdMRxeS7JZ2g4DgQq8casAYNQAibTnwwrRWzD+RNyzBZ+SLQQ2YQVEXN8MYktNySEJ1seiksfOlRHrUMEnTAXfZ33hvvXTjYauISzQstsfriABh3G1uYuJZ5dlXgiQ9q2IqdwTHw3gHOPn4FzvTggtu/MNDBC2okVhoqFyAxFv5XgZXQz603NGEw9wAQOXhxvfMzFEMcRIGofsW4F0YC3XXAUo1OsBFHPbVOgW9+yiTpOlJbdPR5catvBa2eYwaQ5Y9g/OqbadjWAQ39nb0hAGNT4sTl8Vwyv4Bk6Y+gUHxuTQv5Mycc2HEHIZukBrFACIedUstDCXLYHXd4GUSSmCCKfcgiKEctxmf7wZknchUM3Oxm3lRFKcYn+2FofdNNY322W6GshrwckiANrbXnJ3IIVDz+GaxMPzvT8uRfRxK5sPcDewj8aulyHNWlHDG27IFLAoKX3TREjHdREB89ukwHSE4hWZCQEb0rMgYNjHp+eF6p4C8NGf2kW8avOsRBDUugxw/LXH3xTWqBBihUPkjYuM9nL0XhK047fUc0Tp0Wlkftl4m6cCk5AuARHCUiOY05Aq2oFpGIKwwsbPfqBg0fcAwdsqKSZoY9yUgKFWrejuPliHi1ZIgo93tdN24xQytf6lBl1HUekjxEmBzglrHjWVpbMTVjxUEPxQAvu72xR8lHfbR4Fa9o5kA/ajzh4+U3WluQ/p8Na6N4/GQZIrVY2j5acAdEpGQ9spg6sWnqPBflj0z1sgK8FCmohou/xJmucxY869DOi7LHUJqBgnskZvZqpsYAPPMG6cICHAWjeoHlxb3fbHOztmK0rm2Zv67JZXF4xa8dvoM9lLtMfkqWR7m5oSjzcZ3RcYNvmBwx2OfGsux9YwwMHePoL5Sx0PzQbObIXMuIjLRSh22NP5RyB2oMHVeR7QLySJtrG5o7ZpvnCvy6L4G3QpxRBBH1C8rkSdqYgQxtluIK+EAcZLrVUFL8oWmC0TA8YJAOdTgNtnZDYuychxgNByeMloWsr9lIbzm5wP2CDDiq8Nqf0+zBtuJOFKC7sT1w/X14q/YZPL6xuHF3VomOWZHItDWRzik0lBy3XNBojdIJdgss+W1s7vPoZs4v41Sm0k0ceVgnYZc4v6VRNF2INyWKh3TBI8jja36XCvrl0/jGzuHnerO5cNg8/+KB58NEL5uyli2abUvT29oHBM0eYPO0qHG9kXzcnlo15wq23mFtvPm1ufPIzzPHTt5mF5TV+ug3Agwa4FL1JxwQePcZKqDv5jEKnkYpYzUJ/r5KMEvLXEZwBO/7cSeQKJW90E/HwMlKcrPKcYf8PnwCXLWF88rlIyUdDaY8y1AGeujqN+jVRS77HZEagqeNhJRP5hoEsu5/IqO1JzQrfRJBPPHhdDh4wkCd7+pFPOBoYcnjUF2dO45PPIC+RdVkgoaYcCcgBV5TVjFzX1JwkAdeEY4TOcNmMb3zjFfe5ODKAN5ylYiKtojMsdkt/RjenEG+6tgNCYUjwmOFxs2/2t6+YrQuPmIuP3Gfuu+fj5uFHz5lzl3fM+Z1ls3rr08zpJ95ulvGW+p1z5uDSY+bM/Q+YhQuPmmOru+aW255gDhZXzM0nl83NdzzPPHBh3Zy/tGnO3nOfuXFhw3zB859rnvSCzzdrJ26kBLXCT7XhJwVwaRonT9KPoI09ASij2DlzRY+58FiQiktD7g/mClyeRwKyP6tAKyA6IZIELbLBpytMSz5OY5RiB6w9+ttpOBBrNKPZyga/HZJPfyEKilV7CbPu3XOVWKzBOz1jBjcQ48SDs7vtHVznThVAsQNogJVyshkfOaws29/w6R993YIerNGjpmXixgFj+E2MElYIHdvHX929uM5ZT76xjuvzxcN+bHhwPUXHwyrHLRhMWs4BrWr2rlww25fOmIfv/qh54P77zKNnz5uzV/bN9vot5vTTnmVueNItZv1g01x+6G5z9/vebt7/1t827/vgXebcxR1zgFXM8ZNmaWXBLC6t0CqGxh+Vl9dWzfbGtlnc2SXdLfPiZ9xkXvLyl5nPf83rzd0bp8zFT3zcfN5znmqe/cLPMzfc+iRe5fBlTppwkXwwhvFONDyAMPk+WgBnYx6mKugxX0w+sTIJ4GcZ5JgG216Ccyev/B9/YMlecgNGJ5+HL0QrnxkwOKZSZxSJmCekBpjSslvht1XHKVftERN8t18IeemajdyeZHuKrkXiFc8eGcDBtY1X5oDgpMs+I05ZMAHOlNoYYdDBa7RUS/xhB4Qymq7RFWKXUANhAJjocHDv4NH3yqTnPVMBJxa4p2e/XBrFlAkxJpW9dCBRzjhk0OS2t232Ns7TauZ+88CnPmEeefQMJZrL5twOTXC3PsXc/NSnmrVjq2bn4sPmoY+/39z5jt82737775mHzl4y2wv2/g1+XGKREi26BpfY9vd27bsdcbmN2o6xjgmRX6G5SJMmLWJuPHHMLKysmNOnTlCyWzJPXN0yf/xPfqdZeOaXmQfu+oj5ii98sXnSs15E6y2seHAfiOKhvkQy2tpyv/1lGxFAt7Teh4pbF5wZPebDYWUr6Lt4uNk+li/gUv87gStXbAJCYhp00OvUI1S/6slncOZKHd6zIgnREjy5ZTfDb6kAkZcUEatlU/d+/rJD2waDhGwytHuZ/zrFnL4kHnm4APcPrtBZtE08TiiLjLWcgwLQXiQ2nCWVMcJghOZobvH9QVJBV3jT25BHGBgOZlxywyUgefw961ER0Tfra/ZV+f6w13z3mUNPtyQIlCLrqO7ThL17xWxeeMxceOh+8+iZR8z5s2fN/Y9eMBsL62b9CU8zJ286TZP8vrl45kHz8MfeZz78nnebD3/o4+bR8xf4gZidfRw1tJpZXjZ7tPrgt9ZTosETaWgjEq4G5R6KCzw+F6d+pL/0HxMmn6Qv4LU5xhxfXTE33nza3HDyuHnGqWXzJ7/3L5n7Lh8ztx3bN6/88q8mv3hoY4F/wRfHnayIWqufbm5dcCb0mE6bYQnoU83joqujq3Fs23tARKT/G5SAdvAmBEwyDKx87D6Za/IRQ+IGiHZ98FFCkV1gWHLEbPjQ/JYoMGaVU7Pnea5g+yrUqOkHiIzxX36+t4408biHC6r3eDJRVUilMYDhZl/VEV/6yRjrhVJtWqkJtLuu4WCGNjQxBIcSuu4KHjZw9QRRKJDHrl1bp7N8mj3kqdhelLpG3Hh+RpDfm7i/bQ62LpvN84+Zc48+TEnmjHns0cfMQ+evmN1jN5jV07ea9ZMnaE2xa7bOPWjupURz1wc+ZO6++x7z4GNnzMbGjtkiM0t00oKVCsbO9vYOf+LFqrikaBONPbOWb96DLwnIjzcrRptNQPZMHZOi3XCmvrxs++jU2oq5/fYnmFXqvP/t219vbn7Rq83e+UfN13z1683u0holH5vM8bLN0iXQTJcUoCT7lZqYYipshq3Y7tPJx/b14MDeA5JLkjgpgs4WLsHRiQH6WZI9S9MOcXukD3HyiZUljoGuJCqeqkFkmCGpKRCAWRV+CCuYFXfElqmATxXbR6FWy0YAFtYaZJGqPTZ26OxsfWUpeqrNrnjSxJOx2HDSigHDDaueZTq9tEOvrgFuHJVg3Mh1iHVKxkvI+pwSSA/C4NgL/cEBzD9nQfsNtECqEMrwaOzyfMMl54F/Xs1s0oLmkjn38H1m48I5c+axR8zDj50157doxXXyJnPshtNmicbg8tKeufLYveZTH3m/+diHPmTuv/9B89jZC+bcpS2zhZUJLofRmERywct/cY8LqwucCGJlw5MgtQWJhk8OaUDgchoN6vAcDO2luvuI4IzQJ+xh4kT/4hNXAG56xrPN5YOT5oZzHzanb3uC+bpXvsi87Bv/tDlz793m9a99nVk+eZovZSIGfvCATKn5dSSUVs0AwgUKMtN8h+C+9bAVSRWax0VfFzm8eHXBrCAB8T88EUhzDJIz9y+LjU8+D3VddlMmG9ar7IgZVguajhxz0d6AVlC3sMyiCDFq6lkeEd2u4b9AzUYOdk/hIoPV5d1KhR47kMGBe3xt2Zw+scYDALs+TTwFa+PIRaAN+H4PLm30jDyIJJGNdaohumJ0LOYSRIQgJhvYYJ1KXBkC5ledxA8bZMNxRBLBvkbCxyWRkYd8HmTj4GCXzmbwtNlZfrz54mMPm4cefNCcuXjFbCysGbN+yiytHzerx4+b5YMrZvPcA+aBu+40H7/zQ+a+T1LSeehRc2mb7Kysm2WKa39vx+xSm/Yp4B1e1SDhULJZwErGtsFu+1ynArUQdWoitRETv+qmLuTE0aeLC/ZLpDffuGL+8g/+HfOffuUD5sG3/JR56tOfar7xy19mXvr6P2wuPPyg+ZrXfZNZW1tlWTwdukubfwvIKGgdNMgVPYigd1vFxRTvMcJhJRU79yQOfN0WMC9hP+GXUfHdKPQn7GG84vKkrIDmmHyUmYrFLmdVUwULRO6yrREpYCDX0OqphO0IvDR1aJhIwPKBY7XnC8ZSMi5NIPGsmJtO0qQACtnsSjwRKZGoqJaA5IOzylp/ZlkjfBwOKIARMcSias9ZOAF3SCsBz1AgpojRgYtHWfO/4eOUIjL2NxLPsOLsxwKlg4NdSgxbG2b70nlz5eIZc/7h+82DDz1sHru8ZbbNutlbXTNLJ240p06fMssLV8yFB+8x93/0/ebuD33YfPTOu82nHjlndpbWzPL6ulmhOHbJHn7mBEl0G2VKKHv4mg5WaMvLtIrYYt98Oc1mHkRBn/ZoQrN1K3jCCyghWnzB0G1ugqTts25eNd/z1/+K+cl3nDMf/sV/b577nKebb/uaV5onvPTrzFOOG/P5X/Qqs7a+qlY/wwliC5BL9yFBk9hQJJNREVRY3ZCQbBuk4labGr4uBfQ0PkiT/iMR80900/EOTpCAZks+kWrBUrcDJ5jKlw13247hfXVYKPgpanpGh+0MBnWtH+91QmQ+9UYU+s+JZ10SD65l7/PDBbhObZ9qEygL+aKFqCSMPCAGFXziWQYMxhyK5jr9zAwJNIByXomjJ0TfbSwcOyJirl+92CAvBzEmbkxcfgxrkzCn6pDAO7hwOal0yDP9YM+YvW2zs3HJ7G6cMxtnHjX33/tx8/DZC+byzqK5YlaMOX6jOX7zLeb4iZNmZecx8+gnPmQ+8aHfMx/+/febj378PnNukxLF6gmzQmOOJyzyvrtLkzKNQ3wDHj/NzMESD5fPEBzagMl7j+QAfmCA/uGLyHiA', '2019-07-31 04:52:27', 144);
INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'info@bmsdecor.com', 'rajivpastula45@gmail.com', '<p>test</p>', '2019-07-31 04:53:17', 144),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', '<p>test</p>', '2019-07-31 04:53:17', 144),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 04:59:40', 1),
(0, 'info@bmsdecor.com', 'rajiv@technoplusinc.com', '<p>Test</p>', '2019-07-31 05:01:50', 1),
(0, 'info@bmsdecor.com', 'insystemgrp@gmail.com', '<p>Test</p>', '2019-07-31 05:01:51', 1),
(0, 'info@bmsdecor.com', 'rajiv@technoplusinc.com', '<p>test</p>', '2019-07-31 05:07:52', 1),
(0, 'info@bmsdecor.com', 'demoinsys@gmail.com', '<p>test</p>', '2019-07-31 05:07:52', 1),
(0, 'c@bmsdecor.com', 'demoinsys@gmail.com', 'Mail configuration verification', '2019-07-31 13:09:44', 144);

-- --------------------------------------------------------

--
-- Table structure for table `custom_sms_tbl`
--

CREATE TABLE `custom_sms_tbl` (
  `id` int(11) NOT NULL,
  `from` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_sms_tbl`
--

INSERT INTO `custom_sms_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(1, '+12062024567', '+917990284173', 'Thanks for SMS Verified', '2019-07-31 12:15:29', 144),
(2, '+12062024567', '+14782871258', 'Thanks for SMS Verified', '2019-07-31 12:16:45', 144),
(3, '+12062024567', '+14782871258', 'Thanks for SMS Verified', '2019-07-31 13:02:13', 144);

-- --------------------------------------------------------

--
-- Table structure for table `c_card_info`
--

CREATE TABLE `c_card_info` (
  `id` int(11) NOT NULL,
  `card_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `card_holder` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_card_info`
--

INSERT INTO `c_card_info` (`id`, `card_number`, `expiry_month`, `expiry_year`, `card_holder`, `created_by`, `create_date`, `update_by`, `update_date`, `is_active`) VALUES
(1, '4263982640269299', '02', 23, 'Testing Card', 143, 2019, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_cost_factor_tbl`
--

CREATE TABLE `c_cost_factor_tbl` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `individual_cost_factor` float NOT NULL,
  `costfactor_discount` float DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_cost_factor_tbl`
--

INSERT INTO `c_cost_factor_tbl` (`id`, `product_id`, `individual_cost_factor`, `costfactor_discount`, `level_id`, `created_by`, `create_date`, `updated_by`, `update_date`) VALUES
(1, 4, 90, 10, 140, 140, '2019-07-26', 0, '0000-00-00'),
(2, 1, 90, 10, 140, 140, '2019-07-26', 0, '0000-00-00'),
(3, 3, 90, 10, 140, 140, '2019-07-26', 0, '0000-00-00'),
(4, 2, 90, 10, 140, 140, '2019-07-26', 0, '0000-00-00'),
(5, 5, 90, 10, 140, 140, '2019-07-26', 0, '0000-00-00'),
(16, 4, 10, 90, 143, 143, '2019-07-27', 0, '0000-00-00'),
(17, 1, 10, 99, 143, 143, '2019-07-27', 0, '0000-00-00'),
(18, 3, 10, 90, 143, 143, '2019-07-27', 0, '0000-00-00'),
(19, 2, 10, 90, 143, 143, '2019-07-27', 0, '0000-00-00'),
(20, 5, 10, 90, 143, 143, '2019-07-27', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `c_level_upcharges`
--

CREATE TABLE `c_level_upcharges` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_by` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_level_upcharges`
--

INSERT INTO `c_level_upcharges` (`id`, `category_id`, `name`, `type`, `created_by`, `createdDtm`, `value`) VALUES
(17, 1, 'Cordless Lift', 'Amount', 143, '2019-07-27 17:56:56', '40'),
(13, 1, 'RouteLess', 'Percentage(%)', 143, '2019-07-27 17:55:45', '20'),
(14, 1, '3 on One Headrail', 'Percentage(%)', 143, '2019-07-27 17:56:02', '10'),
(15, 1, '2 on One Headrail', 'Percentage(%)', 143, '2019-07-27 17:56:21', '5'),
(16, 1, 'Valance Returned', 'Amount', 143, '2019-07-27 17:56:36', '4');

-- --------------------------------------------------------

--
-- Table structure for table `c_menusetup_tbl`
--

CREATE TABLE `c_menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(3) NOT NULL COMMENT '1 = left, 2 = system',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_menusetup_tbl`
--

INSERT INTO `c_menusetup_tbl` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(519, 1, 'customer', 'ê³ ê°', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 143, 2, '2018-12-12 06:14:46'),
(520, 2, 'add_customer', 'ì¶”ê°€', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 143, 2, '2018-12-12 06:15:26'),
(521, 3, 'customer_list', 'ê´€ë¦¬', 'customer-list', 'customer', 2, 1, 1, 0, '', 1, 143, 2, '2018-12-12 06:16:01'),
(522, 4, 'order', 'ì£¼ë¬¸', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 143, 2, '2018-12-12 06:16:35'),
(523, 5, 'new_order', 'ìƒˆë¡œìš´ ì§ˆì„œ', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 143, 48, '2018-12-12 06:17:40'),
(524, 6, 'manage_order', '', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 143, 2, '2018-12-12 06:18:26'),
(525, 7, 'manage_invoice', '', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 143, 2, '2018-12-12 06:18:56'),
(526, 8, 'cancelled_order', '', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 143, 53, '2018-12-12 06:19:39'),
(527, 9, 'customer_return', '', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 143, 2, '2018-12-12 06:23:22'),
(528, 10, 'track_order', '', 'track-order', 'order', 7, 0, 1, 0, '', 1, 143, 2, '2018-12-12 06:23:48'),
(529, 11, 'accounts', 'Test', '', 'account', 4, 0, 1, 0, 'iconsmind-Calculator', 1, 143, 2, '2018-12-12 06:24:27'),
(530, 12, 'chart_of_account', NULL, 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 143, 2, '2018-12-12 06:25:02'),
(531, 13, 'vouchers', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 143, 2, '2018-12-12 06:25:40'),
(532, 14, 'debit_voucher', '', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 143, 2, '2018-12-12 06:27:09'),
(533, 15, 'credit_voucher', '', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 143, 2, '2018-12-12 06:27:42'),
(534, 16, 'journal_voucher', '', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 143, 2, '2018-12-12 06:28:40'),
(535, 17, 'contra_voucher', '', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 143, 2, '2018-12-12 06:29:13'),
(536, 18, 'voucher_approval', '', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 143, 2, '2018-12-12 06:29:45'),
(537, 19, 'account_report', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 143, 2, '2018-12-12 06:31:35'),
(538, 20, 'cash_book', '', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:32:03'),
(539, 21, 'bank_book', '', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:32:32'),
(540, 22, 'cash_flow', '', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:32:54'),
(541, 23, 'voucher_reports', '', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:33:23'),
(542, 24, 'general_ledger', '', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:33:54'),
(543, 25, 'profit_or_loss', '', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:34:26'),
(544, 26, 'trial_balance', '', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 143, 2, '2018-12-12 06:34:58'),
(545, 27, 'settings', '', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 143, 2, '2019-03-14 08:51:22'),
(546, 28, 'company_profile', '', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:53:27'),
(547, 29, 'paypal_setting', '', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:54:23'),
(548, 30, 'cost_factor', '', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:55:09'),
(549, 31, 'web_iframe_code', '', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:55:51'),
(550, 32, 'employee_and_access', '', '', 'settings', 7, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:56:43'),
(551, 33, 'users', NULL, 'users', 'settings', 1, 32, 1, 0, '', 1, 143, 2, '2019-03-14 08:57:12'),
(552, 34, 'menu_customization', '', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 143, 2, '2019-03-14 08:57:51'),
(553, 35, 'role_permission', '', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 143, 2, '2019-03-14 08:58:37'),
(554, 36, 'role_list', '', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 143, 2, '2019-03-14 08:59:59'),
(555, 37, 'add_user_role', '', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 143, 2, '2019-03-14 09:00:51'),
(556, 38, 'access_role', '', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 143, 2, '2019-03-14 09:01:32'),
(557, 39, 'sales_tax', '', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 143, 69, '2019-03-14 09:02:19'),
(558, 40, 'bulk_upload', NULL, 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 143, 2, '2019-03-20 09:03:23'),
(559, 41, 'quotation', '', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 143, 2, '2019-04-25 14:42:49'),
(560, 42, 'add_new_quotation', NULL, 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 143, 2, '2019-04-25 14:44:40'),
(561, 43, 'manage_quotation', NULL, 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 143, 2, '2019-04-25 14:45:35'),
(562, 44, 'header', '', '', 'header', 0, 0, 2, 0, '', 1, 143, 2, '2019-05-09 05:10:07'),
(563, 45, 'grid', '', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 143, 2, '2019-05-09 05:10:56'),
(564, 46, 'picture', '', '', 'header', 0, 44, 2, 0, '', 1, 143, 2, '2019-05-09 05:11:35'),
(565, 47, 'customers', '', 'customer-list', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 143, 2, '2019-05-09 05:14:18'),
(566, 48, 'orders', '', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 143, 2, '2019-05-09 05:15:52'),
(567, 49, 'appointment', '', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 143, 2, '2019-05-09 05:18:07'),
(568, 50, 'accounts', '', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Calculator', 1, 143, 67, '2019-05-09 05:18:52'),
(569, 51, 'paypal_settings', '', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 143, 2, '2019-05-09 05:20:28'),
(570, 52, 'knowledge_base', '', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 143, 2, '2019-05-09 05:23:04'),
(571, 53, 'company_profile', '', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 143, 2, '2019-05-09 05:25:33'),
(572, 54, 'my_account', '', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 143, 2, '2019-05-09 05:27:58'),
(573, 55, 'change_password', '', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 143, 2, '2019-05-09 05:29:05'),
(574, 56, 'my_orders', '', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 143, 2, '2019-05-09 05:31:35'),
(575, 57, 'logs', '', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 143, 2, '2019-05-09 05:32:54'),
(576, 59, 'dashboard', '', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 143, 2, '2019-05-18 10:43:26'),
(577, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 143, 2, '2019-05-19 11:57:20'),
(578, 61, 'payment_gateway', 'payment_gateway_korean', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 143, 2, '2019-05-22 09:21:00'),
(579, 62, 'payment_logs', 'my_language', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 143, 2, '2019-05-26 12:05:11'),
(580, 64, 'purchase_return', 'k_purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 143, 2, '2019-05-31 09:23:02'),
(581, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 143, 66, '2019-06-24 06:12:52'),
(582, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 143, 2, '2019-06-24 06:12:52'),
(583, 67, 'Gallery', 'Gallery', 'manage_c_gallery_image', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 143, 2, '2019-06-24 06:12:52'),
(584, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 8, 27, 1, 0, '', 1, 143, 2, '2019-06-24 06:12:52'),
(585, 1, 'customer', 'ê³ ê°', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 144, 2, '2018-12-12 06:14:46'),
(586, 2, 'add_customer', 'ì¶”ê°€', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 144, 2, '2018-12-12 06:15:26'),
(587, 3, 'customer_list', 'ê´€ë¦¬', 'customer-list', 'customer', 2, 1, 1, 0, '', 1, 144, 2, '2018-12-12 06:16:01'),
(588, 4, 'order', 'ì£¼ë¬¸', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 144, 2, '2018-12-12 06:16:35'),
(589, 5, 'new_order', 'ìƒˆë¡œìš´ ì§ˆì„œ', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 144, 48, '2018-12-12 06:17:40'),
(590, 6, 'manage_order', '', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 144, 2, '2018-12-12 06:18:26'),
(591, 7, 'manage_invoice', '', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 144, 2, '2018-12-12 06:18:56'),
(592, 8, 'cancelled_order', '', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 144, 53, '2018-12-12 06:19:39'),
(593, 9, 'customer_return', '', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 144, 2, '2018-12-12 06:23:22'),
(594, 10, 'track_order', '', 'track-order', 'order', 7, 0, 1, 0, '', 1, 144, 2, '2018-12-12 06:23:48'),
(595, 11, 'accounts', 'Test', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 144, 2, '2018-12-12 06:24:27'),
(596, 12, 'chart_of_account', NULL, 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 144, 2, '2018-12-12 06:25:02'),
(597, 13, 'vouchers', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 144, 2, '2018-12-12 06:25:40'),
(598, 14, 'debit_voucher', '', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 144, 2, '2018-12-12 06:27:09'),
(599, 15, 'credit_voucher', '', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 144, 2, '2018-12-12 06:27:42'),
(600, 16, 'journal_voucher', '', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 144, 2, '2018-12-12 06:28:40'),
(601, 17, 'contra_voucher', '', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 144, 2, '2018-12-12 06:29:13'),
(602, 18, 'voucher_approval', '', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 144, 2, '2018-12-12 06:29:45'),
(603, 19, 'account_report', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 144, 2, '2018-12-12 06:31:35'),
(604, 20, 'cash_book', '', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:32:03'),
(605, 21, 'bank_book', '', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:32:32'),
(606, 22, 'cash_flow', '', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:32:54'),
(607, 23, 'voucher_reports', '', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:33:23'),
(608, 24, 'general_ledger', '', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:33:54'),
(609, 25, 'profit_or_loss', '', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:34:26'),
(610, 26, 'trial_balance', '', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 144, 2, '2018-12-12 06:34:58'),
(611, 27, 'settings', '', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 144, 2, '2019-03-14 08:51:22'),
(612, 28, 'company_profile', '', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:53:27'),
(613, 29, 'paypal_setting', '', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:54:23'),
(614, 30, 'cost_factor', '', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:55:09'),
(615, 31, 'web_iframe_code', '', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:55:51'),
(616, 32, 'employee_and_access', '', '', 'settings', 7, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:56:43'),
(617, 33, 'users', NULL, 'users', 'settings', 1, 32, 1, 0, '', 1, 144, 2, '2019-03-14 08:57:12'),
(618, 34, 'menu_customization', '', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 144, 2, '2019-03-14 08:57:51'),
(619, 35, 'role_permission', '', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 144, 2, '2019-03-14 08:58:37'),
(620, 36, 'role_list', '', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 144, 2, '2019-03-14 08:59:59'),
(621, 37, 'add_user_role', '', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 144, 2, '2019-03-14 09:00:51'),
(622, 38, 'access_role', '', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 144, 2, '2019-03-14 09:01:32'),
(623, 39, 'sales_tax', '', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 144, 69, '2019-03-14 09:02:19'),
(624, 40, 'bulk_upload', NULL, 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 144, 2, '2019-03-20 09:03:23'),
(625, 41, 'quotation', '', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 144, 2, '2019-04-25 14:42:49'),
(626, 42, 'add_new_quotation', NULL, 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 144, 2, '2019-04-25 14:44:40'),
(627, 43, 'manage_quotation', NULL, 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 144, 2, '2019-04-25 14:45:35'),
(628, 44, 'header', '', '', 'header', 0, 0, 2, 0, '', 1, 144, 2, '2019-05-09 05:10:07'),
(629, 45, 'grid', '', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 144, 2, '2019-05-09 05:10:56'),
(630, 46, 'picture', '', '', 'header', 0, 44, 2, 0, '', 1, 144, 2, '2019-05-09 05:11:35'),
(631, 47, 'customers', '', 'customer-list', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 144, 2, '2019-05-09 05:14:18'),
(632, 48, 'orders', '', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 144, 2, '2019-05-09 05:15:52'),
(633, 49, 'appointment', '', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 144, 2, '2019-05-09 05:18:07'),
(634, 50, 'accounts', '', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 144, 67, '2019-05-09 05:18:52'),
(635, 51, 'paypal_settings', '', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 144, 2, '2019-05-09 05:20:28'),
(636, 52, 'knowledge_base', '', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 144, 2, '2019-05-09 05:23:04'),
(637, 53, 'company_profile', '', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 144, 2, '2019-05-09 05:25:33'),
(638, 54, 'my_account', '', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 144, 2, '2019-05-09 05:27:58'),
(639, 55, 'change_password', '', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 144, 2, '2019-05-09 05:29:05'),
(640, 56, 'my_orders', '', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 144, 2, '2019-05-09 05:31:35'),
(641, 57, 'logs', '', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 144, 2, '2019-05-09 05:32:54'),
(642, 59, 'dashboard', '', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 144, 2, '2019-05-18 10:43:26'),
(643, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 144, 2, '2019-05-19 11:57:20'),
(644, 61, 'payment_gateway', 'payment_gateway_korean', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 144, 2, '2019-05-22 09:21:00'),
(645, 62, 'payment_logs', 'my_language', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 144, 2, '2019-05-26 12:05:11'),
(646, 64, 'purchase_return', 'k_purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 144, 2, '2019-05-31 09:23:02'),
(647, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 144, 66, '2019-06-24 06:12:52'),
(648, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 144, 2, '2019-06-24 06:12:52'),
(649, 67, 'Gallery', 'Gallery', 'manage_c_gallery_image', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 144, 2, '2019-06-24 06:12:52'),
(650, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 8, 27, 1, 0, '', 1, 144, 2, '2019-06-24 06:12:52'),
(651, 1, 'customer', 'ê³ ê°', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 149, 2, '2018-12-12 06:14:46'),
(652, 2, 'add_customer', 'ì¶”ê°€', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 149, 2, '2018-12-12 06:15:26'),
(653, 3, 'customer_list', 'ê´€ë¦¬', 'customer-list', 'customer', 2, 1, 1, 0, '', 1, 149, 2, '2018-12-12 06:16:01'),
(654, 4, 'order', 'ì£¼ë¬¸', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 149, 2, '2018-12-12 06:16:35'),
(655, 5, 'new_order', 'ìƒˆë¡œìš´ ì§ˆì„œ', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 149, 48, '2018-12-12 06:17:40'),
(656, 6, 'manage_order', '', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 149, 2, '2018-12-12 06:18:26'),
(657, 7, 'manage_invoice', '', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 149, 2, '2018-12-12 06:18:56'),
(658, 8, 'cancelled_order', '', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 149, 53, '2018-12-12 06:19:39'),
(659, 9, 'customer_return', '', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 149, 2, '2018-12-12 06:23:22'),
(660, 10, 'track_order', '', 'track-order', 'order', 7, 0, 1, 0, '', 1, 149, 2, '2018-12-12 06:23:48'),
(661, 11, 'accounts', 'Test', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 149, 2, '2018-12-12 06:24:27'),
(662, 12, 'chart_of_account', NULL, 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 149, 2, '2018-12-12 06:25:02'),
(663, 13, 'vouchers', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 149, 2, '2018-12-12 06:25:40'),
(664, 14, 'debit_voucher', '', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 149, 2, '2018-12-12 06:27:09'),
(665, 15, 'credit_voucher', '', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 149, 2, '2018-12-12 06:27:42'),
(666, 16, 'journal_voucher', '', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 149, 2, '2018-12-12 06:28:40'),
(667, 17, 'contra_voucher', '', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 149, 2, '2018-12-12 06:29:13'),
(668, 18, 'voucher_approval', '', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 149, 2, '2018-12-12 06:29:45'),
(669, 19, 'account_report', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 149, 2, '2018-12-12 06:31:35'),
(670, 20, 'cash_book', '', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:32:03'),
(671, 21, 'bank_book', '', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:32:32'),
(672, 22, 'cash_flow', '', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:32:54'),
(673, 23, 'voucher_reports', '', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:33:23'),
(674, 24, 'general_ledger', '', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:33:54'),
(675, 25, 'profit_or_loss', '', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:34:26'),
(676, 26, 'trial_balance', '', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 149, 2, '2018-12-12 06:34:58'),
(677, 27, 'settings', '', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 149, 2, '2019-03-14 08:51:22'),
(678, 28, 'company_profile', '', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:53:27'),
(679, 29, 'paypal_setting', '', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:54:23'),
(680, 30, 'cost_factor', '', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:55:09'),
(681, 31, 'web_iframe_code', '', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:55:51'),
(682, 32, 'employee_and_access', '', '', 'settings', 7, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:56:43'),
(683, 33, 'users', NULL, 'users', 'settings', 1, 32, 1, 0, '', 1, 149, 2, '2019-03-14 08:57:12'),
(684, 34, 'menu_customization', '', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 149, 2, '2019-03-14 08:57:51'),
(685, 35, 'role_permission', '', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 149, 2, '2019-03-14 08:58:37'),
(686, 36, 'role_list', '', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 149, 2, '2019-03-14 08:59:59'),
(687, 37, 'add_user_role', '', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 149, 2, '2019-03-14 09:00:51'),
(688, 38, 'access_role', '', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 149, 2, '2019-03-14 09:01:32'),
(689, 39, 'sales_tax', '', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 149, 69, '2019-03-14 09:02:19'),
(690, 40, 'bulk_upload', NULL, 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 149, 2, '2019-03-20 09:03:23'),
(691, 41, 'quotation', '', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 149, 2, '2019-04-25 14:42:49'),
(692, 42, 'add_new_quotation', NULL, 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 149, 2, '2019-04-25 14:44:40'),
(693, 43, 'manage_quotation', NULL, 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 149, 2, '2019-04-25 14:45:35'),
(694, 44, 'header', '', '', 'header', 0, 0, 2, 0, '', 1, 149, 2, '2019-05-09 05:10:07'),
(695, 45, 'grid', '', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 149, 2, '2019-05-09 05:10:56'),
(696, 46, 'picture', '', '', 'header', 0, 44, 2, 0, '', 1, 149, 2, '2019-05-09 05:11:35'),
(697, 47, 'customers', '', 'customer-list', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 149, 2, '2019-05-09 05:14:18'),
(698, 48, 'orders', '', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 149, 2, '2019-05-09 05:15:52'),
(699, 49, 'appointment', '', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 149, 2, '2019-05-09 05:18:07'),
(700, 50, 'accounts', '', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 149, 67, '2019-05-09 05:18:52'),
(701, 51, 'paypal_settings', '', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 149, 2, '2019-05-09 05:20:28'),
(702, 52, 'knowledge_base', '', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 149, 2, '2019-05-09 05:23:04'),
(703, 53, 'company_profile', '', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 149, 2, '2019-05-09 05:25:33'),
(704, 54, 'my_account', '', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 149, 2, '2019-05-09 05:27:58'),
(705, 55, 'change_password', '', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 149, 2, '2019-05-09 05:29:05'),
(706, 56, 'my_orders', '', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 149, 2, '2019-05-09 05:31:35'),
(707, 57, 'logs', '', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 149, 2, '2019-05-09 05:32:54'),
(708, 59, 'dashboard', '', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 149, 2, '2019-05-18 10:43:26'),
(709, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 149, 2, '2019-05-19 11:57:20'),
(710, 61, 'payment_gateway', 'payment_gateway_korean', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 149, 2, '2019-05-22 09:21:00'),
(711, 62, 'payment_logs', 'my_language', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 149, 2, '2019-05-26 12:05:11'),
(712, 64, 'purchase_return', 'k_purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 149, 2, '2019-05-31 09:23:02'),
(713, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 149, 66, '2019-06-24 06:12:52'),
(714, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 149, 2, '2019-06-24 06:12:52'),
(715, 67, 'Gallery', 'Gallery', 'manage_c_gallery_image', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 149, 2, '2019-06-24 06:12:52'),
(716, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 8, 27, 1, 0, '', 1, 149, 2, '2019-06-24 06:12:52'),
(786, 1, 'customer', 'ê³ ê°', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 152, 2, '2018-12-12 06:14:46'),
(787, 2, 'add_customer', 'ì¶”ê°€', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 152, 2, '2018-12-12 06:15:26'),
(788, 3, 'customer_list', 'ê´€ë¦¬', 'customer-list', 'customer', 2, 1, 1, 0, '', 1, 152, 2, '2018-12-12 06:16:01'),
(789, 4, 'order', 'ì£¼ë¬¸', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 152, 2, '2018-12-12 06:16:35'),
(790, 5, 'new_order', 'ìƒˆë¡œìš´ ì§ˆì„œ', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 152, 48, '2018-12-12 06:17:40'),
(791, 6, 'manage_order', '', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 152, 2, '2018-12-12 06:18:26'),
(792, 7, 'manage_invoice', '', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 152, 2, '2018-12-12 06:18:56'),
(793, 8, 'cancelled_order', '', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 152, 53, '2018-12-12 06:19:39'),
(794, 9, 'customer_return', '', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 152, 2, '2018-12-12 06:23:22'),
(795, 10, 'track_order', '', 'track-order', 'order', 7, 0, 1, 0, '', 1, 152, 2, '2018-12-12 06:23:48'),
(796, 11, 'accounts', 'Test', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 152, 2, '2018-12-12 06:24:27'),
(797, 12, 'chart_of_account', NULL, 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 152, 2, '2018-12-12 06:25:02'),
(798, 13, 'vouchers', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 152, 2, '2018-12-12 06:25:40'),
(799, 14, 'debit_voucher', '', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 152, 2, '2018-12-12 06:27:09'),
(800, 15, 'credit_voucher', '', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 152, 2, '2018-12-12 06:27:42'),
(801, 16, 'journal_voucher', '', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 152, 2, '2018-12-12 06:28:40'),
(802, 17, 'contra_voucher', '', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 152, 2, '2018-12-12 06:29:13'),
(803, 18, 'voucher_approval', '', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 152, 2, '2018-12-12 06:29:45'),
(804, 19, 'account_report', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 152, 2, '2018-12-12 06:31:35'),
(805, 20, 'cash_book', '', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:32:03'),
(806, 21, 'bank_book', '', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:32:32'),
(807, 22, 'cash_flow', '', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:32:54'),
(808, 23, 'voucher_reports', '', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:33:23'),
(809, 24, 'general_ledger', '', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:33:54'),
(810, 25, 'profit_or_loss', '', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:34:26'),
(811, 26, 'trial_balance', '', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 152, 2, '2018-12-12 06:34:58'),
(812, 27, 'settings', '', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 152, 2, '2019-03-14 08:51:22'),
(813, 28, 'company_profile', '', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:53:27'),
(814, 29, 'paypal_setting', '', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:54:23'),
(815, 30, 'cost_factor', '', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:55:09'),
(816, 31, 'web_iframe_code', '', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:55:51'),
(817, 32, 'employee_and_access', '', '', 'settings', 7, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:56:43'),
(818, 33, 'users', NULL, 'users', 'settings', 1, 32, 1, 0, '', 1, 152, 2, '2019-03-14 08:57:12'),
(819, 34, 'menu_customization', '', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 152, 2, '2019-03-14 08:57:51'),
(820, 35, 'role_permission', '', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 152, 2, '2019-03-14 08:58:37'),
(821, 36, 'role_list', '', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 152, 2, '2019-03-14 08:59:59'),
(822, 37, 'add_user_role', '', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 152, 2, '2019-03-14 09:00:51'),
(823, 38, 'access_role', '', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 152, 2, '2019-03-14 09:01:32'),
(824, 39, 'sales_tax', '', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 152, 69, '2019-03-14 09:02:19'),
(825, 40, 'bulk_upload', NULL, 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 152, 2, '2019-03-20 09:03:23'),
(826, 41, 'quotation', '', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 152, 2, '2019-04-25 14:42:49'),
(827, 42, 'add_new_quotation', NULL, 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 152, 2, '2019-04-25 14:44:40'),
(828, 43, 'manage_quotation', NULL, 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 152, 2, '2019-04-25 14:45:35'),
(829, 44, 'header', '', '', 'header', 0, 0, 2, 0, '', 1, 152, 2, '2019-05-09 05:10:07'),
(830, 45, 'grid', '', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 152, 2, '2019-05-09 05:10:56'),
(831, 46, 'picture', '', '', 'header', 0, 44, 2, 0, '', 1, 152, 2, '2019-05-09 05:11:35'),
(832, 47, 'customers', '', 'customer-list', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 152, 2, '2019-05-09 05:14:18'),
(833, 48, 'orders', '', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 152, 2, '2019-05-09 05:15:52'),
(834, 49, 'appointment', '', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 152, 2, '2019-05-09 05:18:07'),
(835, 50, 'accounts', '', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 152, 67, '2019-05-09 05:18:52'),
(836, 51, 'paypal_settings', '', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 152, 2, '2019-05-09 05:20:28'),
(837, 52, 'knowledge_base', '', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 152, 2, '2019-05-09 05:23:04'),
(838, 53, 'company_profile', '', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 152, 2, '2019-05-09 05:25:33'),
(839, 54, 'my_account', '', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 152, 2, '2019-05-09 05:27:58'),
(840, 55, 'change_password', '', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 152, 2, '2019-05-09 05:29:05'),
(841, 56, 'my_orders', '', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 152, 2, '2019-05-09 05:31:35'),
(842, 57, 'logs', '', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 152, 2, '2019-05-09 05:32:54'),
(843, 59, 'dashboard', '', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 152, 2, '2019-05-18 10:43:26'),
(844, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 152, 2, '2019-05-19 11:57:20'),
(845, 61, 'payment_gateway', 'payment_gateway_korean', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 152, 2, '2019-05-22 09:21:00'),
(846, 62, 'payment_logs', 'my_language', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 152, 2, '2019-05-26 12:05:11'),
(847, 64, 'purchase_return', 'k_purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 152, 2, '2019-05-31 09:23:02'),
(848, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 152, 66, '2019-06-24 06:12:52'),
(849, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 152, 2, '2019-06-24 06:12:52'),
(850, 67, 'Gallery', 'Gallery', 'manage_c_gallery_image', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 152, 2, '2019-06-24 06:12:52'),
(851, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 7, 27, 1, 0, '', 1, 152, 2, '2019-06-24 06:12:52'),
(852, 69, 'SMS Configuration', 'SMS Configuration', 'c-sms-configure', 'settings', 10, 27, 1, 0, '', 1, 152, 2, '2019-06-24 06:12:52'),
(853, 70, 'Email Configuration', 'Email Configuration', 'c-mail-configure', 'settings', 10, 27, 1, 0, '', 1, 152, 2, '2019-06-24 06:12:52'),
(854, 71, 'Custom Mail', 'Custom Mail', 'c-email', 'settings', 8, 27, 1, 0, '', 1, 152, 2, '2019-06-24 06:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `c_notification_tbl`
--

CREATE TABLE `c_notification_tbl` (
  `id` int(11) NOT NULL,
  `notification_text` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `go_to_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_notification_tbl`
--

INSERT INTO `c_notification_tbl` (`id`, `notification_text`, `go_to_url`, `created_by`, `date`) VALUES
(5, 'Payment has been received for 1564250423QUWVC-002', 'c_level/invoice_receipt/receipt/1564250423QUWVC-002', 143, '2019-07-27'),
(6, 'Payment has been received for 1564250615AXH9C-002', 'c_level/invoice_receipt/receipt/1564250615AXH9C-002', 143, '2019-07-27'),
(7, 'Payment has been received for 1564251108PDU2C-004', 'c_level/invoice_receipt/receipt/1564251108PDU2C-004', 144, '2019-07-27'),
(8, 'Payment has been submited for 1564251108PDU2C-004', 'c_level/invoice_receipt/receipt/1564251108PDU2C-004', 144, '2019-07-27'),
(9, 'Payment has been received for 1564251588FVQHC-005', 'c_level/invoice_receipt/receipt/1564251588FVQHC-005', 143, '2019-07-27'),
(10, 'Payment has been submited for 1564251588FVQHC-005', 'c_level/invoice_receipt/receipt/1564251588FVQHC-005', 143, '2019-07-27'),
(11, 'Payment has been received for 1564251785ARPSC-006', 'c_level/invoice_receipt/receipt/1564251785ARPSC-006', 143, '2019-07-27'),
(12, 'Payment has been received for 1564252028E4DAC-007', 'c_level/invoice_receipt/receipt/1564252028E4DAC-007', 143, '2019-07-27'),
(13, 'Payment has been received for 1564252257QVF3C-006', 'c_level/invoice_receipt/receipt/1564252257QVF3C-006', 143, '2019-07-27'),
(14, 'Payment has been received for 1564252489UEBDC-007', 'c_level/invoice_receipt/receipt/1564252489UEBDC-007', 143, '2019-07-27'),
(15, 'Payment has been submited for 1564252489UEBDC-007', 'c_level/invoice_receipt/receipt/1564252489UEBDC-007', 143, '2019-07-27'),
(16, 'Payment has been received for 1564252649DBBLC-008', 'c_level/invoice_receipt/receipt/1564252649DBBLC-008', 143, '2019-07-27'),
(17, 'Payment has been received for 1564253146BWSAC-009', 'c_level/invoice_receipt/receipt/1564253146BWSAC-009', 143, '2019-07-27'),
(18, 'Payment has been received for 1564254122USGHC-010', 'c_level/invoice_receipt/receipt/1564254122USGHC-010', 143, '2019-07-27'),
(19, 'Payment has been received for 1564254204FUMFC-011', 'c_level/invoice_receipt/receipt/1564254204FUMFC-011', 143, '2019-07-27'),
(20, 'Payment has been submited for 1564251588FVQHC-005', 'c_level/invoice_receipt/receipt/1564251588FVQHC-005', 143, '2019-07-27'),
(21, 'Payment has been submited for 1564252649DBBLC-008', 'c_level/invoice_receipt/receipt/1564252649DBBLC-008', 143, '2019-07-27'),
(22, 'Payment has been submited for 1564251588FVQHC-005', 'c_level/invoice_receipt/receipt/1564251588FVQHC-005', 143, '2019-07-28'),
(23, 'Payment has been submited for 1564252649DBBLC-008', 'c_level/invoice_receipt/receipt/1564252649DBBLC-008', 143, '2019-07-28'),
(24, 'Payment has been submited for 1564311596CURLC-012', 'c_level/invoice_receipt/receipt/1564311596CURLC-012', 143, '2019-07-29'),
(25, 'Order Stage has been updated to Manufacturing for 1564251121r0Esb-003', '#', 144, '2019-07-31'),
(26, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(27, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(28, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(29, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(30, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(31, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(32, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(33, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(34, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(35, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(36, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(37, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(38, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(39, 'Payment has been received for 1564575953YHUUC-013', 'c_level/invoice_receipt/receipt/1564575953YHUUC-013', 144, '2019-07-31'),
(40, 'Payment has been submited for 15650042321GXNC-014', 'c_level/invoice_receipt/receipt/15650042321GXNC-014', 143, '2019-08-05'),
(41, 'Order Stage has been updated to Manufacturing for 1565085043U7P9B-011', '#', 150, '2019-08-06'),
(42, 'Order Stage has been updated to Manufacturing for 1565003748FB0YB-008', '#', 1, '2019-08-06'),
(43, 'Payment has been submited for 1565160813LAHMC-015', 'c_level/invoice_receipt/receipt/1565160813LAHMC-015', 152, '2019-08-07'),
(44, 'Order Stage has been updated to Quote for 1565085043U7P9B-011', '#', 150, '2019-08-07'),
(45, 'Order Stage has been updated to Manufacturing for 1565085043U7P9B-011', '#', 150, '2019-08-07'),
(46, 'Payment has been received for 1565172271RTPFC-016', 'c_level/invoice_receipt/receipt/1565172271RTPFC-016', 152, '2019-08-07'),
(47, 'Payment has been submited for 1565172271RTPFC-016', 'c_level/invoice_receipt/receipt/1565172271RTPFC-016', 152, '2019-08-07');

-- --------------------------------------------------------

--
-- Table structure for table `c_quatation_details`
--

CREATE TABLE `c_quatation_details` (
  `qd_id` int(11) NOT NULL,
  `fk_qt_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `win_no` int(11) NOT NULL,
  `win_width` float NOT NULL,
  `win_wfraction` varchar(15) DEFAULT NULL,
  `win_height` float NOT NULL,
  `win_htfraction` varchar(15) DEFAULT NULL,
  `win_categories` text NOT NULL,
  `win_created_on` datetime NOT NULL,
  `win_modified_on` datetime NOT NULL,
  `win_created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_quatation_details`
--

INSERT INTO `c_quatation_details` (`qd_id`, `fk_qt_id`, `room_no`, `room_name`, `win_no`, `win_width`, `win_wfraction`, `win_height`, `win_htfraction`, `win_categories`, `win_created_on`, `win_modified_on`, `win_created_by`) VALUES
(31, 15, 0, 'Guest', 1, 66, '1/2', 78, '1/2', '[{\"win_category\":\"1\",\"win_category_name\":\"Blinds\",\"win_product\":\"1\",\"win_product_name\":\"2 Inch Faux Blinds\",\"win_pattern\":\"1\",\"win_pattern_name\":\"Smooth\",\"win_price\":\"646\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"10\"},{\"win_category\":\"1\",\"win_category_name\":\"Blinds\",\"win_product\":\"2\",\"win_product_name\":\"2.5 Inch Shutter Blinds\",\"win_pattern\":\"4\",\"win_pattern_name\":\"None\",\"win_price\":\"775\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"10\"}]', '2019-07-27 18:30:35', '2019-07-27 18:30:35', 143),
(30, 15, 0, 'Guest', 0, 70, '1/2', 80, '1/2', '[{\"win_category\":\"1\",\"win_category_name\":\"Blinds\",\"win_product\":\"1\",\"win_product_name\":\"2 Inch Faux Blinds\",\"win_pattern\":\"1\",\"win_pattern_name\":\"Smooth\",\"win_price\":\"753\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"10\"},{\"win_category\":\"1\",\"win_category_name\":\"Blinds\",\"win_product\":\"2\",\"win_product_name\":\"2.5 Inch Shutter Blinds\",\"win_pattern\":\"4\",\"win_pattern_name\":\"None\",\"win_price\":\"904\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"10\"}]', '2019-07-27 18:30:35', '2019-07-27 18:30:35', 143),
(32, 16, 1, 'Kitchen', 0, 2, '3/8', 2, '3/4', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"10\",\"win_product_name\":\"main product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"10\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 10:34:11', '2019-08-08 10:34:11', 152),
(33, 16, 1, 'Kitchen', 1, 2, '3/4', 1, '3/4', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"10\",\"win_product_name\":\"main product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"10\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 10:34:11', '2019-08-08 10:34:11', 152),
(34, 17, 0, 'Study', 0, 2, '1/8', 2, '1/8', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"9\",\"win_product_name\":\"demo product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"0\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 10:41:00', '2019-08-08 10:41:00', 152),
(35, 17, 1, 'Living', 0, 1, '3/8', 1, '3/4', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"10\",\"win_product_name\":\"main product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"10\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 10:41:00', '2019-08-08 10:41:00', 152),
(36, 18, 0, 'Living', 0, 2, '3/8', 1, '3/8', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"9\",\"win_product_name\":\"demo product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"0\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 11:48:27', '2019-08-08 11:48:27', 152),
(37, 18, 0, 'Living', 1, 2, '1/8', 2, '3/8', '[{\"win_category\":\"6\",\"win_category_name\":\"Blinds\",\"win_product\":\"9\",\"win_product_name\":\"demo product\",\"win_pattern\":\"14\",\"win_pattern_name\":\"test pattern\",\"win_price\":\"0\",\"win_upcharges_array\":\"null\",\"win_upcharges\":0,\"win_discount_amt\":\"\"}]', '2019-08-08 11:48:27', '2019-08-08 11:48:27', 152);

-- --------------------------------------------------------

--
-- Table structure for table `c_quatation_table`
--

CREATE TABLE `c_quatation_table` (
  `qt_id` int(11) NOT NULL,
  `qt_cust_id` int(11) NOT NULL,
  `qt_cust_type` int(11) NOT NULL,
  `qt_subtotal` float NOT NULL,
  `total_discount` varchar(10) NOT NULL,
  `total_upcharge` varchar(10) NOT NULL,
  `qt_tax` float NOT NULL,
  `qt_grand_total` float NOT NULL,
  `qt_order_date` date NOT NULL,
  `qt_created_on` datetime NOT NULL,
  `qt_modified_on` datetime NOT NULL,
  `qt_created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_quatation_table`
--

INSERT INTO `c_quatation_table` (`qt_id`, `qt_cust_id`, `qt_cust_type`, `qt_subtotal`, `total_discount`, `total_upcharge`, `qt_tax`, `qt_grand_total`, `qt_order_date`, `qt_created_on`, `qt_modified_on`, `qt_created_by`) VALUES
(15, 24, 143, 2770.2, '307.80', '0.00', 8.25, 2998.74, '2019-07-27', '2019-07-27 18:30:35', '2019-07-27 18:30:35', 143),
(16, 30, 152, 20, '0.00', '0.00', 0, 20, '2019-08-08', '2019-08-08 10:34:11', '2019-08-08 10:34:11', 152),
(17, 30, 152, 10, '0.00', '0.00', 0, 10, '2019-08-08', '2019-08-08 10:41:00', '2019-08-08 10:41:00', 152),
(18, 30, 152, 0, '0.00', '0.00', 0, 0, '2019-08-08', '2019-08-08 11:48:27', '2019-08-08 11:48:27', 152);

-- --------------------------------------------------------

--
-- Table structure for table `c_us_state_tbl`
--

CREATE TABLE `c_us_state_tbl` (
  `state_id` int(11) NOT NULL,
  `shortcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` float DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_us_state_tbl`
--

INSERT INTO `c_us_state_tbl` (`state_id`, `shortcode`, `state_name`, `tax_rate`, `level_id`, `created_by`) VALUES
(1, 'TX', 'Texas', 8.25, 143, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

CREATE TABLE `gallery_image_tbl` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_image_tbl`
--

INSERT INTO `gallery_image_tbl` (`id`, `image`, `created_by`, `updated_by`) VALUES
(10, './assets/b_level/uploads/gallery/window-shutters-cost-guide.jpg', 1, 1),
(11, './assets/b_level/uploads/gallery/Solars_LivingRoom_Desktop_Carousel_1350x700.jpg', 1, 1),
(12, './assets/b_level/uploads/gallery/D985_40_352_1200.jpg', 1, 1),
(13, './assets/b_level/uploads/gallery/Screenshot_2019-07-31_at_7_25_38_AM.png', 1, 1),
(14, './assets/b_level/uploads/gallery/Screenshot_2019-07-31_at_7_25_46_AM.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tag_tbl`
--

CREATE TABLE `gallery_tag_tbl` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `tag_value` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_tag_tbl`
--

INSERT INTO `gallery_tag_tbl` (`id`, `image_id`, `tag_id`, `tag_value`, `created_date`, `updated_date`, `created_by`, `updated_by`) VALUES
(20, 10, 7, 'Shutters', '2019-07-26', '2019-07-26', 1, 1),
(21, 11, 7, 'Shades', '2019-07-26', '0000-00-00', 1, 1),
(22, 12, 7, 'Blinds 1', '2019-07-26', '2019-07-28', 1, 1),
(23, 13, 7, 'Der', '2019-07-31', '0000-00-00', 1, 1),
(24, 14, 7, 'Der', '2019-07-31', '0000-00-00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gateway_tbl`
--

CREATE TABLE `gateway_tbl` (
  `id` int(11) NOT NULL,
  `payment_gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default_status` int(11) NOT NULL,
  `level_type` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0=development, 1=production'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_row_column_price_tbl`
--

CREATE TABLE `group_row_column_price_tbl` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `row_data` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `column_data` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL COMMENT 'Unit of Measurement',
  `uom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `iframe_code_tbl`
--

CREATE TABLE `iframe_code_tbl` (
  `frame_id` int(11) NOT NULL,
  `iframe_code` text COLLATE utf8_unicode_ci NOT NULL,
  `level_type` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `iframe_code_tbl`
--

INSERT INTO `iframe_code_tbl` (`frame_id`, `iframe_code`, `level_type`, `created_by`, `updated_by`, `created_date`, `status`) VALUES
(1, '<h1>Iframe code generate</h1>', '', 2, 2, '2018-12-18 11:50:37', 1),
(2, '<h1>Admin 2 iframe code generate </h1>', '', 8, 0, '2018-12-19 06:10:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `log_info`
--

CREATE TABLE `log_info` (
  `row_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'FK of User_Info table',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'B or C',
  `is_admin` int(2) NOT NULL,
  `login_datetime` datetime NOT NULL,
  `logout_datetime` datetime NOT NULL,
  `last_login_ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `random_key` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log_info`
--

INSERT INTO `log_info` (`row_id`, `user_id`, `email`, `password`, `user_type`, `is_admin`, `login_datetime`, `logout_datetime`, `last_login_ip`, `random_key`) VALUES
(1, 1, 'info@bmsdecor.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-08-08 18:54:11', '2019-08-08 16:15:38', '127.0.0.1', ''),
(8, 141, 'info@bmslink.net', 'e10adc3949ba59abbe56e057f20f883e', 'c', 2, '2019-07-26 18:41:29', '0000-00-00 00:00:00', '219.91.253.114', ''),
(9, 142, 'rajiv@technoplusinc.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 1, '2019-07-27 16:51:09', '2019-07-27 17:18:51', '219.91.253.114', ''),
(10, 143, 'legacyblinds@aol.com', '26d015d11d40cee34a5fd4183d604161', 'c', 1, '2019-08-05 16:53:20', '2019-08-05 16:54:20', '127.0.0.1', ''),
(11, 144, 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 1, '2019-07-31 10:42:36', '2019-07-31 02:43:57', '123.136.187.107', ''),
(12, 145, 'admin1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 2, '2019-07-27 18:48:54', '2019-07-27 18:49:08', '219.91.253.114', ''),
(14, 147, 'admin2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 2, '2019-07-27 19:01:27', '2019-07-27 18:58:25', '219.91.253.114', ''),
(15, 148, 'admin3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 2, '2019-07-27 19:02:48', '0000-00-00 00:00:00', '219.91.253.114', ''),
(16, 149, 'rajiv@technoplusinc.com', 'a7a2bdbe3001bb940e73abef6c4aa951', 'c', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(17, 150, 'peter@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-08-08 16:53:41', '2019-08-08 16:48:48', '127.0.0.1', '127.0.0.1'),
(19, 152, 'first_customer@customer.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 1, '2019-08-08 16:48:59', '2019-08-08 18:53:45', '127.0.0.1', '');

-- --------------------------------------------------------

--
-- Table structure for table `mail_config_tbl`
--

CREATE TABLE `mail_config_tbl` (
  `id` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_host` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_port` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_user` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mailtype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_config_tbl`
--

INSERT INTO `mail_config_tbl` (`id`, `protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `mailtype`, `updated_by`, `updated_date`, `status`, `created_by`, `is_verified`) VALUES
(1, 'smtp', 'mail.bmsdecor.com', '2525', 'info@bmsdecor.com', 'Bmsd123#', 'html', 0, '2019-07-31 00:00:00', 1, 1, 0),
(2, 'smtp', 'mail.bmsdecor.com', '2525', 'c@bmsdecor.com', 'Bmsd123#', 'html', 144, '2019-07-31 00:00:00', 0, 1, 1),
(3, 'smtp', 'mail.bmsdecor.com', '2525', 'info@bmsdecor.com', 'Bmsd123#', 'html', 1, '2019-07-31 00:00:00', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_quatation_attributes`
--

CREATE TABLE `manufacturing_b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_quatation_tbl`
--

CREATE TABLE `manufacturing_b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_qutation_details`
--

CREATE TABLE `manufacturing_b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_shipment_data`
--

CREATE TABLE `manufacturing_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `customer_name` varchar(250) DEFAULT NULL,
  `customer_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufactur_data`
--

CREATE TABLE `manufactur_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `chk_option` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufactur_data`
--

INSERT INTO `manufactur_data` (`id`, `order_id`, `product_id`, `chk_option`) VALUES
(3, '1565003748FB0YB-008', 8, '{\"sc_chk\":\"1\",\"val_cut_chk\":null,\"chk\":null,\"hr\":null,\"br\":null,\"tb\":null,\"sc\":null,\"cl\":null}'),
(12, '15651559512QRGB-002', 10, '{\"sc_chk\":null,\"val_cut_chk\":\"1\",\"chk\":\"1\",\"hr\":null,\"br\":null,\"tb\":null,\"sc\":null,\"cl\":null}'),
(13, '1565085043U7P9B-011', 9, '{\"sc_chk\":null,\"val_cut_chk\":null,\"chk\":null,\"hr\":null,\"br\":null,\"tb\":null,\"sc\":null,\"cl\":null}'),
(14, '15651559502QJCB-002', 9, '{\"sc_chk\":\"1\",\"val_cut_chk\":null,\"chk\":null,\"hr\":null,\"br\":null,\"tb\":null,\"sc\":null,\"cl\":null}');

-- --------------------------------------------------------

--
-- Table structure for table `menusetup_tbl`
--

CREATE TABLE `menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(3) NOT NULL COMMENT '1 = left, 2 = system',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menusetup_tbl`
--

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 'customer', 'ê³ ê°', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 0, 2, '2018-12-12 06:14:46'),
(2, 'add_customer', 'ì¶”ê°€', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 0, 2, '2018-12-12 06:15:26'),
(3, 'customer_list', 'ê´€ë¦¬', 'customer-list', 'customer', 2, 1, 1, 0, '', 1, 0, 2, '2018-12-12 06:16:01'),
(4, 'order', 'ì£¼ë¬¸', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 0, 2, '2018-12-12 06:16:35'),
(5, 'new_order', 'ìƒˆë¡œìš´ ì§ˆì„œ', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 0, 48, '2018-12-12 06:17:40'),
(6, 'manage_order', '', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:18:26'),
(7, 'manage_invoice', '', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:18:56'),
(8, 'cancelled_order', '', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 0, 53, '2018-12-12 06:19:39'),
(9, 'customer_return', '', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:23:22'),
(10, 'track_order', '', 'track-order', 'order', 7, 0, 1, 0, '', 1, 0, 2, '2018-12-12 06:23:48'),
(11, 'accounts', 'Test', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 0, 2, '2018-12-12 06:24:27'),
(12, 'chart_of_account', NULL, 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 0, 2, '2018-12-12 06:25:02'),
(13, 'vouchers', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 0, 2, '2018-12-12 06:25:40'),
(14, 'debit_voucher', '', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:27:09'),
(15, 'credit_voucher', '', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:27:42'),
(16, 'journal_voucher', '', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:28:40'),
(17, 'contra_voucher', '', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:29:13'),
(18, 'voucher_approval', '', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:29:45'),
(19, 'account_report', NULL, '', 'account', NULL, 11, 1, 0, NULL, 1, 0, 2, '2018-12-12 06:31:35'),
(20, 'cash_book', '', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:03'),
(21, 'bank_book', '', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:32'),
(22, 'cash_flow', '', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:54'),
(23, 'voucher_reports', '', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:33:23'),
(24, 'general_ledger', '', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:33:54'),
(25, 'profit_or_loss', '', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:34:26'),
(26, 'trial_balance', '', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:34:58'),
(27, 'settings', '', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 0, 2, '2019-03-14 08:51:22'),
(28, 'company_profile', '', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:53:27'),
(29, 'paypal_setting', '', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:54:23'),
(30, 'cost_factor', '', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:55:09'),
(31, 'web_iframe_code', '', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:55:51'),
(32, 'employee_and_access', '', '', 'settings', 7, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:56:43'),
(33, 'users', NULL, 'users', 'settings', 1, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:57:12'),
(34, 'menu_customization', '', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:57:51'),
(35, 'role_permission', '', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:58:37'),
(36, 'role_list', '', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:59:59'),
(37, 'add_user_role', '', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 0, 2, '2019-03-14 09:00:51'),
(38, 'access_role', '', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 0, 2, '2019-03-14 09:01:32'),
(39, 'sales_tax', '', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 0, 69, '2019-03-14 09:02:19'),
(40, 'bulk_upload', NULL, 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 0, 2, '2019-03-20 09:03:23'),
(41, 'quotation', '', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 0, 2, '2019-04-25 14:42:49'),
(42, 'add_new_quotation', NULL, 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 0, 2, '2019-04-25 14:44:40'),
(43, 'manage_quotation', NULL, 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 0, 2, '2019-04-25 14:45:35'),
(44, 'header', '', '', 'header', 0, 0, 2, 0, '', 1, 0, 2, '2019-05-09 05:10:07'),
(45, 'grid', '', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 0, 2, '2019-05-09 05:10:56'),
(46, 'picture', '', '', 'header', 0, 44, 2, 0, '', 1, 0, 2, '2019-05-09 05:11:35'),
(47, 'customers', '', 'customer-list', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 0, 2, '2019-05-09 05:14:18'),
(48, 'orders', '', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 0, 2, '2019-05-09 05:15:52'),
(49, 'appointment', '', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 0, 2, '2019-05-09 05:18:07'),
(50, 'accounts', '', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 0, 67, '2019-05-09 05:18:52'),
(51, 'paypal_settings', '', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 0, 2, '2019-05-09 05:20:28'),
(52, 'knowledge_base', '', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 0, 2, '2019-05-09 05:23:04'),
(53, 'company_profile', '', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 0, 2, '2019-05-09 05:25:33'),
(54, 'my_account', '', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 0, 2, '2019-05-09 05:27:58'),
(55, 'change_password', '', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 0, 2, '2019-05-09 05:29:05'),
(56, 'my_orders', '', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 0, 2, '2019-05-09 05:31:35'),
(57, 'logs', '', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 0, 2, '2019-05-09 05:32:54'),
(59, 'dashboard', '', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 0, 2, '2019-05-18 10:43:26'),
(60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 0, 2, '2019-05-19 11:57:20'),
(61, 'payment_gateway', 'payment_gateway_korean', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 0, 2, '2019-05-22 09:21:00'),
(62, 'payment_logs', 'my_language', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 0, 2, '2019-05-26 12:05:11'),
(64, 'purchase_return', 'k_purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 0, 2, '2019-05-31 09:23:02'),
(65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 0, 66, '2019-06-24 06:12:52'),
(66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 0, 2, '2019-06-24 06:12:52'),
(67, 'Gallery', 'Gallery', 'manage_c_gallery_image', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 0, 2, '2019-06-24 06:12:52'),
(68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 7, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(69, 'SMS Configuration', 'SMS Configuration', 'c-sms-configure', 'settings', 10, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(70, 'Email Configuration', 'Email Configuration', 'c-mail-configure', 'settings', 10, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(71, 'Custom Mail', 'Custom Mail', 'c-email', 'settings', 8, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `order_return_details`
--

CREATE TABLE `order_return_details` (
  `id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL,
  `replace_qty` int(11) NOT NULL,
  `replace_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_return_tbl`
--

CREATE TABLE `order_return_tbl` (
  `return_id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `return_date` date NOT NULL,
  `return_comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `returned_by` int(11) NOT NULL,
  `is_approved` tinyint(2) NOT NULL COMMENT '0 = pending, 1 = approved, 2 = cancel',
  `approved_by` int(11) NOT NULL,
  `rma_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `c_approval` tinyint(4) NOT NULL DEFAULT '0',
  `c_approval_by` int(11) NOT NULL,
  `c_approval_date` date NOT NULL,
  `level_id` int(11) NOT NULL,
  `resend_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `resend_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT '1971-01-01 00:00:01'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pattern_model_tbl`
--

CREATE TABLE `pattern_model_tbl` (
  `pattern_model_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `pattern_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pattern_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pattern_model_tbl`
--

INSERT INTO `pattern_model_tbl` (`pattern_model_id`, `category_id`, `pattern_type`, `pattern_name`, `status`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(9, 1, 'Pattern', 'Smooth', 1, 1, 0, '2019-07-29', '0000-00-00'),
(10, 1, 'Pattern', 'Pro Grained', 1, 1, 1, '2019-07-29', '2019-07-29'),
(11, 1, 'Pattern', 'Pro Blast', 1, 1, 1, '2019-07-29', '2019-07-29'),
(12, 1, 'Pattern', 'Beveled', 1, 1, 0, '2019-07-29', '0000-00-00'),
(13, 1, 'Pattern', 'None', 1, 1, 0, '2019-07-29', '0000-00-00'),
(14, 6, 'Pattern', 'first pattern', 1, 150, 150, '2019-08-05', '2019-08-08'),
(15, 6, 'Pattern', 'Second pattern', 1, 150, 0, '2019-08-08', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_card_tbl`
--

CREATE TABLE `payment_card_tbl` (
  `id` int(11) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `card_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cvv` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_check_tbl`
--

CREATE TABLE `payment_check_tbl` (
  `id` int(11) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `check_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `check_image` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_check_tbl`
--

INSERT INTO `payment_check_tbl` (`id`, `payment_id`, `check_number`, `check_image`) VALUES
(1, 51, '212121', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_tbl`
--

CREATE TABLE `payment_tbl` (
  `payment_id` bigint(20) NOT NULL,
  `quotation_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` float NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_tbl`
--

INSERT INTO `payment_tbl` (`payment_id`, `quotation_id`, `payment_method`, `paid_amount`, `description`, `payment_date`, `created_by`, `create_date`, `updated_by`, `update_date`) VALUES
(5, '1564250423QUWVC-002', 'cash', 2239.8, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(6, '1564250615AXH9C-002', 'cash', 824.71, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(7, '1564251108PDU2C-004', 'cash', 376.5, '', '2019-07-27', 144, '2019-07-27', 0, '0000-00-00'),
(8, '1564251121r0Esb-003', 'cash', 366.81, 'Payment given for Order#1564251121r0Esb-003', '2019-07-27', 144, '2019-07-27', 0, '0000-00-00'),
(9, '1564251588FVQHC-005', 'cash', 600, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(10, '1564251614bQbOb-004', 'cash', 655.18, 'Payment given for Order#1564251614bQbOb-004', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(11, '1564251785ARPSC-006', 'cash', 1506.19, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(12, '1564252028E4DAC-007', 'cash', 1506.19, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(13, '1564252257QVF3C-006', 'card', 733.61, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(14, '1564252489UEBDC-007', 'cash', 324.43, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(15, '1564252509yG6Fb-005', 'cash', 324.43, 'Payment given for Order#1564252509yG6Fb-005', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(16, '1564252649DBBLC-008', 'card', 1112, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(17, '1564253146BWSAC-009', 'cash', 727.98, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(18, '1564254122USGHC-010', 'cash', 506.5, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(19, '1564254204FUMFC-011', 'cash', 457.36, '', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(20, '1564251614bQbOb-004', 'cash', 655.19, 'Payment given for Order#1564251614bQbOb-004', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(21, '1564252668pWO0b-006', 'cash', 1212.45, 'Payment given for Order#1564252668pWO0b-006', '2019-07-27', 143, '2019-07-27', 0, '0000-00-00'),
(22, '1564251614bQbOb-004', 'cash', 0, 'Payment given for Order#1564251614bQbOb-004', '2019-07-28', 143, '2019-07-28', 0, '0000-00-00'),
(23, '1564252668pWO0b-006', 'cash', 1212.45, 'Payment given for Order#1564252668pWO0b-006', '2019-07-28', 143, '2019-07-28', 0, '0000-00-00'),
(24, '1564395907fl8bb-007', 'cash', 97.86, 'Payment given for Order#1564395907fl8bb-007', '2019-07-29', 143, '2019-07-29', 0, '0000-00-00'),
(25, '1564575953YHUUC-013', 'cash', 562.5, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(26, '1564575953YHUUC-013', 'cash', 50, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(27, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(28, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(29, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(30, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(31, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(32, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(33, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(34, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(35, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(36, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(37, '1564575953YHUUC-013', 'cash', 10, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(38, '1564575953YHUUC-013', 'cash', 20, '', '2019-07-31', 144, '2019-07-31', 0, '0000-00-00'),
(39, '1565004252lYLjb-009', 'cash', 9.74, 'Payment given for Order#1565004252lYLjb-009', '2019-08-05', 143, '2019-08-05', 0, '0000-00-00'),
(40, '156500641433GQB-002', 'cash', 1, '', '2019-08-05', 150, '2019-08-05', 0, '0000-00-00'),
(41, '156500641433GQB-002', 'cash', 1, '', '2019-08-05', 1, '2019-08-05', 0, '0000-00-00'),
(42, '156500641433GQB-002', 'cash', 18, '', '2019-08-05', 1, '2019-08-05', 0, '0000-00-00'),
(43, '1565160847vpO7b-016', 'cash', 43392.3, 'Payment given for Order#1565160847vpO7b-016', '2019-08-07', 152, '2019-08-07', 0, '0000-00-00'),
(44, '1565161008PG0SB-004', 'cash', 100, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(45, '1565172271RTPFC-016', 'cash', 348.09, '', '2019-08-07', 152, '2019-08-07', 0, '0000-00-00'),
(46, '1565172322hiHqb-017', 'cash', 313.28, 'Payment given for Order#1565172322hiHqb-017', '2019-08-07', 152, '2019-08-07', 0, '0000-00-00'),
(47, '1565180117RILXB-002', 'cash', 350305000000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(48, '1565180117RILXB-002', 'cash', 350305000000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(49, '1565180117RILXB-002', 'cash', 82419800, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(50, '1565180117RILXB-002', 'cash', 16484000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(51, '1565180117RILXB-002', 'check', 164840000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(52, '1565180117RILXB-002', 'cash', 24977000, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(53, '1565180117RILXB-002', 'cash', 606151, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(54, '1565180117RILXB-002', 'cash', 606051, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(55, '1565180117RILXB-002', 'cash', 100, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(56, '1565180117RILXB-002', 'cash', 100, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(57, '1565180117RILXB-002', 'cash', 100, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(58, '1565180117RILXB-002', 'cash', 735807000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(59, '1565180117RILXB-002', 'cash', 288721000, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(60, '1565180117RILXB-002', 'cash', 350306000000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00'),
(61, '1565180117RILXB-002', 'cash', 1, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(62, '1565180117RILXB-002', 'cash', 577401000, '', '2019-08-07', 150, '2019-08-07', 0, '0000-00-00'),
(63, '1565180117RILXB-002', 'cash', 48532000, '', '2019-08-07', 1, '2019-08-07', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `price_chaild_style`
--

CREATE TABLE `price_chaild_style` (
  `id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `row` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_chaild_style`
--

INSERT INTO `price_chaild_style` (`id`, `style_id`, `row`) VALUES
(1, 1, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',78\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(2, 1, '30\'\',137,176,211,247,282,313,352,386,413,449,481,512,550,605,647'),
(3, 1, '36\'\',169,211,247,283,325,352,389,428,455,493,544,576,604,664,710'),
(4, 1, '42\'\',205,232,270,310,354,395,437,481,505,548,599,625,660,726,777'),
(5, 1, '48\'\',211,274,293,329,377,424,475,522,527,571,654,702,749,824,881'),
(6, 1, '54\'\',222,278,318,364,416,467,520,572,604,654,722,788,826,908,972'),
(7, 1, '60\'\',247,299,337,401,457,502,559,614,676,731,773,828,882,970,1038'),
(8, 1, '66\'\',272,328,354,433,497,547,605,666,737,799,859,922,983,1081,1157'),
(9, 1, '72\'\',296,344,401,463,530,587,649,713,796,860,913,978,1042,1146,1226'),
(10, 1, '78\'\',310,359,431,508,580,640,704,775,848,918,983,1054,1124,1237,1323'),
(11, 1, '84\'\',322,379,455,544,620,678,767,845,904,978,1060,1135,1211,1332,1425'),
(12, 1, '90\'\',344,403,484,565,646,727,806,887,976,1056,1129,1210,1289,1418,1517'),
(13, 1, '96\'\',353,418,502,581,664,764,834,917,1007,1090,1181,1265,1350,1485,1589'),
(14, 1, '102\'\',361,427,512,608,696,781,874,960,1058,1147,1237,1326,1414,1555,1664'),
(15, 1, '108\'\',386,479,575,643,734,833,925,1018,1139,1234,1284,1376,1468,1614,1727'),
(16, 2, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',78\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(17, 2, '30\'\',114,147,176,206,235,261,293,322,344,374,401,427,458,504,539'),
(18, 2, '36\'\',141,176,206,236,271,293,324,357,379,411,453,480,503,553,592'),
(19, 2, '42\'\',171,193,225,258,295,329,364,401,421,457,499,521,550,605,647'),
(20, 2, '48\'\',176,228,244,274,314,353,396,435,439,476,545,585,624,686,734'),
(21, 2, '54\'\',185,232,265,303,347,389,433,477,503,545,602,657,688,757,810'),
(22, 2, '60\'\',206,249,281,334,381,418,466,512,563,609,644,690,735,809,865'),
(23, 2, '66\'\',227,273,295,361,414,456,504,555,614,666,716,768,819,901,964'),
(24, 2, '72\'\',247,287,334,386,442,489,541,594,663,717,761,815,868,955,1022'),
(25, 2, '78\'\',258,299,359,423,483,533,587,646,707,765,819,878,937,1031,1103'),
(26, 2, '84\'\',268,316,379,453,517,565,639,704,753,815,883,946,1009,1110,1188'),
(27, 2, '90\'\',287,336,403,471,538,606,672,739,813,880,941,1008,1074,1181,1264'),
(28, 2, '96\'\',294,348,418,484,553,637,695,764,839,908,984,1054,1125,1238,1324'),
(29, 2, '102\'\',301,356,427,507,580,651,728,800,882,956,1031,1105,1178,1296,1387'),
(30, 2, '108\'\',322,399,479,536,612,694,771,848,949,1028,1070,1147,1223,1345,1439'),
(31, 1367, ',   24\'\',   30\'\',   36\'\',   42\'\',   48\'\',   54\'\',   60\'\',   66\'\',   72\'\',   78\'\',   84\'\',   90\'\',   96\'\',102\'\',108\'\''),
(32, 1367, '   30\'\',201,241,285,318,335,379,444,460,475,574,610,683,759,857,960'),
(33, 1367, '   36\'\',233,273,316,354,375,435,503,526,540,631,693,759,844,953,1068'),
(34, 1367, '   42\'\',255,301,349,393,415,485,560,576,608,709,768,858,953,1076,1205'),
(35, 1367, '   48\'\',276,330,381,429,460,536,615,651,683,786,860,969,1076,1216,1362'),
(36, 1367, '   54\'\',305,363,414,469,503,593,666,724,766,881,950,1066,1185,1339,1500'),
(37, 1367, '   60\'\',335,393,450,509,549,648,721,786,833,956,1033,1160,1288,1455,1629'),
(38, 1367, '   66\'\',364,424,484,551,604,708,793,863,915,1045,1124,1305,1450,1639,1835'),
(39, 1367, '   72\'\',398,450,516,593,655,768,864,914,973,1110,1198,1349,1499,1694,1897'),
(40, 1367, '   78\'\',411,460,531,608,676,789,889,945,1040,1184,1281,1439,1599,1807,2023'),
(41, 1367, '   84\'\',443,494,565,654,726,831,905,1011,1118,1266,1366,1539,1709,1931,2163'),
(42, 1367, '   90\'\',473,524,599,694,769,879,956,1074,1184,1330,1455,1638,1820,2057,2303'),
(43, 1367, '   96\'\',490,541,618,723,803,925,1045,1146,1259,1426,1544,1736,1928,2178,2439'),
(44, 1367, '  102\'\',513,571,653,769,854,983,1085,1214,1339,1513,1636,1844,2048,2314,2591'),
(45, 1367, '  108\'\',539,594,685,806,898,1033,1136,1278,1408,1586,1718,1933,2148,2427,2718'),
(46, 1367, '  114\'\',563,621,724,853,948,1086,1201,1344,1485,1678,1808,2033,2259,2552,2859'),
(47, 1367, '  120\'\',591,651,754,891,994,1138,1226,1408,1549,1744,1893,2130,2366,2674,2995'),
(48, 1367, '  126\'\',610,680,798,936,1043,1188,1315,1478,1626,1720,1978,2235,2483,2805,3142'),
(49, 3, ',   24\'\',   30\'\',   36\'\',   42\'\',   48\'\',   54\'\',   60\'\',   66\'\',   72\'\',   78\'\',   84\'\',   90\'\',   96\'\',102\'\',108\'\''),
(50, 3, '   30\'\',201,241,285,318,335,379,444,460,475,574,610,683,759,857,960'),
(51, 3, '   36\'\',233,273,316,354,375,435,503,526,540,631,693,759,844,953,1068'),
(52, 3, '   42\'\',255,301,349,393,415,485,560,576,608,709,768,858,953,1076,1205'),
(53, 3, '   48\'\',276,330,381,429,460,536,615,651,683,786,860,969,1076,1216,1362'),
(54, 3, '   54\'\',305,363,414,469,503,593,666,724,766,881,950,1066,1185,1339,1500'),
(55, 3, '   60\'\',335,393,450,509,549,648,721,786,833,956,1033,1160,1288,1455,1629'),
(56, 3, '   66\'\',364,424,484,551,604,708,793,863,915,1045,1124,1305,1450,1639,1835'),
(57, 3, '   72\'\',398,450,516,593,655,768,864,914,973,1110,1198,1349,1499,1694,1897'),
(58, 3, '   78\'\',411,460,531,608,676,789,889,945,1040,1184,1281,1439,1599,1807,2023'),
(59, 3, '   84\'\',443,494,565,654,726,831,905,1011,1118,1266,1366,1539,1709,1931,2163'),
(60, 3, '   90\'\',473,524,599,694,769,879,956,1074,1184,1330,1455,1638,1820,2057,2303'),
(61, 3, '   96\'\',490,541,618,723,803,925,1045,1146,1259,1426,1544,1736,1928,2178,2439'),
(62, 3, '  102\'\',513,571,653,769,854,983,1085,1214,1339,1513,1636,1844,2048,2314,2591'),
(63, 3, '  108\'\',539,594,685,806,898,1033,1136,1278,1408,1586,1718,1933,2148,2427,2718'),
(64, 3, '  114\'\',563,621,724,853,948,1086,1201,1344,1485,1678,1808,2033,2259,2552,2859'),
(65, 3, '  120\'\',591,651,754,891,994,1138,1226,1408,1549,1744,1893,2130,2366,2674,2995'),
(66, 3, '  126\'\',610,680,798,936,1043,1188,1315,1478,1626,1720,1978,2235,2483,2805,3142'),
(67, 4, ',   24\'\',   30\'\',   36\'\',   42\'\',   48\'\',   54\'\',   60\'\',   66\'\',   72\'\',   78\'\',   84\'\',   90\'\',   96\'\',102\'\',108\'\''),
(68, 4, '   30\'\',161,193,228,254,268,303,355,368,380,459,488,546,607,686,768'),
(69, 4, '   36\'\',186,218,253,283,300,348,402,421,432,505,554,607,675,763,854'),
(70, 4, '   42\'\',204,241,279,314,332,388,448,461,486,567,614,686,762,861,964'),
(71, 4, '   48\'\',221,264,305,343,368,429,492,521,546,629,688,775,861,973,1090'),
(72, 4, '   54\'\',244,290,331,375,402,474,533,579,613,705,760,853,948,1071,1200'),
(73, 4, '   60\'\',268,314,360,407,439,518,577,629,666,765,826,928,1030,1164,1304'),
(74, 4, '   66\'\',291,339,387,441,483,566,634,690,732,836,899,1044,1160,1311,1468'),
(75, 4, '   72\'\',318,360,413,474,524,614,691,731,778,888,958,1079,1199,1355,1517'),
(76, 4, '   78\'\',329,368,425,486,541,631,711,756,832,947,1025,1151,1279,1445,1619'),
(77, 4, '   84\'\',354,395,452,523,581,665,724,809,894,1013,1093,1231,1367,1545,1730'),
(78, 4, '   90\'\',378,419,479,555,615,703,765,859,947,1064,1164,1310,1456,1645,1843'),
(79, 4, '   96\'\',392,433,494,578,642,740,836,917,1007,1141,1235,1389,1542,1742,1952'),
(80, 4, '  102\'\',410,457,522,615,683,786,868,971,1071,1210,1309,1475,1638,1851,2073'),
(81, 4, '  108\'\',431,475,548,645,718,826,909,1022,1126,1269,1374,1546,1718,1941,2174'),
(82, 4, '  114\'\',450,497,579,682,758,869,961,1075,1188,1342,1446,1626,1807,2042,2287'),
(83, 4, '  120\'\',473,521,603,713,795,910,981,1126,1239,1395,1514,1704,1893,2139,2396'),
(84, 4, '  126\'\',488,544,638,749,834,950,1052,1182,1301,1376,1582,1788,1986,2244,2513'),
(100, 5, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',84\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(101, 5, '30\'\',198,256,306,358,409,454,510,560,599,90\'\',698,743,797,877,938'),
(102, 5, '36\'\',245,306,358,411,472,510,564,621,659,96\'\',788,835,875,963,1030'),
(103, 5, '42\'\',298,336,392,449,513,572,633,698,733,102\'\',868,907,957,1053,1126'),
(104, 5, '48\'\',306,397,425,477,546,614,689,757,764,108\'\',948,1018,1086,1194,1278'),
(105, 5, '54\'\',322,404,461,527,604,677,753,830,875,948,1047,1143,1197,1317,1409'),
(106, 5, '60\'\',358,433,489,581,663,727,811,891,980,1060,1121,1201,1279,1407,1505'),
(107, 5, '66\'\',395,475,513,628,720,793,877,966,1068,1159,1246,1336,1425,1568,1677'),
(108, 5, '72\'\',430,499,581,672,769,851,941,1034,1154,1248,1324,1418,1510,1661,1778'),
(109, 5, '78\'\',449,520,625,736,840,927,1021,1124,1230,1331,1425,1528,1630,1793,1919'),
(110, 5, '84\'\',466,550,659,788,900,983,1112,1225,1310,1418,1536,1646,1756,1931,2066'),
(111, 5, '90\'\',499,585,701,820,936,1054,1169,1286,1415,1531,1637,1754,1869,2056,2200'),
(112, 5, '96\'\',512,606,727,842,962,1108,1209,1329,1460,1580,1712,1834,1958,2153,2304'),
(113, 5, '102\'\',524,619,743,882,1009,1133,1267,1392,1535,1663,1794,1923,2050,2255,2413'),
(114, 5, '108\'\',560,694,833,933,1065,1208,1342,1476,1651,1789,1862,1996,2128,2341,2505'),
(115, 6, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(116, 6, '0\'\',182,205,228,251,274,296,319,342,365,388,410,433,456,479,502,524,547,570'),
(117, 7, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(118, 7, '0\'\',205,230,256,281,307,332,358,384,409,435,460,486,512,537,563,588,614,639'),
(119, 8, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(120, 8, '0\'\',147,166,184,202,221,239,258,276,294,313,331,350,368,386,405,423,442,460'),
(121, 9, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(122, 9, '0\'\',91,102,113,125,136,147,159,170,181,193,204,215,227,238,249,261,272,284'),
(123, 10, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(124, 10, '0\'\',243,273,304,334,364,395,425,455,486,516,546,577,607,638,668,698,729,759'),
(125, 11, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(126, 11, '0\'\',243,273,304,334,364,395,425,455,486,516,546,577,607,638,668,698,729,759');

-- --------------------------------------------------------

--
-- Table structure for table `price_model_mapping_tbl`
--

CREATE TABLE `price_model_mapping_tbl` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pattern_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `price_style`
--

CREATE TABLE `price_style` (
  `row_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_style`
--

INSERT INTO `price_style` (`row_id`, `style_id`, `row`, `col`, `price`) VALUES
(1, 1, 24, 30, 137),
(2, 1, 30, 30, 176),
(3, 1, 36, 30, 211),
(4, 1, 42, 30, 247),
(5, 1, 48, 30, 282),
(6, 1, 54, 30, 313),
(7, 1, 60, 30, 352),
(8, 1, 66, 30, 386),
(9, 1, 72, 30, 413),
(10, 1, 84, 30, 90),
(11, 1, 84, 30, 481),
(12, 1, 90, 30, 512),
(13, 1, 96, 30, 550),
(14, 1, 102, 30, 605),
(15, 1, 108, 30, 647),
(16, 1, 24, 36, 169),
(17, 1, 30, 36, 211),
(18, 1, 36, 36, 247),
(19, 1, 42, 36, 283),
(20, 1, 48, 36, 325),
(21, 1, 54, 36, 352),
(22, 1, 60, 36, 389),
(23, 1, 66, 36, 428),
(24, 1, 72, 36, 455),
(25, 1, 84, 36, 96),
(26, 1, 84, 36, 544),
(27, 1, 90, 36, 576),
(28, 1, 96, 36, 604),
(29, 1, 102, 36, 664),
(30, 1, 108, 36, 710),
(31, 1, 24, 42, 205),
(32, 1, 30, 42, 232),
(33, 1, 36, 42, 270),
(34, 1, 42, 42, 310),
(35, 1, 48, 42, 354),
(36, 1, 54, 42, 395),
(37, 1, 60, 42, 437),
(38, 1, 66, 42, 481),
(39, 1, 72, 42, 505),
(40, 1, 84, 42, 102),
(41, 1, 84, 42, 599),
(42, 1, 90, 42, 625),
(43, 1, 96, 42, 660),
(44, 1, 102, 42, 726),
(45, 1, 108, 42, 777),
(46, 1, 24, 48, 211),
(47, 1, 30, 48, 274),
(48, 1, 36, 48, 293),
(49, 1, 42, 48, 329),
(50, 1, 48, 48, 377),
(51, 1, 54, 48, 424),
(52, 1, 60, 48, 475),
(53, 1, 66, 48, 522),
(54, 1, 72, 48, 527),
(55, 1, 84, 48, 108),
(56, 1, 84, 48, 654),
(57, 1, 90, 48, 702),
(58, 1, 96, 48, 749),
(59, 1, 102, 48, 824),
(60, 1, 108, 48, 881),
(61, 1, 24, 54, 222),
(62, 1, 30, 54, 278),
(63, 1, 36, 54, 318),
(64, 1, 42, 54, 364),
(65, 1, 48, 54, 416),
(66, 1, 54, 54, 467),
(67, 1, 60, 54, 520),
(68, 1, 66, 54, 572),
(69, 1, 72, 54, 604),
(70, 1, 84, 54, 654),
(71, 1, 84, 54, 722),
(72, 1, 90, 54, 788),
(73, 1, 96, 54, 826),
(74, 1, 102, 54, 908),
(75, 1, 108, 54, 972),
(76, 1, 24, 60, 247),
(77, 1, 30, 60, 299),
(78, 1, 36, 60, 337),
(79, 1, 42, 60, 401),
(80, 1, 48, 60, 457),
(81, 1, 54, 60, 502),
(82, 1, 60, 60, 559),
(83, 1, 66, 60, 614),
(84, 1, 72, 60, 676),
(85, 1, 84, 60, 731),
(86, 1, 84, 60, 773),
(87, 1, 90, 60, 828),
(88, 1, 96, 60, 882),
(89, 1, 102, 60, 970),
(90, 1, 108, 60, 1038),
(91, 1, 24, 66, 272),
(92, 1, 30, 66, 328),
(93, 1, 36, 66, 354),
(94, 1, 42, 66, 433),
(95, 1, 48, 66, 497),
(96, 1, 54, 66, 547),
(97, 1, 60, 66, 605),
(98, 1, 66, 66, 666),
(99, 1, 72, 66, 737),
(100, 1, 84, 66, 799),
(101, 1, 84, 66, 859),
(102, 1, 90, 66, 922),
(103, 1, 96, 66, 983),
(104, 1, 102, 66, 1081),
(105, 1, 108, 66, 1157),
(106, 1, 24, 72, 296),
(107, 1, 30, 72, 344),
(108, 1, 36, 72, 401),
(109, 1, 42, 72, 463),
(110, 1, 48, 72, 530),
(111, 1, 54, 72, 587),
(112, 1, 60, 72, 649),
(113, 1, 66, 72, 713),
(114, 1, 72, 72, 796),
(115, 1, 84, 72, 860),
(116, 1, 84, 72, 913),
(117, 1, 90, 72, 978),
(118, 1, 96, 72, 1042),
(119, 1, 102, 72, 1146),
(120, 1, 108, 72, 1226),
(121, 1, 24, 78, 310),
(122, 1, 30, 78, 359),
(123, 1, 36, 78, 431),
(124, 1, 42, 78, 508),
(125, 1, 48, 78, 580),
(126, 1, 54, 78, 640),
(127, 1, 60, 78, 704),
(128, 1, 66, 78, 775),
(129, 1, 72, 78, 848),
(130, 1, 84, 78, 918),
(131, 1, 84, 78, 983),
(132, 1, 90, 78, 1054),
(133, 1, 96, 78, 1124),
(134, 1, 102, 78, 1237),
(135, 1, 108, 78, 1323),
(136, 1, 24, 84, 322),
(137, 1, 30, 84, 379),
(138, 1, 36, 84, 455),
(139, 1, 42, 84, 544),
(140, 1, 48, 84, 620),
(141, 1, 54, 84, 678),
(142, 1, 60, 84, 767),
(143, 1, 66, 84, 845),
(144, 1, 72, 84, 904),
(145, 1, 84, 84, 978),
(146, 1, 84, 84, 1060),
(147, 1, 90, 84, 1135),
(148, 1, 96, 84, 1211),
(149, 1, 102, 84, 1332),
(150, 1, 108, 84, 1425),
(151, 1, 24, 90, 344),
(152, 1, 30, 90, 403),
(153, 1, 36, 90, 484),
(154, 1, 42, 90, 565),
(155, 1, 48, 90, 646),
(156, 1, 54, 90, 727),
(157, 1, 60, 90, 806),
(158, 1, 66, 90, 887),
(159, 1, 72, 90, 976),
(160, 1, 84, 90, 1056),
(161, 1, 84, 90, 1129),
(162, 1, 90, 90, 1210),
(163, 1, 96, 90, 1289),
(164, 1, 102, 90, 1418),
(165, 1, 108, 90, 1517),
(166, 1, 24, 96, 353),
(167, 1, 30, 96, 418),
(168, 1, 36, 96, 502),
(169, 1, 42, 96, 581),
(170, 1, 48, 96, 664),
(171, 1, 54, 96, 764),
(172, 1, 60, 96, 834),
(173, 1, 66, 96, 917),
(174, 1, 72, 96, 1007),
(175, 1, 84, 96, 1090),
(176, 1, 84, 96, 1181),
(177, 1, 90, 96, 1265),
(178, 1, 96, 96, 1350),
(179, 1, 102, 96, 1485),
(180, 1, 108, 96, 1589),
(181, 1, 24, 102, 361),
(182, 1, 30, 102, 427),
(183, 1, 36, 102, 512),
(184, 1, 42, 102, 608),
(185, 1, 48, 102, 696),
(186, 1, 54, 102, 781),
(187, 1, 60, 102, 874),
(188, 1, 66, 102, 960),
(189, 1, 72, 102, 1058),
(190, 1, 84, 102, 1147),
(191, 1, 84, 102, 1237),
(192, 1, 90, 102, 1326),
(193, 1, 96, 102, 1414),
(194, 1, 102, 102, 1555),
(195, 1, 108, 102, 1664),
(196, 1, 24, 108, 386),
(197, 1, 30, 108, 479),
(198, 1, 36, 108, 575),
(199, 1, 42, 108, 643),
(200, 1, 48, 108, 734),
(201, 1, 54, 108, 833),
(202, 1, 60, 108, 925),
(203, 1, 66, 108, 1018),
(204, 1, 72, 108, 1139),
(205, 1, 84, 108, 1234),
(206, 1, 84, 108, 1284),
(207, 1, 90, 108, 1376),
(208, 1, 96, 108, 1468),
(209, 1, 102, 108, 1614),
(210, 1, 108, 108, 1727),
(211, 2, 24, 30, 114),
(212, 2, 30, 30, 147),
(213, 2, 36, 30, 176),
(214, 2, 42, 30, 206),
(215, 2, 48, 30, 235),
(216, 2, 54, 30, 261),
(217, 2, 60, 30, 293),
(218, 2, 66, 30, 322),
(219, 2, 72, 30, 344),
(220, 2, 84, 30, 90),
(221, 2, 84, 30, 401),
(222, 2, 90, 30, 427),
(223, 2, 96, 30, 458),
(224, 2, 102, 30, 504),
(225, 2, 108, 30, 539),
(226, 2, 24, 36, 141),
(227, 2, 30, 36, 176),
(228, 2, 36, 36, 206),
(229, 2, 42, 36, 236),
(230, 2, 48, 36, 271),
(231, 2, 54, 36, 293),
(232, 2, 60, 36, 324),
(233, 2, 66, 36, 357),
(234, 2, 72, 36, 379),
(235, 2, 84, 36, 96),
(236, 2, 84, 36, 453),
(237, 2, 90, 36, 480),
(238, 2, 96, 36, 503),
(239, 2, 102, 36, 553),
(240, 2, 108, 36, 592),
(241, 2, 24, 42, 171),
(242, 2, 30, 42, 193),
(243, 2, 36, 42, 225),
(244, 2, 42, 42, 258),
(245, 2, 48, 42, 295),
(246, 2, 54, 42, 329),
(247, 2, 60, 42, 364),
(248, 2, 66, 42, 401),
(249, 2, 72, 42, 421),
(250, 2, 84, 42, 102),
(251, 2, 84, 42, 499),
(252, 2, 90, 42, 521),
(253, 2, 96, 42, 550),
(254, 2, 102, 42, 605),
(255, 2, 108, 42, 647),
(256, 2, 24, 48, 176),
(257, 2, 30, 48, 228),
(258, 2, 36, 48, 244),
(259, 2, 42, 48, 274),
(260, 2, 48, 48, 314),
(261, 2, 54, 48, 353),
(262, 2, 60, 48, 396),
(263, 2, 66, 48, 435),
(264, 2, 72, 48, 439),
(265, 2, 84, 48, 108),
(266, 2, 84, 48, 545),
(267, 2, 90, 48, 585),
(268, 2, 96, 48, 624),
(269, 2, 102, 48, 686),
(270, 2, 108, 48, 734),
(271, 2, 24, 54, 185),
(272, 2, 30, 54, 232),
(273, 2, 36, 54, 265),
(274, 2, 42, 54, 303),
(275, 2, 48, 54, 347),
(276, 2, 54, 54, 389),
(277, 2, 60, 54, 433),
(278, 2, 66, 54, 477),
(279, 2, 72, 54, 503),
(280, 2, 84, 54, 545),
(281, 2, 84, 54, 602),
(282, 2, 90, 54, 657),
(283, 2, 96, 54, 688),
(284, 2, 102, 54, 757),
(285, 2, 108, 54, 810),
(286, 2, 24, 60, 206),
(287, 2, 30, 60, 249),
(288, 2, 36, 60, 281),
(289, 2, 42, 60, 334),
(290, 2, 48, 60, 381),
(291, 2, 54, 60, 418),
(292, 2, 60, 60, 466),
(293, 2, 66, 60, 512),
(294, 2, 72, 60, 563),
(295, 2, 84, 60, 609),
(296, 2, 84, 60, 644),
(297, 2, 90, 60, 690),
(298, 2, 96, 60, 735),
(299, 2, 102, 60, 809),
(300, 2, 108, 60, 865),
(301, 2, 24, 66, 227),
(302, 2, 30, 66, 273),
(303, 2, 36, 66, 295),
(304, 2, 42, 66, 361),
(305, 2, 48, 66, 414),
(306, 2, 54, 66, 456),
(307, 2, 60, 66, 504),
(308, 2, 66, 66, 555),
(309, 2, 72, 66, 614),
(310, 2, 84, 66, 666),
(311, 2, 84, 66, 716),
(312, 2, 90, 66, 768),
(313, 2, 96, 66, 819),
(314, 2, 102, 66, 901),
(315, 2, 108, 66, 964),
(316, 2, 24, 72, 247),
(317, 2, 30, 72, 287),
(318, 2, 36, 72, 334),
(319, 2, 42, 72, 386),
(320, 2, 48, 72, 442),
(321, 2, 54, 72, 489),
(322, 2, 60, 72, 541),
(323, 2, 66, 72, 594),
(324, 2, 72, 72, 663),
(325, 2, 84, 72, 717),
(326, 2, 84, 72, 761),
(327, 2, 90, 72, 815),
(328, 2, 96, 72, 868),
(329, 2, 102, 72, 955),
(330, 2, 108, 72, 1022),
(331, 2, 24, 78, 258),
(332, 2, 30, 78, 299),
(333, 2, 36, 78, 359),
(334, 2, 42, 78, 423),
(335, 2, 48, 78, 483),
(336, 2, 54, 78, 533),
(337, 2, 60, 78, 587),
(338, 2, 66, 78, 646),
(339, 2, 72, 78, 707),
(340, 2, 84, 78, 765),
(341, 2, 84, 78, 819),
(342, 2, 90, 78, 878),
(343, 2, 96, 78, 937),
(344, 2, 102, 78, 1031),
(345, 2, 108, 78, 1103),
(346, 2, 24, 84, 268),
(347, 2, 30, 84, 316),
(348, 2, 36, 84, 379),
(349, 2, 42, 84, 453),
(350, 2, 48, 84, 517),
(351, 2, 54, 84, 565),
(352, 2, 60, 84, 639),
(353, 2, 66, 84, 704),
(354, 2, 72, 84, 753),
(355, 2, 84, 84, 815),
(356, 2, 84, 84, 883),
(357, 2, 90, 84, 946),
(358, 2, 96, 84, 1009),
(359, 2, 102, 84, 1110),
(360, 2, 108, 84, 1188),
(361, 2, 24, 90, 287),
(362, 2, 30, 90, 336),
(363, 2, 36, 90, 403),
(364, 2, 42, 90, 471),
(365, 2, 48, 90, 538),
(366, 2, 54, 90, 606),
(367, 2, 60, 90, 672),
(368, 2, 66, 90, 739),
(369, 2, 72, 90, 813),
(370, 2, 84, 90, 880),
(371, 2, 84, 90, 941),
(372, 2, 90, 90, 1008),
(373, 2, 96, 90, 1074),
(374, 2, 102, 90, 1181),
(375, 2, 108, 90, 1264),
(376, 2, 24, 96, 294),
(377, 2, 30, 96, 348),
(378, 2, 36, 96, 418),
(379, 2, 42, 96, 484),
(380, 2, 48, 96, 553),
(381, 2, 54, 96, 637),
(382, 2, 60, 96, 695),
(383, 2, 66, 96, 764),
(384, 2, 72, 96, 839),
(385, 2, 84, 96, 908),
(386, 2, 84, 96, 984),
(387, 2, 90, 96, 1054),
(388, 2, 96, 96, 1125),
(389, 2, 102, 96, 1238),
(390, 2, 108, 96, 1324),
(391, 2, 24, 102, 301),
(392, 2, 30, 102, 356),
(393, 2, 36, 102, 427),
(394, 2, 42, 102, 507),
(395, 2, 48, 102, 580),
(396, 2, 54, 102, 651),
(397, 2, 60, 102, 728),
(398, 2, 66, 102, 800),
(399, 2, 72, 102, 882),
(400, 2, 84, 102, 956),
(401, 2, 84, 102, 1031),
(402, 2, 90, 102, 1105),
(403, 2, 96, 102, 1178),
(404, 2, 102, 102, 1296),
(405, 2, 108, 102, 1387),
(406, 2, 24, 108, 322),
(407, 2, 30, 108, 399),
(408, 2, 36, 108, 479),
(409, 2, 42, 108, 536),
(410, 2, 48, 108, 612),
(411, 2, 54, 108, 694),
(412, 2, 60, 108, 771),
(413, 2, 66, 108, 848),
(414, 2, 72, 108, 949),
(415, 2, 84, 108, 1028),
(416, 2, 84, 108, 1070),
(417, 2, 90, 108, 1147),
(418, 2, 96, 108, 1223),
(419, 2, 102, 108, 1345),
(420, 2, 108, 108, 1439),
(421, 1367, 24, 30, 201),
(422, 1367, 30, 30, 241),
(423, 1367, 36, 30, 285),
(424, 1367, 42, 30, 318),
(425, 1367, 48, 30, 335),
(426, 1367, 54, 30, 379),
(427, 1367, 60, 30, 444),
(428, 1367, 66, 30, 460),
(429, 1367, 72, 30, 475),
(430, 1367, 84, 30, 90),
(431, 1367, 84, 30, 610),
(432, 1367, 90, 30, 683),
(433, 1367, 96, 30, 759),
(434, 1367, 102, 30, 857),
(435, 1367, 108, 30, 960),
(436, 1367, 24, 36, 233),
(437, 1367, 30, 36, 273),
(438, 1367, 36, 36, 316),
(439, 1367, 42, 36, 354),
(440, 1367, 48, 36, 375),
(441, 1367, 54, 36, 435),
(442, 1367, 60, 36, 503),
(443, 1367, 66, 36, 526),
(444, 1367, 72, 36, 540),
(445, 1367, 84, 36, 96),
(446, 1367, 84, 36, 693),
(447, 1367, 90, 36, 759),
(448, 1367, 96, 36, 844),
(449, 1367, 102, 36, 953),
(450, 1367, 108, 36, 1068),
(451, 1367, 24, 42, 255),
(452, 1367, 30, 42, 301),
(453, 1367, 36, 42, 349),
(454, 1367, 42, 42, 393),
(455, 1367, 48, 42, 415),
(456, 1367, 54, 42, 485),
(457, 1367, 60, 42, 560),
(458, 1367, 66, 42, 576),
(459, 1367, 72, 42, 608),
(460, 1367, 84, 42, 102),
(461, 1367, 84, 42, 768),
(462, 1367, 90, 42, 858),
(463, 1367, 96, 42, 953),
(464, 1367, 102, 42, 1076),
(465, 1367, 108, 42, 1205),
(466, 1367, 24, 48, 276),
(467, 1367, 30, 48, 330),
(468, 1367, 36, 48, 381),
(469, 1367, 42, 48, 429),
(470, 1367, 48, 48, 460),
(471, 1367, 54, 48, 536),
(472, 1367, 60, 48, 615),
(473, 1367, 66, 48, 651),
(474, 1367, 72, 48, 683),
(475, 1367, 84, 48, 108),
(476, 1367, 84, 48, 860),
(477, 1367, 90, 48, 969),
(478, 1367, 96, 48, 1076),
(479, 1367, 102, 48, 1216),
(480, 1367, 108, 48, 1362),
(481, 1367, 24, 54, 305),
(482, 1367, 30, 54, 363),
(483, 1367, 36, 54, 414),
(484, 1367, 42, 54, 469),
(485, 1367, 48, 54, 503),
(486, 1367, 54, 54, 593),
(487, 1367, 60, 54, 666),
(488, 1367, 66, 54, 724),
(489, 1367, 72, 54, 766),
(490, 1367, 84, 54, 114),
(491, 1367, 84, 54, 950),
(492, 1367, 90, 54, 1066),
(493, 1367, 96, 54, 1185),
(494, 1367, 102, 54, 1339),
(495, 1367, 108, 54, 1500),
(496, 1367, 24, 60, 335),
(497, 1367, 30, 60, 393),
(498, 1367, 36, 60, 450),
(499, 1367, 42, 60, 509),
(500, 1367, 48, 60, 549),
(501, 1367, 54, 60, 648),
(502, 1367, 60, 60, 721),
(503, 1367, 66, 60, 786),
(504, 1367, 72, 60, 833),
(505, 1367, 84, 60, 120),
(506, 1367, 84, 60, 1033),
(507, 1367, 90, 60, 1160),
(508, 1367, 96, 60, 1288),
(509, 1367, 102, 60, 1455),
(510, 1367, 108, 60, 1629),
(511, 1367, 24, 66, 364),
(512, 1367, 30, 66, 424),
(513, 1367, 36, 66, 484),
(514, 1367, 42, 66, 551),
(515, 1367, 48, 66, 604),
(516, 1367, 54, 66, 708),
(517, 1367, 60, 66, 793),
(518, 1367, 66, 66, 863),
(519, 1367, 72, 66, 915),
(520, 1367, 84, 66, 126),
(521, 1367, 84, 66, 1124),
(522, 1367, 90, 66, 1305),
(523, 1367, 96, 66, 1450),
(524, 1367, 102, 66, 1639),
(525, 1367, 108, 66, 1835),
(526, 1367, 24, 72, 398),
(527, 1367, 30, 72, 450),
(528, 1367, 36, 72, 516),
(529, 1367, 42, 72, 593),
(530, 1367, 48, 72, 655),
(531, 1367, 54, 72, 768),
(532, 1367, 60, 72, 864),
(533, 1367, 66, 72, 914),
(534, 1367, 72, 72, 973),
(535, 1367, 84, 72, 1110),
(536, 1367, 84, 72, 1198),
(537, 1367, 90, 72, 1349),
(538, 1367, 96, 72, 1499),
(539, 1367, 102, 72, 1694),
(540, 1367, 108, 72, 1897),
(541, 1367, 24, 78, 411),
(542, 1367, 30, 78, 460),
(543, 1367, 36, 78, 531),
(544, 1367, 42, 78, 608),
(545, 1367, 48, 78, 676),
(546, 1367, 54, 78, 789),
(547, 1367, 60, 78, 889),
(548, 1367, 66, 78, 945),
(549, 1367, 72, 78, 1040),
(550, 1367, 84, 78, 1184),
(551, 1367, 84, 78, 1281),
(552, 1367, 90, 78, 1439),
(553, 1367, 96, 78, 1599),
(554, 1367, 102, 78, 1807),
(555, 1367, 108, 78, 2023),
(556, 1367, 24, 84, 443),
(557, 1367, 30, 84, 494),
(558, 1367, 36, 84, 565),
(559, 1367, 42, 84, 654),
(560, 1367, 48, 84, 726),
(561, 1367, 54, 84, 831),
(562, 1367, 60, 84, 905),
(563, 1367, 66, 84, 1011),
(564, 1367, 72, 84, 1118),
(565, 1367, 84, 84, 1266),
(566, 1367, 84, 84, 1366),
(567, 1367, 90, 84, 1539),
(568, 1367, 96, 84, 1709),
(569, 1367, 102, 84, 1931),
(570, 1367, 108, 84, 2163),
(571, 1367, 24, 90, 473),
(572, 1367, 30, 90, 524),
(573, 1367, 36, 90, 599),
(574, 1367, 42, 90, 694),
(575, 1367, 48, 90, 769),
(576, 1367, 54, 90, 879),
(577, 1367, 60, 90, 956),
(578, 1367, 66, 90, 1074),
(579, 1367, 72, 90, 1184),
(580, 1367, 84, 90, 1330),
(581, 1367, 84, 90, 1455),
(582, 1367, 90, 90, 1638),
(583, 1367, 96, 90, 1820),
(584, 1367, 102, 90, 2057),
(585, 1367, 108, 90, 2303),
(586, 1367, 24, 96, 490),
(587, 1367, 30, 96, 541),
(588, 1367, 36, 96, 618),
(589, 1367, 42, 96, 723),
(590, 1367, 48, 96, 803),
(591, 1367, 54, 96, 925),
(592, 1367, 60, 96, 1045),
(593, 1367, 66, 96, 1146),
(594, 1367, 72, 96, 1259),
(595, 1367, 84, 96, 1426),
(596, 1367, 84, 96, 1544),
(597, 1367, 90, 96, 1736),
(598, 1367, 96, 96, 1928),
(599, 1367, 102, 96, 2178),
(600, 1367, 108, 96, 2439),
(601, 1367, 24, 102, 513),
(602, 1367, 30, 102, 571),
(603, 1367, 36, 102, 653),
(604, 1367, 42, 102, 769),
(605, 1367, 48, 102, 854),
(606, 1367, 54, 102, 983),
(607, 1367, 60, 102, 1085),
(608, 1367, 66, 102, 1214),
(609, 1367, 72, 102, 1339),
(610, 1367, 84, 102, 1513),
(611, 1367, 84, 102, 1636),
(612, 1367, 90, 102, 1844),
(613, 1367, 96, 102, 2048),
(614, 1367, 102, 102, 2314),
(615, 1367, 108, 102, 2591),
(616, 1367, 24, 108, 539),
(617, 1367, 30, 108, 594),
(618, 1367, 36, 108, 685),
(619, 1367, 42, 108, 806),
(620, 1367, 48, 108, 898),
(621, 1367, 54, 108, 1033),
(622, 1367, 60, 108, 1136),
(623, 1367, 66, 108, 1278),
(624, 1367, 72, 108, 1408),
(625, 1367, 84, 108, 1586),
(626, 1367, 84, 108, 1718),
(627, 1367, 90, 108, 1933),
(628, 1367, 96, 108, 2148),
(629, 1367, 102, 108, 2427),
(630, 1367, 108, 108, 2718),
(631, 1367, 24, 114, 563),
(632, 1367, 30, 114, 621),
(633, 1367, 36, 114, 724),
(634, 1367, 42, 114, 853),
(635, 1367, 48, 114, 948),
(636, 1367, 54, 114, 1086),
(637, 1367, 60, 114, 1201),
(638, 1367, 66, 114, 1344),
(639, 1367, 72, 114, 1485),
(640, 1367, 84, 114, 1678),
(641, 1367, 84, 114, 1808),
(642, 1367, 90, 114, 2033),
(643, 1367, 96, 114, 2259),
(644, 1367, 102, 114, 2552),
(645, 1367, 108, 114, 2859),
(646, 1367, 24, 120, 591),
(647, 1367, 30, 120, 651),
(648, 1367, 36, 120, 754),
(649, 1367, 42, 120, 891),
(650, 1367, 48, 120, 994),
(651, 1367, 54, 120, 1138),
(652, 1367, 60, 120, 1226),
(653, 1367, 66, 120, 1408),
(654, 1367, 72, 120, 1549),
(655, 1367, 84, 120, 1744),
(656, 1367, 84, 120, 1893),
(657, 1367, 90, 120, 2130),
(658, 1367, 96, 120, 2366),
(659, 1367, 102, 120, 2674),
(660, 1367, 108, 120, 2995),
(661, 1367, 24, 126, 610),
(662, 1367, 30, 126, 680),
(663, 1367, 36, 126, 798),
(664, 1367, 42, 126, 936),
(665, 1367, 48, 126, 1043),
(666, 1367, 54, 126, 1188),
(667, 1367, 60, 126, 1315),
(668, 1367, 66, 126, 1478),
(669, 1367, 72, 126, 1626),
(670, 1367, 84, 126, 1720),
(671, 1367, 84, 126, 1978),
(672, 1367, 90, 126, 2235),
(673, 1367, 96, 126, 2483),
(674, 1367, 102, 126, 2805),
(675, 1367, 108, 126, 3142),
(676, 3, 24, 30, 201),
(677, 3, 30, 30, 241),
(678, 3, 36, 30, 285),
(679, 3, 42, 30, 318),
(680, 3, 48, 30, 335),
(681, 3, 54, 30, 379),
(682, 3, 60, 30, 444),
(683, 3, 66, 30, 460),
(684, 3, 72, 30, 475),
(685, 3, 84, 30, 90),
(686, 3, 84, 30, 610),
(687, 3, 90, 30, 683),
(688, 3, 96, 30, 759),
(689, 3, 102, 30, 857),
(690, 3, 108, 30, 960),
(691, 3, 24, 36, 233),
(692, 3, 30, 36, 273),
(693, 3, 36, 36, 316),
(694, 3, 42, 36, 354),
(695, 3, 48, 36, 375),
(696, 3, 54, 36, 435),
(697, 3, 60, 36, 503),
(698, 3, 66, 36, 526),
(699, 3, 72, 36, 540),
(700, 3, 84, 36, 96),
(701, 3, 84, 36, 693),
(702, 3, 90, 36, 759),
(703, 3, 96, 36, 844),
(704, 3, 102, 36, 953),
(705, 3, 108, 36, 1068),
(706, 3, 24, 42, 255),
(707, 3, 30, 42, 301),
(708, 3, 36, 42, 349),
(709, 3, 42, 42, 393),
(710, 3, 48, 42, 415),
(711, 3, 54, 42, 485),
(712, 3, 60, 42, 560),
(713, 3, 66, 42, 576),
(714, 3, 72, 42, 608),
(715, 3, 84, 42, 102),
(716, 3, 84, 42, 768),
(717, 3, 90, 42, 858),
(718, 3, 96, 42, 953),
(719, 3, 102, 42, 1076),
(720, 3, 108, 42, 1205),
(721, 3, 24, 48, 276),
(722, 3, 30, 48, 330),
(723, 3, 36, 48, 381),
(724, 3, 42, 48, 429),
(725, 3, 48, 48, 460),
(726, 3, 54, 48, 536),
(727, 3, 60, 48, 615),
(728, 3, 66, 48, 651),
(729, 3, 72, 48, 683),
(730, 3, 84, 48, 108),
(731, 3, 84, 48, 860),
(732, 3, 90, 48, 969),
(733, 3, 96, 48, 1076),
(734, 3, 102, 48, 1216),
(735, 3, 108, 48, 1362),
(736, 3, 24, 54, 305),
(737, 3, 30, 54, 363),
(738, 3, 36, 54, 414),
(739, 3, 42, 54, 469),
(740, 3, 48, 54, 503),
(741, 3, 54, 54, 593),
(742, 3, 60, 54, 666),
(743, 3, 66, 54, 724),
(744, 3, 72, 54, 766),
(745, 3, 84, 54, 114),
(746, 3, 84, 54, 950),
(747, 3, 90, 54, 1066),
(748, 3, 96, 54, 1185),
(749, 3, 102, 54, 1339),
(750, 3, 108, 54, 1500),
(751, 3, 24, 60, 335),
(752, 3, 30, 60, 393),
(753, 3, 36, 60, 450),
(754, 3, 42, 60, 509),
(755, 3, 48, 60, 549),
(756, 3, 54, 60, 648),
(757, 3, 60, 60, 721),
(758, 3, 66, 60, 786),
(759, 3, 72, 60, 833),
(760, 3, 84, 60, 120),
(761, 3, 84, 60, 1033),
(762, 3, 90, 60, 1160),
(763, 3, 96, 60, 1288),
(764, 3, 102, 60, 1455),
(765, 3, 108, 60, 1629),
(766, 3, 24, 66, 364),
(767, 3, 30, 66, 424),
(768, 3, 36, 66, 484),
(769, 3, 42, 66, 551),
(770, 3, 48, 66, 604),
(771, 3, 54, 66, 708),
(772, 3, 60, 66, 793),
(773, 3, 66, 66, 863),
(774, 3, 72, 66, 915),
(775, 3, 84, 66, 126),
(776, 3, 84, 66, 1124),
(777, 3, 90, 66, 1305),
(778, 3, 96, 66, 1450),
(779, 3, 102, 66, 1639),
(780, 3, 108, 66, 1835),
(781, 3, 24, 72, 398),
(782, 3, 30, 72, 450),
(783, 3, 36, 72, 516),
(784, 3, 42, 72, 593),
(785, 3, 48, 72, 655),
(786, 3, 54, 72, 768),
(787, 3, 60, 72, 864),
(788, 3, 66, 72, 914),
(789, 3, 72, 72, 973),
(790, 3, 84, 72, 1110),
(791, 3, 84, 72, 1198),
(792, 3, 90, 72, 1349),
(793, 3, 96, 72, 1499),
(794, 3, 102, 72, 1694),
(795, 3, 108, 72, 1897),
(796, 3, 24, 78, 411),
(797, 3, 30, 78, 460),
(798, 3, 36, 78, 531),
(799, 3, 42, 78, 608),
(800, 3, 48, 78, 676),
(801, 3, 54, 78, 789),
(802, 3, 60, 78, 889),
(803, 3, 66, 78, 945),
(804, 3, 72, 78, 1040),
(805, 3, 84, 78, 1184),
(806, 3, 84, 78, 1281),
(807, 3, 90, 78, 1439),
(808, 3, 96, 78, 1599),
(809, 3, 102, 78, 1807),
(810, 3, 108, 78, 2023),
(811, 3, 24, 84, 443),
(812, 3, 30, 84, 494),
(813, 3, 36, 84, 565),
(814, 3, 42, 84, 654),
(815, 3, 48, 84, 726),
(816, 3, 54, 84, 831),
(817, 3, 60, 84, 905),
(818, 3, 66, 84, 1011),
(819, 3, 72, 84, 1118),
(820, 3, 84, 84, 1266),
(821, 3, 84, 84, 1366),
(822, 3, 90, 84, 1539),
(823, 3, 96, 84, 1709),
(824, 3, 102, 84, 1931),
(825, 3, 108, 84, 2163),
(826, 3, 24, 90, 473),
(827, 3, 30, 90, 524),
(828, 3, 36, 90, 599),
(829, 3, 42, 90, 694),
(830, 3, 48, 90, 769),
(831, 3, 54, 90, 879),
(832, 3, 60, 90, 956),
(833, 3, 66, 90, 1074),
(834, 3, 72, 90, 1184),
(835, 3, 84, 90, 1330),
(836, 3, 84, 90, 1455),
(837, 3, 90, 90, 1638),
(838, 3, 96, 90, 1820),
(839, 3, 102, 90, 2057),
(840, 3, 108, 90, 2303),
(841, 3, 24, 96, 490),
(842, 3, 30, 96, 541),
(843, 3, 36, 96, 618),
(844, 3, 42, 96, 723),
(845, 3, 48, 96, 803),
(846, 3, 54, 96, 925),
(847, 3, 60, 96, 1045),
(848, 3, 66, 96, 1146),
(849, 3, 72, 96, 1259),
(850, 3, 84, 96, 1426),
(851, 3, 84, 96, 1544),
(852, 3, 90, 96, 1736),
(853, 3, 96, 96, 1928),
(854, 3, 102, 96, 2178),
(855, 3, 108, 96, 2439),
(856, 3, 24, 102, 513),
(857, 3, 30, 102, 571),
(858, 3, 36, 102, 653),
(859, 3, 42, 102, 769),
(860, 3, 48, 102, 854),
(861, 3, 54, 102, 983),
(862, 3, 60, 102, 1085),
(863, 3, 66, 102, 1214),
(864, 3, 72, 102, 1339),
(865, 3, 84, 102, 1513),
(866, 3, 84, 102, 1636),
(867, 3, 90, 102, 1844),
(868, 3, 96, 102, 2048),
(869, 3, 102, 102, 2314),
(870, 3, 108, 102, 2591),
(871, 3, 24, 108, 539),
(872, 3, 30, 108, 594),
(873, 3, 36, 108, 685),
(874, 3, 42, 108, 806),
(875, 3, 48, 108, 898),
(876, 3, 54, 108, 1033),
(877, 3, 60, 108, 1136),
(878, 3, 66, 108, 1278),
(879, 3, 72, 108, 1408),
(880, 3, 84, 108, 1586),
(881, 3, 84, 108, 1718),
(882, 3, 90, 108, 1933),
(883, 3, 96, 108, 2148),
(884, 3, 102, 108, 2427),
(885, 3, 108, 108, 2718),
(886, 3, 24, 114, 563),
(887, 3, 30, 114, 621),
(888, 3, 36, 114, 724),
(889, 3, 42, 114, 853),
(890, 3, 48, 114, 948),
(891, 3, 54, 114, 1086),
(892, 3, 60, 114, 1201),
(893, 3, 66, 114, 1344),
(894, 3, 72, 114, 1485),
(895, 3, 84, 114, 1678),
(896, 3, 84, 114, 1808),
(897, 3, 90, 114, 2033),
(898, 3, 96, 114, 2259),
(899, 3, 102, 114, 2552),
(900, 3, 108, 114, 2859),
(901, 3, 24, 120, 591),
(902, 3, 30, 120, 651),
(903, 3, 36, 120, 754),
(904, 3, 42, 120, 891),
(905, 3, 48, 120, 994),
(906, 3, 54, 120, 1138),
(907, 3, 60, 120, 1226),
(908, 3, 66, 120, 1408),
(909, 3, 72, 120, 1549),
(910, 3, 84, 120, 1744),
(911, 3, 84, 120, 1893),
(912, 3, 90, 120, 2130),
(913, 3, 96, 120, 2366),
(914, 3, 102, 120, 2674),
(915, 3, 108, 120, 2995),
(916, 3, 24, 126, 610),
(917, 3, 30, 126, 680),
(918, 3, 36, 126, 798),
(919, 3, 42, 126, 936),
(920, 3, 48, 126, 1043),
(921, 3, 54, 126, 1188),
(922, 3, 60, 126, 1315),
(923, 3, 66, 126, 1478),
(924, 3, 72, 126, 1626),
(925, 3, 84, 126, 1720),
(926, 3, 84, 126, 1978),
(927, 3, 90, 126, 2235),
(928, 3, 96, 126, 2483),
(929, 3, 102, 126, 2805),
(930, 3, 108, 126, 3142),
(931, 4, 24, 30, 161),
(932, 4, 30, 30, 193),
(933, 4, 36, 30, 228),
(934, 4, 42, 30, 254),
(935, 4, 48, 30, 268),
(936, 4, 54, 30, 303),
(937, 4, 60, 30, 355),
(938, 4, 66, 30, 368),
(939, 4, 72, 30, 380),
(940, 4, 84, 30, 90),
(941, 4, 84, 30, 488),
(942, 4, 90, 30, 546),
(943, 4, 96, 30, 607),
(944, 4, 102, 30, 686),
(945, 4, 108, 30, 768),
(946, 4, 24, 36, 186),
(947, 4, 30, 36, 218),
(948, 4, 36, 36, 253),
(949, 4, 42, 36, 283),
(950, 4, 48, 36, 300),
(951, 4, 54, 36, 348),
(952, 4, 60, 36, 402),
(953, 4, 66, 36, 421),
(954, 4, 72, 36, 432),
(955, 4, 84, 36, 96),
(956, 4, 84, 36, 554),
(957, 4, 90, 36, 607),
(958, 4, 96, 36, 675),
(959, 4, 102, 36, 763),
(960, 4, 108, 36, 854),
(961, 4, 24, 42, 204),
(962, 4, 30, 42, 241),
(963, 4, 36, 42, 279),
(964, 4, 42, 42, 314),
(965, 4, 48, 42, 332),
(966, 4, 54, 42, 388),
(967, 4, 60, 42, 448),
(968, 4, 66, 42, 461),
(969, 4, 72, 42, 486),
(970, 4, 84, 42, 102),
(971, 4, 84, 42, 614),
(972, 4, 90, 42, 686),
(973, 4, 96, 42, 762),
(974, 4, 102, 42, 861),
(975, 4, 108, 42, 964),
(976, 4, 24, 48, 221),
(977, 4, 30, 48, 264),
(978, 4, 36, 48, 305),
(979, 4, 42, 48, 343),
(980, 4, 48, 48, 368),
(981, 4, 54, 48, 429),
(982, 4, 60, 48, 492),
(983, 4, 66, 48, 521),
(984, 4, 72, 48, 546),
(985, 4, 84, 48, 108),
(986, 4, 84, 48, 688),
(987, 4, 90, 48, 775),
(988, 4, 96, 48, 861),
(989, 4, 102, 48, 973),
(990, 4, 108, 48, 1090),
(991, 4, 24, 54, 244),
(992, 4, 30, 54, 290),
(993, 4, 36, 54, 331),
(994, 4, 42, 54, 375),
(995, 4, 48, 54, 402),
(996, 4, 54, 54, 474),
(997, 4, 60, 54, 533),
(998, 4, 66, 54, 579),
(999, 4, 72, 54, 613),
(1000, 4, 84, 54, 114),
(1001, 4, 84, 54, 760),
(1002, 4, 90, 54, 853),
(1003, 4, 96, 54, 948),
(1004, 4, 102, 54, 1071),
(1005, 4, 108, 54, 1200),
(1006, 4, 24, 60, 268),
(1007, 4, 30, 60, 314),
(1008, 4, 36, 60, 360),
(1009, 4, 42, 60, 407),
(1010, 4, 48, 60, 439),
(1011, 4, 54, 60, 518),
(1012, 4, 60, 60, 577),
(1013, 4, 66, 60, 629),
(1014, 4, 72, 60, 666),
(1015, 4, 84, 60, 120),
(1016, 4, 84, 60, 826),
(1017, 4, 90, 60, 928),
(1018, 4, 96, 60, 1030),
(1019, 4, 102, 60, 1164),
(1020, 4, 108, 60, 1304),
(1021, 4, 24, 66, 291),
(1022, 4, 30, 66, 339),
(1023, 4, 36, 66, 387),
(1024, 4, 42, 66, 441),
(1025, 4, 48, 66, 483),
(1026, 4, 54, 66, 566),
(1027, 4, 60, 66, 634),
(1028, 4, 66, 66, 690),
(1029, 4, 72, 66, 732),
(1030, 4, 84, 66, 126),
(1031, 4, 84, 66, 899),
(1032, 4, 90, 66, 1044),
(1033, 4, 96, 66, 1160),
(1034, 4, 102, 66, 1311),
(1035, 4, 108, 66, 1468),
(1036, 4, 24, 72, 318),
(1037, 4, 30, 72, 360),
(1038, 4, 36, 72, 413),
(1039, 4, 42, 72, 474),
(1040, 4, 48, 72, 524),
(1041, 4, 54, 72, 614),
(1042, 4, 60, 72, 691),
(1043, 4, 66, 72, 731),
(1044, 4, 72, 72, 778),
(1045, 4, 84, 72, 888),
(1046, 4, 84, 72, 958),
(1047, 4, 90, 72, 1079),
(1048, 4, 96, 72, 1199),
(1049, 4, 102, 72, 1355),
(1050, 4, 108, 72, 1517),
(1051, 4, 24, 78, 329),
(1052, 4, 30, 78, 368),
(1053, 4, 36, 78, 425),
(1054, 4, 42, 78, 486),
(1055, 4, 48, 78, 541),
(1056, 4, 54, 78, 631),
(1057, 4, 60, 78, 711),
(1058, 4, 66, 78, 756),
(1059, 4, 72, 78, 832),
(1060, 4, 84, 78, 947),
(1061, 4, 84, 78, 1025),
(1062, 4, 90, 78, 1151),
(1063, 4, 96, 78, 1279),
(1064, 4, 102, 78, 1445),
(1065, 4, 108, 78, 1619),
(1066, 4, 24, 84, 354),
(1067, 4, 30, 84, 395),
(1068, 4, 36, 84, 452),
(1069, 4, 42, 84, 523),
(1070, 4, 48, 84, 581),
(1071, 4, 54, 84, 665),
(1072, 4, 60, 84, 724),
(1073, 4, 66, 84, 809),
(1074, 4, 72, 84, 894),
(1075, 4, 84, 84, 1013),
(1076, 4, 84, 84, 1093),
(1077, 4, 90, 84, 1231),
(1078, 4, 96, 84, 1367),
(1079, 4, 102, 84, 1545),
(1080, 4, 108, 84, 1730),
(1081, 4, 24, 90, 378),
(1082, 4, 30, 90, 419),
(1083, 4, 36, 90, 479),
(1084, 4, 42, 90, 555),
(1085, 4, 48, 90, 615),
(1086, 4, 54, 90, 703),
(1087, 4, 60, 90, 765),
(1088, 4, 66, 90, 859),
(1089, 4, 72, 90, 947),
(1090, 4, 84, 90, 1064),
(1091, 4, 84, 90, 1164),
(1092, 4, 90, 90, 1310),
(1093, 4, 96, 90, 1456),
(1094, 4, 102, 90, 1645),
(1095, 4, 108, 90, 1843),
(1096, 4, 24, 96, 392),
(1097, 4, 30, 96, 433),
(1098, 4, 36, 96, 494),
(1099, 4, 42, 96, 578),
(1100, 4, 48, 96, 642),
(1101, 4, 54, 96, 740),
(1102, 4, 60, 96, 836),
(1103, 4, 66, 96, 917),
(1104, 4, 72, 96, 1007),
(1105, 4, 84, 96, 1141),
(1106, 4, 84, 96, 1235),
(1107, 4, 90, 96, 1389),
(1108, 4, 96, 96, 1542),
(1109, 4, 102, 96, 1742),
(1110, 4, 108, 96, 1952),
(1111, 4, 24, 102, 410),
(1112, 4, 30, 102, 457),
(1113, 4, 36, 102, 522),
(1114, 4, 42, 102, 615),
(1115, 4, 48, 102, 683),
(1116, 4, 54, 102, 786),
(1117, 4, 60, 102, 868),
(1118, 4, 66, 102, 971),
(1119, 4, 72, 102, 1071),
(1120, 4, 84, 102, 1210),
(1121, 4, 84, 102, 1309),
(1122, 4, 90, 102, 1475),
(1123, 4, 96, 102, 1638),
(1124, 4, 102, 102, 1851),
(1125, 4, 108, 102, 2073),
(1126, 4, 24, 108, 431),
(1127, 4, 30, 108, 475),
(1128, 4, 36, 108, 548),
(1129, 4, 42, 108, 645),
(1130, 4, 48, 108, 718),
(1131, 4, 54, 108, 826),
(1132, 4, 60, 108, 909),
(1133, 4, 66, 108, 1022),
(1134, 4, 72, 108, 1126),
(1135, 4, 84, 108, 1269),
(1136, 4, 84, 108, 1374),
(1137, 4, 90, 108, 1546),
(1138, 4, 96, 108, 1718),
(1139, 4, 102, 108, 1941),
(1140, 4, 108, 108, 2174),
(1141, 4, 24, 114, 450),
(1142, 4, 30, 114, 497),
(1143, 4, 36, 114, 579),
(1144, 4, 42, 114, 682),
(1145, 4, 48, 114, 758),
(1146, 4, 54, 114, 869),
(1147, 4, 60, 114, 961),
(1148, 4, 66, 114, 1075),
(1149, 4, 72, 114, 1188),
(1150, 4, 84, 114, 1342),
(1151, 4, 84, 114, 1446),
(1152, 4, 90, 114, 1626),
(1153, 4, 96, 114, 1807),
(1154, 4, 102, 114, 2042),
(1155, 4, 108, 114, 2287),
(1156, 4, 24, 120, 473),
(1157, 4, 30, 120, 521),
(1158, 4, 36, 120, 603),
(1159, 4, 42, 120, 713),
(1160, 4, 48, 120, 795),
(1161, 4, 54, 120, 910),
(1162, 4, 60, 120, 981),
(1163, 4, 66, 120, 1126),
(1164, 4, 72, 120, 1239),
(1165, 4, 84, 120, 1395),
(1166, 4, 84, 120, 1514),
(1167, 4, 90, 120, 1704),
(1168, 4, 96, 120, 1893),
(1169, 4, 102, 120, 2139),
(1170, 4, 108, 120, 2396),
(1171, 4, 24, 126, 488),
(1172, 4, 30, 126, 544),
(1173, 4, 36, 126, 638),
(1174, 4, 42, 126, 749),
(1175, 4, 48, 126, 834),
(1176, 4, 54, 126, 950),
(1177, 4, 60, 126, 1052),
(1178, 4, 66, 126, 1182),
(1179, 4, 72, 126, 1301),
(1180, 4, 84, 126, 1376),
(1181, 4, 84, 126, 1582),
(1182, 4, 90, 126, 1788),
(1183, 4, 96, 126, 1986),
(1184, 4, 102, 126, 2244),
(1185, 4, 108, 126, 2513),
(1396, 5, 24, 30, 198),
(1397, 5, 30, 30, 256),
(1398, 5, 36, 30, 306),
(1399, 5, 42, 30, 358),
(1400, 5, 48, 30, 409),
(1401, 5, 54, 30, 454),
(1402, 5, 60, 30, 510),
(1403, 5, 66, 30, 560),
(1404, 5, 72, 30, 599),
(1405, 5, 84, 30, 90),
(1406, 5, 84, 30, 698),
(1407, 5, 90, 30, 743),
(1408, 5, 96, 30, 797),
(1409, 5, 102, 30, 877),
(1410, 5, 108, 30, 938),
(1411, 5, 24, 36, 245),
(1412, 5, 30, 36, 306),
(1413, 5, 36, 36, 358),
(1414, 5, 42, 36, 411),
(1415, 5, 48, 36, 472),
(1416, 5, 54, 36, 510),
(1417, 5, 60, 36, 564),
(1418, 5, 66, 36, 621),
(1419, 5, 72, 36, 659),
(1420, 5, 84, 36, 96),
(1421, 5, 84, 36, 788),
(1422, 5, 90, 36, 835),
(1423, 5, 96, 36, 875),
(1424, 5, 102, 36, 963),
(1425, 5, 108, 36, 1030),
(1426, 5, 24, 42, 298),
(1427, 5, 30, 42, 336),
(1428, 5, 36, 42, 392),
(1429, 5, 42, 42, 449),
(1430, 5, 48, 42, 513),
(1431, 5, 54, 42, 572),
(1432, 5, 60, 42, 633),
(1433, 5, 66, 42, 698),
(1434, 5, 72, 42, 733),
(1435, 5, 84, 42, 102),
(1436, 5, 84, 42, 868),
(1437, 5, 90, 42, 907),
(1438, 5, 96, 42, 957),
(1439, 5, 102, 42, 1053),
(1440, 5, 108, 42, 1126),
(1441, 5, 24, 48, 306),
(1442, 5, 30, 48, 397),
(1443, 5, 36, 48, 425),
(1444, 5, 42, 48, 477),
(1445, 5, 48, 48, 546),
(1446, 5, 54, 48, 614),
(1447, 5, 60, 48, 689),
(1448, 5, 66, 48, 757),
(1449, 5, 72, 48, 764),
(1450, 5, 84, 48, 108),
(1451, 5, 84, 48, 948),
(1452, 5, 90, 48, 1018),
(1453, 5, 96, 48, 1086),
(1454, 5, 102, 48, 1194),
(1455, 5, 108, 48, 1278),
(1456, 5, 24, 54, 322),
(1457, 5, 30, 54, 404),
(1458, 5, 36, 54, 461),
(1459, 5, 42, 54, 527),
(1460, 5, 48, 54, 604),
(1461, 5, 54, 54, 677),
(1462, 5, 60, 54, 753),
(1463, 5, 66, 54, 830),
(1464, 5, 72, 54, 875),
(1465, 5, 84, 54, 948),
(1466, 5, 84, 54, 1047),
(1467, 5, 90, 54, 1143),
(1468, 5, 96, 54, 1197),
(1469, 5, 102, 54, 1317),
(1470, 5, 108, 54, 1409),
(1471, 5, 24, 60, 358),
(1472, 5, 30, 60, 433),
(1473, 5, 36, 60, 489),
(1474, 5, 42, 60, 581),
(1475, 5, 48, 60, 663),
(1476, 5, 54, 60, 727),
(1477, 5, 60, 60, 811),
(1478, 5, 66, 60, 891),
(1479, 5, 72, 60, 980),
(1480, 5, 84, 60, 1060),
(1481, 5, 84, 60, 1121),
(1482, 5, 90, 60, 1201),
(1483, 5, 96, 60, 1279),
(1484, 5, 102, 60, 1407),
(1485, 5, 108, 60, 1505),
(1486, 5, 24, 66, 395),
(1487, 5, 30, 66, 475),
(1488, 5, 36, 66, 513),
(1489, 5, 42, 66, 628),
(1490, 5, 48, 66, 720),
(1491, 5, 54, 66, 793),
(1492, 5, 60, 66, 877),
(1493, 5, 66, 66, 966),
(1494, 5, 72, 66, 1068),
(1495, 5, 84, 66, 1159),
(1496, 5, 84, 66, 1246),
(1497, 5, 90, 66, 1336),
(1498, 5, 96, 66, 1425),
(1499, 5, 102, 66, 1568),
(1500, 5, 108, 66, 1677),
(1501, 5, 24, 72, 430),
(1502, 5, 30, 72, 499),
(1503, 5, 36, 72, 581),
(1504, 5, 42, 72, 672),
(1505, 5, 48, 72, 769),
(1506, 5, 54, 72, 851),
(1507, 5, 60, 72, 941),
(1508, 5, 66, 72, 1034),
(1509, 5, 72, 72, 1154),
(1510, 5, 84, 72, 1248),
(1511, 5, 84, 72, 1324),
(1512, 5, 90, 72, 1418),
(1513, 5, 96, 72, 1510),
(1514, 5, 102, 72, 1661),
(1515, 5, 108, 72, 1778),
(1516, 5, 24, 78, 449),
(1517, 5, 30, 78, 520),
(1518, 5, 36, 78, 625),
(1519, 5, 42, 78, 736),
(1520, 5, 48, 78, 840),
(1521, 5, 54, 78, 927),
(1522, 5, 60, 78, 1021),
(1523, 5, 66, 78, 1124),
(1524, 5, 72, 78, 1230),
(1525, 5, 84, 78, 1331),
(1526, 5, 84, 78, 1425),
(1527, 5, 90, 78, 1528),
(1528, 5, 96, 78, 1630),
(1529, 5, 102, 78, 1793),
(1530, 5, 108, 78, 1919),
(1531, 5, 24, 84, 466),
(1532, 5, 30, 84, 550),
(1533, 5, 36, 84, 659),
(1534, 5, 42, 84, 788),
(1535, 5, 48, 84, 900),
(1536, 5, 54, 84, 983),
(1537, 5, 60, 84, 1112),
(1538, 5, 66, 84, 1225),
(1539, 5, 72, 84, 1310),
(1540, 5, 84, 84, 1418),
(1541, 5, 84, 84, 1536),
(1542, 5, 90, 84, 1646),
(1543, 5, 96, 84, 1756),
(1544, 5, 102, 84, 1931),
(1545, 5, 108, 84, 2066),
(1546, 5, 24, 90, 499),
(1547, 5, 30, 90, 585),
(1548, 5, 36, 90, 701),
(1549, 5, 42, 90, 820),
(1550, 5, 48, 90, 936),
(1551, 5, 54, 90, 1054),
(1552, 5, 60, 90, 1169),
(1553, 5, 66, 90, 1286),
(1554, 5, 72, 90, 1415),
(1555, 5, 84, 90, 1531),
(1556, 5, 84, 90, 1637),
(1557, 5, 90, 90, 1754),
(1558, 5, 96, 90, 1869),
(1559, 5, 102, 90, 2056),
(1560, 5, 108, 90, 2200),
(1561, 5, 24, 96, 512),
(1562, 5, 30, 96, 606),
(1563, 5, 36, 96, 727),
(1564, 5, 42, 96, 842),
(1565, 5, 48, 96, 962),
(1566, 5, 54, 96, 1108),
(1567, 5, 60, 96, 1209),
(1568, 5, 66, 96, 1329),
(1569, 5, 72, 96, 1460),
(1570, 5, 84, 96, 1580),
(1571, 5, 84, 96, 1712),
(1572, 5, 90, 96, 1834),
(1573, 5, 96, 96, 1958),
(1574, 5, 102, 96, 2153),
(1575, 5, 108, 96, 2304),
(1576, 5, 24, 102, 524),
(1577, 5, 30, 102, 619),
(1578, 5, 36, 102, 743),
(1579, 5, 42, 102, 882),
(1580, 5, 48, 102, 1009),
(1581, 5, 54, 102, 1133),
(1582, 5, 60, 102, 1267),
(1583, 5, 66, 102, 1392),
(1584, 5, 72, 102, 1535),
(1585, 5, 84, 102, 1663),
(1586, 5, 84, 102, 1794),
(1587, 5, 90, 102, 1923),
(1588, 5, 96, 102, 2050),
(1589, 5, 102, 102, 2255),
(1590, 5, 108, 102, 2413),
(1591, 5, 24, 108, 560),
(1592, 5, 30, 108, 694),
(1593, 5, 36, 108, 833),
(1594, 5, 42, 108, 933),
(1595, 5, 48, 108, 1065),
(1596, 5, 54, 108, 1208),
(1597, 5, 60, 108, 1342),
(1598, 5, 66, 108, 1476),
(1599, 5, 72, 108, 1651),
(1600, 5, 84, 108, 1789),
(1601, 5, 84, 108, 1862),
(1602, 5, 90, 108, 1996),
(1603, 5, 96, 108, 2128),
(1604, 5, 102, 108, 2341),
(1605, 5, 108, 108, 2505),
(1606, 6, 24, 0, 182),
(1607, 6, 27, 0, 205),
(1608, 6, 30, 0, 228),
(1609, 6, 33, 0, 251),
(1610, 6, 36, 0, 274),
(1611, 6, 39, 0, 296),
(1612, 6, 42, 0, 319),
(1613, 6, 45, 0, 342),
(1614, 6, 48, 0, 365),
(1615, 6, 51, 0, 388),
(1616, 6, 54, 0, 410),
(1617, 6, 57, 0, 433),
(1618, 6, 60, 0, 456),
(1619, 6, 63, 0, 479),
(1620, 6, 66, 0, 502),
(1621, 6, 69, 0, 524),
(1622, 6, 72, 0, 547),
(1623, 6, 75, 0, 570),
(1624, 7, 24, 0, 205),
(1625, 7, 27, 0, 230),
(1626, 7, 30, 0, 256),
(1627, 7, 33, 0, 281),
(1628, 7, 36, 0, 307),
(1629, 7, 39, 0, 332),
(1630, 7, 42, 0, 358),
(1631, 7, 45, 0, 384),
(1632, 7, 48, 0, 409),
(1633, 7, 51, 0, 435),
(1634, 7, 54, 0, 460),
(1635, 7, 57, 0, 486),
(1636, 7, 60, 0, 512),
(1637, 7, 63, 0, 537),
(1638, 7, 66, 0, 563),
(1639, 7, 69, 0, 588),
(1640, 7, 72, 0, 614),
(1641, 7, 75, 0, 639),
(1642, 8, 24, 0, 147),
(1643, 8, 27, 0, 166),
(1644, 8, 30, 0, 184),
(1645, 8, 33, 0, 202),
(1646, 8, 36, 0, 221),
(1647, 8, 39, 0, 239),
(1648, 8, 42, 0, 258),
(1649, 8, 45, 0, 276),
(1650, 8, 48, 0, 294),
(1651, 8, 51, 0, 313),
(1652, 8, 54, 0, 331),
(1653, 8, 57, 0, 350),
(1654, 8, 60, 0, 368),
(1655, 8, 63, 0, 386),
(1656, 8, 66, 0, 405),
(1657, 8, 69, 0, 423),
(1658, 8, 72, 0, 442),
(1659, 8, 75, 0, 460),
(1660, 9, 24, 0, 91),
(1661, 9, 27, 0, 102),
(1662, 9, 30, 0, 113),
(1663, 9, 33, 0, 125),
(1664, 9, 36, 0, 136),
(1665, 9, 39, 0, 147),
(1666, 9, 42, 0, 159),
(1667, 9, 45, 0, 170),
(1668, 9, 48, 0, 181),
(1669, 9, 51, 0, 193),
(1670, 9, 54, 0, 204),
(1671, 9, 57, 0, 215),
(1672, 9, 60, 0, 227),
(1673, 9, 63, 0, 238),
(1674, 9, 66, 0, 249),
(1675, 9, 69, 0, 261),
(1676, 9, 72, 0, 272),
(1677, 9, 75, 0, 284),
(1678, 10, 24, 0, 243),
(1679, 10, 27, 0, 273),
(1680, 10, 30, 0, 304),
(1681, 10, 33, 0, 334),
(1682, 10, 36, 0, 364),
(1683, 10, 39, 0, 395),
(1684, 10, 42, 0, 425),
(1685, 10, 45, 0, 455),
(1686, 10, 48, 0, 486),
(1687, 10, 51, 0, 516),
(1688, 10, 54, 0, 546),
(1689, 10, 57, 0, 577),
(1690, 10, 60, 0, 607),
(1691, 10, 63, 0, 638),
(1692, 10, 66, 0, 668),
(1693, 10, 69, 0, 698),
(1694, 10, 72, 0, 729),
(1695, 10, 75, 0, 759),
(1696, 11, 24, 0, 243),
(1697, 11, 27, 0, 273),
(1698, 11, 30, 0, 304),
(1699, 11, 33, 0, 334),
(1700, 11, 36, 0, 364),
(1701, 11, 39, 0, 395),
(1702, 11, 42, 0, 425),
(1703, 11, 45, 0, 455),
(1704, 11, 48, 0, 486),
(1705, 11, 51, 0, 516),
(1706, 11, 54, 0, 546),
(1707, 11, 57, 0, 577),
(1708, 11, 60, 0, 607),
(1709, 11, 63, 0, 638),
(1710, 11, 66, 0, 668),
(1711, 11, 69, 0, 698),
(1712, 11, 72, 0, 729),
(1713, 11, 75, 0, 759);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE `product_attribute` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `attribute_condition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_attribute`
--

INSERT INTO `product_attribute` (`id`, `attribute_id`, `product_id`, `category_id`, `attribute_condition`, `is_active`) VALUES
(1, 1, 1, 1, '', 0),
(2, 2, 1, 1, '', 0),
(3, 3, 1, 1, '', 0),
(4, 4, 1, 1, '', 0),
(5, 5, 1, 1, '', 0),
(6, 6, 1, 1, '', 0),
(7, 7, 1, 1, '', 0),
(8, 8, 1, 1, '', 0),
(9, 9, 1, 1, '', 0),
(10, 10, 1, 1, '', 0),
(11, 11, 1, 1, '', 0),
(12, 12, 1, 1, '', 0),
(13, 13, 1, 1, '', 0),
(14, 14, 1, 1, '', 0),
(15, 1, 2, 1, '', 0),
(16, 2, 2, 1, '', 0),
(17, 3, 2, 1, '', 0),
(18, 4, 2, 1, '', 0),
(19, 5, 2, 1, '', 0),
(20, 6, 2, 1, '', 0),
(21, 7, 2, 1, '', 0),
(22, 8, 2, 1, '', 0),
(23, 9, 2, 1, '', 0),
(24, 10, 2, 1, '', 0),
(25, 11, 2, 1, '', 0),
(26, 12, 2, 1, '', 0),
(27, 13, 2, 1, '', 0),
(28, 14, 2, 1, '', 0),
(29, 1, 3, 1, '', 0),
(30, 2, 3, 1, '', 0),
(31, 3, 3, 1, '', 0),
(32, 4, 3, 1, '', 0),
(33, 5, 3, 1, '', 0),
(34, 6, 3, 1, '', 0),
(35, 7, 3, 1, '', 0),
(36, 8, 3, 1, '', 0),
(37, 9, 3, 1, '', 0),
(38, 10, 3, 1, '', 0),
(39, 11, 3, 1, '', 0),
(40, 12, 3, 1, '', 0),
(41, 13, 3, 1, '', 0),
(42, 14, 3, 1, '', 0),
(43, 1, 4, 1, '', 0),
(44, 2, 4, 1, '', 0),
(45, 3, 4, 1, '', 0),
(46, 4, 4, 1, '', 0),
(47, 5, 4, 1, '', 0),
(48, 6, 4, 1, '', 0),
(49, 7, 4, 1, '', 0),
(50, 8, 4, 1, '', 0),
(51, 9, 4, 1, '', 0),
(52, 10, 4, 1, '', 0),
(53, 11, 4, 1, '', 0),
(54, 12, 4, 1, '', 0),
(55, 13, 4, 1, '', 0),
(56, 14, 4, 1, '', 0),
(57, 1, 5, 1, '', 0),
(58, 2, 5, 1, '', 0),
(59, 3, 5, 1, '', 0),
(60, 4, 5, 1, '', 0),
(61, 5, 5, 1, '', 0),
(62, 6, 5, 1, '', 0),
(63, 7, 5, 1, '', 0),
(64, 8, 5, 1, '', 0),
(65, 9, 5, 1, '', 0),
(66, 10, 5, 1, '', 0),
(67, 11, 5, 1, '', 0),
(68, 12, 5, 1, '', 0),
(69, 13, 5, 1, '', 0),
(70, 14, 5, 1, '', 0),
(71, 1, 6, 1, '', 0),
(72, 2, 6, 1, '', 0),
(73, 3, 6, 1, '', 0),
(74, 4, 6, 1, '', 0),
(75, 5, 6, 1, '', 0),
(76, 6, 6, 1, '', 0),
(77, 7, 6, 1, '', 0),
(78, 8, 6, 1, '', 0),
(79, 9, 6, 1, '', 0),
(80, 10, 6, 1, '', 0),
(81, 11, 6, 1, '', 0),
(82, 12, 6, 1, '', 0),
(83, 13, 6, 1, '', 0),
(84, 14, 6, 1, '', 0),
(85, 1, 7, 1, '', 0),
(86, 2, 7, 1, '', 0),
(87, 3, 7, 1, '', 0),
(88, 4, 7, 1, '', 0),
(89, 5, 7, 1, '', 0),
(90, 6, 7, 1, '', 0),
(91, 7, 7, 1, '', 0),
(92, 8, 7, 1, '', 0),
(93, 9, 7, 1, '', 0),
(94, 10, 7, 1, '', 0),
(95, 11, 7, 1, '', 0),
(96, 12, 7, 1, '', 0),
(97, 13, 7, 1, '', 0),
(98, 14, 7, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option`
--

CREATE TABLE `product_attr_option` (
  `id` int(11) NOT NULL,
  `pro_attr_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option`
--

INSERT INTO `product_attr_option` (`id`, `pro_attr_id`, `product_id`, `attribute_id`, `option_id`, `option_condition`) VALUES
(1, 1, 1, 1, 1, ''),
(2, 1, 1, 1, 2, ''),
(3, 1, 1, 1, 3, ''),
(4, 2, 1, 2, 4, '-1/2'),
(5, 2, 1, 2, 5, ''),
(6, 3, 1, 3, 6, ''),
(7, 3, 1, 3, 7, ''),
(8, 4, 1, 4, 8, ''),
(9, 4, 1, 4, 9, ''),
(10, 5, 1, 5, 10, ''),
(11, 5, 1, 5, 11, ''),
(12, 6, 1, 6, 12, ''),
(13, 6, 1, 6, 13, ''),
(14, 7, 1, 7, 14, ''),
(15, 7, 1, 7, 15, ''),
(16, 8, 1, 8, 16, ''),
(17, 8, 1, 8, 17, ''),
(18, 9, 1, 9, 18, '-1/8'),
(19, 9, 1, 9, 19, ''),
(20, 10, 1, 10, 20, ''),
(21, 10, 1, 10, 21, ''),
(22, 11, 1, 11, 22, ''),
(23, 11, 1, 11, 23, ''),
(24, 11, 1, 11, 24, ''),
(25, 12, 1, 12, 25, ''),
(26, 12, 1, 12, 26, '1'),
(27, 12, 1, 12, 27, ''),
(28, 13, 1, 13, 28, ''),
(29, 13, 1, 13, 29, '-1/8'),
(30, 13, 1, 13, 30, '-1/8'),
(31, 13, 1, 13, 31, '-1/4'),
(32, 14, 1, 14, 32, ''),
(33, 14, 1, 14, 33, ''),
(34, 15, 2, 1, 1, ''),
(35, 15, 2, 1, 2, ''),
(36, 15, 2, 1, 3, ''),
(37, 16, 2, 2, 4, '-1/2'),
(38, 16, 2, 2, 5, ''),
(39, 17, 2, 3, 6, ''),
(40, 17, 2, 3, 7, ''),
(41, 18, 2, 4, 8, ''),
(42, 18, 2, 4, 9, ''),
(43, 19, 2, 5, 10, ''),
(44, 19, 2, 5, 11, ''),
(45, 20, 2, 6, 12, ''),
(46, 20, 2, 6, 13, ''),
(47, 21, 2, 7, 14, ''),
(48, 21, 2, 7, 15, ''),
(49, 22, 2, 8, 16, ''),
(50, 22, 2, 8, 17, ''),
(51, 23, 2, 9, 18, '-1/8'),
(52, 23, 2, 9, 19, ''),
(53, 24, 2, 10, 20, ''),
(54, 24, 2, 10, 21, ''),
(55, 25, 2, 11, 22, ''),
(56, 25, 2, 11, 23, ''),
(57, 25, 2, 11, 24, ''),
(58, 26, 2, 12, 25, ''),
(59, 26, 2, 12, 26, '1'),
(60, 26, 2, 12, 27, ''),
(61, 27, 2, 13, 28, ''),
(62, 27, 2, 13, 29, '-1/8'),
(63, 27, 2, 13, 30, '-1/8'),
(64, 27, 2, 13, 31, '-1/4'),
(65, 28, 2, 14, 32, ''),
(66, 28, 2, 14, 33, ''),
(67, 29, 3, 1, 1, ''),
(68, 29, 3, 1, 2, ''),
(69, 29, 3, 1, 3, ''),
(70, 30, 3, 2, 4, '-1/2'),
(71, 30, 3, 2, 5, ''),
(72, 31, 3, 3, 6, ''),
(73, 31, 3, 3, 7, ''),
(74, 32, 3, 4, 8, ''),
(75, 32, 3, 4, 9, ''),
(76, 33, 3, 5, 10, ''),
(77, 33, 3, 5, 11, ''),
(78, 34, 3, 6, 12, ''),
(79, 34, 3, 6, 13, ''),
(80, 35, 3, 7, 14, ''),
(81, 35, 3, 7, 15, ''),
(82, 36, 3, 8, 16, ''),
(83, 36, 3, 8, 17, ''),
(84, 37, 3, 9, 18, '-1/8'),
(85, 37, 3, 9, 19, ''),
(86, 38, 3, 10, 20, ''),
(87, 38, 3, 10, 21, ''),
(88, 39, 3, 11, 22, ''),
(89, 39, 3, 11, 23, ''),
(90, 39, 3, 11, 24, ''),
(91, 40, 3, 12, 25, ''),
(92, 40, 3, 12, 26, '1'),
(93, 40, 3, 12, 27, ''),
(94, 41, 3, 13, 28, ''),
(95, 41, 3, 13, 29, '-1/8'),
(96, 41, 3, 13, 30, '-1/8'),
(97, 41, 3, 13, 31, '-1/4'),
(98, 42, 3, 14, 32, ''),
(99, 42, 3, 14, 33, ''),
(100, 43, 4, 1, 1, ''),
(101, 43, 4, 1, 2, ''),
(102, 43, 4, 1, 3, ''),
(103, 44, 4, 2, 4, '-1/2'),
(104, 44, 4, 2, 5, ''),
(105, 45, 4, 3, 6, ''),
(106, 45, 4, 3, 7, ''),
(107, 46, 4, 4, 8, ''),
(108, 46, 4, 4, 9, ''),
(109, 47, 4, 5, 10, ''),
(110, 47, 4, 5, 11, ''),
(111, 48, 4, 6, 12, ''),
(112, 48, 4, 6, 13, ''),
(113, 49, 4, 7, 14, ''),
(114, 49, 4, 7, 15, ''),
(115, 50, 4, 8, 16, ''),
(116, 50, 4, 8, 17, ''),
(117, 51, 4, 9, 18, '-1/8'),
(118, 51, 4, 9, 19, ''),
(119, 52, 4, 10, 20, ''),
(120, 52, 4, 10, 21, ''),
(121, 53, 4, 11, 22, ''),
(122, 53, 4, 11, 23, ''),
(123, 53, 4, 11, 24, ''),
(124, 54, 4, 12, 25, ''),
(125, 54, 4, 12, 26, '1'),
(126, 54, 4, 12, 27, ''),
(127, 55, 4, 13, 28, ''),
(128, 55, 4, 13, 29, '-1/8'),
(129, 55, 4, 13, 30, '-1/8'),
(130, 55, 4, 13, 31, '-1/4'),
(131, 56, 4, 14, 32, ''),
(132, 56, 4, 14, 33, ''),
(133, 57, 5, 1, 1, ''),
(134, 57, 5, 1, 2, ''),
(135, 57, 5, 1, 3, ''),
(136, 58, 5, 2, 4, '-1/2'),
(137, 58, 5, 2, 5, ''),
(138, 59, 5, 3, 6, ''),
(139, 59, 5, 3, 7, ''),
(140, 60, 5, 4, 8, ''),
(141, 60, 5, 4, 9, ''),
(142, 61, 5, 5, 10, ''),
(143, 61, 5, 5, 11, ''),
(144, 62, 5, 6, 12, ''),
(145, 62, 5, 6, 13, ''),
(146, 63, 5, 7, 14, ''),
(147, 63, 5, 7, 15, ''),
(148, 64, 5, 8, 16, ''),
(149, 64, 5, 8, 17, ''),
(150, 65, 5, 9, 18, '-1/8'),
(151, 65, 5, 9, 19, ''),
(152, 66, 5, 10, 20, ''),
(153, 66, 5, 10, 21, ''),
(154, 67, 5, 11, 22, ''),
(155, 67, 5, 11, 23, ''),
(156, 67, 5, 11, 24, ''),
(157, 68, 5, 12, 25, ''),
(158, 68, 5, 12, 26, '1'),
(159, 68, 5, 12, 27, ''),
(160, 69, 5, 13, 28, ''),
(161, 69, 5, 13, 29, '-1/8'),
(162, 69, 5, 13, 30, '-1/8'),
(163, 69, 5, 13, 31, '-1/4'),
(164, 70, 5, 14, 32, ''),
(165, 70, 5, 14, 33, ''),
(166, 71, 6, 1, 1, ''),
(167, 71, 6, 1, 2, ''),
(168, 71, 6, 1, 3, ''),
(169, 72, 6, 2, 4, '-1/2'),
(170, 72, 6, 2, 5, ''),
(171, 73, 6, 3, 6, ''),
(172, 73, 6, 3, 7, ''),
(173, 74, 6, 4, 8, ''),
(174, 74, 6, 4, 9, ''),
(175, 75, 6, 5, 10, ''),
(176, 75, 6, 5, 11, ''),
(177, 76, 6, 6, 12, ''),
(178, 76, 6, 6, 13, ''),
(179, 77, 6, 7, 14, ''),
(180, 77, 6, 7, 15, ''),
(181, 78, 6, 8, 16, ''),
(182, 78, 6, 8, 17, ''),
(183, 79, 6, 9, 18, '-1/8'),
(184, 79, 6, 9, 19, ''),
(185, 80, 6, 10, 20, ''),
(186, 80, 6, 10, 21, ''),
(187, 81, 6, 11, 22, ''),
(188, 81, 6, 11, 23, ''),
(189, 81, 6, 11, 24, ''),
(190, 82, 6, 12, 25, ''),
(191, 82, 6, 12, 26, '1'),
(192, 82, 6, 12, 27, ''),
(193, 83, 6, 13, 28, ''),
(194, 83, 6, 13, 29, '-1/8'),
(195, 83, 6, 13, 30, '-1/8'),
(196, 83, 6, 13, 31, '-1/4'),
(197, 84, 6, 14, 32, ''),
(198, 84, 6, 14, 33, ''),
(199, 85, 7, 1, 1, ''),
(200, 85, 7, 1, 2, ''),
(201, 85, 7, 1, 3, ''),
(202, 86, 7, 2, 4, '-1/2'),
(203, 86, 7, 2, 5, ''),
(204, 87, 7, 3, 6, ''),
(205, 87, 7, 3, 7, ''),
(206, 88, 7, 4, 8, ''),
(207, 88, 7, 4, 9, ''),
(208, 89, 7, 5, 10, ''),
(209, 89, 7, 5, 11, ''),
(210, 90, 7, 6, 12, ''),
(211, 90, 7, 6, 13, ''),
(212, 91, 7, 7, 14, ''),
(213, 91, 7, 7, 15, ''),
(214, 92, 7, 8, 16, ''),
(215, 92, 7, 8, 17, ''),
(216, 93, 7, 9, 18, '-1/8'),
(217, 93, 7, 9, 19, ''),
(218, 94, 7, 10, 20, ''),
(219, 94, 7, 10, 21, ''),
(220, 95, 7, 11, 22, ''),
(221, 95, 7, 11, 23, ''),
(222, 95, 7, 11, 24, ''),
(223, 96, 7, 12, 25, ''),
(224, 96, 7, 12, 26, '1'),
(225, 96, 7, 12, 27, ''),
(226, 97, 7, 13, 28, ''),
(227, 97, 7, 13, 29, '-1/8'),
(228, 97, 7, 13, 30, '-1/8'),
(229, 97, 7, 13, 31, '-1/4'),
(230, 98, 7, 14, 32, ''),
(231, 98, 7, 14, 33, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option_option`
--

CREATE TABLE `product_attr_option_option` (
  `id` int(11) NOT NULL,
  `pro_att_op_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_op_id` int(11) NOT NULL,
  `op_op_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option_option`
--

INSERT INTO `product_attr_option_option` (`id`, `pro_att_op_id`, `product_id`, `attribute_id`, `op_op_id`, `op_op_condition`) VALUES
(1, 2, 1, 1, 1, '-3/8'),
(2, 2, 1, 1, 2, '-1/4'),
(3, 3, 1, 1, 3, '-1/4'),
(4, 3, 1, 1, 4, '-1/4'),
(5, 3, 1, 1, 5, '-1/4'),
(6, 29, 1, 13, 6, ''),
(7, 29, 1, 13, 7, ''),
(8, 30, 1, 13, 8, ''),
(9, 30, 1, 13, 9, ''),
(10, 31, 1, 13, 10, ''),
(11, 31, 1, 13, 11, ''),
(12, 35, 2, 1, 1, '-3/8'),
(13, 35, 2, 1, 2, '-1/4'),
(14, 36, 2, 1, 3, '-1/4'),
(15, 36, 2, 1, 4, '-1/4'),
(16, 36, 2, 1, 5, '-1/4'),
(17, 62, 2, 13, 6, ''),
(18, 62, 2, 13, 7, ''),
(19, 63, 2, 13, 8, ''),
(20, 63, 2, 13, 9, ''),
(21, 64, 2, 13, 10, ''),
(22, 64, 2, 13, 11, ''),
(23, 68, 3, 1, 1, '-3/8'),
(24, 68, 3, 1, 2, '-1/4'),
(25, 69, 3, 1, 3, '-1/4'),
(26, 69, 3, 1, 4, '-1/4'),
(27, 69, 3, 1, 5, '-1/4'),
(28, 95, 3, 13, 6, ''),
(29, 95, 3, 13, 7, ''),
(30, 96, 3, 13, 8, ''),
(31, 96, 3, 13, 9, ''),
(32, 97, 3, 13, 10, ''),
(33, 97, 3, 13, 11, ''),
(34, 101, 4, 1, 1, '-3/8'),
(35, 101, 4, 1, 2, '-1/4'),
(36, 102, 4, 1, 3, '-1/4'),
(37, 102, 4, 1, 4, '-1/4'),
(38, 102, 4, 1, 5, '-1/4'),
(39, 128, 4, 13, 6, ''),
(40, 128, 4, 13, 7, ''),
(41, 129, 4, 13, 8, ''),
(42, 129, 4, 13, 9, ''),
(43, 130, 4, 13, 10, ''),
(44, 130, 4, 13, 11, ''),
(45, 134, 5, 1, 1, '-3/8'),
(46, 134, 5, 1, 2, '-1/4'),
(47, 135, 5, 1, 3, '-3/8'),
(48, 135, 5, 1, 4, '-1/4'),
(49, 135, 5, 1, 5, '-3/8'),
(50, 161, 5, 13, 6, ''),
(51, 161, 5, 13, 7, ''),
(52, 162, 5, 13, 8, ''),
(53, 162, 5, 13, 9, ''),
(54, 163, 5, 13, 10, ''),
(55, 163, 5, 13, 11, ''),
(56, 167, 6, 1, 1, '-3/8'),
(57, 167, 6, 1, 2, '-1/4'),
(58, 168, 6, 1, 3, '-1/4'),
(59, 168, 6, 1, 4, '-1/4'),
(60, 168, 6, 1, 5, '-1/4'),
(61, 194, 6, 13, 6, ''),
(62, 194, 6, 13, 7, ''),
(63, 195, 6, 13, 8, ''),
(64, 195, 6, 13, 9, ''),
(65, 196, 6, 13, 10, ''),
(66, 196, 6, 13, 11, ''),
(67, 200, 7, 1, 1, '-3/8'),
(68, 200, 7, 1, 2, '-1/4'),
(69, 201, 7, 1, 3, '-1/4'),
(70, 201, 7, 1, 4, '-1/4'),
(71, 201, 7, 1, 5, '-1/4'),
(72, 227, 7, 13, 6, ''),
(73, 227, 7, 13, 7, ''),
(74, 228, 7, 13, 8, ''),
(75, 228, 7, 13, 9, ''),
(76, 229, 7, 13, 10, ''),
(77, 229, 7, 13, 11, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option_option_option`
--

CREATE TABLE `product_attr_option_option_option` (
  `id` int(11) NOT NULL,
  `pro_att_op_op_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_op_op_id` int(11) NOT NULL,
  `op_op_op_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_conditions_tbl`
--

CREATE TABLE `product_conditions_tbl` (
  `condition_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `condition_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width_condition` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '+- and 0-9 capable',
  `width_fraction_id` int(11) NOT NULL,
  `height_condition` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '+- and 0-9 capable',
  `height_fraction_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_condition_mapping`
--

CREATE TABLE `product_condition_mapping` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `pattern_models_ids` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'multiple pattern id save use comma separator',
  `price_style_type` int(11) NOT NULL COMMENT '1= row column, 2 = sqr fit and 3 = fixed price',
  `price_rowcol_style_id` int(11) NOT NULL,
  `sqft_price` float NOT NULL,
  `fixed_price` float NOT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `shipping` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `init_stock` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_price` float NOT NULL,
  `individual_price` float DEFAULT NULL,
  `uom` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Unit of Measurement',
  `colors` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `active_status` int(11) NOT NULL COMMENT '1=active or 0=inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`product_id`, `product_name`, `category_id`, `subcategory_id`, `pattern_models_ids`, `price_style_type`, `price_rowcol_style_id`, `sqft_price`, `fixed_price`, `condition_id`, `shipping`, `tax`, `init_stock`, `dealer_price`, `individual_price`, `uom`, `colors`, `created_by`, `updated_by`, `created_date`, `updated_date`, `active_status`) VALUES
(7, '2 Inch Faux Blinds', 1, 0, '9', 1, 2, 0, 0, NULL, NULL, NULL, NULL, 10, NULL, '', '193,192,191', 1, 0, '2019-07-31', '0000-00-00', 1),
(8, 'test product', 1, 0, '9,10,11', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 10, NULL, '', '193', 1, 0, '2019-08-05', '0000-00-00', 1),
(9, 'demo product', 6, 0, '14,15', 5, 2, 0, 0, NULL, NULL, NULL, NULL, 10, NULL, '', '', 150, 0, '2019-08-08', '0000-00-00', 1),
(10, 'main product', 6, 0, '14', 2, 0, 10, 0, NULL, NULL, NULL, NULL, 10, NULL, '', '237', 150, 0, '2019-08-06', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details_tbl`
--

CREATE TABLE `purchase_details_tbl` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `purchase_price` float NOT NULL,
  `quantity` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `discount` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_tbl`
--

CREATE TABLE `purchase_tbl` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `invoice_discount` float DEFAULT NULL,
  `total_discount` float DEFAULT NULL,
  `grand_total` float NOT NULL,
  `paid_amount` float DEFAULT NULL,
  `due_amount` float DEFAULT NULL,
  `payment_type` tinyint(1) NOT NULL COMMENT '1=cash ,2=check',
  `check_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_branch_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quatation_attributes`
--

CREATE TABLE `quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quatation_attributes`
--

INSERT INTO `quatation_attributes` (`row_id`, `fk_od_id`, `order_id`, `product_id`, `product_attribute`) VALUES
(77, 77, '1564250897CWPIC-003', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(78, 78, '1564251108PDU2C-004', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(79, 79, '1564251588FVQHC-005', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(80, 80, '1564251785ARPSC-006', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(81, 81, '1564251785ARPSC-006', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(82, 82, '1564251785ARPSC-006', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(83, 83, '1564251785ARPSC-006', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(84, 84, '1564252028E4DAC-007', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(85, 85, '1564252028E4DAC-007', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(86, 86, '1564252028E4DAC-007', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(87, 87, '1564252028E4DAC-007', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(90, 90, '1564252489UEBDC-007', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(91, 91, '1564252649DBBLC-008', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(92, 92, '1564252649DBBLC-008', 3, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(93, 93, '1564253146BWSAC-009', 1, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(94, 94, '1564254122USGHC-010', 4, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(95, 95, '1564254204FUMFC-011', 2, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(96, 96, '1564311596CURLC-012', 2, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(98, 98, '1564578996V635C-013', 7, '[{\"attribute_id\":\"1\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"1\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"2\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"4\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"3\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"6\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"4\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"8\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"5\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"10\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"6\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"12\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"7\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"14\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"8\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"16\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"9\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"18\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"10\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"20\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"11\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"22\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"12\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"25\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"13\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"28\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]},{\"attribute_id\":\"14\",\"attribute_value\":\"\",\"options\":[{\"option_id\":\"32\",\"option_value\":\"\"}],\"opop\":[],\"opopop\":[],\"opopopop\":[]}]'),
(99, 99, '15650042321GXNC-014', 8, '[]'),
(100, 100, '1565160813LAHMC-015', 9, '[]'),
(101, 101, '1565160813LAHMC-015', 10, '[]'),
(102, 102, '1565172271RTPFC-016', 10, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `quatation_tbl`
--

CREATE TABLE `quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `cancel_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `synk_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quatation_tbl`
--

INSERT INTO `quatation_tbl` (`id`, `order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `cancel_comment`, `level_id`, `order_date`, `created_by`, `updated_by`, `synk_status`, `created_date`, `updated_date`) VALUES
(47, '1564250897CWPIC-003', 22, 'Rajiv-1004', '', 'assets/barcode/c/1564250897CWPIC-003.jpg', '0', '', 0, 0, 0, 0, 0, '0', 663, 663, 0, 663, 0, '1', 1, NULL, 144, '2019-07-27', 144, 0, 1, '2019-07-27', '0000-00-00'),
(48, '1564251108PDU2C-004', 23, 'Rajiv-1000', '', 'assets/barcode/c/1564251108PDU2C-004.jpg', '0', '', 0, 0, 0, 0, 0, '0', 753, 753, 376.5, 376.5, 0, '1', 3, NULL, 144, '2019-07-27', 144, 0, 1, '2019-07-27', '0000-00-00'),
(49, '1564251588FVQHC-005', 24, 'Daniel-1000', '', 'assets/barcode/c/1564251588FVQHC-005.jpg', '0', '', 8.25, 0, 0, 0, 110.37, '0', 1200, 1210.5, 600, 600, 0, '1', 3, NULL, 143, '2019-07-27', 143, 0, 1, '2019-07-27', '0000-00-00'),
(53, '1564252489UEBDC-007', 20, 'Jessica-1000', '', 'assets/barcode/c/1564252489UEBDC-007.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 648.85, 599.4, 324.43, 324.42, 0, '1', 3, NULL, 143, '2019-07-27', 143, 0, 1, '2019-07-27', '0000-00-00'),
(54, '1564252649DBBLC-008', 24, 'Daniel-1000', '', 'assets/barcode/c/1564252649DBBLC-008.jpg', '0', '', 8.25, 0, 0, 0, 200.91, '0', 2224, 2240.1, 1112, 1112, 0, '1', 3, NULL, 143, '2019-07-27', 143, 0, 1, '2019-07-27', '0000-00-00'),
(55, '1564253146BWSAC-009', 24, 'Daniel-1000', '', 'assets/barcode/c/1564253146BWSAC-009.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 1455.96, 1345, 727.98, 727.98, 46.4, '1', 3, NULL, 143, '2019-07-27', 145, 0, 1, '2019-07-27', '0000-00-00'),
(56, '1564254122USGHC-010', 24, 'Daniel-1000', '', 'assets/barcode/c/1564254122USGHC-010.jpg', '0', '', 8.25, 0, 0, 0, 83.57, '0', 1013, 1013, 506.5, 506.5, 50.65, '1', 3, NULL, 143, '2019-07-27', 147, 0, 1, '2019-07-27', '0000-00-00'),
(57, '1564254204FUMFC-011', 20, 'Jessica-1000', '', 'assets/barcode/c/1564254204FUMFC-011.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 914.71, 845, 457.36, 457.35, 9.15, '1', 3, NULL, 143, '2019-07-27', 148, 0, 1, '2019-07-27', '0000-00-00'),
(58, '1564311596CURLC-012', 24, 'Daniel-1000', '', 'assets/barcode/c/1564311596CURLC-012.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 880.72, 813.6, 0, 880.72, 0, '1', 1, NULL, 143, '2019-07-28', 143, 0, 1, '2019-07-28', '0000-00-00'),
(60, '1564578996V635C-013', 23, 'Rajiv-1000', '', 'assets/barcode/c/1564578996V635C-013.jpg', '0', '', 0, 0, 0, 0, 0, '0', 1125, 1125, 0, 1125, 0, '1', 1, NULL, 144, '2019-07-31', 144, 0, 0, '2019-07-31', '0000-00-00'),
(61, '15650042321GXNC-014', 24, 'Daniel-1000', '', 'assets/barcode/c/15650042321GXNC-014.jpg', '0', '', 8.25, 0, 0, 0, 0, '0', 21.65, 20, 0, 21.65, 0, '1', 1, NULL, 143, '2019-08-05', 143, 0, 1, '2019-08-05', '0000-00-00'),
(62, '1565160813LAHMC-015', 30, 'd_user-Indore', '', 'assets/barcode/c/1565160813LAHMC-015.jpg', '0', '', 0, 0, 0, 0, 0, '0', 96427.4, 96427.4, 0, 96427.4, 0, '1', 1, NULL, 152, '2019-08-07', 152, 0, 1, '2019-08-07', '0000-00-00'),
(63, '1565172271RTPFC-016', 31, 'new_d-Indore', '', 'assets/barcode/c/1565172271RTPFC-016.jpg', '0', '', 0, 0, 0, 0, 0, '0', 696.18, 696.18, 348.09, 348.09, 0, '1', 3, NULL, 152, '2019-08-07', 152, 0, 1, '2019-08-07', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_details`
--

CREATE TABLE `quote_comparison_details` (
  `row_id` bigint(20) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `room_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `pattern_id` int(11) DEFAULT NULL,
  `quote_price` float NOT NULL,
  `subtotal` float DEFAULT NULL,
  `product_price` float NOT NULL,
  `upcharge` float DEFAULT NULL,
  `discount` float NOT NULL,
  `product_data` text COLLATE utf8_unicode_ci NOT NULL,
  `upcharge_data` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_rooms`
--

CREATE TABLE `quote_comparison_rooms` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `window_qty` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_tbl`
--

CREATE TABLE `quote_comparison_tbl` (
  `quote_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `grand_total` float NOT NULL,
  `sub_total` float NOT NULL,
  `sales_tax` float NOT NULL,
  `created_date` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qutation_details`
--

CREATE TABLE `qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `list_price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `unit_total_price` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `qutation_details`
--

INSERT INTO `qutation_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `list_price`, `discount`, `unit_total_price`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`) VALUES
(77, '1564250897CWPIC-003', 'Study', '1', 1, 1, 663, 0, 663, 1, 30, 70, 70, 0, 0, 'Testing'),
(78, '1564251108PDU2C-004', 'Dining', '1', 1, 1, 753, 0, 753, 1, 31, 70, 80, 0, 4, ''),
(79, '1564251588FVQHC-005', 'Study', '1', 1, 1, 1345, 10, 1210.5, 1, 29, 102, 106, 0, 0, 'Testing'),
(80, '1564251785ARPSC-006', 'Living', '1', 1, 1, 880, 10, 792, 1, 29, 80, 86, 1, 4, ''),
(81, '1564251785ARPSC-006', 'Living', '1', 1, 1, 813, 10, 731.7, 1, 29, 70, 88, 4, 4, ''),
(82, '1564251785ARPSC-006', 'Study', '1', 1, 1, 646, 10, 581.4, 1, 30, 60, 77, 4, 4, ''),
(83, '1564251785ARPSC-006', 'Study', '1', 1, 1, 753, 10, 677.7, 1, 29, 70, 80, 4, 4, ''),
(84, '1564252028E4DAC-007', 'Living', '1', 1, 1, 880, 10, 792, 1, 29, 80, 86, 1, 4, ''),
(85, '1564252028E4DAC-007', 'Living', '1', 1, 1, 813, 10, 731.7, 1, 29, 70, 88, 4, 4, ''),
(86, '1564252028E4DAC-007', 'Study', '1', 1, 1, 646, 10, 581.4, 1, 30, 60, 77, 4, 4, ''),
(87, '1564252028E4DAC-007', 'Study', '1', 1, 1, 753, 10, 677.7, 1, 29, 70, 80, 4, 4, ''),
(90, '1564252489UEBDC-007', 'Dining', '1', 1, 1, 666, 10, 599.4, 1, 29, 80, 60, 4, 4, ''),
(91, '1564252649DBBLC-008', 'Guest Bath', '1', 1, 1, 753, 10, 677.7, 1, 30, 70, 80, 4, 4, 'Testing'),
(92, '1564252649DBBLC-008', 'Living', '3', 1, 1, 1736, 10, 1562.4, 5, 91, 88, 90, 8, 1, ''),
(93, '1564253146BWSAC-009', 'Dining', '1', 1, 1, 1345, 0, 1345, 2, 38, 102, 106, 4, 0, 'Testing'),
(94, '1564254122USGHC-010', 'Guest', '4', 1, 1, 1013, 0, 1013, 5, 95, 77, 80, 4, 4, 'Testing'),
(95, '1564254204FUMFC-011', 'Living', '2', 1, 1, 845, 0, 845, 4, 103, 65, 80, 1, 1, ''),
(96, '1564311596CURLC-012', 'Guest', '2', 1, 1, 904, 10, 813.6, 4, 56, 66, 78, 4, 4, 'test'),
(98, '1564578996V635C-013', 'Study', '7', 1, 1, 1125, 0, 1125, 9, 191, 94, 94, 4, 4, 'Test'),
(99, '15650042321GXNC-014', 'Dining', '8', 1, 1, 20, 0, 20, 9, 193, 10, 18, 2, 3, 'test'),
(100, '1565160813LAHMC-015', 'Dining', '9', 6, 1, 96420.2, 0, 96420.2, 14, 232, 200, 200, 2, 1, 'cus first order'),
(101, '1565160813LAHMC-015', 'Dining', '10', 6, 1, 7.12, 0, 7.12, 14, 232, 10, 10, 1, 1, 'cus second order'),
(102, '1565172271RTPFC-016', 'Living', '10', 6, 1, 696.18, 0, 696.18, 14, 232, 100, 100, 1, 1, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_color_mapping_tbl`
--

CREATE TABLE `raw_material_color_mapping_tbl` (
  `id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_return_details_tbl`
--

CREATE TABLE `raw_material_return_details_tbl` (
  `return_id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_return_tbl`
--

CREATE TABLE `raw_material_return_tbl` (
  `return_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `return_date` date NOT NULL,
  `return_comments` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `returned_by` int(11) NOT NULL,
  `is_approved` tinyint(1) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_permission_tbl`
--

CREATE TABLE `role_permission_tbl` (
  `id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `can_access` tinyint(1) NOT NULL,
  `can_create` tinyint(1) NOT NULL,
  `can_edit` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_permission_tbl`
--

INSERT INTO `role_permission_tbl` (`id`, `role_id`, `menu_id`, `can_access`, `can_create`, `can_edit`, `can_delete`, `created_by`, `level_id`, `create_date`) VALUES
(1345, 13, 11, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1346, 13, 12, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1347, 13, 13, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1348, 13, 14, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1349, 13, 15, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1350, 13, 16, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1351, 13, 17, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1352, 13, 18, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1353, 13, 19, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1354, 13, 20, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1355, 13, 21, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1356, 13, 22, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1357, 13, 23, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1358, 13, 24, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1359, 13, 25, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1360, 13, 26, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1361, 13, 66, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1362, 13, 68, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1363, 13, 1, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1364, 13, 2, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1365, 13, 3, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1366, 13, 40, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1367, 13, 67, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1368, 13, 44, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1369, 13, 45, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1370, 13, 46, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1371, 13, 47, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1372, 13, 48, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1373, 13, 49, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1374, 13, 50, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1375, 13, 51, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1376, 13, 53, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1377, 13, 54, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1378, 13, 55, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1379, 13, 56, 1, 1, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1380, 13, 57, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1381, 13, 62, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1382, 13, 4, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1383, 13, 5, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1384, 13, 6, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1385, 13, 7, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1386, 13, 8, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1387, 13, 9, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1388, 13, 10, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1389, 13, 64, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1390, 13, 41, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1391, 13, 42, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1392, 13, 43, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1393, 13, 65, 1, 1, 1, 0, 143, 143, '2019-07-27 19:00:32'),
(1394, 13, 27, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1395, 13, 28, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1396, 13, 29, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1397, 13, 30, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1398, 13, 31, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1399, 13, 32, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1400, 13, 33, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1401, 13, 34, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1402, 13, 35, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1403, 13, 36, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1404, 13, 37, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1405, 13, 38, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1406, 13, 39, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1407, 13, 60, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32'),
(1408, 13, 61, 0, 0, 0, 0, 143, 143, '2019-07-27 19:00:32');

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`id`, `role_name`, `description`, `created_by`, `level_id`, `create_date`, `role_status`) VALUES
(13, 'Sales Person', '', 143, 143, '2019-07-27 19:00:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_name`) VALUES
(1, 'Study'),
(2, 'Dining'),
(3, 'Living'),
(4, 'Guest'),
(5, 'Guest Bath'),
(6, 'Master Bedroom'),
(7, 'Master Bathroom'),
(8, 'Master Closet'),
(9, 'Exercise Room'),
(10, 'Family'),
(11, 'Nook'),
(12, 'Patio Door'),
(13, 'Kitchen'),
(14, 'Utility'),
(15, 'Mud Room'),
(16, 'Garage'),
(17, 'Sunroom'),
(18, 'Hallway-1'),
(19, 'Hallway-2'),
(20, 'Bedroom-1'),
(21, 'Bedroom-2'),
(22, 'Bedroom-3'),
(23, 'Main Door'),
(24, 'Stairs'),
(25, 'Upstairs Room-1'),
(26, 'Upstairs Room-2'),
(27, 'Upstairs Room-3'),
(28, 'Upstairs Room-4'),
(29, 'Upstairs Closet-1'),
(30, 'Upstairs Closet-2'),
(31, 'Game'),
(32, 'Card Room'),
(33, 'Bar'),
(34, 'Landing'),
(35, 'Upstairs Bath-1'),
(36, 'Upstairs Bath-2'),
(37, 'Upstairs Bath-3'),
(38, 'Upstairs Hall-1'),
(39, 'Upstairs Hall-2'),
(40, 'Media'),
(41, 'Other Room');

-- --------------------------------------------------------

--
-- Table structure for table `row_column`
--

CREATE TABLE `row_column` (
  `style_id` int(11) NOT NULL,
  `style_name` text NOT NULL,
  `style_type` int(2) NOT NULL COMMENT '1 = row-column price, 4 = group price',
  `create_data_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `row_column`
--

INSERT INTO `row_column` (`style_id`, `style_name`, `style_type`, `create_data_time`, `create_by`, `active_status`) VALUES
(1, '2 1/2 Inch Shutter Blinds', 1, '2019-07-25 14:50:38', 1, 1),
(2, '2 Inch Faux Blinds', 1, '2019-07-25 15:41:08', 1, 1),
(3, '2 1/2 Inch Basswood Blinds', 1, '2019-07-25 16:40:58', 1, 1),
(4, '2 Inch Basswood Blinds', 1, '2019-07-25 16:44:47', 1, 1),
(5, '3 Inch Wi-Vi Blinds', 1, '2019-07-25 17:12:00', 1, 1),
(6, 'NON OPERABLE EYEBROWS', 1, '2019-07-25 17:39:26', 1, 1),
(7, 'NON OPERABLE EYEBROWS with SCREEN', 1, '2019-07-25 17:39:51', 1, 1),
(8, 'SOLID EYEBROWS', 1, '2019-07-25 17:40:10', 1, 1),
(9, 'FACIAS', 1, '2019-07-25 17:40:27', 1, 1),
(10, 'OPERABLE ARCHES', 1, '2019-07-25 17:40:47', 1, 1),
(11, 'OPERABLE EYEBROWS', 1, '2019-07-25 17:41:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `row_material_stock_tbl`
--

CREATE TABLE `row_material_stock_tbl` (
  `id` bigint(20) NOT NULL,
  `row_material_id` int(11) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `in_qty` int(11) NOT NULL,
  `out_qty` int(11) NOT NULL,
  `from_module` varchar(200) NOT NULL,
  `measurment` float NOT NULL,
  `stock_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `row_material_tbl`
--

CREATE TABLE `row_material_tbl` (
  `id` int(11) NOT NULL,
  `material_name` text NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `uom` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shipment_data`
--

CREATE TABLE `shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `customer_name` varchar(250) DEFAULT NULL,
  `customer_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_method`
--

CREATE TABLE `shipping_method` (
  `id` int(11) NOT NULL,
  `method_name` varchar(100) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `access_token` varchar(200) NOT NULL,
  `account_id` varchar(100) NOT NULL,
  `service_type` varchar(20) DEFAULT NULL,
  `pickup_method` varchar(20) DEFAULT NULL,
  `mode` varchar(20) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_method`
--

INSERT INTO `shipping_method` (`id`, `method_name`, `username`, `password`, `access_token`, `account_id`, `service_type`, `pickup_method`, `mode`, `created_by`, `updated_by`, `status`) VALUES
(1, 'UPS', 'ejaipal123', 'India75063#', 'ED5B9CD43D1C1EAC', '2YE475', NULL, '01', 'test', 1, 1, 1),
(2, 'Delivery in Person', 'Null', 'Null', 'Null', 'Null', NULL, NULL, NULL, 1, 1, 1),
(3, 'Customer Pickup', 'Null', 'Null', 'Null', 'Null', NULL, '01', 'test', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway`
--

CREATE TABLE `sms_gateway` (
  `gateway_id` int(11) NOT NULL,
  `provider_name` text NOT NULL,
  `user` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `authentication` text NOT NULL,
  `link` text NOT NULL,
  `default_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `is_verify` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 : No | 1 : Yes ',
  `test_sms_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_gateway`
--

INSERT INTO `sms_gateway` (`gateway_id`, `provider_name`, `user`, `password`, `phone`, `authentication`, `link`, `default_status`, `status`, `created_by`, `is_verify`, `test_sms_number`) VALUES
(4, 'Twilio', 'AC742f8864f265947d93b163a91ff098d5', 'b21a37c51834e01c2f30a35b8b35e87a', '+14694051516', 'BMS Window Decor', '', 1, 1, 0, 0, ''),
(5, 'Twilio', 'ACbf1bb71d0816cd1c88b81b8a240e792f', 'c5b6264fb1bf8ed2c628c1c6af71b2d7', '+12062024567', 'PaTaaK', '', 1, 1, 144, 1, '+14782871258');

-- --------------------------------------------------------

--
-- Table structure for table `sqm_price_model_mapping_tbl`
--

CREATE TABLE `sqm_price_model_mapping_tbl` (
  `id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_id` int(11) NOT NULL,
  `pattern_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sqm_price_model_mapping_tbl`
--

INSERT INTO `sqm_price_model_mapping_tbl` (`id`, `price`, `product_id`, `pattern_id`) VALUES
(1, 10.00, 94, '333'),
(3, 20.00, 97, '340'),
(4, 140.00, 103, '340'),
(5, 10.00, 94, '333'),
(6, 20.00, 8, '9'),
(8, 24000.00, 9, '14');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_raw_material_mapping`
--

CREATE TABLE `supplier_raw_material_mapping` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_tbl`
--

CREATE TABLE `supplier_tbl` (
  `supplier_id` int(11) NOT NULL,
  `supplier_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_sku` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `material_product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previous_balance` float DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price_item` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag_tbl`
--

CREATE TABLE `tag_tbl` (
  `id` int(11) NOT NULL,
  `tag_name` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag_tbl`
--

INSERT INTO `tag_tbl` (`id`, `tag_name`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(7, 'Category', 1, 1, '2019-07-26', '2019-07-26');

-- --------------------------------------------------------

--
-- Table structure for table `tms_payment_setting`
--

CREATE TABLE `tms_payment_setting` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mode` tinyint(4) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tms_payment_setting`
--

INSERT INTO `tms_payment_setting` (`id`, `url`, `user_name`, `password`, `mode`, `level_id`) VALUES
(1, 'https://secure.tmspaymentgateway.com/api/transact.php', 'bms6750', 'Bmsd123#', 0, 1),
(2, 'https://secure.tmspaymentgateway.com/api/transact.php', 'Lega6615', 'Lega123#', 0, 143);

-- --------------------------------------------------------

--
-- Table structure for table `unit_of_measurement`
--

CREATE TABLE `unit_of_measurement` (
  `uom_id` int(11) NOT NULL,
  `uom_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `unit_of_measurement`
--

INSERT INTO `unit_of_measurement` (`uom_id`, `uom_name`, `status`) VALUES
(2, 'Lbs', 1),
(3, 'Inches', 1),
(4, 'Cms', 1),
(5, 'Ft', 1),
(7, 'Box', 1),
(9, 'Roll', 1),
(11, 'Yard', 1),
(12, 'Bundle', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_tbl`
--

CREATE TABLE `user_access_tbl` (
  `role_acc_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_access_tbl`
--

INSERT INTO `user_access_tbl` (`role_acc_id`, `role_id`, `user_id`, `created_by`, `level_id`) VALUES
(1, 11, '141', '140', 140),
(8, 13, '148', '143', 143),
(9, 13, '147', '143', 143),
(10, 13, '145', '143', 143);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `created_by` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fixed_commission` float DEFAULT '0',
  `percentage_commission` float DEFAULT '0',
  `create_date` date NOT NULL,
  `update_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `user_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'B or C'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `created_by`, `first_name`, `last_name`, `user_image`, `company`, `address`, `city`, `state`, `zip_code`, `country_code`, `phone`, `email`, `reference`, `language`, `fixed_commission`, `percentage_commission`, `create_date`, `update_date`, `updated_by`, `user_type`) VALUES
(1, '', 'BMSLink', 'Info', '', 'BMS Window Decor', '3333 Silas Creek Parkway, Winston-Salem, NC, USA', 'Winston-Salem', 'Forsyth County', '27103', 'US', '+1(469)-491-9966', 'info@bmsdecor.com', '', 'English', 0, 0, '2019-01-02', '2019-07-01', 1, 'b'),
(141, '140', 'Denial', 'Park', NULL, '', '', '', '', '', '', '', 'info@bmslink.net', '', 'English', 0, 5, '2019-07-26', '0000-00-00', 0, 'c'),
(142, '1', 'Rajiv', 'Pastula', NULL, 'PaTaaK Inc', '100 Park Avenue', 'New York', 'US', '10017', 'US', '+1 (478) 287-1258', 'rajiv@technoplusinc.com', '', 'English', 0, 0, '2019-07-27', '0000-00-00', 0, 'c'),
(143, '1', 'James', 'Park', NULL, 'Legacy Blinds Mfg', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (972) 820-0007', 'legacyblinds@aol.com', '', 'English', 0, 0, '2019-07-27', '0000-00-00', 0, 'c'),
(144, '1', 'Rajiv', 'Pastula', NULL, 'Legacy blinds', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (478) 287-1258', 'admin@gmail.com', '', 'English', 0, 0, '2019-07-27', '0000-00-00', 0, 'c'),
(145, '143', 'Michael', 'Smith', NULL, '', '', '', '', '', '', '', 'admin1@gmail.com', '', 'English', 10, 5, '2019-07-27', '0000-00-00', 0, 'c'),
(147, '143', 'Maria', 'Garcia', NULL, '', '', '', '', '', '', '', 'admin2@gmail.com', '', 'English', 0, 10, '2019-07-27', '0000-00-00', 0, 'c'),
(148, '143', 'Roert', 'Smith', NULL, '', '', '', '', '', '', '', 'admin3@gmail.com', '', 'English', 0, 2, '2019-07-27', '0000-00-00', 0, 'c'),
(149, '1', 'Rajiv', 'Pastula', NULL, 'TechnoPlus Inc', '1004 Ridge Hollow Trail', 'Irving', 'TX', '75063', 'US', '+1 (478) 287-1258', 'rajiv@technoplusinc.com', '', 'English', 0, 0, '2019-07-29', '0000-00-00', 0, 'c'),
(150, '', 'peter', 'parker', '', 'todayinfra_main', 'indore', 'indore', 'mp', '12345', 'IN', '9876543210', 'peter@mailinator.com', '', 'English', 0, 0, '2019-08-05', '2019-08-07', 150, 'b'),
(152, '150', 'first', 'customer', NULL, 'todayinfra', 'indore', 'indore', 'mp', '12345', 'India', '+912121212121', 'first_customer@customer.com', '', 'English', 0, 0, '2019-08-06', '0000-00-00', 0, 'c');

-- --------------------------------------------------------

--
-- Table structure for table `us_state_tbl`
--

CREATE TABLE `us_state_tbl` (
  `state_id` int(11) NOT NULL,
  `shortcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` float NOT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `us_state_tbl`
--

INSERT INTO `us_state_tbl` (`state_id`, `shortcode`, `state_name`, `tax_rate`, `level_id`, `created_by`) VALUES
(1, 'TX', 'Texas', 8.25, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `width_height_fractions`
--

CREATE TABLE `width_height_fractions` (
  `id` int(11) NOT NULL,
  `fraction_value` varchar(20) NOT NULL,
  `decimal_value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `width_height_fractions`
--

INSERT INTO `width_height_fractions` (`id`, `fraction_value`, `decimal_value`) VALUES
(1, '1/8', 0.125),
(2, '3/4', 0.75),
(3, '3/8', 0.375),
(4, '1/2', 0.5),
(5, '5/8', 0.625),
(7, '7/8', 0.875),
(8, '1/4', 0.25);

-- --------------------------------------------------------

--
-- Table structure for table `ws_payments`
--

CREATE TABLE `ws_payments` (
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesslog`
--
ALTER TABLE `accesslog`
  ADD UNIQUE KEY `SerialNo` (`sl_no`);

--
-- Indexes for table `acc_coa`
--
ALTER TABLE `acc_coa`
  ADD PRIMARY KEY (`row_id`),
  ADD UNIQUE KEY `HeadCode` (`HeadCode`,`level_id`);

--
-- Indexes for table `acc_coa_default`
--
ALTER TABLE `acc_coa_default`
  ADD UNIQUE KEY `HeadCode` (`HeadCode`,`level_id`);

--
-- Indexes for table `acc_customer_income`
--
ALTER TABLE `acc_customer_income`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_glsummarybalance`
--
ALTER TABLE `acc_glsummarybalance`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `appointment_calendar`
--
ALTER TABLE `appointment_calendar`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `attr_options`
--
ALTER TABLE `attr_options`
  ADD PRIMARY KEY (`att_op_id`);

--
-- Indexes for table `attr_options_option_option_tbl`
--
ALTER TABLE `attr_options_option_option_tbl`
  ADD PRIMARY KEY (`att_op_op_op_id`);

--
-- Indexes for table `attr_options_option_tbl`
--
ALTER TABLE `attr_options_option_tbl`
  ADD PRIMARY KEY (`op_op_id`);

--
-- Indexes for table `b_acc_coa`
--
ALTER TABLE `b_acc_coa`
  ADD PRIMARY KEY (`row_id`) USING BTREE,
  ADD UNIQUE KEY `row_id` (`HeadName`) USING BTREE;

--
-- Indexes for table `b_acc_transaction`
--
ALTER TABLE `b_acc_transaction`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `b_cost_factor_tbl`
--
ALTER TABLE `b_cost_factor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_level_quatation_attributes`
--
ALTER TABLE `b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_level_quatation_tbl`
--
ALTER TABLE `b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_level_qutation_details`
--
ALTER TABLE `b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_menusetup_tbl`
--
ALTER TABLE `b_menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_notification_tbl`
--
ALTER TABLE `b_notification_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_role_permission_tbl`
--
ALTER TABLE `b_role_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_role_tbl`
--
ALTER TABLE `b_role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_to_b_level_quatation_attributes`
--
ALTER TABLE `b_to_b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_to_b_level_quatation_tbl`
--
ALTER TABLE `b_to_b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_user_access_tbl`
--
ALTER TABLE `b_user_access_tbl`
  ADD PRIMARY KEY (`role_acc_id`);

--
-- Indexes for table `b_user_catalog_products`
--
ALTER TABLE `b_user_catalog_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_user_catalog_request`
--
ALTER TABLE `b_user_catalog_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `city_state_tbl`
--
ALTER TABLE `city_state_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_tbl`
--
ALTER TABLE `color_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_profile`
--
ALTER TABLE `company_profile`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `customer_commet_tbl`
--
ALTER TABLE `customer_commet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_file_tbl`
--
ALTER TABLE `customer_file_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_phone_type_tbl`
--
ALTER TABLE `customer_phone_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_query`
--
ALTER TABLE `customer_query`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `custom_sms_tbl`
--
ALTER TABLE `custom_sms_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_card_info`
--
ALTER TABLE `c_card_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_cost_factor_tbl`
--
ALTER TABLE `c_cost_factor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_level_upcharges`
--
ALTER TABLE `c_level_upcharges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_menusetup_tbl`
--
ALTER TABLE `c_menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_notification_tbl`
--
ALTER TABLE `c_notification_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_quatation_details`
--
ALTER TABLE `c_quatation_details`
  ADD PRIMARY KEY (`qd_id`);

--
-- Indexes for table `c_quatation_table`
--
ALTER TABLE `c_quatation_table`
  ADD PRIMARY KEY (`qt_id`);

--
-- Indexes for table `c_us_state_tbl`
--
ALTER TABLE `c_us_state_tbl`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_id` (`image_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `gateway_tbl`
--
ALTER TABLE `gateway_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_row_column_price_tbl`
--
ALTER TABLE `group_row_column_price_tbl`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `iframe_code_tbl`
--
ALTER TABLE `iframe_code_tbl`
  ADD PRIMARY KEY (`frame_id`);

--
-- Indexes for table `log_info`
--
ALTER TABLE `log_info`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `mail_config_tbl`
--
ALTER TABLE `mail_config_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturing_b_level_quatation_attributes`
--
ALTER TABLE `manufacturing_b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturing_b_level_qutation_details`
--
ALTER TABLE `manufacturing_b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufactur_data`
--
ALTER TABLE `manufactur_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menusetup_tbl`
--
ALTER TABLE `menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_details`
--
ALTER TABLE `order_return_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_tbl`
--
ALTER TABLE `order_return_tbl`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `pattern_model_tbl`
--
ALTER TABLE `pattern_model_tbl`
  ADD PRIMARY KEY (`pattern_model_id`);

--
-- Indexes for table `payment_card_tbl`
--
ALTER TABLE `payment_card_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_check_tbl`
--
ALTER TABLE `payment_check_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `price_chaild_style`
--
ALTER TABLE `price_chaild_style`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_model_mapping_tbl`
--
ALTER TABLE `price_model_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_style`
--
ALTER TABLE `price_style`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `product_attribute`
--
ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option`
--
ALTER TABLE `product_attr_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option_option`
--
ALTER TABLE `product_attr_option_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option_option_option`
--
ALTER TABLE `product_attr_option_option_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_conditions_tbl`
--
ALTER TABLE `product_conditions_tbl`
  ADD PRIMARY KEY (`condition_id`);

--
-- Indexes for table `product_condition_mapping`
--
ALTER TABLE `product_condition_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchase_details_tbl`
--
ALTER TABLE `purchase_details_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_tbl`
--
ALTER TABLE `purchase_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_id` (`purchase_id`);

--
-- Indexes for table `quatation_attributes`
--
ALTER TABLE `quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_comparison_details`
--
ALTER TABLE `quote_comparison_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `quote_comparison_rooms`
--
ALTER TABLE `quote_comparison_rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `quote_comparison_tbl`
--
ALTER TABLE `quote_comparison_tbl`
  ADD PRIMARY KEY (`quote_id`);

--
-- Indexes for table `qutation_details`
--
ALTER TABLE `qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `raw_material_color_mapping_tbl`
--
ALTER TABLE `raw_material_color_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_material_return_tbl`
--
ALTER TABLE `raw_material_return_tbl`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `role_permission_tbl`
--
ALTER TABLE `role_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `row_column`
--
ALTER TABLE `row_column`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `row_material_stock_tbl`
--
ALTER TABLE `row_material_stock_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `row_material_tbl`
--
ALTER TABLE `row_material_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_data`
--
ALTER TABLE `shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_method`
--
ALTER TABLE `shipping_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  ADD PRIMARY KEY (`gateway_id`);

--
-- Indexes for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_raw_material_mapping`
--
ALTER TABLE `supplier_raw_material_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tag_tbl`
--
ALTER TABLE `tag_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_payment_setting`
--
ALTER TABLE `tms_payment_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_of_measurement`
--
ALTER TABLE `unit_of_measurement`
  ADD PRIMARY KEY (`uom_id`);

--
-- Indexes for table `user_access_tbl`
--
ALTER TABLE `user_access_tbl`
  ADD PRIMARY KEY (`role_acc_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_state_tbl`
--
ALTER TABLE `us_state_tbl`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `width_height_fractions`
--
ALTER TABLE `width_height_fractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_payments`
--
ALTER TABLE `ws_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accesslog`
--
ALTER TABLE `accesslog`
  MODIFY `sl_no` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1810;
--
-- AUTO_INCREMENT for table `acc_coa`
--
ALTER TABLE `acc_coa`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=693;
--
-- AUTO_INCREMENT for table `acc_customer_income`
--
ALTER TABLE `acc_customer_income`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_glsummarybalance`
--
ALTER TABLE `acc_glsummarybalance`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `appointment_calendar`
--
ALTER TABLE `appointment_calendar`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `attr_options`
--
ALTER TABLE `attr_options`
  MODIFY `att_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `attr_options_option_option_tbl`
--
ALTER TABLE `attr_options_option_option_tbl`
  MODIFY `att_op_op_op_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attr_options_option_tbl`
--
ALTER TABLE `attr_options_option_tbl`
  MODIFY `op_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `b_acc_coa`
--
ALTER TABLE `b_acc_coa`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `b_acc_transaction`
--
ALTER TABLE `b_acc_transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `b_cost_factor_tbl`
--
ALTER TABLE `b_cost_factor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `b_level_quatation_attributes`
--
ALTER TABLE `b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `b_level_quatation_tbl`
--
ALTER TABLE `b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `b_level_qutation_details`
--
ALTER TABLE `b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `b_menusetup_tbl`
--
ALTER TABLE `b_menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `b_notification_tbl`
--
ALTER TABLE `b_notification_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `b_role_permission_tbl`
--
ALTER TABLE `b_role_permission_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_role_tbl`
--
ALTER TABLE `b_role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_level_quatation_attributes`
--
ALTER TABLE `b_to_b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_level_quatation_tbl`
--
ALTER TABLE `b_to_b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_user_access_tbl`
--
ALTER TABLE `b_user_access_tbl`
  MODIFY `role_acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `b_user_catalog_products`
--
ALTER TABLE `b_user_catalog_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `b_user_catalog_request`
--
ALTER TABLE `b_user_catalog_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `city_state_tbl`
--
ALTER TABLE `city_state_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `color_tbl`
--
ALTER TABLE `color_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT for table `company_profile`
--
ALTER TABLE `company_profile`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `customer_commet_tbl`
--
ALTER TABLE `customer_commet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_file_tbl`
--
ALTER TABLE `customer_file_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `customer_phone_type_tbl`
--
ALTER TABLE `customer_phone_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `customer_query`
--
ALTER TABLE `customer_query`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom_sms_tbl`
--
ALTER TABLE `custom_sms_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `c_card_info`
--
ALTER TABLE `c_card_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `c_cost_factor_tbl`
--
ALTER TABLE `c_cost_factor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `c_level_upcharges`
--
ALTER TABLE `c_level_upcharges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `c_menusetup_tbl`
--
ALTER TABLE `c_menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=855;
--
-- AUTO_INCREMENT for table `c_notification_tbl`
--
ALTER TABLE `c_notification_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `c_quatation_details`
--
ALTER TABLE `c_quatation_details`
  MODIFY `qd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `c_quatation_table`
--
ALTER TABLE `c_quatation_table`
  MODIFY `qt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `c_us_state_tbl`
--
ALTER TABLE `c_us_state_tbl`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `gateway_tbl`
--
ALTER TABLE `gateway_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_row_column_price_tbl`
--
ALTER TABLE `group_row_column_price_tbl`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iframe_code_tbl`
--
ALTER TABLE `iframe_code_tbl`
  MODIFY `frame_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `log_info`
--
ALTER TABLE `log_info`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `mail_config_tbl`
--
ALTER TABLE `mail_config_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_quatation_attributes`
--
ALTER TABLE `manufacturing_b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_qutation_details`
--
ALTER TABLE `manufacturing_b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufactur_data`
--
ALTER TABLE `manufactur_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `menusetup_tbl`
--
ALTER TABLE `menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `order_return_details`
--
ALTER TABLE `order_return_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_return_tbl`
--
ALTER TABLE `order_return_tbl`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pattern_model_tbl`
--
ALTER TABLE `pattern_model_tbl`
  MODIFY `pattern_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `payment_card_tbl`
--
ALTER TABLE `payment_card_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_check_tbl`
--
ALTER TABLE `payment_check_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  MODIFY `payment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `price_chaild_style`
--
ALTER TABLE `price_chaild_style`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `price_model_mapping_tbl`
--
ALTER TABLE `price_model_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `price_style`
--
ALTER TABLE `price_style`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1714;
--
-- AUTO_INCREMENT for table `product_attribute`
--
ALTER TABLE `product_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `product_attr_option`
--
ALTER TABLE `product_attr_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;
--
-- AUTO_INCREMENT for table `product_attr_option_option`
--
ALTER TABLE `product_attr_option_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `product_attr_option_option_option`
--
ALTER TABLE `product_attr_option_option_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_conditions_tbl`
--
ALTER TABLE `product_conditions_tbl`
  MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_condition_mapping`
--
ALTER TABLE `product_condition_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `purchase_details_tbl`
--
ALTER TABLE `purchase_details_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_tbl`
--
ALTER TABLE `purchase_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quatation_attributes`
--
ALTER TABLE `quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `quote_comparison_details`
--
ALTER TABLE `quote_comparison_details`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote_comparison_rooms`
--
ALTER TABLE `quote_comparison_rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote_comparison_tbl`
--
ALTER TABLE `quote_comparison_tbl`
  MODIFY `quote_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qutation_details`
--
ALTER TABLE `qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `raw_material_color_mapping_tbl`
--
ALTER TABLE `raw_material_color_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `raw_material_return_tbl`
--
ALTER TABLE `raw_material_return_tbl`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_permission_tbl`
--
ALTER TABLE `role_permission_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1409;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `row_column`
--
ALTER TABLE `row_column`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `row_material_stock_tbl`
--
ALTER TABLE `row_material_stock_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `row_material_tbl`
--
ALTER TABLE `row_material_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shipment_data`
--
ALTER TABLE `shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shipping_method`
--
ALTER TABLE `shipping_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  MODIFY `gateway_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `supplier_raw_material_mapping`
--
ALTER TABLE `supplier_raw_material_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tag_tbl`
--
ALTER TABLE `tag_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tms_payment_setting`
--
ALTER TABLE `tms_payment_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `unit_of_measurement`
--
ALTER TABLE `unit_of_measurement`
  MODIFY `uom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_access_tbl`
--
ALTER TABLE `user_access_tbl`
  MODIFY `role_acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;
--
-- AUTO_INCREMENT for table `us_state_tbl`
--
ALTER TABLE `us_state_tbl`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `width_height_fractions`
--
ALTER TABLE `width_height_fractions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ws_payments`
--
ALTER TABLE `ws_payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  ADD CONSTRAINT `gallery_tag_tbl_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `gallery_image_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gallery_tag_tbl_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag_tbl` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
