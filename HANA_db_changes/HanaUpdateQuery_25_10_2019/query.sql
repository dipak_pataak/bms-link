UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 69;
UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 70;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 70;
UPDATE `menusetup_tbl` SET `ordering` = '3' WHERE `menusetup_tbl`.`id` = 69;
UPDATE `menusetup_tbl` SET `ordering` = '4' WHERE `menusetup_tbl`.`id` = 61;
UPDATE `menusetup_tbl` SET `ordering` = '5' WHERE `menusetup_tbl`.`id` = 29;

UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 39;
UPDATE `menusetup_tbl` SET `parent_menu` = '96' WHERE `menusetup_tbl`.`id` = 84;
UPDATE `menusetup_tbl` SET `ordering` = '6' WHERE `menusetup_tbl`.`id` = 39;
UPDATE `menusetup_tbl` SET `ordering` = '7' WHERE `menusetup_tbl`.`id` = 84;


INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'Bulk Communication', 'Bulk Communication', '', 'settings', '9', '27', '1', '0', '', '1', '0', '2', '2019-03-14 19:54:23');
UPDATE `menusetup_tbl` SET `parent_menu` = '103' WHERE `menusetup_tbl`.`id` = 68;
UPDATE `menusetup_tbl` SET `parent_menu` = '103' WHERE `menusetup_tbl`.`id` = 71;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 68;
UPDATE `menusetup_tbl` SET `ordering` = '1' WHERE `menusetup_tbl`.`id` = 71;

UPDATE `menusetup_tbl` SET `menu_title` = 'enquire_form' WHERE `menusetup_tbl`.`id` = 31;
UPDATE `menusetup_tbl` SET `korean_name` = 'enquire_form' WHERE `menusetup_tbl`.`id` = 31;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'Connect Wholesaler', 'Connect Wholesaler', 'wholesaler-connection', 'settings', '1', '96', '1', '0', '', '1', '0', '2', '2019-06-24 11:42:52');



UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 94;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'stock', 'stock', '', 'c_production', '3', '87', '1', '0', '', '1', '0', '2', '2019-03-14 19:54:23');
UPDATE `menusetup_tbl` SET `parent_menu` = '105' WHERE `menusetup_tbl`.`id` = 90;
UPDATE `menusetup_tbl` SET `parent_menu` = '105' WHERE `menusetup_tbl`.`id` = 91;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 91;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'return', 'return', '', 'c_production', '4', '87', '1', '0', '', '1', '0', '2', '2019-03-14 19:54:23');
UPDATE `menusetup_tbl` SET `parent_menu` = '106' WHERE `menusetup_tbl`.`id` = 92;
UPDATE `menusetup_tbl` SET `parent_menu` = '106' WHERE `menusetup_tbl`.`id` = 93;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 93;

UPDATE `menusetup_tbl` SET `ordering` = '5' WHERE `menusetup_tbl`.`id` = 89;


/****************************/


UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 47;
UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 48;
UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 51;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'gallery', 'gallery', 'manage_c_gallery_image', 'header', '0', '45', '2', '0', 'iconsmind-Photos', '1', '0', '2', '2019-05-09 10:48:07');
INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'commission report', 'commission report', 'c_commission_report', 'header', '0', '45', '2', '0', 'iconsmind-Statistic', '1', '0', '2', '2019-05-09 10:48:07');
INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'faq', 'faq', 'faq', 'header', '0', '45', '2', '0', 'iconsmind-question', '1', '0', '2', '2019-05-09 10:48:07');

UPDATE `menusetup_tbl` SET `icon` = 'simple-icon-question' WHERE `menusetup_tbl`.`id` = 109;
UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 53;
UPDATE `menusetup_tbl` SET `status` = '0' WHERE `menusetup_tbl`.`id` = 56;
UPDATE `menusetup_tbl` SET `ordering` = '2' WHERE `menusetup_tbl`.`id` = 95;

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'Unit of Measurement', 'Unit of Measurement', 'customer-uom-list', 'settings', '9', '96', '1', '0', '', '1', '0', '69', '2019-03-14 20:02:19');
INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES (NULL, 'shipping', 'shipping', 'customer-shipping', 'settings', '8', '96', '1', '0', '', '1', '0', '69', '2019-03-14 20:02:19');
