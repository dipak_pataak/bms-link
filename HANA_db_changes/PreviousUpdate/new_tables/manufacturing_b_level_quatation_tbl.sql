-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2019 at 03:09 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_new_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_quatation_tbl`
--

CREATE TABLE `manufacturing_b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
