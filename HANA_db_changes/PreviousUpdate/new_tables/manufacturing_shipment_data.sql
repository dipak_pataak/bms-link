-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2019 at 03:09 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_new_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_shipment_data`
--

CREATE TABLE `manufacturing_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `customer_name` varchar(250) DEFAULT NULL,
  `customer_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
