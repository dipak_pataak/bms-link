-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2019 at 06:21 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `hana_package`
--

CREATE TABLE `hana_package` (
  `id` int(11) NOT NULL,
  `package` varchar(255) DEFAULT NULL,
  `trial_days` int(11) NOT NULL DEFAULT '0',
  `installation_price` double(8,2) NOT NULL DEFAULT '0.00',
  `monthly_price` double(8,2) NOT NULL DEFAULT '0.00',
  `yearly_price` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hana_package`
--

INSERT INTO `hana_package` (`id`, `package`, `trial_days`, `installation_price`, `monthly_price`, `yearly_price`) VALUES
(1, 'Basic FREE', 0, 0.00, 0.00, 0.00),
(2, 'Advanced', 20, 1999.00, 199.00, 2388.00),
(3, 'Pro', 30, 3999.00, 225.00, 3060.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hana_package`
--
ALTER TABLE `hana_package`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hana_package`
--
ALTER TABLE `hana_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
