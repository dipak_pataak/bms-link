-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2019 at 07:10 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `hana_package_option`
--

CREATE TABLE `hana_package_option` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `package_key` varchar(255) DEFAULT NULL,
  `basic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `advanced` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `pro` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hana_package_option`
--

INSERT INTO `hana_package_option` (`id`, `title`, `package_key`, `basic`, `advanced`, `pro`) VALUES
(1, 'B-level Connection', 'b_level_package', 0, 0, 0),
(2, 'New Order Form', 'new_order_form', 0, 0, 0),
(3, 'B-C Status', 'b_c_status', 0, 0, 0),
(4, 'Accouting (Basic)', 'accounting', 0, 0, 0),
(5, 'Settings (Basic)', 'setting', 0, 0, 0),
(6, 'Gallery (Basic)', 'gallery', 0, 0, 0),
(7, 'Customer (D-level)', 'customer', 0, 0, 0),
(8, 'Multiqoute', 'multiqoute', 0, 0, 0),
(9, 'Appointment', 'appointment', 0, 0, 0),
(10, 'Catalog (View existing product without price + Add new products with price)', 'catalog', 0, 0, 0),
(11, 'Transfer Order and Email', 'transfer_order_an_email', 0, 0, 0),
(12, 'Bulk SMS', 'bulk_sms', 0, 0, 0),
(13, 'Bulk Email', 'bulk_email', 0, 0, 0),
(14, 'Sales Commission', 'sales_commission', 0, 0, 0),
(15, 'Production (MFG)', 'production', 0, 0, 0),
(16, 'Inventory', 'inventory', 0, 0, 0),
(17, 'Shipping', 'shipping', 0, 0, 0),
(18, 'Setting (Pro)', 'setting', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hana_package_option`
--
ALTER TABLE `hana_package_option`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hana_package_option`
--
ALTER TABLE `hana_package_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
