-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2019 at 07:09 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesslog`
--

CREATE TABLE `accesslog` (
  `sl_no` bigint(20) NOT NULL,
  `action_page` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_done` text COLLATE utf8_unicode_ci,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `ip_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accesslog`
--

INSERT INTO `accesslog` (`sl_no`, `action_page`, `action_done`, `remarks`, `user_name`, `level_id`, `ip_address`, `entry_date`) VALUES
(628, 'super_admin', 'insert', 'User information save', '29', 0, NULL, '2019-09-16 15:08:56'),
(629, 'super_admin', 'insert', 'User information save', '2', 0, NULL, '2019-09-16 15:39:20'),
(630, 'super_admin', 'insert', 'User information save', '2', 0, NULL, '2019-09-16 16:01:48'),
(631, 'customer_save', 'insert', 'Customer information save', '2', 2, '219.91.253.114', '2019-09-16 16:07:32'),
(632, 'company-profile-update', 'updated', 'company profile information updated', '3', 3, '219.91.253.114', '2019-09-16 16:17:15'),
(633, 'super_admin', 'insert', 'User information save', '2', 0, NULL, '2019-09-17 03:31:45'),
(634, 'customer_save', 'insert', 'Customer information save', '2', 2, '219.91.253.114', '2019-09-17 11:57:58'),
(635, 'super_admin', 'insert', 'User information save', '4', 0, NULL, '2019-09-17 12:07:35'),
(636, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 12:39:05'),
(637, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 17:30:13'),
(638, 'category_update', 'updated', 'Category information updated', '2', 2, '219.91.253.114', '2019-09-17 17:30:26'),
(639, 'category_update', 'updated', 'Category information updated', '2', 2, '219.91.253.114', '2019-09-17 17:30:33'),
(640, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 17:30:53'),
(641, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 17:31:40'),
(642, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 17:59:16'),
(643, 'save_category', 'insert', 'Category information save', '2', 2, '219.91.253.114', '2019-09-17 18:02:36'),
(644, 'import-pattern-save', 'insert', 'Pattern csv information imported done', '2', 2, '219.91.253.114', '2019-09-17 18:12:59'),
(645, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:27:30'),
(646, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:30:58'),
(647, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:32:16'),
(648, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:33:01'),
(649, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:34:04'),
(650, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:38:00'),
(651, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:38:41'),
(652, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:39:03'),
(653, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:39:28'),
(654, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:39:49'),
(655, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:40:14'),
(656, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:40:34'),
(657, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:41:01'),
(658, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:41:24'),
(659, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:41:48'),
(660, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:42:17'),
(661, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:42:52'),
(662, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:43:17'),
(663, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:43:41'),
(664, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 18:45:27'),
(665, 'import-color-save', 'insert', 'color csv information imported done', '2', 2, '219.91.253.114', '2019-09-17 18:47:05'),
(666, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:03:20'),
(667, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:04:10'),
(668, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:04:52'),
(669, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:17:54'),
(670, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:47:40'),
(671, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-17 19:58:43'),
(672, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 20:00:36'),
(673, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:00:53'),
(674, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 20:03:51'),
(675, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:04:23'),
(676, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 20:05:51'),
(677, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:06:00'),
(678, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 20:10:03'),
(679, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:10:12'),
(680, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 20:13:53'),
(681, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:13:59'),
(682, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:29:35'),
(683, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:29:59'),
(684, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:30:28'),
(685, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:30:43'),
(686, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:31:23'),
(687, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:31:37'),
(688, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:31:56'),
(689, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:32:23'),
(690, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:32:38'),
(691, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:32:50'),
(692, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:33:10'),
(693, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:34:36'),
(694, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:34:59'),
(695, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:35:18'),
(696, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:35:29'),
(697, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:35:42'),
(698, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:35:56'),
(699, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:37:13'),
(700, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:37:21'),
(701, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:37:36'),
(702, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:37:49'),
(703, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:38:45'),
(704, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:39:17'),
(705, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:39:42'),
(706, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:40:05'),
(707, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:40:19'),
(708, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 20:40:33'),
(709, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:46:48'),
(710, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:47:06'),
(711, 'product_update', 'updated', 'product information updated', '2', 0, '219.91.253.114', '2019-09-17 20:48:56'),
(712, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:49:15'),
(713, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:50:21'),
(714, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:51:14'),
(715, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:52:15'),
(716, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:53:05'),
(717, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:53:38'),
(718, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:53:59'),
(719, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:54:16'),
(720, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:54:40'),
(721, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:55:08'),
(722, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:55:21'),
(723, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:55:39'),
(724, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:55:52'),
(725, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:56:15'),
(726, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:57:50'),
(727, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:58:26'),
(728, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:58:54'),
(729, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:59:13'),
(730, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:59:28'),
(731, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 20:59:55'),
(732, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:00:34'),
(733, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:01:00'),
(734, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:01:20'),
(735, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:02:30'),
(736, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:03:24'),
(737, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:06:19'),
(738, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:07:23'),
(739, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:07:45'),
(740, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:08:06'),
(741, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:08:48'),
(742, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:09:07'),
(743, 'save_price_model_mapping', 'insert', 'price model mapping information save', '2', 0, '219.91.253.114', '2019-09-17 21:11:07'),
(744, 'pattern_save', 'insert', 'Pattern information save', '2', 2, '219.91.253.114', '2019-09-17 21:17:48'),
(745, 'b_level', 'insert', 'Color information save', '2', 2, '219.91.253.114', '2019-09-17 21:19:34'),
(746, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 21:19:56'),
(747, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 21:20:11'),
(748, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 21:20:54'),
(749, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 21:21:00'),
(750, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 21:21:42'),
(751, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 21:21:47'),
(752, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:28:16'),
(753, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:28:54'),
(754, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:29:22'),
(755, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:30:07'),
(756, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:30:31'),
(757, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 21:30:57'),
(758, 'company_profile_update', 'updated', 'Company profile information updated', '2', 2, '219.91.253.114', '2019-09-17 21:35:42'),
(759, 'b_level', 'insert', 'Test email send', '2', 2, '219.91.253.114', '2019-09-17 21:41:59'),
(760, 'update-mail-configure', 'insert', 'Email configuration save', '2', 2, '219.91.253.114', '2019-09-17 21:42:43'),
(761, 'b_level', 'insert', 'Test email send', '2', 2, '219.91.253.114', '2019-09-17 21:42:56'),
(762, 'sms-config-save', 'insert', 'sms configuration insert', '2', 2, '219.91.253.114', '2019-09-17 21:44:53'),
(763, 'sms-config-update', 'updated', 'sms configuration updated', '2', 2, '219.91.253.114', '2019-09-17 21:45:04'),
(764, 'save-price-style', 'insert', 'price model information save', '2', 2, '219.91.253.114', '2019-09-17 22:03:56'),
(765, 'pattern_save', 'insert', 'Pattern information save', '2', 2, '219.91.253.114', '2019-09-17 22:05:09'),
(766, 'b_level', 'insert', 'Color information save', '2', 2, '219.91.253.114', '2019-09-17 22:06:26'),
(767, 'product_save', 'insert', 'product information save', '2', 0, '219.91.253.114', '2019-09-17 22:07:55'),
(768, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-17 22:08:12'),
(769, 'update_price_style', 'updated', 'price model information updated', '2', 2, '219.91.253.114', '2019-09-18 09:55:08'),
(770, 'save_attribute', 'insert', 'attribute information save', '2', 2, '219.91.253.114', '2019-09-18 11:29:24'),
(771, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-18 11:39:26'),
(772, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-18 11:39:30'),
(773, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '219.91.253.114', '2019-09-18 11:39:34'),
(774, 'save_category', 'insert', 'Category information save', '4', 4, '::1', '2019-09-18 14:25:21'),
(775, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '::1', '2019-09-18 14:36:12'),
(776, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '::1', '2019-09-18 14:46:58'),
(777, 'save_shipping_method', 'insert', 'Shipping method insert', '2', 2, '::1', '2019-09-18 15:06:37'),
(778, 'save_shipping_method', 'insert', 'Shipping method insert', '2', 2, '::1', '2019-09-18 15:06:56'),
(779, 'save_shipping_method', 'insert', 'Shipping method insert', '2', 2, '::1', '2019-09-18 15:25:31'),
(780, 'import-pattern-save', 'insert', 'Pattern csv information imported done', '4', 4, '::1', '2019-09-18 15:35:20'),
(781, 'import-color-save', 'insert', 'color csv information imported done', '4', 4, '::1', '2019-09-18 15:39:22'),
(782, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 15:46:10'),
(783, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 15:49:43'),
(784, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 15:50:59'),
(785, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:00:58'),
(786, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:01:32'),
(787, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:05:49'),
(788, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:07:16'),
(789, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:08:06'),
(790, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:11:40'),
(791, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:14:08'),
(792, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:16:07'),
(793, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:16:43'),
(794, 'save_attribute', 'insert', 'attribute information save', '4', 4, '::1', '2019-09-18 16:17:12'),
(795, 'category_update', 'updated', 'Category information updated', '4', 4, '::1', '2019-09-18 16:51:46'),
(796, 'category_update', 'updated', 'Category information updated', '4', 4, '::1', '2019-09-18 16:57:50'),
(797, 'category_update', 'updated', 'Category information updated', '4', 4, '::1', '2019-09-18 17:11:37'),
(798, 'pattern_update', 'Updated', 'pattern information updated', '4', 4, '::1', '2019-09-18 17:13:10'),
(799, 'pattern_update', 'Updated', 'pattern information updated', '4', 4, '::1', '2019-09-18 17:13:19'),
(800, 'import-color-save', 'insert', 'color csv information imported done', '4', 4, '::1', '2019-09-18 18:43:20'),
(801, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:07:56'),
(802, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:08:05'),
(803, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:08:36'),
(804, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:08:42'),
(805, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:08:48'),
(806, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:08:54'),
(807, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:09:00'),
(808, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:09:07'),
(809, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:09:12'),
(810, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:09:18'),
(811, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:09:23'),
(812, 'company_profile_update', 'updated', 'Company profile information updated', '4', 4, '::1', '2019-09-18 19:14:33'),
(813, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:22:55'),
(814, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:23:18'),
(815, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:23:34'),
(816, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:24:23'),
(817, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:24:43'),
(818, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:25:05'),
(819, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:25:27'),
(820, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:25:43'),
(821, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:25:59'),
(822, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:27:59'),
(823, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:28:52'),
(824, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:29:26'),
(825, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:29:48'),
(826, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:30:01'),
(827, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:30:27'),
(828, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:30:51'),
(829, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:31:45'),
(830, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:32:18'),
(831, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:32:38'),
(832, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:33:30'),
(833, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:33:49'),
(834, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:35:35'),
(835, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:36:27'),
(836, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:37:14'),
(837, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:37:49'),
(838, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:38:31'),
(839, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:38:59'),
(840, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:39:46'),
(841, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:40:44'),
(842, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:41:24'),
(843, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:44:45'),
(844, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:45:07'),
(845, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:45:23'),
(846, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:46:59'),
(847, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:47:17'),
(848, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:47:42'),
(849, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:48:49'),
(850, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:49:38'),
(851, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:49:50'),
(852, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:50:04'),
(853, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:50:32'),
(854, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:51:09'),
(855, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:51:25'),
(856, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:51:38'),
(857, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:52:00'),
(858, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:52:17'),
(859, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:52:46'),
(860, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:53:01'),
(861, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:53:20'),
(862, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:53:37'),
(863, 'product_update', 'updated', 'product information updated', '4', 0, '::1', '2019-09-18 19:55:30'),
(864, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:55:50'),
(865, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:56:04'),
(866, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:56:52'),
(867, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:57:08'),
(868, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:57:26'),
(869, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:57:47'),
(870, 'save_sqm_price_model_mapping', 'insert', 'sqm price model mapping information save', '4', 0, '::1', '2019-09-18 19:58:03'),
(871, 'company_profile_update', 'updated', 'Company profile information updated', '4', 4, '219.91.253.114', '2019-09-18 20:34:28'),
(872, 'b-update-password', 'updated', 'password change successfully', '4', 4, '219.91.253.114', '2019-09-18 20:36:49'),
(873, 'b-update-password', 'updated', 'password change successfully', '2', 2, '219.91.253.114', '2019-09-18 20:38:03'),
(874, 'c-update-password', 'update', 'user password updated', '3', 3, '219.91.253.114', '2019-09-18 20:42:15'),
(875, 'super_admin', 'insert', 'User information save', '8', 0, NULL, '2019-09-19 16:11:08'),
(876, 'super_admin', 'insert', 'User information save', '9', 0, NULL, '2019-09-19 16:27:34'),
(877, 'save_category', 'insert', 'Category information save', '8', 8, '127.0.0.1', '2019-09-19 16:37:06'),
(878, 'b_level', 'insert', 'Color information save', '8', 8, '127.0.0.1', '2019-09-19 16:39:24'),
(879, 'pattern_save', 'insert', 'Pattern information save', '8', 8, '127.0.0.1', '2019-09-19 16:39:45'),
(880, 'b_level', 'insert', 'Color information save', '8', 8, '127.0.0.1', '2019-09-19 16:40:02'),
(881, 'product_save', 'insert', 'product information save', '8', 0, '127.0.0.1', '2019-09-19 16:40:37'),
(882, 'super_admin', 'insert', 'User information save', '10', 0, NULL, '2019-09-19 16:47:38'),
(883, 'super_admin', 'insert', 'User information save', '11', 0, NULL, '2019-09-19 16:48:27'),
(884, 'save_order', 'inserted', 'order information inserted', '12', 12, '127.0.0.1', '2019-09-20 11:35:34'),
(885, 'product_save', 'insert', 'product information save', '2', 0, '127.0.0.1', '2019-09-20 11:40:20'),
(886, 'save_maping_product_attribute', 'insert', 'mapping product attribute information save', '2', 0, '127.0.0.1', '2019-09-20 11:43:13'),
(887, 'supplier_save', 'insert', 'supplier information save', '2', 2, '127.0.0.1', '2019-09-21 15:41:06'),
(888, 'supplier_save', 'insert', 'supplier information save', '2', 2, '127.0.0.1', '2019-09-21 16:08:42');

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa`
--

CREATE TABLE `acc_coa` (
  `row_id` bigint(20) NOT NULL,
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa`
--

INSERT INTO `acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(1661, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1662, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1663, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1664, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1665, '1010202', 'Computers & Printers etc', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1666, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1667, '1010401', 'Phones', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1668, '1010404', 'Internal office Design', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1669, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1670, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1671, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1672, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1673, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1674, '1020101', 'Petty Cash', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1675, '1020102', 'Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1676, '102010201', 'Bank Of America', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1677, '1020103', 'Credit Card', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1678, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1679, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1680, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1681, '10202', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1682, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1683, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1684, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1685, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1686, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1687, '301', 'Business Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1688, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1689, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1690, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1691, '4', 'Expense', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1692, '402', 'Other Expenses', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1693, '40201', 'Travel & Food', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1694, '40202', 'Telephone Bill', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1695, '40203', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1696, '40204', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1697, '40205', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1698, '4020501', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1699, '4020502', 'Miscellaneous Benefits', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1700, '40206', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1701, '40207', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1702, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1703, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1704, '50101', 'Long Term Loans', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1705, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1706, '50201', 'Short Term Loans', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1707, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1708, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1709, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1710, '5020301', 'Taxes', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 3, '2', '2019-09-16 00:00:00', '2', '2019-09-16 00:00:00'),
(1711, '102030101', 'CUS-0004-caption america', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '12', '2019-09-20 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa_default`
--

CREATE TABLE `acc_coa_default` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) DEFAULT NULL COMMENT 'log_info.user_id',
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa_default`
--

INSERT INTO `acc_coa_default` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
('1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010202', 'Computers & Printers etc', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010401', 'Phones', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010404', 'Internal office Design', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020101', 'Petty Cash', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020102', 'Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102010201', 'Bank Of America', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020103', 'Credit Card', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10202', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('301', 'Business Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4', 'Expense', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('402', 'Other Expenses', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40201', 'Travel & Food', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40202', 'Telephone Bill', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40203', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40204', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40205', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4020501', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4020502', 'Miscellaneous Benefits', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40206', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40207', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50101', 'Long Term Loans', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50201', 'Short Term Loans', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5020301', 'Taxes', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acc_customer_income`
--

CREATE TABLE `acc_customer_income` (
  `ID` int(11) NOT NULL,
  `Customer_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Amount` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_glsummarybalance`
--

CREATE TABLE `acc_glsummarybalance` (
  `ID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `FYear` int(11) DEFAULT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_income_expence`
--

CREATE TABLE `acc_income_expence` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Student_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Paymode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Perpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci NOT NULL,
  `StoreID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `IsApprove` tinyint(4) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_temp`
--

CREATE TABLE `acc_temp` (
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Debit` decimal(18,2) NOT NULL,
  `Credit` decimal(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_transaction`
--

CREATE TABLE `acc_transaction` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `StoreID` int(11) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_calendar`
--

CREATE TABLE `appointment_calendar` (
  `appointment_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `c_level_id` int(11) NOT NULL,
  `appointment_date` date NOT NULL,
  `appointment_time` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `c_level_staff_id` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_by` int(11) NOT NULL,
  `level_from` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_tbl`
--

CREATE TABLE `attribute_tbl` (
  `attribute_id` int(11) NOT NULL,
  `attribute_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_type` tinyint(4) DEFAULT NULL COMMENT '1=text,2=option',
  `category_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_tbl`
--

INSERT INTO `attribute_tbl` (`attribute_id`, `attribute_name`, `attribute_type`, `category_id`, `position`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(1, 'Multi Blinds', 2, 1, 1, 2, 1, '2019-08-09', '2019-09-14', 0),
(2, 'Mount', 2, 1, 2, 2, 1, '2019-08-09', '2019-09-14', 0),
(3, 'Hold Down', 2, 1, 3, 2, 0, '2019-08-09', '2019-09-14', 0),
(4, 'Tilt Type', 2, 1, 4, 2, 0, '2019-08-09', '2019-09-14', 0),
(5, 'Tilt Position', 2, 1, 5, 2, 0, '2019-08-09', '2019-09-14', 0),
(6, 'Lift Type', 2, 1, 6, 2, 0, '2019-08-09', '2019-09-14', 0),
(7, 'Lift Position', 2, 1, 7, 2, 0, '2019-08-09', '2019-09-14', 0),
(8, 'Routless', 2, 1, 8, 2, 0, '2019-08-09', '2019-09-14', 0),
(9, 'Valance Type', 2, 1, 9, 2, 0, '2019-08-09', '2019-09-14', 0),
(10, 'Valance Size', 2, 1, 10, 2, 0, '2019-08-09', '2019-09-14', 0),
(11, 'Common Valance', 2, 1, 11, 2, 0, '2019-08-09', '2019-09-14', 0),
(12, 'Valance Clips', 2, 1, 12, 2, 0, '2019-08-09', '2019-09-14', 0),
(13, 'Custom Valance Returned', 2, 1, 13, 2, 0, '2019-08-09', '2019-09-14', 0),
(14, 'Tile Cut Outs', 2, 1, 14, 2, 0, '2019-08-09', '2019-09-14', 0),
(15, 'Image Pattern', 1, 2, 1, 2, 0, '2019-08-10', '2019-09-14', 0),
(16, 'Privacy Height', 2, 2, 2, 2, 0, '2019-08-10', '2019-09-14', 0),
(17, 'Mount', 2, 2, 3, 2, 0, '2019-08-10', '2019-09-14', 0),
(18, 'Control Type', 2, 2, 4, 2, 0, '2019-08-10', '2019-09-14', 0),
(19, 'Control Position', 2, 2, 5, 2, 0, '2019-08-10', '2019-09-14', 0),
(20, 'Headrail', 2, 2, 6, 2, 0, '2019-08-10', '2019-09-14', 0),
(22, 'Bottom Rail', 2, 2, 7, 2, 0, '2019-08-10', '2019-09-14', 0),
(23, 'Tube', 2, 2, 8, 2, 0, '2019-08-10', '2019-09-14', 0),
(24, 'Side Channel', 2, 2, 9, 2, 0, '2019-08-10', '2019-09-14', 0),
(25, 'Headrail /Cord Color', 2, 2, 10, 2, 0, '2019-08-10', '2019-09-14', 0),
(26, 'Bottom Rail Color', 2, 2, 11, 2, 0, '2019-08-10', '2019-09-14', 0),
(27, 'Control Length', 2, 2, 12, 2, 0, '2019-08-10', '2019-09-14', 0),
(28, 'Side By Side', 2, 2, 13, 2, 0, '2019-08-10', '2019-09-14', 0),
(30, 'Mount', 2, 3, 1, 2, 1, '2019-08-13', '2019-09-14', 0),
(31, 'Measurements', 2, 3, 2, 2, 0, '2019-08-13', '2019-09-14', 0),
(32, 'Number of Panel', 1, 3, 3, 2, 0, '2019-08-13', '2019-09-14', 0),
(33, 'Tilt Bar', 2, 3, 4, 2, 0, '2019-08-13', '2019-09-14', 0),
(34, 'Tilt Bar Split Location', 1, 3, 5, 2, 0, '2019-08-13', '2019-09-14', 0),
(35, 'Frame Type', 2, 3, 6, 2, 0, '2019-08-13', '2019-09-14', 0),
(36, 'Frame Sides', 2, 3, 7, 2, 0, '2019-08-13', '2019-09-14', 0),
(37, 'Build out Thickness', 2, 3, 8, 2, 0, '2019-08-13', '2019-09-14', 0),
(38, 'Divider Rail', 2, 3, 9, 2, 0, '2019-08-13', '2019-09-14', 0),
(39, 'Panel Configuration', 2, 3, 11, 2, 1, '2019-08-13', '2019-08-13', 0),
(40, 'Hinge Color', 2, 3, 13, 2, 0, '2019-08-13', '2019-09-14', 0),
(41, 'Double Hung Shutters', 2, 3, 14, 2, 0, '2019-08-13', '2019-09-14', 0),
(42, 'Left Leg', 1, 4, 1, 2, 0, '2019-08-13', '2019-09-14', 0),
(43, 'Tallest Height', 1, 4, 2, 2, 0, '2019-08-13', '2019-09-14', 0),
(44, 'Right Leg', 1, 4, 3, 2, 0, '2019-08-13', '2019-09-14', 0),
(45, 'Mount', 2, 4, 4, 2, 0, '2019-08-13', '2019-09-14', 0),
(46, 'Custom Cord Length', 2, 1, 15, 2, 0, '2019-09-17', '0000-00-00', 0),
(47, 'Specialty  Type', 4, 3, 12, 2, 0, '2019-09-17', '0000-00-00', 0),
(48, 'T-Post', 2, 3, 10, 2, 0, '2019-09-18', '0000-00-00', 0),
(49, 'Image Pattern', 1, 7, 1, 4, 0, '2019-09-18', '0000-00-00', 0),
(50, 'Privacy Height', 2, 7, 2, 4, 0, '2019-09-18', '0000-00-00', 0),
(51, 'Mount', 2, 7, 3, 4, 0, '2019-09-18', '0000-00-00', 0),
(52, 'Control Type', 2, 7, 4, 4, 0, '2019-09-18', '0000-00-00', 0),
(53, 'Control Position', 2, 7, 5, 4, 0, '2019-09-18', '0000-00-00', 0),
(54, 'Headrail', 2, 7, 6, 4, 0, '2019-09-18', '0000-00-00', 0),
(55, 'Bottom Rail', 2, 7, 7, 4, 0, '2019-09-18', '0000-00-00', 0),
(56, 'Tube', 2, 7, 8, 4, 0, '2019-09-18', '0000-00-00', 0),
(57, 'Side Channel', 2, 7, 9, 4, 0, '2019-09-18', '0000-00-00', 0),
(58, 'Headrail /Cord Color', 2, 7, 10, 4, 0, '2019-09-18', '0000-00-00', 0),
(59, 'Bottom Rail Color', 2, 7, 11, 4, 0, '2019-09-18', '0000-00-00', 0),
(60, 'Control Length', 2, 7, 12, 4, 0, '2019-09-18', '0000-00-00', 0),
(61, 'Side By Side', 2, 7, 13, 4, 0, '2019-09-18', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `attr_options`
--

CREATE TABLE `attr_options` (
  `att_op_id` int(11) NOT NULL,
  `option_name` text NOT NULL,
  `option_type` tinyint(4) DEFAULT NULL COMMENT '1=text,2=option',
  `attribute_id` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `price_type` tinyint(4) DEFAULT NULL COMMENT '1=$,2=%',
  `op_condition` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_options`
--

INSERT INTO `attr_options` (`att_op_id`, `option_name`, `option_type`, `attribute_id`, `price`, `price_type`, `op_condition`) VALUES
(1, 'No', 0, 1, 10, 1, ''),
(2, '2 on One', 3, 1, 5, 2, ''),
(3, '3 on One', 3, 1, 10, 2, ''),
(4, 'Inside Mount (IB)', 0, 2, 10, 1, '-1/2'),
(5, 'Outside Mount (OB)', 0, 2, 0, 1, ''),
(6, 'Yes', 0, 3, 0, 1, ''),
(7, 'No', 0, 3, 0, 1, ''),
(8, 'Cord Tilter', 0, 4, 0, 1, ''),
(9, 'Wand Tilter', 0, 4, 0, 1, ''),
(10, 'Left', 0, 5, 0, 1, ''),
(11, 'Right', 0, 5, 0, 1, ''),
(12, 'Cord Lift', 0, 6, 0, 1, ''),
(13, 'Cordless Lift', 0, 6, 40, 1, ''),
(14, 'Right', 0, 7, 0, 1, ''),
(15, 'Left', 0, 7, 0, 1, ''),
(16, 'No', 0, 8, 0, 1, ''),
(17, 'Yes', 0, 8, 20, 2, ''),
(18, '3 1/4\" Royal', 0, 9, 0, 1, ''),
(19, '2 1/2\" Standard ', 0, 9, 0, 1, ''),
(20, '4\" WiVi', 0, 9, 0, 1, ''),
(21, 'Default', 0, 10, 0, 1, '-1/8'),
(22, 'Custom Size', 1, 10, 0, 1, ''),
(23, 'No', 0, 11, 0, 1, ''),
(24, 'Yes', 1, 11, 0, 1, ''),
(25, 'C- Clip', 0, 12, 0, 1, ''),
(26, 'High Position', 0, 12, 0, 1, ''),
(27, 'Low Position', 0, 12, 0, 1, ''),
(28, 'No Return', 0, 13, 0, 1, ''),
(29, '1/2\" Return', 0, 13, 4, 1, '1'),
(30, 'Custom Size Return', 1, 13, 4, 1, '1'),
(31, 'No', 0, 14, 0, 1, ''),
(32, 'Bottom Left Cutout', 5, 14, 0, 1, '-1/8'),
(33, 'Bottom Right Cutout', 5, 14, 0, 1, '-1/8'),
(34, 'Both Bottom Cutout', 5, 14, 0, 1, '-1/4'),
(35, '20\"', 0, 16, 0, 1, ''),
(36, '30\"', 0, 16, 0, 1, ''),
(37, '40\"', 0, 16, 0, 1, ''),
(38, 'Inside Mount (IB)', 0, 17, 0, 1, '-1/8'),
(39, 'Outside Mount(OB)', 0, 17, 0, 1, ''),
(40, 'End Mount(EB)', 0, 17, 0, 1, '-1/4'),
(41, 'Continuous Cord', 0, 18, 0, 1, ''),
(42, 'Chain', 0, 18, 0, 1, ''),
(43, 'Motorized', 4, 18, 0, 1, ''),
(44, 'Right', 0, 19, 0, 1, ''),
(45, 'Left', 0, 19, 0, 1, ''),
(46, 'Triple Cassette (01)', 0, 20, 0, 1, ''),
(47, 'Combi Cassette (02)', 0, 20, 0, 1, ''),
(48, 'Roller Screen Cassette', 2, 20, 0, 1, ''),
(49, 'Roller Screen Facia', 2, 20, 0, 1, ''),
(50, 'Fabric Covered Cassette', 0, 20, 10, 2, ''),
(51, 'Fabric Covered Facia', 0, 20, 10, 2, ''),
(58, 'Round Bottom Rail (01)', 0, 22, 0, 1, ''),
(59, 'Standard combi ', 0, 22, 0, 1, ''),
(60, 'Triple Bottom Rail', 0, 22, 0, 1, ''),
(61, 'U triple Bottom Rail', 0, 22, 0, 1, ''),
(62, '32mm (1 1/4\")', 0, 23, 0, 1, ''),
(63, '38mm (1 1/2\")', 0, 23, 0, 1, ''),
(64, '45mm (1 3/4\")', 0, 23, 0, 1, ''),
(65, 'Not selected', 0, 24, 0, 1, ''),
(66, 'Yes', 2, 24, 0, 1, ''),
(67, 'Default', 0, 25, 0, 1, ''),
(68, 'Choose different color', 2, 25, 0, 1, ''),
(69, 'Default', 0, 26, 0, 1, ''),
(70, 'Choose different color', 2, 26, 0, 1, ''),
(71, 'Default', 0, 27, 0, 1, ''),
(72, 'Custom Cord Length', 1, 27, 0, 1, ''),
(73, 'No', 0, 28, 0, 1, ''),
(74, 'Yes', 0, 28, 0, 1, ''),
(78, 'Inside Mound', 0, 30, 0, 1, ''),
(79, 'Outside Mound', 0, 30, 0, 1, ''),
(80, 'Window Size', 0, 31, 0, 1, ''),
(81, 'Finished Size', 0, 31, 0, 1, ''),
(82, 'Front Tilt-Standard', 0, 33, 0, 1, ''),
(83, 'Hidden Tilt', 0, 33, 0, 1, ''),
(84, '2.5\" Z Frames', 0, 35, 0, 1, ''),
(85, 'L Frame', 0, 35, 0, 1, ''),
(86, 'French Door L Frame', 0, 35, 0, 1, ''),
(87, '3\" Z Frames', 0, 35, 0, 1, ''),
(88, '4 Sided', 0, 36, 0, 1, ''),
(89, '3 Sided w/Sill Plate', 0, 36, 0, 1, ''),
(90, '3 Sided', 0, 36, 0, 1, ''),
(91, 'None', 0, 37, 0, 1, ''),
(92, 'Entry', 1, 37, 0, 1, ''),
(93, '1 Divider Rail', 0, 38, 0, 1, ''),
(94, '2 Divider Rails', 0, 38, 0, 1, ''),
(95, '1 Panel-L', 0, 39, 0, 1, ''),
(96, '1 Panel-R', 0, 39, 0, 1, ''),
(97, '2 Panel-LL', 0, 39, 0, 1, ''),
(98, '2 Panel-LR', 0, 39, 0, 1, ''),
(99, '2 Panel-RR', 0, 39, 0, 1, ''),
(100, '3 Panel-LLR', 0, 39, 0, 1, ''),
(101, '3 Panel-LRR', 0, 39, 0, 1, ''),
(102, '4 Panel-LLRR', 0, 39, 0, 1, ''),
(103, 'Paint Hinge-Paint coordinate', 0, 40, 0, 1, ''),
(104, 'Specialty Hinge', 1, 40, 0, 1, ''),
(105, 'No', 0, 41, 0, 1, ''),
(106, 'Yes', 2, 41, 0, 1, ''),
(107, 'Inside Mount', 0, 45, 0, 1, ''),
(108, 'Outside Mount', 0, 45, 0, 1, ''),
(109, 'No', 0, 46, 0, 1, ''),
(110, 'Custom Cord Length', 1, 46, 0, 1, ''),
(111, 'Contoured-Full Arch', 3, 47, 0, 1, ''),
(112, 'Contoured-Half Arch-Left', 3, 47, 0, 1, ''),
(113, 'Contoured-Half Arch-Right', 3, 47, 0, 1, ''),
(114, 'No', 0, 48, 0, 1, ''),
(115, 'Yes', 2, 48, 0, 1, ''),
(116, '20\"', 0, 50, 0, 1, ''),
(117, '30\"', 0, 50, 0, 1, ''),
(118, '40\"', 0, 50, 0, 1, ''),
(119, 'Inside Mount (IB)', 0, 51, 0, 1, '-1/8'),
(120, 'Outside Mount(OB)', 0, 51, 0, 1, ''),
(121, 'End Mount(EB)', 0, 51, 0, 1, '-1/4'),
(122, 'Continuous Cord', 0, 52, 0, 1, ''),
(123, 'Chain', 0, 52, 0, 1, ''),
(124, 'Motorized', 4, 52, 0, 1, ''),
(125, 'Right', 0, 53, 0, 1, ''),
(126, 'Left', 0, 53, 0, 1, ''),
(127, 'Triple Cassette (01)', 0, 54, 0, 1, ''),
(128, 'Combi Cassette (02)', 0, 54, 0, 1, ''),
(129, 'Roller Screen Cassette', 2, 54, 0, 1, ''),
(130, 'Roller Screen Facia', 2, 54, 0, 1, ''),
(131, 'Fabric Covered Cassette', 0, 54, 10, 2, ''),
(132, 'Fabric Covered Facia', 0, 54, 10, 2, ''),
(133, 'Round Bottom Rail (01)', 0, 55, 0, 1, ''),
(134, 'Standard combi ', 0, 55, 0, 1, ''),
(135, 'Triple Bottom Rail', 0, 55, 0, 1, ''),
(136, 'U triple Bottom Rail', 0, 55, 0, 1, ''),
(137, '32mm (1 1/4\")', 0, 56, 0, 1, ''),
(138, '38mm (1 1/2\")', 0, 56, 0, 1, ''),
(139, '45mm (1 3/4\")', 0, 56, 0, 1, ''),
(140, 'Not selected', 0, 57, 0, 1, ''),
(141, 'Yes', 2, 57, 0, 1, ''),
(142, 'Default', 0, 58, 0, 1, ''),
(143, 'Choose different color', 2, 58, 0, 1, ''),
(144, 'Default', 0, 59, 0, 1, ''),
(145, 'Choose different color ', 2, 59, 0, 1, ''),
(146, 'Default', 0, 60, 0, 1, ''),
(147, 'Custome Cord Length', 1, 60, 0, 1, ''),
(148, 'Yes', 0, 61, 0, 1, ''),
(149, 'No', 0, 61, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `attr_options_option_option_tbl`
--

CREATE TABLE `attr_options_option_option_tbl` (
  `att_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_name` text NOT NULL,
  `att_op_op_op_type` int(11) NOT NULL,
  `att_op_op_id` int(11) NOT NULL,
  `op_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `att_op_op_op_price_type` varchar(4) DEFAULT NULL,
  `att_op_op_op_price` float DEFAULT NULL,
  `att_op_op_op_condition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_options_option_option_tbl`
--

INSERT INTO `attr_options_option_option_tbl` (`att_op_op_op_id`, `att_op_op_op_name`, `att_op_op_op_type`, `att_op_op_id`, `op_id`, `attribute_id`, `att_op_op_op_price_type`, `att_op_op_op_price`, `att_op_op_op_condition`) VALUES
(1, 'Rechargeable Motor-Charger', 0, 12, 43, 18, '1', 310, ''),
(2, 'Rechargeable Motor-Solar ', 0, 12, 43, 18, '1', 340, ''),
(3, 'A/C Motor', 0, 12, 43, 18, '1', 300, ''),
(4, '6 Channel Remote', 0, 13, 43, 18, '1', 40, ''),
(5, '16 Channel Remote', 0, 13, 43, 18, '1', 50, ''),
(6, 'Smart Hub', 0, 13, 43, 18, '1', 130, ''),
(7, 'CH 1', 0, 14, 43, 18, '1', 0, ''),
(8, 'CH 2', 0, 14, 43, 18, '1', 0, ''),
(9, 'CH 3', 0, 14, 43, 18, '1', 0, ''),
(10, 'CH 4', 0, 14, 43, 18, '1', 0, ''),
(11, 'CH 5', 0, 14, 43, 18, '1', 0, ''),
(12, 'CH 6', 0, 14, 43, 18, '1', 0, ''),
(13, 'CH 7', 0, 14, 43, 18, '1', 0, ''),
(14, 'CH 8', 0, 14, 43, 18, '1', 0, ''),
(15, 'CH 9', 0, 14, 43, 18, '1', 0, ''),
(16, 'CH 10', 0, 14, 43, 18, '1', 0, ''),
(17, 'CH 11', 0, 14, 43, 18, '1', 0, ''),
(18, 'CH 12', 0, 14, 43, 18, '1', 0, ''),
(19, 'CH 13', 0, 14, 43, 18, '1', 0, ''),
(20, 'CH 14', 0, 14, 43, 18, '1', 0, ''),
(21, 'CH 15', 0, 14, 43, 18, '1', 0, ''),
(22, 'CH 16', 0, 14, 43, 18, '1', 0, ''),
(23, 'White', 0, 15, 48, 20, '1', 0, ''),
(24, 'Black', 0, 15, 48, 20, '1', 0, ''),
(25, 'Silver', 0, 15, 48, 20, '1', 0, ''),
(26, 'Bronze', 0, 15, 48, 20, '1', 0, ''),
(27, 'Ivory', 0, 15, 48, 20, '1', 0, ''),
(28, 'White', 0, 16, 48, 20, '1', 0, ''),
(29, 'Black', 0, 16, 48, 20, '1', 0, ''),
(30, 'Silver', 0, 16, 48, 20, '1', 0, ''),
(31, 'Bronze', 0, 16, 48, 20, '1', 0, ''),
(32, 'Ivory', 0, 16, 48, 20, '1', 0, ''),
(44, 'White', 0, 17, 48, 20, '1', 0, ''),
(45, 'Black', 0, 17, 48, 20, '1', 0, ''),
(46, 'Silver', 0, 17, 48, 20, '1', 0, ''),
(47, 'Bronze', 0, 17, 48, 20, '1', 0, ''),
(48, 'Ivory', 0, 17, 48, 20, '1', 0, ''),
(49, 'White', 0, 18, 48, 20, '1', 0, ''),
(50, 'Black', 0, 18, 48, 20, '1', 0, ''),
(51, 'Silver', 0, 18, 48, 20, '1', 0, ''),
(52, 'Bronze', 0, 18, 48, 20, '1', 0, ''),
(53, 'Ivory', 0, 18, 48, 20, '1', 0, ''),
(54, '1st section panel configuration', 2, 70, 115, 48, '1', 0, ''),
(55, 'T-Post #1 Location', 1, 70, 115, 48, '1', 0, ''),
(56, '2nd section panel configuration', 2, 70, 115, 48, '1', 0, ''),
(57, 'T-Post #2 Location', 1, 70, 115, 48, '1', 0, ''),
(58, '3rd section panel configuration', 2, 70, 115, 48, '1', 0, ''),
(59, 'T-Post #3 Location', 1, 70, 115, 48, '1', 0, ''),
(60, '1st section panel configuration', 2, 71, 115, 48, '1', 0, ''),
(61, 'T-Post #1 Location', 1, 71, 115, 48, '1', 0, ''),
(62, '2nd section panel configuration', 2, 71, 115, 48, '1', 0, ''),
(63, 'T-Post #2 Location', 1, 71, 115, 48, '1', 0, ''),
(64, '3rd section panel configuration', 2, 71, 115, 48, '1', 0, ''),
(65, 'T-Post #3 Location', 1, 71, 115, 48, '1', 0, ''),
(66, 'Rechargeable Motor-Charger', 0, 72, 124, 52, '1', 310, ''),
(67, 'Rechargeable Motor-Solar', 0, 72, 124, 52, '1', 340, ''),
(68, 'A/C Motor', 0, 72, 124, 52, '1', 300, ''),
(69, 'Not Selected', 0, 73, 124, 52, '1', 0, ''),
(70, '6 Channel Remote', 0, 73, 124, 52, '1', 40, ''),
(71, '16 Channel Remote', 0, 73, 124, 52, '1', 50, ''),
(72, 'Smart Hub', 0, 73, 124, 52, '1', 130, ''),
(73, 'CH 1', 0, 74, 124, 52, '1', 0, ''),
(74, 'CH 2', 0, 74, 124, 52, '1', 0, ''),
(75, 'CH 3', 0, 74, 124, 52, '1', 0, ''),
(76, 'CH 4', 0, 74, 124, 52, '1', 0, ''),
(77, 'CH 5', 0, 74, 124, 52, '1', 0, ''),
(78, 'CH 6', 0, 74, 124, 52, '1', 0, ''),
(79, 'CH 7', 0, 74, 124, 52, '1', 0, ''),
(80, 'CH 8', 0, 74, 124, 52, '1', 0, ''),
(81, 'CH 9', 0, 74, 124, 52, '1', 0, ''),
(82, 'CH 10', 0, 74, 124, 52, '1', 0, ''),
(83, 'CH 11', 0, 74, 124, 52, '1', 0, ''),
(84, 'CH 12', 0, 74, 124, 52, '1', 0, ''),
(85, 'CH 13', 0, 74, 124, 52, '1', 0, ''),
(86, 'CH 14', 0, 74, 124, 52, '1', 0, ''),
(87, 'CH 15', 0, 74, 124, 52, '1', 0, ''),
(88, 'CH 16', 0, 74, 124, 52, '1', 0, ''),
(89, 'White', 0, 75, 129, 54, '1', 0, ''),
(90, 'Black', 0, 75, 129, 54, '1', 0, ''),
(91, 'Silver', 0, 75, 129, 54, '1', 0, ''),
(92, 'Bronze', 0, 75, 129, 54, '1', 0, ''),
(93, 'Ivory', 0, 75, 129, 54, '1', 0, ''),
(94, 'White', 0, 76, 129, 54, '1', 0, ''),
(95, 'Black', 0, 76, 129, 54, '1', 0, ''),
(96, 'Silver', 0, 76, 129, 54, '1', 0, ''),
(97, 'Bronze', 0, 76, 129, 54, '1', 0, ''),
(98, 'Ivory', 0, 76, 129, 54, '1', 0, ''),
(99, 'White', 0, 77, 130, 54, '1', 0, ''),
(100, 'Black', 0, 77, 130, 54, '1', 0, ''),
(101, 'Silver', 0, 77, 130, 54, '1', 0, ''),
(102, 'Bronze', 0, 77, 130, 54, '1', 0, ''),
(103, 'Ivory', 0, 77, 130, 54, '1', 0, ''),
(104, 'White', 0, 78, 130, 54, '1', 0, ''),
(105, 'Black', 0, 78, 130, 54, '1', 0, ''),
(106, 'Silver', 0, 78, 130, 54, '1', 0, ''),
(107, 'Bronze', 0, 78, 130, 54, '1', 0, ''),
(108, 'Ivory', 0, 78, 130, 54, '1', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `attr_options_option_tbl`
--

CREATE TABLE `attr_options_option_tbl` (
  `op_op_id` int(11) NOT NULL,
  `op_op_name` text NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=text,2=option',
  `att_op_op_price_type` varchar(5) DEFAULT NULL,
  `att_op_op_price` float DEFAULT NULL,
  `att_op_op_condition` text NOT NULL,
  `option_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_options_option_tbl`
--

INSERT INTO `attr_options_option_tbl` (`op_op_id`, `op_op_name`, `type`, `att_op_op_price_type`, `att_op_op_price`, `att_op_op_condition`, `option_id`, `attribute_id`) VALUES
(1, 'Left', 1, '1', 0, '-3/8', 2, 1),
(2, 'Right', 1, '1', 0, '-1/4', 2, 1),
(3, 'Left', 0, '1', 0, '-1/4', 3, 1),
(4, 'Center', 0, '1', 0, '-1/4', 3, 1),
(5, 'Right', 0, '1', 0, '-1/4', 3, 1),
(6, 'Width-Tile to Wall', 1, '1', 0, '', 32, 14),
(7, 'Height-tile height', 1, '1', 0, '', 32, 14),
(8, 'Width-Wall to Tile', 1, '1', 0, '', 33, 14),
(9, 'Height-tile height', 1, '1', 0, '', 33, 14),
(10, 'Width-Tile to Tile', 1, '1', 0, '', 34, 14),
(11, 'Height-tile height', 1, '1', 0, '', 34, 14),
(12, 'Type of Motor', 2, '1', 0, '', 43, 18),
(13, 'Remote Controller', 2, '1', 0, '', 43, 18),
(14, 'Remote Channels Programming', 2, '1', 0, '', 43, 18),
(15, '3\" Cassette (03)', 2, '1', 0, '', 48, 20),
(16, '4\" Cassette (04)', 2, '1', 0, '', 48, 20),
(17, '3\" Facia (05)', 2, '1', 0, '', 49, 20),
(18, '4\" Facia (06)', 2, '1', 0, '', 49, 20),
(23, 'Ivory', 0, '1', 0, '', 66, 24),
(24, 'Beige', 0, '1', 0, '', 66, 24),
(25, 'Pink', 0, '1', 0, '', 66, 24),
(26, 'Green', 0, '1', 0, '', 66, 24),
(27, 'Blue', 0, '1', 0, '', 66, 24),
(28, 'Purple', 0, '1', 0, '', 66, 24),
(29, 'Black', 0, '1', 0, '', 66, 24),
(30, 'Brown', 0, '1', 0, '', 66, 24),
(31, 'Wine', 0, '1', 0, '', 66, 24),
(32, 'Grey', 0, '1', 0, '', 66, 24),
(33, 'White', 0, '1', 0, '', 66, 24),
(34, 'Ivory', 0, '1', 0, '', 68, 25),
(35, 'Beige', 0, '1', 0, '', 68, 25),
(36, 'Pink', 0, '1', 0, '', 68, 25),
(37, 'Green', 0, '1', 0, '', 68, 25),
(38, 'Blue', 0, '1', 0, '', 68, 25),
(39, 'Purple', 0, '1', 0, '', 68, 25),
(40, 'Black', 0, '1', 0, '', 68, 25),
(41, 'Brown', 0, '1', 0, '', 68, 25),
(42, 'Wine', 0, '1', 0, '', 68, 25),
(43, 'Grey', 0, '1', 0, '', 68, 25),
(44, 'White', 0, '1', 0, '', 68, 25),
(45, 'Ivory', 0, '1', 0, '', 70, 26),
(46, 'Beige', 0, '1', 0, '', 70, 26),
(47, 'Pink', 0, '1', 0, '', 70, 26),
(48, 'Green', 0, '1', 0, '', 70, 26),
(49, 'Blue', 0, '1', 0, '', 70, 26),
(50, 'Purple', 0, '1', 0, '', 70, 26),
(51, 'Black', 0, '1', 0, '', 70, 26),
(52, 'Brown', 0, '1', 0, '', 70, 26),
(53, 'Wine', 0, '1', 0, '', 70, 26),
(54, 'Grey', 0, '1', 0, '', 70, 26),
(55, 'White', 0, '1', 0, '', 70, 26),
(60, 'Double Hung Split Location', 1, '1', 0, '', 106, 41),
(61, 'Left Leg Height', 1, '1', 0, '', 111, 47),
(62, 'Tallest Height', 1, '1', 0, '', 111, 47),
(63, 'Right Leg Height', 1, '1', 0, '', 111, 47),
(64, 'Left Leg Height', 1, '1', 0, '', 112, 47),
(65, 'Mid Arch Height', 1, '1', 0, '', 112, 47),
(66, 'Tallest Height', 1, '1', 0, '', 112, 47),
(67, 'Tallest Height', 1, '1', 0, '', 113, 47),
(68, 'Mid Arch Height', 1, '1', 0, '', 113, 47),
(69, 'Right Leg Height', 1, '1', 0, '', 113, 47),
(70, '1\" T-Post', 4, '1', 0, '', 115, 48),
(71, '2\" T-Post', 4, '1', 0, '', 115, 48),
(72, 'Type of Motor', 2, '1', 0, '', 124, 52),
(73, 'Remote Controller', 2, '1', 0, '', 124, 52),
(74, 'Remote Channels Programming', 2, '1', 0, '', 124, 52),
(75, '3\" Cassette (03)', 2, '1', 0, '', 129, 54),
(76, '4\" Cassettte (04)', 2, '1', 0, '', 129, 54),
(77, '3\" Facia (05)', 2, '1', 0, '', 130, 54),
(78, '4\" Facia (06)', 2, '1', 0, '', 130, 54),
(79, 'Ivory', 0, '1', 0, '', 141, 57),
(80, 'Beige', 0, '1', 0, '', 141, 57),
(81, 'Pink', 0, '1', 0, '', 141, 57),
(82, 'Green', 0, '1', 0, '', 141, 57),
(83, 'Blue', 0, '1', 0, '', 141, 57),
(84, 'Purple', 0, '1', 0, '', 141, 57),
(85, 'Black', 0, '1', 0, '', 141, 57),
(86, 'Brown', 0, '1', 0, '', 141, 57),
(87, 'Wine', 0, '1', 0, '', 141, 57),
(88, 'Grey', 0, '1', 0, '', 141, 57),
(89, 'White', 0, '1', 0, '', 141, 57),
(90, 'Ivory', 0, '1', 0, '', 143, 58),
(91, 'Beige', 0, '1', 0, '', 143, 58),
(92, 'Pink', 0, '1', 0, '', 143, 58),
(93, 'Green', 0, '1', 0, '', 143, 58),
(94, 'Blue', 0, '1', 0, '', 143, 58),
(95, 'Purple', 0, '1', 0, '', 143, 58),
(96, 'Black', 0, '1', 0, '', 143, 58),
(97, 'Brown', 0, '1', 0, '', 143, 58),
(98, 'Wine', 0, '1', 0, '', 143, 58),
(99, 'Grey', 0, '1', 0, '', 143, 58),
(100, 'White', 0, '1', 0, '', 143, 58),
(101, 'Ivory', 0, '1', 0, '', 145, 59),
(102, 'Beige', 0, '1', 0, '', 145, 59),
(103, 'Pink', 0, '1', 0, '', 145, 59),
(104, 'Green', 0, '1', 0, '', 145, 59),
(105, 'Blue', 0, '1', 0, '', 145, 59),
(106, 'Purple', 0, '1', 0, '', 145, 59),
(107, 'Black', 0, '1', 0, '', 145, 59),
(108, 'Brown', 0, '1', 0, '', 145, 59),
(109, 'Wine', 0, '1', 0, '', 145, 59),
(110, 'Grey', 0, '1', 0, '', 145, 59),
(111, 'White', 0, '1', 0, '', 145, 59);

-- --------------------------------------------------------

--
-- Table structure for table `attr_op_op_op_op_tbl`
--

CREATE TABLE `attr_op_op_op_op_tbl` (
  `att_op_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_op_name` text NOT NULL,
  `att_op_op_op_op_type` int(11) NOT NULL,
  `att_op_op_op_op_price_type` int(11) NOT NULL,
  `att_op_op_op_op_price` float NOT NULL,
  `att_op_op_op_op_condition` text NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_id` int(11) NOT NULL,
  `op_op_id` int(11) NOT NULL,
  `op_op_op_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attr_op_op_op_op_tbl`
--

INSERT INTO `attr_op_op_op_op_tbl` (`att_op_op_op_op_id`, `att_op_op_op_op_name`, `att_op_op_op_op_type`, `att_op_op_op_op_price_type`, `att_op_op_op_op_price`, `att_op_op_op_op_condition`, `attribute_id`, `op_id`, `op_op_id`, `op_op_op_id`) VALUES
(1, '1 Panel-L', 0, 1, 0, '', 48, 115, 70, 54),
(2, '1 Panel-R', 0, 1, 0, '', 48, 115, 70, 54),
(3, '2 Panel-LL', 0, 1, 0, '', 48, 115, 70, 54),
(4, '2 Panel-LR', 0, 1, 0, '', 48, 115, 70, 54),
(5, '2 Panel-RR', 0, 1, 0, '', 48, 115, 70, 54),
(6, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 70, 54),
(7, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 70, 54),
(8, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 70, 54),
(9, '1 Panel-L', 0, 1, 0, '', 48, 115, 70, 56),
(10, '1 Panel-R', 0, 1, 0, '', 48, 115, 70, 56),
(11, '2 Panel-LL', 0, 1, 0, '', 48, 115, 70, 56),
(12, '2 Panel-LR', 0, 1, 0, '', 48, 115, 70, 56),
(13, '2 Panel-RR', 0, 1, 0, '', 48, 115, 70, 56),
(14, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 70, 56),
(15, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 70, 56),
(16, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 70, 56),
(17, '1 Panel-L', 0, 1, 0, '', 48, 115, 70, 58),
(18, '1 Panel-R', 0, 1, 0, '', 48, 115, 70, 58),
(19, '2 Panel-LL', 0, 1, 0, '', 48, 115, 70, 58),
(20, '2 Panel-LR', 0, 1, 0, '', 48, 115, 70, 58),
(21, '2 Panel-RR', 0, 1, 0, '', 48, 115, 70, 58),
(22, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 70, 58),
(23, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 70, 58),
(24, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 70, 58),
(25, '1 Panel-L', 0, 1, 0, '', 48, 115, 71, 60),
(26, '1 Panel-R', 0, 1, 0, '', 48, 115, 71, 60),
(27, '2 Panel-LL', 0, 1, 0, '', 48, 115, 71, 60),
(28, '2 Panel-LR', 0, 1, 0, '', 48, 115, 71, 60),
(29, '2 Panel-RR', 0, 1, 0, '', 48, 115, 71, 60),
(30, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 71, 60),
(31, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 71, 60),
(32, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 71, 60),
(33, '1 Panel-L', 0, 1, 0, '', 48, 115, 71, 62),
(34, '1 Panel-R', 0, 1, 0, '', 48, 115, 71, 62),
(35, '2 Panel-LL', 0, 1, 0, '', 48, 115, 71, 62),
(36, '2 Panel-LR', 0, 1, 0, '', 48, 115, 71, 62),
(37, '2 Panel-RR', 0, 1, 0, '', 48, 115, 71, 62),
(38, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 71, 62),
(39, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 71, 62),
(40, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 71, 62),
(41, '1 Panel-L', 0, 1, 0, '', 48, 115, 71, 64),
(42, '1 Panel-R', 0, 1, 0, '', 48, 115, 71, 64),
(43, '2 Panel-LL', 0, 1, 0, '', 48, 115, 71, 64),
(44, '2 Panel-LR', 0, 1, 0, '', 48, 115, 71, 64),
(45, '2 Panel-RR', 0, 1, 0, '', 48, 115, 71, 64),
(46, '3 Panel-LLR', 0, 1, 0, '', 48, 115, 71, 64),
(47, '3 Panel-LRR', 0, 1, 0, '', 48, 115, 71, 64),
(48, '4 Panel-LLRR', 0, 1, 0, '', 48, 115, 71, 64);

-- --------------------------------------------------------

--
-- Table structure for table `b_acc_coa`
--

CREATE TABLE `b_acc_coa` (
  `row_id` int(11) NOT NULL,
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_acc_coa`
--

INSERT INTO `b_acc_coa` (`row_id`, `HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
(209, '1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(210, '101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(211, '10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(212, '10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(213, '1010202', 'Computers & Printers etc', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(214, '10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(215, '1010401', 'Phones', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(216, '1010404', 'Internal office Design', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(217, '1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(218, '1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(219, '10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(220, '102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(221, '10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(222, '1020101', 'Petty Cash', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(223, '1020102', 'Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(224, '102010201', 'Bank Of America', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(225, '1020103', 'Credit Card', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(226, '1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(227, '1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(228, '102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(229, '10202', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(230, '2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(231, '201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(232, '20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(233, '202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(234, '3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(235, '301', 'Business Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(236, '302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(237, '30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(238, '30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(239, '4', 'Expense', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(240, '402', 'Other Expenses', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(241, '40201', 'Travel & Food', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(242, '40202', 'Telephone Bill', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(243, '40203', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(244, '40204', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(245, '40205', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(246, '4020501', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(247, '4020502', 'Miscellaneous Benefits', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(248, '40206', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(249, '40207', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(250, '5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(251, '501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(252, '50101', 'Long Term Loans', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(253, '502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(254, '50201', 'Short Term Loans', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(255, '50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(256, '5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(257, '50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(258, '5020301', 'Taxes', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 29, '1', '2019-09-16 00:00:00', '1', '2019-09-16 00:00:00'),
(359, '1020301-1', 'CUS-0002-James Park', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 2, '2', '2019-09-16 00:00:00', '', '0000-00-00 00:00:00'),
(464, '1', 'Assets-1568889667', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(465, '101', 'Non Current Assets-1568889667', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(466, '10101', 'Furniture & Fixturers-1568889667', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(467, '10102', 'Office Equipment-1568889667', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(468, '1010202', 'Computers & Printers etc-1568889667', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(469, '10104', 'Others Assets-1568889667', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(470, '1010401', 'Phones-1568889667', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(471, '1010404', 'Internal office Design-1568889667', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(472, '1010406', 'Security Equipment-1568889667', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(473, '1010407', 'Books and Journal-1568889667', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(474, '10107', 'Inventory-1568889668', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(475, '102', 'Current Asset-1568889668', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(476, '10201', 'Cash & Cash Equivalent-1568889668', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(477, '1020101', 'Petty Cash-1568889668', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(478, '1020102', 'Bank-1568889668', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(479, '102010201', 'Bank Of America-1568889668', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(480, '1020103', 'Credit Card-1568889668', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(481, '1020104', 'Paypal-1568889668', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(482, '1020201', 'Advance-1568889668', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(483, '102020105', 'Salary-1568889668', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(484, '10202', 'Account Receivable-1568889668', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(485, '2', 'Equity-1568889668', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(486, '201', 'Share Holders Equity-1568889668', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(487, '20101', 'Share Capital-1568889668', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(488, '202', 'Reserve & Surplus-1568889668', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(489, '3', 'Income-1568889668', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(490, '301', 'Business Income-1568889668', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(491, '302', 'Other Income-1568889668', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(492, '30202', 'Miscellaneous (Income)-1568889668', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(493, '30203', 'Bank Interest-1568889668', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(494, '4', 'Expense-1568889668', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(495, '402', 'Other Expenses-1568889668', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(496, '40201', 'Travel & Food-1568889668', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(497, '40202', 'Telephone Bill-1568889668', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(498, '40203', 'Miscellaneous Expenses-1568889668', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(499, '40204', 'Software Development Expenses-1568889668', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(500, '40205', 'Salary & Allowances-1568889668', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(501, '4020501', 'Special Allowances-1568889668', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(502, '4020502', 'Miscellaneous Benefits-1568889668', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(503, '40206', 'Financial Expenses-1568889668', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(504, '40207', 'Repair and Maintenance-1568889668', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(505, '5', 'Liabilities-1568889668', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(506, '501', 'Non Current Liabilities-1568889668', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(507, '50101', 'Long Term Loans-1568889668', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(508, '502', 'Current Liabilities-1568889668', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(509, '50201', 'Short Term Loans-1568889668', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(510, '50202', 'Account Payable-1568889668', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(511, '5020201', 'Supplier-1568889668', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(512, '50203', 'Liabilities for Expenses-1568889668', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(513, '5020301', 'Taxes-1568889668', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 8, '8', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(514, '1', 'Assets-1568890653', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(515, '101', 'Non Current Assets-1568890653', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(516, '10101', 'Furniture & Fixturers-1568890653', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(517, '10102', 'Office Equipment-1568890653', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(518, '1010202', 'Computers & Printers etc-1568890653', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(519, '10104', 'Others Assets-1568890653', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(520, '1010401', 'Phones-1568890653', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(521, '1010404', 'Internal office Design-1568890653', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(522, '1010406', 'Security Equipment-1568890653', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(523, '1010407', 'Books and Journal-1568890653', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(524, '10107', 'Inventory-1568890653', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(525, '102', 'Current Asset-1568890653', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(526, '10201', 'Cash & Cash Equivalent-1568890653', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(527, '1020101', 'Petty Cash-1568890653', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(528, '1020102', 'Bank-1568890653', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(529, '102010201', 'Bank Of America-1568890653', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(530, '1020103', 'Credit Card-1568890653', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(531, '1020104', 'Paypal-1568890653', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(532, '1020201', 'Advance-1568890653', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(533, '102020105', 'Salary-1568890653', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(534, '10202', 'Account Receivable-1568890653', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(535, '2', 'Equity-1568890653', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(536, '201', 'Share Holders Equity-1568890653', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(537, '20101', 'Share Capital-1568890653', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(538, '202', 'Reserve & Surplus-1568890653', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(539, '3', 'Income-1568890653', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(540, '301', 'Business Income-1568890653', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(541, '302', 'Other Income-1568890653', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(542, '30202', 'Miscellaneous (Income)-1568890654', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(543, '30203', 'Bank Interest-1568890654', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(544, '4', 'Expense-1568890654', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(545, '402', 'Other Expenses-1568890654', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(546, '40201', 'Travel & Food-1568890654', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(547, '40202', 'Telephone Bill-1568890654', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(548, '40203', 'Miscellaneous Expenses-1568890654', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(549, '40204', 'Software Development Expenses-1568890654', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(550, '40205', 'Salary & Allowances-1568890654', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(551, '4020501', 'Special Allowances-1568890654', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(552, '4020502', 'Miscellaneous Benefits-1568890654', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(553, '40206', 'Financial Expenses-1568890654', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(554, '40207', 'Repair and Maintenance-1568890654', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(555, '5', 'Liabilities-1568890654', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(556, '501', 'Non Current Liabilities-1568890654', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(557, '50101', 'Long Term Loans-1568890654', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(558, '502', 'Current Liabilities-1568890654', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(559, '50201', 'Short Term Loans-1568890654', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(560, '50202', 'Account Payable-1568890654', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(561, '5020201', 'Supplier-1568890654', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(562, '50203', 'Liabilities for Expenses-1568890654', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(563, '5020301', 'Taxes-1568890654', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 9, '9', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(564, '1', 'Assets-1568891857', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(565, '101', 'Non Current Assets-1568891857', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(566, '10101', 'Furniture & Fixturers-1568891857', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(567, '10102', 'Office Equipment-1568891857', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(568, '1010202', 'Computers & Printers etc-1568891857', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(569, '10104', 'Others Assets-1568891857', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(570, '1010401', 'Phones-1568891857', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(571, '1010404', 'Internal office Design-1568891857', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(572, '1010406', 'Security Equipment-1568891857', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(573, '1010407', 'Books and Journal-1568891857', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(574, '10107', 'Inventory-1568891857', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(575, '102', 'Current Asset-1568891857', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(576, '10201', 'Cash & Cash Equivalent-1568891857', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(577, '1020101', 'Petty Cash-1568891857', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(578, '1020102', 'Bank-1568891857', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(579, '102010201', 'Bank Of America-1568891857', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(580, '1020103', 'Credit Card-1568891857', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(581, '1020104', 'Paypal-1568891857', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(582, '1020201', 'Advance-1568891857', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(583, '102020105', 'Salary-1568891857', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(584, '10202', 'Account Receivable-1568891857', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(585, '2', 'Equity-1568891857', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(586, '201', 'Share Holders Equity-1568891857', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(587, '20101', 'Share Capital-1568891857', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(588, '202', 'Reserve & Surplus-1568891857', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(589, '3', 'Income-1568891857', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(590, '301', 'Business Income-1568891857', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(591, '302', 'Other Income-1568891857', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(592, '30202', 'Miscellaneous (Income)-1568891857', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(593, '30203', 'Bank Interest-1568891857', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(594, '4', 'Expense-1568891857', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(595, '402', 'Other Expenses-1568891857', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(596, '40201', 'Travel & Food-1568891857', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(597, '40202', 'Telephone Bill-1568891857', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(598, '40203', 'Miscellaneous Expenses-1568891857', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(599, '40204', 'Software Development Expenses-1568891857', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(600, '40205', 'Salary & Allowances-1568891857', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(601, '4020501', 'Special Allowances-1568891857', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(602, '4020502', 'Miscellaneous Benefits-1568891857', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(603, '40206', 'Financial Expenses-1568891857', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(604, '40207', 'Repair and Maintenance-1568891857', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(605, '5', 'Liabilities-1568891857', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(606, '501', 'Non Current Liabilities-1568891857', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(607, '50101', 'Long Term Loans-1568891857', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(608, '502', 'Current Liabilities-1568891857', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(609, '50201', 'Short Term Loans-1568891857', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(610, '50202', 'Account Payable-1568891857', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(611, '5020201', 'Supplier-1568891857', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(612, '50203', 'Liabilities for Expenses-1568891857', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(613, '5020301', 'Taxes-1568891857', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 10, '10', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(614, '1', 'Assets-1568891906', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(615, '101', 'Non Current Assets-1568891906', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(616, '10101', 'Furniture & Fixturers-1568891906', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(617, '10102', 'Office Equipment-1568891906', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(618, '1010202', 'Computers & Printers etc-1568891906', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(619, '10104', 'Others Assets-1568891906', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(620, '1010401', 'Phones-1568891906', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(621, '1010404', 'Internal office Design-1568891906', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(622, '1010406', 'Security Equipment-1568891906', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(623, '1010407', 'Books and Journal-1568891906', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(624, '10107', 'Inventory-1568891906', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(625, '102', 'Current Asset-1568891906', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(626, '10201', 'Cash & Cash Equivalent-1568891906', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(627, '1020101', 'Petty Cash-1568891906', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(628, '1020102', 'Bank-1568891906', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(629, '102010201', 'Bank Of America-1568891906', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(630, '1020103', 'Credit Card-1568891906', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(631, '1020104', 'Paypal-1568891906', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(632, '1020201', 'Advance-1568891906', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(633, '102020105', 'Salary-1568891906', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(634, '10202', 'Account Receivable-1568891906', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(635, '2', 'Equity-1568891906', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(636, '201', 'Share Holders Equity-1568891906', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(637, '20101', 'Share Capital-1568891906', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(638, '202', 'Reserve & Surplus-1568891906', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(639, '3', 'Income-1568891906', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(640, '301', 'Business Income-1568891906', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(641, '302', 'Other Income-1568891906', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(642, '30202', 'Miscellaneous (Income)-1568891906', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(643, '30203', 'Bank Interest-1568891906', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(644, '4', 'Expense-1568891906', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(645, '402', 'Other Expenses-1568891906', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(646, '40201', 'Travel & Food-1568891906', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(647, '40202', 'Telephone Bill-1568891906', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(648, '40203', 'Miscellaneous Expenses-1568891906', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(649, '40204', 'Software Development Expenses-1568891906', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(650, '40205', 'Salary & Allowances-1568891906', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(651, '4020501', 'Special Allowances-1568891906', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(652, '4020502', 'Miscellaneous Benefits-1568891906', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(653, '40206', 'Financial Expenses-1568891906', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(654, '40207', 'Repair and Maintenance-1568891906', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(655, '5', 'Liabilities-1568891906', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(656, '501', 'Non Current Liabilities-1568891906', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(657, '50101', 'Long Term Loans-1568891906', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(658, '502', 'Current Liabilities-1568891906', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(659, '50201', 'Short Term Loans-1568891906', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(660, '50202', 'Account Payable-1568891906', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(661, '5020201', 'Supplier-1568891906', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(662, '50203', 'Liabilities for Expenses-1568891906', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(663, '5020301', 'Taxes-1568891906', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 11, '11', '2019-09-19 00:00:00', '1', '2019-09-19 00:00:00'),
(664, '1020301-2', 'CUS-0003-cust test', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, '0', '2019-09-20 00:00:00', '', '0000-00-00 00:00:00'),
(666, '502020101', '-avinashs', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 2, '2', '2019-09-21 00:00:00', '12', '2019-09-21 00:00:00'),
(667, '502020102', 'SUP-0003-pranav', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 2, '2', '2019-09-21 00:00:00', '', '0000-00-00 00:00:00'),
(668, '502020103', '-alit', 'Supplier', 4, 1, 1, 0, 'L', 0, 0, '0.00', 12, '12', '2019-09-21 00:00:00', '12', '2019-09-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `b_acc_transaction`
--

CREATE TABLE `b_acc_transaction` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `StoreID` int(11) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_cost_factor_tbl`
--

CREATE TABLE `b_cost_factor_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `dealer_cost_factor` float NOT NULL,
  `individual_cost_factor` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_level_quatation_attributes`
--

CREATE TABLE `b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_level_quatation_tbl`
--

CREATE TABLE `b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `manufacture_order_date` datetime DEFAULT NULL,
  `ship_order_date` datetime DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_level_quatation_tbl`
--

INSERT INTO `b_level_quatation_tbl` (`id`, `order_id`, `clevel_order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `manufacture_order_date`, `ship_order_date`, `level_id`, `order_date`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(1, 'BMSW-Jame-1000-001', NULL, 1, 'James-1000', '', 'assets/barcode/b/BMSW-Jame-1000-001.jpg', '0', '', NULL, 0, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, '', 4, NULL, NULL, 2, '2019-09-20', 2, 0, '2019-09-20', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_level_qutation_details`
--

CREATE TABLE `b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `manufactured_scan_date` datetime DEFAULT NULL,
  `manufactured_scan_by` int(11) DEFAULT NULL,
  `shipping_scan_date` datetime DEFAULT NULL,
  `shipping_scan_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_menusetup_tbl`
--

CREATE TABLE `b_menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(255) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(2) NOT NULL COMMENT '1 = left, 2 = system, 3 = top',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_menusetup_tbl`
--

INSERT INTO `b_menusetup_tbl` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(2, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(3, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(4, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(5, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(6, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(7, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(8, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(9, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(10, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(11, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(12, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(13, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(14, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(15, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(16, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(17, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(18, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(19, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(20, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(21, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(22, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(23, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(24, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(25, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(26, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(27, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(28, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(29, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(30, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(31, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(32, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(33, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(34, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(35, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(36, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(37, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(38, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(39, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(40, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(41, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(42, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(43, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(44, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(45, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(46, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(47, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(48, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(49, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(50, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(51, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(52, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(53, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(54, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(55, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(56, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(57, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(58, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(59, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(60, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(61, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(62, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(63, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(64, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(65, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(66, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(67, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(68, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(69, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(70, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(71, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(72, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(73, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(74, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(75, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(76, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(77, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(78, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(79, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(80, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(81, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(82, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(83, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(84, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(85, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(86, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(87, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(88, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(89, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(90, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(91, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(92, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(93, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(94, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(95, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(96, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(97, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(98, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(99, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(100, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(101, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(102, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(103, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(104, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(105, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(106, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(107, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(108, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(109, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(110, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 2, 2, '2019-09-17 00:00:00'),
(111, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(112, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(113, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(114, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(115, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(116, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(117, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(118, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(119, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(120, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(121, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(122, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(123, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(124, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(125, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(126, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(127, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(128, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 2, 2, '2019-09-17 00:00:00'),
(129, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(130, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 2, 2, '2019-09-17 00:00:00'),
(131, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(132, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(133, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(134, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(135, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(136, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(137, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(138, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(139, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(140, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(141, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(142, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(143, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(144, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(145, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(146, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(147, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(148, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(149, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(150, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(151, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(152, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(153, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(154, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(155, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(156, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(157, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(158, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(159, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(160, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(161, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(162, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(163, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(164, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(165, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(166, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(167, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(168, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(169, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(170, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(171, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(172, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(173, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(174, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(175, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(176, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(177, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(178, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(179, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(180, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(181, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(182, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(183, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(184, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(185, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(186, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(187, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(188, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(189, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(190, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(191, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(192, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(193, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(194, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(195, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(196, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(197, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(198, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(199, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(200, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(201, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(202, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(203, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(204, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(205, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(206, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(207, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(208, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(209, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(210, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(211, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(212, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(213, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(214, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(215, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(216, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(217, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(218, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(219, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(220, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(221, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(222, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(223, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(224, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(225, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(226, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(227, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(228, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(229, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(230, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(231, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(232, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(233, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(234, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(235, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(236, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(237, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(238, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(239, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(240, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 4, 4, '2019-09-17 00:00:00'),
(241, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(242, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(243, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(244, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(245, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(246, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(247, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(248, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(249, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(250, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(251, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(252, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(253, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(254, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(255, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(256, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(257, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(258, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 4, 4, '2019-09-17 00:00:00'),
(259, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(260, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 4, 4, '2019-09-17 00:00:00'),
(261, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(262, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(263, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(264, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(265, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(266, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(267, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(268, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(269, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(270, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(271, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(272, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(273, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(274, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(275, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(276, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(277, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(278, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(279, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(280, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(281, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(282, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(283, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(284, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(285, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(286, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(287, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(288, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(289, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(290, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(291, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(292, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(293, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(294, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(295, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(296, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(297, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(298, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(299, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(300, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(301, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(302, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(303, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(304, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(305, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(306, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(307, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(308, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(309, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(310, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(311, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(312, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(313, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(314, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(315, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(316, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(317, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(318, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(319, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(320, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(321, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(322, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(323, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(324, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(325, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(326, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(327, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(328, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(329, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(330, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(331, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(332, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(333, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(334, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(335, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(336, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(337, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(338, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(339, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(340, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(341, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(342, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(343, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(344, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(345, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(346, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(347, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(348, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(349, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(350, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(351, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(352, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(353, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(354, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(355, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(356, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(357, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(358, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(359, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(360, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(361, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(362, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(363, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(364, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(365, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(366, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(367, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(368, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(369, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(370, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 8, 8, '2019-09-18 18:30:00'),
(371, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(372, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(373, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(374, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(375, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(376, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(377, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(378, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(379, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(380, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(381, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(382, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(383, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(384, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(385, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(386, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(387, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(388, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 8, 8, '2019-09-18 18:30:00'),
(389, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(390, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 8, 8, '2019-09-18 18:30:00'),
(391, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(392, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(393, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(394, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(395, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(396, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(397, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(398, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(399, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(400, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(401, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(402, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(403, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(404, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(405, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(406, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(407, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(408, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(409, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(410, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(411, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(412, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(413, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(414, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(415, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(416, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(417, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(418, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(419, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(420, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(421, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(422, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(423, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(424, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00');
INSERT INTO `b_menusetup_tbl` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(425, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(426, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(427, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(428, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(429, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(430, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(431, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(432, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(433, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(434, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(435, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(436, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(437, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(438, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(439, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(440, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(441, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(442, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(443, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(444, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(445, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(446, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(447, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(448, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(449, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(450, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(451, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(452, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(453, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(454, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(455, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(456, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(457, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(458, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(459, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(460, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(461, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(462, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(463, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(464, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(465, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(466, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(467, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(468, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(469, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(470, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(471, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(472, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(473, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(474, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(475, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(476, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(477, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(478, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(479, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(480, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(481, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(482, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(483, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(484, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(485, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(486, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(487, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(488, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(489, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(490, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(491, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(492, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(493, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(494, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(495, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(496, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(497, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(498, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(499, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(500, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 9, 9, '2019-09-18 18:30:00'),
(501, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(502, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(503, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(504, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(505, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(506, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(507, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(508, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(509, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(510, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(511, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(512, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(513, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(514, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(515, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(516, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(517, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(518, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 9, 9, '2019-09-18 18:30:00'),
(519, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(520, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 9, 9, '2019-09-18 18:30:00'),
(521, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(522, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(523, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(524, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(525, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(526, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(527, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(528, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(529, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(530, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(531, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(532, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(533, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(534, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(535, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(536, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(537, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(538, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(539, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(540, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(541, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(542, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(543, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(544, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(545, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(546, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(547, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(548, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(549, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(550, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(551, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(552, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(553, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(554, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(555, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(556, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(557, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(558, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(559, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(560, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(561, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(562, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(563, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(564, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(565, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(566, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(567, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(568, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(569, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(570, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(571, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(572, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(573, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(574, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(575, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(576, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(577, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(578, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(579, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(580, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(581, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(582, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(583, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(584, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(585, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(586, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(587, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(588, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(589, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(590, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(591, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(592, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(593, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(594, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(595, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(596, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(597, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(598, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(599, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(600, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(601, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(602, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(603, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(604, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(605, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(606, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(607, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(608, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(609, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(610, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(611, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(612, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(613, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(614, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(615, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(616, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(617, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(618, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(619, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(620, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(621, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(622, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(623, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(624, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(625, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(626, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(627, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(628, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(629, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(630, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 10, 10, '2019-09-18 18:30:00'),
(631, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(632, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(633, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(634, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(635, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(636, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(637, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(638, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(639, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(640, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(641, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(642, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(643, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(644, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(645, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(646, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(647, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(648, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 10, 10, '2019-09-18 18:30:00'),
(649, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(650, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 10, 10, '2019-09-18 18:30:00'),
(651, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(652, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(653, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(654, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(655, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(656, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(657, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(658, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(659, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(660, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(661, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(662, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(663, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(664, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(665, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(666, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(667, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(668, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(669, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(670, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(671, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(672, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(673, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(674, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(675, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(676, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(677, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(678, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(679, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(680, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(681, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(682, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(683, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(684, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(685, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(686, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(687, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(688, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(689, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(690, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(691, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(692, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(693, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(694, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(695, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(696, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(697, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(698, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(699, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(700, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(701, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(702, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(703, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(704, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(705, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(706, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(707, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(708, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(709, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(710, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(711, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(712, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(713, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(714, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(715, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(716, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(717, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(718, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(719, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(720, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(721, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(722, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(723, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(724, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(725, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(726, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(727, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(728, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(729, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(730, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(731, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(732, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(733, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(734, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(735, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(736, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(737, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(738, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(739, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(740, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(741, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(742, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(743, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(744, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(745, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(746, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(747, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(748, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(749, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(750, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(751, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(752, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(753, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(754, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(755, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(756, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(757, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(758, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(759, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(760, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, 11, 11, '2019-09-18 18:30:00'),
(761, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(762, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(763, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(764, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(765, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(766, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(767, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(768, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(769, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(770, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(771, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(772, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(773, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(774, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(775, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(776, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(777, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(778, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, 11, 11, '2019-09-18 18:30:00'),
(779, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00'),
(780, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, 11, 11, '2019-09-18 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `b_menusetup_tbl_default`
--

CREATE TABLE `b_menusetup_tbl_default` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(255) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(2) NOT NULL COMMENT '1 = left, 2 = system, 3 = top',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `b_menusetup_tbl_default`
--

INSERT INTO `b_menusetup_tbl_default` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 1, 'customer', 'Customer', '', 'customer', 2, 0, 1, 0, '', 1, NULL, 1, '2019-01-03 06:27:31'),
(2, 2, 'add', 'Add', 'add-b-customer', 'customer', 0, 1, 1, 0, '', 1, NULL, 1, '2019-01-03 06:28:27'),
(3, 3, 'manage_customer', 'Manage customer', 'b-customer-list', 'customer', 0, 1, 1, 0, '', 1, NULL, 1, '2019-01-03 06:28:54'),
(5, 5, 'order', 'Order', '', 'order', 3, 0, 1, 0, '', 1, NULL, 1, '2019-01-03 06:35:04'),
(6, 6, 'new', 'New', 'new-order', 'order', 0, 5, 1, 0, '', 1, NULL, 1, '2019-01-03 06:35:35'),
(7, 7, 'manage_order', 'Manage order', '', 'order', 0, 5, 1, 0, '', 1, NULL, 1, '2019-01-03 06:36:25'),
(8, 8, 'kanban_view', 'Kanban view', 'order-kanban', 'order', 0, 7, 1, 0, '', 1, NULL, 1, '2019-01-03 06:37:13'),
(9, 9, 'list_view', 'List view', 'b-order-list', 'order', 0, 7, 1, 0, '', 1, NULL, 1, '2019-01-03 06:37:50'),
(10, 10, 'catalog', 'Catalog', '', 'catalog', 4, 0, 1, 0, '', 1, NULL, 1, '2019-01-03 06:38:44'),
(11, 11, 'category', 'Category', '', 'catalog', 1, 10, 1, 0, '', 1, NULL, 1, '2019-01-03 06:40:09'),
(13, 13, 'add_or_manage', 'Add or manage', 'manage-category', 'catalog', 0, 11, 1, 0, '', 1, NULL, 1, '2019-01-03 06:41:34'),
(14, 14, 'assigned', 'Assigned', 'category-assign', 'catalog', 0, 11, 1, 0, '', 1, NULL, 1, '2019-01-03 06:42:45'),
(15, 15, 'products', 'Products', '', 'catalog', 6, 10, 1, 0, '', 1, NULL, 1, '2019-01-03 06:43:34'),
(16, 16, 'add', 'Add', 'add-product', 'catalog', 0, 15, 1, 0, '', 1, NULL, 1, '2019-01-03 06:44:03'),
(17, 17, 'manage_product', 'Manage product', 'product-manage', 'catalog', 0, 15, 1, 0, '', 1, NULL, 1, '2019-01-03 06:46:24'),
(20, 20, 'pattern', 'Pattern', 'manage-pattern', 'catalog', 2, 10, 1, 0, '', 1, NULL, 1, '2019-01-03 06:49:38'),
(21, 21, 'attributes', 'Attributes', '', 'catalog', 5, 10, 1, 0, '', 1, NULL, 1, '2019-01-03 06:52:13'),
(22, 22, 'add_attribute', 'Add attribute', 'add-attribute', 'catalog', 0, 21, 1, 0, '', 1, NULL, 1, '2019-01-03 06:53:15'),
(23, 23, 'manage_attribute', 'Manage attribute', 'manage-attribute', 'catalog', 0, 21, 1, 0, '', 1, NULL, 1, '2019-01-03 06:53:51'),
(24, 24, 'condition', 'Condition', 'add-condition', 'catalog', 7, 10, 1, 0, '', 0, NULL, 1, '2019-01-03 06:54:44'),
(28, 28, 'price_model', 'Price model', '', 'catalog', 4, 10, 1, 0, '', 1, NULL, 1, '2019-01-03 06:57:45'),
(29, 29, 'row_column_price', 'Row column price', '', 'catalog', 0, 28, 1, 0, '', 1, NULL, 1, '2019-01-03 06:58:40'),
(30, 30, 'add', 'Add', 'add-price', 'catalog', 0, 29, 1, 0, '', 1, NULL, 1, '2019-01-03 06:59:43'),
(31, 31, 'manage', 'Manage', 'manage-price', 'catalog', 0, 29, 1, 0, '', 1, NULL, 1, '2019-01-03 07:00:30'),
(35, 35, 'Production/MFG', 'Production/MFG', '', 'production', 7, 0, 1, 0, '', 1, NULL, 1, '2019-01-04 23:19:38'),
(36, 36, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 1, 35, 1, 0, '', 1, NULL, 1, '2019-01-04 23:21:43'),
(37, 37, 'manufacturing', 'Manufacturing', 'add-manufacturer', 'production', 0, 36, 1, 0, '', 0, NULL, 1, '2019-01-04 23:22:34'),
(38, 38, 'manage_manufacturer', 'Manage manufacturer', 'manage-manufacturer', 'production', 0, 36, 1, 0, '', 0, NULL, 1, '2019-01-04 23:44:50'),
(39, 39, 'stock', 'Stock', '', 'production', 3, 35, 1, 0, '', 1, NULL, 1, '2019-01-04 23:45:44'),
(40, 40, 'stock_availability', 'Stock availability', 'stock-availability', 'production', 0, 39, 1, 0, '', 1, NULL, 1, '2019-01-04 23:46:47'),
(41, 41, 'stock_history', 'Stock history', 'stock-history', 'production', 0, 39, 1, 0, '', 1, NULL, 1, '2019-01-04 23:48:56'),
(42, 42, 'suppliers', 'Suppliers', 'supplier-list', 'production', 5, 35, 1, 0, '', 1, NULL, 1, '2019-01-04 23:49:33'),
(46, 46, 'return', 'Return', '', 'production', 4, 35, 1, 0, '', 1, NULL, 1, '2019-01-04 23:52:57'),
(47, 47, 'customer_return', 'Customer return', 'b-customer-return', 'production', 0, 46, 1, 0, '', 1, NULL, 1, '2019-01-04 23:53:32'),
(48, 48, 'supplier_return', 'Supplier return', '', 'production', 0, 46, 1, 0, '', 1, NULL, 1, '2019-01-04 23:54:09'),
(50, 50, 'account', 'Account', '', 'account', 9, 0, 1, 0, '', 1, NULL, 1, '2019-01-05 00:14:12'),
(51, 51, 'account_chart', 'Account chart', 'b-account-chart', 'account', 0, 50, 1, 0, '', 1, NULL, 1, '2019-01-05 00:17:13'),
(52, 52, 'purchase', 'Purchase', '', 'account', 0, 50, 1, 0, '', 1, NULL, 1, '2019-01-05 00:19:17'),
(53, 53, 'purchase_list', 'Purchase list', '', 'account', 0, 50, 1, 0, '', 1, NULL, 1, '2019-01-05 00:25:38'),
(54, 54, 'voucher', 'Voucher', '', 'account', 0, 50, 1, 0, '', 1, NULL, 1, '2019-01-05 00:26:55'),
(55, 55, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:32:46'),
(56, 56, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:36:32'),
(57, 57, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:40:23'),
(58, 58, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:42:25'),
(59, 59, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:50:28'),
(60, 60, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'account', 0, 54, 1, 0, '', 1, NULL, 1, '2019-01-05 00:51:59'),
(61, 61, 'account_reports', 'Account reports', '', 'account', 0, 50, 1, 0, '', 1, NULL, 1, '2019-01-05 00:52:31'),
(62, 62, 'bank_book', 'Bank book', 'b-bank-book', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 00:54:15'),
(63, 63, 'cash_book', 'Cash book', 'b-cash-book', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 00:57:28'),
(64, 64, 'cash_flow', 'Cash flow', 'b-cash-flow', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 00:58:03'),
(65, 65, 'general_ledger', 'General ledger', 'b-general-ledger', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 00:58:50'),
(66, 66, 'profit_loss', 'Profit loss', 'b-profit-loss', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 01:00:04'),
(67, 67, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'account', 0, 61, 1, 0, '', 1, NULL, 1, '2019-01-05 01:00:35'),
(68, 68, 'setting', 'Setting', '', 'setting', 9, 0, 1, 0, '', 1, NULL, 1, '2019-01-05 01:06:53'),
(69, 69, 'company_profile', 'Company profile', 'profile-setting', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-01-05 01:20:57'),
(70, 70, 'integration', 'Integration', '', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-01-05 01:21:54'),
(71, 71, 'shipping', 'Shipping', 'shipping', 'setting', 0, 70, 1, 0, '', 1, NULL, 1, '2019-01-05 01:22:47'),
(72, 72, 'email', 'Email', 'mail', 'setting', 0, 70, 1, 0, '', 1, NULL, 1, '2019-01-05 01:23:45'),
(73, 73, 'custom_sms', 'Custom sms', 'sms', 'setting', 1, 68, 1, 0, '', 1, NULL, 1, '2019-01-05 01:24:27'),
(74, 74, 'paypal_settings', 'Paypal settings', 'gateway', 'setting', 0, 70, 1, 0, '', 1, NULL, 1, '2019-01-05 01:24:53'),
(75, 75, 'employees_and_permissions', 'Employees and permissions', '', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-01-05 01:25:44'),
(76, 76, 'unit_of_measurement', 'Unit of measurement', 'uom-list', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-01-15 03:26:56'),
(78, 78, 'color', 'Color', 'color-list-filter', 'catalog', 3, 10, 1, 0, '', 1, NULL, 1, '2019-01-19 06:14:02'),
(79, 79, 'raw_materials', 'Raw materials', 'row-material-list', 'catalog', 8, 10, 1, 0, '', 1, NULL, 1, '2019-01-19 06:29:03'),
(83, 83, 'raw_material_purchase', 'Raw material purchase', 'purchase-entry', 'account', 0, 52, 1, 0, '', 1, NULL, 1, '2019-02-08 00:18:21'),
(84, 84, 'raw_material', 'Raw material', 'purchase-list', 'account', 0, 53, 1, 0, '', 1, NULL, 1, '2019-02-08 00:22:51'),
(85, 85, 'raw_material_supplier_return', 'Raw material supplier return', 'raw-material-supplier-return', 'production', 0, 48, 1, 0, '', 1, NULL, 1, '2019-02-09 03:26:38'),
(86, 86, 'product_return', 'Product return', 'product-return', 'production', 0, 48, 1, 0, '', 0, NULL, 1, '2019-02-09 03:27:58'),
(87, 87, 'state_tax', 'State tax', 'us-state', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-02-18 07:32:52'),
(88, 88, 'employees', 'Employees', 'b-level-users', 'setting', 1, 75, 1, 0, '', 1, NULL, 1, '2019-02-27 23:24:19'),
(89, 89, 'employee_role', 'Employee role', 'b-user-role-assign', 'setting', 2, 75, 1, 0, '', 1, NULL, 1, '2019-02-27 23:29:10'),
(90, 90, 'access_roles', 'Access roles', 'b-user-access-role-list', 'setting', 1, 75, 1, 0, '', 1, NULL, 1, '2019-02-27 23:29:52'),
(91, 91, 'role_permissions', 'Role permissions', 'b-role-permission', 'setting', 4, 75, 1, 0, '', 1, NULL, 1, '2019-02-27 23:30:45'),
(92, 92, 'role_list', 'Role list', 'b-role-list', 'setting', 3, 75, 1, 0, '', 1, NULL, 1, '2019-02-27 23:31:25'),
(93, 93, 'Menu_Customization', 'Menu Customization', 'b-menu-setup', 'setting', 0, 68, 1, 0, '', 1, NULL, 1, '2019-02-27 23:33:24'),
(94, 94, 'sms_configuration', 'Sms configuration', 'sms-configure', 'setting', 0, 70, 1, 0, '', 1, NULL, 1, '2019-02-28 00:09:56'),
(95, 95, 'iframe_code', 'Iframe code', 'b-iframe-code', 'setting', 0, 68, 1, 0, '', 0, NULL, 1, '2019-03-08 23:04:18'),
(96, 96, 'group_price_mapping', 'Group price mapping', 'b_level/product_controller/group_price_mapping', 'catalog', 3, 28, 1, 0, '', 1, NULL, 1, '2019-04-17 07:53:20'),
(104, 104, 'system_menu', 'System menu', '', 'system_menu', 0, 0, 2, 0, '', 1, NULL, 1, '2019-05-09 03:00:36'),
(105, 105, 'my_account', 'My account', 'b-my-account', 'system_menu', 1, 104, 2, 0, '', 1, NULL, 1, '2019-05-09 03:01:59'),
(106, 106, 'notifications', 'Notifications', 'b_level/notification/notifications', 'system_menu', 2, 104, 2, 0, '', 1, NULL, 1, '2019-05-09 03:04:08'),
(107, 107, 'top_menu', 'Top menu', '', 'top_menu', 0, 0, 3, 0, '', 1, NULL, 1, '2019-05-09 03:08:46'),
(108, 108, 'suppliers', 'Suppliers', 'supplier-list', 'top_menu', 1, 107, 3, 0, '', 1, NULL, 1, '2019-05-09 03:10:00'),
(109, 109, 'cost_factor', 'Cost factor', 'b-cost-factor', 'top_menu', 2, 107, 3, 0, '', 1, NULL, 1, '2019-05-09 03:10:54'),
(110, 110, 'accounting', 'Accounting', '', 'top_menu', 3, 107, 3, 0, '', 1, NULL, 1, '2019-05-09 03:11:32'),
(111, 111, 'settings', 'Settings', '', 'top_menu', 4, 107, 3, 0, '', 1, NULL, 1, '2019-05-09 03:12:08'),
(112, 112, 'account_chart', 'Account chart', 'b-account-chart', 'top_menu', 0, 110, 3, 0, '', 1, NULL, 1, '2019-05-09 03:13:03'),
(113, 113, 'purchase_entry', 'Purchase entry', 'purchase-entry', 'top_menu', 0, 110, 3, 0, '', 1, NULL, 1, '2019-05-10 21:56:42'),
(114, 114, 'purchase_list', 'Purchase list', 'purchase-list', 'top_menu', 0, 110, 3, 0, '', 1, NULL, 1, '2019-05-10 21:57:56'),
(115, 115, 'voucher', 'Voucher', '', 'top_menu', 0, 110, 3, 0, '', 1, NULL, 1, '2019-05-10 21:58:45'),
(116, 116, 'debit_voucher', 'Debit voucher', 'b-debit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 21:59:55'),
(117, 117, 'credit_voucher', 'Credit voucher', 'b-credit-voucher', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 22:00:54'),
(118, 118, 'journal_voucher', 'Journal voucher', 'b-journal-voucher', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 22:01:29'),
(119, 119, 'contra_voucher', 'Contra voucher', 'b-contra-voucher', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 22:02:10'),
(120, 120, 'voucher_approval', 'Voucher approval', 'b-voucher-approval', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 22:02:51'),
(121, 121, 'voucher_reports', 'Voucher reports', 'b-voucher-reports', 'top_menu', 0, 115, 3, 0, '', 1, NULL, 1, '2019-05-10 22:04:26'),
(122, 122, 'account_reports', 'Account reports', '', 'top_menu', 0, 110, 3, 0, '', 1, NULL, 1, '2019-05-10 22:05:17'),
(123, 123, 'bank_book', 'Bank book', 'b-bank-book', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:05:53'),
(124, 124, 'cash_book', 'Cash book', 'b-cash-book', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:06:36'),
(125, 125, 'cash_flow', 'Cash flow', 'b-cash-flow', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:07:24'),
(126, 126, 'general_ledger', 'General ledger', 'b-general-ledger', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:08:07'),
(127, 127, 'profit_loss', 'Profit loss', 'b-profit-loss', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:08:31'),
(128, 128, 'trial_balance', 'Trial balance', 'b-trial-ballance', 'top_menu', 0, 122, 3, 0, '', 1, NULL, 1, '2019-05-10 22:09:17'),
(129, 129, 'company_profile', 'Company profile', 'profile-setting', 'top_menu', 0, 111, 3, 0, '', 1, NULL, 1, '2019-05-10 22:10:13'),
(130, 130, 'paypal_settings', 'Paypal settings', 'gateway', 'top_menu', 0, 111, 3, 0, '', 1, NULL, 1, '2019-05-10 22:10:38'),
(131, 131, 'change_password', 'Change password', 'b-change-password', 'top_menu', 0, 111, 3, 0, '', 1, NULL, 1, '2019-05-10 22:11:14'),
(132, 132, 'group_price', 'Group price', '', 'catalog', 0, 28, 1, 0, '', 1, NULL, 1, '2019-05-14 22:19:02'),
(133, 133, 'add_group_price', 'Add group price', 'add-group-price', 'catalog', 0, 132, 1, 0, '', 1, NULL, 1, '2019-05-14 22:24:22'),
(134, 134, 'manage_group_price', 'Manage group price', 'manage-group-price', 'catalog', 0, 132, 1, 0, '', 1, NULL, 1, '2019-05-14 22:26:19'),
(135, 135, 'dashboard', 'Dashboard', 'b-level-dashboard', 'dashboard', 1, 0, 1, 0, '', 0, NULL, 1, '2019-05-18 05:08:14'),
(136, 136, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit	', 'header', 0, 111, 3, 0, '', 1, NULL, 1, '2019-05-19 06:29:07'),
(137, 137, 'access_logs', 'Access logs', 'b-access-logs', 'header', 4, 111, 3, 0, '', 1, NULL, 1, '2019-05-19 22:33:56'),
(138, 138, 'payment gateway', 'Payment gateway', 'b_level/setting_controller/tms_gateway_edit ', 'setting', 6, 70, 1, 0, '', 1, NULL, 1, '2019-05-21 01:06:51'),
(139, 139, 'raw_material_usage', 'Raw material usage', 'raw-material-usage', 'production', 2, 35, 1, 0, '', 1, NULL, 1, '2019-06-01 05:50:26'),
(140, 140, 'commission report	', 'Commission report	', 'b_commission_report', 'setting', 6, 75, 1, 0, '', 1, NULL, 1, '2019-07-25 08:20:15'),
(141, 141, 'gallery', 'Gallery', '#', 'Gallery', 6, 0, 1, 0, '', 1, NULL, 1, '2019-07-25 08:20:50'),
(142, 142, 'tag', 'Tag', 'gallery_tag', 'gallery', 1, 141, 1, 0, '', 1, NULL, 1, '2019-07-25 08:21:44'),
(143, 143, 'image', 'Image', '#', 'gallery', 2, 141, 1, 0, '', 1, NULL, 1, '2019-07-25 08:22:24'),
(144, 144, 'add', 'Add', 'add_b_gallery_image', 'gallery', 1, 143, 1, 0, '', 1, NULL, 1, '2019-07-25 08:22:48'),
(145, 145, 'manage', 'Manage', 'manage_b_gallery_image', 'gallery', 2, 143, 1, 0, '', 1, NULL, 1, '2019-07-25 08:23:09'),
(146, 146, 'custom email', 'Email', 'email', 'setting', 2, 68, 1, 0, '', 1, NULL, 1, '2019-07-29 03:16:27'),
(147, 147, 'shared catalog', 'shared catalog', '', 'shared_catalog', 5, 0, 1, 0, '', 1, NULL, 1, '2019-08-05 05:22:57'),
(148, 148, 'Catalog user', 'Catalog user', 'catalog-user', 'shared_catalog', 52, 147, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:23:34'),
(149, 149, 'Catalog request', 'Catalog request', 'catalog-request', 'shared_catalog', 53, 147, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:24:12'),
(150, 150, 'My Orders', 'My Orders', 'catalog-order-list', 'shared_catalog', 54, 147, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:24:12'),
(151, 151, 'manufacturing order list', 'manufacturing order list', 'manufacturing-catalog-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:24:26'),
(152, 152, 'Receive orders', 'Receive orders', 'catalog-b-user-order-list', 'shared_catalog', 55, 147, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:24:26'),
(153, 153, 'sqm_price_mapping', 'sqm_price_mapping', 'b_level/product_controller/sqm_price_mapping', 'catalog', 3, 28, 1, 0, NULL, 1, NULL, 1, '2019-08-05 05:24:26'),
(154, 154, 'scan product', '계기반', 'b-level-scan-product', 'scanproduct', 8, 0, 1, 0, '', 1, NULL, 1, '2019-08-06 23:43:38'),
(156, 156, 'state list', '', 'b-state', 'setting', 6, 68, 1, 0, '', 1, NULL, 1, '2019-09-06 11:23:51');

-- --------------------------------------------------------

--
-- Table structure for table `b_notification_tbl`
--

CREATE TABLE `b_notification_tbl` (
  `id` int(11) NOT NULL,
  `notification_text` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `go_to_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `b_user_id` int(11) NOT NULL,
  `notification_for` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CUSTOMER',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_notification_tbl`
--

INSERT INTO `b_notification_tbl` (`id`, `notification_text`, `go_to_url`, `created_by`, `b_user_id`, `notification_for`, `date`) VALUES
(20, 'You have a new request for catalog', 'catalog-request', 2, 4, 'B_USER', '2019-09-18'),
(21, 'Your \"Need Shades Products\" request has been approved', 'b-user-catalog-product/4', 4, 2, 'B_USER', '2019-09-18'),
(22, 'You have a new request for catalog', 'catalog-request', 2, 8, 'B_USER', '2019-09-19'),
(23, 'You have a new request for catalog', 'catalog-request', 2, 8, 'B_USER', '2019-09-19'),
(24, 'Your \"hi hulk\" request has been approved', 'b-user-catalog-product/8', 8, 2, 'B_USER', '2019-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `b_role_permission_tbl`
--

CREATE TABLE `b_role_permission_tbl` (
  `id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `can_access` tinyint(1) NOT NULL,
  `can_create` tinyint(1) NOT NULL,
  `can_edit` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_role_tbl`
--

CREATE TABLE `b_role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_quatation_attributes`
--

CREATE TABLE `b_to_b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_quatation_tbl`
--

CREATE TABLE `b_to_b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b_user_id` int(11) DEFAULT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_level_qutation_details`
--

CREATE TABLE `b_to_b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_to_b_shipment_data`
--

CREATE TABLE `b_to_b_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `b_user_name` varchar(250) DEFAULT NULL,
  `b_user_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_access_tbl`
--

CREATE TABLE `b_user_access_tbl` (
  `role_acc_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_catalog_products`
--

CREATE TABLE `b_user_catalog_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pattern_model_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_link` int(11) NOT NULL DEFAULT '0',
  `pattern_link` int(11) NOT NULL DEFAULT '0',
  `request_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL DEFAULT '0',
  `create_date` date NOT NULL,
  `approve_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:approve,0:not approve by b2 user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `b_user_catalog_products`
--

INSERT INTO `b_user_catalog_products` (`id`, `product_id`, `pattern_model_id`, `product_link`, `pattern_link`, `request_id`, `requested_by`, `create_date`, `approve_status`) VALUES
(94, 54, '469', 0, 0, 5, 2, '2019-09-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `b_user_catalog_request`
--

CREATE TABLE `b_user_catalog_request` (
  `request_id` int(11) NOT NULL,
  `b_user_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:not approved ,1:approved'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `b_user_catalog_request`
--

INSERT INTO `b_user_catalog_request` (`request_id`, `b_user_id`, `requested_by`, `remark`, `create_date`, `status`) VALUES
(4, 8, 2, 'ffff', '2019-09-19', 0),
(5, 8, 2, 'hi hulk', '2019-09-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_category` int(11) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fractions` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `level_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`category_id`, `category_name`, `parent_category`, `description`, `fractions`, `status`, `level_id`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(1, 'Blinds', 0, '', '1/8,1/4,3/8,1/2,5/8,3/4,7/8', 1, NULL, 2, 2, '2019-09-17', '2019-09-17', 0),
(2, 'Shades', 0, '', '1/8,1/4,3/8,1/2,5/8,3/4,7/8', 1, NULL, 2, 2, '2019-09-17', '2019-09-17', 0),
(3, 'Shutters', 0, '', '1/16,1/8,1/4,3/8,1/2,5/8,3/4,7/8', 1, NULL, 2, NULL, '2019-09-17', '0000-00-00', 0),
(4, 'Arch', 0, '', '1/4,3/4,1/8,7/8', 1, NULL, 2, NULL, '2019-09-17', '0000-00-00', 0),
(5, 'Misc', 0, '', '', 1, NULL, 2, NULL, '2019-09-17', '0000-00-00', 0),
(6, 'Cellular', 0, '', '1/8,1/4,3/8,1/2,5/8,3//4,7/8', 1, NULL, 2, NULL, '2019-09-17', '0000-00-00', 0),
(7, 'Shades', 0, '', '1/8,1/4,3/8,1/2,5/8,3/4,7/8', 1, NULL, 4, 4, '2019-09-18', '2019-09-18', 0),
(8, 'Elecronic', 0, 'elc cat', '', 1, NULL, 8, NULL, '2019-09-19', '0000-00-00', 0),
(9, 'electronic', 0, '', '', 1, NULL, 12, NULL, '2019-09-20', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city_state_tbl`
--

CREATE TABLE `city_state_tbl` (
  `id` int(11) NOT NULL,
  `state_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `color_tbl`
--

CREATE TABLE `color_tbl` (
  `id` int(11) NOT NULL,
  `pattern_id` int(10) DEFAULT NULL,
  `color_name` text NOT NULL,
  `color_number` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color_tbl`
--

INSERT INTO `color_tbl` (`id`, `pattern_id`, `color_name`, `color_number`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(861, 317, 'White White', 'P01-S', 2, 0, '2019-09-17', '2019-09-19', 0),
(862, 317, 'Antique White', 'P02-S', 2, 0, '2019-09-17', '2019-09-19', 0),
(863, 317, 'Alabaster', 'P03-S', 2, 0, '2019-09-17', '2019-09-19', 0),
(864, 318, 'White White', 'P01-G', 2, 0, '2019-09-17', '2019-09-19', 0),
(865, 318, 'Antique White', 'P02-G', 2, 0, '2019-09-17', '2019-09-19', 0),
(866, 318, 'Alabaster', 'P03-G', 2, 0, '2019-09-17', '2019-09-19', 0),
(867, 319, 'White White', 'P01-B', 2, 0, '2019-09-17', '2019-09-19', 0),
(868, 319, 'Antique White', 'P02-B', 2, 0, '2019-09-17', '2019-09-19', 0),
(869, 319, 'Alabaster', 'P03-B', 2, 0, '2019-09-17', '2019-09-19', 0),
(870, 320, 'White White', 'P01-2.5', 2, 0, '2019-09-17', '2019-09-19', 0),
(871, 320, 'Antique White', 'P02-2.5', 2, 0, '2019-09-17', '2019-09-19', 0),
(872, 320, 'Alabaster', 'P03-2.5', 2, 0, '2019-09-17', '2019-09-19', 0),
(873, 317, 'White White', 'P01-WiVi', 2, 0, '2019-09-17', '2019-09-19', 0),
(874, 317, 'Antique White', 'P02-WiVi', 2, 0, '2019-09-17', '2019-09-19', 0),
(875, 321, 'Blanco', '1124', 2, 0, '2019-09-17', '2019-09-19', 0),
(876, 321, 'Vivid White', '1104', 2, 0, '2019-09-17', '2019-09-19', 0),
(877, 321, 'Pure White', '1101', 2, 0, '2019-09-17', '2019-09-19', 0),
(878, 321, 'Bone', '2910', 2, 0, '2019-09-17', '2019-09-19', 0),
(879, 321, 'Alabaster', '1123', 2, 0, '2019-09-17', '2019-09-19', 0),
(880, 321, 'Smoky Grey', '206', 2, 0, '2019-09-17', '2019-09-19', 0),
(881, 321, 'Slat Natural', '1107', 2, 0, '2019-09-17', '2019-09-19', 0),
(882, 321, 'Sugar Maple', '1109', 2, 0, '2019-09-17', '2019-09-19', 0),
(883, 321, 'Sunrise Oak', '1111', 2, 0, '2019-09-17', '2019-09-19', 0),
(884, 321, ' Mink', '1112', 2, 0, '2019-09-17', '2019-09-19', 0),
(885, 321, 'Cherry Wood', '202', 2, 0, '2019-09-17', '2019-09-19', 0),
(886, 321, 'Golden Oak', '1110', 2, 0, '2019-09-17', '2019-09-19', 0),
(887, 321, 'Warm Cherry', '1118', 2, 0, '2019-09-17', '2019-09-19', 0),
(888, 321, 'Evening Oak', '1115', 2, 0, '2019-09-17', '2019-09-19', 0),
(889, 321, 'Toasted Walnut', '1121', 2, 0, '2019-09-17', '2019-09-19', 0),
(890, 321, 'Warm Chestnut', '1122', 2, 0, '2019-09-17', '2019-09-19', 0),
(891, 321, 'Cherry', '1116', 2, 0, '2019-09-17', '2019-09-19', 0),
(892, 321, 'Mahogany', '203', 2, 0, '2019-09-17', '2019-09-19', 0),
(893, 321, 'Dark Mahogany', '205', 2, 0, '2019-09-17', '2019-09-19', 0),
(894, 321, 'Kona', '1125', 2, 0, '2019-09-17', '2019-09-19', 0),
(895, 172, 'white #11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(896, 172, 'Ivory #1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(897, 172, 'Beige #2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(898, 172, 'Grey #10', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(899, 172, 'Taupe #2', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(900, 172, 'black #7', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(901, 173, 'white', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(902, 173, 'Ivory', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(903, 173, 'Beige', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(904, 173, 'Grey', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(905, 173, 'Taupe', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(906, 173, 'Khaki', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(907, 174, 'silver lvory #11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(908, 174, 'silver wine #9', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(909, 174, 'gold beige #2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(910, 174, 'gold red #9', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(911, 175, 'Ivory #1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(912, 175, 'Silver Ivory #1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(913, 175, 'Silver White #11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(914, 175, 'White #11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(915, 176, 'white-11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(916, 176, 'lvory-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(917, 176, 'peach-2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(918, 176, 'mustard', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(919, 176, 'purple', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(920, 176, 'pink', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(921, 176, 'dark green', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(922, 176, 'blue', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(923, 176, 'grey', '9', 2, 0, '2019-09-17', '2019-09-19', 0),
(924, 176, 'brown', '10', 2, 0, '2019-09-17', '2019-09-19', 0),
(925, 176, 'caramel', '11', 2, 0, '2019-09-17', '2019-09-19', 0),
(926, 176, 'yellow', '12', 2, 0, '2019-09-17', '2019-09-19', 0),
(927, 176, 'mocha', '13', 2, 0, '2019-09-17', '2019-09-19', 0),
(928, 176, 'black', '14', 2, 0, '2019-09-17', '2019-09-19', 0),
(929, 176, 'deep green', '15', 2, 0, '2019-09-17', '2019-09-19', 0),
(930, 176, 'deep blue', '16', 2, 0, '2019-09-17', '2019-09-19', 0),
(931, 177, 'M/mushroom-2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(932, 177, 'M/khaki-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(933, 177, 'Brown-8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(934, 177, 'Nile-4', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(935, 177, 'Chocolate-8', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(936, 177, 'White-11', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(937, 177, 'M/pink-3', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(938, 177, 'M/green-4', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(939, 177, 'M/blue-5', '9', 2, 0, '2019-09-17', '2019-09-19', 0),
(940, 177, 'Cherry-8', '10', 2, 0, '2019-09-17', '2019-09-19', 0),
(941, 177, 'Mellow green-4', '11', 2, 0, '2019-09-17', '2019-09-19', 0),
(942, 177, 'Grey-10', '12', 2, 0, '2019-09-17', '2019-09-19', 0),
(943, 177, 'Black-7', '13', 2, 0, '2019-09-17', '2019-09-19', 0),
(944, 177, 'Sand-2', '14', 2, 0, '2019-09-17', '2019-09-19', 0),
(945, 177, 'Ash tree-10', '15', 2, 0, '2019-09-17', '2019-09-19', 0),
(946, 177, 'Lilac-6', '16', 2, 0, '2019-09-17', '2019-09-19', 0),
(947, 177, 'Peach-2', '17', 2, 0, '2019-09-17', '2019-09-19', 0),
(948, 177, 'Turquiose-5', '18', 2, 0, '2019-09-17', '2019-09-19', 0),
(949, 177, 'Wine-9', '19', 2, 0, '2019-09-17', '2019-09-19', 0),
(950, 178, 'lvory-1', '101', 2, 0, '2019-09-17', '2019-09-19', 0),
(951, 178, 'mushroom-2', '102', 2, 0, '2019-09-17', '2019-09-19', 0),
(952, 178, 'ashtree-2', '103', 2, 0, '2019-09-17', '2019-09-19', 0),
(953, 178, 'silver grey-10', '104', 2, 0, '2019-09-17', '2019-09-19', 0),
(954, 178, 'grey-10', '105', 2, 0, '2019-09-17', '2019-09-19', 0),
(955, 178, 'Chocolate-8', '106', 2, 0, '2019-09-17', '2019-09-19', 0),
(956, 179, 'green-4', '301', 2, 0, '2019-09-17', '2019-09-19', 0),
(957, 179, 'pink-3', '302', 2, 0, '2019-09-17', '2019-09-19', 0),
(958, 179, 'blue-5', '303', 2, 0, '2019-09-17', '2019-09-19', 0),
(959, 179, 'brown-8', '304', 2, 0, '2019-09-17', '2019-09-19', 0),
(960, 179, 'black-7', '305', 2, 0, '2019-09-17', '2019-09-19', 0),
(961, 179, 'indian pink-2', '306', 2, 0, '2019-09-17', '2019-09-19', 0),
(962, 180, 'Tuape-2', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(963, 180, 'Beige-2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(964, 180, 'Gold-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(965, 180, 'Grey-10', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(966, 180, 'Brown-2', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(967, 180, 'Indian Pink-3', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(968, 180, 'Green-4', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(969, 180, 'Black Grey-7', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(970, 180, 'D/Brown-8', '9', 2, 0, '2019-09-17', '2019-09-19', 0),
(971, 181, 'Ivory-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(972, 181, 'N.lvory-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(973, 181, 'N.pink-2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(974, 181, 'N.brown-2', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(975, 181, 'Coffee-8', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(976, 181, 'Purple-6', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(977, 181, 'Wine-9', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(978, 181, 'Monaco-8', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(979, 181, 'Grey-10', '9', 2, 0, '2019-09-17', '2019-09-19', 0),
(980, 181, 'Black-7', '10', 2, 0, '2019-09-17', '2019-09-19', 0),
(981, 181, 'Brown-8', '11', 2, 0, '2019-09-17', '2019-09-19', 0),
(982, 181, 'Vanilla-2', '12', 2, 0, '2019-09-17', '2019-09-19', 0),
(983, 181, 'Scarlet-2', '13', 2, 0, '2019-09-17', '2019-09-19', 0),
(984, 181, 'Lime-4', '14', 2, 0, '2019-09-17', '2019-09-19', 0),
(985, 181, 'Peach-3', '15', 2, 0, '2019-09-17', '2019-09-19', 0),
(986, 181, 'Begie-2', '16', 2, 0, '2019-09-17', '2019-09-19', 0),
(987, 181, 'Light pink-2', '17', 2, 0, '2019-09-17', '2019-09-19', 0),
(988, 182, 'Green-4', '4101', 2, 0, '2019-09-17', '2019-09-19', 0),
(989, 182, 'Brown-2', '4102', 2, 0, '2019-09-17', '2019-09-19', 0),
(990, 182, 'Pink-3', '4103', 2, 0, '2019-09-17', '2019-09-19', 0),
(991, 182, 'Blue-5', '4104', 2, 0, '2019-09-17', '2019-09-19', 0),
(992, 182, 'Black-7', '4105', 2, 0, '2019-09-17', '2019-09-19', 0),
(993, 182, 'Grey-10', '4106', 2, 0, '2019-09-17', '2019-09-19', 0),
(994, 182, 'Indian Pink-2', '4107', 2, 0, '2019-09-17', '2019-09-19', 0),
(995, 183, 'ice white-11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(996, 183, 'natural-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(997, 183, 'peach-2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(998, 183, 'green-4', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(999, 183, 'beige-2', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1000, 183, 'brown-8', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1001, 183, 'grey-10', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1002, 184, 'dark cream-2', '1801', 2, 0, '2019-09-17', '2019-09-19', 0),
(1003, 184, 'green-4', '1802', 2, 0, '2019-09-17', '2019-09-19', 0),
(1004, 184, 'pink-3', '1803', 2, 0, '2019-09-17', '2019-09-19', 0),
(1005, 184, 'blue-5', '1804', 2, 0, '2019-09-17', '2019-09-19', 0),
(1006, 184, 'brown-8', '1805', 2, 0, '2019-09-17', '2019-09-19', 0),
(1007, 184, 'chocolate-8', '1806', 2, 0, '2019-09-17', '2019-09-19', 0),
(1008, 184, 'sand-2', '1807', 2, 0, '2019-09-17', '2019-09-19', 0),
(1009, 184, 'grey-10', '1808', 2, 0, '2019-09-17', '2019-09-19', 0),
(1010, 184, 'dark green-4', '1809', 2, 0, '2019-09-17', '2019-09-19', 0),
(1011, 185, 'white-11', 'JM 1-1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1012, 185, 'ice beige-1', 'JM 1-2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1013, 185, 'sang-2', 'JM 1-3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1014, 185, 'olive gold-2', 'JM 1-4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1015, 185, 'light grey-10', 'JM 1-5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1016, 185, 'khakie-2', 'JM 1-6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1017, 185, 'dark grey-10', 'JM 1-7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1018, 186, 'lvory-1', 'EM 3-1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1019, 186, 'sand-1', 'EM 3-2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1020, 186, 'beige-2', 'EM 3-3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1021, 186, 'light grey-10', 'EM 3-4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1022, 186, 'brown-8', 'EM 3-5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1023, 186, 'wood-8', 'EM 3-6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1024, 186, 'dark grey-10', 'EM 3-7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1025, 187, 'beige-2', '4004', 2, 0, '2019-09-17', '2019-09-19', 0),
(1026, 187, 'grey-10', '4005', 2, 0, '2019-09-17', '2019-09-19', 0),
(1027, 187, 'wood-8', '4006', 2, 0, '2019-09-17', '2019-09-19', 0),
(1028, 187, 'wine-9', '4007', 2, 0, '2019-09-17', '2019-09-19', 0),
(1029, 187, 'ink black-7', '4008', 2, 0, '2019-09-17', '2019-09-19', 0),
(1030, 188, 'White-11', '7901', 2, 0, '2019-09-17', '2019-09-19', 0),
(1031, 188, 'lvory-1', '7902', 2, 0, '2019-09-17', '2019-09-19', 0),
(1032, 188, 'Beige-2', '7903', 2, 0, '2019-09-17', '2019-09-19', 0),
(1033, 188, 'dark beige-8', '7904', 2, 0, '2019-09-17', '2019-09-19', 0),
(1034, 188, 'Teak-8', '7905', 2, 0, '2019-09-17', '2019-09-19', 0),
(1035, 188, 'Chocolate-8', '7906', 2, 0, '2019-09-17', '2019-09-19', 0),
(1036, 188, 'Blue Grey-10', '7907', 2, 0, '2019-09-17', '2019-09-19', 0),
(1037, 188, 'Grey-10', '7908', 2, 0, '2019-09-17', '2019-09-19', 0),
(1038, 188, 'Dark Grey-10', '7909', 2, 0, '2019-09-17', '2019-09-19', 0),
(1039, 188, 'Black-7', '7910', 2, 0, '2019-09-17', '2019-09-19', 0),
(1040, 189, 'Cadet Blue', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1041, 189, 'Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1042, 189, 'Vanilla Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1043, 189, 'Mushroom', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1044, 189, 'Choclate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1045, 190, 'White-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1046, 190, 'Mushroom -2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1047, 190, 'Cherry -2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1048, 190, 'GREEN -4', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1049, 190, 'Chocolate -8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1050, 191, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1051, 191, 'Onion', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1052, 191, 'Beige-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1053, 191, 'Purple', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1054, 191, 'Pink', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1055, 192, 'lvory-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1056, 192, 'grey-10', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1057, 192, 'beige-2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1058, 192, 'brown-8', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1059, 192, 'dark grey-10', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1060, 192, 'chocolate-8', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1061, 193, 'white-11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1062, 193, 'lvory-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1063, 193, 'brown-8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1064, 193, 'light grey-10', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1065, 193, 'grey-10', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1066, 193, 'wine-9', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1067, 194, 'White', 'BA 6-1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1068, 194, 'Ivory', 'BA 6-2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1069, 194, 'Beige', 'BA 6-3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1070, 194, 'Vanilla', 'BA 6-4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1071, 194, 'Grey', 'BA 6-5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1072, 194, 'Dark Grey', 'BA 6-6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1073, 194, 'Brown', 'BA 6-7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1074, 194, 'Ink Blackout', 'BA 6-8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1075, 194, 'Charcoal', 'BA 6-9', 2, 0, '2019-09-17', '2019-09-19', 0),
(1076, 195, 'Cherry-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1077, 195, 'Khaki-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1078, 195, 'Green-4', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1079, 195, 'Lilac-6', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1080, 195, 'Blue-5', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1081, 195, 'Pink-3', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1082, 195, 'Brown-8', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1083, 196, 'Beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1084, 196, 'Gold', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1085, 196, 'Indian Pink', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1086, 196, 'Green', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1087, 196, 'Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1088, 196, 'Dark Brown', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1089, 197, 'lvory-2', '802', 2, 0, '2019-09-17', '2019-09-19', 0),
(1090, 197, 'Vanilla-2', '803', 2, 0, '2019-09-17', '2019-09-19', 0),
(1091, 197, 'Peach-2', '804', 2, 0, '2019-09-17', '2019-09-19', 0),
(1092, 197, 'Beige-2', '805', 2, 0, '2019-09-17', '2019-09-19', 0),
(1093, 197, 'Grey-10', '806', 2, 0, '2019-09-17', '2019-09-19', 0),
(1094, 197, 'Dark Grey-10', '807', 2, 0, '2019-09-17', '2019-09-19', 0),
(1095, 197, 'Brown-8', '808', 2, 0, '2019-09-17', '2019-09-19', 0),
(1096, 197, 'Black-7', '809', 2, 0, '2019-09-17', '2019-09-19', 0),
(1097, 198, 'Beige-2', '5501', 2, 0, '2019-09-17', '2019-09-19', 0),
(1098, 198, 'Grey-10', '5502', 2, 0, '2019-09-17', '2019-09-19', 0),
(1099, 198, 'Teak-8', '5503', 2, 0, '2019-09-17', '2019-09-19', 0),
(1100, 198, 'Anthracite-10', '5504', 2, 0, '2019-09-17', '2019-09-19', 0),
(1101, 198, 'Chocolate-8', '5505', 2, 0, '2019-09-17', '2019-09-19', 0),
(1102, 198, 'Black-7', '5506', 2, 0, '2019-09-17', '2019-09-19', 0),
(1103, 199, 'ca-01-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1104, 199, 'ca-02-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1105, 199, 'ca-04-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1106, 199, 'ca-05-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1107, 199, 'ca-07-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1108, 199, 'ca-08-7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1109, 199, 'ca-09-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1110, 200, 'Rose-2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1111, 200, 'Sandy-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1112, 200, 'Lime-4', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1113, 200, 'Peach-2', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1114, 200, 'Lavender-2', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1115, 200, 'Olive-4', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1116, 200, 'Oak-8', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1117, 201, 'Ivory-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1118, 201, 'Beige-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1119, 201, 'Green-4', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1120, 201, 'Light Grey-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1121, 201, 'Wood-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1122, 202, 'Brown-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1123, 202, 'Ivory-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1124, 202, 'Oil Green-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1125, 202, 'Dark Green-4', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1126, 202, 'Ink Black-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1127, 202, 'White-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1128, 202, 'Beige-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1129, 203, 'Chocolate chip-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1130, 203, 'Nougat-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1131, 203, 'Olive gold-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1132, 203, 'Ink black-7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1133, 203, 'White-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1134, 203, 'Grey-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1135, 203, 'Cream lvory-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1136, 203, 'Light Brown-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1137, 204, 'white-1', 'sm 3601', 2, 0, '2019-09-17', '2019-09-19', 0),
(1138, 204, 'lvory-2', 'sm 3602', 2, 0, '2019-09-17', '2019-09-19', 0),
(1139, 204, 'beige-2', 'sm 3603', 2, 0, '2019-09-17', '2019-09-19', 0),
(1140, 204, 'brown-8', 'sm 3604', 2, 0, '2019-09-17', '2019-09-19', 0),
(1141, 204, 'wood-7', 'sm 3605', 2, 0, '2019-09-17', '2019-09-19', 0),
(1142, 205, 'Rf-stat 1486-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1143, 205, 'Rf-stat 1422-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1144, 205, 'Rf-stat 1442-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1145, 205, 'Rf-stat 1448-7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1146, 205, 'Rf-stat 1433-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1147, 205, 'Rf-stat 1412-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1148, 205, 'Rf-stat 1445-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1149, 205, 'Rf-stat 1419-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1150, 206, 'RF-STD1443-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1151, 206, 'RF-STN 1431-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1152, 206, 'RF-STD1432-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1153, 206, 'RF-STN1434-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1154, 206, 'RF-STN1484-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1155, 207, 'FT-1101', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1156, 207, 'FT-1104', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1157, 207, 'FT-1105', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1158, 207, 'FT-1106', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1159, 207, 'FT-1108', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1160, 208, 'White-1', '101', 2, 0, '2019-09-17', '2019-09-19', 0),
(1161, 208, 'lvory-1', '102', 2, 0, '2019-09-17', '2019-09-19', 0),
(1162, 208, 'Gold-2', '103', 2, 0, '2019-09-17', '2019-09-19', 0),
(1163, 208, 'Silver Grey-10', '104', 2, 0, '2019-09-17', '2019-09-19', 0),
(1164, 208, 'Grey-10', '105', 2, 0, '2019-09-17', '2019-09-19', 0),
(1165, 208, 'Black-7', '106', 2, 0, '2019-09-17', '2019-09-19', 0),
(1166, 208, 'Coffee-8', '107', 2, 0, '2019-09-17', '2019-09-19', 0),
(1167, 209, 'VL-BA1313', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1168, 209, 'VL-BA2313', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1169, 209, 'VL-BAD4313', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1170, 209, 'VL-BAD4813', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1171, 209, 'VL-BA4513', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1172, 209, 'VL-BAN4913', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1173, 209, 'VL-BAN9513', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1174, 209, 'VL-BA9513', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1175, 210, 'White-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1176, 210, 'Ivory-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1177, 210, 'Mushroom-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1178, 210, 'L.Grey-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1179, 210, 'Choco Chip-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1180, 210, 'Grey-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1181, 210, 'Olive Gold-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1182, 210, 'Brown-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1183, 210, 'D.Beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1184, 210, 'Charcoal-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1185, 211, 'White', '8501', 2, 0, '2019-09-17', '2019-09-19', 0),
(1186, 211, 'Beige', '8503', 2, 0, '2019-09-17', '2019-09-19', 0),
(1187, 211, 'grey-10', '8504', 2, 0, '2019-09-17', '2019-09-19', 0),
(1188, 211, 'Dark Grey', '8505', 2, 0, '2019-09-17', '2019-09-19', 0),
(1189, 211, 'Sand', '8506', 2, 0, '2019-09-17', '2019-09-19', 0),
(1190, 211, 'Cadet Blue', '8509', 2, 0, '2019-09-17', '2019-09-19', 0),
(1191, 212, 'Ashtree', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1192, 212, 'Tivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1193, 212, 'Beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1194, 212, 'Water Gren', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1195, 213, 'Ice white', '6501', 2, 0, '2019-09-17', '2019-09-19', 0),
(1196, 213, 'Ivory', '6503', 2, 0, '2019-09-17', '2019-09-19', 0),
(1197, 213, 'Beige', '6504', 2, 0, '2019-09-17', '2019-09-19', 0),
(1198, 213, 'Dark Beige', '6505', 2, 0, '2019-09-17', '2019-09-19', 0),
(1199, 213, 'Teak', '6506', 2, 0, '2019-09-17', '2019-09-19', 0),
(1200, 213, 'Chocolate', '6507', 2, 0, '2019-09-17', '2019-09-19', 0),
(1201, 213, 'Charcoal', '6511', 2, 0, '2019-09-17', '2019-09-19', 0),
(1202, 214, 'White-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1203, 214, 'Ivory-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1204, 214, 'Beige-1', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1205, 214, 'Light Gray-10', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1206, 214, 'Gray-10', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1207, 214, 'Brown-8', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1208, 214, 'Chocolate-8', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1209, 215, 'White-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1210, 215, 'Ivory-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1211, 215, 'Gray-10', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1212, 215, 'Beige-2', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1213, 215, 'Brown-8', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1214, 215, 'Chocolate-8', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1215, 216, 'OR-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1216, 216, 'OR-10', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1217, 217, 'He-2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1218, 217, 'He-10', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1219, 218, 'Chocolate-8', '7509', 2, 0, '2019-09-17', '2019-09-19', 0),
(1220, 218, 'White-1', '7501', 2, 0, '2019-09-17', '2019-09-19', 0),
(1221, 218, 'Ivory-1', '7502', 2, 0, '2019-09-17', '2019-09-19', 0),
(1222, 218, 'beige-2', '7503', 2, 0, '2019-09-17', '2019-09-19', 0),
(1223, 218, 'Green-4', '7504', 2, 0, '2019-09-17', '2019-09-19', 0),
(1224, 218, 'Pink-3', '7505', 2, 0, '2019-09-17', '2019-09-19', 0),
(1225, 218, 'Grey-10', '7506', 2, 0, '2019-09-17', '2019-09-19', 0),
(1226, 218, 'Dark Grey-10', '7507', 2, 0, '2019-09-17', '2019-09-19', 0),
(1227, 218, 'Brown-8', '7508', 2, 0, '2019-09-17', '2019-09-19', 0),
(1228, 219, 'White', '6201', 2, 0, '2019-09-17', '2019-09-19', 0),
(1229, 219, 'Ivory', '6202', 2, 0, '2019-09-17', '2019-09-19', 0),
(1230, 219, 'Beige', '6203', 2, 0, '2019-09-17', '2019-09-19', 0),
(1231, 219, 'Khaki', '6204', 2, 0, '2019-09-17', '2019-09-19', 0),
(1232, 219, 'Cherry', '6205', 2, 0, '2019-09-17', '2019-09-19', 0),
(1233, 219, 'Grey', '6207', 2, 0, '2019-09-17', '2019-09-19', 0),
(1234, 219, 'Dark Grey', '6208', 2, 0, '2019-09-17', '2019-09-19', 0),
(1235, 219, 'Caramel', '6210', 2, 0, '2019-09-17', '2019-09-19', 0),
(1236, 220, 'White-1', '2201', 2, 0, '2019-09-17', '2019-09-19', 0),
(1237, 220, 'lvory-2', '2202', 2, 0, '2019-09-17', '2019-09-19', 0),
(1238, 220, 'Vanilla-2', '2203', 2, 0, '2019-09-17', '2019-09-19', 0),
(1239, 220, 'Beige-2', '2204', 2, 0, '2019-09-17', '2019-09-19', 0),
(1240, 220, 'Grey-10', '2205', 2, 0, '2019-09-17', '2019-09-19', 0),
(1241, 220, 'Dark grey-10', '2206', 2, 0, '2019-09-17', '2019-09-19', 0),
(1242, 220, 'Brown-8', '2207', 2, 0, '2019-09-17', '2019-09-19', 0),
(1243, 220, 'Oak-2', '2210', 2, 0, '2019-09-17', '2019-09-19', 0),
(1244, 220, '2209 Black-7', '2209', 2, 0, '2019-09-17', '2019-09-19', 0),
(1245, 221, 'White-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1246, 221, 'Beige-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1247, 221, 'Brown-8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1248, 221, 'Coconut-8', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1249, 221, 'Grey-10', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1250, 221, 'Charcoal-7', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1251, 222, 'Ivory-2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1252, 222, 'Choco-8', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1253, 222, 'Brown-2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1254, 222, 'W/Brown-1', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1255, 222, 'I/Choco-7', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1256, 223, 'Os-1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1257, 223, 'Os-2', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1258, 223, 'Os-10', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1259, 223, 'Os-8', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1260, 223, 'Os-7', '10', 2, 0, '2019-09-17', '2019-09-19', 0),
(1261, 224, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1262, 224, 'Light Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1263, 224, 'Dark Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1264, 224, 'Yellow Green', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1265, 224, 'Blue-5', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1266, 224, 'green #4', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1267, 224, 'Gold', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1268, 224, 'Choco', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1269, 224, 'Wine', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1270, 225, 'PR-2', '101', 2, 0, '2019-09-17', '2019-09-19', 0),
(1271, 225, 'PR-10', '102', 2, 0, '2019-09-17', '2019-09-19', 0),
(1272, 226, '002 WHITE #1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1273, 226, '002 ANTIQUE WHITE #2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1274, 226, '003 brown olive sheen #8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1275, 226, '004 pin bark #8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1276, 226, '005 penguin #8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1277, 226, '006 caster grey #7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1278, 226, '007 cadet blue #5', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1279, 226, '008 Grape Royate #7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1280, 227, 'White-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1281, 227, 'Gold-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1282, 227, 'Khaki-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1283, 227, 'Black-7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1284, 228, 'Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1285, 228, 'Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1286, 228, 'Beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1287, 229, 'MO-2', '103', 2, 0, '2019-09-17', '2019-09-19', 0),
(1288, 229, 'MO-10', '104', 2, 0, '2019-09-17', '2019-09-19', 0),
(1289, 230, 'White-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1290, 230, 'Ivory-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1291, 230, 'Blue-5', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1292, 230, 'Green-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1293, 230, 'Gold-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1294, 230, 'Brown-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1295, 230, 'Wine-9', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1296, 230, 'Grey-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1297, 230, 'Black-7', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1298, 231, 'White', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1299, 231, 'Sand', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1300, 231, 'Land', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1301, 231, 'Saddle Brown', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1302, 231, 'Ruby', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1303, 232, 'lvory #2', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1304, 232, 'beige #2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1305, 232, 'peach #8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1306, 232, 'purple #6', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1307, 233, 'Grey-10', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1308, 233, 'Charcoal-8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1309, 233, 'Coconut-2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1310, 233, 'Graphite-7', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1311, 234, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1312, 234, 'Grey', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1313, 234, 'beige', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1314, 234, 'Coffee', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1315, 234, 'D.Brown', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1316, 234, 'Water Melon', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1317, 234, 'Wine', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1318, 234, 'Charcoal', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1319, 234, 'L/Green', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1320, 234, 'Pink', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1321, 234, 'D/Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1322, 234, 'Navy', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1323, 234, 'Sky Blue', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1324, 235, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1325, 236, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1326, 237, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1327, 238, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1328, 239, 'white #11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1329, 239, 'mushroom #1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1330, 239, 'Sand #2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1331, 239, 'grey #10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1332, 239, 'chocolate #8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1333, 240, 'White', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1334, 240, 'Ivory', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1335, 240, 'Green', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1336, 240, 'Blue', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1337, 240, 'Grey', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1338, 241, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1339, 241, 'Pink', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1340, 241, 'Grey', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1341, 241, 'Brown', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1342, 242, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1343, 242, 'Yellow Green', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1344, 242, 'Brown', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1345, 242, 'Blue', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1346, 184, 'L.Beige', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1347, 184, 'Blue', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1348, 184, 'Grey', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1349, 184, 'L.Green', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1350, 243, 'Mushroom', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1351, 243, 'Grey', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1352, 243, 'Blue', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1353, 243, 'Brown', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1354, 244, 'Yellow', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1355, 244, 'Khaki', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1356, 244, 'Red Wine', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1357, 244, 'Purple', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1358, 245, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1359, 245, 'Yellow', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1360, 245, 'Grey', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1361, 245, 'Blue', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1362, 246, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1363, 246, 'Khaki', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1364, 246, 'Beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1365, 246, 'Nile', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1366, 246, 'Brown', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1367, 246, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1368, 246, 'Mushroom', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1369, 246, 'Chocolate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1370, 246, 'Black', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1371, 247, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1372, 247, 'Beige', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1373, 247, 'Green', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1374, 247, 'Grey', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1375, 247, 'Blue', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1376, 248, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1377, 248, 'L/Green', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1378, 248, 'Brown', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1379, 248, 'Blue', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1380, 248, 'Green', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1381, 248, 'OZ', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1382, 249, 'Mushroom', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1383, 249, 'Yellow', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1384, 249, 'Brown', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1385, 250, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1386, 250, 'Beige', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1387, 250, 'Green', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1388, 250, 'Grey', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1389, 250, 'Choco', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1390, 250, 'Black', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1391, 251, 'White #11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1392, 251, 'Ivory #1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1393, 251, 'Lemon #2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1394, 251, 'Green #4', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1395, 251, 'Pink #3', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1396, 251, 'Blue #5', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1397, 251, 'Grey #10', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1398, 252, 'Ivory #1', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1399, 252, 'L/Green #2', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1400, 252, 'Brown #8', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1401, 252, 'Blue #5', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1402, 253, 'Ivory', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1403, 253, 'Grey', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1404, 253, 'beige', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1405, 253, 'Coffee', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1406, 253, 'D.Brown', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1407, 253, 'Water Melon', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1408, 253, 'Wine', '7', 2, 0, '2019-09-17', '2019-09-19', 0),
(1409, 253, 'Charcoal', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1410, 253, 'L/Green', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1411, 253, 'Pink', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1412, 253, 'D/Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1413, 253, 'Navy', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1414, 253, 'Sky Blue', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1415, 254, 'white', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1416, 254, 'lvory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1417, 254, 'beige', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1418, 254, 'coconut', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1419, 254, 'khaki', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1420, 255, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1421, 255, 'Off White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1422, 255, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1423, 255, 'Sand', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1424, 255, 'Mocha', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1425, 255, 'Sone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1426, 256, 'White', '20301', 2, 0, '2019-09-17', '2019-09-19', 0),
(1427, 256, 'Ivory', '20302', 2, 0, '2019-09-17', '2019-09-19', 0),
(1428, 256, 'Grey', '20303', 2, 0, '2019-09-17', '2019-09-19', 0),
(1429, 256, 'Brown', '20304', 2, 0, '2019-09-17', '2019-09-19', 0),
(1430, 256, 'Dark Blue', '20305', 2, 0, '2019-09-17', '2019-09-19', 0),
(1431, 257, 'White', '20501', 2, 0, '2019-09-17', '2019-09-19', 0),
(1432, 257, 'Mushroom', '20502', 2, 0, '2019-09-17', '2019-09-19', 0),
(1433, 257, 'Grey', '20503', 2, 0, '2019-09-17', '2019-09-19', 0),
(1434, 257, 'Khaki', '20504', 2, 0, '2019-09-17', '2019-09-19', 0),
(1435, 258, 'Snow White #11', '20101', 2, 0, '2019-09-17', '2019-09-19', 0),
(1436, 258, 'Oyster White #1', '20102', 2, 0, '2019-09-17', '2019-09-19', 0),
(1437, 258, 'Marzipan', '20103', 2, 0, '2019-09-17', '2019-09-19', 0),
(1438, 258, 'Chanterelle', '20104', 2, 0, '2019-09-17', '2019-09-19', 0),
(1439, 258, 'Wood Smoke', '20105', 2, 0, '2019-09-17', '2019-09-19', 0),
(1440, 258, 'Bright White', '20106', 2, 0, '2019-09-17', '2019-09-19', 0),
(1441, 258, 'Barely Blue', '20107', 2, 0, '2019-09-17', '2019-09-19', 0),
(1442, 258, 'Fint Grey', '20108', 2, 0, '2019-09-17', '2019-09-19', 0),
(1443, 258, 'China Blue', '20109', 2, 0, '2019-09-17', '2019-09-19', 0),
(1444, 258, 'Black Coffee', '20110', 2, 0, '2019-09-17', '2019-09-19', 0),
(1445, 258, 'Bitter Chocolate', '20111', 2, 0, '2019-09-17', '2019-09-19', 0),
(1446, 258, 'Twilight Blue', '20112', 2, 0, '2019-09-17', '2019-09-19', 0),
(1447, 258, 'Black', '20113', 2, 0, '2019-09-17', '2019-09-19', 0),
(1448, 259, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1449, 259, 'White Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1450, 259, 'Ebony Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1451, 259, 'Ebony', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1452, 259, 'White Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1453, 259, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1454, 259, 'Taupe', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1455, 259, 'Chestnut', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1456, 259, 'Brown Chocolate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1457, 260, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1458, 260, 'White Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1459, 260, 'Ebony Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1460, 260, 'Ebony', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1461, 260, 'White Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1462, 260, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1463, 260, 'Taupe', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1464, 260, 'Chestnut', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1465, 260, 'Brown Chocolate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1466, 261, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1467, 261, 'White Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1468, 261, 'Ebony Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1469, 261, 'Ebony', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1470, 261, 'White Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1471, 261, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1472, 261, 'Taupe', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1473, 261, 'Chestnut', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1474, 261, 'Brown Chocolate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1475, 262, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1476, 262, 'Milk', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1477, 262, 'Peach', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1478, 262, 'Silver Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1479, 262, 'Shark', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1480, 263, 'White-11', '60751', 2, 0, '2019-09-17', '2019-09-19', 0),
(1481, 263, 'Cream-1', '60752', 2, 0, '2019-09-17', '2019-09-19', 0),
(1482, 263, 'Beige-2', '60753', 2, 0, '2019-09-17', '2019-09-19', 0),
(1483, 263, 'Latte-8', '60755', 2, 0, '2019-09-17', '2019-09-19', 0),
(1484, 263, 'Shark-10', '60756', 2, 0, '2019-09-17', '2019-09-19', 0),
(1485, 263, 'Silver Grey-2', '60754', 2, 0, '2019-09-17', '2019-09-19', 0),
(1486, 263, 'Coffee-8', '60758', 2, 0, '2019-09-17', '2019-09-19', 0),
(1487, 263, 'Black-7', '60759', 2, 0, '2019-09-17', '2019-09-19', 0),
(1488, 264, 'White-11', 'D7513', 2, 0, '2019-09-17', '2019-09-19', 0),
(1489, 264, 'Cream Ivory-2', 'D7520', 2, 0, '2019-09-17', '2019-09-19', 0),
(1490, 264, 'Ivory-1', 'D7521', 2, 0, '2019-09-17', '2019-09-19', 0),
(1491, 264, 'Cream-2', 'D7531', 2, 0, '2019-09-17', '2019-09-19', 0),
(1492, 264, 'Natural Beige-10', 'D7532', 2, 0, '2019-09-17', '2019-09-19', 0),
(1493, 264, 'Natural Grey-10', 'D7541', 2, 0, '2019-09-17', '2019-09-19', 0),
(1494, 264, 'Natura D/Grey-10', 'D7542', 2, 0, '2019-09-17', '2019-09-19', 0),
(1495, 264, 'Charcoal-7', 'D7546', 2, 0, '2019-09-17', '2019-09-19', 0),
(1496, 264, 'Blue Willow-11', 'D7556', 2, 0, '2019-09-17', '2019-09-19', 0),
(1497, 265, 'lvory-1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1498, 265, 'dark lvory-10', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1499, 265, 'grey-10', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1500, 265, 'beige-2', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1501, 265, 'brown-8', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1502, 265, 'pink-3', '8', 2, 0, '2019-09-17', '2019-09-19', 0),
(1503, 266, 'w-3-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1504, 266, 'w-7-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1505, 266, 'w-20-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1506, 266, 'w-32-11', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1507, 267, 'light carmel -2', '250', 2, 0, '2019-09-17', '2019-09-19', 0),
(1508, 267, 'greyapricot-10', 'k168', 2, 0, '2019-09-17', '2019-09-19', 0),
(1509, 267, 'brown-8', 'k192', 2, 0, '2019-09-17', '2019-09-19', 0),
(1510, 267, 'light apricot-2', 'k157', 2, 0, '2019-09-17', '2019-09-19', 0),
(1511, 267, 'dark camel-8', '249', 2, 0, '2019-09-17', '2019-09-19', 0),
(1512, 268, 'lvory-1', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1513, 268, 'beige-2', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1514, 268, 'light grey-10', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1515, 268, 'brown-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1516, 268, 'chocolate-8', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1517, 269, 'white #11', '1', 2, 0, '2019-09-17', '2019-09-19', 0),
(1518, 269, 'mushroom #1', '2', 2, 0, '2019-09-17', '2019-09-19', 0),
(1519, 269, 'cherry #2', '3', 2, 0, '2019-09-17', '2019-09-19', 0),
(1520, 269, 'brown #8', '4', 2, 0, '2019-09-17', '2019-09-19', 0),
(1521, 269, 'chocolate #8', '5', 2, 0, '2019-09-17', '2019-09-19', 0),
(1522, 269, 'green #4', '6', 2, 0, '2019-09-17', '2019-09-19', 0),
(1523, 270, 'Cashmere', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1524, 270, 'Silver', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1525, 270, 'Charcoal', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1526, 270, 'Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1527, 271, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1528, 271, 'Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1529, 271, 'Oyster', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1530, 271, 'Gold', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1531, 271, 'Copper', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1532, 271, 'Emerald', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1533, 271, 'Ice Blue', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1534, 272, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1535, 273, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1536, 273, 'Charcoal', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1537, 273, 'Oatmeal', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1538, 273, 'Parchment', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1539, 274, 'Midnight', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1540, 274, 'Daybreak', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1541, 275, 'Jade', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1542, 275, 'Ochre', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1543, 276, 'China White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1544, 276, 'Luna', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1545, 276, 'Shadow', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1546, 276, 'Raven', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1547, 276, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1548, 276, 'Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1549, 276, 'Ecru', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1550, 276, 'Taupe', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1551, 276, 'Papaya', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1552, 276, 'Clay', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1553, 277, 'Stone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1554, 277, 'Platinum', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1555, 277, 'Anchor', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1556, 277, 'Shell', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1557, 277, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1558, 277, 'Flax', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1559, 278, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1560, 278, 'Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1561, 278, 'Black', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1562, 279, 'Serenity', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1563, 279, 'Kimono', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1564, 279, 'Geisha', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1565, 280, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1566, 280, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1567, 280, 'Dove', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1568, 280, 'Charcoal', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1569, 281, 'Diamon Dust', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1570, 281, 'Moonlight Mink', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1571, 281, 'Lily Pad', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1572, 281, 'Champagne Fizz', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1573, 281, 'Antique Gold', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1574, 282, 'Ocra', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1575, 282, 'Lago', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1576, 282, 'Alba', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1577, 283, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1578, 284, 'Whistler White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1579, 284, 'Chateaux Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1580, 284, 'London Slate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1581, 284, 'Nordic Mist', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1582, 284, 'Cornsih Blue', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1583, 285, 'Pearl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1584, 285, 'Champagne', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1585, 285, 'Silver', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1586, 286, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1587, 287, 'Silver Sand', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1588, 287, 'Picket Fence', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1589, 287, 'Beach Cove', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1590, 287, 'Boat House', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1591, 287, 'Mahogany Brown', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1592, 287, 'Sea Breeze', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1593, 288, 'Greystone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1594, 288, 'Chalk', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1595, 288, 'Sandstone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1596, 288, 'Limestone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1597, 289, 'White Ash', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1598, 289, 'Magnolia', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1599, 289, 'Willow', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1600, 289, 'Silver Birch', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1601, 290, 'Marine', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1602, 291, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1603, 291, 'Birch', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1604, 291, 'Luna', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1605, 291, 'Papaya', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1606, 292, 'Pipin', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1607, 292, 'Inky', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1608, 292, 'Rosa', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1609, 293, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1610, 293, 'Angora', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1611, 293, 'Maize', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1612, 293, 'Truffle', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1613, 294, 'Silver', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1614, 295, 'Nightingale', NULL, 2, 0, '2019-09-17', '2019-09-19', 0);
INSERT INTO `color_tbl` (`id`, `pattern_id`, `color_name`, `color_number`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(1615, 295, 'Dove', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1616, 295, 'Lark', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1617, 296, 'Smoke Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1618, 296, 'Nimbus Cloud', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1619, 296, 'Desert Sand', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1620, 296, 'Blue Haze', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1621, 297, 'Ice', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1622, 297, 'Onyx', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1623, 297, 'Opal', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1624, 297, 'Sand', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1625, 297, 'Mineral', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1626, 298, 'Shale', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1627, 298, 'Marble', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1628, 298, 'Sand', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1629, 299, 'Frost', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1630, 299, 'Haze', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1631, 299, 'Mist', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1632, 299, 'Fog', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1633, 299, 'Cloud', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1634, 299, 'Storm', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1635, 300, 'Frost', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1636, 300, 'Cloud', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1637, 300, 'Fog', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1638, 300, 'Storm', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1639, 300, 'Haze', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1640, 300, 'Mist', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1641, 301, 'Glow', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1642, 302, 'Graphite', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1643, 302, 'Birch', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1644, 302, 'Bamboo', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1645, 302, 'Maple', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1646, 302, 'Jasper', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1647, 302, 'Hazel', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1648, 303, 'Pebble', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1649, 303, 'Bamboo', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1650, 303, 'Wicker', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1651, 303, 'Rice', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1652, 303, 'Driftwood', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1653, 304, 'Feather', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1654, 304, 'Cinder', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1655, 304, 'Arborio', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1656, 304, 'Husk', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1657, 304, 'Nimbus', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1658, 305, 'Chalk', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1659, 305, 'Mercury', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1660, 305, 'Lava', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1661, 305, 'Pebble', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1662, 306, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1663, 306, 'Pewter', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1664, 306, 'Steel', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1665, 306, 'Cream', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1666, 307, 'Silver', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1667, 307, 'Gold', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1668, 307, 'Copper', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1669, 308, 'Cotton', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1670, 308, 'Birch', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1671, 308, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1672, 308, 'Jasmine', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1673, 308, 'Blonde', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1674, 309, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1675, 309, 'Champagne', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1676, 309, 'Pannacotta', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1677, 309, 'Portobello', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1678, 309, 'Moonstone', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1679, 309, 'Mineral', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1680, 310, 'White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1681, 310, 'Cloud Grey', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1682, 310, 'Flint', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1683, 310, 'Parchment', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1684, 311, 'Frost', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1685, 311, 'Calico', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1686, 311, 'Gray Marl', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1687, 311, 'Bamboo', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1688, 312, 'Cotton', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1689, 312, 'Mist', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1690, 312, 'Slate', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1691, 312, 'Linen', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1692, 312, 'Hemp', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1693, 313, 'Ivory', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1694, 313, 'Mushroom', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1695, 313, 'Taupe', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1696, 314, 'Brazen Yellow', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1697, 314, 'Birch White', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1698, 314, 'Morning Mist', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1699, 315, 'Pastel', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1700, 316, 'Silver Birch', NULL, 2, 0, '2019-09-17', '2019-09-19', 0),
(1701, 322, 'White', '', 2, 0, '2019-09-17', '2019-09-19', 0),
(1702, 323, 'White', '', 2, 0, '2019-09-17', '2019-09-19', 0),
(1703, 324, 'white #11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1704, 324, 'Ivory #1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1705, 324, 'Beige #2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1706, 324, 'Grey #10', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1707, 324, 'Taupe #2', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1708, 324, 'black #7', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1709, 325, 'white', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1710, 325, 'Ivory', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1711, 325, 'Beige', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1712, 325, 'Grey', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1713, 325, 'Taupe', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1714, 325, 'Khaki', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1715, 326, 'silver lvory #11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1716, 326, 'silver wine #9', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1717, 326, 'gold beige #2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1718, 326, 'gold red #9', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1719, 327, 'Ivory #1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1720, 327, 'Silver Ivory #1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1721, 327, 'Silver White #11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1722, 327, 'White #11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1723, 328, 'white-11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1724, 328, 'lvory-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1725, 328, 'peach-2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1726, 328, 'mustard', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1727, 328, 'purple', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1728, 328, 'pink', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1729, 328, 'dark green', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1730, 328, 'blue', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1731, 328, 'grey', '9', 4, 0, '2019-09-17', '2019-09-19', 0),
(1732, 328, 'brown', '10', 4, 0, '2019-09-17', '2019-09-19', 0),
(1733, 328, 'caramel', '11', 4, 0, '2019-09-17', '2019-09-19', 0),
(1734, 328, 'yellow', '12', 4, 0, '2019-09-17', '2019-09-19', 0),
(1735, 328, 'mocha', '13', 4, 0, '2019-09-17', '2019-09-19', 0),
(1736, 328, 'black', '14', 4, 0, '2019-09-17', '2019-09-19', 0),
(1737, 328, 'deep green', '15', 4, 0, '2019-09-17', '2019-09-19', 0),
(1738, 328, 'deep blue', '16', 4, 0, '2019-09-17', '2019-09-19', 0),
(1739, 329, 'M/mushroom-2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1740, 329, 'M/khaki-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1741, 329, 'Brown-8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1742, 329, 'Nile-4', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1743, 329, 'Chocolate-8', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1744, 329, 'White-11', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1745, 329, 'M/pink-3', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1746, 329, 'M/green-4', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1747, 329, 'M/blue-5', '9', 4, 0, '2019-09-17', '2019-09-19', 0),
(1748, 329, 'Cherry-8', '10', 4, 0, '2019-09-17', '2019-09-19', 0),
(1749, 329, 'Mellow green-4', '11', 4, 0, '2019-09-17', '2019-09-19', 0),
(1750, 329, 'Grey-10', '12', 4, 0, '2019-09-17', '2019-09-19', 0),
(1751, 329, 'Black-7', '13', 4, 0, '2019-09-17', '2019-09-19', 0),
(1752, 329, 'Sand-2', '14', 4, 0, '2019-09-17', '2019-09-19', 0),
(1753, 329, 'Ash tree-10', '15', 4, 0, '2019-09-17', '2019-09-19', 0),
(1754, 329, 'Lilac-6', '16', 4, 0, '2019-09-17', '2019-09-19', 0),
(1755, 329, 'Peach-2', '17', 4, 0, '2019-09-17', '2019-09-19', 0),
(1756, 329, 'Turquiose-5', '18', 4, 0, '2019-09-17', '2019-09-19', 0),
(1757, 329, 'Wine-9', '19', 4, 0, '2019-09-17', '2019-09-19', 0),
(1758, 330, 'lvory-1', '101', 4, 0, '2019-09-17', '2019-09-19', 0),
(1759, 330, 'mushroom-2', '102', 4, 0, '2019-09-17', '2019-09-19', 0),
(1760, 330, 'ashtree-2', '103', 4, 0, '2019-09-17', '2019-09-19', 0),
(1761, 330, 'silver grey-10', '104', 4, 0, '2019-09-17', '2019-09-19', 0),
(1762, 330, 'grey-10', '105', 4, 0, '2019-09-17', '2019-09-19', 0),
(1763, 330, 'Chocolate-8', '106', 4, 0, '2019-09-17', '2019-09-19', 0),
(1764, 331, 'green-4', '301', 4, 0, '2019-09-17', '2019-09-19', 0),
(1765, 331, 'pink-3', '302', 4, 0, '2019-09-17', '2019-09-19', 0),
(1766, 331, 'blue-5', '303', 4, 0, '2019-09-17', '2019-09-19', 0),
(1767, 331, 'brown-8', '304', 4, 0, '2019-09-17', '2019-09-19', 0),
(1768, 331, 'black-7', '305', 4, 0, '2019-09-17', '2019-09-19', 0),
(1769, 331, 'indian pink-2', '306', 4, 0, '2019-09-17', '2019-09-19', 0),
(1770, 332, 'Tuape-2', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1771, 332, 'Beige-2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1772, 332, 'Gold-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1773, 332, 'Grey-10', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1774, 332, 'Brown-2', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1775, 332, 'Indian Pink-3', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1776, 332, 'Green-4', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1777, 332, 'Black Grey-7', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1778, 332, 'D/Brown-8', '9', 4, 0, '2019-09-17', '2019-09-19', 0),
(1779, 333, 'Ivory-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1780, 333, 'N.lvory-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1781, 333, 'N.pink-2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1782, 333, 'N.brown-2', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1783, 333, 'Coffee-8', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1784, 333, 'Purple-6', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1785, 333, 'Wine-9', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1786, 333, 'Monaco-8', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1787, 333, 'Grey-10', '9', 4, 0, '2019-09-17', '2019-09-19', 0),
(1788, 333, 'Black-7', '10', 4, 0, '2019-09-17', '2019-09-19', 0),
(1789, 333, 'Brown-8', '11', 4, 0, '2019-09-17', '2019-09-19', 0),
(1790, 333, 'Vanilla-2', '12', 4, 0, '2019-09-17', '2019-09-19', 0),
(1791, 333, 'Scarlet-2', '13', 4, 0, '2019-09-17', '2019-09-19', 0),
(1792, 333, 'Lime-4', '14', 4, 0, '2019-09-17', '2019-09-19', 0),
(1793, 333, 'Peach-3', '15', 4, 0, '2019-09-17', '2019-09-19', 0),
(1794, 333, 'Begie-2', '16', 4, 0, '2019-09-17', '2019-09-19', 0),
(1795, 333, 'Light pink-2', '17', 4, 0, '2019-09-17', '2019-09-19', 0),
(1796, 334, 'Green-4', '4101', 4, 0, '2019-09-17', '2019-09-19', 0),
(1797, 334, 'Brown-2', '4102', 4, 0, '2019-09-17', '2019-09-19', 0),
(1798, 334, 'Pink-3', '4103', 4, 0, '2019-09-17', '2019-09-19', 0),
(1799, 334, 'Blue-5', '4104', 4, 0, '2019-09-17', '2019-09-19', 0),
(1800, 334, 'Black-7', '4105', 4, 0, '2019-09-17', '2019-09-19', 0),
(1801, 334, 'Grey-10', '4106', 4, 0, '2019-09-17', '2019-09-19', 0),
(1802, 334, 'Indian Pink-2', '4107', 4, 0, '2019-09-17', '2019-09-19', 0),
(1803, 335, 'ice white-11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1804, 335, 'natural-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1805, 335, 'peach-2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1806, 335, 'green-4', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1807, 335, 'beige-2', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1808, 335, 'brown-8', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1809, 335, 'grey-10', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1810, 336, 'dark cream-2', '1801', 4, 0, '2019-09-17', '2019-09-19', 0),
(1811, 336, 'green-4', '1802', 4, 0, '2019-09-17', '2019-09-19', 0),
(1812, 336, 'pink-3', '1803', 4, 0, '2019-09-17', '2019-09-19', 0),
(1813, 336, 'blue-5', '1804', 4, 0, '2019-09-17', '2019-09-19', 0),
(1814, 336, 'brown-8', '1805', 4, 0, '2019-09-17', '2019-09-19', 0),
(1815, 336, 'chocolate-8', '1806', 4, 0, '2019-09-17', '2019-09-19', 0),
(1816, 336, 'sand-2', '1807', 4, 0, '2019-09-17', '2019-09-19', 0),
(1817, 336, 'grey-10', '1808', 4, 0, '2019-09-17', '2019-09-19', 0),
(1818, 336, 'dark green-4', '1809', 4, 0, '2019-09-17', '2019-09-19', 0),
(1819, 337, 'white-11', 'JM 1-1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1820, 337, 'ice beige-1', 'JM 1-2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1821, 337, 'sang-2', 'JM 1-3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1822, 337, 'olive gold-2', 'JM 1-4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1823, 337, 'light grey-10', 'JM 1-5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1824, 337, 'khakie-2', 'JM 1-6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1825, 337, 'dark grey-10', 'JM 1-7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1826, 338, 'lvory-1', 'EM 3-1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1827, 338, 'sand-1', 'EM 3-2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1828, 338, 'beige-2', 'EM 3-3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1829, 338, 'light grey-10', 'EM 3-4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1830, 338, 'brown-8', 'EM 3-5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1831, 338, 'wood-8', 'EM 3-6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1832, 338, 'dark grey-10', 'EM 3-7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1833, 339, 'beige-2', '4004', 4, 0, '2019-09-17', '2019-09-19', 0),
(1834, 339, 'grey-10', '4005', 4, 0, '2019-09-17', '2019-09-19', 0),
(1835, 339, 'wood-8', '4006', 4, 0, '2019-09-17', '2019-09-19', 0),
(1836, 339, 'wine-9', '4007', 4, 0, '2019-09-17', '2019-09-19', 0),
(1837, 339, 'ink black-7', '4008', 4, 0, '2019-09-17', '2019-09-19', 0),
(1838, 340, 'White-11', '7901', 4, 0, '2019-09-17', '2019-09-19', 0),
(1839, 340, 'lvory-1', '7902', 4, 0, '2019-09-17', '2019-09-19', 0),
(1840, 340, 'Beige-2', '7903', 4, 0, '2019-09-17', '2019-09-19', 0),
(1841, 340, 'dark beige-8', '7904', 4, 0, '2019-09-17', '2019-09-19', 0),
(1842, 340, 'Teak-8', '7905', 4, 0, '2019-09-17', '2019-09-19', 0),
(1843, 340, 'Chocolate-8', '7906', 4, 0, '2019-09-17', '2019-09-19', 0),
(1844, 340, 'Blue Grey-10', '7907', 4, 0, '2019-09-17', '2019-09-19', 0),
(1845, 340, 'Grey-10', '7908', 4, 0, '2019-09-17', '2019-09-19', 0),
(1846, 340, 'Dark Grey-10', '7909', 4, 0, '2019-09-17', '2019-09-19', 0),
(1847, 340, 'Black-7', '7910', 4, 0, '2019-09-17', '2019-09-19', 0),
(1848, 341, 'Cadet Blue', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1849, 341, 'Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1850, 341, 'Vanilla Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1851, 341, 'Mushroom', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1852, 341, 'Choclate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1853, 342, 'White-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1854, 342, 'Mushroom -2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1855, 342, 'Cherry -2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1856, 342, 'GREEN -4', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1857, 342, 'Chocolate -8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1858, 343, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1859, 343, 'Onion', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1860, 343, 'Beige-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1861, 343, 'Purple', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1862, 343, 'Pink', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1863, 344, 'lvory-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1864, 344, 'grey-10', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1865, 344, 'beige-2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1866, 344, 'brown-8', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1867, 344, 'dark grey-10', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1868, 344, 'chocolate-8', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1869, 345, 'white-11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1870, 345, 'lvory-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1871, 345, 'brown-8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1872, 345, 'light grey-10', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1873, 345, 'grey-10', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1874, 345, 'wine-9', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1875, 346, 'White', 'BA 6-1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1876, 346, 'Ivory', 'BA 6-2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1877, 346, 'Beige', 'BA 6-3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1878, 346, 'Vanilla', 'BA 6-4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1879, 346, 'Grey', 'BA 6-5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1880, 346, 'Dark Grey', 'BA 6-6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1881, 346, 'Brown', 'BA 6-7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1882, 346, 'Ink Blackout', 'BA 6-8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1883, 346, 'Charcoal', 'BA 6-9', 4, 0, '2019-09-17', '2019-09-19', 0),
(1884, 347, 'Cherry-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1885, 347, 'Khaki-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1886, 347, 'Green-4', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1887, 347, 'Lilac-6', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(1888, 347, 'Blue-5', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1889, 347, 'Pink-3', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1890, 347, 'Brown-8', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1891, 348, 'Beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1892, 348, 'Gold', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1893, 348, 'Indian Pink', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1894, 348, 'Green', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1895, 348, 'Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1896, 348, 'Dark Brown', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1897, 349, 'lvory-2', '802', 4, 0, '2019-09-17', '2019-09-19', 0),
(1898, 349, 'Vanilla-2', '803', 4, 0, '2019-09-17', '2019-09-19', 0),
(1899, 349, 'Peach-2', '804', 4, 0, '2019-09-17', '2019-09-19', 0),
(1900, 349, 'Beige-2', '805', 4, 0, '2019-09-17', '2019-09-19', 0),
(1901, 349, 'Grey-10', '806', 4, 0, '2019-09-17', '2019-09-19', 0),
(1902, 349, 'Dark Grey-10', '807', 4, 0, '2019-09-17', '2019-09-19', 0),
(1903, 349, 'Brown-8', '808', 4, 0, '2019-09-17', '2019-09-19', 0),
(1904, 349, 'Black-7', '809', 4, 0, '2019-09-17', '2019-09-19', 0),
(1905, 350, 'Beige-2', '5501', 4, 0, '2019-09-17', '2019-09-19', 0),
(1906, 350, 'Grey-10', '5502', 4, 0, '2019-09-17', '2019-09-19', 0),
(1907, 350, 'Teak-8', '5503', 4, 0, '2019-09-17', '2019-09-19', 0),
(1908, 350, 'Anthracite-10', '5504', 4, 0, '2019-09-17', '2019-09-19', 0),
(1909, 350, 'Chocolate-8', '5505', 4, 0, '2019-09-17', '2019-09-19', 0),
(1910, 350, 'Black-7', '5506', 4, 0, '2019-09-17', '2019-09-19', 0),
(1911, 351, 'ca-01-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1912, 351, 'ca-02-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1913, 351, 'ca-04-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1914, 351, 'ca-05-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1915, 351, 'ca-07-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1916, 351, 'ca-08-7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1917, 351, 'ca-09-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1918, 352, 'Rose-2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(1919, 352, 'Sandy-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(1920, 352, 'Lime-4', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(1921, 352, 'Peach-2', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(1922, 352, 'Lavender-2', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(1923, 352, 'Olive-4', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(1924, 352, 'Oak-8', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(1925, 353, 'Ivory-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1926, 353, 'Beige-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1927, 353, 'Green-4', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1928, 353, 'Light Grey-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1929, 353, 'Wood-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1930, 354, 'Brown-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1931, 354, 'Ivory-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1932, 354, 'Oil Green-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1933, 354, 'Dark Green-4', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1934, 354, 'Ink Black-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1935, 354, 'White-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1936, 354, 'Beige-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1937, 355, 'Chocolate chip-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1938, 355, 'Nougat-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1939, 355, 'Olive gold-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1940, 355, 'Ink black-7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1941, 355, 'White-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1942, 355, 'Grey-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1943, 355, 'Cream lvory-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1944, 355, 'Light Brown-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1945, 356, 'white-1', 'sm 3601', 4, 0, '2019-09-17', '2019-09-19', 0),
(1946, 356, 'lvory-2', 'sm 3602', 4, 0, '2019-09-17', '2019-09-19', 0),
(1947, 356, 'beige-2', 'sm 3603', 4, 0, '2019-09-17', '2019-09-19', 0),
(1948, 356, 'brown-8', 'sm 3604', 4, 0, '2019-09-17', '2019-09-19', 0),
(1949, 356, 'wood-7', 'sm 3605', 4, 0, '2019-09-17', '2019-09-19', 0),
(1950, 357, 'Rf-stat 1486-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1951, 357, 'Rf-stat 1422-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1952, 357, 'Rf-stat 1442-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1953, 357, 'Rf-stat 1448-7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1954, 357, 'Rf-stat 1433-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1955, 357, 'Rf-stat 1412-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1956, 357, 'Rf-stat 1445-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1957, 357, 'Rf-stat 1419-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1958, 358, 'RF-STD1443-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1959, 358, 'RF-STN 1431-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1960, 358, 'RF-STD1432-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1961, 358, 'RF-STN1434-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1962, 358, 'RF-STN1484-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1963, 359, 'FT-1101', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1964, 359, 'FT-1104', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1965, 359, 'FT-1105', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1966, 359, 'FT-1106', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1967, 359, 'FT-1108', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1968, 360, 'White-1', '101', 4, 0, '2019-09-17', '2019-09-19', 0),
(1969, 360, 'lvory-1', '102', 4, 0, '2019-09-17', '2019-09-19', 0),
(1970, 360, 'Gold-2', '103', 4, 0, '2019-09-17', '2019-09-19', 0),
(1971, 360, 'Silver Grey-10', '104', 4, 0, '2019-09-17', '2019-09-19', 0),
(1972, 360, 'Grey-10', '105', 4, 0, '2019-09-17', '2019-09-19', 0),
(1973, 360, 'Black-7', '106', 4, 0, '2019-09-17', '2019-09-19', 0),
(1974, 360, 'Coffee-8', '107', 4, 0, '2019-09-17', '2019-09-19', 0),
(1975, 361, 'VL-BA1313', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1976, 361, 'VL-BA2313', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1977, 361, 'VL-BAD4313', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1978, 361, 'VL-BAD4813', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1979, 361, 'VL-BA4513', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1980, 361, 'VL-BAN4913', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1981, 361, 'VL-BAN9513', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1982, 361, 'VL-BA9513', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1983, 362, 'White-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1984, 362, 'Ivory-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1985, 362, 'Mushroom-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1986, 362, 'L.Grey-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1987, 362, 'Choco Chip-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1988, 362, 'Grey-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1989, 362, 'Olive Gold-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1990, 362, 'Brown-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1991, 362, 'D.Beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1992, 362, 'Charcoal-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(1993, 363, 'White', '8501', 4, 0, '2019-09-17', '2019-09-19', 0),
(1994, 363, 'Beige', '8503', 4, 0, '2019-09-17', '2019-09-19', 0),
(1995, 363, 'grey-10', '8504', 4, 0, '2019-09-17', '2019-09-19', 0),
(1996, 363, 'Dark Grey', '8505', 4, 0, '2019-09-17', '2019-09-19', 0),
(1997, 363, 'Sand', '8506', 4, 0, '2019-09-17', '2019-09-19', 0),
(1998, 363, 'Cadet Blue', '8509', 4, 0, '2019-09-17', '2019-09-19', 0),
(1999, 364, 'Ashtree', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2000, 364, 'Tivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2001, 364, 'Beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2002, 364, 'Water Gren', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2003, 365, 'Ice white', '6501', 4, 0, '2019-09-17', '2019-09-19', 0),
(2004, 365, 'Ivory', '6503', 4, 0, '2019-09-17', '2019-09-19', 0),
(2005, 365, 'Beige', '6504', 4, 0, '2019-09-17', '2019-09-19', 0),
(2006, 365, 'Dark Beige', '6505', 4, 0, '2019-09-17', '2019-09-19', 0),
(2007, 365, 'Teak', '6506', 4, 0, '2019-09-17', '2019-09-19', 0),
(2008, 365, 'Chocolate', '6507', 4, 0, '2019-09-17', '2019-09-19', 0),
(2009, 365, 'Charcoal', '6511', 4, 0, '2019-09-17', '2019-09-19', 0),
(2010, 366, 'White-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2011, 366, 'Ivory-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2012, 366, 'Beige-1', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2013, 366, 'Light Gray-10', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2014, 366, 'Gray-10', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2015, 366, 'Brown-8', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2016, 366, 'Chocolate-8', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(2017, 367, 'White-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2018, 367, 'Ivory-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2019, 367, 'Gray-10', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2020, 367, 'Beige-2', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2021, 367, 'Brown-8', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2022, 367, 'Chocolate-8', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2023, 368, 'OR-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2024, 368, 'OR-10', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2025, 369, 'He-2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2026, 369, 'He-10', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2027, 370, 'Chocolate-8', '7509', 4, 0, '2019-09-17', '2019-09-19', 0),
(2028, 370, 'White-1', '7501', 4, 0, '2019-09-17', '2019-09-19', 0),
(2029, 370, 'Ivory-1', '7502', 4, 0, '2019-09-17', '2019-09-19', 0),
(2030, 370, 'beige-2', '7503', 4, 0, '2019-09-17', '2019-09-19', 0),
(2031, 370, 'Green-4', '7504', 4, 0, '2019-09-17', '2019-09-19', 0),
(2032, 370, 'Pink-3', '7505', 4, 0, '2019-09-17', '2019-09-19', 0),
(2033, 370, 'Grey-10', '7506', 4, 0, '2019-09-17', '2019-09-19', 0),
(2034, 370, 'Dark Grey-10', '7507', 4, 0, '2019-09-17', '2019-09-19', 0),
(2035, 370, 'Brown-8', '7508', 4, 0, '2019-09-17', '2019-09-19', 0),
(2036, 371, 'White', '6201', 4, 0, '2019-09-17', '2019-09-19', 0),
(2037, 371, 'Ivory', '6202', 4, 0, '2019-09-17', '2019-09-19', 0),
(2038, 371, 'Beige', '6203', 4, 0, '2019-09-17', '2019-09-19', 0),
(2039, 371, 'Khaki', '6204', 4, 0, '2019-09-17', '2019-09-19', 0),
(2040, 371, 'Cherry', '6205', 4, 0, '2019-09-17', '2019-09-19', 0),
(2041, 371, 'Grey', '6207', 4, 0, '2019-09-17', '2019-09-19', 0),
(2042, 371, 'Dark Grey', '6208', 4, 0, '2019-09-17', '2019-09-19', 0),
(2043, 371, 'Caramel', '6210', 4, 0, '2019-09-17', '2019-09-19', 0),
(2044, 372, 'White-1', '2201', 4, 0, '2019-09-17', '2019-09-19', 0),
(2045, 372, 'lvory-2', '2202', 4, 0, '2019-09-17', '2019-09-19', 0),
(2046, 372, 'Vanilla-2', '2203', 4, 0, '2019-09-17', '2019-09-19', 0),
(2047, 372, 'Beige-2', '2204', 4, 0, '2019-09-17', '2019-09-19', 0),
(2048, 372, 'Grey-10', '2205', 4, 0, '2019-09-17', '2019-09-19', 0),
(2049, 372, 'Dark grey-10', '2206', 4, 0, '2019-09-17', '2019-09-19', 0),
(2050, 372, 'Brown-8', '2207', 4, 0, '2019-09-17', '2019-09-19', 0),
(2051, 372, 'Oak-2', '2210', 4, 0, '2019-09-17', '2019-09-19', 0),
(2052, 372, '2209 Black-7', '2209', 4, 0, '2019-09-17', '2019-09-19', 0),
(2053, 373, 'White-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2054, 373, 'Beige-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2055, 373, 'Brown-8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2056, 373, 'Coconut-8', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2057, 373, 'Grey-10', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2058, 373, 'Charcoal-7', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2059, 374, 'Ivory-2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2060, 374, 'Choco-8', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2061, 374, 'Brown-2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2062, 374, 'W/Brown-1', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2063, 374, 'I/Choco-7', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2064, 375, 'Os-1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2065, 375, 'Os-2', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2066, 375, 'Os-10', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(2067, 375, 'Os-8', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(2068, 375, 'Os-7', '10', 4, 0, '2019-09-17', '2019-09-19', 0),
(2069, 376, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2070, 376, 'Light Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2071, 376, 'Dark Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2072, 376, 'Yellow Green', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2073, 376, 'Blue-5', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2074, 376, 'green #4', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2075, 376, 'Gold', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2076, 376, 'Choco', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2077, 376, 'Wine', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2078, 377, 'PR-2', '101', 4, 0, '2019-09-17', '2019-09-19', 0),
(2079, 377, 'PR-10', '102', 4, 0, '2019-09-17', '2019-09-19', 0),
(2080, 378, '002 WHITE #1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2081, 378, '002 ANTIQUE WHITE #2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2082, 378, '003 brown olive sheen #8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2083, 378, '004 pin bark #8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2084, 378, '005 penguin #8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2085, 378, '006 caster grey #7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2086, 378, '007 cadet blue #5', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2087, 378, '008 Grape Royate #7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2088, 379, 'White-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2089, 379, 'Gold-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2090, 379, 'Khaki-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2091, 379, 'Black-7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2092, 380, 'Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2093, 380, 'Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2094, 380, 'Beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2095, 381, 'MO-2', '103', 4, 0, '2019-09-17', '2019-09-19', 0),
(2096, 381, 'MO-10', '104', 4, 0, '2019-09-17', '2019-09-19', 0),
(2097, 382, 'White-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2098, 382, 'Ivory-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2099, 382, 'Blue-5', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2100, 382, 'Green-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2101, 382, 'Gold-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2102, 382, 'Brown-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2103, 382, 'Wine-9', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2104, 382, 'Grey-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2105, 382, 'Black-7', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2106, 383, 'White', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2107, 383, 'Sand', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2108, 383, 'Land', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2109, 383, 'Saddle Brown', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2110, 383, 'Ruby', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2111, 384, 'lvory #2', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2112, 384, 'beige #2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2113, 384, 'peach #8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2114, 384, 'purple #6', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2115, 385, 'Grey-10', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2116, 385, 'Charcoal-8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2117, 385, 'Coconut-2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2118, 385, 'Graphite-7', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2119, 386, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2120, 386, 'Grey', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2121, 386, 'beige', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2122, 386, 'Coffee', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2123, 386, 'D.Brown', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2124, 386, 'Water Melon', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2125, 386, 'Wine', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(2126, 386, 'Charcoal', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(2127, 386, 'L/Green', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2128, 386, 'Pink', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2129, 386, 'D/Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2130, 386, 'Navy', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2131, 386, 'Sky Blue', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2132, 387, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2133, 388, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2134, 389, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2135, 390, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2136, 391, 'white #11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2137, 391, 'mushroom #1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2138, 391, 'Sand #2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2139, 391, 'grey #10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2140, 391, 'chocolate #8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2141, 392, 'White', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2142, 392, 'Ivory', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2143, 392, 'Green', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2144, 392, 'Blue', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2145, 392, 'Grey', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2146, 393, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2147, 393, 'Pink', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2148, 393, 'Grey', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2149, 393, 'Brown', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2150, 394, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2151, 394, 'Yellow Green', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2152, 394, 'Brown', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2153, 394, 'Blue', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2154, 336, 'L.Beige', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2155, 336, 'Blue', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2156, 336, 'Grey', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2157, 336, 'L.Green', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2158, 395, 'Mushroom', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2159, 395, 'Grey', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2160, 395, 'Blue', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2161, 395, 'Brown', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2162, 396, 'Yellow', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2163, 396, 'Khaki', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2164, 396, 'Red Wine', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2165, 396, 'Purple', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2166, 397, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2167, 397, 'Yellow', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2168, 397, 'Grey', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2169, 397, 'Blue', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2170, 398, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2171, 398, 'Khaki', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2172, 398, 'Beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2173, 398, 'Nile', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2174, 398, 'Brown', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2175, 398, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2176, 398, 'Mushroom', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2177, 398, 'Chocolate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2178, 398, 'Black', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2179, 399, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2180, 399, 'Beige', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2181, 399, 'Green', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2182, 399, 'Grey', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2183, 399, 'Blue', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2184, 400, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2185, 400, 'L/Green', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2186, 400, 'Brown', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2187, 400, 'Blue', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2188, 400, 'Green', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2189, 400, 'OZ', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2190, 401, 'Mushroom', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2191, 401, 'Yellow', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2192, 401, 'Brown', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2193, 402, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2194, 402, 'Beige', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2195, 402, 'Green', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2196, 402, 'Grey', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2197, 402, 'Choco', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2198, 402, 'Black', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2199, 403, 'White #11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2200, 403, 'Ivory #1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2201, 403, 'Lemon #2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2202, 403, 'Green #4', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2203, 403, 'Pink #3', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2204, 403, 'Blue #5', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2205, 403, 'Grey #10', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(2206, 404, 'Ivory #1', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2207, 404, 'L/Green #2', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2208, 404, 'Brown #8', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2209, 404, 'Blue #5', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2210, 405, 'Ivory', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2211, 405, 'Grey', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2212, 405, 'beige', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2213, 405, 'Coffee', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2214, 405, 'D.Brown', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2215, 405, 'Water Melon', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2216, 405, 'Wine', '7', 4, 0, '2019-09-17', '2019-09-19', 0),
(2217, 405, 'Charcoal', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(2218, 405, 'L/Green', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2219, 405, 'Pink', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2220, 405, 'D/Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2221, 405, 'Navy', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2222, 405, 'Sky Blue', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2223, 406, 'white', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2224, 406, 'lvory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2225, 406, 'beige', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2226, 406, 'coconut', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2227, 406, 'khaki', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2228, 407, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2229, 407, 'Off White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2230, 407, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2231, 407, 'Sand', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2232, 407, 'Mocha', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2233, 407, 'Sone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2234, 408, 'White', '20301', 4, 0, '2019-09-17', '2019-09-19', 0),
(2235, 408, 'Ivory', '20302', 4, 0, '2019-09-17', '2019-09-19', 0),
(2236, 408, 'Grey', '20303', 4, 0, '2019-09-17', '2019-09-19', 0),
(2237, 408, 'Brown', '20304', 4, 0, '2019-09-17', '2019-09-19', 0),
(2238, 408, 'Dark Blue', '20305', 4, 0, '2019-09-17', '2019-09-19', 0),
(2239, 409, 'White', '20501', 4, 0, '2019-09-17', '2019-09-19', 0),
(2240, 409, 'Mushroom', '20502', 4, 0, '2019-09-17', '2019-09-19', 0),
(2241, 409, 'Grey', '20503', 4, 0, '2019-09-17', '2019-09-19', 0),
(2242, 409, 'Khaki', '20504', 4, 0, '2019-09-17', '2019-09-19', 0),
(2243, 410, 'Snow White #11', '20101', 4, 0, '2019-09-17', '2019-09-19', 0),
(2244, 410, 'Oyster White #1', '20102', 4, 0, '2019-09-17', '2019-09-19', 0),
(2245, 410, 'Marzipan', '20103', 4, 0, '2019-09-17', '2019-09-19', 0),
(2246, 410, 'Chanterelle', '20104', 4, 0, '2019-09-17', '2019-09-19', 0),
(2247, 410, 'Wood Smoke', '20105', 4, 0, '2019-09-17', '2019-09-19', 0),
(2248, 410, 'Bright White', '20106', 4, 0, '2019-09-17', '2019-09-19', 0),
(2249, 410, 'Barely Blue', '20107', 4, 0, '2019-09-17', '2019-09-19', 0),
(2250, 410, 'Fint Grey', '20108', 4, 0, '2019-09-17', '2019-09-19', 0),
(2251, 410, 'China Blue', '20109', 4, 0, '2019-09-17', '2019-09-19', 0),
(2252, 410, 'Black Coffee', '20110', 4, 0, '2019-09-17', '2019-09-19', 0),
(2253, 410, 'Bitter Chocolate', '20111', 4, 0, '2019-09-17', '2019-09-19', 0),
(2254, 410, 'Twilight Blue', '20112', 4, 0, '2019-09-17', '2019-09-19', 0),
(2255, 410, 'Black', '20113', 4, 0, '2019-09-17', '2019-09-19', 0),
(2256, 411, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2257, 411, 'White Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2258, 411, 'Ebony Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2259, 411, 'Ebony', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2260, 411, 'White Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2261, 411, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2262, 411, 'Taupe', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2263, 411, 'Chestnut', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2264, 411, 'Brown Chocolate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2265, 412, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2266, 412, 'White Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2267, 412, 'Ebony Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2268, 412, 'Ebony', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2269, 412, 'White Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2270, 412, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2271, 412, 'Taupe', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2272, 412, 'Chestnut', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2273, 412, 'Brown Chocolate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2274, 413, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2275, 413, 'White Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2276, 413, 'Ebony Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2277, 413, 'Ebony', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2278, 413, 'White Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2279, 413, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2280, 413, 'Taupe', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2281, 413, 'Chestnut', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2282, 413, 'Brown Chocolate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2283, 414, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2284, 414, 'Milk', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2285, 414, 'Peach', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2286, 414, 'Silver Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2287, 414, 'Shark', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2288, 415, 'White-11', '60751', 4, 0, '2019-09-17', '2019-09-19', 0),
(2289, 415, 'Cream-1', '60752', 4, 0, '2019-09-17', '2019-09-19', 0),
(2290, 415, 'Beige-2', '60753', 4, 0, '2019-09-17', '2019-09-19', 0),
(2291, 415, 'Latte-8', '60755', 4, 0, '2019-09-17', '2019-09-19', 0),
(2292, 415, 'Shark-10', '60756', 4, 0, '2019-09-17', '2019-09-19', 0),
(2293, 415, 'Silver Grey-2', '60754', 4, 0, '2019-09-17', '2019-09-19', 0),
(2294, 415, 'Coffee-8', '60758', 4, 0, '2019-09-17', '2019-09-19', 0),
(2295, 415, 'Black-7', '60759', 4, 0, '2019-09-17', '2019-09-19', 0),
(2296, 416, 'White-11', 'D7513', 4, 0, '2019-09-17', '2019-09-19', 0),
(2297, 416, 'Cream Ivory-2', 'D7520', 4, 0, '2019-09-17', '2019-09-19', 0),
(2298, 416, 'Ivory-1', 'D7521', 4, 0, '2019-09-17', '2019-09-19', 0),
(2299, 416, 'Cream-2', 'D7531', 4, 0, '2019-09-17', '2019-09-19', 0),
(2300, 416, 'Natural Beige-10', 'D7532', 4, 0, '2019-09-17', '2019-09-19', 0),
(2301, 416, 'Natural Grey-10', 'D7541', 4, 0, '2019-09-17', '2019-09-19', 0),
(2302, 416, 'Natura D/Grey-10', 'D7542', 4, 0, '2019-09-17', '2019-09-19', 0),
(2303, 416, 'Charcoal-7', 'D7546', 4, 0, '2019-09-17', '2019-09-19', 0),
(2304, 416, 'Blue Willow-11', 'D7556', 4, 0, '2019-09-17', '2019-09-19', 0),
(2305, 417, 'lvory-1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2306, 417, 'dark lvory-10', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2307, 417, 'grey-10', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2308, 417, 'beige-2', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2309, 417, 'brown-8', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2310, 417, 'pink-3', '8', 4, 0, '2019-09-17', '2019-09-19', 0),
(2311, 418, 'w-3-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2312, 418, 'w-7-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2313, 418, 'w-20-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2314, 418, 'w-32-11', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2315, 419, 'light carmel -2', '250', 4, 0, '2019-09-17', '2019-09-19', 0),
(2316, 419, 'greyapricot-10', 'k168', 4, 0, '2019-09-17', '2019-09-19', 0),
(2317, 419, 'brown-8', 'k192', 4, 0, '2019-09-17', '2019-09-19', 0),
(2318, 419, 'light apricot-2', 'k157', 4, 0, '2019-09-17', '2019-09-19', 0),
(2319, 419, 'dark camel-8', '249', 4, 0, '2019-09-17', '2019-09-19', 0),
(2320, 420, 'lvory-1', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2321, 420, 'beige-2', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2322, 420, 'light grey-10', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2323, 420, 'brown-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2324, 420, 'chocolate-8', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2325, 421, 'white #11', '1', 4, 0, '2019-09-17', '2019-09-19', 0),
(2326, 421, 'mushroom #1', '2', 4, 0, '2019-09-17', '2019-09-19', 0),
(2327, 421, 'cherry #2', '3', 4, 0, '2019-09-17', '2019-09-19', 0),
(2328, 421, 'brown #8', '4', 4, 0, '2019-09-17', '2019-09-19', 0),
(2329, 421, 'chocolate #8', '5', 4, 0, '2019-09-17', '2019-09-19', 0),
(2330, 421, 'green #4', '6', 4, 0, '2019-09-17', '2019-09-19', 0),
(2331, 422, 'Cashmere', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2332, 422, 'Silver', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2333, 422, 'Charcoal', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2334, 422, 'Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2335, 423, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2336, 423, 'Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2337, 423, 'Oyster', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2338, 423, 'Gold', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2339, 423, 'Copper', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2340, 423, 'Emerald', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2341, 423, 'Ice Blue', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2342, 424, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2343, 425, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2344, 425, 'Charcoal', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2345, 425, 'Oatmeal', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2346, 425, 'Parchment', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2347, 426, 'Midnight', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2348, 426, 'Daybreak', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2349, 427, 'Jade', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2350, 427, 'Ochre', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2351, 428, 'China White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2352, 428, 'Luna', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2353, 428, 'Shadow', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2354, 428, 'Raven', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2355, 428, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2356, 428, 'Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2357, 428, 'Ecru', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2358, 428, 'Taupe', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2359, 428, 'Papaya', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2360, 428, 'Clay', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2361, 429, 'Stone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2362, 429, 'Platinum', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2363, 429, 'Anchor', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2364, 429, 'Shell', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2365, 429, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2366, 429, 'Flax', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2367, 430, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2368, 430, 'Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2369, 430, 'Black', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2370, 431, 'Serenity', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2371, 431, 'Kimono', NULL, 4, 0, '2019-09-17', '2019-09-19', 0);
INSERT INTO `color_tbl` (`id`, `pattern_id`, `color_name`, `color_number`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(2372, 431, 'Geisha', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2373, 432, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2374, 432, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2375, 432, 'Dove', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2376, 432, 'Charcoal', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2377, 433, 'Diamon Dust', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2378, 433, 'Moonlight Mink', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2379, 433, 'Lily Pad', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2380, 433, 'Champagne Fizz', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2381, 433, 'Antique Gold', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2382, 434, 'Ocra', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2383, 434, 'Lago', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2384, 434, 'Alba', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2385, 435, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2386, 436, 'Whistler White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2387, 436, 'Chateaux Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2388, 436, 'London Slate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2389, 436, 'Nordic Mist', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2390, 436, 'Cornsih Blue', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2391, 437, 'Pearl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2392, 437, 'Champagne', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2393, 437, 'Silver', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2394, 438, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2395, 439, 'Silver Sand', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2396, 439, 'Picket Fence', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2397, 439, 'Beach Cove', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2398, 439, 'Boat House', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2399, 439, 'Mahogany Brown', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2400, 439, 'Sea Breeze', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2401, 440, 'Greystone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2402, 440, 'Chalk', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2403, 440, 'Sandstone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2404, 440, 'Limestone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2405, 441, 'White Ash', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2406, 441, 'Magnolia', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2407, 441, 'Willow', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2408, 441, 'Silver Birch', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2409, 442, 'Marine', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2410, 443, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2411, 443, 'Birch', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2412, 443, 'Luna', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2413, 443, 'Papaya', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2414, 444, 'Pipin', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2415, 444, 'Inky', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2416, 444, 'Rosa', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2417, 445, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2418, 445, 'Angora', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2419, 445, 'Maize', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2420, 445, 'Truffle', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2421, 446, 'Silver', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2422, 447, 'Nightingale', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2423, 447, 'Dove', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2424, 447, 'Lark', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2425, 448, 'Smoke Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2426, 448, 'Nimbus Cloud', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2427, 448, 'Desert Sand', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2428, 448, 'Blue Haze', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2429, 449, 'Ice', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2430, 449, 'Onyx', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2431, 449, 'Opal', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2432, 449, 'Sand', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2433, 449, 'Mineral', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2434, 450, 'Shale', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2435, 450, 'Marble', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2436, 450, 'Sand', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2437, 451, 'Frost', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2438, 451, 'Haze', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2439, 451, 'Mist', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2440, 451, 'Fog', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2441, 451, 'Cloud', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2442, 451, 'Storm', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2443, 452, 'Frost', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2444, 452, 'Cloud', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2445, 452, 'Fog', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2446, 452, 'Storm', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2447, 452, 'Haze', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2448, 452, 'Mist', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2449, 453, 'Glow', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2450, 454, 'Graphite', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2451, 454, 'Birch', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2452, 454, 'Bamboo', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2453, 454, 'Maple', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2454, 454, 'Jasper', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2455, 454, 'Hazel', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2456, 455, 'Pebble', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2457, 455, 'Bamboo', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2458, 455, 'Wicker', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2459, 455, 'Rice', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2460, 455, 'Driftwood', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2461, 456, 'Feather', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2462, 456, 'Cinder', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2463, 456, 'Arborio', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2464, 456, 'Husk', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2465, 456, 'Nimbus', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2466, 457, 'Chalk', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2467, 457, 'Mercury', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2468, 457, 'Lava', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2469, 457, 'Pebble', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2470, 458, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2471, 458, 'Pewter', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2472, 458, 'Steel', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2473, 458, 'Cream', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2474, 459, 'Silver', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2475, 459, 'Gold', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2476, 459, 'Copper', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2477, 460, 'Cotton', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2478, 460, 'Birch', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2479, 460, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2480, 460, 'Jasmine', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2481, 460, 'Blonde', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2482, 461, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2483, 461, 'Champagne', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2484, 461, 'Pannacotta', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2485, 461, 'Portobello', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2486, 461, 'Moonstone', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2487, 461, 'Mineral', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2488, 462, 'White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2489, 462, 'Cloud Grey', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2490, 462, 'Flint', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2491, 462, 'Parchment', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2492, 463, 'Frost', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2493, 463, 'Calico', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2494, 463, 'Gray Marl', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2495, 463, 'Bamboo', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2496, 464, 'Cotton', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2497, 464, 'Mist', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2498, 464, 'Slate', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2499, 464, 'Linen', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2500, 464, 'Hemp', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2501, 465, 'Ivory', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2502, 465, 'Mushroom', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2503, 465, 'Taupe', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2504, 466, 'Brazen Yellow', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2505, 466, 'Birch White', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2506, 466, 'Morning Mist', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2507, 467, 'Pastel', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2508, 468, 'Silver Birch', NULL, 4, 0, '2019-09-17', '2019-09-19', 0),
(2543, 469, 'silver', 'silver', 8, 0, '2019-09-19', '0000-00-00', 0),
(2544, 470, 'silver', 'silver', 12, 0, '2019-09-20', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_profile`
--

CREATE TABLE `company_profile` (
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms_notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inches',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `setting_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company_profile`
--

INSERT INTO `company_profile` (`company_id`, `user_id`, `company_name`, `email`, `phone`, `picture`, `email_notification`, `sms_notification`, `currency`, `unit`, `logo`, `address`, `city`, `state`, `zip_code`, `country_code`, `created_by`, `created_at`, `updated_by`, `updated_date`, `status`, `setting_status`) VALUES
(20, 3, 'Legacy Blinds', 'info@legacyblinds.com', '+1 (972) 820-0007', NULL, '2020101', '2020101', '$', 'inches', '1510f853fdc0b3a806c4e6f8eb6920f0.png', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 2, '2019-09-16', 3, NULL, 1, 1),
(21, 3, 'Legacy Blinds', 'info@legacyblinds.com', '+1 (972) 820-0007', NULL, NULL, NULL, NULL, 'inches', NULL, '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 2, '2019-09-17', NULL, NULL, 1, 0),
(22, 2, 'BMS Window Decor', 'info@bmsdecor.com', '(972) 820-0007', NULL, NULL, NULL, '$', 'inches', 'BMS-Logos_Decor-150.png', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', 2, NULL, 2, NULL, 1, 0),
(23, 4, 'HANA', 'shesdeco@gmail.com', '(031) 293-6656', NULL, NULL, NULL, '₩', 'cm', 'hana.jpg', '152-4 Deck-ri', 'Hwaseong-si', 'KR', '445-894', 'KR', 4, NULL, 4, NULL, 1, 0),
(24, 12, 'it', 'cust_test@mailinator.com', '123456789', NULL, NULL, NULL, NULL, 'inches', NULL, 'India', 'Wadgaon', 'MH', NULL, 'IN', 0, '2019-09-20', NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_commet_tbl`
--

CREATE TABLE `customer_commet_tbl` (
  `id` int(11) NOT NULL,
  `comment_from` int(11) NOT NULL,
  `comment_to` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `is_visited` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_file_tbl`
--

CREATE TABLE `customer_file_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_user_id` int(11) NOT NULL,
  `file_upload` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `customer_id` int(11) NOT NULL,
  `customer_user_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qb_cust_id` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_customer_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `customer_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'business=c,personal=d',
  `side_mark` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL COMMENT 'Only  level id',
  `now_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL COMMENT 'C level user ID',
  `create_date` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` date NOT NULL,
  `package_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`customer_id`, `customer_user_id`, `qb_cust_id`, `customer_no`, `company_customer_id`, `first_name`, `last_name`, `company`, `street_no`, `address`, `city`, `state`, `zip_code`, `country_code`, `phone`, `email`, `reference`, `customer_type`, `side_mark`, `level_id`, `now_status`, `created_by`, `create_date`, `update_by`, `update_date`, `package_id`) VALUES
(1, '3', NULL, 'CUS-0002-James Park', 'LEG-1', 'James', 'Park', 'Legacy Blinds', '1000', '1000 Crowley Drive', 'Carrollton', 'TX', NULL, 'US', '+1 (972) 820-0007', 'info@legacyblinds.com', '', 'business', 'James-1000', 2, NULL, '2', '2019-09-17', 0, '0000-00-00', 3),
(2, '12', NULL, 'CUS-0003-cust test', 'IT-2', 'cust', 'test', 'it', 'India', 'India', 'Wadgaon', 'MH', NULL, 'IN', '123456789', 'cust_test@mailinator.com', '', 'business', 'cust-India', 2, NULL, '2', '2019-09-20', 0, '0000-00-00', 3),
(3, NULL, NULL, 'CUS-0004-caption america', 'IT-3', 'caption', 'america', 'it', 'Ingolstadt', 'Ingolstadt', 'Ingolstadt', 'BY', '85049', 'DE', '+1 (123) 456-7891', 'caption@mailinator.com', '', 'personal', 'caption-Ingolstadt', 12, NULL, '12', '2019-09-20', 0, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_phone_type_tbl`
--

CREATE TABLE `customer_phone_type_tbl` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_user_id` int(11) NOT NULL,
  `phone_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_phone_type_tbl`
--

INSERT INTO `customer_phone_type_tbl` (`id`, `customer_id`, `customer_user_id`, `phone_type`, `phone`) VALUES
(42, 34, 3, 'Phone', '+1 (972) 820-0007'),
(44, 1, 3, 'Phone', '+1 (972) 820-0007'),
(46, 3, 0, 'Mobile', '+1 (123) 456-7891'),
(48, 2, 12, 'Mobile', '123456789');

-- --------------------------------------------------------

--
-- Table structure for table `customer_query`
--

CREATE TABLE `customer_query` (
  `row_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `c_level_id` int(11) NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `create_date` date NOT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_email_tbl`
--

CREATE TABLE `custom_email_tbl` (
  `id` int(11) NOT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_email_tbl`
--

INSERT INTO `custom_email_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(0, 'info@bmsdecor.com', 'jaipal@pataak.com', 'Mail configuration verification', '2019-09-17 21:42:56', 2);

-- --------------------------------------------------------

--
-- Table structure for table `custom_sms_tbl`
--

CREATE TABLE `custom_sms_tbl` (
  `id` int(11) NOT NULL,
  `from` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_sms_tbl`
--

INSERT INTO `custom_sms_tbl` (`id`, `from`, `to`, `message`, `created_date`, `created_by`) VALUES
(11, '+14694051516', '+12148370214', 'Thanks for SMS Verified', '2019-09-17 21:45:09', 2);

-- --------------------------------------------------------

--
-- Table structure for table `c_card_info`
--

CREATE TABLE `c_card_info` (
  `id` int(11) NOT NULL,
  `card_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `card_holder` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `c_cost_factor_tbl`
--

CREATE TABLE `c_cost_factor_tbl` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `individual_cost_factor` float NOT NULL,
  `costfactor_discount` float DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `c_level_upcharges`
--

CREATE TABLE `c_level_upcharges` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `created_by` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `value` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_manufactur`
--

CREATE TABLE `c_manufactur` (
  `id` int(11) NOT NULL,
  `order_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `cancel_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `synk_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_manufactur`
--

INSERT INTO `c_manufactur` (`id`, `order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `cancel_comment`, `level_id`, `order_date`, `created_by`, `updated_by`, `synk_status`, `created_date`, `updated_date`) VALUES
(1, 'itit-capt-Ingo-001', 3, 'caption-Ingolstadt', '', 'assets/barcode/c/itit-capt-Ingo-001.jpg', '0', '', 0, 0, 0, 0, 0, '0', 121, 121, 0, 121, 0, '', 4, NULL, 12, '2019-09-20', 12, 0, 0, '2019-09-20', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `c_manufactur_details`
--

CREATE TABLE `c_manufactur_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `list_price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `unit_total_price` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_manufactur_details`
--

INSERT INTO `c_manufactur_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `list_price`, `discount`, `unit_total_price`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`, `created_status`) VALUES
(1, 'itit-capt-Ingo-001', 'Family', '55', 9, 1, 121, 0, 121, 470, 2544, 2, 4, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `c_menusetup_tbl`
--

CREATE TABLE `c_menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(3) NOT NULL COMMENT '1 = left, 2 = system',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `c_menusetup_tbl`
--

INSERT INTO `c_menusetup_tbl` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 1, 'customer', 'customer', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 12, 2, '2018-12-12 06:14:46'),
(2, 2, 'add_customer', 'add_customer', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 12, 2, '2018-12-12 06:15:26'),
(3, 3, 'customer_list', 'customer_list', 'customer-list-filter', 'customer', 2, 1, 1, 0, '', 1, 12, 2, '2018-12-12 06:16:01'),
(4, 4, 'order', 'order', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 12, 2, '2018-12-12 06:16:35'),
(5, 5, 'new_order', 'new_order', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 12, 48, '2018-12-12 06:17:40'),
(6, 6, 'manage_order', 'manage_order', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 12, 2, '2018-12-12 06:18:26'),
(7, 7, 'manage_invoice', 'manage_invoice', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 12, 2, '2018-12-12 06:18:56'),
(8, 8, 'cancelled_order', 'cancelled_order', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 12, 53, '2018-12-12 06:19:39'),
(9, 9, 'customer_return', 'customer_return', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 12, 2, '2018-12-12 06:23:22'),
(10, 10, 'track_order', 'track_order', 'track-order', 'order', 7, 0, 1, 0, '', 1, 12, 2, '2018-12-12 06:23:48'),
(11, 11, 'accounts', 'accounts', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 12, 2, '2018-12-12 06:24:27'),
(12, 12, 'chart_of_account', 'chart_of_account', 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 12, 2, '2018-12-12 06:25:02'),
(13, 13, 'vouchers', 'vouchers', '', 'account', NULL, 11, 1, 0, NULL, 1, 12, 2, '2018-12-12 06:25:40'),
(14, 14, 'debit_voucher', 'debit_voucher', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 12, 2, '2018-12-12 06:27:09'),
(15, 15, 'credit_voucher', 'credit_voucher', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 12, 2, '2018-12-12 06:27:42'),
(16, 16, 'journal_voucher', 'journal_voucher', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 12, 2, '2018-12-12 06:28:40'),
(17, 17, 'contra_voucher', 'contra_voucher', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 12, 2, '2018-12-12 06:29:13'),
(18, 18, 'voucher_approval', 'voucher_approval', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 12, 2, '2018-12-12 06:29:45'),
(19, 19, 'account_report', 'account_report', '', 'account', NULL, 11, 1, 0, NULL, 1, 12, 2, '2018-12-12 06:31:35'),
(20, 20, 'cash_book', 'cash_book', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:32:03'),
(21, 21, 'bank_book', 'bank_book', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:32:32'),
(22, 22, 'cash_flow', 'cash_flow', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:32:54'),
(23, 23, 'voucher_reports', 'voucher_reports', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:33:23'),
(24, 24, 'general_ledger', 'general_ledger', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:33:54'),
(25, 25, 'profit_or_loss', 'profit_or_loss', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:34:26'),
(26, 26, 'trial_balance', 'trial_balance', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 12, 2, '2018-12-12 06:34:58'),
(27, 27, 'settings', 'settings', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 12, 2, '2019-03-14 08:51:22'),
(28, 28, 'company_profile', 'company_profile', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:53:27'),
(29, 29, 'paypal_setting', 'paypal_setting', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:54:23'),
(30, 30, 'cost_factor', 'cost_factor', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:55:09'),
(31, 31, 'web_iframe_code', 'web_iframe_code', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:55:51'),
(32, 32, 'employee_and_access', 'employee_and_access', '', 'settings', 7, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:56:43'),
(33, 33, 'users', 'users', 'users', 'settings', 1, 32, 1, 0, '', 1, 12, 2, '2019-03-14 08:57:12'),
(34, 34, 'menu_customization', 'menu_customization', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 12, 2, '2019-03-14 08:57:51'),
(35, 35, 'role_permission', 'role_permission', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 12, 2, '2019-03-14 08:58:37'),
(36, 36, 'role_list', 'role_list', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 12, 2, '2019-03-14 08:59:59'),
(37, 37, 'add_user_role', 'add_user_role', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 12, 2, '2019-03-14 09:00:51'),
(38, 38, 'access_role', 'access_role', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 12, 2, '2019-03-14 09:01:32'),
(39, 39, 'sales_tax', 'sales_tax', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 12, 69, '2019-03-14 09:02:19'),
(40, 40, 'bulk_upload', 'bulk_upload', 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 12, 2, '2019-03-20 09:03:23'),
(41, 41, 'quotation', 'quotation', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 12, 2, '2019-04-25 14:42:49'),
(42, 42, 'add_new_quotation', 'add_new_quotation', 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 12, 2, '2019-04-25 14:44:40'),
(43, 43, 'manage_quotation', 'manage_quotation', 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 12, 2, '2019-04-25 14:45:35'),
(44, 44, 'header', 'header', '', 'header', 0, 0, 2, 0, '', 1, 12, 2, '2019-05-09 05:10:07'),
(45, 45, 'grid', 'grid', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 12, 2, '2019-05-09 05:10:56'),
(46, 46, 'picture', 'picture', '', 'header', 0, 44, 2, 0, '', 1, 12, 2, '2019-05-09 05:11:35'),
(47, 47, 'customers', 'customers', 'customer-list-filter', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 12, 2, '2019-05-09 05:14:18'),
(48, 48, 'orders', 'orders', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 12, 2, '2019-05-09 05:15:52'),
(49, 49, 'appointment', 'appointment', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 12, 2, '2019-05-09 05:18:07'),
(50, 50, 'accounts', 'accounts', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 12, 67, '2019-05-09 05:18:52'),
(51, 51, 'paypal_settings', 'paypal_settings', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 12, 2, '2019-05-09 05:20:28'),
(52, 52, 'knowledge_base', 'knowledge_base', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 12, 2, '2019-05-09 05:23:04'),
(53, 53, 'company_profile', 'company_profile', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 12, 2, '2019-05-09 05:25:33'),
(54, 54, 'my_account', 'my_account', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 12, 2, '2019-05-09 05:27:58'),
(55, 55, 'change_password', 'change_password', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 12, 2, '2019-05-09 05:29:05'),
(56, 56, 'my_orders', 'my_orders', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 12, 2, '2019-05-09 05:31:35'),
(57, 57, 'logs', 'logs', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 12, 2, '2019-05-09 05:32:54'),
(58, 59, 'dashboard', 'dashboard', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 12, 2, '2019-05-18 10:43:26'),
(59, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 12, 2, '2019-05-19 11:57:20'),
(60, 61, 'payment_gateway', 'payment_gateway', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 12, 2, '2019-05-22 09:21:00'),
(61, 62, 'payment_logs', 'payment_logs', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 12, 2, '2019-05-26 12:05:11'),
(62, 64, 'purchase_return', 'purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 12, 2, '2019-05-31 09:23:02'),
(63, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 12, 66, '2019-06-24 06:12:52'),
(64, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 12, 2, '2019-06-24 06:12:52'),
(65, 67, 'Gallery', 'Gallery', '', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 12, 2, '2019-06-24 06:12:52'),
(66, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 7, 27, 1, 0, '', 1, 12, 2, '2019-06-24 06:12:52'),
(67, 69, 'SMS Configuration', 'SMS Configuration', 'c-sms-configure', 'settings', 10, 27, 1, 0, '', 1, 12, 2, '2019-06-24 06:12:52'),
(68, 70, 'Email Configuration', 'Email Configuration', 'c-mail-configure', 'settings', 10, 27, 1, 0, '', 1, 12, 2, '2019-06-24 06:12:52'),
(69, 71, 'Custom Mail', 'Custom Mail', 'c-email', 'settings', 8, 27, 1, 0, '', 1, 12, 2, '2019-06-24 06:12:52'),
(70, 73, 'Add Gallery', 'Add Gallery', 'add_c_gallery_image', 'Gallery', 1, 67, 1, 0, '', 1, 12, 2, '2018-12-12 11:45:26'),
(71, 74, 'Tags', 'Tags', 'c_gallery_tag', 'Gallery', 3, 67, 1, 0, '', 1, 12, 2, '2018-12-12 11:45:26'),
(72, 75, 'Manage', 'Manage', 'manage_c_gallery_image', 'Gallery', 2, 67, 1, 0, '', 1, 12, 2, '2018-12-12 11:45:26'),
(73, 76, 'Catalog', 'Catalog', '', 'c_catalog', 11, 0, 1, 0, 'iconsmind-File-Chart', 1, 12, 0, '2019-09-10 13:40:42'),
(74, 77, 'Category list', 'Category list', 'customer/catalog/category/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(75, 78, 'Color list', 'Color list', 'customer/catalog/color/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(76, 79, 'Product list', 'Product list', 'customer/catalog/product/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(77, 80, 'Pattern list', 'Pattern list', 'customer/catalog/pattern/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(78, 81, 'Attribute list', 'Attribute list', 'customer/catalog/attribute/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(79, 82, 'Price list', 'Price list', 'customer/catalog/price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(80, 83, 'Group list', 'Group list', 'customer/catalog/group-price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-10 13:40:42'),
(81, 84, 'State list', 'State list', 'c-state', 'settings', 8, 27, 1, 0, '', 1, 12, 69, '2019-03-14 14:32:19'),
(82, 85, 'Sqm Price mapping', 'Sqm Price mapping', 'customer/catalog/sqm-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-16 13:57:49'),
(83, 86, 'Group Price mapping', 'Group Price mapping', 'customer/catalog/group-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 12, 0, '2019-09-16 13:57:49'),
(84, 87, 'Production', 'Production', '', 'c_production', 12, 0, 1, 0, 'iconsmind-File-Chart', 1, 12, 0, '2019-09-21 06:47:35'),
(85, 88, 'Manufactur', 'Manufactur', 'c-manufactur/list', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-21 06:47:49'),
(86, 1, 'customer', 'customer', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 3, 2, '2018-12-12 06:14:46'),
(87, 2, 'add_customer', 'add_customer', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 3, 2, '2018-12-12 06:15:26'),
(88, 3, 'customer_list', 'customer_list', 'customer-list-filter', 'customer', 2, 1, 1, 0, '', 1, 3, 2, '2018-12-12 06:16:01'),
(89, 4, 'order', 'order', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 3, 2, '2018-12-12 06:16:35'),
(90, 5, 'new_order', 'new_order', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 3, 48, '2018-12-12 06:17:40'),
(91, 6, 'manage_order', 'manage_order', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 3, 2, '2018-12-12 06:18:26'),
(92, 7, 'manage_invoice', 'manage_invoice', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 3, 2, '2018-12-12 06:18:56'),
(93, 8, 'cancelled_order', 'cancelled_order', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 3, 53, '2018-12-12 06:19:39'),
(94, 9, 'customer_return', 'customer_return', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 3, 2, '2018-12-12 06:23:22'),
(95, 10, 'track_order', 'track_order', 'track-order', 'order', 7, 0, 1, 0, '', 1, 3, 2, '2018-12-12 06:23:48'),
(96, 11, 'accounts', 'accounts', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 3, 2, '2018-12-12 06:24:27'),
(97, 12, 'chart_of_account', 'chart_of_account', 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 3, 2, '2018-12-12 06:25:02'),
(98, 13, 'vouchers', 'vouchers', '', 'account', NULL, 11, 1, 0, NULL, 1, 3, 2, '2018-12-12 06:25:40'),
(99, 14, 'debit_voucher', 'debit_voucher', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 3, 2, '2018-12-12 06:27:09'),
(100, 15, 'credit_voucher', 'credit_voucher', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 3, 2, '2018-12-12 06:27:42'),
(101, 16, 'journal_voucher', 'journal_voucher', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 3, 2, '2018-12-12 06:28:40'),
(102, 17, 'contra_voucher', 'contra_voucher', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 3, 2, '2018-12-12 06:29:13'),
(103, 18, 'voucher_approval', 'voucher_approval', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 3, 2, '2018-12-12 06:29:45'),
(104, 19, 'account_report', 'account_report', '', 'account', NULL, 11, 1, 0, NULL, 1, 3, 2, '2018-12-12 06:31:35'),
(105, 20, 'cash_book', 'cash_book', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:32:03'),
(106, 21, 'bank_book', 'bank_book', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:32:32'),
(107, 22, 'cash_flow', 'cash_flow', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:32:54'),
(108, 23, 'voucher_reports', 'voucher_reports', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:33:23'),
(109, 24, 'general_ledger', 'general_ledger', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:33:54'),
(110, 25, 'profit_or_loss', 'profit_or_loss', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:34:26'),
(111, 26, 'trial_balance', 'trial_balance', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 3, 2, '2018-12-12 06:34:58'),
(112, 27, 'settings', 'settings', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 3, 2, '2019-03-14 08:51:22'),
(113, 28, 'company_profile', 'company_profile', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:53:27'),
(114, 29, 'paypal_setting', 'paypal_setting', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:54:23'),
(115, 30, 'cost_factor', 'cost_factor', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:55:09'),
(116, 31, 'web_iframe_code', 'web_iframe_code', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:55:51'),
(117, 32, 'employee_and_access', 'employee_and_access', '', 'settings', 7, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:56:43'),
(118, 33, 'users', 'users', 'users', 'settings', 1, 32, 1, 0, '', 1, 3, 2, '2019-03-14 08:57:12'),
(119, 34, 'menu_customization', 'menu_customization', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 3, 2, '2019-03-14 08:57:51'),
(120, 35, 'role_permission', 'role_permission', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 3, 2, '2019-03-14 08:58:37'),
(121, 36, 'role_list', 'role_list', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 3, 2, '2019-03-14 08:59:59'),
(122, 37, 'add_user_role', 'add_user_role', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 3, 2, '2019-03-14 09:00:51'),
(123, 38, 'access_role', 'access_role', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 3, 2, '2019-03-14 09:01:32'),
(124, 39, 'sales_tax', 'sales_tax', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 3, 69, '2019-03-14 09:02:19'),
(125, 40, 'bulk_upload', 'bulk_upload', 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 3, 2, '2019-03-20 09:03:23'),
(126, 41, 'quotation', 'quotation', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 3, 2, '2019-04-25 14:42:49'),
(127, 42, 'add_new_quotation', 'add_new_quotation', 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 3, 2, '2019-04-25 14:44:40'),
(128, 43, 'manage_quotation', 'manage_quotation', 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 3, 2, '2019-04-25 14:45:35'),
(129, 44, 'header', 'header', '', 'header', 0, 0, 2, 0, '', 1, 3, 2, '2019-05-09 05:10:07'),
(130, 45, 'grid', 'grid', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 3, 2, '2019-05-09 05:10:56'),
(131, 46, 'picture', 'picture', '', 'header', 0, 44, 2, 0, '', 1, 3, 2, '2019-05-09 05:11:35'),
(132, 47, 'customers', 'customers', 'customer-list-filter', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 3, 2, '2019-05-09 05:14:18'),
(133, 48, 'orders', 'orders', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 3, 2, '2019-05-09 05:15:52'),
(134, 49, 'appointment', 'appointment', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 3, 2, '2019-05-09 05:18:07'),
(135, 50, 'accounts', 'accounts', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 3, 67, '2019-05-09 05:18:52'),
(136, 51, 'paypal_settings', 'paypal_settings', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 3, 2, '2019-05-09 05:20:28'),
(137, 52, 'knowledge_base', 'knowledge_base', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 3, 2, '2019-05-09 05:23:04'),
(138, 53, 'company_profile', 'company_profile', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 3, 2, '2019-05-09 05:25:33'),
(139, 54, 'my_account', 'my_account', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 3, 2, '2019-05-09 05:27:58'),
(140, 55, 'change_password', 'change_password', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 3, 2, '2019-05-09 05:29:05'),
(141, 56, 'my_orders', 'my_orders', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 3, 2, '2019-05-09 05:31:35'),
(142, 57, 'logs', 'logs', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 3, 2, '2019-05-09 05:32:54'),
(143, 59, 'dashboard', 'dashboard', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 3, 2, '2019-05-18 10:43:26'),
(144, 60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 3, 2, '2019-05-19 11:57:20'),
(145, 61, 'payment_gateway', 'payment_gateway', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 3, 2, '2019-05-22 09:21:00'),
(146, 62, 'payment_logs', 'payment_logs', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 3, 2, '2019-05-26 12:05:11'),
(147, 64, 'purchase_return', 'purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 3, 2, '2019-05-31 09:23:02'),
(148, 65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 3, 66, '2019-06-24 06:12:52'),
(149, 66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 3, 2, '2019-06-24 06:12:52'),
(150, 67, 'Gallery', 'Gallery', '', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 3, 2, '2019-06-24 06:12:52'),
(151, 68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 7, 27, 1, 0, '', 1, 3, 2, '2019-06-24 06:12:52'),
(152, 69, 'SMS Configuration', 'SMS Configuration', 'c-sms-configure', 'settings', 10, 27, 1, 0, '', 1, 3, 2, '2019-06-24 06:12:52'),
(153, 70, 'Email Configuration', 'Email Configuration', 'c-mail-configure', 'settings', 10, 27, 1, 0, '', 1, 3, 2, '2019-06-24 06:12:52'),
(154, 71, 'Custom Mail', 'Custom Mail', 'c-email', 'settings', 8, 27, 1, 0, '', 1, 3, 2, '2019-06-24 06:12:52'),
(155, 73, 'Add Gallery', 'Add Gallery', 'add_c_gallery_image', 'Gallery', 1, 67, 1, 0, '', 1, 3, 2, '2018-12-12 11:45:26'),
(156, 74, 'Tags', 'Tags', 'c_gallery_tag', 'Gallery', 3, 67, 1, 0, '', 1, 3, 2, '2018-12-12 11:45:26'),
(157, 75, 'Manage', 'Manage', 'manage_c_gallery_image', 'Gallery', 2, 67, 1, 0, '', 1, 3, 2, '2018-12-12 11:45:26'),
(158, 76, 'Catalog', 'Catalog', '', 'c_catalog', 11, 0, 1, 0, 'iconsmind-File-Chart', 1, 3, 0, '2019-09-10 13:40:42'),
(159, 77, 'Category list', 'Category list', 'customer/catalog/category/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(160, 78, 'Color list', 'Color list', 'customer/catalog/color/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(161, 79, 'Product list', 'Product list', 'customer/catalog/product/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(162, 80, 'Pattern list', 'Pattern list', 'customer/catalog/pattern/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(163, 81, 'Attribute list', 'Attribute list', 'customer/catalog/attribute/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(164, 82, 'Price list', 'Price list', 'customer/catalog/price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(165, 83, 'Group list', 'Group list', 'customer/catalog/group-price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-10 13:40:42'),
(166, 84, 'State list', 'State list', 'c-state', 'settings', 8, 27, 1, 0, '', 1, 3, 69, '2019-03-14 14:32:19'),
(167, 85, 'Sqm Price mapping', 'Sqm Price mapping', 'customer/catalog/sqm-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-16 13:57:49'),
(168, 86, 'Group Price mapping', 'Group Price mapping', 'customer/catalog/group-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 3, 0, '2019-09-16 13:57:49'),
(169, 87, 'Production', 'Production', '', 'c_production', 12, 0, 1, 0, 'iconsmind-File-Chart', 1, 3, 0, '2019-09-21 06:47:35'),
(170, 88, 'Manufactur', 'Manufactur', 'c-manufactur/list', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-21 06:47:49'),
(171, 89, 'Supplier', 'Supplier', 'c-supplier/list', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-21 09:45:46'),
(172, 89, 'Supplier', 'Supplier', 'c-supplier/list', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-21 09:45:46'),
(173, 90, 'Stock Availability', 'Stock Availability', 'c-stock-availability', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-23 06:15:21'),
(174, 90, 'Stock Availability', 'Stock Availability', 'c-stock-availability', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-23 06:15:21'),
(175, 91, 'Stock History', 'Stock History', 'c-stock-history', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-23 10:43:25'),
(176, 91, 'Stock History', 'Stock History', 'c-stock-history', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-23 10:43:25'),
(177, 92, 'Customer Return', 'Customer Return', 'c-customer-return', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-23 12:06:28'),
(178, 92, 'Customer Return', 'Customer Return', 'c-customer-return', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-23 12:06:28'),
(179, 93, 'Supplier Return', 'Supplier Return', 'c-supplier-return', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-23 12:09:25'),
(180, 93, 'Supplier Return', 'Supplier Return', 'c-supplier-return', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-23 12:09:25'),
(181, 94, 'Raw Material Usage', 'Raw Material Usage', 'c-raw-material-usage', 'c_production', 1, 87, 1, 0, '1', 1, 12, 0, '2019-09-24 05:44:40'),
(182, 94, 'Raw Material Usage', 'Raw Material Usage', 'c-raw-material-usage', 'c_production', 1, 87, 1, 0, '1', 1, 3, 0, '2019-09-24 05:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `c_notification_tbl`
--

CREATE TABLE `c_notification_tbl` (
  `id` int(11) NOT NULL,
  `notification_text` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `go_to_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `c_notification_tbl`
--

INSERT INTO `c_notification_tbl` (`id`, `notification_text`, `go_to_url`, `created_by`, `date`) VALUES
(1, 'Order Stage has been updated to Manufacturing for BMSW-Jame-1000-001', '#', 2, '2019-09-20'),
(2, 'Order Stage has been updated to Manufacturing for BMSW-Jame-1000-001', '#', 2, '2019-09-20'),
(3, 'Order Stage has been updated to Manufacturing for BMSW-Jame-1000-001', '#', 2, '2019-09-20'),
(4, 'Order Stage has been updated to Quote for BMSW-Jame-1000-001', '#', 2, '2019-09-20'),
(5, 'Order Stage has been updated to Manufacturing for BMSW-Jame-1000-001', '#', 2, '2019-09-20');

-- --------------------------------------------------------

--
-- Table structure for table `c_quatation_details`
--

CREATE TABLE `c_quatation_details` (
  `qd_id` int(11) NOT NULL,
  `fk_qt_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `win_no` int(11) NOT NULL,
  `win_width` float NOT NULL,
  `win_wfraction` varchar(15) DEFAULT NULL,
  `win_height` float NOT NULL,
  `win_htfraction` varchar(15) DEFAULT NULL,
  `win_comment` text,
  `win_categories` text NOT NULL,
  `win_created_on` datetime NOT NULL,
  `win_modified_on` datetime NOT NULL,
  `win_created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_quatation_table`
--

CREATE TABLE `c_quatation_table` (
  `qt_id` int(11) NOT NULL,
  `qt_cust_id` int(11) NOT NULL,
  `qt_cust_type` int(11) NOT NULL,
  `qt_subtotal` float NOT NULL,
  `total_discount` varchar(10) NOT NULL,
  `total_upcharge` varchar(10) NOT NULL,
  `qt_tax` float NOT NULL,
  `qt_grand_total` float NOT NULL,
  `qt_order_date` date NOT NULL,
  `qt_created_on` datetime NOT NULL,
  `qt_modified_on` datetime NOT NULL,
  `qt_created_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `c_us_state_tbl`
--

CREATE TABLE `c_us_state_tbl` (
  `state_id` int(11) NOT NULL,
  `shortcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` float DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

CREATE TABLE `gallery_image_tbl` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tag_tbl`
--

CREATE TABLE `gallery_tag_tbl` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `tag_value` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gateway_tbl`
--

CREATE TABLE `gateway_tbl` (
  `id` int(11) NOT NULL,
  `payment_gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `default_status` int(11) NOT NULL,
  `level_type` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` date NOT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0=development, 1=production'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_row_column_price_tbl`
--

CREATE TABLE `group_row_column_price_tbl` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `row_data` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `column_data` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL COMMENT 'Unit of Measurement',
  `uom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hana_package`
--

CREATE TABLE `hana_package` (
  `id` int(11) NOT NULL,
  `package` varchar(255) DEFAULT NULL,
  `trial_days` int(11) NOT NULL DEFAULT '0',
  `installation_price` double(8,2) NOT NULL DEFAULT '0.00',
  `monthly_price` double(8,2) NOT NULL DEFAULT '0.00',
  `yearly_price` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hana_package`
--

INSERT INTO `hana_package` (`id`, `package`, `trial_days`, `installation_price`, `monthly_price`, `yearly_price`) VALUES
(1, 'Basic FREE', 0, 0.00, 0.00, 0.00),
(2, 'Advanced', 20, 1999.00, 199.00, 2388.00),
(3, 'Pro', 30, 3999.00, 225.00, 3060.00);

-- --------------------------------------------------------

--
-- Table structure for table `hana_package_option`
--

CREATE TABLE `hana_package_option` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `package_key` varchar(255) DEFAULT NULL,
  `basic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `advanced` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `pro` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hana_package_option`
--

INSERT INTO `hana_package_option` (`id`, `title`, `package_key`, `basic`, `advanced`, `pro`) VALUES
(1, 'B-level Connection', 'b_level_package', 0, 0, 0),
(2, 'New Order Form', 'new_order_form', 0, 0, 0),
(3, 'B-C Status', 'b_c_status', 0, 0, 0),
(4, 'Accouting (Basic)', 'accounting', 0, 0, 0),
(5, 'Settings (Basic)', 'setting', 0, 0, 0),
(6, 'Gallery (Basic)', 'gallery', 0, 0, 0),
(7, 'Customer (D-level)', 'customer', 0, 0, 0),
(8, 'Multiqoute', 'multiqoute', 0, 0, 0),
(9, 'Appointment', 'appointment', 0, 0, 0),
(10, 'Catalog (View existing product without price + Add new products with price)', 'catalog', 0, 0, 0),
(11, 'Transfer Order and Email', 'transfer_order_an_email', 0, 0, 0),
(12, 'Bulk SMS', 'bulk_sms', 0, 0, 0),
(13, 'Bulk Email', 'bulk_email', 0, 0, 0),
(14, 'Sales Commission', 'sales_commission', 0, 0, 0),
(15, 'Production (MFG)', 'production', 0, 0, 0),
(16, 'Inventory', 'inventory', 0, 0, 0),
(17, 'Shipping', 'shipping', 0, 0, 0),
(18, 'Setting (Pro)', 'setting', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `iframe_code_tbl`
--

CREATE TABLE `iframe_code_tbl` (
  `frame_id` int(11) NOT NULL,
  `iframe_code` text COLLATE utf8_unicode_ci NOT NULL,
  `level_type` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_info`
--

CREATE TABLE `log_info` (
  `row_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'FK of User_Info table',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'B or C',
  `is_admin` int(2) NOT NULL,
  `login_datetime` datetime NOT NULL,
  `logout_datetime` datetime NOT NULL,
  `last_login_ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `random_key` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log_info`
--

INSERT INTO `log_info` (`row_id`, `user_id`, `email`, `password`, `user_type`, `is_admin`, `login_datetime`, `logout_datetime`, `last_login_ip`, `random_key`) VALUES
(1, 1, 'admin@bmslink.net', 'e10adc3949ba59abbe56e057f20f883e', 's', 1, '2019-09-24 17:24:55', '2019-09-23 11:23:07', '127.0.0.1', ''),
(2, 2, 'james@bmsdecor.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-09-24 10:21:16', '2019-09-20 11:21:48', '127.0.0.1', ''),
(3, 3, 'info@legacyblinds.com', '26f132619a6b82be133d96688b3a3d97', 'c', 1, '2019-09-19 14:32:22', '2019-09-19 14:33:07', '127.0.0.1', ''),
(4, 4, 'shesdeco@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-09-19 16:26:23', '2019-09-19 16:28:43', '127.0.0.1', ''),
(5, 8, 'hulk@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-09-19 19:02:45', '2019-09-19 18:51:38', '127.0.0.1', ''),
(6, 9, 'silvester@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 10, 'test_b_1@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-09-19 18:52:21', '2019-09-19 19:02:51', '127.0.0.1', ''),
(8, 11, 'test_b_2@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'b', 1, '2019-09-19 18:52:01', '2019-09-19 19:02:32', '127.0.0.1', ''),
(9, 12, 'cust_test@mailinator.com', 'e10adc3949ba59abbe56e057f20f883e', 'c', 1, '2019-09-24 17:21:18', '2019-09-24 16:48:12', '127.0.0.1', '');

-- --------------------------------------------------------

--
-- Table structure for table `mail_config_tbl`
--

CREATE TABLE `mail_config_tbl` (
  `id` int(11) NOT NULL,
  `protocol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_host` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_port` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_user` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `smtp_pass` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mailtype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_config_tbl`
--

INSERT INTO `mail_config_tbl` (`id`, `protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `mailtype`, `updated_by`, `updated_date`, `status`, `created_by`, `is_verified`) VALUES
(3, 'stmp', 'mail.bmsdecor.com', '2525', 'info@bmsdecor.com', 'Bmsd123#', 'html', 2, '2019-09-17 00:00:00', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_quatation_attributes`
--

CREATE TABLE `manufacturing_b_level_quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_quatation_tbl`
--

CREATE TABLE `manufacturing_b_level_quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clevel_order_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_b_level_qutation_details`
--

CREATE TABLE `manufacturing_b_level_qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `unit_total_price` float NOT NULL,
  `list_price` float NOT NULL,
  `discount` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_shipment_data`
--

CREATE TABLE `manufacturing_shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `customer_name` varchar(250) DEFAULT NULL,
  `customer_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manufactur_data`
--

CREATE TABLE `manufactur_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `chk_option` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mapping_measurement`
--

CREATE TABLE `mapping_measurement` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_measure_id` int(11) NOT NULL,
  `to_measure_id` int(11) NOT NULL,
  `measure_value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menusetup_tbl`
--

CREATE TABLE `menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_title` varchar(200) NOT NULL,
  `korean_name` varchar(50) DEFAULT NULL,
  `page_url` varchar(200) NOT NULL,
  `module` varchar(200) NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(3) NOT NULL COMMENT '1 = left, 2 = system',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menusetup_tbl`
--

INSERT INTO `menusetup_tbl` (`id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 'customer', 'customer', '', 'customer', 1, 0, 1, 0, 'simple-icon-people', 1, 0, 2, '2018-12-12 06:14:46'),
(2, 'add_customer', 'add_customer', 'add-customer', 'customer', 1, 1, 1, 0, '', 1, 0, 2, '2018-12-12 06:15:26'),
(3, 'customer_list', 'customer_list', 'customer-list-filter', 'customer', 2, 1, 1, 0, '', 1, 0, 2, '2018-12-12 06:16:01'),
(4, 'order', 'order', '', 'order', 2, 0, 1, 0, 'iconsmind-Address-Book2', 1, 0, 2, '2018-12-12 06:16:35'),
(5, 'new_order', 'new_order', 'new-quotation', 'order', 1, 4, 1, 0, '', 1, 0, 48, '2018-12-12 06:17:40'),
(6, 'manage_order', 'manage_order', 'manage-order', 'order', 2, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:18:26'),
(7, 'manage_invoice', 'manage_invoice', 'manage-invoice', 'order', 3, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:18:56'),
(8, 'cancelled_order', 'cancelled_order', 'order-cancel', 'order', 4, 4, 1, 0, '', 1, 0, 53, '2018-12-12 06:19:39'),
(9, 'customer_return', 'customer_return', 'customer-return', 'order', 5, 4, 1, 0, '', 1, 0, 2, '2018-12-12 06:23:22'),
(10, 'track_order', 'track_order', 'track-order', 'order', 7, 0, 1, 0, '', 1, 0, 2, '2018-12-12 06:23:48'),
(11, 'accounts', 'accounts', '', 'account', 4, 0, 1, 0, 'iconsmind-Pencil d-block', 1, 0, 2, '2018-12-12 06:24:27'),
(12, 'chart_of_account', 'chart_of_account', 'chart-of-account', 'account', NULL, 11, 1, 0, '', 1, 0, 2, '2018-12-12 06:25:02'),
(13, 'vouchers', 'vouchers', '', 'account', NULL, 11, 1, 0, NULL, 1, 0, 2, '2018-12-12 06:25:40'),
(14, 'debit_voucher', 'debit_voucher', 'voucher-debit', 'account', 1, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:27:09'),
(15, 'credit_voucher', 'credit_voucher', 'voucher-credit', 'account', 2, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:27:42'),
(16, 'journal_voucher', 'journal_voucher', 'voucher-journal', 'account', 3, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:28:40'),
(17, 'contra_voucher', 'contra_voucher', 'contra-voucher', 'account', 4, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:29:13'),
(18, 'voucher_approval', 'voucher_approval', 'voucher-approval', 'account', 5, 13, 1, 0, '', 1, 0, 2, '2018-12-12 06:29:45'),
(19, 'account_report', 'account_report', '', 'account', NULL, 11, 1, 0, NULL, 1, 0, 2, '2018-12-12 06:31:35'),
(20, 'cash_book', 'cash_book', 'cash-book', 'account', 2, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:03'),
(21, 'bank_book', 'bank_book', 'bank-book', 'account', 1, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:32'),
(22, 'cash_flow', 'cash_flow', 'cash-flow', 'account', 3, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:32:54'),
(23, 'voucher_reports', 'voucher_reports', 'voucher-reports', 'account', 7, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:33:23'),
(24, 'general_ledger', 'general_ledger', 'general-ledger', 'account', 4, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:33:54'),
(25, 'profit_or_loss', 'profit_or_loss', 'profit-loss', 'account', 5, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:34:26'),
(26, 'trial_balance', 'trial_balance', 'trial-balance', 'account', 6, 19, 1, 0, '', 1, 0, 2, '2018-12-12 06:34:58'),
(27, 'settings', 'settings', '', 'settings', 5, 0, 1, 0, 'simple-icon-settings', 1, 0, 2, '2019-03-14 08:51:22'),
(28, 'company_profile', 'company_profile', 'company-profile', 'settings', 2, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:53:27'),
(29, 'paypal_setting', 'paypal_setting', 'payment-setting', 'settings', 3, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:54:23'),
(30, 'cost_factor', 'cost_factor', 'c-cost-factor', 'settings', 5, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:55:09'),
(31, 'web_iframe_code', 'web_iframe_code', 'iframe-code', 'settings', 6, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:55:51'),
(32, 'employee_and_access', 'employee_and_access', '', 'settings', 7, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:56:43'),
(33, 'users', 'users', 'users', 'settings', 1, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:57:12'),
(34, 'menu_customization', 'menu_customization', 'c-menu-setup', 'settings', 4, 27, 1, 0, '', 1, 0, 2, '2019-03-14 08:57:51'),
(35, 'role_permission', 'role_permission', 'role-permission', 'settings', 2, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:58:37'),
(36, 'role_list', 'role_list', 'role-list', 'settings', 3, 32, 1, 0, '', 1, 0, 2, '2019-03-14 08:59:59'),
(37, 'add_user_role', 'add_user_role', 'user-role', 'settings', 4, 32, 1, 0, '', 1, 0, 2, '2019-03-14 09:00:51'),
(38, 'access_role', 'access_role', 'access-role', 'settings', 5, 32, 1, 0, '', 1, 0, 2, '2019-03-14 09:01:32'),
(39, 'sales_tax', 'sales_tax', 'c-us-state', 'settings', 8, 27, 1, 0, '', 1, 0, 69, '2019-03-14 09:02:19'),
(40, 'bulk_upload', 'bulk_upload', 'c-customer-bulk-upload', 'customer', 3, 1, 1, 0, '', 1, 0, 2, '2019-03-20 09:03:23'),
(41, 'quotation', 'quotation', '', 'quotation', 3, 0, 1, 0, 'simple-icon-badge', 1, 0, 2, '2019-04-25 14:42:49'),
(42, 'add_new_quotation', 'add_new_quotation', 'c_level/quotation_controller/add_new_quotation', 'quotation', 1, 41, 1, 0, '', 1, 0, 2, '2019-04-25 14:44:40'),
(43, 'manage_quotation', 'manage_quotation', 'c_level/quotation_controller/manage_quotation', 'quotation', 2, 41, 1, 0, '', 1, 0, 2, '2019-04-25 14:45:35'),
(44, 'header', 'header', '', 'header', 0, 0, 2, 0, '', 1, 0, 2, '2019-05-09 05:10:07'),
(45, 'grid', 'grid', '', 'header', 0, 44, 2, 0, 'simple-icon-grid', 1, 0, 2, '2019-05-09 05:10:56'),
(46, 'picture', 'picture', '', 'header', 0, 44, 2, 0, '', 1, 0, 2, '2019-05-09 05:11:35'),
(47, 'customers', 'customers', 'customer-list-filter', 'header', 0, 45, 2, 0, 'iconsmind-MaleFemale d-block', 1, 0, 2, '2019-05-09 05:14:18'),
(48, 'orders', 'orders', 'manage-order', 'header', 0, 45, 2, 0, 'iconsmind-Address-Book2 d-block', 1, 0, 2, '2019-05-09 05:15:52'),
(49, 'appointment', 'appointment', 'appointment-form', 'header', 0, 45, 2, 0, 'iconsmind-Clock d-block', 1, 0, 2, '2019-05-09 05:18:07'),
(50, 'accounts', 'accounts', 'chart-of-account', 'header', 0, 0, 2, 0, 'iconsmind-Pencil d-block', 1, 0, 67, '2019-05-09 05:18:52'),
(51, 'paypal_settings', 'paypal_settings', 'payment-setting', 'header', 0, 45, 2, 0, 'iconsmind-Settings-Window d-block', 1, 0, 2, '2019-05-09 05:20:28'),
(52, 'knowledge_base', 'knowledge_base', 'faq', 'header', 0, 45, 2, 0, 'iconsmind-Suitcase d-block', 0, 0, 2, '2019-05-09 05:23:04'),
(53, 'company_profile', 'company_profile', 'company-profile', 'header', 1, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 0, 2, '2019-05-09 05:25:33'),
(54, 'my_account', 'my_account', 'c-my-account', 'header', 2, 46, 2, 0, 'glyph-icon simple-icon-user', 1, 0, 2, '2019-05-09 05:27:58'),
(55, 'change_password', 'change_password', 'c-password-change', 'header', 4, 46, 2, 0, 'simple-icon-key', 1, 0, 2, '2019-05-09 05:29:05'),
(56, 'my_orders', 'my_orders', 'my-orders', 'header', 5, 46, 2, 0, 'glyph-icon simple-icon-puzzle', 1, 0, 2, '2019-05-09 05:31:35'),
(57, 'logs', 'logs', 'logs', 'header', 6, 46, 2, 0, 'glyph-icon simple-icon-refresh', 1, 0, 2, '2019-05-09 05:32:54'),
(59, 'dashboard', 'dashboard', 'c-level-dashboard', 'dashboard', 0, 0, 1, 0, 'iconsmind-Shop-4', 0, 0, 2, '2019-05-18 10:43:26'),
(60, 'card info', 'card info', 'c_level/setting_controller/card_info', 'settings', 9, 27, 1, 0, '', 1, 0, 2, '2019-05-19 11:57:20'),
(61, 'payment_gateway', 'payment_gateway', 'payment-gateway', 'settings', 1, 27, 1, 0, '', 1, 0, 2, '2019-05-22 09:21:00'),
(62, 'payment_logs', 'payment_logs', 'c_level/payment_log/get_log', 'header', 3, 46, 2, 0, 'simple-icon-wallet', 1, 0, 2, '2019-05-26 12:05:11'),
(64, 'purchase_return', 'purchase_return', 'c-purchase-return', 'order', 6, 4, 1, 0, '', 1, 0, 2, '2019-05-31 09:23:02'),
(65, 'upcharges', 'upcharges', 'c_level/quotation_controller/manageUpcharges', 'quotation', 3, 41, 1, 0, '', 1, 0, 66, '2019-06-24 06:12:52'),
(66, 'Commission report', 'Commission report', 'c_commission_report', 'Commission', 7, 0, 1, 0, 'iconsmind-Statistic', 1, 0, 2, '2019-06-24 06:12:52'),
(67, 'Gallery', 'Gallery', '', 'gallery', 6, 0, 1, 0, 'iconsmind-Photos', 1, 0, 2, '2019-06-24 06:12:52'),
(68, 'Custom SMS', 'Custom SMS', 'c-sms', 'settings', 7, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(69, 'SMS Configuration', 'SMS Configuration', 'c-sms-configure', 'settings', 10, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(70, 'Email Configuration', 'Email Configuration', 'c-mail-configure', 'settings', 10, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(71, 'Custom Mail', 'Custom Mail', 'c-email', 'settings', 8, 27, 1, 0, '', 1, 0, 2, '2019-06-24 06:12:52'),
(73, 'Add Gallery', 'Add Gallery', 'add_c_gallery_image', 'Gallery', 1, 67, 1, 0, '', 1, 0, 2, '2018-12-12 11:45:26'),
(74, 'Tags', 'Tags', 'c_gallery_tag', 'Gallery', 3, 67, 1, 0, '', 1, 0, 2, '2018-12-12 11:45:26'),
(75, 'Manage', 'Manage', 'manage_c_gallery_image', 'Gallery', 2, 67, 1, 0, '', 1, 0, 2, '2018-12-12 11:45:26'),
(76, 'Catalog', 'Catalog', '', 'c_catalog', 11, 0, 1, 0, 'iconsmind-File-Chart', 1, 0, 0, '2019-09-10 13:40:42'),
(77, 'Category list', 'Category list', 'customer/catalog/category/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(78, 'Color list', 'Color list', 'customer/catalog/color/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(79, 'Product list', 'Product list', 'customer/catalog/product/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(80, 'Pattern list', 'Pattern list', 'customer/catalog/pattern/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(81, 'Attribute list', 'Attribute list', 'customer/catalog/attribute/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(82, 'Price list', 'Price list', 'customer/catalog/price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(83, 'Group list', 'Group list', 'customer/catalog/group-price/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-10 13:40:42'),
(84, 'State list', 'State list', 'c-state', 'settings', 8, 27, 1, 0, '', 1, 0, 69, '2019-03-14 14:32:19'),
(85, 'Sqm Price mapping', 'Sqm Price mapping', 'customer/catalog/sqm-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-16 13:57:49'),
(86, 'Group Price mapping', 'Group Price mapping', 'customer/catalog/group-price-mapping/list', 'c_catalog', 1, 76, 1, 0, '1', 1, 0, 0, '2019-09-16 13:57:49'),
(87, 'Production', 'Production', '', 'c_production', 12, 0, 1, 0, 'iconsmind-File-Chart', 1, 0, 0, '2019-09-21 06:47:35'),
(88, 'Manufactur', 'Manufactur', 'c-manufactur/list', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-21 06:47:49'),
(89, 'Supplier', 'Supplier', 'c-supplier/list', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-21 09:45:46'),
(90, 'Stock Availability', 'Stock Availability', 'c-stock-availability', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-23 06:15:21'),
(91, 'Stock History', 'Stock History', 'c-stock-history', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-23 10:43:25'),
(92, 'Customer Return', 'Customer Return', 'c-customer-return', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-23 12:06:28'),
(93, 'Supplier Return', 'Supplier Return', 'c-supplier-return', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-23 12:09:25'),
(94, 'Raw Material Usage', 'Raw Material Usage', 'c-raw-material-usage', 'c_production', 1, 87, 1, 0, '1', 1, 0, 0, '2019-09-24 05:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `order_return_details`
--

CREATE TABLE `order_return_details` (
  `id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL,
  `replace_qty` int(11) NOT NULL,
  `replace_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_return_tbl`
--

CREATE TABLE `order_return_tbl` (
  `return_id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `return_date` date NOT NULL,
  `return_comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `returned_by` int(11) NOT NULL,
  `is_approved` tinyint(2) NOT NULL COMMENT '0 = pending, 1 = approved, 2 = cancel',
  `approved_by` int(11) NOT NULL,
  `rma_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `c_approval` tinyint(4) NOT NULL DEFAULT '0',
  `c_approval_by` int(11) NOT NULL,
  `c_approval_date` date NOT NULL,
  `level_id` int(11) NOT NULL,
  `resend_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `resend_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT '1971-01-01 00:00:01'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pattern_model_tbl`
--

CREATE TABLE `pattern_model_tbl` (
  `pattern_model_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `pattern_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pattern_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pattern_model_tbl`
--

INSERT INTO `pattern_model_tbl` (`pattern_model_id`, `category_id`, `pattern_type`, `pattern_name`, `status`, `created_by`, `updated_by`, `created_date`, `updated_date`, `created_status`) VALUES
(172, 2, '', '3D Shades', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(173, 2, '', '3D Shades Semi Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(174, 2, '', 'Jacquard Ares', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(175, 2, '', 'Ares', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(176, 2, '', 'CTB', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(177, 2, '', 'WTW', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(178, 2, '', 'Sensation', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(179, 2, '', 'Net woodlook', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(180, 2, '', 'Roadview', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(181, 2, '', 'BTL', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(182, 2, '', 'small Trio', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(183, 2, '', 'Loraine', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(184, 2, '', 'Rainbow', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(185, 2, '', 'Bio Jasmine', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(186, 2, '', 'Esther Magic', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(187, 2, '', 'Sinabro', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(188, 2, '', 'elegance Rudy', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(189, 2, '', 'Smart Comforts', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(190, 2, '', 'Iren', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(191, 2, '', 'Rosetta', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(192, 2, '', 'Elsa', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(193, 2, '', 'Darin', 1, 2, 0, '2019-09-17', '2019-09-17 18:12:59', 0),
(194, 2, '', 'Bio Amor', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(195, 2, '', 'Hublot', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(196, 2, '', '6 Line Gradation', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(197, 2, '', 'Elegance Blackout 50%', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(198, 2, '', 'Elegance Twin 50%', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(199, 2, '', 'Calm', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(200, 2, '', 'N-Monica Triple', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(201, 2, '', 'Timber Magic', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(202, 2, '', 'Blind Magic', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(203, 2, '', 'Open Grace', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(204, 2, '', 'sweet magic', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(205, 2, '', 'Romance Fresh', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(206, 2, '', 'Romance Fresh Natural Dandy', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(207, 2, '', 'Faro Twins', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(208, 2, '', 'smart blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(209, 2, '', 'Valentine Carisma blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(210, 2, '', 'Serina Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(211, 2, '', 'Blackout Wide', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(212, 2, '', 'Samrt Zelkova', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(213, 2, '', 'Blackout Lime', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(214, 2, '', 'Jasmine blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(215, 2, '', 'Elis Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(216, 2, '', 'Orion', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(217, 2, '', 'Hera', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(218, 2, '', 'Blackout Graphic', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(219, 2, '', 'Blackout Slope', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(220, 2, '', 'Blackout Combi', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(221, 2, '', 'Nova', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(222, 2, '', 'Sunset blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(223, 2, '', 'Oscar blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(224, 2, '', 'Ruby Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(225, 2, '', 'Pretty', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(226, 2, '', 'Smart Pola', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(227, 2, '', 'Chrome', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(228, 2, '', 'Smart Anna Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(229, 2, '', 'Motive', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(230, 2, '', 'Magic 5', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(231, 2, '', 'Elegant Marble Roman', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(232, 2, '', 'Elegant Diamond Roman', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(233, 2, '', 'Vague blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(234, 2, '', 'Roller Lasor', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(235, 2, '', 'Triple Printing', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(236, 2, '', 'Roller Muji Printing', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(237, 2, '', 'Combi Printing', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(238, 2, '', 'Roller B/O Printing', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(239, 2, '', 'Picaso Black Edition', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(240, 2, '', 'Muji', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(241, 2, '', 'Acro', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(242, 2, '', 'Riga', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(243, 2, '', 'Scarlet', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(244, 2, '', 'Prestige', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(245, 2, '', 'Hurricane', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(246, 2, '', 'Eco Amalfy', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(247, 2, '', 'Guess', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(248, 2, '', 'Silkroad', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(249, 2, '', 'Renaissance', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(250, 2, '', 'New Obidose', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(251, 2, '', 'Base', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(252, 2, '', 'Neo Silkroad', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(253, 2, '', 'Silk Look', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(254, 2, '', 'Woodlook Elegant BLACKOUT', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(255, 2, '', 'Stucco', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(256, 2, '', 'Venitian Foam', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(257, 2, '', 'Woodlook Single Foam', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(258, 2, '', 'Mars Foam', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(259, 2, '', 'VX Screen 3000-1%', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(260, 2, '', 'VX Screen 3000-3%', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(261, 2, '', 'VX Screen 3000-5%', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(262, 2, '', 'BELUGA 2', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(263, 2, '', 'Swan', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(264, 2, '', 'Venezia', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(265, 2, '', 'Shangrila Blackout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(266, 2, '', 'UV graphic', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(267, 2, '', 'Printing (Linen/Cloud)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(268, 2, '', 'Cubic Triple-winplus', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(269, 2, '', 'Vision Shade 2', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(270, 2, '', 'Albery', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(271, 2, '', 'Aura', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(272, 2, '', 'Ayana-White', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(273, 2, '', 'Banbury', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(274, 2, '', 'Birdsong', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(275, 2, '', 'Blossom', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(276, 2, '', 'Carnival', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(277, 2, '', 'Carnival SPC Low E', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(278, 2, '', 'Chatsworth', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(279, 2, '', 'Cherry Blossom(Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(280, 2, '', 'Classic (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(281, 2, '', 'Collina (Blacout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(282, 2, '', 'Como', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(283, 2, '', 'Coral-White', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(284, 2, '', 'Crush (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(285, 2, '', 'Crystal (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(286, 2, '', 'Flutter', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(287, 2, '', 'Hampton', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(288, 2, '', 'Harmony', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(289, 2, '', 'Harmony (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(290, 2, '', 'Jurassic BO', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(291, 2, '', 'Linara', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(292, 2, '', 'Magnolia (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(293, 2, '', 'Maine (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(294, 2, '', 'Marble BO', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(295, 2, '', 'Meadow', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(296, 2, '', 'Melton', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(297, 2, '', 'Monroe', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(298, 2, '', 'Monterey', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(299, 2, '', 'Nimbus BO', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(300, 2, '', 'Nimbus LF', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(301, 2, '', 'Night Night', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(302, 2, '', 'Oslo', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(303, 2, '', 'Parchment', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(304, 2, '', 'Portland', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(305, 2, '', 'Prima', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(306, 2, '', 'Prima Dimout', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(307, 2, '', 'Ravello', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(308, 2, '', 'Ritz', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(309, 2, '', 'Shot Silk BO', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(310, 2, '', 'Sparkle', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(311, 2, '', 'Tundra', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(312, 2, '', 'Vermont', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(313, 2, '', 'Versailles (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(314, 2, '', 'Willow (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(315, 2, '', 'Wonderland (Blackout)', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(316, 2, '', 'Woodland', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(317, 1, '', 'Smooth', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(318, 1, '', 'Pro Grained', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(319, 1, '', 'Pro Blast', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(320, 1, '', 'Beveled', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(321, 1, '', 'None', 1, 2, 0, '2019-09-17', '2019-09-17 18:13:00', 0),
(322, 3, 'Pattern', 'None', 1, 2, 0, '2019-09-17', '2019-09-17 21:17:48', 0),
(323, 4, 'Pattern', 'None', 1, 2, 0, '2019-09-17', '2019-09-17 22:05:09', 0),
(324, 7, '', '3D Shades', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:20', 0),
(325, 7, '', '3D Shades Semi Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(326, 7, '', 'Jacquard Ares', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(327, 7, '', 'Ares', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(328, 7, '', 'CTB', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(329, 7, '', 'WTW', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(330, 7, '', 'Sensation', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(331, 7, '', 'Net woodlook', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(332, 7, '', 'Roadview', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(333, 7, '', 'BTL', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(334, 7, '', 'small Trio', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(335, 7, '', 'Loraine', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(336, 7, '', 'Rainbow', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(337, 7, '', 'Bio Jasmine', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(338, 7, '', 'Esther Magic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(339, 7, '', 'Sinabro', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(340, 7, '', 'elegance Rudy', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(341, 7, '', 'Smart Comforts', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(342, 7, '', 'Iren', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(343, 7, '', 'Rosetta', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(344, 7, '', 'Elsa', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(345, 7, '', 'Darin', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(346, 7, '', 'Bio Amor', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(347, 7, '', 'Hublot', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(348, 7, '', '6 Line Gradation', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(349, 7, '', 'Elegance Blackout 50%', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(350, 7, '', 'Elegance Twin 50%', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(351, 7, '', 'Calm', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(352, 7, '', 'N-Monica Triple', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(353, 7, '', 'Timber Magic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:21', 0),
(354, 7, '', 'Blind Magic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(355, 7, '', 'Open Grace', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(356, 7, '', 'sweet magic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(357, 7, '', 'Romance Fresh', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(358, 7, '', 'Romance Fresh Natural Dandy', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(359, 7, '', 'Faro Twins', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(360, 7, '', 'smart blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(361, 7, '', 'Valentine Carisma blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(362, 7, '', 'Serina Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(363, 7, '', 'Blackout Wide', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(364, 7, '', 'Samrt Zelkova', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(365, 7, '', 'Blackout Lime', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(366, 7, '', 'Jasmine blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(367, 7, '', 'Elis Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(368, 7, '', 'Orion', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(369, 7, '', 'Hera', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(370, 7, '', 'Blackout Graphic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(371, 7, '', 'Blackout Slope', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(372, 7, '', 'Blackout Combi', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(373, 7, '', 'Nova', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(374, 7, '', 'Sunset blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(375, 7, '', 'Oscar blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(376, 7, '', 'Ruby Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(377, 7, '', 'Pretty', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(378, 7, '', 'Smart Pola', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(379, 7, '', 'Chrome', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(380, 7, '', 'Smart Anna Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(381, 7, '', 'Motive', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(382, 7, '', 'Magic 5', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(383, 7, '', 'Elegant Marble Roman', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(384, 7, '', 'Elegant Diamond Roman', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(385, 7, '', 'Vague blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(386, 7, '', 'Roller Lasor', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(387, 7, '', 'Triple Printing', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(388, 7, '', 'Roller Muji Printing', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:22', 0),
(389, 7, '', 'Combi Printing', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(390, 7, '', 'Roller B/O Printing', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(391, 7, '', 'Picaso Black Edition', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(392, 7, '', 'Muji', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(393, 7, '', 'Acro', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(394, 7, '', 'Riga', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(395, 7, '', 'Scarlet', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(396, 7, '', 'Prestige', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(397, 7, '', 'Hurricane', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(398, 7, '', 'Eco Amalfy', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(399, 7, '', 'Guess', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(400, 7, '', 'Silkroad', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(401, 7, '', 'Renaissance', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(402, 7, '', 'New Obidose', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(403, 7, '', 'Base', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(404, 7, '', 'Neo Silkroad', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(405, 7, '', 'Silk Look', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(406, 7, '', 'Woodlook Elegant BLACKOUT', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(407, 7, '', 'Stucco', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(408, 7, '', 'Venitian Foam', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(409, 7, '', 'Woodlook Single Foam', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(410, 7, '', 'Mars Foam', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(411, 7, '', 'VX Screen 3000-1%', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(412, 7, '', 'VX Screen 3000-3%', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(413, 7, '', 'VX Screen 3000-5%', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(414, 7, '', 'BELUGA 2', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(415, 7, '', 'Swan', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(416, 7, '', 'Venezia', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(417, 7, '', 'Shangrila Blackout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:23', 0),
(418, 7, '', 'UV graphic', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(419, 7, '', 'Printing (Linen/Cloud)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(420, 7, '', 'Cubic Triple-winplus', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(421, 7, '', 'Vision Shade 2', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(422, 7, '', 'Albery', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(423, 7, '', 'Aura', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(424, 7, '', 'Ayana-White', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(425, 7, '', 'Banbury', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(426, 7, '', 'Birdsong', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(427, 7, '', 'Blossom', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(428, 7, '', 'Carnival', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(429, 7, '', 'Carnival SPC Low E', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(430, 7, '', 'Chatsworth', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(431, 7, '', 'Cherry Blossom(Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(432, 7, '', 'Classic (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(433, 7, '', 'Collina (Blacout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(434, 7, '', 'Como', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(435, 7, '', 'Coral-White', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(436, 7, '', 'Crush (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(437, 7, '', 'Crystal (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(438, 7, '', 'Flutter', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(439, 7, '', 'Hampton', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(440, 7, '', 'Harmony', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(441, 7, '', 'Harmony (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(442, 7, '', 'Jurassic BO', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(443, 7, '', 'Linara', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(444, 7, '', 'Magnolia (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(445, 7, '', 'Maine (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:24', 0),
(446, 7, '', 'Marble BO', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(447, 7, '', 'Meadow', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(448, 7, '', 'Melton', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(449, 7, '', 'Monroe', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(450, 7, '', 'Monterey', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(451, 7, '', 'Nimbus BO', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(452, 7, '', 'Nimbus LF', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(453, 7, '', 'Night Night', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(454, 7, '', 'Oslo', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(455, 7, '', 'Parchment', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(456, 7, '', 'Portland', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(457, 7, '', 'Prima', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(458, 7, '', 'Prima Dimout', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(459, 7, '', 'Ravello', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(460, 7, '', 'Ritz', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(461, 7, '', 'Shot Silk BO', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(462, 7, '', 'Sparkle', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(463, 7, '', 'Tundra', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(464, 7, '', 'Vermont', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(465, 7, '', 'Versailles (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(466, 7, '', 'Willow (Blackout)', 1, 4, 0, '2019-09-18', '2019-09-18 10:05:25', 0),
(467, 7, 'Pattern', 'Wonderland (Blackout)', 1, 4, 4, '2019-09-18', '2019-09-17 13:00:00', 0),
(468, 7, 'Pattern', 'Woodland', 1, 4, 4, '2019-09-18', '2019-09-17 13:00:00', 0),
(469, 8, 'Pattern', 'pet-1', 1, 8, 0, '2019-09-19', '2019-09-19 11:09:45', 0),
(470, 9, 'Pattern', 'per 1', 1, 12, 0, '2019-09-20', '2019-09-20 06:02:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_card_tbl`
--

CREATE TABLE `payment_card_tbl` (
  `id` int(11) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `card_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_holder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cvv` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_check_tbl`
--

CREATE TABLE `payment_check_tbl` (
  `id` int(11) NOT NULL,
  `payment_id` bigint(20) NOT NULL,
  `check_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `check_image` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_tbl`
--

CREATE TABLE `payment_tbl` (
  `payment_id` bigint(20) NOT NULL,
  `quotation_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` float NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `price_chaild_style`
--

CREATE TABLE `price_chaild_style` (
  `id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `row` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_chaild_style`
--

INSERT INTO `price_chaild_style` (`id`, `style_id`, `row`) VALUES
(255, 21, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',78\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(256, 21, '30\'\',198,256,306,358,409,454,510,560,599,651,698,743,797,877,938'),
(257, 21, '36\'\',245,306,358,411,472,510,564,621,659,715,788,835,875,963,1030'),
(258, 21, '42\'\',298,336,392,449,513,572,633,698,733,795,868,907,957,1053,1126'),
(259, 21, '48\'\',306,397,425,477,546,614,689,757,764,828,948,1018,1086,1194,1278'),
(260, 21, '54\'\',322,404,461,527,604,677,753,830,875,948,1047,1143,1197,1317,1409'),
(261, 21, '60\'\',358,433,489,581,663,727,811,891,980,1060,1121,1201,1279,1407,1505'),
(262, 21, '66\'\',395,475,513,628,720,793,877,966,1068,1159,1246,1336,1425,1568,1677'),
(263, 21, '72\'\',430,499,581,672,769,851,941,1034,1154,1248,1324,1418,1510,1661,1778'),
(264, 21, '78\'\',449,520,625,736,840,927,1021,1124,1230,1331,1425,1528,1630,1793,1919'),
(265, 21, '84\'\',466,550,659,788,900,983,1112,1225,1310,1418,1536,1646,1756,1931,2066'),
(266, 21, '90\'\',499,585,701,820,936,1054,1169,1286,1415,1531,1637,1754,1869,2056,2200'),
(267, 21, '96\'\',512,606,727,842,962,1108,1209,1329,1460,1580,1712,1834,1958,2153,2304'),
(268, 21, '102\'\',524,619,743,882,1009,1133,1267,1392,1535,1663,1794,1923,2050,2255,2413'),
(269, 21, '108\'\',560,694,833,933,1065,1208,1342,1476,1651,1789,1862,1996,2128,2341,2505'),
(270, 22, ',   24\'\',   30\'\',   36\'\',   42\'\',   48\'\',   54\'\',   60\'\',   66\'\',   72\'\',   78\'\',   84\'\',   90\'\',   96\'\',102\'\',108\'\''),
(271, 22, '   30\'\',201,241,285,318,335,379,444,460,475,574,610,683,759,857,960'),
(272, 22, '   36\'\',233,273,316,354,375,435,503,526,540,631,693,759,844,953,1068'),
(273, 22, '   42\'\',255,301,349,393,415,485,560,576,608,709,768,858,953,1076,1205'),
(274, 22, '   48\'\',276,330,381,429,460,536,615,651,683,786,860,969,1076,1216,1362'),
(275, 22, '   54\'\',305,363,414,469,503,593,666,724,766,881,950,1066,1185,1339,1500'),
(276, 22, '   60\'\',335,393,450,509,549,648,721,786,833,956,1033,1160,1288,1455,1629'),
(277, 22, '   66\'\',364,424,484,551,604,708,793,863,915,1045,1124,1305,1450,1639,1835'),
(278, 22, '   72\'\',398,450,516,593,655,768,864,914,973,1110,1198,1349,1499,1694,1897'),
(279, 22, '   78\'\',411,460,531,608,676,789,889,945,1040,1184,1281,1439,1599,1807,2023'),
(280, 22, '   84\'\',443,494,565,654,726,831,905,1011,1118,1266,1366,1539,1709,1931,2163'),
(281, 22, '   90\'\',473,524,599,694,769,879,956,1074,1184,1330,1455,1638,1820,2057,2303'),
(282, 22, '   96\'\',490,541,618,723,803,925,1045,1146,1259,1426,1544,1736,1928,2178,2439'),
(283, 22, '  102\'\',513,571,653,769,854,983,1085,1214,1339,1513,1636,1844,2048,2314,2591'),
(284, 22, '  108\'\',539,594,685,806,898,1033,1136,1278,1408,1586,1718,1933,2148,2427,2718'),
(285, 22, '  114\'\',563,621,724,853,948,1086,1201,1344,1485,1678,1808,2033,2259,2552,2859'),
(286, 22, '  120\'\',591,651,754,891,994,1138,1226,1408,1549,1744,1893,2130,2366,2674,2995'),
(287, 22, '  126\'\',610,680,798,936,1043,1188,1315,1478,1626,1720,1978,2235,2483,2805,3142'),
(288, 23, ',   24\'\',   30\'\',   36\'\',   42\'\',   48\'\',   54\'\',   60\'\',   66\'\',   72\'\',   78\'\',   84\'\',   90\'\',   96\'\',102\'\',108\'\''),
(289, 23, '   30\'\',161,193,228,254,268,303,355,368,380,459,488,546,607,686,768'),
(290, 23, '   36\'\',186,218,253,283,300,348,402,421,432,505,554,607,675,763,854'),
(291, 23, '   42\'\',204,241,279,314,332,388,448,461,486,567,614,686,762,861,964'),
(292, 23, '   48\'\',221,264,305,343,368,429,492,521,546,629,688,775,861,973,1090'),
(293, 23, '   54\'\',244,290,331,375,402,474,533,579,613,705,760,853,948,1071,1200'),
(294, 23, '   60\'\',268,314,360,407,439,518,577,629,666,765,826,928,1030,1164,1304'),
(295, 23, '   66\'\',291,339,387,441,483,566,634,690,732,836,899,1044,1160,1311,1468'),
(296, 23, '   72\'\',318,360,413,474,524,614,691,731,778,888,958,1079,1199,1355,1517'),
(297, 23, '   78\'\',329,368,425,486,541,631,711,756,832,947,1025,1151,1279,1445,1619'),
(298, 23, '   84\'\',354,395,452,523,581,665,724,809,894,1013,1093,1231,1367,1545,1730'),
(299, 23, '   90\'\',378,419,479,555,615,703,765,859,947,1064,1164,1310,1456,1645,1843'),
(300, 23, '   96\'\',392,433,494,578,642,740,836,917,1007,1141,1235,1389,1542,1742,1952'),
(301, 23, '  102\'\',410,457,522,615,683,786,868,971,1071,1210,1309,1475,1638,1851,2073'),
(302, 23, '  108\'\',431,475,548,645,718,826,909,1022,1126,1269,1374,1546,1718,1941,2174'),
(303, 23, '  114\'\',450,497,579,682,758,869,961,1075,1188,1342,1446,1626,1807,2042,2287'),
(304, 23, '  120\'\',473,521,603,713,795,910,981,1126,1239,1395,1514,1704,1893,2139,2396'),
(305, 23, '  126\'\',488,544,638,749,834,950,1052,1182,1301,1376,1582,1788,1986,2244,2513'),
(306, 24, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',78\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(307, 24, '30\'\',114,147,176,206,235,261,293,322,344,374,401,427,458,504,539'),
(308, 24, '36\'\',141,176,206,236,271,293,324,357,379,411,453,480,503,553,592'),
(309, 24, '42\'\',171,193,225,258,295,329,364,401,421,457,499,521,550,605,647'),
(310, 24, '48\'\',176,228,244,274,314,353,396,435,439,476,545,585,624,686,734'),
(311, 24, '54\'\',185,232,265,303,347,389,433,477,503,545,602,657,688,757,810'),
(312, 24, '60\'\',206,249,281,334,381,418,466,512,563,609,644,690,735,809,865'),
(313, 24, '66\'\',227,273,295,361,414,456,504,555,614,666,716,768,819,901,964'),
(314, 24, '72\'\',247,287,334,386,442,489,541,594,663,717,761,815,868,955,1022'),
(315, 24, '78\'\',258,299,359,423,483,533,587,646,707,765,819,878,937,1031,1103'),
(316, 24, '84\'\',268,316,379,453,517,565,639,704,753,815,883,946,1009,1110,1188'),
(317, 24, '90\'\',287,336,403,471,538,606,672,739,813,880,941,1008,1074,1181,1264'),
(318, 24, '96\'\',294,348,418,484,553,637,695,764,839,908,984,1054,1125,1238,1324'),
(319, 24, '102\'\',301,356,427,507,580,651,728,800,882,956,1031,1105,1178,1296,1387'),
(320, 24, '108\'\',322,399,479,536,612,694,771,848,949,1028,1070,1147,1223,1345,1439'),
(321, 25, ',24\'\',30\'\',36\'\',42\'\',48\'\',54\'\',60\'\',66\'\',72\'\',78\'\',84\'\',90\'\',96\'\',102\'\',108\'\''),
(322, 25, '30\'\',137,176,211,247,282,313,352,386,413,449,481,512,550,605,647'),
(323, 25, '36\'\',169,211,247,283,325,352,389,428,455,493,544,576,604,664,710'),
(324, 25, '42\'\',205,232,270,310,354,395,437,481,505,548,599,625,660,726,777'),
(325, 25, '48\'\',211,274,293,329,377,424,475,522,527,571,654,702,749,824,881'),
(326, 25, '54\'\',222,278,318,364,416,467,520,572,604,654,722,788,826,908,972'),
(327, 25, '60\'\',247,299,337,401,457,502,559,614,676,731,773,828,882,970,1038'),
(328, 25, '66\'\',272,328,354,433,497,547,605,666,737,799,859,922,983,1081,1157'),
(329, 25, '72\'\',296,344,401,463,530,587,649,713,796,860,913,978,1042,1146,1226'),
(330, 25, '78\'\',310,359,431,508,580,640,704,775,848,918,983,1054,1124,1237,1323'),
(331, 25, '84\'\',322,379,455,544,620,678,767,845,904,978,1060,1135,1211,1332,1425'),
(332, 25, '90\'\',344,403,484,565,646,727,806,887,976,1056,1129,1210,1289,1418,1517'),
(333, 25, '96\'\',353,418,502,581,664,764,834,917,1007,1090,1181,1265,1350,1485,1589'),
(334, 25, '102\'\',361,427,512,608,696,781,874,960,1058,1147,1237,1326,1414,1555,1664'),
(335, 25, '108\'\',386,479,575,643,734,833,925,1018,1139,1234,1284,1376,1468,1614,1727'),
(336, 26, ',36,48,60,72,84,96,108'),
(337, 26, '36,121,126,132,140,149,155,202'),
(338, 26, '48,124,130,144,149,155,163,212'),
(339, 26, '60,128,135,149,159,169,179,234'),
(340, 26, '72,131,148,158,171,184,195,243'),
(341, 26, '84,135,156,171,184,197,212,266'),
(342, 26, '96,148,165,178,195,212,228,288'),
(343, 26, '108,153,171,189,207,225,244,306'),
(344, 26, '120,162,180,199,220,239,259,324'),
(345, 27, ',36,48,60,72,84,96,108'),
(346, 27, '36,134,140,147,155,165,172,224'),
(347, 27, '48,138,144,160,165,172,181,235'),
(348, 27, '60,142,150,165,177,188,199,260'),
(349, 27, '72,146,164,175,190,204,217,270'),
(350, 27, '84,150,173,190,204,219,235,295'),
(351, 27, '96,164,183,198,217,235,253,320'),
(352, 27, '108,170,190,210,230,250,271,340'),
(353, 27, '120,180,200,221,244,266,288,360'),
(354, 28, ',36,48,60,72,84,96,108'),
(355, 28, '36,150,157,173,183,195,203,264'),
(356, 28, '48,155,161,189,195,203,214,277'),
(357, 28, '60,159,168,195,209,222,235,307'),
(358, 28, '72,164,184,207,224,241,256,319'),
(359, 28, '84,168,194,224,241,258,277,348'),
(360, 28, '96,184,205,234,256,277,299,378'),
(361, 28, '108,190,213,248,271,295,320,401'),
(362, 28, '120,202,224,261,288,314,340,425'),
(363, 29, ',36,48,60,72,84,96,108'),
(364, 29, '36,161,168,185,195,208,217,309'),
(365, 29, '48,166,173,202,208,217,228,324'),
(366, 29, '60,170,180,208,223,237,251,359'),
(367, 29, '72,175,197,221,239,257,273,373'),
(368, 29, '84,180,208,239,257,276,296,407'),
(369, 29, '96,197,220,249,273,296,319,442'),
(370, 29, '108,204,228,265,290,315,341,469'),
(371, 29, '120,216,240,278,307,335,363,497'),
(372, 30, ',36,48,60,72,84,96,108'),
(373, 30, '36,174,182,201,212,226,236,340'),
(374, 30, '48,179,187,219,226,236,248,357'),
(375, 30, '60,185,195,226,242,258,273,395'),
(376, 30, '72,190,213,240,260,279,297,410'),
(377, 30, '84,195,225,260,279,300,322,448'),
(378, 30, '96,213,238,271,297,322,347,486'),
(379, 30, '108,221,247,288,315,343,371,516'),
(380, 30, '120,234,260,303,334,364,395,547'),
(381, 31, ',36,48,60,72,84,96,108'),
(382, 31, '36,190,199,219,231,246,256,374'),
(383, 31, '48,196,204,238,246,256,270,393'),
(384, 31, '60,202,213,246,264,280,297,434'),
(385, 31, '72,207,233,261,283,304,323,451'),
(386, 31, '84,213,246,283,304,326,350,493'),
(387, 31, '96,233,260,295,323,350,377,535'),
(388, 31, '108,241,270,313,343,373,404,568'),
(389, 31, '120,256,284,329,364,396,429,601'),
(390, 32, ',36,48,60,72,84,96,108'),
(391, 32, '36,208,217,240,253,269,280,400'),
(392, 32, '48,214,223,261,269,280,295,420'),
(393, 32, '60,220,233,269,289,306,324,465'),
(394, 32, '72,226,254,285,310,333,354,483'),
(395, 32, '84,233,268,310,333,357,383,527'),
(396, 32, '96,254,284,323,354,383,412,572'),
(397, 32, '108,264,295,342,375,408,442,608'),
(398, 32, '120,279,310,360,398,434,469,643'),
(399, 33, ',36,48,60,72,84,96,108'),
(400, 33, '36,221,231,254,268,285,298,420'),
(401, 33, '48,228,238,277,285,298,313,441'),
(402, 33, '60,234,248,285,306,325,344,488'),
(403, 33, '72,241,271,303,329,353,375,507'),
(404, 33, '84,248,285,329,353,379,407,554'),
(405, 33, '96,271,302,343,375,407,438,601'),
(406, 33, '108,281,314,363,398,433,469,638'),
(407, 33, '120,297,330,382,422,460,498,676'),
(408, 34, ',36,48,60,72,84,96,108'),
(409, 34, '36,240,251,276,291,310,323,433'),
(410, 34, '48,247,258,301,310,323,340,454'),
(411, 34, '60,254,269,310,333,353,374,503'),
(412, 34, '72,261,294,329,357,384,408,522'),
(413, 34, '84,269,310,357,384,412,442,570'),
(414, 34, '96,294,328,372,408,442,476,619'),
(415, 34, '108,304,340,395,432,470,509,657'),
(416, 34, '120,322,358,415,459,500,541,696'),
(417, 35, ',36,48,60,72,84,96,108'),
(418, 35, '36,259,270,298,315,335,349,455'),
(419, 35, '48,266,278,325,335,349,367,477'),
(420, 35, '60,274,290,335,359,382,404,528'),
(421, 35, '72,282,317,355,386,414,441,548'),
(422, 35, '84,290,334,386,414,445,477,599'),
(423, 35, '96,317,353,402,441,477,514,650'),
(424, 35, '108,328,367,426,467,508,550,690'),
(425, 35, '120,347,386,449,495,540,585,731'),
(426, 36, ',36,48,60,72,84,96,108'),
(427, 36, '36,185,203,231,252,275,295,368'),
(428, 36, '48,201,223,266,287,311,337,416'),
(429, 36, '60,217,234,281,313,343,373,463'),
(430, 36, '72,233,267,310,349,386,422,506'),
(431, 36, '84,241,291,345,386,428,471,566'),
(432, 36, '96,267,318,372,422,471,520,627'),
(433, 36, '108,284,337,404,458,513,569,682'),
(434, 36, '120,305,362,434,496,556,617,736'),
(435, 37, ',36,48,60,72,84,96,108'),
(436, 37, '36,260,287,319,347,380,407,543'),
(437, 37, '48,277,307,366,395,428,464,610'),
(438, 37, '60,299,333,401,447,491,535,702'),
(439, 37, '72,321,381,443,499,554,607,767'),
(440, 37, '84,343,416,494,554,615,678,859'),
(441, 37, '96,381,456,533,607,678,750,951'),
(442, 37, '108,406,484,579,659,739,821,1034'),
(443, 37, '120,437,521,624,714,802,891,1118'),
(444, 38, ',36,48,60,72,84,96,108'),
(445, 38, '36,149,164,180,197,215,231,287'),
(446, 38, '48,162,180,208,225,244,265,326'),
(447, 38, '60,175,190,220,245,269,293,363'),
(448, 38, '72,189,216,243,274,304,333,397'),
(449, 38, '84,197,236,271,304,337,372,446'),
(450, 38, '96,218,258,293,333,372,411,494'),
(451, 38, '108,232,274,318,361,405,450,538'),
(452, 38, '120,250,294,342,391,440,488,581'),
(453, 39, ',36,48,60,72,84,96,108'),
(454, 39, '36,183,201,229,250,273,292,364'),
(455, 39, '48,199,221,263,284,307,332,411'),
(456, 39, '60,215,239,287,320,352,383,474'),
(457, 39, '72,230,273,318,358,396,434,519'),
(458, 39, '84,246,298,354,396,440,485,582'),
(459, 39, '96,273,326,382,434,485,536,644'),
(460, 39, '108,290,346,415,472,529,587,701'),
(461, 39, '120,313,372,446,510,573,636,758'),
(462, 40, ',24\'\',36\'\',48\'\',60\'\',72\'\',84\'\',96\'\',108\'\',120\'\',132\'\',144\'\',156\'\',168\'\''),
(463, 40, '36\'\',164,181,198,216,237,254,316,40,40,53,66,79,92'),
(464, 40, '48\'\',179,198,228,247,268,291,358,40,33,44,55,66,77'),
(465, 40, '60\'\',193,209,242,270,296,323,399,53,44,59,74,88,103'),
(466, 40, '72\'\',207,238,268,301,334,366,437,66,55,63,83,99,116'),
(467, 40, '84\'\',216,259,298,334,371,409,490,79,66,79,99,119,139'),
(468, 40, '96\'\',240,284,322,366,409,452,543,92,71,93,116,139,162'),
(469, 40, '108\'\',256,301,349,398,446,495,591,106,82,108,132,159,185'),
(470, 40, '120\'\',275,324,376,430,484,537,639,119,92,119,149,179,209'),
(471, 40, '132\'\',0,0,0,0,0,0,0,132,102,132,166,199,232'),
(472, 40, '144\'\',0,0,0,0,0,0,0,0,0,0,0,0,0'),
(473, 40, '156\'\',0,0,0,0,0,0,0,0,0,0,0,0,0'),
(474, 40, '168\'\',0,0,0,0,0,0,0,0,0,0,0,0,0'),
(475, 40, '180\'\',0,0,0,0,0,0,0,0,0,0,0,0,0'),
(476, 40, '192\'\',40,53,66,79,92,106,119,40,40,53,66,79,92'),
(477, 40, '204\'\',202,221,252,275,300,321,401,40,39,52,64,77,90'),
(478, 40, '216\'\',219,243,289,312,337,365,452,53,52,69,86,103,120'),
(479, 40, '228\'\',236,263,316,352,387,421,521,66,64,82,107,129,150'),
(480, 41, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(481, 41, '0,182,205,228,251,274,296,319,342,365,388,410,433,456,479,502,524,547,570'),
(482, 42, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(483, 42, '0,205,230,256,281,307,332,358,384,409,435,460,486,512,537,563,588,614,639'),
(484, 43, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(485, 43, '0,147,166,184,202,221,239,258,276,294,313,331,350,368,386,405,423,442,460'),
(486, 44, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(487, 44, '0,91,102,113,125,136,147,159,170,181,193,204,215,227,238,249,261,272,284'),
(490, 46, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(491, 46, '0,243,273,304,334,364,395,425,455,486,516,546,577,607,638,668,698,729,759'),
(492, 47, ',22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73'),
(493, 47, '0,80,80,87,87,94,94,101,101,108,108,116,116,123,123,130,130,137,141,144,144,152,152,159,162,166,166,173,173,180,184,188,188,195,195,202,206,209,209,216,216,216,227,227,227,238,238,238,249,252,252,260,260'),
(494, 45, ',24\'\',27\'\',30\'\',33\'\',36\'\',39\'\',42\'\',45\'\',48\'\',51\'\',54\'\',57\'\',60\'\',63\'\',66\'\',69\'\',72\'\',75\'\''),
(495, 45, '0\'\',243,273,304,334,364,395,425,455,486,516,546,577,607,638,668,698,729,759'),
(496, 48, 'aaaa'),
(497, 48, 'aaaa'),
(498, 49, 'sddd'),
(499, 49, 'ggg');

-- --------------------------------------------------------

--
-- Table structure for table `price_model_mapping_tbl`
--

CREATE TABLE `price_model_mapping_tbl` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pattern_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `price_model_mapping_tbl`
--

INSERT INTO `price_model_mapping_tbl` (`id`, `group_id`, `product_id`, `pattern_id`) VALUES
(148, 33, 28, '172'),
(149, 35, 29, '173'),
(150, 31, 30, '174'),
(151, 31, 30, '175'),
(152, 27, 31, '176'),
(153, 27, 31, '177'),
(154, 28, 31, '178'),
(155, 28, 31, '179'),
(156, 28, 31, '180'),
(157, 28, 31, '181'),
(158, 28, 31, '182'),
(159, 28, 31, '183'),
(160, 28, 31, '184'),
(161, 28, 31, '185'),
(162, 28, 31, '186'),
(163, 28, 31, '187'),
(164, 29, 31, '188'),
(165, 29, 31, '189'),
(166, 29, 31, '190'),
(167, 29, 31, '191'),
(168, 29, 31, '192'),
(169, 29, 31, '193'),
(170, 29, 31, '194'),
(171, 29, 31, '195'),
(172, 29, 31, '196'),
(173, 29, 31, '197'),
(174, 29, 31, '198'),
(175, 29, 31, '199'),
(176, 30, 31, '200'),
(177, 30, 31, '201'),
(178, 30, 31, '202'),
(179, 30, 31, '203'),
(180, 30, 31, '204'),
(181, 30, 31, '205'),
(182, 30, 31, '206'),
(183, 30, 31, '207'),
(184, 30, 31, '208'),
(185, 30, 31, '209'),
(186, 30, 31, '210'),
(187, 30, 31, '211'),
(188, 30, 31, '212'),
(189, 30, 31, '213'),
(190, 30, 31, '214'),
(191, 30, 31, '215'),
(192, 30, 31, '216'),
(193, 30, 31, '217'),
(194, 31, 31, '218'),
(195, 31, 31, '219'),
(196, 31, 31, '220'),
(197, 31, 31, '221'),
(198, 31, 31, '222'),
(199, 31, 31, '223'),
(200, 31, 31, '224'),
(201, 31, 31, '225'),
(202, 31, 31, '226'),
(203, 31, 31, '227'),
(204, 31, 31, '228'),
(205, 32, 31, '229'),
(206, 32, 31, '230'),
(207, 33, 31, '231'),
(208, 35, 31, '232'),
(209, 35, 31, '233'),
(210, 36, 32, '234'),
(211, 37, 32, '235'),
(212, 38, 32, '236'),
(213, 38, 32, '237'),
(214, 39, 32, '238'),
(215, 32, 33, '239'),
(216, 27, 34, '184'),
(217, 27, 34, '240'),
(218, 27, 34, '241'),
(219, 27, 34, '242'),
(220, 27, 34, '243'),
(221, 27, 34, '244'),
(222, 27, 34, '245'),
(223, 27, 34, '246'),
(224, 27, 34, '247'),
(225, 28, 34, '248'),
(226, 28, 34, '249'),
(227, 28, 34, '250'),
(228, 28, 34, '251'),
(229, 29, 34, '252'),
(230, 29, 34, '253'),
(231, 29, 34, '254'),
(232, 29, 34, '255'),
(233, 30, 34, '256'),
(234, 30, 34, '257'),
(235, 31, 34, '258'),
(236, 40, 35, '259'),
(237, 40, 35, '260'),
(238, 40, 35, '261'),
(239, 32, 36, '262'),
(240, 32, 36, '263'),
(241, 32, 36, '264'),
(242, 32, 36, '265'),
(243, 35, 36, '266'),
(244, 35, 36, '267'),
(245, 35, 36, '268'),
(246, 32, 37, '269'),
(247, 26, 38, '271'),
(248, 26, 38, '305'),
(249, 27, 38, '280'),
(250, 27, 38, '286'),
(251, 27, 38, '288'),
(252, 27, 38, '302'),
(253, 27, 38, '310'),
(254, 27, 38, '316'),
(255, 28, 38, '270'),
(256, 28, 38, '272'),
(257, 28, 38, '274'),
(258, 28, 38, '275'),
(259, 28, 38, '277'),
(260, 28, 38, '278'),
(261, 28, 38, '282'),
(262, 28, 38, '283'),
(263, 28, 38, '287'),
(264, 28, 38, '289'),
(265, 28, 38, '291'),
(266, 28, 38, '295'),
(267, 28, 38, '298'),
(268, 28, 38, '300'),
(269, 28, 38, '303'),
(270, 28, 38, '304'),
(271, 28, 38, '306'),
(272, 28, 38, '308'),
(273, 28, 38, '311'),
(274, 28, 38, '312'),
(275, 29, 38, '273'),
(276, 29, 38, '276'),
(277, 29, 38, '290'),
(278, 29, 38, '294'),
(279, 29, 38, '301'),
(280, 29, 38, '315'),
(281, 30, 38, '296'),
(282, 30, 38, '299'),
(283, 31, 38, '293'),
(284, 31, 38, '314'),
(285, 32, 38, '297'),
(286, 32, 38, '307'),
(287, 33, 38, '279'),
(288, 33, 38, '284'),
(289, 34, 38, '281'),
(290, 34, 38, '285'),
(291, 34, 38, '292'),
(292, 34, 38, '309'),
(293, 34, 38, '313'),
(294, 0, 55, '470');

-- --------------------------------------------------------

--
-- Table structure for table `price_style`
--

CREATE TABLE `price_style` (
  `row_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_style`
--

INSERT INTO `price_style` (`row_id`, `style_id`, `row`, `col`, `price`) VALUES
(2339, 21, 24, 30, 198),
(2340, 21, 30, 30, 256),
(2341, 21, 36, 30, 306),
(2342, 21, 42, 30, 358),
(2343, 21, 48, 30, 409),
(2344, 21, 54, 30, 454),
(2345, 21, 60, 30, 510),
(2346, 21, 66, 30, 560),
(2347, 21, 72, 30, 599),
(2348, 21, 84, 30, 90),
(2349, 21, 84, 30, 698),
(2350, 21, 90, 30, 743),
(2351, 21, 96, 30, 797),
(2352, 21, 102, 30, 877),
(2353, 21, 108, 30, 938),
(2354, 21, 24, 36, 245),
(2355, 21, 30, 36, 306),
(2356, 21, 36, 36, 358),
(2357, 21, 42, 36, 411),
(2358, 21, 48, 36, 472),
(2359, 21, 54, 36, 510),
(2360, 21, 60, 36, 564),
(2361, 21, 66, 36, 621),
(2362, 21, 72, 36, 659),
(2363, 21, 84, 36, 96),
(2364, 21, 84, 36, 788),
(2365, 21, 90, 36, 835),
(2366, 21, 96, 36, 875),
(2367, 21, 102, 36, 963),
(2368, 21, 108, 36, 1030),
(2369, 21, 24, 42, 298),
(2370, 21, 30, 42, 336),
(2371, 21, 36, 42, 392),
(2372, 21, 42, 42, 449),
(2373, 21, 48, 42, 513),
(2374, 21, 54, 42, 572),
(2375, 21, 60, 42, 633),
(2376, 21, 66, 42, 698),
(2377, 21, 72, 42, 733),
(2378, 21, 84, 42, 102),
(2379, 21, 84, 42, 868),
(2380, 21, 90, 42, 907),
(2381, 21, 96, 42, 957),
(2382, 21, 102, 42, 1053),
(2383, 21, 108, 42, 1126),
(2384, 21, 24, 48, 306),
(2385, 21, 30, 48, 397),
(2386, 21, 36, 48, 425),
(2387, 21, 42, 48, 477),
(2388, 21, 48, 48, 546),
(2389, 21, 54, 48, 614),
(2390, 21, 60, 48, 689),
(2391, 21, 66, 48, 757),
(2392, 21, 72, 48, 764),
(2393, 21, 84, 48, 108),
(2394, 21, 84, 48, 948),
(2395, 21, 90, 48, 1018),
(2396, 21, 96, 48, 1086),
(2397, 21, 102, 48, 1194),
(2398, 21, 108, 48, 1278),
(2399, 21, 24, 54, 322),
(2400, 21, 30, 54, 404),
(2401, 21, 36, 54, 461),
(2402, 21, 42, 54, 527),
(2403, 21, 48, 54, 604),
(2404, 21, 54, 54, 677),
(2405, 21, 60, 54, 753),
(2406, 21, 66, 54, 830),
(2407, 21, 72, 54, 875),
(2408, 21, 84, 54, 948),
(2409, 21, 84, 54, 1047),
(2410, 21, 90, 54, 1143),
(2411, 21, 96, 54, 1197),
(2412, 21, 102, 54, 1317),
(2413, 21, 108, 54, 1409),
(2414, 21, 24, 60, 358),
(2415, 21, 30, 60, 433),
(2416, 21, 36, 60, 489),
(2417, 21, 42, 60, 581),
(2418, 21, 48, 60, 663),
(2419, 21, 54, 60, 727),
(2420, 21, 60, 60, 811),
(2421, 21, 66, 60, 891),
(2422, 21, 72, 60, 980),
(2423, 21, 84, 60, 1060),
(2424, 21, 84, 60, 1121),
(2425, 21, 90, 60, 1201),
(2426, 21, 96, 60, 1279),
(2427, 21, 102, 60, 1407),
(2428, 21, 108, 60, 1505),
(2429, 21, 24, 66, 395),
(2430, 21, 30, 66, 475),
(2431, 21, 36, 66, 513),
(2432, 21, 42, 66, 628),
(2433, 21, 48, 66, 720),
(2434, 21, 54, 66, 793),
(2435, 21, 60, 66, 877),
(2436, 21, 66, 66, 966),
(2437, 21, 72, 66, 1068),
(2438, 21, 84, 66, 1159),
(2439, 21, 84, 66, 1246),
(2440, 21, 90, 66, 1336),
(2441, 21, 96, 66, 1425),
(2442, 21, 102, 66, 1568),
(2443, 21, 108, 66, 1677),
(2444, 21, 24, 72, 430),
(2445, 21, 30, 72, 499),
(2446, 21, 36, 72, 581),
(2447, 21, 42, 72, 672),
(2448, 21, 48, 72, 769),
(2449, 21, 54, 72, 851),
(2450, 21, 60, 72, 941),
(2451, 21, 66, 72, 1034),
(2452, 21, 72, 72, 1154),
(2453, 21, 84, 72, 1248),
(2454, 21, 84, 72, 1324),
(2455, 21, 90, 72, 1418),
(2456, 21, 96, 72, 1510),
(2457, 21, 102, 72, 1661),
(2458, 21, 108, 72, 1778),
(2459, 21, 24, 78, 449),
(2460, 21, 30, 78, 520),
(2461, 21, 36, 78, 625),
(2462, 21, 42, 78, 736),
(2463, 21, 48, 78, 840),
(2464, 21, 54, 78, 927),
(2465, 21, 60, 78, 1021),
(2466, 21, 66, 78, 1124),
(2467, 21, 72, 78, 1230),
(2468, 21, 84, 78, 1331),
(2469, 21, 84, 78, 1425),
(2470, 21, 90, 78, 1528),
(2471, 21, 96, 78, 1630),
(2472, 21, 102, 78, 1793),
(2473, 21, 108, 78, 1919),
(2474, 21, 24, 84, 466),
(2475, 21, 30, 84, 550),
(2476, 21, 36, 84, 659),
(2477, 21, 42, 84, 788),
(2478, 21, 48, 84, 900),
(2479, 21, 54, 84, 983),
(2480, 21, 60, 84, 1112),
(2481, 21, 66, 84, 1225),
(2482, 21, 72, 84, 1310),
(2483, 21, 84, 84, 1418),
(2484, 21, 84, 84, 1536),
(2485, 21, 90, 84, 1646),
(2486, 21, 96, 84, 1756),
(2487, 21, 102, 84, 1931),
(2488, 21, 108, 84, 2066),
(2489, 21, 24, 90, 499),
(2490, 21, 30, 90, 585),
(2491, 21, 36, 90, 701),
(2492, 21, 42, 90, 820),
(2493, 21, 48, 90, 936),
(2494, 21, 54, 90, 1054),
(2495, 21, 60, 90, 1169),
(2496, 21, 66, 90, 1286),
(2497, 21, 72, 90, 1415),
(2498, 21, 84, 90, 1531),
(2499, 21, 84, 90, 1637),
(2500, 21, 90, 90, 1754),
(2501, 21, 96, 90, 1869),
(2502, 21, 102, 90, 2056),
(2503, 21, 108, 90, 2200),
(2504, 21, 24, 96, 512),
(2505, 21, 30, 96, 606),
(2506, 21, 36, 96, 727),
(2507, 21, 42, 96, 842),
(2508, 21, 48, 96, 962),
(2509, 21, 54, 96, 1108),
(2510, 21, 60, 96, 1209),
(2511, 21, 66, 96, 1329),
(2512, 21, 72, 96, 1460),
(2513, 21, 84, 96, 1580),
(2514, 21, 84, 96, 1712),
(2515, 21, 90, 96, 1834),
(2516, 21, 96, 96, 1958),
(2517, 21, 102, 96, 2153),
(2518, 21, 108, 96, 2304),
(2519, 21, 24, 102, 524),
(2520, 21, 30, 102, 619),
(2521, 21, 36, 102, 743),
(2522, 21, 42, 102, 882),
(2523, 21, 48, 102, 1009),
(2524, 21, 54, 102, 1133),
(2525, 21, 60, 102, 1267),
(2526, 21, 66, 102, 1392),
(2527, 21, 72, 102, 1535),
(2528, 21, 84, 102, 1663),
(2529, 21, 84, 102, 1794),
(2530, 21, 90, 102, 1923),
(2531, 21, 96, 102, 2050),
(2532, 21, 102, 102, 2255),
(2533, 21, 108, 102, 2413),
(2534, 21, 24, 108, 560),
(2535, 21, 30, 108, 694),
(2536, 21, 36, 108, 833),
(2537, 21, 42, 108, 933),
(2538, 21, 48, 108, 1065),
(2539, 21, 54, 108, 1208),
(2540, 21, 60, 108, 1342),
(2541, 21, 66, 108, 1476),
(2542, 21, 72, 108, 1651),
(2543, 21, 84, 108, 1789),
(2544, 21, 84, 108, 1862),
(2545, 21, 90, 108, 1996),
(2546, 21, 96, 108, 2128),
(2547, 21, 102, 108, 2341),
(2548, 21, 108, 108, 2505),
(2549, 22, 24, 30, 201),
(2550, 22, 30, 30, 241),
(2551, 22, 36, 30, 285),
(2552, 22, 42, 30, 318),
(2553, 22, 48, 30, 335),
(2554, 22, 54, 30, 379),
(2555, 22, 60, 30, 444),
(2556, 22, 66, 30, 460),
(2557, 22, 72, 30, 475),
(2558, 22, 84, 30, 90),
(2559, 22, 84, 30, 610),
(2560, 22, 90, 30, 683),
(2561, 22, 96, 30, 759),
(2562, 22, 102, 30, 857),
(2563, 22, 108, 30, 960),
(2564, 22, 24, 36, 233),
(2565, 22, 30, 36, 273),
(2566, 22, 36, 36, 316),
(2567, 22, 42, 36, 354),
(2568, 22, 48, 36, 375),
(2569, 22, 54, 36, 435),
(2570, 22, 60, 36, 503),
(2571, 22, 66, 36, 526),
(2572, 22, 72, 36, 540),
(2573, 22, 84, 36, 96),
(2574, 22, 84, 36, 693),
(2575, 22, 90, 36, 759),
(2576, 22, 96, 36, 844),
(2577, 22, 102, 36, 953),
(2578, 22, 108, 36, 1068),
(2579, 22, 24, 42, 255),
(2580, 22, 30, 42, 301),
(2581, 22, 36, 42, 349),
(2582, 22, 42, 42, 393),
(2583, 22, 48, 42, 415),
(2584, 22, 54, 42, 485),
(2585, 22, 60, 42, 560),
(2586, 22, 66, 42, 576),
(2587, 22, 72, 42, 608),
(2588, 22, 84, 42, 102),
(2589, 22, 84, 42, 768),
(2590, 22, 90, 42, 858),
(2591, 22, 96, 42, 953),
(2592, 22, 102, 42, 1076),
(2593, 22, 108, 42, 1205),
(2594, 22, 24, 48, 276),
(2595, 22, 30, 48, 330),
(2596, 22, 36, 48, 381),
(2597, 22, 42, 48, 429),
(2598, 22, 48, 48, 460),
(2599, 22, 54, 48, 536),
(2600, 22, 60, 48, 615),
(2601, 22, 66, 48, 651),
(2602, 22, 72, 48, 683),
(2603, 22, 84, 48, 108),
(2604, 22, 84, 48, 860),
(2605, 22, 90, 48, 969),
(2606, 22, 96, 48, 1076),
(2607, 22, 102, 48, 1216),
(2608, 22, 108, 48, 1362),
(2609, 22, 24, 54, 305),
(2610, 22, 30, 54, 363),
(2611, 22, 36, 54, 414),
(2612, 22, 42, 54, 469),
(2613, 22, 48, 54, 503),
(2614, 22, 54, 54, 593),
(2615, 22, 60, 54, 666),
(2616, 22, 66, 54, 724),
(2617, 22, 72, 54, 766),
(2618, 22, 84, 54, 114),
(2619, 22, 84, 54, 950),
(2620, 22, 90, 54, 1066),
(2621, 22, 96, 54, 1185),
(2622, 22, 102, 54, 1339),
(2623, 22, 108, 54, 1500),
(2624, 22, 24, 60, 335),
(2625, 22, 30, 60, 393),
(2626, 22, 36, 60, 450),
(2627, 22, 42, 60, 509),
(2628, 22, 48, 60, 549),
(2629, 22, 54, 60, 648),
(2630, 22, 60, 60, 721),
(2631, 22, 66, 60, 786),
(2632, 22, 72, 60, 833),
(2633, 22, 84, 60, 120),
(2634, 22, 84, 60, 1033),
(2635, 22, 90, 60, 1160),
(2636, 22, 96, 60, 1288),
(2637, 22, 102, 60, 1455),
(2638, 22, 108, 60, 1629),
(2639, 22, 24, 66, 364),
(2640, 22, 30, 66, 424),
(2641, 22, 36, 66, 484),
(2642, 22, 42, 66, 551),
(2643, 22, 48, 66, 604),
(2644, 22, 54, 66, 708),
(2645, 22, 60, 66, 793),
(2646, 22, 66, 66, 863),
(2647, 22, 72, 66, 915),
(2648, 22, 84, 66, 126),
(2649, 22, 84, 66, 1124),
(2650, 22, 90, 66, 1305),
(2651, 22, 96, 66, 1450),
(2652, 22, 102, 66, 1639),
(2653, 22, 108, 66, 1835),
(2654, 22, 24, 72, 398),
(2655, 22, 30, 72, 450),
(2656, 22, 36, 72, 516),
(2657, 22, 42, 72, 593),
(2658, 22, 48, 72, 655),
(2659, 22, 54, 72, 768),
(2660, 22, 60, 72, 864),
(2661, 22, 66, 72, 914),
(2662, 22, 72, 72, 973),
(2663, 22, 84, 72, 1110),
(2664, 22, 84, 72, 1198),
(2665, 22, 90, 72, 1349),
(2666, 22, 96, 72, 1499),
(2667, 22, 102, 72, 1694),
(2668, 22, 108, 72, 1897),
(2669, 22, 24, 78, 411),
(2670, 22, 30, 78, 460),
(2671, 22, 36, 78, 531),
(2672, 22, 42, 78, 608),
(2673, 22, 48, 78, 676),
(2674, 22, 54, 78, 789),
(2675, 22, 60, 78, 889),
(2676, 22, 66, 78, 945),
(2677, 22, 72, 78, 1040),
(2678, 22, 84, 78, 1184),
(2679, 22, 84, 78, 1281),
(2680, 22, 90, 78, 1439),
(2681, 22, 96, 78, 1599),
(2682, 22, 102, 78, 1807),
(2683, 22, 108, 78, 2023),
(2684, 22, 24, 84, 443),
(2685, 22, 30, 84, 494),
(2686, 22, 36, 84, 565),
(2687, 22, 42, 84, 654),
(2688, 22, 48, 84, 726),
(2689, 22, 54, 84, 831),
(2690, 22, 60, 84, 905),
(2691, 22, 66, 84, 1011),
(2692, 22, 72, 84, 1118),
(2693, 22, 84, 84, 1266),
(2694, 22, 84, 84, 1366),
(2695, 22, 90, 84, 1539),
(2696, 22, 96, 84, 1709),
(2697, 22, 102, 84, 1931),
(2698, 22, 108, 84, 2163),
(2699, 22, 24, 90, 473),
(2700, 22, 30, 90, 524),
(2701, 22, 36, 90, 599),
(2702, 22, 42, 90, 694),
(2703, 22, 48, 90, 769),
(2704, 22, 54, 90, 879),
(2705, 22, 60, 90, 956),
(2706, 22, 66, 90, 1074),
(2707, 22, 72, 90, 1184),
(2708, 22, 84, 90, 1330),
(2709, 22, 84, 90, 1455),
(2710, 22, 90, 90, 1638),
(2711, 22, 96, 90, 1820),
(2712, 22, 102, 90, 2057),
(2713, 22, 108, 90, 2303),
(2714, 22, 24, 96, 490),
(2715, 22, 30, 96, 541),
(2716, 22, 36, 96, 618),
(2717, 22, 42, 96, 723),
(2718, 22, 48, 96, 803),
(2719, 22, 54, 96, 925),
(2720, 22, 60, 96, 1045),
(2721, 22, 66, 96, 1146),
(2722, 22, 72, 96, 1259),
(2723, 22, 84, 96, 1426),
(2724, 22, 84, 96, 1544),
(2725, 22, 90, 96, 1736),
(2726, 22, 96, 96, 1928),
(2727, 22, 102, 96, 2178),
(2728, 22, 108, 96, 2439),
(2729, 22, 24, 102, 513),
(2730, 22, 30, 102, 571),
(2731, 22, 36, 102, 653),
(2732, 22, 42, 102, 769),
(2733, 22, 48, 102, 854),
(2734, 22, 54, 102, 983),
(2735, 22, 60, 102, 1085),
(2736, 22, 66, 102, 1214),
(2737, 22, 72, 102, 1339),
(2738, 22, 84, 102, 1513),
(2739, 22, 84, 102, 1636),
(2740, 22, 90, 102, 1844),
(2741, 22, 96, 102, 2048),
(2742, 22, 102, 102, 2314),
(2743, 22, 108, 102, 2591),
(2744, 22, 24, 108, 539),
(2745, 22, 30, 108, 594),
(2746, 22, 36, 108, 685),
(2747, 22, 42, 108, 806),
(2748, 22, 48, 108, 898),
(2749, 22, 54, 108, 1033),
(2750, 22, 60, 108, 1136),
(2751, 22, 66, 108, 1278),
(2752, 22, 72, 108, 1408),
(2753, 22, 84, 108, 1586),
(2754, 22, 84, 108, 1718),
(2755, 22, 90, 108, 1933),
(2756, 22, 96, 108, 2148),
(2757, 22, 102, 108, 2427),
(2758, 22, 108, 108, 2718),
(2759, 22, 24, 114, 563),
(2760, 22, 30, 114, 621),
(2761, 22, 36, 114, 724),
(2762, 22, 42, 114, 853),
(2763, 22, 48, 114, 948),
(2764, 22, 54, 114, 1086),
(2765, 22, 60, 114, 1201),
(2766, 22, 66, 114, 1344),
(2767, 22, 72, 114, 1485),
(2768, 22, 84, 114, 1678),
(2769, 22, 84, 114, 1808),
(2770, 22, 90, 114, 2033),
(2771, 22, 96, 114, 2259),
(2772, 22, 102, 114, 2552),
(2773, 22, 108, 114, 2859),
(2774, 22, 24, 120, 591),
(2775, 22, 30, 120, 651),
(2776, 22, 36, 120, 754),
(2777, 22, 42, 120, 891),
(2778, 22, 48, 120, 994),
(2779, 22, 54, 120, 1138),
(2780, 22, 60, 120, 1226),
(2781, 22, 66, 120, 1408),
(2782, 22, 72, 120, 1549),
(2783, 22, 84, 120, 1744),
(2784, 22, 84, 120, 1893),
(2785, 22, 90, 120, 2130),
(2786, 22, 96, 120, 2366),
(2787, 22, 102, 120, 2674),
(2788, 22, 108, 120, 2995),
(2789, 22, 24, 126, 610),
(2790, 22, 30, 126, 680),
(2791, 22, 36, 126, 798),
(2792, 22, 42, 126, 936),
(2793, 22, 48, 126, 1043),
(2794, 22, 54, 126, 1188),
(2795, 22, 60, 126, 1315),
(2796, 22, 66, 126, 1478),
(2797, 22, 72, 126, 1626),
(2798, 22, 84, 126, 1720),
(2799, 22, 84, 126, 1978),
(2800, 22, 90, 126, 2235),
(2801, 22, 96, 126, 2483),
(2802, 22, 102, 126, 2805),
(2803, 22, 108, 126, 3142),
(2804, 23, 24, 30, 161),
(2805, 23, 30, 30, 193),
(2806, 23, 36, 30, 228),
(2807, 23, 42, 30, 254),
(2808, 23, 48, 30, 268),
(2809, 23, 54, 30, 303),
(2810, 23, 60, 30, 355),
(2811, 23, 66, 30, 368),
(2812, 23, 72, 30, 380),
(2813, 23, 84, 30, 90),
(2814, 23, 84, 30, 488),
(2815, 23, 90, 30, 546),
(2816, 23, 96, 30, 607),
(2817, 23, 102, 30, 686),
(2818, 23, 108, 30, 768),
(2819, 23, 24, 36, 186),
(2820, 23, 30, 36, 218),
(2821, 23, 36, 36, 253),
(2822, 23, 42, 36, 283),
(2823, 23, 48, 36, 300),
(2824, 23, 54, 36, 348),
(2825, 23, 60, 36, 402),
(2826, 23, 66, 36, 421),
(2827, 23, 72, 36, 432),
(2828, 23, 84, 36, 96),
(2829, 23, 84, 36, 554),
(2830, 23, 90, 36, 607),
(2831, 23, 96, 36, 675),
(2832, 23, 102, 36, 763),
(2833, 23, 108, 36, 854),
(2834, 23, 24, 42, 204),
(2835, 23, 30, 42, 241),
(2836, 23, 36, 42, 279),
(2837, 23, 42, 42, 314),
(2838, 23, 48, 42, 332),
(2839, 23, 54, 42, 388),
(2840, 23, 60, 42, 448),
(2841, 23, 66, 42, 461),
(2842, 23, 72, 42, 486),
(2843, 23, 84, 42, 102),
(2844, 23, 84, 42, 614),
(2845, 23, 90, 42, 686),
(2846, 23, 96, 42, 762),
(2847, 23, 102, 42, 861),
(2848, 23, 108, 42, 964),
(2849, 23, 24, 48, 221),
(2850, 23, 30, 48, 264),
(2851, 23, 36, 48, 305),
(2852, 23, 42, 48, 343),
(2853, 23, 48, 48, 368),
(2854, 23, 54, 48, 429),
(2855, 23, 60, 48, 492),
(2856, 23, 66, 48, 521),
(2857, 23, 72, 48, 546),
(2858, 23, 84, 48, 108),
(2859, 23, 84, 48, 688),
(2860, 23, 90, 48, 775),
(2861, 23, 96, 48, 861),
(2862, 23, 102, 48, 973),
(2863, 23, 108, 48, 1090),
(2864, 23, 24, 54, 244),
(2865, 23, 30, 54, 290),
(2866, 23, 36, 54, 331),
(2867, 23, 42, 54, 375),
(2868, 23, 48, 54, 402),
(2869, 23, 54, 54, 474),
(2870, 23, 60, 54, 533),
(2871, 23, 66, 54, 579),
(2872, 23, 72, 54, 613),
(2873, 23, 84, 54, 114),
(2874, 23, 84, 54, 760),
(2875, 23, 90, 54, 853),
(2876, 23, 96, 54, 948),
(2877, 23, 102, 54, 1071),
(2878, 23, 108, 54, 1200),
(2879, 23, 24, 60, 268),
(2880, 23, 30, 60, 314),
(2881, 23, 36, 60, 360),
(2882, 23, 42, 60, 407),
(2883, 23, 48, 60, 439),
(2884, 23, 54, 60, 518),
(2885, 23, 60, 60, 577),
(2886, 23, 66, 60, 629),
(2887, 23, 72, 60, 666),
(2888, 23, 84, 60, 120),
(2889, 23, 84, 60, 826),
(2890, 23, 90, 60, 928),
(2891, 23, 96, 60, 1030),
(2892, 23, 102, 60, 1164),
(2893, 23, 108, 60, 1304),
(2894, 23, 24, 66, 291),
(2895, 23, 30, 66, 339),
(2896, 23, 36, 66, 387),
(2897, 23, 42, 66, 441),
(2898, 23, 48, 66, 483),
(2899, 23, 54, 66, 566),
(2900, 23, 60, 66, 634),
(2901, 23, 66, 66, 690),
(2902, 23, 72, 66, 732),
(2903, 23, 84, 66, 126),
(2904, 23, 84, 66, 899),
(2905, 23, 90, 66, 1044),
(2906, 23, 96, 66, 1160),
(2907, 23, 102, 66, 1311),
(2908, 23, 108, 66, 1468),
(2909, 23, 24, 72, 318),
(2910, 23, 30, 72, 360),
(2911, 23, 36, 72, 413),
(2912, 23, 42, 72, 474),
(2913, 23, 48, 72, 524),
(2914, 23, 54, 72, 614),
(2915, 23, 60, 72, 691),
(2916, 23, 66, 72, 731),
(2917, 23, 72, 72, 778),
(2918, 23, 84, 72, 888),
(2919, 23, 84, 72, 958),
(2920, 23, 90, 72, 1079),
(2921, 23, 96, 72, 1199),
(2922, 23, 102, 72, 1355),
(2923, 23, 108, 72, 1517),
(2924, 23, 24, 78, 329),
(2925, 23, 30, 78, 368),
(2926, 23, 36, 78, 425),
(2927, 23, 42, 78, 486),
(2928, 23, 48, 78, 541),
(2929, 23, 54, 78, 631),
(2930, 23, 60, 78, 711),
(2931, 23, 66, 78, 756),
(2932, 23, 72, 78, 832),
(2933, 23, 84, 78, 947),
(2934, 23, 84, 78, 1025),
(2935, 23, 90, 78, 1151),
(2936, 23, 96, 78, 1279),
(2937, 23, 102, 78, 1445),
(2938, 23, 108, 78, 1619),
(2939, 23, 24, 84, 354),
(2940, 23, 30, 84, 395),
(2941, 23, 36, 84, 452),
(2942, 23, 42, 84, 523),
(2943, 23, 48, 84, 581),
(2944, 23, 54, 84, 665),
(2945, 23, 60, 84, 724),
(2946, 23, 66, 84, 809),
(2947, 23, 72, 84, 894),
(2948, 23, 84, 84, 1013),
(2949, 23, 84, 84, 1093),
(2950, 23, 90, 84, 1231),
(2951, 23, 96, 84, 1367),
(2952, 23, 102, 84, 1545),
(2953, 23, 108, 84, 1730),
(2954, 23, 24, 90, 378),
(2955, 23, 30, 90, 419),
(2956, 23, 36, 90, 479),
(2957, 23, 42, 90, 555),
(2958, 23, 48, 90, 615),
(2959, 23, 54, 90, 703),
(2960, 23, 60, 90, 765),
(2961, 23, 66, 90, 859),
(2962, 23, 72, 90, 947),
(2963, 23, 84, 90, 1064),
(2964, 23, 84, 90, 1164),
(2965, 23, 90, 90, 1310),
(2966, 23, 96, 90, 1456),
(2967, 23, 102, 90, 1645),
(2968, 23, 108, 90, 1843),
(2969, 23, 24, 96, 392),
(2970, 23, 30, 96, 433),
(2971, 23, 36, 96, 494),
(2972, 23, 42, 96, 578),
(2973, 23, 48, 96, 642),
(2974, 23, 54, 96, 740),
(2975, 23, 60, 96, 836),
(2976, 23, 66, 96, 917),
(2977, 23, 72, 96, 1007),
(2978, 23, 84, 96, 1141),
(2979, 23, 84, 96, 1235),
(2980, 23, 90, 96, 1389),
(2981, 23, 96, 96, 1542),
(2982, 23, 102, 96, 1742),
(2983, 23, 108, 96, 1952),
(2984, 23, 24, 102, 410),
(2985, 23, 30, 102, 457),
(2986, 23, 36, 102, 522),
(2987, 23, 42, 102, 615),
(2988, 23, 48, 102, 683),
(2989, 23, 54, 102, 786),
(2990, 23, 60, 102, 868),
(2991, 23, 66, 102, 971),
(2992, 23, 72, 102, 1071),
(2993, 23, 84, 102, 1210),
(2994, 23, 84, 102, 1309),
(2995, 23, 90, 102, 1475),
(2996, 23, 96, 102, 1638),
(2997, 23, 102, 102, 1851),
(2998, 23, 108, 102, 2073),
(2999, 23, 24, 108, 431),
(3000, 23, 30, 108, 475),
(3001, 23, 36, 108, 548),
(3002, 23, 42, 108, 645),
(3003, 23, 48, 108, 718),
(3004, 23, 54, 108, 826),
(3005, 23, 60, 108, 909),
(3006, 23, 66, 108, 1022),
(3007, 23, 72, 108, 1126),
(3008, 23, 84, 108, 1269),
(3009, 23, 84, 108, 1374),
(3010, 23, 90, 108, 1546),
(3011, 23, 96, 108, 1718),
(3012, 23, 102, 108, 1941),
(3013, 23, 108, 108, 2174),
(3014, 23, 24, 114, 450),
(3015, 23, 30, 114, 497),
(3016, 23, 36, 114, 579),
(3017, 23, 42, 114, 682),
(3018, 23, 48, 114, 758),
(3019, 23, 54, 114, 869),
(3020, 23, 60, 114, 961),
(3021, 23, 66, 114, 1075),
(3022, 23, 72, 114, 1188),
(3023, 23, 84, 114, 1342),
(3024, 23, 84, 114, 1446),
(3025, 23, 90, 114, 1626),
(3026, 23, 96, 114, 1807),
(3027, 23, 102, 114, 2042),
(3028, 23, 108, 114, 2287),
(3029, 23, 24, 120, 473),
(3030, 23, 30, 120, 521),
(3031, 23, 36, 120, 603),
(3032, 23, 42, 120, 713),
(3033, 23, 48, 120, 795),
(3034, 23, 54, 120, 910),
(3035, 23, 60, 120, 981),
(3036, 23, 66, 120, 1126),
(3037, 23, 72, 120, 1239),
(3038, 23, 84, 120, 1395),
(3039, 23, 84, 120, 1514),
(3040, 23, 90, 120, 1704),
(3041, 23, 96, 120, 1893),
(3042, 23, 102, 120, 2139),
(3043, 23, 108, 120, 2396),
(3044, 23, 24, 126, 488),
(3045, 23, 30, 126, 544),
(3046, 23, 36, 126, 638),
(3047, 23, 42, 126, 749),
(3048, 23, 48, 126, 834),
(3049, 23, 54, 126, 950),
(3050, 23, 60, 126, 1052),
(3051, 23, 66, 126, 1182),
(3052, 23, 72, 126, 1301),
(3053, 23, 84, 126, 1376),
(3054, 23, 84, 126, 1582),
(3055, 23, 90, 126, 1788),
(3056, 23, 96, 126, 1986),
(3057, 23, 102, 126, 2244),
(3058, 23, 108, 126, 2513),
(3059, 24, 24, 30, 114),
(3060, 24, 30, 30, 147),
(3061, 24, 36, 30, 176),
(3062, 24, 42, 30, 206),
(3063, 24, 48, 30, 235),
(3064, 24, 54, 30, 261),
(3065, 24, 60, 30, 293),
(3066, 24, 66, 30, 322),
(3067, 24, 72, 30, 344),
(3068, 24, 84, 30, 90),
(3069, 24, 84, 30, 401),
(3070, 24, 90, 30, 427),
(3071, 24, 96, 30, 458),
(3072, 24, 102, 30, 504),
(3073, 24, 108, 30, 539),
(3074, 24, 24, 36, 141),
(3075, 24, 30, 36, 176),
(3076, 24, 36, 36, 206),
(3077, 24, 42, 36, 236),
(3078, 24, 48, 36, 271),
(3079, 24, 54, 36, 293),
(3080, 24, 60, 36, 324),
(3081, 24, 66, 36, 357),
(3082, 24, 72, 36, 379),
(3083, 24, 84, 36, 96),
(3084, 24, 84, 36, 453),
(3085, 24, 90, 36, 480),
(3086, 24, 96, 36, 503),
(3087, 24, 102, 36, 553),
(3088, 24, 108, 36, 592),
(3089, 24, 24, 42, 171),
(3090, 24, 30, 42, 193),
(3091, 24, 36, 42, 225),
(3092, 24, 42, 42, 258),
(3093, 24, 48, 42, 295),
(3094, 24, 54, 42, 329),
(3095, 24, 60, 42, 364),
(3096, 24, 66, 42, 401),
(3097, 24, 72, 42, 421),
(3098, 24, 84, 42, 102),
(3099, 24, 84, 42, 499),
(3100, 24, 90, 42, 521),
(3101, 24, 96, 42, 550),
(3102, 24, 102, 42, 605),
(3103, 24, 108, 42, 647),
(3104, 24, 24, 48, 176),
(3105, 24, 30, 48, 228),
(3106, 24, 36, 48, 244),
(3107, 24, 42, 48, 274),
(3108, 24, 48, 48, 314),
(3109, 24, 54, 48, 353),
(3110, 24, 60, 48, 396),
(3111, 24, 66, 48, 435),
(3112, 24, 72, 48, 439),
(3113, 24, 84, 48, 108),
(3114, 24, 84, 48, 545),
(3115, 24, 90, 48, 585),
(3116, 24, 96, 48, 624),
(3117, 24, 102, 48, 686),
(3118, 24, 108, 48, 734),
(3119, 24, 24, 54, 185),
(3120, 24, 30, 54, 232),
(3121, 24, 36, 54, 265),
(3122, 24, 42, 54, 303),
(3123, 24, 48, 54, 347),
(3124, 24, 54, 54, 389),
(3125, 24, 60, 54, 433),
(3126, 24, 66, 54, 477),
(3127, 24, 72, 54, 503),
(3128, 24, 84, 54, 545),
(3129, 24, 84, 54, 602),
(3130, 24, 90, 54, 657),
(3131, 24, 96, 54, 688),
(3132, 24, 102, 54, 757),
(3133, 24, 108, 54, 810),
(3134, 24, 24, 60, 206),
(3135, 24, 30, 60, 249),
(3136, 24, 36, 60, 281),
(3137, 24, 42, 60, 334),
(3138, 24, 48, 60, 381),
(3139, 24, 54, 60, 418),
(3140, 24, 60, 60, 466),
(3141, 24, 66, 60, 512),
(3142, 24, 72, 60, 563),
(3143, 24, 84, 60, 609),
(3144, 24, 84, 60, 644),
(3145, 24, 90, 60, 690),
(3146, 24, 96, 60, 735),
(3147, 24, 102, 60, 809),
(3148, 24, 108, 60, 865),
(3149, 24, 24, 66, 227),
(3150, 24, 30, 66, 273),
(3151, 24, 36, 66, 295),
(3152, 24, 42, 66, 361),
(3153, 24, 48, 66, 414),
(3154, 24, 54, 66, 456),
(3155, 24, 60, 66, 504),
(3156, 24, 66, 66, 555),
(3157, 24, 72, 66, 614),
(3158, 24, 84, 66, 666),
(3159, 24, 84, 66, 716),
(3160, 24, 90, 66, 768),
(3161, 24, 96, 66, 819),
(3162, 24, 102, 66, 901),
(3163, 24, 108, 66, 964),
(3164, 24, 24, 72, 247),
(3165, 24, 30, 72, 287),
(3166, 24, 36, 72, 334),
(3167, 24, 42, 72, 386),
(3168, 24, 48, 72, 442),
(3169, 24, 54, 72, 489),
(3170, 24, 60, 72, 541),
(3171, 24, 66, 72, 594),
(3172, 24, 72, 72, 663),
(3173, 24, 84, 72, 717),
(3174, 24, 84, 72, 761),
(3175, 24, 90, 72, 815),
(3176, 24, 96, 72, 868),
(3177, 24, 102, 72, 955),
(3178, 24, 108, 72, 1022),
(3179, 24, 24, 78, 258),
(3180, 24, 30, 78, 299),
(3181, 24, 36, 78, 359),
(3182, 24, 42, 78, 423),
(3183, 24, 48, 78, 483),
(3184, 24, 54, 78, 533),
(3185, 24, 60, 78, 587),
(3186, 24, 66, 78, 646),
(3187, 24, 72, 78, 707),
(3188, 24, 84, 78, 765),
(3189, 24, 84, 78, 819),
(3190, 24, 90, 78, 878),
(3191, 24, 96, 78, 937),
(3192, 24, 102, 78, 1031),
(3193, 24, 108, 78, 1103),
(3194, 24, 24, 84, 268),
(3195, 24, 30, 84, 316),
(3196, 24, 36, 84, 379),
(3197, 24, 42, 84, 453),
(3198, 24, 48, 84, 517),
(3199, 24, 54, 84, 565),
(3200, 24, 60, 84, 639),
(3201, 24, 66, 84, 704),
(3202, 24, 72, 84, 753),
(3203, 24, 84, 84, 815),
(3204, 24, 84, 84, 883),
(3205, 24, 90, 84, 946),
(3206, 24, 96, 84, 1009),
(3207, 24, 102, 84, 1110),
(3208, 24, 108, 84, 1188),
(3209, 24, 24, 90, 287),
(3210, 24, 30, 90, 336),
(3211, 24, 36, 90, 403),
(3212, 24, 42, 90, 471),
(3213, 24, 48, 90, 538),
(3214, 24, 54, 90, 606),
(3215, 24, 60, 90, 672),
(3216, 24, 66, 90, 739),
(3217, 24, 72, 90, 813),
(3218, 24, 84, 90, 880),
(3219, 24, 84, 90, 941),
(3220, 24, 90, 90, 1008),
(3221, 24, 96, 90, 1074),
(3222, 24, 102, 90, 1181),
(3223, 24, 108, 90, 1264),
(3224, 24, 24, 96, 294),
(3225, 24, 30, 96, 348),
(3226, 24, 36, 96, 418),
(3227, 24, 42, 96, 484),
(3228, 24, 48, 96, 553),
(3229, 24, 54, 96, 637),
(3230, 24, 60, 96, 695),
(3231, 24, 66, 96, 764),
(3232, 24, 72, 96, 839),
(3233, 24, 84, 96, 908),
(3234, 24, 84, 96, 984),
(3235, 24, 90, 96, 1054),
(3236, 24, 96, 96, 1125),
(3237, 24, 102, 96, 1238),
(3238, 24, 108, 96, 1324),
(3239, 24, 24, 102, 301),
(3240, 24, 30, 102, 356),
(3241, 24, 36, 102, 427),
(3242, 24, 42, 102, 507),
(3243, 24, 48, 102, 580),
(3244, 24, 54, 102, 651),
(3245, 24, 60, 102, 728),
(3246, 24, 66, 102, 800),
(3247, 24, 72, 102, 882),
(3248, 24, 84, 102, 956),
(3249, 24, 84, 102, 1031),
(3250, 24, 90, 102, 1105),
(3251, 24, 96, 102, 1178),
(3252, 24, 102, 102, 1296),
(3253, 24, 108, 102, 1387),
(3254, 24, 24, 108, 322),
(3255, 24, 30, 108, 399),
(3256, 24, 36, 108, 479),
(3257, 24, 42, 108, 536),
(3258, 24, 48, 108, 612),
(3259, 24, 54, 108, 694),
(3260, 24, 60, 108, 771),
(3261, 24, 66, 108, 848),
(3262, 24, 72, 108, 949),
(3263, 24, 84, 108, 1028),
(3264, 24, 84, 108, 1070),
(3265, 24, 90, 108, 1147),
(3266, 24, 96, 108, 1223),
(3267, 24, 102, 108, 1345),
(3268, 24, 108, 108, 1439),
(3269, 25, 24, 30, 137),
(3270, 25, 30, 30, 176),
(3271, 25, 36, 30, 211),
(3272, 25, 42, 30, 247),
(3273, 25, 48, 30, 282),
(3274, 25, 54, 30, 313),
(3275, 25, 60, 30, 352),
(3276, 25, 66, 30, 386),
(3277, 25, 72, 30, 413),
(3278, 25, 84, 30, 90),
(3279, 25, 84, 30, 481),
(3280, 25, 90, 30, 512),
(3281, 25, 96, 30, 550),
(3282, 25, 102, 30, 605),
(3283, 25, 108, 30, 647),
(3284, 25, 24, 36, 169),
(3285, 25, 30, 36, 211),
(3286, 25, 36, 36, 247),
(3287, 25, 42, 36, 283),
(3288, 25, 48, 36, 325),
(3289, 25, 54, 36, 352),
(3290, 25, 60, 36, 389),
(3291, 25, 66, 36, 428),
(3292, 25, 72, 36, 455),
(3293, 25, 84, 36, 96),
(3294, 25, 84, 36, 544),
(3295, 25, 90, 36, 576),
(3296, 25, 96, 36, 604),
(3297, 25, 102, 36, 664),
(3298, 25, 108, 36, 710),
(3299, 25, 24, 42, 205),
(3300, 25, 30, 42, 232),
(3301, 25, 36, 42, 270),
(3302, 25, 42, 42, 310),
(3303, 25, 48, 42, 354),
(3304, 25, 54, 42, 395),
(3305, 25, 60, 42, 437),
(3306, 25, 66, 42, 481),
(3307, 25, 72, 42, 505),
(3308, 25, 84, 42, 102),
(3309, 25, 84, 42, 599),
(3310, 25, 90, 42, 625),
(3311, 25, 96, 42, 660),
(3312, 25, 102, 42, 726),
(3313, 25, 108, 42, 777),
(3314, 25, 24, 48, 211),
(3315, 25, 30, 48, 274),
(3316, 25, 36, 48, 293),
(3317, 25, 42, 48, 329),
(3318, 25, 48, 48, 377),
(3319, 25, 54, 48, 424),
(3320, 25, 60, 48, 475),
(3321, 25, 66, 48, 522),
(3322, 25, 72, 48, 527),
(3323, 25, 84, 48, 108),
(3324, 25, 84, 48, 654),
(3325, 25, 90, 48, 702),
(3326, 25, 96, 48, 749),
(3327, 25, 102, 48, 824),
(3328, 25, 108, 48, 881),
(3329, 25, 24, 54, 222),
(3330, 25, 30, 54, 278),
(3331, 25, 36, 54, 318),
(3332, 25, 42, 54, 364),
(3333, 25, 48, 54, 416),
(3334, 25, 54, 54, 467),
(3335, 25, 60, 54, 520),
(3336, 25, 66, 54, 572),
(3337, 25, 72, 54, 604),
(3338, 25, 84, 54, 654),
(3339, 25, 84, 54, 722),
(3340, 25, 90, 54, 788),
(3341, 25, 96, 54, 826),
(3342, 25, 102, 54, 908),
(3343, 25, 108, 54, 972),
(3344, 25, 24, 60, 247),
(3345, 25, 30, 60, 299),
(3346, 25, 36, 60, 337),
(3347, 25, 42, 60, 401),
(3348, 25, 48, 60, 457),
(3349, 25, 54, 60, 502),
(3350, 25, 60, 60, 559),
(3351, 25, 66, 60, 614),
(3352, 25, 72, 60, 676),
(3353, 25, 84, 60, 731),
(3354, 25, 84, 60, 773),
(3355, 25, 90, 60, 828),
(3356, 25, 96, 60, 882),
(3357, 25, 102, 60, 970),
(3358, 25, 108, 60, 1038),
(3359, 25, 24, 66, 272),
(3360, 25, 30, 66, 328),
(3361, 25, 36, 66, 354),
(3362, 25, 42, 66, 433),
(3363, 25, 48, 66, 497),
(3364, 25, 54, 66, 547),
(3365, 25, 60, 66, 605),
(3366, 25, 66, 66, 666),
(3367, 25, 72, 66, 737),
(3368, 25, 84, 66, 799),
(3369, 25, 84, 66, 859),
(3370, 25, 90, 66, 922),
(3371, 25, 96, 66, 983),
(3372, 25, 102, 66, 1081),
(3373, 25, 108, 66, 1157),
(3374, 25, 24, 72, 296),
(3375, 25, 30, 72, 344),
(3376, 25, 36, 72, 401),
(3377, 25, 42, 72, 463),
(3378, 25, 48, 72, 530),
(3379, 25, 54, 72, 587),
(3380, 25, 60, 72, 649),
(3381, 25, 66, 72, 713),
(3382, 25, 72, 72, 796),
(3383, 25, 84, 72, 860),
(3384, 25, 84, 72, 913),
(3385, 25, 90, 72, 978),
(3386, 25, 96, 72, 1042),
(3387, 25, 102, 72, 1146),
(3388, 25, 108, 72, 1226),
(3389, 25, 24, 78, 310),
(3390, 25, 30, 78, 359),
(3391, 25, 36, 78, 431),
(3392, 25, 42, 78, 508),
(3393, 25, 48, 78, 580),
(3394, 25, 54, 78, 640),
(3395, 25, 60, 78, 704),
(3396, 25, 66, 78, 775),
(3397, 25, 72, 78, 848),
(3398, 25, 84, 78, 918),
(3399, 25, 84, 78, 983),
(3400, 25, 90, 78, 1054),
(3401, 25, 96, 78, 1124),
(3402, 25, 102, 78, 1237),
(3403, 25, 108, 78, 1323),
(3404, 25, 24, 84, 322),
(3405, 25, 30, 84, 379),
(3406, 25, 36, 84, 455),
(3407, 25, 42, 84, 544),
(3408, 25, 48, 84, 620),
(3409, 25, 54, 84, 678),
(3410, 25, 60, 84, 767),
(3411, 25, 66, 84, 845),
(3412, 25, 72, 84, 904),
(3413, 25, 84, 84, 978),
(3414, 25, 84, 84, 1060),
(3415, 25, 90, 84, 1135),
(3416, 25, 96, 84, 1211),
(3417, 25, 102, 84, 1332),
(3418, 25, 108, 84, 1425),
(3419, 25, 24, 90, 344),
(3420, 25, 30, 90, 403),
(3421, 25, 36, 90, 484),
(3422, 25, 42, 90, 565),
(3423, 25, 48, 90, 646),
(3424, 25, 54, 90, 727),
(3425, 25, 60, 90, 806),
(3426, 25, 66, 90, 887),
(3427, 25, 72, 90, 976),
(3428, 25, 84, 90, 1056),
(3429, 25, 84, 90, 1129),
(3430, 25, 90, 90, 1210),
(3431, 25, 96, 90, 1289),
(3432, 25, 102, 90, 1418),
(3433, 25, 108, 90, 1517),
(3434, 25, 24, 96, 353),
(3435, 25, 30, 96, 418),
(3436, 25, 36, 96, 502),
(3437, 25, 42, 96, 581),
(3438, 25, 48, 96, 664),
(3439, 25, 54, 96, 764),
(3440, 25, 60, 96, 834),
(3441, 25, 66, 96, 917),
(3442, 25, 72, 96, 1007),
(3443, 25, 84, 96, 1090),
(3444, 25, 84, 96, 1181),
(3445, 25, 90, 96, 1265),
(3446, 25, 96, 96, 1350),
(3447, 25, 102, 96, 1485),
(3448, 25, 108, 96, 1589),
(3449, 25, 24, 102, 361),
(3450, 25, 30, 102, 427),
(3451, 25, 36, 102, 512),
(3452, 25, 42, 102, 608),
(3453, 25, 48, 102, 696),
(3454, 25, 54, 102, 781),
(3455, 25, 60, 102, 874),
(3456, 25, 66, 102, 960),
(3457, 25, 72, 102, 1058),
(3458, 25, 84, 102, 1147),
(3459, 25, 84, 102, 1237),
(3460, 25, 90, 102, 1326),
(3461, 25, 96, 102, 1414),
(3462, 25, 102, 102, 1555),
(3463, 25, 108, 102, 1664),
(3464, 25, 24, 108, 386),
(3465, 25, 30, 108, 479),
(3466, 25, 36, 108, 575),
(3467, 25, 42, 108, 643),
(3468, 25, 48, 108, 734),
(3469, 25, 54, 108, 833),
(3470, 25, 60, 108, 925),
(3471, 25, 66, 108, 1018),
(3472, 25, 72, 108, 1139),
(3473, 25, 84, 108, 1234),
(3474, 25, 84, 108, 1284),
(3475, 25, 90, 108, 1376),
(3476, 25, 96, 108, 1468),
(3477, 25, 102, 108, 1614),
(3478, 25, 108, 108, 1727),
(3479, 26, 36, 36, 121),
(3480, 26, 48, 36, 126),
(3481, 26, 60, 36, 132),
(3482, 26, 72, 36, 140),
(3483, 26, 84, 36, 149),
(3484, 26, 96, 36, 155),
(3485, 26, 108, 36, 202),
(3486, 26, 36, 48, 124),
(3487, 26, 48, 48, 130),
(3488, 26, 60, 48, 144),
(3489, 26, 72, 48, 149),
(3490, 26, 84, 48, 155),
(3491, 26, 96, 48, 163),
(3492, 26, 108, 48, 212),
(3493, 26, 36, 60, 128),
(3494, 26, 48, 60, 135),
(3495, 26, 60, 60, 149),
(3496, 26, 72, 60, 159),
(3497, 26, 84, 60, 169),
(3498, 26, 96, 60, 179),
(3499, 26, 108, 60, 234),
(3500, 26, 36, 72, 131),
(3501, 26, 48, 72, 148),
(3502, 26, 60, 72, 158),
(3503, 26, 72, 72, 171),
(3504, 26, 84, 72, 184),
(3505, 26, 96, 72, 195),
(3506, 26, 108, 72, 243),
(3507, 26, 36, 84, 135),
(3508, 26, 48, 84, 156),
(3509, 26, 60, 84, 171),
(3510, 26, 72, 84, 184),
(3511, 26, 84, 84, 197),
(3512, 26, 96, 84, 212),
(3513, 26, 108, 84, 266),
(3514, 26, 36, 96, 148),
(3515, 26, 48, 96, 165),
(3516, 26, 60, 96, 178),
(3517, 26, 72, 96, 195),
(3518, 26, 84, 96, 212),
(3519, 26, 96, 96, 228),
(3520, 26, 108, 96, 288),
(3521, 26, 36, 108, 153),
(3522, 26, 48, 108, 171),
(3523, 26, 60, 108, 189),
(3524, 26, 72, 108, 207),
(3525, 26, 84, 108, 225),
(3526, 26, 96, 108, 244),
(3527, 26, 108, 108, 306),
(3528, 26, 36, 120, 162),
(3529, 26, 48, 120, 180),
(3530, 26, 60, 120, 199),
(3531, 26, 72, 120, 220),
(3532, 26, 84, 120, 239),
(3533, 26, 96, 120, 259),
(3534, 26, 108, 120, 324),
(3535, 27, 36, 36, 134),
(3536, 27, 48, 36, 140),
(3537, 27, 60, 36, 147),
(3538, 27, 72, 36, 155),
(3539, 27, 84, 36, 165),
(3540, 27, 96, 36, 172),
(3541, 27, 108, 36, 224),
(3542, 27, 36, 48, 138),
(3543, 27, 48, 48, 144),
(3544, 27, 60, 48, 160),
(3545, 27, 72, 48, 165),
(3546, 27, 84, 48, 172),
(3547, 27, 96, 48, 181),
(3548, 27, 108, 48, 235),
(3549, 27, 36, 60, 142),
(3550, 27, 48, 60, 150),
(3551, 27, 60, 60, 165),
(3552, 27, 72, 60, 177),
(3553, 27, 84, 60, 188),
(3554, 27, 96, 60, 199),
(3555, 27, 108, 60, 260),
(3556, 27, 36, 72, 146),
(3557, 27, 48, 72, 164),
(3558, 27, 60, 72, 175),
(3559, 27, 72, 72, 190),
(3560, 27, 84, 72, 204),
(3561, 27, 96, 72, 217),
(3562, 27, 108, 72, 270),
(3563, 27, 36, 84, 150),
(3564, 27, 48, 84, 173),
(3565, 27, 60, 84, 190),
(3566, 27, 72, 84, 204),
(3567, 27, 84, 84, 219),
(3568, 27, 96, 84, 235),
(3569, 27, 108, 84, 295),
(3570, 27, 36, 96, 164),
(3571, 27, 48, 96, 183),
(3572, 27, 60, 96, 198),
(3573, 27, 72, 96, 217),
(3574, 27, 84, 96, 235),
(3575, 27, 96, 96, 253),
(3576, 27, 108, 96, 320),
(3577, 27, 36, 108, 170),
(3578, 27, 48, 108, 190),
(3579, 27, 60, 108, 210),
(3580, 27, 72, 108, 230),
(3581, 27, 84, 108, 250),
(3582, 27, 96, 108, 271),
(3583, 27, 108, 108, 340),
(3584, 27, 36, 120, 180),
(3585, 27, 48, 120, 200),
(3586, 27, 60, 120, 221),
(3587, 27, 72, 120, 244),
(3588, 27, 84, 120, 266),
(3589, 27, 96, 120, 288),
(3590, 27, 108, 120, 360),
(3591, 28, 36, 36, 150),
(3592, 28, 48, 36, 157),
(3593, 28, 60, 36, 173),
(3594, 28, 72, 36, 183),
(3595, 28, 84, 36, 195),
(3596, 28, 96, 36, 203),
(3597, 28, 108, 36, 264),
(3598, 28, 36, 48, 155),
(3599, 28, 48, 48, 161),
(3600, 28, 60, 48, 189),
(3601, 28, 72, 48, 195),
(3602, 28, 84, 48, 203),
(3603, 28, 96, 48, 214),
(3604, 28, 108, 48, 277),
(3605, 28, 36, 60, 159),
(3606, 28, 48, 60, 168),
(3607, 28, 60, 60, 195),
(3608, 28, 72, 60, 209),
(3609, 28, 84, 60, 222),
(3610, 28, 96, 60, 235),
(3611, 28, 108, 60, 307),
(3612, 28, 36, 72, 164),
(3613, 28, 48, 72, 184),
(3614, 28, 60, 72, 207),
(3615, 28, 72, 72, 224),
(3616, 28, 84, 72, 241),
(3617, 28, 96, 72, 256),
(3618, 28, 108, 72, 319),
(3619, 28, 36, 84, 168),
(3620, 28, 48, 84, 194),
(3621, 28, 60, 84, 224),
(3622, 28, 72, 84, 241),
(3623, 28, 84, 84, 258),
(3624, 28, 96, 84, 277),
(3625, 28, 108, 84, 348),
(3626, 28, 36, 96, 184),
(3627, 28, 48, 96, 205),
(3628, 28, 60, 96, 234),
(3629, 28, 72, 96, 256),
(3630, 28, 84, 96, 277),
(3631, 28, 96, 96, 299),
(3632, 28, 108, 96, 378),
(3633, 28, 36, 108, 190),
(3634, 28, 48, 108, 213),
(3635, 28, 60, 108, 248),
(3636, 28, 72, 108, 271),
(3637, 28, 84, 108, 295),
(3638, 28, 96, 108, 320),
(3639, 28, 108, 108, 401),
(3640, 28, 36, 120, 202),
(3641, 28, 48, 120, 224),
(3642, 28, 60, 120, 261),
(3643, 28, 72, 120, 288),
(3644, 28, 84, 120, 314),
(3645, 28, 96, 120, 340),
(3646, 28, 108, 120, 425),
(3647, 29, 36, 36, 161),
(3648, 29, 48, 36, 168),
(3649, 29, 60, 36, 185),
(3650, 29, 72, 36, 195),
(3651, 29, 84, 36, 208),
(3652, 29, 96, 36, 217),
(3653, 29, 108, 36, 309),
(3654, 29, 36, 48, 166),
(3655, 29, 48, 48, 173),
(3656, 29, 60, 48, 202),
(3657, 29, 72, 48, 208),
(3658, 29, 84, 48, 217),
(3659, 29, 96, 48, 228),
(3660, 29, 108, 48, 324),
(3661, 29, 36, 60, 170),
(3662, 29, 48, 60, 180),
(3663, 29, 60, 60, 208),
(3664, 29, 72, 60, 223),
(3665, 29, 84, 60, 237),
(3666, 29, 96, 60, 251),
(3667, 29, 108, 60, 359),
(3668, 29, 36, 72, 175),
(3669, 29, 48, 72, 197),
(3670, 29, 60, 72, 221),
(3671, 29, 72, 72, 239),
(3672, 29, 84, 72, 257),
(3673, 29, 96, 72, 273),
(3674, 29, 108, 72, 373),
(3675, 29, 36, 84, 180),
(3676, 29, 48, 84, 208),
(3677, 29, 60, 84, 239),
(3678, 29, 72, 84, 257),
(3679, 29, 84, 84, 276),
(3680, 29, 96, 84, 296),
(3681, 29, 108, 84, 407),
(3682, 29, 36, 96, 197),
(3683, 29, 48, 96, 220),
(3684, 29, 60, 96, 249),
(3685, 29, 72, 96, 273),
(3686, 29, 84, 96, 296),
(3687, 29, 96, 96, 319),
(3688, 29, 108, 96, 442),
(3689, 29, 36, 108, 204),
(3690, 29, 48, 108, 228),
(3691, 29, 60, 108, 265),
(3692, 29, 72, 108, 290),
(3693, 29, 84, 108, 315),
(3694, 29, 96, 108, 341),
(3695, 29, 108, 108, 469),
(3696, 29, 36, 120, 216),
(3697, 29, 48, 120, 240),
(3698, 29, 60, 120, 278),
(3699, 29, 72, 120, 307),
(3700, 29, 84, 120, 335),
(3701, 29, 96, 120, 363),
(3702, 29, 108, 120, 497),
(3703, 30, 36, 36, 174),
(3704, 30, 48, 36, 182),
(3705, 30, 60, 36, 201),
(3706, 30, 72, 36, 212),
(3707, 30, 84, 36, 226),
(3708, 30, 96, 36, 236),
(3709, 30, 108, 36, 340),
(3710, 30, 36, 48, 179),
(3711, 30, 48, 48, 187),
(3712, 30, 60, 48, 219),
(3713, 30, 72, 48, 226),
(3714, 30, 84, 48, 236),
(3715, 30, 96, 48, 248),
(3716, 30, 108, 48, 357),
(3717, 30, 36, 60, 185),
(3718, 30, 48, 60, 195),
(3719, 30, 60, 60, 226),
(3720, 30, 72, 60, 242),
(3721, 30, 84, 60, 258),
(3722, 30, 96, 60, 273),
(3723, 30, 108, 60, 395),
(3724, 30, 36, 72, 190),
(3725, 30, 48, 72, 213),
(3726, 30, 60, 72, 240),
(3727, 30, 72, 72, 260),
(3728, 30, 84, 72, 279),
(3729, 30, 96, 72, 297),
(3730, 30, 108, 72, 410),
(3731, 30, 36, 84, 195),
(3732, 30, 48, 84, 225),
(3733, 30, 60, 84, 260),
(3734, 30, 72, 84, 279),
(3735, 30, 84, 84, 300),
(3736, 30, 96, 84, 322),
(3737, 30, 108, 84, 448),
(3738, 30, 36, 96, 213),
(3739, 30, 48, 96, 238),
(3740, 30, 60, 96, 271),
(3741, 30, 72, 96, 297),
(3742, 30, 84, 96, 322),
(3743, 30, 96, 96, 347),
(3744, 30, 108, 96, 486),
(3745, 30, 36, 108, 221),
(3746, 30, 48, 108, 247),
(3747, 30, 60, 108, 288),
(3748, 30, 72, 108, 315),
(3749, 30, 84, 108, 343),
(3750, 30, 96, 108, 371),
(3751, 30, 108, 108, 516),
(3752, 30, 36, 120, 234),
(3753, 30, 48, 120, 260),
(3754, 30, 60, 120, 303),
(3755, 30, 72, 120, 334),
(3756, 30, 84, 120, 364),
(3757, 30, 96, 120, 395),
(3758, 30, 108, 120, 547),
(3759, 31, 36, 36, 190),
(3760, 31, 48, 36, 199),
(3761, 31, 60, 36, 219),
(3762, 31, 72, 36, 231),
(3763, 31, 84, 36, 246),
(3764, 31, 96, 36, 256),
(3765, 31, 108, 36, 374),
(3766, 31, 36, 48, 196),
(3767, 31, 48, 48, 204),
(3768, 31, 60, 48, 238),
(3769, 31, 72, 48, 246),
(3770, 31, 84, 48, 256),
(3771, 31, 96, 48, 270),
(3772, 31, 108, 48, 393),
(3773, 31, 36, 60, 202),
(3774, 31, 48, 60, 213),
(3775, 31, 60, 60, 246),
(3776, 31, 72, 60, 264),
(3777, 31, 84, 60, 280),
(3778, 31, 96, 60, 297),
(3779, 31, 108, 60, 434),
(3780, 31, 36, 72, 207),
(3781, 31, 48, 72, 233),
(3782, 31, 60, 72, 261),
(3783, 31, 72, 72, 283),
(3784, 31, 84, 72, 304),
(3785, 31, 96, 72, 323),
(3786, 31, 108, 72, 451),
(3787, 31, 36, 84, 213),
(3788, 31, 48, 84, 246),
(3789, 31, 60, 84, 283),
(3790, 31, 72, 84, 304),
(3791, 31, 84, 84, 326),
(3792, 31, 96, 84, 350),
(3793, 31, 108, 84, 493),
(3794, 31, 36, 96, 233),
(3795, 31, 48, 96, 260),
(3796, 31, 60, 96, 295),
(3797, 31, 72, 96, 323),
(3798, 31, 84, 96, 350),
(3799, 31, 96, 96, 377),
(3800, 31, 108, 96, 535),
(3801, 31, 36, 108, 241),
(3802, 31, 48, 108, 270),
(3803, 31, 60, 108, 313),
(3804, 31, 72, 108, 343),
(3805, 31, 84, 108, 373),
(3806, 31, 96, 108, 404),
(3807, 31, 108, 108, 568),
(3808, 31, 36, 120, 256),
(3809, 31, 48, 120, 284),
(3810, 31, 60, 120, 329),
(3811, 31, 72, 120, 364),
(3812, 31, 84, 120, 396),
(3813, 31, 96, 120, 429),
(3814, 31, 108, 120, 601),
(3815, 32, 36, 36, 208),
(3816, 32, 48, 36, 217),
(3817, 32, 60, 36, 240),
(3818, 32, 72, 36, 253),
(3819, 32, 84, 36, 269),
(3820, 32, 96, 36, 280),
(3821, 32, 108, 36, 400),
(3822, 32, 36, 48, 214),
(3823, 32, 48, 48, 223),
(3824, 32, 60, 48, 261),
(3825, 32, 72, 48, 269),
(3826, 32, 84, 48, 280),
(3827, 32, 96, 48, 295),
(3828, 32, 108, 48, 420),
(3829, 32, 36, 60, 220),
(3830, 32, 48, 60, 233),
(3831, 32, 60, 60, 269),
(3832, 32, 72, 60, 289),
(3833, 32, 84, 60, 306),
(3834, 32, 96, 60, 324),
(3835, 32, 108, 60, 465),
(3836, 32, 36, 72, 226),
(3837, 32, 48, 72, 254),
(3838, 32, 60, 72, 285),
(3839, 32, 72, 72, 310),
(3840, 32, 84, 72, 333),
(3841, 32, 96, 72, 354),
(3842, 32, 108, 72, 483),
(3843, 32, 36, 84, 233),
(3844, 32, 48, 84, 268),
(3845, 32, 60, 84, 310),
(3846, 32, 72, 84, 333),
(3847, 32, 84, 84, 357),
(3848, 32, 96, 84, 383),
(3849, 32, 108, 84, 527),
(3850, 32, 36, 96, 254),
(3851, 32, 48, 96, 284),
(3852, 32, 60, 96, 323),
(3853, 32, 72, 96, 354),
(3854, 32, 84, 96, 383),
(3855, 32, 96, 96, 412),
(3856, 32, 108, 96, 572),
(3857, 32, 36, 108, 264),
(3858, 32, 48, 108, 295),
(3859, 32, 60, 108, 342),
(3860, 32, 72, 108, 375),
(3861, 32, 84, 108, 408),
(3862, 32, 96, 108, 442),
(3863, 32, 108, 108, 608),
(3864, 32, 36, 120, 279),
(3865, 32, 48, 120, 310),
(3866, 32, 60, 120, 360),
(3867, 32, 72, 120, 398),
(3868, 32, 84, 120, 434),
(3869, 32, 96, 120, 469),
(3870, 32, 108, 120, 643),
(3871, 33, 36, 36, 221),
(3872, 33, 48, 36, 231),
(3873, 33, 60, 36, 254),
(3874, 33, 72, 36, 268),
(3875, 33, 84, 36, 285),
(3876, 33, 96, 36, 298),
(3877, 33, 108, 36, 420),
(3878, 33, 36, 48, 228),
(3879, 33, 48, 48, 238),
(3880, 33, 60, 48, 277),
(3881, 33, 72, 48, 285),
(3882, 33, 84, 48, 298),
(3883, 33, 96, 48, 313),
(3884, 33, 108, 48, 441),
(3885, 33, 36, 60, 234),
(3886, 33, 48, 60, 248),
(3887, 33, 60, 60, 285),
(3888, 33, 72, 60, 306),
(3889, 33, 84, 60, 325),
(3890, 33, 96, 60, 344),
(3891, 33, 108, 60, 488),
(3892, 33, 36, 72, 241),
(3893, 33, 48, 72, 271),
(3894, 33, 60, 72, 303),
(3895, 33, 72, 72, 329),
(3896, 33, 84, 72, 353),
(3897, 33, 96, 72, 375),
(3898, 33, 108, 72, 507),
(3899, 33, 36, 84, 248),
(3900, 33, 48, 84, 285),
(3901, 33, 60, 84, 329),
(3902, 33, 72, 84, 353),
(3903, 33, 84, 84, 379),
(3904, 33, 96, 84, 407),
(3905, 33, 108, 84, 554),
(3906, 33, 36, 96, 271),
(3907, 33, 48, 96, 302),
(3908, 33, 60, 96, 343),
(3909, 33, 72, 96, 375),
(3910, 33, 84, 96, 407),
(3911, 33, 96, 96, 438),
(3912, 33, 108, 96, 601),
(3913, 33, 36, 108, 281),
(3914, 33, 48, 108, 314),
(3915, 33, 60, 108, 363),
(3916, 33, 72, 108, 398),
(3917, 33, 84, 108, 433),
(3918, 33, 96, 108, 469),
(3919, 33, 108, 108, 638),
(3920, 33, 36, 120, 297),
(3921, 33, 48, 120, 330),
(3922, 33, 60, 120, 382),
(3923, 33, 72, 120, 422),
(3924, 33, 84, 120, 460),
(3925, 33, 96, 120, 498),
(3926, 33, 108, 120, 676),
(3927, 34, 36, 36, 240),
(3928, 34, 48, 36, 251),
(3929, 34, 60, 36, 276),
(3930, 34, 72, 36, 291),
(3931, 34, 84, 36, 310),
(3932, 34, 96, 36, 323),
(3933, 34, 108, 36, 433),
(3934, 34, 36, 48, 247),
(3935, 34, 48, 48, 258),
(3936, 34, 60, 48, 301),
(3937, 34, 72, 48, 310),
(3938, 34, 84, 48, 323),
(3939, 34, 96, 48, 340),
(3940, 34, 108, 48, 454),
(3941, 34, 36, 60, 254),
(3942, 34, 48, 60, 269),
(3943, 34, 60, 60, 310),
(3944, 34, 72, 60, 333),
(3945, 34, 84, 60, 353),
(3946, 34, 96, 60, 374),
(3947, 34, 108, 60, 503),
(3948, 34, 36, 72, 261),
(3949, 34, 48, 72, 294),
(3950, 34, 60, 72, 329),
(3951, 34, 72, 72, 357),
(3952, 34, 84, 72, 384),
(3953, 34, 96, 72, 408),
(3954, 34, 108, 72, 522),
(3955, 34, 36, 84, 269),
(3956, 34, 48, 84, 310),
(3957, 34, 60, 84, 357),
(3958, 34, 72, 84, 384),
(3959, 34, 84, 84, 412),
(3960, 34, 96, 84, 442),
(3961, 34, 108, 84, 570),
(3962, 34, 36, 96, 294),
(3963, 34, 48, 96, 328),
(3964, 34, 60, 96, 372),
(3965, 34, 72, 96, 408),
(3966, 34, 84, 96, 442),
(3967, 34, 96, 96, 476),
(3968, 34, 108, 96, 619),
(3969, 34, 36, 108, 304),
(3970, 34, 48, 108, 340),
(3971, 34, 60, 108, 395),
(3972, 34, 72, 108, 432),
(3973, 34, 84, 108, 470),
(3974, 34, 96, 108, 509),
(3975, 34, 108, 108, 657),
(3976, 34, 36, 120, 322),
(3977, 34, 48, 120, 358),
(3978, 34, 60, 120, 415),
(3979, 34, 72, 120, 459),
(3980, 34, 84, 120, 500),
(3981, 34, 96, 120, 541),
(3982, 34, 108, 120, 696),
(3983, 35, 36, 36, 259),
(3984, 35, 48, 36, 270),
(3985, 35, 60, 36, 298),
(3986, 35, 72, 36, 315),
(3987, 35, 84, 36, 335),
(3988, 35, 96, 36, 349),
(3989, 35, 108, 36, 455),
(3990, 35, 36, 48, 266),
(3991, 35, 48, 48, 278),
(3992, 35, 60, 48, 325),
(3993, 35, 72, 48, 335),
(3994, 35, 84, 48, 349),
(3995, 35, 96, 48, 367),
(3996, 35, 108, 48, 477),
(3997, 35, 36, 60, 274),
(3998, 35, 48, 60, 290),
(3999, 35, 60, 60, 335),
(4000, 35, 72, 60, 359),
(4001, 35, 84, 60, 382),
(4002, 35, 96, 60, 404),
(4003, 35, 108, 60, 528),
(4004, 35, 36, 72, 282),
(4005, 35, 48, 72, 317),
(4006, 35, 60, 72, 355),
(4007, 35, 72, 72, 386),
(4008, 35, 84, 72, 414),
(4009, 35, 96, 72, 441),
(4010, 35, 108, 72, 548),
(4011, 35, 36, 84, 290),
(4012, 35, 48, 84, 334),
(4013, 35, 60, 84, 386),
(4014, 35, 72, 84, 414),
(4015, 35, 84, 84, 445),
(4016, 35, 96, 84, 477),
(4017, 35, 108, 84, 599),
(4018, 35, 36, 96, 317),
(4019, 35, 48, 96, 353),
(4020, 35, 60, 96, 402),
(4021, 35, 72, 96, 441),
(4022, 35, 84, 96, 477),
(4023, 35, 96, 96, 514),
(4024, 35, 108, 96, 650),
(4025, 35, 36, 108, 328),
(4026, 35, 48, 108, 367),
(4027, 35, 60, 108, 426),
(4028, 35, 72, 108, 467),
(4029, 35, 84, 108, 508),
(4030, 35, 96, 108, 550),
(4031, 35, 108, 108, 690),
(4032, 35, 36, 120, 347),
(4033, 35, 48, 120, 386),
(4034, 35, 60, 120, 449),
(4035, 35, 72, 120, 495),
(4036, 35, 84, 120, 540),
(4037, 35, 96, 120, 585),
(4038, 35, 108, 120, 731),
(4039, 36, 36, 36, 185),
(4040, 36, 48, 36, 203),
(4041, 36, 60, 36, 231),
(4042, 36, 72, 36, 252),
(4043, 36, 84, 36, 275),
(4044, 36, 96, 36, 295),
(4045, 36, 108, 36, 368),
(4046, 36, 36, 48, 201),
(4047, 36, 48, 48, 223),
(4048, 36, 60, 48, 266),
(4049, 36, 72, 48, 287),
(4050, 36, 84, 48, 311),
(4051, 36, 96, 48, 337),
(4052, 36, 108, 48, 416),
(4053, 36, 36, 60, 217),
(4054, 36, 48, 60, 234),
(4055, 36, 60, 60, 281),
(4056, 36, 72, 60, 313),
(4057, 36, 84, 60, 343),
(4058, 36, 96, 60, 373),
(4059, 36, 108, 60, 463),
(4060, 36, 36, 72, 233),
(4061, 36, 48, 72, 267),
(4062, 36, 60, 72, 310),
(4063, 36, 72, 72, 349),
(4064, 36, 84, 72, 386),
(4065, 36, 96, 72, 422),
(4066, 36, 108, 72, 506),
(4067, 36, 36, 84, 241),
(4068, 36, 48, 84, 291),
(4069, 36, 60, 84, 345),
(4070, 36, 72, 84, 386),
(4071, 36, 84, 84, 428),
(4072, 36, 96, 84, 471),
(4073, 36, 108, 84, 566),
(4074, 36, 36, 96, 267),
(4075, 36, 48, 96, 318),
(4076, 36, 60, 96, 372),
(4077, 36, 72, 96, 422),
(4078, 36, 84, 96, 471),
(4079, 36, 96, 96, 520),
(4080, 36, 108, 96, 627),
(4081, 36, 36, 108, 284),
(4082, 36, 48, 108, 337),
(4083, 36, 60, 108, 404),
(4084, 36, 72, 108, 458),
(4085, 36, 84, 108, 513),
(4086, 36, 96, 108, 569),
(4087, 36, 108, 108, 682),
(4088, 36, 36, 120, 305),
(4089, 36, 48, 120, 362),
(4090, 36, 60, 120, 434),
(4091, 36, 72, 120, 496),
(4092, 36, 84, 120, 556),
(4093, 36, 96, 120, 617),
(4094, 36, 108, 120, 736),
(4095, 37, 36, 36, 260),
(4096, 37, 48, 36, 287),
(4097, 37, 60, 36, 319),
(4098, 37, 72, 36, 347),
(4099, 37, 84, 36, 380),
(4100, 37, 96, 36, 407),
(4101, 37, 108, 36, 543),
(4102, 37, 36, 48, 277),
(4103, 37, 48, 48, 307),
(4104, 37, 60, 48, 366),
(4105, 37, 72, 48, 395),
(4106, 37, 84, 48, 428),
(4107, 37, 96, 48, 464),
(4108, 37, 108, 48, 610),
(4109, 37, 36, 60, 299),
(4110, 37, 48, 60, 333),
(4111, 37, 60, 60, 401),
(4112, 37, 72, 60, 447),
(4113, 37, 84, 60, 491),
(4114, 37, 96, 60, 535),
(4115, 37, 108, 60, 702),
(4116, 37, 36, 72, 321),
(4117, 37, 48, 72, 381),
(4118, 37, 60, 72, 443),
(4119, 37, 72, 72, 499),
(4120, 37, 84, 72, 554),
(4121, 37, 96, 72, 607),
(4122, 37, 108, 72, 767),
(4123, 37, 36, 84, 343),
(4124, 37, 48, 84, 416),
(4125, 37, 60, 84, 494),
(4126, 37, 72, 84, 554),
(4127, 37, 84, 84, 615),
(4128, 37, 96, 84, 678),
(4129, 37, 108, 84, 859),
(4130, 37, 36, 96, 381),
(4131, 37, 48, 96, 456),
(4132, 37, 60, 96, 533),
(4133, 37, 72, 96, 607),
(4134, 37, 84, 96, 678),
(4135, 37, 96, 96, 750),
(4136, 37, 108, 96, 951),
(4137, 37, 36, 108, 406),
(4138, 37, 48, 108, 484),
(4139, 37, 60, 108, 579),
(4140, 37, 72, 108, 659),
(4141, 37, 84, 108, 739),
(4142, 37, 96, 108, 821),
(4143, 37, 108, 108, 1034),
(4144, 37, 36, 120, 437),
(4145, 37, 48, 120, 521),
(4146, 37, 60, 120, 624),
(4147, 37, 72, 120, 714),
(4148, 37, 84, 120, 802),
(4149, 37, 96, 120, 891),
(4150, 37, 108, 120, 1118),
(4151, 38, 36, 36, 149),
(4152, 38, 48, 36, 164),
(4153, 38, 60, 36, 180),
(4154, 38, 72, 36, 197),
(4155, 38, 84, 36, 215),
(4156, 38, 96, 36, 231),
(4157, 38, 108, 36, 287),
(4158, 38, 36, 48, 162),
(4159, 38, 48, 48, 180),
(4160, 38, 60, 48, 208),
(4161, 38, 72, 48, 225),
(4162, 38, 84, 48, 244),
(4163, 38, 96, 48, 265),
(4164, 38, 108, 48, 326),
(4165, 38, 36, 60, 175),
(4166, 38, 48, 60, 190),
(4167, 38, 60, 60, 220),
(4168, 38, 72, 60, 245),
(4169, 38, 84, 60, 269),
(4170, 38, 96, 60, 293),
(4171, 38, 108, 60, 363),
(4172, 38, 36, 72, 189),
(4173, 38, 48, 72, 216),
(4174, 38, 60, 72, 243),
(4175, 38, 72, 72, 274),
(4176, 38, 84, 72, 304),
(4177, 38, 96, 72, 333),
(4178, 38, 108, 72, 397),
(4179, 38, 36, 84, 197),
(4180, 38, 48, 84, 236),
(4181, 38, 60, 84, 271),
(4182, 38, 72, 84, 304),
(4183, 38, 84, 84, 337),
(4184, 38, 96, 84, 372),
(4185, 38, 108, 84, 446),
(4186, 38, 36, 96, 218),
(4187, 38, 48, 96, 258),
(4188, 38, 60, 96, 293),
(4189, 38, 72, 96, 333),
(4190, 38, 84, 96, 372),
(4191, 38, 96, 96, 411),
(4192, 38, 108, 96, 494),
(4193, 38, 36, 108, 232),
(4194, 38, 48, 108, 274),
(4195, 38, 60, 108, 318),
(4196, 38, 72, 108, 361),
(4197, 38, 84, 108, 405),
(4198, 38, 96, 108, 450),
(4199, 38, 108, 108, 538),
(4200, 38, 36, 120, 250),
(4201, 38, 48, 120, 294),
(4202, 38, 60, 120, 342),
(4203, 38, 72, 120, 391),
(4204, 38, 84, 120, 440),
(4205, 38, 96, 120, 488),
(4206, 38, 108, 120, 581),
(4207, 39, 36, 36, 183),
(4208, 39, 48, 36, 201),
(4209, 39, 60, 36, 229),
(4210, 39, 72, 36, 250),
(4211, 39, 84, 36, 273),
(4212, 39, 96, 36, 292),
(4213, 39, 108, 36, 364),
(4214, 39, 36, 48, 199),
(4215, 39, 48, 48, 221),
(4216, 39, 60, 48, 263),
(4217, 39, 72, 48, 284),
(4218, 39, 84, 48, 307),
(4219, 39, 96, 48, 332),
(4220, 39, 108, 48, 411),
(4221, 39, 36, 60, 215),
(4222, 39, 48, 60, 239),
(4223, 39, 60, 60, 287),
(4224, 39, 72, 60, 320),
(4225, 39, 84, 60, 352),
(4226, 39, 96, 60, 383),
(4227, 39, 108, 60, 474),
(4228, 39, 36, 72, 230),
(4229, 39, 48, 72, 273),
(4230, 39, 60, 72, 318),
(4231, 39, 72, 72, 358),
(4232, 39, 84, 72, 396),
(4233, 39, 96, 72, 434),
(4234, 39, 108, 72, 519),
(4235, 39, 36, 84, 246),
(4236, 39, 48, 84, 298),
(4237, 39, 60, 84, 354),
(4238, 39, 72, 84, 396),
(4239, 39, 84, 84, 440),
(4240, 39, 96, 84, 485),
(4241, 39, 108, 84, 582),
(4242, 39, 36, 96, 273),
(4243, 39, 48, 96, 326),
(4244, 39, 60, 96, 382),
(4245, 39, 72, 96, 434),
(4246, 39, 84, 96, 485),
(4247, 39, 96, 96, 536),
(4248, 39, 108, 96, 644),
(4249, 39, 36, 108, 290),
(4250, 39, 48, 108, 346),
(4251, 39, 60, 108, 415),
(4252, 39, 72, 108, 472),
(4253, 39, 84, 108, 529),
(4254, 39, 96, 108, 587),
(4255, 39, 108, 108, 701),
(4256, 39, 36, 120, 313),
(4257, 39, 48, 120, 372),
(4258, 39, 60, 120, 446),
(4259, 39, 72, 120, 510),
(4260, 39, 84, 120, 573),
(4261, 39, 96, 120, 636),
(4262, 39, 108, 120, 758),
(4263, 40, 24, 36, 164),
(4264, 40, 36, 36, 181),
(4265, 40, 48, 36, 198),
(4266, 40, 60, 36, 216),
(4267, 40, 72, 36, 237),
(4268, 40, 84, 36, 254),
(4269, 40, 96, 36, 316),
(4270, 40, 108, 36, 40),
(4271, 40, 120, 36, 40),
(4272, 40, 144, 36, 156),
(4273, 40, 144, 36, 66),
(4274, 40, 156, 36, 79),
(4275, 40, 168, 36, 92),
(4276, 40, 24, 48, 179),
(4277, 40, 36, 48, 198),
(4278, 40, 48, 48, 228),
(4279, 40, 60, 48, 247),
(4280, 40, 72, 48, 268),
(4281, 40, 84, 48, 291),
(4282, 40, 96, 48, 358),
(4283, 40, 108, 48, 40),
(4284, 40, 120, 48, 33),
(4285, 40, 144, 48, 168),
(4286, 40, 144, 48, 55),
(4287, 40, 156, 48, 66),
(4288, 40, 168, 48, 77),
(4289, 40, 24, 60, 193),
(4290, 40, 36, 60, 209),
(4291, 40, 48, 60, 242),
(4292, 40, 60, 60, 270),
(4293, 40, 72, 60, 296),
(4294, 40, 84, 60, 323),
(4295, 40, 96, 60, 399),
(4296, 40, 108, 60, 53),
(4297, 40, 120, 60, 44),
(4298, 40, 144, 60, 180),
(4299, 40, 144, 60, 74),
(4300, 40, 156, 60, 88),
(4301, 40, 168, 60, 103),
(4302, 40, 24, 72, 207),
(4303, 40, 36, 72, 238),
(4304, 40, 48, 72, 268),
(4305, 40, 60, 72, 301),
(4306, 40, 72, 72, 334),
(4307, 40, 84, 72, 366),
(4308, 40, 96, 72, 437),
(4309, 40, 108, 72, 66),
(4310, 40, 120, 72, 55),
(4311, 40, 144, 72, 192),
(4312, 40, 144, 72, 83),
(4313, 40, 156, 72, 99),
(4314, 40, 168, 72, 116),
(4315, 40, 24, 84, 216),
(4316, 40, 36, 84, 259),
(4317, 40, 48, 84, 298),
(4318, 40, 60, 84, 334),
(4319, 40, 72, 84, 371),
(4320, 40, 84, 84, 409),
(4321, 40, 96, 84, 490),
(4322, 40, 108, 84, 79),
(4323, 40, 120, 84, 66),
(4324, 40, 144, 84, 204),
(4325, 40, 144, 84, 99),
(4326, 40, 156, 84, 119),
(4327, 40, 168, 84, 139),
(4328, 40, 24, 96, 240),
(4329, 40, 36, 96, 284),
(4330, 40, 48, 96, 322),
(4331, 40, 60, 96, 366),
(4332, 40, 72, 96, 409),
(4333, 40, 84, 96, 452),
(4334, 40, 96, 96, 543),
(4335, 40, 108, 96, 92),
(4336, 40, 120, 96, 71),
(4337, 40, 144, 96, 216),
(4338, 40, 144, 96, 116),
(4339, 40, 156, 96, 139),
(4340, 40, 168, 96, 162),
(4341, 40, 24, 108, 256),
(4342, 40, 36, 108, 301),
(4343, 40, 48, 108, 349),
(4344, 40, 60, 108, 398),
(4345, 40, 72, 108, 446),
(4346, 40, 84, 108, 495),
(4347, 40, 96, 108, 591),
(4348, 40, 108, 108, 106),
(4349, 40, 120, 108, 82),
(4350, 40, 144, 108, 228),
(4351, 40, 144, 108, 132),
(4352, 40, 156, 108, 159),
(4353, 40, 168, 108, 185),
(4354, 40, 24, 120, 275),
(4355, 40, 36, 120, 324),
(4356, 40, 48, 120, 376),
(4357, 40, 60, 120, 430),
(4358, 40, 72, 120, 484),
(4359, 40, 84, 120, 537),
(4360, 40, 96, 120, 639),
(4361, 40, 108, 120, 119),
(4362, 40, 120, 120, 92),
(4363, 40, 144, 120, 119),
(4364, 40, 144, 120, 149),
(4365, 40, 156, 120, 179),
(4366, 40, 168, 120, 209),
(4367, 40, 24, 132, 0),
(4368, 40, 36, 132, 0),
(4369, 40, 48, 132, 0),
(4370, 40, 60, 132, 0),
(4371, 40, 72, 132, 0),
(4372, 40, 84, 132, 0),
(4373, 40, 96, 132, 0),
(4374, 40, 108, 132, 132),
(4375, 40, 120, 132, 102),
(4376, 40, 144, 132, 132),
(4377, 40, 144, 132, 166),
(4378, 40, 156, 132, 199),
(4379, 40, 168, 132, 232),
(4380, 40, 24, 144, 0),
(4381, 40, 36, 144, 0),
(4382, 40, 48, 144, 0),
(4383, 40, 60, 144, 0),
(4384, 40, 72, 144, 0),
(4385, 40, 84, 144, 0),
(4386, 40, 96, 144, 0),
(4387, 40, 108, 144, 0),
(4388, 40, 120, 144, 0),
(4389, 40, 144, 144, 0),
(4390, 40, 144, 144, 0),
(4391, 40, 156, 144, 0),
(4392, 40, 168, 144, 0),
(4393, 40, 24, 156, 0),
(4394, 40, 36, 156, 0),
(4395, 40, 48, 156, 0),
(4396, 40, 60, 156, 0),
(4397, 40, 72, 156, 0),
(4398, 40, 84, 156, 0),
(4399, 40, 96, 156, 0),
(4400, 40, 108, 156, 0),
(4401, 40, 120, 156, 0),
(4402, 40, 144, 156, 0),
(4403, 40, 144, 156, 0),
(4404, 40, 156, 156, 0),
(4405, 40, 168, 156, 0),
(4406, 40, 24, 168, 0),
(4407, 40, 36, 168, 0),
(4408, 40, 48, 168, 0),
(4409, 40, 60, 168, 0),
(4410, 40, 72, 168, 0),
(4411, 40, 84, 168, 0),
(4412, 40, 96, 168, 0),
(4413, 40, 108, 168, 0),
(4414, 40, 120, 168, 0),
(4415, 40, 144, 168, 0),
(4416, 40, 144, 168, 0),
(4417, 40, 156, 168, 0),
(4418, 40, 168, 168, 0),
(4419, 40, 24, 180, 0),
(4420, 40, 36, 180, 0),
(4421, 40, 48, 180, 0),
(4422, 40, 60, 180, 0),
(4423, 40, 72, 180, 0),
(4424, 40, 84, 180, 0),
(4425, 40, 96, 180, 0),
(4426, 40, 108, 180, 0),
(4427, 40, 120, 180, 0),
(4428, 40, 144, 180, 0),
(4429, 40, 144, 180, 0),
(4430, 40, 156, 180, 0),
(4431, 40, 168, 180, 0),
(4432, 40, 24, 192, 40),
(4433, 40, 36, 192, 53),
(4434, 40, 48, 192, 66),
(4435, 40, 60, 192, 79),
(4436, 40, 72, 192, 92),
(4437, 40, 84, 192, 106),
(4438, 40, 96, 192, 119),
(4439, 40, 108, 192, 40),
(4440, 40, 120, 192, 40),
(4441, 40, 144, 192, 53),
(4442, 40, 144, 192, 66),
(4443, 40, 156, 192, 79),
(4444, 40, 168, 192, 92),
(4445, 40, 24, 204, 202),
(4446, 40, 36, 204, 221),
(4447, 40, 48, 204, 252),
(4448, 40, 60, 204, 275),
(4449, 40, 72, 204, 300),
(4450, 40, 84, 204, 321),
(4451, 40, 96, 204, 401),
(4452, 40, 108, 204, 40),
(4453, 40, 120, 204, 39),
(4454, 40, 144, 204, 52),
(4455, 40, 144, 204, 64),
(4456, 40, 156, 204, 77),
(4457, 40, 168, 204, 90),
(4458, 40, 24, 216, 219),
(4459, 40, 36, 216, 243),
(4460, 40, 48, 216, 289),
(4461, 40, 60, 216, 312);
INSERT INTO `price_style` (`row_id`, `style_id`, `row`, `col`, `price`) VALUES
(4462, 40, 72, 216, 337),
(4463, 40, 84, 216, 365),
(4464, 40, 96, 216, 452),
(4465, 40, 108, 216, 53),
(4466, 40, 120, 216, 52),
(4467, 40, 144, 216, 69),
(4468, 40, 144, 216, 86),
(4469, 40, 156, 216, 103),
(4470, 40, 168, 216, 120),
(4471, 40, 24, 228, 236),
(4472, 40, 36, 228, 263),
(4473, 40, 48, 228, 316),
(4474, 40, 60, 228, 352),
(4475, 40, 72, 228, 387),
(4476, 40, 84, 228, 421),
(4477, 40, 96, 228, 521),
(4478, 40, 108, 228, 66),
(4479, 40, 120, 228, 64),
(4480, 40, 144, 228, 82),
(4481, 40, 144, 228, 107),
(4482, 40, 156, 228, 129),
(4483, 40, 168, 228, 150),
(4484, 45, 24, 0, 243),
(4485, 45, 27, 0, 273),
(4486, 45, 30, 0, 304),
(4487, 45, 33, 0, 334),
(4488, 45, 36, 0, 364),
(4489, 45, 39, 0, 395),
(4490, 45, 42, 0, 425),
(4491, 45, 45, 0, 455),
(4492, 45, 48, 0, 486),
(4493, 45, 51, 0, 516),
(4494, 45, 54, 0, 546),
(4495, 45, 57, 0, 577),
(4496, 45, 60, 0, 607),
(4497, 45, 63, 0, 638),
(4498, 45, 66, 0, 668),
(4499, 45, 69, 0, 698),
(4500, 45, 72, 0, 729),
(4501, 45, 75, 0, 759),
(4502, 48, 0, 0, 0),
(4503, 49, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE `product_attribute` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `attribute_condition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_attribute`
--

INSERT INTO `product_attribute` (`id`, `attribute_id`, `product_id`, `category_id`, `attribute_condition`, `is_active`) VALUES
(206, 1, 23, 1, '', 0),
(207, 2, 23, 1, '', 0),
(208, 3, 23, 1, '', 0),
(209, 4, 23, 1, '', 0),
(210, 5, 23, 1, '', 0),
(211, 6, 23, 1, '', 0),
(212, 7, 23, 1, '', 0),
(213, 8, 23, 1, '', 0),
(214, 9, 23, 1, '', 0),
(215, 10, 23, 1, '', 0),
(216, 11, 23, 1, '', 0),
(217, 12, 23, 1, '', 0),
(218, 13, 23, 1, '', 0),
(219, 14, 23, 1, '', 0),
(220, 46, 23, 1, '', 0),
(221, 1, 25, 1, '', 0),
(222, 2, 25, 1, '', 0),
(223, 3, 25, 1, '', 0),
(224, 4, 25, 1, '', 0),
(225, 5, 25, 1, '', 0),
(226, 6, 25, 1, '', 0),
(227, 7, 25, 1, '', 0),
(228, 8, 25, 1, '', 0),
(229, 9, 25, 1, '', 0),
(230, 10, 25, 1, '', 0),
(231, 11, 25, 1, '', 0),
(232, 12, 25, 1, '', 0),
(233, 13, 25, 1, '', 0),
(234, 14, 25, 1, '', 0),
(235, 46, 25, 1, '', 0),
(236, 1, 26, 1, '', 0),
(237, 2, 26, 1, '', 0),
(238, 3, 26, 1, '', 0),
(239, 4, 26, 1, '', 0),
(240, 5, 26, 1, '', 0),
(241, 6, 26, 1, '', 0),
(242, 7, 26, 1, '', 0),
(243, 8, 26, 1, '', 0),
(244, 9, 26, 1, '', 0),
(245, 10, 26, 1, '', 0),
(246, 11, 26, 1, '', 0),
(247, 12, 26, 1, '', 0),
(248, 13, 26, 1, '', 0),
(249, 14, 26, 1, '', 0),
(250, 46, 26, 1, '', 0),
(251, 1, 27, 1, '', 0),
(252, 2, 27, 1, '', 0),
(253, 3, 27, 1, '', 0),
(254, 4, 27, 1, '', 0),
(255, 5, 27, 1, '', 0),
(256, 6, 27, 1, '', 0),
(257, 7, 27, 1, '', 0),
(258, 8, 27, 1, '', 0),
(259, 9, 27, 1, '', 0),
(260, 10, 27, 1, '', 0),
(261, 11, 27, 1, '', 0),
(262, 12, 27, 1, '', 0),
(263, 13, 27, 1, '', 0),
(264, 14, 27, 1, '', 0),
(265, 46, 27, 1, '', 0),
(344, 15, 32, 2, '', 0),
(345, 17, 32, 2, '', 0),
(346, 18, 32, 2, '', 0),
(347, 19, 32, 2, '', 0),
(348, 20, 32, 2, '', 0),
(349, 22, 32, 2, '', 0),
(350, 23, 32, 2, '', 0),
(351, 24, 32, 2, '', 0),
(352, 25, 32, 2, '', 0),
(353, 26, 32, 2, '', 0),
(354, 27, 32, 2, '', 0),
(355, 28, 32, 2, '', 0),
(356, 17, 31, 2, '', 0),
(357, 18, 31, 2, '', 0),
(358, 19, 31, 2, '', 0),
(359, 20, 31, 2, '', 0),
(360, 22, 31, 2, '', 0),
(361, 23, 31, 2, '', 0),
(362, 24, 31, 2, '', 0),
(363, 25, 31, 2, '', 0),
(364, 26, 31, 2, '', 0),
(365, 27, 31, 2, '', 0),
(366, 28, 31, 2, '', 0),
(367, 17, 30, 2, '', 0),
(368, 18, 30, 2, '', 0),
(369, 19, 30, 2, '', 0),
(370, 20, 30, 2, '', 0),
(371, 22, 30, 2, '', 0),
(372, 23, 30, 2, '', 0),
(373, 24, 30, 2, '', 0),
(374, 25, 30, 2, '', 0),
(375, 26, 30, 2, '', 0),
(376, 27, 30, 2, '', 0),
(377, 28, 30, 2, '', 0),
(378, 17, 29, 2, '', 0),
(379, 18, 29, 2, '', 0),
(380, 19, 29, 2, '', 0),
(381, 20, 29, 2, '', 0),
(382, 22, 29, 2, '', 0),
(383, 23, 29, 2, '', 0),
(384, 24, 29, 2, '', 0),
(385, 25, 29, 2, '', 0),
(386, 26, 29, 2, '', 0),
(387, 27, 29, 2, '', 0),
(388, 28, 29, 2, '', 0),
(389, 17, 38, 2, '', 0),
(390, 18, 38, 2, '', 0),
(391, 19, 38, 2, '', 0),
(392, 20, 38, 2, '', 0),
(393, 22, 38, 2, '', 0),
(394, 23, 38, 2, '', 0),
(395, 24, 38, 2, '', 0),
(396, 25, 38, 2, '', 0),
(397, 26, 38, 2, '', 0),
(398, 27, 38, 2, '', 0),
(399, 28, 38, 2, '', 0),
(400, 17, 37, 2, '', 0),
(401, 18, 37, 2, '', 0),
(402, 19, 37, 2, '', 0),
(403, 20, 37, 2, '', 0),
(404, 22, 37, 2, '', 0),
(405, 23, 37, 2, '', 0),
(406, 24, 37, 2, '', 0),
(407, 25, 37, 2, '', 0),
(408, 26, 37, 2, '', 0),
(409, 27, 37, 2, '', 0),
(410, 28, 37, 2, '', 0),
(411, 17, 36, 2, '', 0),
(412, 18, 36, 2, '', 0),
(413, 19, 36, 2, '', 0),
(414, 20, 36, 2, '', 0),
(415, 22, 36, 2, '', 0),
(416, 23, 36, 2, '', 0),
(417, 24, 36, 2, '', 0),
(418, 25, 36, 2, '', 0),
(419, 26, 36, 2, '', 0),
(420, 27, 36, 2, '', 0),
(421, 28, 36, 2, '', 0),
(422, 17, 35, 2, '', 0),
(423, 18, 35, 2, '', 0),
(424, 19, 35, 2, '', 0),
(425, 20, 35, 2, '', 0),
(426, 22, 35, 2, '', 0),
(427, 23, 35, 2, '', 0),
(428, 24, 35, 2, '', 0),
(429, 25, 35, 2, '', 0),
(430, 26, 35, 2, '', 0),
(431, 27, 35, 2, '', 0),
(432, 28, 35, 2, '', 0),
(433, 17, 34, 2, '', 0),
(434, 18, 34, 2, '', 0),
(435, 19, 34, 2, '', 0),
(436, 20, 34, 2, '', 0),
(437, 22, 34, 2, '', 0),
(438, 23, 34, 2, '', 0),
(439, 24, 34, 2, '', 0),
(440, 25, 34, 2, '', 0),
(441, 26, 34, 2, '', 0),
(442, 27, 34, 2, '', 0),
(443, 28, 34, 2, '', 0),
(444, 17, 33, 2, '', 0),
(445, 18, 33, 2, '', 0),
(446, 19, 33, 2, '', 0),
(447, 20, 33, 2, '', 0),
(448, 22, 33, 2, '', 0),
(449, 23, 33, 2, '', 0),
(450, 24, 33, 2, '', 0),
(451, 25, 33, 2, '', 0),
(452, 26, 33, 2, '', 0),
(453, 27, 33, 2, '', 0),
(454, 28, 33, 2, '', 0),
(494, 43, 42, 4, '', 0),
(495, 45, 42, 4, '', 0),
(524, 30, 39, 3, '', 0),
(525, 31, 39, 3, '', 0),
(526, 32, 39, 3, '', 0),
(527, 33, 39, 3, '', 0),
(528, 34, 39, 3, '', 0),
(529, 35, 39, 3, '', 0),
(530, 36, 39, 3, '', 0),
(531, 37, 39, 3, '', 0),
(532, 38, 39, 3, '', 0),
(533, 39, 39, 3, '', 0),
(534, 40, 39, 3, '', 0),
(535, 41, 39, 3, '', 0),
(536, 47, 39, 3, '', 0),
(537, 48, 39, 3, '', 0),
(538, 30, 41, 3, '', 0),
(539, 31, 41, 3, '', 0),
(540, 32, 41, 3, '', 0),
(541, 33, 41, 3, '', 0),
(542, 34, 41, 3, '', 0),
(543, 35, 41, 3, '', 0),
(544, 36, 41, 3, '', 0),
(545, 37, 41, 3, '', 0),
(546, 38, 41, 3, '', 0),
(547, 39, 41, 3, '', 0),
(548, 40, 41, 3, '', 0),
(549, 41, 41, 3, '', 0),
(550, 47, 41, 3, '', 0),
(551, 48, 41, 3, '', 0),
(552, 30, 40, 3, '', 0),
(553, 31, 40, 3, '', 0),
(554, 32, 40, 3, '', 0),
(555, 33, 40, 3, '', 0),
(556, 34, 40, 3, '', 0),
(557, 35, 40, 3, '', 0),
(558, 36, 40, 3, '', 0),
(559, 37, 40, 3, '', 0),
(560, 38, 40, 3, '', 0),
(561, 39, 40, 3, '', 0),
(562, 40, 40, 3, '', 0),
(563, 41, 40, 3, '', 0),
(564, 47, 40, 3, '', 0),
(565, 48, 40, 3, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option`
--

CREATE TABLE `product_attr_option` (
  `id` int(11) NOT NULL,
  `pro_attr_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option`
--

INSERT INTO `product_attr_option` (`id`, `pro_attr_id`, `product_id`, `attribute_id`, `option_id`, `option_condition`) VALUES
(539, 206, 23, 1, 1, ''),
(540, 206, 23, 1, 2, ''),
(541, 206, 23, 1, 3, ''),
(542, 207, 23, 2, 4, '-1/2'),
(543, 207, 23, 2, 5, ''),
(544, 208, 23, 3, 6, ''),
(545, 208, 23, 3, 7, ''),
(546, 209, 23, 4, 8, ''),
(547, 209, 23, 4, 9, ''),
(548, 210, 23, 5, 10, ''),
(549, 210, 23, 5, 11, ''),
(550, 211, 23, 6, 12, ''),
(551, 211, 23, 6, 13, ''),
(552, 212, 23, 7, 14, ''),
(553, 212, 23, 7, 15, ''),
(554, 213, 23, 8, 16, ''),
(555, 213, 23, 8, 17, ''),
(556, 214, 23, 9, 18, ''),
(557, 214, 23, 9, 19, ''),
(558, 214, 23, 9, 20, ''),
(559, 215, 23, 10, 21, '-1/8'),
(560, 215, 23, 10, 22, ''),
(561, 216, 23, 11, 23, ''),
(562, 216, 23, 11, 24, ''),
(563, 217, 23, 12, 25, ''),
(564, 217, 23, 12, 26, ''),
(565, 217, 23, 12, 27, ''),
(566, 218, 23, 13, 28, ''),
(567, 218, 23, 13, 29, '1'),
(568, 218, 23, 13, 30, '1'),
(569, 219, 23, 14, 31, ''),
(570, 219, 23, 14, 32, '-1/8'),
(571, 219, 23, 14, 33, '-1/8'),
(572, 219, 23, 14, 34, '-1/4'),
(573, 220, 23, 46, 109, ''),
(574, 220, 23, 46, 110, ''),
(575, 221, 25, 1, 1, ''),
(576, 221, 25, 1, 2, ''),
(577, 221, 25, 1, 3, ''),
(578, 222, 25, 2, 4, '-1/2'),
(579, 222, 25, 2, 5, ''),
(580, 223, 25, 3, 6, ''),
(581, 223, 25, 3, 7, ''),
(582, 224, 25, 4, 8, ''),
(583, 224, 25, 4, 9, ''),
(584, 225, 25, 5, 10, ''),
(585, 225, 25, 5, 11, ''),
(586, 226, 25, 6, 12, ''),
(587, 226, 25, 6, 13, ''),
(588, 227, 25, 7, 14, ''),
(589, 227, 25, 7, 15, ''),
(590, 228, 25, 8, 16, ''),
(591, 228, 25, 8, 17, ''),
(592, 229, 25, 9, 18, ''),
(593, 229, 25, 9, 19, ''),
(594, 229, 25, 9, 20, ''),
(595, 230, 25, 10, 21, '-1/8'),
(596, 230, 25, 10, 22, ''),
(597, 231, 25, 11, 23, ''),
(598, 231, 25, 11, 24, ''),
(599, 232, 25, 12, 25, ''),
(600, 232, 25, 12, 26, ''),
(601, 232, 25, 12, 27, ''),
(602, 233, 25, 13, 28, ''),
(603, 233, 25, 13, 29, '1'),
(604, 233, 25, 13, 30, '1'),
(605, 234, 25, 14, 31, ''),
(606, 234, 25, 14, 32, '-1/8'),
(607, 234, 25, 14, 33, '-1/8'),
(608, 234, 25, 14, 34, '-1/4'),
(609, 235, 25, 46, 109, ''),
(610, 235, 25, 46, 110, ''),
(611, 236, 26, 1, 1, ''),
(612, 236, 26, 1, 2, ''),
(613, 236, 26, 1, 3, ''),
(614, 237, 26, 2, 4, '-1/2'),
(615, 237, 26, 2, 5, ''),
(616, 238, 26, 3, 6, ''),
(617, 238, 26, 3, 7, ''),
(618, 239, 26, 4, 8, ''),
(619, 239, 26, 4, 9, ''),
(620, 240, 26, 5, 10, ''),
(621, 240, 26, 5, 11, ''),
(622, 241, 26, 6, 12, ''),
(623, 241, 26, 6, 13, ''),
(624, 242, 26, 7, 14, ''),
(625, 242, 26, 7, 15, ''),
(626, 243, 26, 8, 16, ''),
(627, 243, 26, 8, 17, ''),
(628, 244, 26, 9, 18, ''),
(629, 244, 26, 9, 19, ''),
(630, 244, 26, 9, 20, ''),
(631, 245, 26, 10, 21, '-1/8'),
(632, 245, 26, 10, 22, ''),
(633, 246, 26, 11, 23, ''),
(634, 246, 26, 11, 24, ''),
(635, 247, 26, 12, 25, ''),
(636, 247, 26, 12, 26, ''),
(637, 247, 26, 12, 27, ''),
(638, 248, 26, 13, 28, ''),
(639, 248, 26, 13, 29, '1'),
(640, 248, 26, 13, 30, '1'),
(641, 249, 26, 14, 31, ''),
(642, 249, 26, 14, 32, '-1/8'),
(643, 249, 26, 14, 33, '-1/8'),
(644, 249, 26, 14, 34, '-1/4'),
(645, 250, 26, 46, 109, ''),
(646, 250, 26, 46, 110, ''),
(647, 251, 27, 1, 1, ''),
(648, 251, 27, 1, 2, ''),
(649, 251, 27, 1, 3, ''),
(650, 252, 27, 2, 4, '-1/2'),
(651, 252, 27, 2, 5, ''),
(652, 253, 27, 3, 6, ''),
(653, 253, 27, 3, 7, ''),
(654, 254, 27, 4, 8, ''),
(655, 254, 27, 4, 9, ''),
(656, 255, 27, 5, 10, ''),
(657, 255, 27, 5, 11, ''),
(658, 256, 27, 6, 12, ''),
(659, 256, 27, 6, 13, ''),
(660, 257, 27, 7, 14, ''),
(661, 257, 27, 7, 15, ''),
(662, 258, 27, 8, 16, ''),
(663, 258, 27, 8, 17, ''),
(664, 259, 27, 9, 18, ''),
(665, 259, 27, 9, 19, ''),
(666, 259, 27, 9, 20, ''),
(667, 260, 27, 10, 21, '-1/8'),
(668, 260, 27, 10, 22, ''),
(669, 261, 27, 11, 23, ''),
(670, 261, 27, 11, 24, ''),
(671, 262, 27, 12, 25, ''),
(672, 262, 27, 12, 26, ''),
(673, 262, 27, 12, 27, ''),
(674, 263, 27, 13, 28, ''),
(675, 263, 27, 13, 29, '1'),
(676, 263, 27, 13, 30, '1'),
(677, 264, 27, 14, 31, ''),
(678, 264, 27, 14, 32, '-1/8'),
(679, 264, 27, 14, 33, '-1/8'),
(680, 264, 27, 14, 34, '-1/4'),
(681, 265, 27, 46, 109, ''),
(682, 265, 27, 46, 110, ''),
(887, 345, 32, 17, 38, '-1/8'),
(888, 345, 32, 17, 39, ''),
(889, 345, 32, 17, 40, '-1/4'),
(890, 346, 32, 18, 41, ''),
(891, 346, 32, 18, 42, ''),
(892, 346, 32, 18, 43, ''),
(893, 347, 32, 19, 44, ''),
(894, 347, 32, 19, 45, ''),
(895, 348, 32, 20, 46, ''),
(896, 348, 32, 20, 47, ''),
(897, 348, 32, 20, 48, ''),
(898, 348, 32, 20, 49, ''),
(899, 348, 32, 20, 50, ''),
(900, 348, 32, 20, 51, ''),
(901, 349, 32, 22, 58, ''),
(902, 349, 32, 22, 59, ''),
(903, 349, 32, 22, 60, ''),
(904, 349, 32, 22, 61, ''),
(905, 350, 32, 23, 62, ''),
(906, 350, 32, 23, 63, ''),
(907, 350, 32, 23, 64, ''),
(908, 351, 32, 24, 65, ''),
(909, 351, 32, 24, 66, ''),
(910, 352, 32, 25, 67, ''),
(911, 352, 32, 25, 68, ''),
(912, 353, 32, 26, 69, ''),
(913, 353, 32, 26, 70, ''),
(914, 354, 32, 27, 71, ''),
(915, 354, 32, 27, 72, ''),
(916, 355, 32, 28, 73, ''),
(917, 355, 32, 28, 74, ''),
(918, 356, 31, 17, 38, '-1/8'),
(919, 356, 31, 17, 39, ''),
(920, 356, 31, 17, 40, '-1/4'),
(921, 357, 31, 18, 41, ''),
(922, 357, 31, 18, 42, ''),
(923, 357, 31, 18, 43, ''),
(924, 358, 31, 19, 44, ''),
(925, 358, 31, 19, 45, ''),
(926, 359, 31, 20, 46, ''),
(927, 359, 31, 20, 47, ''),
(928, 359, 31, 20, 48, ''),
(929, 359, 31, 20, 49, ''),
(930, 359, 31, 20, 50, ''),
(931, 359, 31, 20, 51, ''),
(932, 360, 31, 22, 58, ''),
(933, 360, 31, 22, 59, ''),
(934, 360, 31, 22, 60, ''),
(935, 360, 31, 22, 61, ''),
(936, 361, 31, 23, 62, ''),
(937, 361, 31, 23, 63, ''),
(938, 361, 31, 23, 64, ''),
(939, 362, 31, 24, 65, ''),
(940, 362, 31, 24, 66, ''),
(941, 363, 31, 25, 67, ''),
(942, 363, 31, 25, 68, ''),
(943, 364, 31, 26, 69, ''),
(944, 364, 31, 26, 70, ''),
(945, 365, 31, 27, 71, ''),
(946, 365, 31, 27, 72, ''),
(947, 366, 31, 28, 73, ''),
(948, 366, 31, 28, 74, ''),
(949, 367, 30, 17, 38, '-1/8'),
(950, 367, 30, 17, 39, ''),
(951, 367, 30, 17, 40, '-1/4'),
(952, 368, 30, 18, 41, ''),
(953, 368, 30, 18, 42, ''),
(954, 368, 30, 18, 43, ''),
(955, 369, 30, 19, 44, ''),
(956, 369, 30, 19, 45, ''),
(957, 370, 30, 20, 46, ''),
(958, 370, 30, 20, 47, ''),
(959, 370, 30, 20, 48, ''),
(960, 370, 30, 20, 49, ''),
(961, 370, 30, 20, 50, ''),
(962, 370, 30, 20, 51, ''),
(963, 371, 30, 22, 58, ''),
(964, 371, 30, 22, 59, ''),
(965, 371, 30, 22, 60, ''),
(966, 371, 30, 22, 61, ''),
(967, 372, 30, 23, 62, ''),
(968, 372, 30, 23, 63, ''),
(969, 372, 30, 23, 64, ''),
(970, 373, 30, 24, 65, ''),
(971, 373, 30, 24, 66, ''),
(972, 374, 30, 25, 67, ''),
(973, 374, 30, 25, 68, ''),
(974, 375, 30, 26, 69, ''),
(975, 375, 30, 26, 70, ''),
(976, 376, 30, 27, 71, ''),
(977, 376, 30, 27, 72, ''),
(978, 377, 30, 28, 73, ''),
(979, 377, 30, 28, 74, ''),
(980, 378, 29, 17, 38, '-1/8'),
(981, 378, 29, 17, 39, ''),
(982, 378, 29, 17, 40, '-1/4'),
(983, 379, 29, 18, 41, ''),
(984, 379, 29, 18, 42, ''),
(985, 379, 29, 18, 43, ''),
(986, 380, 29, 19, 44, ''),
(987, 380, 29, 19, 45, ''),
(988, 381, 29, 20, 46, ''),
(989, 381, 29, 20, 47, ''),
(990, 381, 29, 20, 48, ''),
(991, 381, 29, 20, 49, ''),
(992, 381, 29, 20, 50, ''),
(993, 381, 29, 20, 51, ''),
(994, 382, 29, 22, 58, ''),
(995, 382, 29, 22, 59, ''),
(996, 382, 29, 22, 60, ''),
(997, 382, 29, 22, 61, ''),
(998, 383, 29, 23, 62, ''),
(999, 383, 29, 23, 63, ''),
(1000, 383, 29, 23, 64, ''),
(1001, 384, 29, 24, 65, ''),
(1002, 384, 29, 24, 66, ''),
(1003, 385, 29, 25, 67, ''),
(1004, 385, 29, 25, 68, ''),
(1005, 386, 29, 26, 69, ''),
(1006, 386, 29, 26, 70, ''),
(1007, 387, 29, 27, 71, ''),
(1008, 387, 29, 27, 72, ''),
(1009, 388, 29, 28, 73, ''),
(1010, 388, 29, 28, 74, ''),
(1011, 389, 38, 17, 38, '-1/8'),
(1012, 389, 38, 17, 39, ''),
(1013, 389, 38, 17, 40, '-1/4'),
(1014, 390, 38, 18, 41, ''),
(1015, 390, 38, 18, 42, ''),
(1016, 390, 38, 18, 43, ''),
(1017, 391, 38, 19, 44, ''),
(1018, 391, 38, 19, 45, ''),
(1019, 392, 38, 20, 46, ''),
(1020, 392, 38, 20, 47, ''),
(1021, 392, 38, 20, 48, ''),
(1022, 392, 38, 20, 49, ''),
(1023, 392, 38, 20, 50, ''),
(1024, 392, 38, 20, 51, ''),
(1025, 393, 38, 22, 58, ''),
(1026, 393, 38, 22, 59, ''),
(1027, 393, 38, 22, 60, ''),
(1028, 393, 38, 22, 61, ''),
(1029, 394, 38, 23, 62, ''),
(1030, 394, 38, 23, 63, ''),
(1031, 394, 38, 23, 64, ''),
(1032, 395, 38, 24, 65, ''),
(1033, 395, 38, 24, 66, ''),
(1034, 396, 38, 25, 67, ''),
(1035, 396, 38, 25, 68, ''),
(1036, 397, 38, 26, 69, ''),
(1037, 397, 38, 26, 70, ''),
(1038, 398, 38, 27, 71, ''),
(1039, 398, 38, 27, 72, ''),
(1040, 399, 38, 28, 73, ''),
(1041, 399, 38, 28, 74, ''),
(1042, 400, 37, 17, 38, '-1/8'),
(1043, 400, 37, 17, 39, ''),
(1044, 400, 37, 17, 40, '-1/4'),
(1045, 401, 37, 18, 41, ''),
(1046, 401, 37, 18, 42, ''),
(1047, 401, 37, 18, 43, ''),
(1048, 402, 37, 19, 44, ''),
(1049, 402, 37, 19, 45, ''),
(1050, 403, 37, 20, 46, ''),
(1051, 403, 37, 20, 47, ''),
(1052, 403, 37, 20, 48, ''),
(1053, 403, 37, 20, 49, ''),
(1054, 403, 37, 20, 50, ''),
(1055, 403, 37, 20, 51, ''),
(1056, 404, 37, 22, 58, ''),
(1057, 404, 37, 22, 59, ''),
(1058, 404, 37, 22, 60, ''),
(1059, 404, 37, 22, 61, ''),
(1060, 405, 37, 23, 62, ''),
(1061, 405, 37, 23, 63, ''),
(1062, 405, 37, 23, 64, ''),
(1063, 406, 37, 24, 65, ''),
(1064, 406, 37, 24, 66, ''),
(1065, 407, 37, 25, 67, ''),
(1066, 407, 37, 25, 68, ''),
(1067, 408, 37, 26, 69, ''),
(1068, 408, 37, 26, 70, ''),
(1069, 409, 37, 27, 71, ''),
(1070, 409, 37, 27, 72, ''),
(1071, 410, 37, 28, 73, ''),
(1072, 410, 37, 28, 74, ''),
(1073, 411, 36, 17, 38, '-1/8'),
(1074, 411, 36, 17, 39, ''),
(1075, 411, 36, 17, 40, '-1/4'),
(1076, 412, 36, 18, 41, ''),
(1077, 412, 36, 18, 42, ''),
(1078, 412, 36, 18, 43, ''),
(1079, 413, 36, 19, 44, ''),
(1080, 413, 36, 19, 45, ''),
(1081, 414, 36, 20, 46, ''),
(1082, 414, 36, 20, 47, ''),
(1083, 414, 36, 20, 48, ''),
(1084, 414, 36, 20, 49, ''),
(1085, 414, 36, 20, 50, ''),
(1086, 414, 36, 20, 51, ''),
(1087, 415, 36, 22, 58, ''),
(1088, 415, 36, 22, 59, ''),
(1089, 415, 36, 22, 60, ''),
(1090, 415, 36, 22, 61, ''),
(1091, 416, 36, 23, 62, ''),
(1092, 416, 36, 23, 63, ''),
(1093, 416, 36, 23, 64, ''),
(1094, 417, 36, 24, 65, ''),
(1095, 417, 36, 24, 66, ''),
(1096, 418, 36, 25, 67, ''),
(1097, 418, 36, 25, 68, ''),
(1098, 419, 36, 26, 69, ''),
(1099, 419, 36, 26, 70, ''),
(1100, 420, 36, 27, 71, ''),
(1101, 420, 36, 27, 72, ''),
(1102, 421, 36, 28, 73, ''),
(1103, 421, 36, 28, 74, ''),
(1104, 422, 35, 17, 38, '-1/8'),
(1105, 422, 35, 17, 39, ''),
(1106, 422, 35, 17, 40, '-1/4'),
(1107, 423, 35, 18, 41, ''),
(1108, 423, 35, 18, 42, ''),
(1109, 423, 35, 18, 43, ''),
(1110, 424, 35, 19, 44, ''),
(1111, 424, 35, 19, 45, ''),
(1112, 425, 35, 20, 46, ''),
(1113, 425, 35, 20, 47, ''),
(1114, 425, 35, 20, 48, ''),
(1115, 425, 35, 20, 49, ''),
(1116, 425, 35, 20, 50, ''),
(1117, 425, 35, 20, 51, ''),
(1118, 426, 35, 22, 58, ''),
(1119, 426, 35, 22, 59, ''),
(1120, 426, 35, 22, 60, ''),
(1121, 426, 35, 22, 61, ''),
(1122, 427, 35, 23, 62, ''),
(1123, 427, 35, 23, 63, ''),
(1124, 427, 35, 23, 64, ''),
(1125, 428, 35, 24, 65, ''),
(1126, 428, 35, 24, 66, ''),
(1127, 429, 35, 25, 67, ''),
(1128, 429, 35, 25, 68, ''),
(1129, 430, 35, 26, 69, ''),
(1130, 430, 35, 26, 70, ''),
(1131, 431, 35, 27, 71, ''),
(1132, 431, 35, 27, 72, ''),
(1133, 432, 35, 28, 73, ''),
(1134, 432, 35, 28, 74, ''),
(1135, 433, 34, 17, 38, '-1/8'),
(1136, 433, 34, 17, 39, ''),
(1137, 433, 34, 17, 40, '-1/4'),
(1138, 434, 34, 18, 41, ''),
(1139, 434, 34, 18, 42, ''),
(1140, 434, 34, 18, 43, ''),
(1141, 435, 34, 19, 44, ''),
(1142, 435, 34, 19, 45, ''),
(1143, 436, 34, 20, 46, ''),
(1144, 436, 34, 20, 47, ''),
(1145, 436, 34, 20, 48, ''),
(1146, 436, 34, 20, 49, ''),
(1147, 436, 34, 20, 50, ''),
(1148, 436, 34, 20, 51, ''),
(1149, 437, 34, 22, 58, ''),
(1150, 437, 34, 22, 59, ''),
(1151, 437, 34, 22, 60, ''),
(1152, 437, 34, 22, 61, ''),
(1153, 438, 34, 23, 62, ''),
(1154, 438, 34, 23, 63, ''),
(1155, 438, 34, 23, 64, ''),
(1156, 439, 34, 24, 65, ''),
(1157, 439, 34, 24, 66, ''),
(1158, 440, 34, 25, 67, ''),
(1159, 440, 34, 25, 68, ''),
(1160, 441, 34, 26, 69, ''),
(1161, 441, 34, 26, 70, ''),
(1162, 442, 34, 27, 71, ''),
(1163, 442, 34, 27, 72, ''),
(1164, 443, 34, 28, 73, ''),
(1165, 443, 34, 28, 74, ''),
(1166, 444, 33, 17, 38, '-1/8'),
(1167, 444, 33, 17, 39, ''),
(1168, 444, 33, 17, 40, '-1/4'),
(1169, 445, 33, 18, 41, ''),
(1170, 445, 33, 18, 42, ''),
(1171, 445, 33, 18, 43, ''),
(1172, 446, 33, 19, 44, ''),
(1173, 446, 33, 19, 45, ''),
(1174, 447, 33, 20, 46, ''),
(1175, 447, 33, 20, 47, ''),
(1176, 447, 33, 20, 48, ''),
(1177, 447, 33, 20, 49, ''),
(1178, 447, 33, 20, 50, ''),
(1179, 447, 33, 20, 51, ''),
(1180, 448, 33, 22, 58, ''),
(1181, 448, 33, 22, 59, ''),
(1182, 448, 33, 22, 60, ''),
(1183, 448, 33, 22, 61, ''),
(1184, 449, 33, 23, 62, ''),
(1185, 449, 33, 23, 63, ''),
(1186, 449, 33, 23, 64, ''),
(1187, 450, 33, 24, 65, ''),
(1188, 450, 33, 24, 66, ''),
(1189, 451, 33, 25, 67, ''),
(1190, 451, 33, 25, 68, ''),
(1191, 452, 33, 26, 69, ''),
(1192, 452, 33, 26, 70, ''),
(1193, 453, 33, 27, 71, ''),
(1194, 453, 33, 27, 72, ''),
(1195, 454, 33, 28, 73, ''),
(1196, 454, 33, 28, 74, ''),
(1293, 495, 42, 45, 107, ''),
(1294, 495, 42, 45, 108, ''),
(1363, 524, 39, 30, 78, ''),
(1364, 524, 39, 30, 79, ''),
(1365, 525, 39, 31, 80, ''),
(1366, 525, 39, 31, 81, ''),
(1367, 527, 39, 33, 82, ''),
(1368, 527, 39, 33, 83, ''),
(1369, 529, 39, 35, 84, ''),
(1370, 529, 39, 35, 85, ''),
(1371, 529, 39, 35, 86, ''),
(1372, 529, 39, 35, 87, ''),
(1373, 530, 39, 36, 88, ''),
(1374, 530, 39, 36, 89, ''),
(1375, 530, 39, 36, 90, ''),
(1376, 531, 39, 37, 91, ''),
(1377, 531, 39, 37, 92, ''),
(1378, 532, 39, 38, 93, ''),
(1379, 532, 39, 38, 94, ''),
(1380, 533, 39, 39, 95, ''),
(1381, 533, 39, 39, 96, ''),
(1382, 533, 39, 39, 97, ''),
(1383, 533, 39, 39, 98, ''),
(1384, 533, 39, 39, 99, ''),
(1385, 533, 39, 39, 100, ''),
(1386, 533, 39, 39, 101, ''),
(1387, 533, 39, 39, 102, ''),
(1388, 534, 39, 40, 103, ''),
(1389, 534, 39, 40, 104, ''),
(1390, 535, 39, 41, 105, ''),
(1391, 535, 39, 41, 106, ''),
(1392, 536, 39, 47, 111, ''),
(1393, 536, 39, 47, 112, ''),
(1394, 536, 39, 47, 113, ''),
(1395, 537, 39, 48, 114, ''),
(1396, 537, 39, 48, 115, ''),
(1397, 538, 41, 30, 78, ''),
(1398, 538, 41, 30, 79, ''),
(1399, 539, 41, 31, 80, ''),
(1400, 539, 41, 31, 81, ''),
(1401, 541, 41, 33, 82, ''),
(1402, 541, 41, 33, 83, ''),
(1403, 543, 41, 35, 84, ''),
(1404, 543, 41, 35, 85, ''),
(1405, 543, 41, 35, 86, ''),
(1406, 543, 41, 35, 87, ''),
(1407, 544, 41, 36, 88, ''),
(1408, 544, 41, 36, 89, ''),
(1409, 544, 41, 36, 90, ''),
(1410, 545, 41, 37, 91, ''),
(1411, 545, 41, 37, 92, ''),
(1412, 546, 41, 38, 93, ''),
(1413, 546, 41, 38, 94, ''),
(1414, 547, 41, 39, 95, ''),
(1415, 547, 41, 39, 96, ''),
(1416, 547, 41, 39, 97, ''),
(1417, 547, 41, 39, 98, ''),
(1418, 547, 41, 39, 99, ''),
(1419, 547, 41, 39, 100, ''),
(1420, 547, 41, 39, 101, ''),
(1421, 547, 41, 39, 102, ''),
(1422, 548, 41, 40, 103, ''),
(1423, 548, 41, 40, 104, ''),
(1424, 549, 41, 41, 105, ''),
(1425, 549, 41, 41, 106, ''),
(1426, 550, 41, 47, 111, ''),
(1427, 550, 41, 47, 112, ''),
(1428, 550, 41, 47, 113, ''),
(1429, 551, 41, 48, 114, ''),
(1430, 551, 41, 48, 115, ''),
(1431, 552, 40, 30, 78, ''),
(1432, 552, 40, 30, 79, ''),
(1433, 553, 40, 31, 80, ''),
(1434, 553, 40, 31, 81, ''),
(1435, 555, 40, 33, 82, ''),
(1436, 555, 40, 33, 83, ''),
(1437, 557, 40, 35, 84, ''),
(1438, 557, 40, 35, 85, ''),
(1439, 557, 40, 35, 86, ''),
(1440, 557, 40, 35, 87, ''),
(1441, 558, 40, 36, 88, ''),
(1442, 558, 40, 36, 89, ''),
(1443, 558, 40, 36, 90, ''),
(1444, 559, 40, 37, 91, ''),
(1445, 559, 40, 37, 92, ''),
(1446, 560, 40, 38, 93, ''),
(1447, 560, 40, 38, 94, ''),
(1448, 561, 40, 39, 95, ''),
(1449, 561, 40, 39, 96, ''),
(1450, 561, 40, 39, 97, ''),
(1451, 561, 40, 39, 98, ''),
(1452, 561, 40, 39, 99, ''),
(1453, 561, 40, 39, 100, ''),
(1454, 561, 40, 39, 101, ''),
(1455, 561, 40, 39, 102, ''),
(1456, 562, 40, 40, 103, ''),
(1457, 562, 40, 40, 104, ''),
(1458, 563, 40, 41, 105, ''),
(1459, 563, 40, 41, 106, ''),
(1460, 564, 40, 47, 111, ''),
(1461, 564, 40, 47, 112, ''),
(1462, 564, 40, 47, 113, ''),
(1463, 565, 40, 48, 114, ''),
(1464, 565, 40, 48, 115, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option_option`
--

CREATE TABLE `product_attr_option_option` (
  `id` int(11) NOT NULL,
  `pro_att_op_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_op_id` int(11) NOT NULL,
  `op_op_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option_option`
--

INSERT INTO `product_attr_option_option` (`id`, `pro_att_op_id`, `product_id`, `attribute_id`, `op_op_id`, `op_op_condition`) VALUES
(535, 540, 23, 1, 1, '-3/8'),
(536, 540, 23, 1, 2, '-1/4'),
(537, 541, 23, 1, 3, '-1/4'),
(538, 541, 23, 1, 4, '-1/4'),
(539, 541, 23, 1, 5, '-1/4'),
(540, 570, 23, 14, 6, ''),
(541, 570, 23, 14, 7, ''),
(542, 571, 23, 14, 8, ''),
(543, 571, 23, 14, 9, ''),
(544, 572, 23, 14, 10, ''),
(545, 572, 23, 14, 11, ''),
(546, 576, 25, 1, 1, '-3/8'),
(547, 576, 25, 1, 2, '-1/4'),
(548, 577, 25, 1, 3, '-1/4'),
(549, 577, 25, 1, 4, '-1/4'),
(550, 577, 25, 1, 5, '-1/4'),
(551, 606, 25, 14, 6, ''),
(552, 606, 25, 14, 7, ''),
(553, 607, 25, 14, 8, ''),
(554, 607, 25, 14, 9, ''),
(555, 608, 25, 14, 10, ''),
(556, 608, 25, 14, 11, ''),
(557, 612, 26, 1, 1, '-3/8'),
(558, 612, 26, 1, 2, '-1/4'),
(559, 613, 26, 1, 3, '-1/4'),
(560, 613, 26, 1, 4, '-1/4'),
(561, 613, 26, 1, 5, '-1/4'),
(562, 642, 26, 14, 6, ''),
(563, 642, 26, 14, 7, ''),
(564, 643, 26, 14, 8, ''),
(565, 643, 26, 14, 9, ''),
(566, 644, 26, 14, 10, ''),
(567, 644, 26, 14, 11, ''),
(568, 648, 27, 1, 1, '-3/8'),
(569, 648, 27, 1, 2, '-1/4'),
(570, 649, 27, 1, 3, '-1/4'),
(571, 649, 27, 1, 4, '-1/4'),
(572, 649, 27, 1, 5, '-1/4'),
(573, 678, 27, 14, 6, ''),
(574, 678, 27, 14, 7, ''),
(575, 679, 27, 14, 8, ''),
(576, 679, 27, 14, 9, ''),
(577, 680, 27, 14, 10, ''),
(578, 680, 27, 14, 11, ''),
(819, 892, 32, 18, 12, ''),
(820, 892, 32, 18, 13, ''),
(821, 892, 32, 18, 14, ''),
(822, 897, 32, 20, 15, ''),
(823, 897, 32, 20, 16, ''),
(824, 898, 32, 20, 17, ''),
(825, 898, 32, 20, 18, ''),
(826, 909, 32, 24, 23, ''),
(827, 909, 32, 24, 24, ''),
(828, 909, 32, 24, 25, ''),
(829, 909, 32, 24, 26, ''),
(830, 909, 32, 24, 27, ''),
(831, 909, 32, 24, 28, ''),
(832, 909, 32, 24, 29, ''),
(833, 909, 32, 24, 30, ''),
(834, 909, 32, 24, 31, ''),
(835, 909, 32, 24, 32, ''),
(836, 909, 32, 24, 33, ''),
(837, 911, 32, 25, 34, ''),
(838, 911, 32, 25, 35, ''),
(839, 911, 32, 25, 36, ''),
(840, 911, 32, 25, 37, ''),
(841, 911, 32, 25, 38, ''),
(842, 911, 32, 25, 39, ''),
(843, 911, 32, 25, 40, ''),
(844, 911, 32, 25, 41, ''),
(845, 911, 32, 25, 42, ''),
(846, 911, 32, 25, 43, ''),
(847, 911, 32, 25, 44, ''),
(848, 913, 32, 26, 45, ''),
(849, 913, 32, 26, 46, ''),
(850, 913, 32, 26, 47, ''),
(851, 913, 32, 26, 48, ''),
(852, 913, 32, 26, 49, ''),
(853, 913, 32, 26, 50, ''),
(854, 913, 32, 26, 51, ''),
(855, 913, 32, 26, 52, ''),
(856, 913, 32, 26, 53, ''),
(857, 913, 32, 26, 54, ''),
(858, 913, 32, 26, 55, ''),
(859, 923, 31, 18, 12, ''),
(860, 923, 31, 18, 13, ''),
(861, 923, 31, 18, 14, ''),
(862, 928, 31, 20, 15, ''),
(863, 928, 31, 20, 16, ''),
(864, 929, 31, 20, 17, ''),
(865, 929, 31, 20, 18, ''),
(866, 940, 31, 24, 23, ''),
(867, 940, 31, 24, 24, ''),
(868, 940, 31, 24, 25, ''),
(869, 940, 31, 24, 26, ''),
(870, 940, 31, 24, 27, ''),
(871, 940, 31, 24, 28, ''),
(872, 940, 31, 24, 29, ''),
(873, 940, 31, 24, 30, ''),
(874, 940, 31, 24, 31, ''),
(875, 940, 31, 24, 32, ''),
(876, 940, 31, 24, 33, ''),
(877, 942, 31, 25, 34, ''),
(878, 942, 31, 25, 35, ''),
(879, 942, 31, 25, 36, ''),
(880, 942, 31, 25, 37, ''),
(881, 942, 31, 25, 38, ''),
(882, 942, 31, 25, 39, ''),
(883, 942, 31, 25, 40, ''),
(884, 942, 31, 25, 41, ''),
(885, 942, 31, 25, 42, ''),
(886, 942, 31, 25, 43, ''),
(887, 942, 31, 25, 44, ''),
(888, 944, 31, 26, 45, ''),
(889, 944, 31, 26, 46, ''),
(890, 944, 31, 26, 47, ''),
(891, 944, 31, 26, 48, ''),
(892, 944, 31, 26, 49, ''),
(893, 944, 31, 26, 50, ''),
(894, 944, 31, 26, 51, ''),
(895, 944, 31, 26, 52, ''),
(896, 944, 31, 26, 53, ''),
(897, 944, 31, 26, 54, ''),
(898, 944, 31, 26, 55, ''),
(899, 954, 30, 18, 12, ''),
(900, 954, 30, 18, 13, ''),
(901, 954, 30, 18, 14, ''),
(902, 959, 30, 20, 15, ''),
(903, 959, 30, 20, 16, ''),
(904, 960, 30, 20, 17, ''),
(905, 960, 30, 20, 18, ''),
(906, 971, 30, 24, 23, ''),
(907, 971, 30, 24, 24, ''),
(908, 971, 30, 24, 25, ''),
(909, 971, 30, 24, 26, ''),
(910, 971, 30, 24, 27, ''),
(911, 971, 30, 24, 28, ''),
(912, 971, 30, 24, 29, ''),
(913, 971, 30, 24, 30, ''),
(914, 971, 30, 24, 31, ''),
(915, 971, 30, 24, 32, ''),
(916, 971, 30, 24, 33, ''),
(917, 973, 30, 25, 34, ''),
(918, 973, 30, 25, 35, ''),
(919, 973, 30, 25, 36, ''),
(920, 973, 30, 25, 37, ''),
(921, 973, 30, 25, 38, ''),
(922, 973, 30, 25, 39, ''),
(923, 973, 30, 25, 40, ''),
(924, 973, 30, 25, 41, ''),
(925, 973, 30, 25, 42, ''),
(926, 973, 30, 25, 43, ''),
(927, 973, 30, 25, 44, ''),
(928, 975, 30, 26, 45, ''),
(929, 975, 30, 26, 46, ''),
(930, 975, 30, 26, 47, ''),
(931, 975, 30, 26, 48, ''),
(932, 975, 30, 26, 49, ''),
(933, 975, 30, 26, 50, ''),
(934, 975, 30, 26, 51, ''),
(935, 975, 30, 26, 52, ''),
(936, 975, 30, 26, 53, ''),
(937, 975, 30, 26, 54, ''),
(938, 975, 30, 26, 55, ''),
(939, 985, 29, 18, 12, ''),
(940, 985, 29, 18, 13, ''),
(941, 985, 29, 18, 14, ''),
(942, 990, 29, 20, 15, ''),
(943, 990, 29, 20, 16, ''),
(944, 991, 29, 20, 17, ''),
(945, 991, 29, 20, 18, ''),
(946, 1002, 29, 24, 23, ''),
(947, 1002, 29, 24, 24, ''),
(948, 1002, 29, 24, 25, ''),
(949, 1002, 29, 24, 26, ''),
(950, 1002, 29, 24, 27, ''),
(951, 1002, 29, 24, 28, ''),
(952, 1002, 29, 24, 29, ''),
(953, 1002, 29, 24, 30, ''),
(954, 1002, 29, 24, 31, ''),
(955, 1002, 29, 24, 32, ''),
(956, 1002, 29, 24, 33, ''),
(957, 1004, 29, 25, 34, ''),
(958, 1004, 29, 25, 35, ''),
(959, 1004, 29, 25, 36, ''),
(960, 1004, 29, 25, 37, ''),
(961, 1004, 29, 25, 38, ''),
(962, 1004, 29, 25, 39, ''),
(963, 1004, 29, 25, 40, ''),
(964, 1004, 29, 25, 41, ''),
(965, 1004, 29, 25, 42, ''),
(966, 1004, 29, 25, 43, ''),
(967, 1004, 29, 25, 44, ''),
(968, 1006, 29, 26, 45, ''),
(969, 1006, 29, 26, 46, ''),
(970, 1006, 29, 26, 47, ''),
(971, 1006, 29, 26, 48, ''),
(972, 1006, 29, 26, 49, ''),
(973, 1006, 29, 26, 50, ''),
(974, 1006, 29, 26, 51, ''),
(975, 1006, 29, 26, 52, ''),
(976, 1006, 29, 26, 53, ''),
(977, 1006, 29, 26, 54, ''),
(978, 1006, 29, 26, 55, ''),
(979, 1016, 38, 18, 12, ''),
(980, 1016, 38, 18, 13, ''),
(981, 1016, 38, 18, 14, ''),
(982, 1021, 38, 20, 15, ''),
(983, 1021, 38, 20, 16, ''),
(984, 1022, 38, 20, 17, ''),
(985, 1022, 38, 20, 18, ''),
(986, 1033, 38, 24, 23, ''),
(987, 1033, 38, 24, 24, ''),
(988, 1033, 38, 24, 25, ''),
(989, 1033, 38, 24, 26, ''),
(990, 1033, 38, 24, 27, ''),
(991, 1033, 38, 24, 28, ''),
(992, 1033, 38, 24, 29, ''),
(993, 1033, 38, 24, 30, ''),
(994, 1033, 38, 24, 31, ''),
(995, 1033, 38, 24, 32, ''),
(996, 1033, 38, 24, 33, ''),
(997, 1035, 38, 25, 34, ''),
(998, 1035, 38, 25, 35, ''),
(999, 1035, 38, 25, 36, ''),
(1000, 1035, 38, 25, 37, ''),
(1001, 1035, 38, 25, 38, ''),
(1002, 1035, 38, 25, 39, ''),
(1003, 1035, 38, 25, 40, ''),
(1004, 1035, 38, 25, 41, ''),
(1005, 1035, 38, 25, 42, ''),
(1006, 1035, 38, 25, 43, ''),
(1007, 1035, 38, 25, 44, ''),
(1008, 1037, 38, 26, 45, ''),
(1009, 1037, 38, 26, 46, ''),
(1010, 1037, 38, 26, 47, ''),
(1011, 1037, 38, 26, 48, ''),
(1012, 1037, 38, 26, 49, ''),
(1013, 1037, 38, 26, 50, ''),
(1014, 1037, 38, 26, 51, ''),
(1015, 1037, 38, 26, 52, ''),
(1016, 1037, 38, 26, 53, ''),
(1017, 1037, 38, 26, 54, ''),
(1018, 1037, 38, 26, 55, ''),
(1019, 1047, 37, 18, 12, ''),
(1020, 1047, 37, 18, 13, ''),
(1021, 1047, 37, 18, 14, ''),
(1022, 1052, 37, 20, 15, ''),
(1023, 1052, 37, 20, 16, ''),
(1024, 1053, 37, 20, 17, ''),
(1025, 1053, 37, 20, 18, ''),
(1026, 1064, 37, 24, 23, ''),
(1027, 1064, 37, 24, 24, ''),
(1028, 1064, 37, 24, 25, ''),
(1029, 1064, 37, 24, 26, ''),
(1030, 1064, 37, 24, 27, ''),
(1031, 1064, 37, 24, 28, ''),
(1032, 1064, 37, 24, 29, ''),
(1033, 1064, 37, 24, 30, ''),
(1034, 1064, 37, 24, 31, ''),
(1035, 1064, 37, 24, 32, ''),
(1036, 1064, 37, 24, 33, ''),
(1037, 1066, 37, 25, 34, ''),
(1038, 1066, 37, 25, 35, ''),
(1039, 1066, 37, 25, 36, ''),
(1040, 1066, 37, 25, 37, ''),
(1041, 1066, 37, 25, 38, ''),
(1042, 1066, 37, 25, 39, ''),
(1043, 1066, 37, 25, 40, ''),
(1044, 1066, 37, 25, 41, ''),
(1045, 1066, 37, 25, 42, ''),
(1046, 1066, 37, 25, 43, ''),
(1047, 1066, 37, 25, 44, ''),
(1048, 1068, 37, 26, 45, ''),
(1049, 1068, 37, 26, 46, ''),
(1050, 1068, 37, 26, 47, ''),
(1051, 1068, 37, 26, 48, ''),
(1052, 1068, 37, 26, 49, ''),
(1053, 1068, 37, 26, 50, ''),
(1054, 1068, 37, 26, 51, ''),
(1055, 1068, 37, 26, 52, ''),
(1056, 1068, 37, 26, 53, ''),
(1057, 1068, 37, 26, 54, ''),
(1058, 1068, 37, 26, 55, ''),
(1059, 1078, 36, 18, 12, ''),
(1060, 1078, 36, 18, 13, ''),
(1061, 1078, 36, 18, 14, ''),
(1062, 1083, 36, 20, 15, ''),
(1063, 1083, 36, 20, 16, ''),
(1064, 1084, 36, 20, 17, ''),
(1065, 1084, 36, 20, 18, ''),
(1066, 1095, 36, 24, 23, ''),
(1067, 1095, 36, 24, 24, ''),
(1068, 1095, 36, 24, 25, ''),
(1069, 1095, 36, 24, 26, ''),
(1070, 1095, 36, 24, 27, ''),
(1071, 1095, 36, 24, 28, ''),
(1072, 1095, 36, 24, 29, ''),
(1073, 1095, 36, 24, 30, ''),
(1074, 1095, 36, 24, 31, ''),
(1075, 1095, 36, 24, 32, ''),
(1076, 1095, 36, 24, 33, ''),
(1077, 1097, 36, 25, 34, ''),
(1078, 1097, 36, 25, 35, ''),
(1079, 1097, 36, 25, 36, ''),
(1080, 1097, 36, 25, 37, ''),
(1081, 1097, 36, 25, 38, ''),
(1082, 1097, 36, 25, 39, ''),
(1083, 1097, 36, 25, 40, ''),
(1084, 1097, 36, 25, 41, ''),
(1085, 1097, 36, 25, 42, ''),
(1086, 1097, 36, 25, 43, ''),
(1087, 1097, 36, 25, 44, ''),
(1088, 1099, 36, 26, 45, ''),
(1089, 1099, 36, 26, 46, ''),
(1090, 1099, 36, 26, 47, ''),
(1091, 1099, 36, 26, 48, ''),
(1092, 1099, 36, 26, 49, ''),
(1093, 1099, 36, 26, 50, ''),
(1094, 1099, 36, 26, 51, ''),
(1095, 1099, 36, 26, 52, ''),
(1096, 1099, 36, 26, 53, ''),
(1097, 1099, 36, 26, 54, ''),
(1098, 1099, 36, 26, 55, ''),
(1099, 1109, 35, 18, 12, ''),
(1100, 1109, 35, 18, 13, ''),
(1101, 1109, 35, 18, 14, ''),
(1102, 1114, 35, 20, 15, ''),
(1103, 1114, 35, 20, 16, ''),
(1104, 1115, 35, 20, 17, ''),
(1105, 1115, 35, 20, 18, ''),
(1106, 1126, 35, 24, 23, ''),
(1107, 1126, 35, 24, 24, ''),
(1108, 1126, 35, 24, 25, ''),
(1109, 1126, 35, 24, 26, ''),
(1110, 1126, 35, 24, 27, ''),
(1111, 1126, 35, 24, 28, ''),
(1112, 1126, 35, 24, 29, ''),
(1113, 1126, 35, 24, 30, ''),
(1114, 1126, 35, 24, 31, ''),
(1115, 1126, 35, 24, 32, ''),
(1116, 1126, 35, 24, 33, ''),
(1117, 1128, 35, 25, 34, ''),
(1118, 1128, 35, 25, 35, ''),
(1119, 1128, 35, 25, 36, ''),
(1120, 1128, 35, 25, 37, ''),
(1121, 1128, 35, 25, 38, ''),
(1122, 1128, 35, 25, 39, ''),
(1123, 1128, 35, 25, 40, ''),
(1124, 1128, 35, 25, 41, ''),
(1125, 1128, 35, 25, 42, ''),
(1126, 1128, 35, 25, 43, ''),
(1127, 1128, 35, 25, 44, ''),
(1128, 1130, 35, 26, 45, ''),
(1129, 1130, 35, 26, 46, ''),
(1130, 1130, 35, 26, 47, ''),
(1131, 1130, 35, 26, 48, ''),
(1132, 1130, 35, 26, 49, ''),
(1133, 1130, 35, 26, 50, ''),
(1134, 1130, 35, 26, 51, ''),
(1135, 1130, 35, 26, 52, ''),
(1136, 1130, 35, 26, 53, ''),
(1137, 1130, 35, 26, 54, ''),
(1138, 1130, 35, 26, 55, ''),
(1139, 1140, 34, 18, 12, ''),
(1140, 1140, 34, 18, 13, ''),
(1141, 1140, 34, 18, 14, ''),
(1142, 1145, 34, 20, 15, ''),
(1143, 1145, 34, 20, 16, ''),
(1144, 1146, 34, 20, 17, ''),
(1145, 1146, 34, 20, 18, ''),
(1146, 1157, 34, 24, 23, ''),
(1147, 1157, 34, 24, 24, ''),
(1148, 1157, 34, 24, 25, ''),
(1149, 1157, 34, 24, 26, ''),
(1150, 1157, 34, 24, 27, ''),
(1151, 1157, 34, 24, 28, ''),
(1152, 1157, 34, 24, 29, ''),
(1153, 1157, 34, 24, 30, ''),
(1154, 1157, 34, 24, 31, ''),
(1155, 1157, 34, 24, 32, ''),
(1156, 1157, 34, 24, 33, ''),
(1157, 1159, 34, 25, 34, ''),
(1158, 1159, 34, 25, 35, ''),
(1159, 1159, 34, 25, 36, ''),
(1160, 1159, 34, 25, 37, ''),
(1161, 1159, 34, 25, 38, ''),
(1162, 1159, 34, 25, 39, ''),
(1163, 1159, 34, 25, 40, ''),
(1164, 1159, 34, 25, 41, ''),
(1165, 1159, 34, 25, 42, ''),
(1166, 1159, 34, 25, 43, ''),
(1167, 1159, 34, 25, 44, ''),
(1168, 1161, 34, 26, 45, ''),
(1169, 1161, 34, 26, 46, ''),
(1170, 1161, 34, 26, 47, ''),
(1171, 1161, 34, 26, 48, ''),
(1172, 1161, 34, 26, 49, ''),
(1173, 1161, 34, 26, 50, ''),
(1174, 1161, 34, 26, 51, ''),
(1175, 1161, 34, 26, 52, ''),
(1176, 1161, 34, 26, 53, ''),
(1177, 1161, 34, 26, 54, ''),
(1178, 1161, 34, 26, 55, ''),
(1179, 1171, 33, 18, 12, ''),
(1180, 1171, 33, 18, 13, ''),
(1181, 1171, 33, 18, 14, ''),
(1182, 1176, 33, 20, 15, ''),
(1183, 1176, 33, 20, 16, ''),
(1184, 1177, 33, 20, 17, ''),
(1185, 1177, 33, 20, 18, ''),
(1186, 1188, 33, 24, 23, ''),
(1187, 1188, 33, 24, 24, ''),
(1188, 1188, 33, 24, 25, ''),
(1189, 1188, 33, 24, 26, ''),
(1190, 1188, 33, 24, 27, ''),
(1191, 1188, 33, 24, 28, ''),
(1192, 1188, 33, 24, 29, ''),
(1193, 1188, 33, 24, 30, ''),
(1194, 1188, 33, 24, 31, ''),
(1195, 1188, 33, 24, 32, ''),
(1196, 1188, 33, 24, 33, ''),
(1197, 1190, 33, 25, 34, ''),
(1198, 1190, 33, 25, 35, ''),
(1199, 1190, 33, 25, 36, ''),
(1200, 1190, 33, 25, 37, ''),
(1201, 1190, 33, 25, 38, ''),
(1202, 1190, 33, 25, 39, ''),
(1203, 1190, 33, 25, 40, ''),
(1204, 1190, 33, 25, 41, ''),
(1205, 1190, 33, 25, 42, ''),
(1206, 1190, 33, 25, 43, ''),
(1207, 1190, 33, 25, 44, ''),
(1208, 1192, 33, 26, 45, ''),
(1209, 1192, 33, 26, 46, ''),
(1210, 1192, 33, 26, 47, ''),
(1211, 1192, 33, 26, 48, ''),
(1212, 1192, 33, 26, 49, ''),
(1213, 1192, 33, 26, 50, ''),
(1214, 1192, 33, 26, 51, ''),
(1215, 1192, 33, 26, 52, ''),
(1216, 1192, 33, 26, 53, ''),
(1217, 1192, 33, 26, 54, ''),
(1218, 1192, 33, 26, 55, ''),
(1273, 1391, 39, 41, 60, ''),
(1274, 1392, 39, 47, 61, ''),
(1275, 1392, 39, 47, 62, ''),
(1276, 1392, 39, 47, 63, ''),
(1277, 1393, 39, 47, 64, ''),
(1278, 1393, 39, 47, 65, ''),
(1279, 1393, 39, 47, 66, ''),
(1280, 1394, 39, 47, 67, ''),
(1281, 1394, 39, 47, 68, ''),
(1282, 1394, 39, 47, 69, ''),
(1283, 1396, 39, 48, 70, ''),
(1284, 1396, 39, 48, 71, ''),
(1285, 1425, 41, 41, 60, ''),
(1286, 1426, 41, 47, 61, ''),
(1287, 1426, 41, 47, 62, ''),
(1288, 1426, 41, 47, 63, ''),
(1289, 1427, 41, 47, 64, ''),
(1290, 1427, 41, 47, 65, ''),
(1291, 1427, 41, 47, 66, ''),
(1292, 1428, 41, 47, 67, ''),
(1293, 1428, 41, 47, 68, ''),
(1294, 1428, 41, 47, 69, ''),
(1295, 1430, 41, 48, 70, ''),
(1296, 1430, 41, 48, 71, ''),
(1297, 1459, 40, 41, 60, ''),
(1298, 1460, 40, 47, 61, ''),
(1299, 1460, 40, 47, 62, ''),
(1300, 1460, 40, 47, 63, ''),
(1301, 1461, 40, 47, 64, ''),
(1302, 1461, 40, 47, 65, ''),
(1303, 1461, 40, 47, 66, ''),
(1304, 1462, 40, 47, 67, ''),
(1305, 1462, 40, 47, 68, ''),
(1306, 1462, 40, 47, 69, ''),
(1307, 1464, 40, 48, 70, ''),
(1308, 1464, 40, 48, 71, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option_option_option`
--

CREATE TABLE `product_attr_option_option_option` (
  `id` int(11) NOT NULL,
  `pro_att_op_op_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_op_op_id` int(11) NOT NULL,
  `op_op_op_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option_option_option`
--

INSERT INTO `product_attr_option_option_option` (`id`, `pro_att_op_op_id`, `product_id`, `attribute_id`, `op_op_op_id`, `op_op_op_condition`) VALUES
(673, 819, 32, 18, 1, ''),
(674, 819, 32, 18, 2, ''),
(675, 819, 32, 18, 3, ''),
(676, 820, 32, 18, 4, ''),
(677, 820, 32, 18, 5, ''),
(678, 820, 32, 18, 6, ''),
(679, 821, 32, 18, 7, ''),
(680, 821, 32, 18, 8, ''),
(681, 821, 32, 18, 9, ''),
(682, 821, 32, 18, 10, ''),
(683, 821, 32, 18, 11, ''),
(684, 821, 32, 18, 12, ''),
(685, 821, 32, 18, 13, ''),
(686, 821, 32, 18, 14, ''),
(687, 821, 32, 18, 15, ''),
(688, 821, 32, 18, 16, ''),
(689, 821, 32, 18, 17, ''),
(690, 821, 32, 18, 18, ''),
(691, 821, 32, 18, 19, ''),
(692, 821, 32, 18, 20, ''),
(693, 821, 32, 18, 21, ''),
(694, 821, 32, 18, 22, ''),
(695, 822, 32, 20, 23, ''),
(696, 822, 32, 20, 24, ''),
(697, 822, 32, 20, 25, ''),
(698, 822, 32, 20, 26, ''),
(699, 822, 32, 20, 27, ''),
(700, 823, 32, 20, 28, ''),
(701, 823, 32, 20, 29, ''),
(702, 823, 32, 20, 30, ''),
(703, 823, 32, 20, 31, ''),
(704, 823, 32, 20, 32, ''),
(705, 824, 32, 20, 44, ''),
(706, 824, 32, 20, 45, ''),
(707, 824, 32, 20, 46, ''),
(708, 824, 32, 20, 47, ''),
(709, 824, 32, 20, 48, ''),
(710, 825, 32, 20, 49, ''),
(711, 825, 32, 20, 50, ''),
(712, 825, 32, 20, 51, ''),
(713, 825, 32, 20, 52, ''),
(714, 825, 32, 20, 53, ''),
(715, 859, 31, 18, 1, ''),
(716, 859, 31, 18, 2, ''),
(717, 859, 31, 18, 3, ''),
(718, 860, 31, 18, 4, ''),
(719, 860, 31, 18, 5, ''),
(720, 860, 31, 18, 6, ''),
(721, 861, 31, 18, 7, ''),
(722, 861, 31, 18, 8, ''),
(723, 861, 31, 18, 9, ''),
(724, 861, 31, 18, 10, ''),
(725, 861, 31, 18, 11, ''),
(726, 861, 31, 18, 12, ''),
(727, 861, 31, 18, 13, ''),
(728, 861, 31, 18, 14, ''),
(729, 861, 31, 18, 15, ''),
(730, 861, 31, 18, 16, ''),
(731, 861, 31, 18, 17, ''),
(732, 861, 31, 18, 18, ''),
(733, 861, 31, 18, 19, ''),
(734, 861, 31, 18, 20, ''),
(735, 861, 31, 18, 21, ''),
(736, 861, 31, 18, 22, ''),
(737, 862, 31, 20, 23, ''),
(738, 862, 31, 20, 24, ''),
(739, 862, 31, 20, 25, ''),
(740, 862, 31, 20, 26, ''),
(741, 862, 31, 20, 27, ''),
(742, 863, 31, 20, 28, ''),
(743, 863, 31, 20, 29, ''),
(744, 863, 31, 20, 30, ''),
(745, 863, 31, 20, 31, ''),
(746, 863, 31, 20, 32, ''),
(747, 864, 31, 20, 44, ''),
(748, 864, 31, 20, 45, ''),
(749, 864, 31, 20, 46, ''),
(750, 864, 31, 20, 47, ''),
(751, 864, 31, 20, 48, ''),
(752, 865, 31, 20, 49, ''),
(753, 865, 31, 20, 50, ''),
(754, 865, 31, 20, 51, ''),
(755, 865, 31, 20, 52, ''),
(756, 865, 31, 20, 53, ''),
(757, 899, 30, 18, 1, ''),
(758, 899, 30, 18, 2, ''),
(759, 899, 30, 18, 3, ''),
(760, 900, 30, 18, 4, ''),
(761, 900, 30, 18, 5, ''),
(762, 900, 30, 18, 6, ''),
(763, 901, 30, 18, 7, ''),
(764, 901, 30, 18, 8, ''),
(765, 901, 30, 18, 9, ''),
(766, 901, 30, 18, 10, ''),
(767, 901, 30, 18, 11, ''),
(768, 901, 30, 18, 12, ''),
(769, 901, 30, 18, 13, ''),
(770, 901, 30, 18, 14, ''),
(771, 901, 30, 18, 15, ''),
(772, 901, 30, 18, 16, ''),
(773, 901, 30, 18, 17, ''),
(774, 901, 30, 18, 18, ''),
(775, 901, 30, 18, 19, ''),
(776, 901, 30, 18, 20, ''),
(777, 901, 30, 18, 21, ''),
(778, 901, 30, 18, 22, ''),
(779, 902, 30, 20, 23, ''),
(780, 902, 30, 20, 24, ''),
(781, 902, 30, 20, 25, ''),
(782, 902, 30, 20, 26, ''),
(783, 902, 30, 20, 27, ''),
(784, 903, 30, 20, 28, ''),
(785, 903, 30, 20, 29, ''),
(786, 903, 30, 20, 30, ''),
(787, 903, 30, 20, 31, ''),
(788, 903, 30, 20, 32, ''),
(789, 904, 30, 20, 44, ''),
(790, 904, 30, 20, 45, ''),
(791, 904, 30, 20, 46, ''),
(792, 904, 30, 20, 47, ''),
(793, 904, 30, 20, 48, ''),
(794, 905, 30, 20, 49, ''),
(795, 905, 30, 20, 50, ''),
(796, 905, 30, 20, 51, ''),
(797, 905, 30, 20, 52, ''),
(798, 905, 30, 20, 53, ''),
(799, 939, 29, 18, 1, ''),
(800, 939, 29, 18, 2, ''),
(801, 939, 29, 18, 3, ''),
(802, 940, 29, 18, 4, ''),
(803, 940, 29, 18, 5, ''),
(804, 940, 29, 18, 6, ''),
(805, 941, 29, 18, 7, ''),
(806, 941, 29, 18, 8, ''),
(807, 941, 29, 18, 9, ''),
(808, 941, 29, 18, 10, ''),
(809, 941, 29, 18, 11, ''),
(810, 941, 29, 18, 12, ''),
(811, 941, 29, 18, 13, ''),
(812, 941, 29, 18, 14, ''),
(813, 941, 29, 18, 15, ''),
(814, 941, 29, 18, 16, ''),
(815, 941, 29, 18, 17, ''),
(816, 941, 29, 18, 18, ''),
(817, 941, 29, 18, 19, ''),
(818, 941, 29, 18, 20, ''),
(819, 941, 29, 18, 21, ''),
(820, 941, 29, 18, 22, ''),
(821, 942, 29, 20, 23, ''),
(822, 942, 29, 20, 24, ''),
(823, 942, 29, 20, 25, ''),
(824, 942, 29, 20, 26, ''),
(825, 942, 29, 20, 27, ''),
(826, 943, 29, 20, 28, ''),
(827, 943, 29, 20, 29, ''),
(828, 943, 29, 20, 30, ''),
(829, 943, 29, 20, 31, ''),
(830, 943, 29, 20, 32, ''),
(831, 944, 29, 20, 44, ''),
(832, 944, 29, 20, 45, ''),
(833, 944, 29, 20, 46, ''),
(834, 944, 29, 20, 47, ''),
(835, 944, 29, 20, 48, ''),
(836, 945, 29, 20, 49, ''),
(837, 945, 29, 20, 50, ''),
(838, 945, 29, 20, 51, ''),
(839, 945, 29, 20, 52, ''),
(840, 945, 29, 20, 53, ''),
(841, 979, 38, 18, 1, ''),
(842, 979, 38, 18, 2, ''),
(843, 979, 38, 18, 3, ''),
(844, 980, 38, 18, 4, ''),
(845, 980, 38, 18, 5, ''),
(846, 980, 38, 18, 6, ''),
(847, 981, 38, 18, 7, ''),
(848, 981, 38, 18, 8, ''),
(849, 981, 38, 18, 9, ''),
(850, 981, 38, 18, 10, ''),
(851, 981, 38, 18, 11, ''),
(852, 981, 38, 18, 12, ''),
(853, 981, 38, 18, 13, ''),
(854, 981, 38, 18, 14, ''),
(855, 981, 38, 18, 15, ''),
(856, 981, 38, 18, 16, ''),
(857, 981, 38, 18, 17, ''),
(858, 981, 38, 18, 18, ''),
(859, 981, 38, 18, 19, ''),
(860, 981, 38, 18, 20, ''),
(861, 981, 38, 18, 21, ''),
(862, 981, 38, 18, 22, ''),
(863, 982, 38, 20, 23, ''),
(864, 982, 38, 20, 24, ''),
(865, 982, 38, 20, 25, ''),
(866, 982, 38, 20, 26, ''),
(867, 982, 38, 20, 27, ''),
(868, 983, 38, 20, 28, ''),
(869, 983, 38, 20, 29, ''),
(870, 983, 38, 20, 30, ''),
(871, 983, 38, 20, 31, ''),
(872, 983, 38, 20, 32, ''),
(873, 984, 38, 20, 44, ''),
(874, 984, 38, 20, 45, ''),
(875, 984, 38, 20, 46, ''),
(876, 984, 38, 20, 47, ''),
(877, 984, 38, 20, 48, ''),
(878, 985, 38, 20, 49, ''),
(879, 985, 38, 20, 50, ''),
(880, 985, 38, 20, 51, ''),
(881, 985, 38, 20, 52, ''),
(882, 985, 38, 20, 53, ''),
(883, 1019, 37, 18, 1, ''),
(884, 1019, 37, 18, 2, ''),
(885, 1019, 37, 18, 3, ''),
(886, 1020, 37, 18, 4, ''),
(887, 1020, 37, 18, 5, ''),
(888, 1020, 37, 18, 6, ''),
(889, 1021, 37, 18, 7, ''),
(890, 1021, 37, 18, 8, ''),
(891, 1021, 37, 18, 9, ''),
(892, 1021, 37, 18, 10, ''),
(893, 1021, 37, 18, 11, ''),
(894, 1021, 37, 18, 12, ''),
(895, 1021, 37, 18, 13, ''),
(896, 1021, 37, 18, 14, ''),
(897, 1021, 37, 18, 15, ''),
(898, 1021, 37, 18, 16, ''),
(899, 1021, 37, 18, 17, ''),
(900, 1021, 37, 18, 18, ''),
(901, 1021, 37, 18, 19, ''),
(902, 1021, 37, 18, 20, ''),
(903, 1021, 37, 18, 21, ''),
(904, 1021, 37, 18, 22, ''),
(905, 1022, 37, 20, 23, ''),
(906, 1022, 37, 20, 24, ''),
(907, 1022, 37, 20, 25, ''),
(908, 1022, 37, 20, 26, ''),
(909, 1022, 37, 20, 27, ''),
(910, 1023, 37, 20, 28, ''),
(911, 1023, 37, 20, 29, ''),
(912, 1023, 37, 20, 30, ''),
(913, 1023, 37, 20, 31, ''),
(914, 1023, 37, 20, 32, ''),
(915, 1024, 37, 20, 44, ''),
(916, 1024, 37, 20, 45, ''),
(917, 1024, 37, 20, 46, ''),
(918, 1024, 37, 20, 47, ''),
(919, 1024, 37, 20, 48, ''),
(920, 1025, 37, 20, 49, ''),
(921, 1025, 37, 20, 50, ''),
(922, 1025, 37, 20, 51, ''),
(923, 1025, 37, 20, 52, ''),
(924, 1025, 37, 20, 53, ''),
(925, 1059, 36, 18, 1, ''),
(926, 1059, 36, 18, 2, ''),
(927, 1059, 36, 18, 3, ''),
(928, 1060, 36, 18, 4, ''),
(929, 1060, 36, 18, 5, ''),
(930, 1060, 36, 18, 6, ''),
(931, 1061, 36, 18, 7, ''),
(932, 1061, 36, 18, 8, ''),
(933, 1061, 36, 18, 9, ''),
(934, 1061, 36, 18, 10, ''),
(935, 1061, 36, 18, 11, ''),
(936, 1061, 36, 18, 12, ''),
(937, 1061, 36, 18, 13, ''),
(938, 1061, 36, 18, 14, ''),
(939, 1061, 36, 18, 15, ''),
(940, 1061, 36, 18, 16, ''),
(941, 1061, 36, 18, 17, ''),
(942, 1061, 36, 18, 18, ''),
(943, 1061, 36, 18, 19, ''),
(944, 1061, 36, 18, 20, ''),
(945, 1061, 36, 18, 21, ''),
(946, 1061, 36, 18, 22, ''),
(947, 1062, 36, 20, 23, ''),
(948, 1062, 36, 20, 24, ''),
(949, 1062, 36, 20, 25, ''),
(950, 1062, 36, 20, 26, ''),
(951, 1062, 36, 20, 27, ''),
(952, 1063, 36, 20, 28, ''),
(953, 1063, 36, 20, 29, ''),
(954, 1063, 36, 20, 30, ''),
(955, 1063, 36, 20, 31, ''),
(956, 1063, 36, 20, 32, ''),
(957, 1064, 36, 20, 44, ''),
(958, 1064, 36, 20, 45, ''),
(959, 1064, 36, 20, 46, ''),
(960, 1064, 36, 20, 47, ''),
(961, 1064, 36, 20, 48, ''),
(962, 1065, 36, 20, 49, ''),
(963, 1065, 36, 20, 50, ''),
(964, 1065, 36, 20, 51, ''),
(965, 1065, 36, 20, 52, ''),
(966, 1065, 36, 20, 53, ''),
(967, 1099, 35, 18, 1, ''),
(968, 1099, 35, 18, 2, ''),
(969, 1099, 35, 18, 3, ''),
(970, 1100, 35, 18, 4, ''),
(971, 1100, 35, 18, 5, ''),
(972, 1100, 35, 18, 6, ''),
(973, 1101, 35, 18, 7, ''),
(974, 1101, 35, 18, 8, ''),
(975, 1101, 35, 18, 9, ''),
(976, 1101, 35, 18, 10, ''),
(977, 1101, 35, 18, 11, ''),
(978, 1101, 35, 18, 12, ''),
(979, 1101, 35, 18, 13, ''),
(980, 1101, 35, 18, 14, ''),
(981, 1101, 35, 18, 15, ''),
(982, 1101, 35, 18, 16, ''),
(983, 1101, 35, 18, 17, ''),
(984, 1101, 35, 18, 18, ''),
(985, 1101, 35, 18, 19, ''),
(986, 1101, 35, 18, 20, ''),
(987, 1101, 35, 18, 21, ''),
(988, 1101, 35, 18, 22, ''),
(989, 1102, 35, 20, 23, ''),
(990, 1102, 35, 20, 24, ''),
(991, 1102, 35, 20, 25, ''),
(992, 1102, 35, 20, 26, ''),
(993, 1102, 35, 20, 27, ''),
(994, 1103, 35, 20, 28, ''),
(995, 1103, 35, 20, 29, ''),
(996, 1103, 35, 20, 30, ''),
(997, 1103, 35, 20, 31, ''),
(998, 1103, 35, 20, 32, ''),
(999, 1104, 35, 20, 44, ''),
(1000, 1104, 35, 20, 45, ''),
(1001, 1104, 35, 20, 46, ''),
(1002, 1104, 35, 20, 47, ''),
(1003, 1104, 35, 20, 48, ''),
(1004, 1105, 35, 20, 49, ''),
(1005, 1105, 35, 20, 50, ''),
(1006, 1105, 35, 20, 51, ''),
(1007, 1105, 35, 20, 52, ''),
(1008, 1105, 35, 20, 53, ''),
(1009, 1139, 34, 18, 1, ''),
(1010, 1139, 34, 18, 2, ''),
(1011, 1139, 34, 18, 3, ''),
(1012, 1140, 34, 18, 4, ''),
(1013, 1140, 34, 18, 5, ''),
(1014, 1140, 34, 18, 6, ''),
(1015, 1141, 34, 18, 7, ''),
(1016, 1141, 34, 18, 8, ''),
(1017, 1141, 34, 18, 9, ''),
(1018, 1141, 34, 18, 10, ''),
(1019, 1141, 34, 18, 11, ''),
(1020, 1141, 34, 18, 12, ''),
(1021, 1141, 34, 18, 13, ''),
(1022, 1141, 34, 18, 14, ''),
(1023, 1141, 34, 18, 15, ''),
(1024, 1141, 34, 18, 16, ''),
(1025, 1141, 34, 18, 17, ''),
(1026, 1141, 34, 18, 18, ''),
(1027, 1141, 34, 18, 19, ''),
(1028, 1141, 34, 18, 20, ''),
(1029, 1141, 34, 18, 21, ''),
(1030, 1141, 34, 18, 22, ''),
(1031, 1142, 34, 20, 23, ''),
(1032, 1142, 34, 20, 24, ''),
(1033, 1142, 34, 20, 25, ''),
(1034, 1142, 34, 20, 26, ''),
(1035, 1142, 34, 20, 27, ''),
(1036, 1143, 34, 20, 28, ''),
(1037, 1143, 34, 20, 29, ''),
(1038, 1143, 34, 20, 30, ''),
(1039, 1143, 34, 20, 31, ''),
(1040, 1143, 34, 20, 32, ''),
(1041, 1144, 34, 20, 44, ''),
(1042, 1144, 34, 20, 45, ''),
(1043, 1144, 34, 20, 46, ''),
(1044, 1144, 34, 20, 47, ''),
(1045, 1144, 34, 20, 48, ''),
(1046, 1145, 34, 20, 49, ''),
(1047, 1145, 34, 20, 50, ''),
(1048, 1145, 34, 20, 51, ''),
(1049, 1145, 34, 20, 52, ''),
(1050, 1145, 34, 20, 53, ''),
(1051, 1179, 33, 18, 1, ''),
(1052, 1179, 33, 18, 2, ''),
(1053, 1179, 33, 18, 3, ''),
(1054, 1180, 33, 18, 4, ''),
(1055, 1180, 33, 18, 5, ''),
(1056, 1180, 33, 18, 6, ''),
(1057, 1181, 33, 18, 7, ''),
(1058, 1181, 33, 18, 8, ''),
(1059, 1181, 33, 18, 9, ''),
(1060, 1181, 33, 18, 10, ''),
(1061, 1181, 33, 18, 11, ''),
(1062, 1181, 33, 18, 12, ''),
(1063, 1181, 33, 18, 13, ''),
(1064, 1181, 33, 18, 14, ''),
(1065, 1181, 33, 18, 15, ''),
(1066, 1181, 33, 18, 16, ''),
(1067, 1181, 33, 18, 17, ''),
(1068, 1181, 33, 18, 18, ''),
(1069, 1181, 33, 18, 19, ''),
(1070, 1181, 33, 18, 20, ''),
(1071, 1181, 33, 18, 21, ''),
(1072, 1181, 33, 18, 22, ''),
(1073, 1182, 33, 20, 23, ''),
(1074, 1182, 33, 20, 24, ''),
(1075, 1182, 33, 20, 25, ''),
(1076, 1182, 33, 20, 26, ''),
(1077, 1182, 33, 20, 27, ''),
(1078, 1183, 33, 20, 28, ''),
(1079, 1183, 33, 20, 29, ''),
(1080, 1183, 33, 20, 30, ''),
(1081, 1183, 33, 20, 31, ''),
(1082, 1183, 33, 20, 32, ''),
(1083, 1184, 33, 20, 44, ''),
(1084, 1184, 33, 20, 45, ''),
(1085, 1184, 33, 20, 46, ''),
(1086, 1184, 33, 20, 47, ''),
(1087, 1184, 33, 20, 48, ''),
(1088, 1185, 33, 20, 49, ''),
(1089, 1185, 33, 20, 50, ''),
(1090, 1185, 33, 20, 51, ''),
(1091, 1185, 33, 20, 52, ''),
(1092, 1185, 33, 20, 53, ''),
(1093, 1295, 41, 48, 54, ''),
(1094, 1295, 41, 48, 55, ''),
(1095, 1295, 41, 48, 56, ''),
(1096, 1295, 41, 48, 57, ''),
(1097, 1295, 41, 48, 58, ''),
(1098, 1295, 41, 48, 59, ''),
(1099, 1296, 41, 48, 60, ''),
(1100, 1296, 41, 48, 61, ''),
(1101, 1296, 41, 48, 62, ''),
(1102, 1296, 41, 48, 63, ''),
(1103, 1296, 41, 48, 64, ''),
(1104, 1296, 41, 48, 65, ''),
(1105, 1307, 40, 48, 54, ''),
(1106, 1307, 40, 48, 55, ''),
(1107, 1307, 40, 48, 56, ''),
(1108, 1307, 40, 48, 57, ''),
(1109, 1307, 40, 48, 58, ''),
(1110, 1307, 40, 48, 59, ''),
(1111, 1308, 40, 48, 60, ''),
(1112, 1308, 40, 48, 61, ''),
(1113, 1308, 40, 48, 62, ''),
(1114, 1308, 40, 48, 63, ''),
(1115, 1308, 40, 48, 64, ''),
(1116, 1308, 40, 48, 65, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_attr_option_option_option_option`
--

CREATE TABLE `product_attr_option_option_option_option` (
  `id` int(11) NOT NULL,
  `pro_att_op_op_op_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `op_op_op_op_id` int(11) NOT NULL,
  `op_op_op_op_condition` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attr_option_option_option_option`
--

INSERT INTO `product_attr_option_option_option_option` (`id`, `pro_att_op_op_op_id`, `product_id`, `attribute_id`, `op_op_op_op_id`, `op_op_op_op_condition`) VALUES
(1, 1093, 41, 48, 1, ''),
(2, 1093, 41, 48, 2, ''),
(3, 1093, 41, 48, 3, ''),
(4, 1093, 41, 48, 4, ''),
(5, 1093, 41, 48, 5, ''),
(6, 1093, 41, 48, 6, ''),
(7, 1093, 41, 48, 7, ''),
(8, 1093, 41, 48, 8, ''),
(9, 1095, 41, 48, 9, ''),
(10, 1095, 41, 48, 10, ''),
(11, 1095, 41, 48, 11, ''),
(12, 1095, 41, 48, 12, ''),
(13, 1095, 41, 48, 13, ''),
(14, 1095, 41, 48, 14, ''),
(15, 1095, 41, 48, 15, ''),
(16, 1095, 41, 48, 16, ''),
(17, 1097, 41, 48, 17, ''),
(18, 1097, 41, 48, 18, ''),
(19, 1097, 41, 48, 19, ''),
(20, 1097, 41, 48, 20, ''),
(21, 1097, 41, 48, 21, ''),
(22, 1097, 41, 48, 22, ''),
(23, 1097, 41, 48, 23, ''),
(24, 1097, 41, 48, 24, ''),
(25, 1099, 41, 48, 25, ''),
(26, 1099, 41, 48, 26, ''),
(27, 1099, 41, 48, 27, ''),
(28, 1099, 41, 48, 28, ''),
(29, 1099, 41, 48, 29, ''),
(30, 1099, 41, 48, 30, ''),
(31, 1099, 41, 48, 31, ''),
(32, 1099, 41, 48, 32, ''),
(33, 1101, 41, 48, 33, ''),
(34, 1101, 41, 48, 34, ''),
(35, 1101, 41, 48, 35, ''),
(36, 1101, 41, 48, 36, ''),
(37, 1101, 41, 48, 37, ''),
(38, 1101, 41, 48, 38, ''),
(39, 1101, 41, 48, 39, ''),
(40, 1101, 41, 48, 40, ''),
(41, 1103, 41, 48, 41, ''),
(42, 1103, 41, 48, 42, ''),
(43, 1103, 41, 48, 43, ''),
(44, 1103, 41, 48, 44, ''),
(45, 1103, 41, 48, 45, ''),
(46, 1103, 41, 48, 46, ''),
(47, 1103, 41, 48, 47, ''),
(48, 1103, 41, 48, 48, ''),
(49, 1105, 40, 48, 1, ''),
(50, 1105, 40, 48, 2, ''),
(51, 1105, 40, 48, 3, ''),
(52, 1105, 40, 48, 4, ''),
(53, 1105, 40, 48, 5, ''),
(54, 1105, 40, 48, 6, ''),
(55, 1105, 40, 48, 7, ''),
(56, 1105, 40, 48, 8, ''),
(57, 1107, 40, 48, 9, ''),
(58, 1107, 40, 48, 10, ''),
(59, 1107, 40, 48, 11, ''),
(60, 1107, 40, 48, 12, ''),
(61, 1107, 40, 48, 13, ''),
(62, 1107, 40, 48, 14, ''),
(63, 1107, 40, 48, 15, ''),
(64, 1107, 40, 48, 16, ''),
(65, 1109, 40, 48, 17, ''),
(66, 1109, 40, 48, 18, ''),
(67, 1109, 40, 48, 19, ''),
(68, 1109, 40, 48, 20, ''),
(69, 1109, 40, 48, 21, ''),
(70, 1109, 40, 48, 22, ''),
(71, 1109, 40, 48, 23, ''),
(72, 1109, 40, 48, 24, ''),
(73, 1111, 40, 48, 25, ''),
(74, 1111, 40, 48, 26, ''),
(75, 1111, 40, 48, 27, ''),
(76, 1111, 40, 48, 28, ''),
(77, 1111, 40, 48, 29, ''),
(78, 1111, 40, 48, 30, ''),
(79, 1111, 40, 48, 31, ''),
(80, 1111, 40, 48, 32, ''),
(81, 1113, 40, 48, 33, ''),
(82, 1113, 40, 48, 34, ''),
(83, 1113, 40, 48, 35, ''),
(84, 1113, 40, 48, 36, ''),
(85, 1113, 40, 48, 37, ''),
(86, 1113, 40, 48, 38, ''),
(87, 1113, 40, 48, 39, ''),
(88, 1113, 40, 48, 40, ''),
(89, 1115, 40, 48, 41, ''),
(90, 1115, 40, 48, 42, ''),
(91, 1115, 40, 48, 43, ''),
(92, 1115, 40, 48, 44, ''),
(93, 1115, 40, 48, 45, ''),
(94, 1115, 40, 48, 46, ''),
(95, 1115, 40, 48, 47, ''),
(96, 1115, 40, 48, 48, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_conditions_tbl`
--

CREATE TABLE `product_conditions_tbl` (
  `condition_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `condition_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width_condition` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '+- and 0-9 capable',
  `width_fraction_id` int(11) NOT NULL,
  `height_condition` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT '+- and 0-9 capable',
  `height_fraction_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_condition_mapping`
--

CREATE TABLE `product_condition_mapping` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `pattern_models_ids` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'multiple pattern id save use comma separator',
  `price_style_type` int(11) NOT NULL COMMENT '1= row column, 2 = sqr fit and 3 = fixed price',
  `price_rowcol_style_id` int(11) DEFAULT NULL,
  `sqft_price` float NOT NULL,
  `fixed_price` float NOT NULL,
  `condition_id` int(11) DEFAULT NULL,
  `shipping` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `init_stock` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer_price` float DEFAULT NULL,
  `individual_price` float DEFAULT NULL,
  `uom` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Unit of Measurement',
  `colors` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `active_status` int(11) NOT NULL COMMENT '1=active or 0=inactive',
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`product_id`, `product_name`, `category_id`, `subcategory_id`, `pattern_models_ids`, `price_style_type`, `price_rowcol_style_id`, `sqft_price`, `fixed_price`, `condition_id`, `shipping`, `tax`, `init_stock`, `dealer_price`, `individual_price`, `uom`, `colors`, `created_by`, `updated_by`, `created_date`, `updated_date`, `active_status`, `created_status`) VALUES
(23, '2&quot; Faux Blinds', 1, 0, '319,318,317', 1, 24, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '863,866,869,862,865,868,874,861,864,867,873', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(24, '3&quot; WiVi Blinds', 1, 0, '317', 1, 21, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '874,873', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(25, '2 1/2&quot; Shutter Blinds', 1, 0, '320', 1, 25, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '872,871,870', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(26, '2&quot; BassWood Blinds', 1, 0, '321', 1, 23, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '879,891,885,893,886,892,877,881,880,882,883,887,890', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(27, '2 1/2&quot; BassWood Blinds', 1, 0, '321', 1, 22, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '884,875,878,888,894,889,876', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(28, '3D', 2, 0, '172', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '897,900,898,896,899,895', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(29, '3D Dimout', 2, 0, '173', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '903,904,902,906,905,901', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(30, 'Ares', 2, 0, '174,175', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '909,910,911,912,907,913,908,914', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(31, 'Combi', 2, 0, '176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1273,1272,1274,1275,1276,1277,1278,1279,1244,1100,945,1191,952,986,1286,1069,1083,1186,1193,1197,1230,1304,1052,1204,1246,1025,1032,1057,1092,1097,1118,1128,1139,1212,963,1222,1239,999,1020,928,969,1283,1039,1297,1096,1102,1165,943,960,980,992,1347,922,1036,1291,1080,958,991,1005,1265,1073,924,966,989,1253,1294,1058,1063,1082,1095,1122,1140,1182,933,1207,1213,959,1227,981,1242,1247,1000,1006,1022,1103,1104,1105,1106,1107,1108,1109,1040,1190,925,1235,1075,1201,1250,1308,1184,1232,1047,1076,940,1044,1268,1179,1252,1200,1049,1129,1035,1060,1101,935,1208,955,1214,1219,1007,1309,1248,1166,975,1285,1135,1183,970,1198,1033,1088,1002,921,1125,1010,1072,1188,1234,1263,1024,1038,1059,1094,1226,1241,1017,930,929,1155,1156,1157,1158,1159,1084,1267,1281,1293,1162,964,1310,1206,1211,1086,1266,1048,1292,1078,1119,956,1223,968,988,998,1003,1284,1041,1071,1087,1348,923,1233,1026,1037,1296,1307,1056,1065,1093,1098,1134,1164,1180,1187,942,954,965,1225,979,1240,993,1249,1001,1009,1218,1217,1255,1012,1195,995,1085,961,994,967,1029,1132,1126,1074,1050,1068,1196,1229,1261,1290,1123,1176,1203,1210,1221,971,1117,1251,1231,1282,1077,1016,1346,1349,1178,1300,1114,1136,1205,1262,1064,1015,1021,1120,987,1079,946,1112,984,1303,1031,1055,1161,916,950,1018,1062,1089,1138,1237,939,938,932,931,937,941,1288,1287,927,978,1043,1046,1177,951,918,974,972,973,996,934,1130,1243,1116,1124,1181,1014,1131,1115,1051,1215,1216,1256,1258,1257,1260,1259,1305,1091,1113,917,947,997,985,1054,920,1081,957,1224,990,1004,1271,1270,1053,919,1306,976,1147,1149,1143,1146,1144,1148,1145,1142,1152,1150,1151,1153,1154,1110,1302,1301,1299,1189,1019,944,1008,1111,1013,983,1163,953,1199,1034,1099,1192,962,948,1070,1042,1090,982,1238,1167,1168,1171,1174,1169,1170,1172,1173,1254,1194,1298,1067,1185,1228,1280,1289,1137,1160,1175,1202,1209,1220,1236,1245,1030,1045,1061,1127,1133,915,936,1011,1269,1028,1295,1066,949,977,1141,1027,1121,1023,926,1264', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(32, 'Lasor/Printing', 2, 0, '234,235,236,237,238', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1313,1318,1314,1315,1321,1312,1311,1319,1322,1320,1323,1316,1324,1325,1326,1327,1317', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(33, 'Picaso', 2, 0, '239', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1332,1331,1329,1330,1328', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(34, 'Roller', 2, 0, '184,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1441,1364,1372,1386,1404,1417,1445,1370,1390,1447,1444,1336,1345,1347,1352,1361,1375,1379,1396,1401,1005,1440,1341,1344,1353,1366,1378,1384,1429,1400,1006,1438,1409,1443,1389,1369,1007,1418,1405,1406,1412,1430,1002,1010,1442,1335,1373,1380,1387,1394,1003,1337,1340,1348,1351,1360,1374,1388,1403,1428,1433,1397,1009,1334,1338,1342,1358,1362,1371,1376,1385,1402,1427,1392,1398,1355,1363,1419,1434,1346,1349,1377,1410,1399,1393,1422,1416,1437,1424,1350,1368,1382,1432,1413,1365,1421,1436,1381,1339,1411,1395,1004,1357,1356,1423,1008,1414,1435,1425,1446,1407,1333,1367,1415,1420,1426,1431,1391,1408,1439,1354,1359,1383,1343', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(35, 'Roller Screen', 2, 0, '259,260,261', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1456,1465,1474,1455,1464,1473,1451,1460,1469,1450,1459,1468,1453,1462,1471,1454,1463,1472,1448,1457,1466,1452,1461,1470,1449,1458,1467', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(36, 'Triple', 2, 0, '262,263,264,265,266,267,268', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1482,1500,1513,1487,1496,1501,1509,1515,1495,1516,1486,1489,1481,1491,1511,1498,1499,1508,1490,1483,1510,1507,1514,1497,1512,1476,1494,1492,1493,1477,1502,1479,1484,1478,1485,1505,1503,1506,1504,1475,1480,1488', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(37, 'Vision Shades', 2, 0, '269', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1520,1519,1521,1522,1518,1517', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(38, 'Roller - LL', 2, 0, '270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316', 4, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1576,1555,1610,1573,1655,1644,1649,1687,1589,1603,1643,1670,1697,1561,1673,1620,1590,1696,1685,1523,1594,1658,1584,1675,1572,1536,1568,1525,1579,1543,1654,1552,1633,1636,1681,1668,1531,1582,1669,1688,1548,1560,1665,1528,1540,1619,1569,1567,1615,1652,1549,1532,1653,1558,1682,1632,1637,1629,1635,1684,1564,1641,1667,1530,1642,1686,1593,1630,1639,1647,1692,1656,1621,1533,1607,1547,1566,1671,1674,1693,1541,1672,1646,1563,1575,1616,1660,1571,1596,1557,1691,1580,1544,1604,1598,1591,1611,1645,1627,1601,1659,1539,1625,1679,1631,1640,1689,1570,1678,1698,1694,1614,1657,1618,1581,1537,1542,1574,1622,1623,1529,1676,1551,1605,1538,1683,1699,1583,1526,1648,1661,1663,1588,1606,1554,1677,1546,1651,1608,1624,1628,1595,1592,1562,1545,1626,1556,1585,1613,1666,1524,1600,1700,1587,1690,1617,1664,1553,1634,1638,1550,1695,1612,1578,1559,1565,1577,1586,1602,1609,1662,1680,1527,1534,1535,1597,1650,1599', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(39, 'Polycore Shutters', 3, 0, '322', 2, 0, 17, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1701', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(40, 'China Basswood Shutters', 3, 0, '322', 2, 0, 18.5, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1701', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(41, 'Domestic Basswood Shutters', 3, 0, '322', 2, 0, 20, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1701', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(42, 'Sunburst Stock Arches-Operable', 4, 0, '323', 1, 45, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1702', 2, 0, '2019-09-17', '0000-00-00', 1, 0),
(43, '3D', 7, 0, '324', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1705,1708,1706,1704,1707,1703', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(44, '3D Dimout', 7, 0, '325', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1711,1712,1710,1714,1713,1709', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(45, 'Ares', 7, 0, '326,327', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '1717,1718,1719,1720,1715,1721,1716,1722', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(46, 'Combi', 7, 0, '328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2081,2080,2082,2083,2084,2085,2086,2087,2052,1908,1753,1999,1760,1794,2094,1877,1891,1994,2001,2005,2038,2112,2054,1860,2012,1807,1828,1833,1840,1865,1900,1905,1926,1936,1947,2020,1771,2030,2047,1736,1777,1800,2091,1847,2105,1904,1910,1973,1751,1768,1788,2155,1730,1844,1799,1813,2073,2099,1888,1766,1881,1732,1797,2061,1774,2050,2055,1808,1814,1830,2102,1866,1871,1890,1903,1930,1948,1990,1741,2015,2021,1767,2035,1789,1911,1912,1913,1914,1915,1916,1917,1848,1998,1733,2043,1883,2009,2058,2116,1992,2040,1855,1884,1748,1852,2076,1987,2060,2008,1857,1937,1815,1843,1868,1909,1743,2016,1763,2022,2027,2117,2056,1974,1783,2093,1943,1991,1778,2006,1841,1896,1810,1729,1818,1933,2071,1880,1996,2042,2049,1825,1832,1846,1867,1902,2034,1738,1737,1963,1964,1965,1966,1967,2075,1892,2089,2101,1970,1772,2118,2014,2019,1894,2074,1856,2100,1796,1806,1811,1886,1927,1764,2031,1776,2092,1849,1879,1895,2156,1731,2041,2048,1801,2057,1809,1817,1834,1845,2104,2115,1864,1873,1901,1906,1942,1972,1988,1995,1750,1762,1773,2033,1787,2026,2025,2063,1820,2003,1803,1893,1802,1769,1775,1837,1940,1934,1882,2069,1858,1876,2004,2037,2098,1931,1984,2011,2018,2029,1779,2059,1925,2039,2090,1885,1824,2154,2157,1986,2108,1922,1944,2013,2070,1823,1829,1872,1928,1795,1887,1754,1792,1920,2111,1826,1839,1863,1969,1724,1758,1870,1897,1946,2045,1747,1746,1740,1739,1745,1749,2096,2095,1735,1786,1851,1854,1985,1759,1726,1782,1780,1781,1804,1742,1938,2051,1924,1932,1822,1989,1939,1923,1859,2023,2024,2064,2066,2065,2068,2067,2113,1805,1899,1921,1725,1755,1793,1862,1728,1798,1812,1889,1765,2032,2079,2078,1861,1727,2114,1784,1955,1957,1951,1954,1952,1956,1953,1950,1960,1958,1959,1961,1962,1918,2110,2109,2107,1997,1827,1816,1752,1919,1821,1791,1971,1761,2007,1842,1907,2000,1770,1756,1878,1850,1898,1790,2046,1975,1976,1979,1982,1977,1978,1980,1981,2062,2002,2106,1875,1993,2036,2053,2088,2097,1945,1968,1983,2010,2017,2028,2044,1819,1838,1853,1869,1935,1941,1723,1744,2077,1836,2103,1874,1757,1785,1949,1831,1835,1929,1734,2072', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(47, 'Lasor/Printing', 7, 0, '386,387,388,389,390', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2121,2126,2122,2123,2129,2120,2119,2127,2130,2128,2131,2124,2132,2133,2134,2135,2125', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(48, 'Picaso', 7, 0, '391', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2140,2139,2137,2138,2136', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(49, 'Roller', 7, 0, '336,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2249,2172,2180,2194,2212,2225,2253,2178,2198,2255,2252,2144,2153,2155,2160,2169,2183,2187,2204,2209,1813,2248,2149,2152,2161,2174,2186,2192,2237,2208,1814,2246,2217,2251,2197,2177,1815,2226,2213,2214,2220,2238,1810,1818,2250,2143,2181,2188,2195,2202,1811,2145,2148,2156,2159,2168,2182,2196,2211,2236,2241,2205,1817,2142,2146,2150,2166,2170,2179,2184,2193,2210,2235,2200,2206,2163,2171,2227,2242,2154,2157,2185,2218,2207,2201,2230,2224,2245,2232,2158,2176,2190,2240,2221,2173,2229,2244,2189,2147,2219,2203,1812,2165,2164,2231,1816,2222,2243,2233,2254,2215,2141,2175,2223,2228,2234,2239,2199,2216,2247,2162,2167,2191,2151', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(50, 'Roller Screen', 7, 0, '411,412,413', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2264,2273,2282,2263,2272,2281,2259,2268,2277,2258,2267,2276,2261,2270,2279,2262,2271,2280,2256,2265,2274,2260,2269,2278,2257,2266,2275', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(51, 'Triple', 7, 0, '414,415,416,417,418,419,420', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2308,2321,2290,2295,2304,2309,2317,2323,2303,2324,2294,2297,2289,2299,2319,2306,2307,2316,2298,2291,2318,2315,2322,2305,2320,2284,2302,2300,2301,2285,2310,2287,2292,2286,2293,2313,2311,2314,2312,2283,2288,2296', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(52, 'Vision Shades', 7, 0, '421', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2328,2327,2329,2330,2326,2325', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(53, 'Roller - LL', 7, 0, '422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468', 5, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', '2384,2363,2418,2381,2463,2452,2457,2495,2397,2411,2451,2478,2505,2369,2481,2428,2398,2504,2493,2331,2402,2466,2392,2483,2380,2333,2344,2376,2387,2351,2462,2360,2441,2444,2489,2339,2476,2390,2477,2496,2336,2356,2368,2473,2348,2427,2377,2375,2423,2460,2357,2340,2461,2366,2490,2440,2445,2437,2443,2492,2372,2449,2338,2475,2450,2494,2401,2438,2447,2455,2500,2464,2429,2341,2415,2355,2374,2479,2482,2501,2349,2480,2454,2371,2383,2424,2468,2379,2404,2365,2499,2388,2352,2412,2406,2399,2419,2453,2435,2409,2467,2347,2433,2487,2439,2448,2497,2378,2486,2506,2502,2422,2465,2426,2389,2345,2350,2382,2430,2431,2337,2484,2359,2413,2346,2491,2507,2334,2391,2456,2469,2471,2396,2414,2362,2485,2354,2459,2416,2432,2436,2403,2400,2370,2353,2434,2364,2332,2393,2421,2474,2408,2508,2395,2498,2425,2472,2361,2442,2446,2358,2503,2420,2386,2335,2342,2343,2367,2373,2385,2394,2410,2417,2470,2488,2405,2458,2407', 4, 0, '2019-09-18', '2019-09-17', 1, 0),
(54, 'nokia x2', 8, 0, '469', 1, 0, 0, 0, NULL, NULL, NULL, NULL, 20, NULL, '', '2543', 8, 0, '2019-09-19', '0000-00-00', 1, 0),
(55, 'nokia x2', 9, 0, '470', 1, 26, 0, 20, NULL, NULL, NULL, NULL, 13, NULL, '', '2544', 12, 0, '2019-09-20', '0000-00-00', 1, 1),
(56, 'lenovo k6 Power', 1, 0, '320', 2, 41, 0, 0, NULL, NULL, NULL, NULL, 20, NULL, '', '872,871,870', 2, 0, '2019-09-20', '0000-00-00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details_tbl`
--

CREATE TABLE `purchase_details_tbl` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `purchase_price` float NOT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `quantity` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `discount` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_tbl`
--

CREATE TABLE `purchase_tbl` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `invoice_discount` float DEFAULT NULL,
  `total_discount` float DEFAULT NULL,
  `grand_total` float NOT NULL,
  `paid_amount` float DEFAULT NULL,
  `due_amount` float DEFAULT NULL,
  `payment_type` tinyint(1) NOT NULL COMMENT '1=cash ,2=check',
  `check_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_branch_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_description` text COLLATE utf8_unicode_ci,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quatation_attributes`
--

CREATE TABLE `quatation_attributes` (
  `row_id` int(11) NOT NULL,
  `fk_od_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attribute` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quatation_attributes`
--

INSERT INTO `quatation_attributes` (`row_id`, `fk_od_id`, `order_id`, `product_id`, `product_attribute`) VALUES
(1, 1, 'itit-capt-Ingo-001', 55, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `quatation_tbl`
--

CREATE TABLE `quatation_tbl` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `side_mark` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_different_shipping` text COLLATE utf8_unicode_ci,
  `different_shipping_address` text COLLATE utf8_unicode_ci,
  `state_tax` float DEFAULT NULL,
  `shipping_charges` float NOT NULL,
  `installation_charge` float DEFAULT NULL,
  `other_charge` float DEFAULT NULL,
  `invoice_discount` float DEFAULT NULL,
  `misc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grand_total` float DEFAULT NULL,
  `subtotal` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due` float NOT NULL,
  `commission_amt` float DEFAULT '0',
  `order_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_stage` int(11) NOT NULL DEFAULT '1' COMMENT '1=Quote,2=Paid,3=Partially Paid,4=Manufacturing,5=Shipping,6=Cancelled, 7= Delivered',
  `cancel_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `order_date` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `synk_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quatation_tbl`
--

INSERT INTO `quatation_tbl` (`id`, `order_id`, `customer_id`, `side_mark`, `upload_file`, `barcode`, `is_different_shipping`, `different_shipping_address`, `state_tax`, `shipping_charges`, `installation_charge`, `other_charge`, `invoice_discount`, `misc`, `grand_total`, `subtotal`, `paid_amount`, `due`, `commission_amt`, `order_status`, `order_stage`, `cancel_comment`, `level_id`, `order_date`, `created_by`, `updated_by`, `synk_status`, `created_date`, `updated_date`) VALUES
(1, 'itit-capt-Ingo-001', 3, 'caption-Ingolstadt', '', 'assets/barcode/c/itit-capt-Ingo-001.jpg', '0', '', 0, 0, 0, 0, 0, '0', 121, 121, 0, 121, 0, '', 4, NULL, 12, '2019-09-20', 12, 0, 0, '2019-09-20', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_details`
--

CREATE TABLE `quote_comparison_details` (
  `row_id` bigint(20) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `room_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `pattern_id` int(11) DEFAULT NULL,
  `quote_price` float NOT NULL,
  `subtotal` float DEFAULT NULL,
  `product_price` float NOT NULL,
  `upcharge` float DEFAULT NULL,
  `discount` float NOT NULL,
  `product_data` text COLLATE utf8_unicode_ci NOT NULL,
  `upcharge_data` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_rooms`
--

CREATE TABLE `quote_comparison_rooms` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `window_qty` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quote_comparison_tbl`
--

CREATE TABLE `quote_comparison_tbl` (
  `quote_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `grand_total` float NOT NULL,
  `sub_total` float NOT NULL,
  `sales_tax` float NOT NULL,
  `created_date` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qutation_details`
--

CREATE TABLE `qutation_details` (
  `row_id` int(11) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `room` varchar(250) DEFAULT NULL,
  `product_id` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `list_price` float NOT NULL,
  `discount` float DEFAULT NULL,
  `unit_total_price` float NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `height_fraction_id` int(11) DEFAULT NULL,
  `width_fraction_id` int(11) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `qutation_details`
--

INSERT INTO `qutation_details` (`row_id`, `order_id`, `room`, `product_id`, `category_id`, `product_qty`, `list_price`, `discount`, `unit_total_price`, `pattern_model_id`, `color_id`, `width`, `height`, `height_fraction_id`, `width_fraction_id`, `notes`, `created_status`) VALUES
(1, 'itit-capt-Ingo-001', 'Family', '55', 9, 1, 121, 0, 121, 470, 2544, 2, 4, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_color_mapping_tbl`
--

CREATE TABLE `raw_material_color_mapping_tbl` (
  `id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_return_details_tbl`
--

CREATE TABLE `raw_material_return_details_tbl` (
  `return_id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `return_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_return_tbl`
--

CREATE TABLE `raw_material_return_tbl` (
  `return_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `return_date` date NOT NULL,
  `return_comments` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `returned_by` int(11) NOT NULL,
  `is_approved` tinyint(1) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_permission_tbl`
--

CREATE TABLE `role_permission_tbl` (
  `id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `can_access` tinyint(1) NOT NULL,
  `can_create` tinyint(1) NOT NULL,
  `can_edit` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_name`) VALUES
(1, 'Study'),
(2, 'Dining'),
(3, 'Living'),
(4, 'Guest'),
(5, 'Guest Bath'),
(6, 'Master Bedroom'),
(7, 'Master Bathroom'),
(8, 'Master Closet'),
(9, 'Exercise Room'),
(10, 'Family'),
(11, 'Nook'),
(12, 'Patio Door'),
(13, 'Kitchen'),
(14, 'Utility'),
(15, 'Mud Room'),
(16, 'Garage'),
(17, 'Sunroom'),
(18, 'Hallway-1'),
(19, 'Hallway-2'),
(20, 'Bedroom-1'),
(21, 'Bedroom-2'),
(22, 'Bedroom-3'),
(23, 'Main Door'),
(24, 'Stairs'),
(25, 'Upstairs Room-1'),
(26, 'Upstairs Room-2'),
(27, 'Upstairs Room-3'),
(28, 'Upstairs Room-4'),
(29, 'Upstairs Closet-1'),
(30, 'Upstairs Closet-2'),
(31, 'Game'),
(32, 'Card Room'),
(33, 'Bar'),
(34, 'Landing'),
(35, 'Upstairs Bath-1'),
(36, 'Upstairs Bath-2'),
(37, 'Upstairs Bath-3'),
(38, 'Upstairs Hall-1'),
(39, 'Upstairs Hall-2'),
(40, 'Media'),
(41, 'Other Room');

-- --------------------------------------------------------

--
-- Table structure for table `row_column`
--

CREATE TABLE `row_column` (
  `style_id` int(11) NOT NULL,
  `style_name` text NOT NULL,
  `style_type` int(2) NOT NULL COMMENT '1 = row-column price, 4 = group price',
  `create_data_time` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `active_status` int(11) NOT NULL DEFAULT '1',
  `created_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:add by b user ,1 : add by customer'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `row_column`
--

INSERT INTO `row_column` (`style_id`, `style_name`, `style_type`, `create_data_time`, `create_by`, `active_status`, `created_status`) VALUES
(21, '3\" Wi-Vi Blinds', 1, '2019-09-17 18:27:30', 2, 1, 0),
(22, '2 1/2\" BassWood Blinds', 1, '2019-09-17 18:30:58', 2, 1, 0),
(23, '2\" BassWood Blinds', 1, '2019-09-17 18:32:16', 2, 1, 0),
(24, '2\" Wood a Like Blinds', 1, '2019-09-17 18:33:01', 2, 1, 0),
(25, '2 1/2\" Shutter Blinds', 1, '2019-09-17 18:34:04', 2, 1, 0),
(26, 'Group 0', 4, '2019-09-17 18:38:00', 2, 1, 0),
(27, 'Group 1', 4, '2019-09-17 18:38:41', 2, 1, 0),
(28, 'Group 2', 4, '2019-09-17 18:39:03', 2, 1, 0),
(29, 'Group 3', 4, '2019-09-17 18:39:28', 2, 1, 0),
(30, 'Group 4', 4, '2019-09-17 18:39:49', 2, 1, 0),
(31, 'Group 5', 4, '2019-09-17 18:40:14', 2, 1, 0),
(32, 'Group 6', 4, '2019-09-17 18:40:34', 2, 1, 0),
(33, 'Group 7', 4, '2019-09-17 18:41:01', 2, 1, 0),
(34, 'Group 8', 4, '2019-09-17 18:41:24', 2, 1, 0),
(35, 'Group 9', 4, '2019-09-17 18:41:48', 2, 1, 0),
(36, 'Group 10-Laser Roll', 4, '2019-09-17 18:42:17', 2, 1, 0),
(37, 'Group 11 - Triple Image printing', 4, '2019-09-17 18:42:52', 2, 1, 0),
(38, 'Group 12 - Roller/Combi Image printing ', 4, '2019-09-17 18:43:17', 2, 1, 0),
(39, 'Group 13 - Blackout Roller Image printing ', 4, '2019-09-17 18:43:41', 2, 1, 0),
(40, 'GROUP 14 - Roller Screen: Screen 3000', 4, '2019-09-17 18:45:27', 2, 1, 0),
(41, 'NON OPERABLE EYEBROWS', 1, '2019-09-17 21:28:17', 2, 1, 0),
(42, 'NON OPERABLE EYEBROWS with SCREEN', 1, '2019-09-17 21:28:55', 2, 1, 0),
(43, 'SOLID EYEBROWS', 1, '2019-09-17 21:29:22', 2, 1, 0),
(44, 'FACIAS', 1, '2019-09-17 21:30:07', 2, 1, 0),
(45, 'OPERABLE ARCHES', 1, '2019-09-18 09:55:08', 2, 1, 0),
(46, 'OPERABLE EYEBROWS', 1, '2019-09-17 21:30:57', 2, 1, 0),
(47, ' LOUVERED ARCH DIMENSION AND COVERAGE', 1, '2019-09-17 22:03:56', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `row_material_stock_tbl`
--

CREATE TABLE `row_material_stock_tbl` (
  `id` bigint(20) NOT NULL,
  `row_material_id` int(11) NOT NULL,
  `row_id` varchar(50) NOT NULL,
  `pattern_model_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `in_qty` int(11) NOT NULL,
  `out_qty` int(11) NOT NULL,
  `from_module` varchar(200) NOT NULL,
  `measurment` float NOT NULL,
  `stock_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `row_material_tbl`
--

CREATE TABLE `row_material_tbl` (
  `id` int(11) NOT NULL,
  `material_name` text NOT NULL,
  `pattern_model_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `uom` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  `measurement_type` int(11) DEFAULT NULL COMMENT '1- Quantity, 0-Unit'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shipment_data`
--

CREATE TABLE `shipment_data` (
  `id` int(11) NOT NULL,
  `order_id` varchar(200) NOT NULL,
  `customer_name` varchar(250) DEFAULT NULL,
  `customer_phone` varchar(25) DEFAULT NULL,
  `track_number` varchar(200) NOT NULL,
  `shipping_charges` float NOT NULL,
  `graphic_image` varchar(200) NOT NULL,
  `html_image` varchar(200) NOT NULL,
  `delivery_date` date NOT NULL,
  `shipping_addres` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `method_id` int(11) NOT NULL,
  `service_type` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_method`
--

CREATE TABLE `shipping_method` (
  `id` int(11) NOT NULL,
  `method_name` varchar(100) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `access_token` varchar(200) NOT NULL,
  `account_id` varchar(100) NOT NULL,
  `service_type` varchar(20) DEFAULT NULL,
  `pickup_method` varchar(20) DEFAULT NULL,
  `mode` varchar(20) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_method`
--

INSERT INTO `shipping_method` (`id`, `method_name`, `username`, `password`, `access_token`, `account_id`, `service_type`, `pickup_method`, `mode`, `created_by`, `updated_by`, `status`) VALUES
(4, 'Delivery in Person', 'Null', 'Null', 'Null', 'Null', NULL, NULL, NULL, 2, 2, 1),
(5, 'Customer Pickup', 'Null', 'Null', 'Null', 'Null', NULL, NULL, NULL, 2, 2, 1),
(6, 'UPS', 'BMSLink', 'Bmswd123#', '6D6BE5BA3A18A6D5', '502A5V', NULL, NULL, NULL, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway`
--

CREATE TABLE `sms_gateway` (
  `gateway_id` int(11) NOT NULL,
  `provider_name` text NOT NULL,
  `user` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `authentication` text NOT NULL,
  `link` text NOT NULL,
  `default_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `is_verify` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 : No | 1 : Yes ',
  `test_sms_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_gateway`
--

INSERT INTO `sms_gateway` (`gateway_id`, `provider_name`, `user`, `password`, `phone`, `authentication`, `link`, `default_status`, `status`, `created_by`, `is_verify`, `test_sms_number`) VALUES
(7, 'Twilio', 'AC742f8864f265947d93b163a91ff098d5', 'b21a37c51834e01c2f30a35b8b35e87a', '+14694051516', 'BMS Window Decor', '', 1, 1, 2, 1, '+12148370214');

-- --------------------------------------------------------

--
-- Table structure for table `sqm_price_model_mapping_tbl`
--

CREATE TABLE `sqm_price_model_mapping_tbl` (
  `id` int(11) NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `product_id` int(11) NOT NULL,
  `pattern_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sqm_price_model_mapping_tbl`
--

INSERT INTO `sqm_price_model_mapping_tbl` (`id`, `price`, `product_id`, `pattern_id`) VALUES
(11, 32000.00, 43, '324'),
(12, 35000.00, 44, '325'),
(13, 25000.00, 45, '326'),
(14, 25000.00, 45, '327'),
(15, 9500.00, 46, '328'),
(16, 13500.00, 46, '329'),
(17, 15000.00, 46, '330'),
(18, 15000.00, 46, '331'),
(19, 15400.00, 46, '332'),
(20, 16000.00, 46, '333'),
(21, 16000.00, 46, '334'),
(22, 16400.00, 46, '335'),
(23, 16500.00, 46, '336'),
(24, 17000.00, 46, '337'),
(25, 17500.00, 46, '338'),
(26, 17600.00, 46, '339'),
(27, 18000.00, 46, '340'),
(28, 18000.00, 46, '341'),
(29, 19000.00, 46, '342'),
(30, 19400.00, 46, '343'),
(31, 19400.00, 46, '344'),
(32, 19400.00, 46, '345'),
(33, 19400.00, 46, '346'),
(34, 19500.00, 46, '347'),
(35, 19800.00, 46, '348'),
(36, 20000.00, 46, '349'),
(37, 20000.00, 46, '350'),
(38, 20000.00, 46, '351'),
(39, 20000.00, 46, '352'),
(40, 20900.00, 46, '353'),
(41, 21000.00, 46, '354'),
(42, 21000.00, 46, '355'),
(43, 21000.00, 46, '356'),
(44, 21200.00, 46, '357'),
(45, 21200.00, 46, '358'),
(46, 21400.00, 46, '359'),
(47, 21500.00, 46, '360'),
(48, 22000.00, 46, '361'),
(49, 22000.00, 46, '362'),
(50, 22000.00, 46, '363'),
(51, 22000.00, 46, '364'),
(52, 22900.00, 46, '365'),
(53, 23000.00, 46, '366'),
(54, 23000.00, 46, '367'),
(55, 23000.00, 46, '368'),
(56, 23000.00, 46, '369'),
(57, 23000.00, 46, '370'),
(58, 23500.00, 46, '371'),
(59, 24000.00, 46, '372'),
(60, 24000.00, 46, '373'),
(61, 24000.00, 46, '374'),
(62, 24000.00, 46, '375'),
(63, 25000.00, 46, '376'),
(64, 25000.00, 46, '377'),
(65, 25000.00, 46, '378'),
(66, 25000.00, 46, '379'),
(67, 27000.00, 46, '381'),
(68, 27800.00, 46, '382'),
(69, 32000.00, 46, '383'),
(70, 36000.00, 46, '384'),
(71, 42500.00, 46, '385'),
(72, 20500.00, 47, '386'),
(73, 31000.00, 47, '387'),
(74, 16500.00, 47, '388'),
(75, 18700.00, 47, '389'),
(76, 29500.00, 48, '391'),
(77, 10000.00, 49, '392'),
(78, 11000.00, 49, '393'),
(79, 11000.00, 49, '394'),
(80, 12000.00, 49, '336'),
(81, 13900.00, 49, '395'),
(82, 13900.00, 49, '396'),
(83, 13900.00, 49, '397'),
(84, 13900.00, 49, '398'),
(85, 14000.00, 49, '399'),
(86, 15000.00, 49, '400'),
(87, 15000.00, 49, '401'),
(88, 15000.00, 49, '402'),
(89, 16000.00, 49, '403'),
(90, 18300.00, 49, '404'),
(91, 18300.00, 49, '405'),
(92, 20900.00, 49, '406'),
(93, 25500.00, 49, '408'),
(94, 25500.00, 49, '409'),
(95, 27500.00, 49, '410'),
(96, 26400.00, 51, '414'),
(97, 26400.00, 51, '415'),
(98, 26400.00, 51, '416'),
(99, 26800.00, 51, '417'),
(100, 28000.00, 52, '421'),
(101, 35000.00, 51, '418'),
(102, 35000.00, 51, '419'),
(103, 37600.00, 51, '420');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_raw_material_mapping`
--

CREATE TABLE `supplier_raw_material_mapping` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_raw_material_mapping`
--

INSERT INTO `supplier_raw_material_mapping` (`id`, `supplier_id`, `raw_material_id`) VALUES
(1, 0, 0),
(3, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_tbl`
--

CREATE TABLE `supplier_tbl` (
  `supplier_id` int(11) NOT NULL,
  `supplier_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_sku` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `material_product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previous_balance` float DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price_item` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qb_vendor_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `supplier_tbl`
--

INSERT INTO `supplier_tbl` (`supplier_id`, `supplier_no`, `supplier_sku`, `supplier_name`, `company_name`, `email`, `phone`, `material_product`, `previous_balance`, `address`, `city`, `state`, `zip`, `country_code`, `price_item`, `qb_vendor_id`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(2, 'SUP-0003-pranav', '456', 'pranav', 'pra', 'pranav@mailinator.com', '+1 (123) 456-7891', NULL, 52, 'Surat', 'Surat', 'GJ', '395017', 'IN', NULL, NULL, 2, 0, '2019-09-21', '0000-00-00'),
(3, '-alit', 'alit', 'alit', 'alit', 'a@mailinator', '123456789', NULL, 800, 'India', 'Wadgaon', 'MH', '442305', 'IN', NULL, NULL, 12, 12, '2019-09-21', '2019-09-21');

-- --------------------------------------------------------

--
-- Table structure for table `tag_tbl`
--

CREATE TABLE `tag_tbl` (
  `id` int(11) NOT NULL,
  `tag_name` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_payment_setting`
--

CREATE TABLE `tms_payment_setting` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mode` tinyint(4) NOT NULL DEFAULT '1',
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tms_payment_setting`
--

INSERT INTO `tms_payment_setting` (`id`, `url`, `user_name`, `password`, `mode`, `level_id`) VALUES
(2, 'https://secure.tmspaymentgateway.com/api/transact.php', 'bms6750', 'Bmsd123#', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `unit_of_conversion`
--

CREATE TABLE `unit_of_conversion` (
  `id` int(15) NOT NULL,
  `unit_id` varchar(25) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `unit_value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit_of_measurement`
--

CREATE TABLE `unit_of_measurement` (
  `uom_id` int(11) NOT NULL,
  `uom_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `unit_types` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_access_tbl`
--

CREATE TABLE `user_access_tbl` (
  `role_acc_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `created_by` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fixed_commission` float DEFAULT '0',
  `percentage_commission` float DEFAULT '0',
  `create_date` date NOT NULL,
  `update_date` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `user_type` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'B or C',
  `user_type_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:hana b,0:other b',
  `package_id` tinyint(4) NOT NULL DEFAULT '1',
  `sub_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `created_by`, `first_name`, `last_name`, `user_image`, `company`, `address`, `city`, `state`, `zip_code`, `country_code`, `phone`, `email`, `reference`, `language`, `fixed_commission`, `percentage_commission`, `create_date`, `update_date`, `updated_by`, `user_type`, `user_type_status`, `package_id`, `sub_domain`) VALUES
(1, '1', 'Super', 'Admin', NULL, 'Super', '', '', '', '', '', '', 'admin@bmslink.net', '', NULL, 0, 0, '2019-09-07', '2019-09-07', 0, 's', 0, 1, NULL),
(2, '', 'James', 'Park', NULL, 'BMS Window Decor', '1000 Crowley Drive, Carrollton, TX, USA', 'Carrollton', 'TX', '75063', 'US', '(972) 820-0007', 'james@bmsdecor.com', '', 'English', 0, 0, '2019-09-17', '0000-00-00', 0, 'b', 1, 1, NULL),
(3, '2', 'James', 'Park', NULL, 'Legacy Blinds', '1000 Crowley Drive', 'Carrollton', 'TX', '75006', 'US', '+1 (972) 820-0007', 'info@legacyblinds.com', '', 'English', 0, 0, '2019-09-17', '0000-00-00', 0, 'c', 0, 3, NULL),
(4, '', 'Ahn', 'Ki Sook', NULL, 'HANA', '152-4 Deck-ri, Bongdam-eup, Hwaseong-si, Gyeonggi-do, South Korea', 'Hwaseong-si', 'KR', '445-894', 'KR', '(031) 293-6656', 'shesdeco@gmail.com', '', 'English', 0, 0, '2019-09-17', '0000-00-00', 0, 'b', 1, 1, NULL),
(5, '', 'jake', 'wright', NULL, 'ti', 'India', 'Wadgaon', 'MH', '442305', 'India', '1234567891', 'jake@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 1, 1, 'jake'),
(6, '', 'tony', 'stark', NULL, 'it', 'India', 'Wadgaon', 'MH', '442305', 'IN', '1234567891', 'tony@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 0, 1, 'toni'),
(7, '', 'caption', 'america', NULL, 'ti', 'India', 'Wadgaon', 'MH', '442305', 'India', '9977456123', 'caption@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 1, 1, 'captin'),
(8, '', 'hulk', 'doctor', NULL, 'it', 'Indianapolis, IN, USA', 'Indianapolis', 'IN', '46204', 'US', '1234568791', 'hulk@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 1, 1, 'hulk'),
(9, '', 'silvester', 'stalan', NULL, 'it', 'Inverness, UK', 'Wadgaon', 'MH', '442305', 'India', '123456', 'silvester@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 0, 1, 'silvester'),
(10, '', 'test-b-1', 'demo', NULL, 'it', 'India', 'Wadgaon', 'MH', '442305', 'IN', '1234567891', 'test_b_1@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 1, 1, 'test-b-1'),
(11, '', 'test-b-2', 'demo', NULL, 'it', 'India', 'Wadgaon', 'MH', '442305', 'India', '1234567891', 'test_b_2@mailinator.com', '', 'English', 0, 0, '2019-09-19', '0000-00-00', 0, 'b', 1, 1, 'test-b-2'),
(12, '2', 'cust', 'test', NULL, 'it', 'India', 'Wadgaon', 'MH', '', 'IN', '123456789', 'cust_test@mailinator.com', '', 'English', 0, 0, '2019-09-20', '0000-00-00', 0, 'c', 0, 3, NULL),
(13, '', 'jake', 'michele', NULL, 'it', 'India Gate, New Delhi, Delhi, India', 'New Delhi', 'IN', '110001', 'IN', '1234567891', 'jake_michele@mailinator.com', '', 'English', 0, 0, '2019-09-21', '0000-00-00', 0, 'b', 1, 1, 'ja-ke');

-- --------------------------------------------------------

--
-- Table structure for table `us_state_tbl`
--

CREATE TABLE `us_state_tbl` (
  `state_id` int(11) NOT NULL,
  `shortcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_rate` float NOT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `width_height_fractions`
--

CREATE TABLE `width_height_fractions` (
  `id` int(11) NOT NULL,
  `fraction_value` varchar(20) NOT NULL,
  `decimal_value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `width_height_fractions`
--

INSERT INTO `width_height_fractions` (`id`, `fraction_value`, `decimal_value`) VALUES
(1, '1/16', 0.0625),
(2, '1/8', 0.125),
(3, '1/4', 0.25),
(4, '3/8', 0.375),
(5, '1/2', 0.5),
(6, '5/8', 0.625),
(7, '3/4', 0.75),
(8, '7/8', 0.875);

-- --------------------------------------------------------

--
-- Table structure for table `ws_payments`
--

CREATE TABLE `ws_payments` (
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesslog`
--
ALTER TABLE `accesslog`
  ADD UNIQUE KEY `SerialNo` (`sl_no`);

--
-- Indexes for table `acc_coa`
--
ALTER TABLE `acc_coa`
  ADD PRIMARY KEY (`row_id`),
  ADD UNIQUE KEY `HeadCode` (`HeadCode`,`level_id`);

--
-- Indexes for table `acc_coa_default`
--
ALTER TABLE `acc_coa_default`
  ADD UNIQUE KEY `HeadCode` (`HeadCode`,`level_id`);

--
-- Indexes for table `acc_customer_income`
--
ALTER TABLE `acc_customer_income`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_glsummarybalance`
--
ALTER TABLE `acc_glsummarybalance`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `appointment_calendar`
--
ALTER TABLE `appointment_calendar`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `attr_options`
--
ALTER TABLE `attr_options`
  ADD PRIMARY KEY (`att_op_id`);

--
-- Indexes for table `attr_options_option_option_tbl`
--
ALTER TABLE `attr_options_option_option_tbl`
  ADD PRIMARY KEY (`att_op_op_op_id`);

--
-- Indexes for table `attr_options_option_tbl`
--
ALTER TABLE `attr_options_option_tbl`
  ADD PRIMARY KEY (`op_op_id`);

--
-- Indexes for table `attr_op_op_op_op_tbl`
--
ALTER TABLE `attr_op_op_op_op_tbl`
  ADD PRIMARY KEY (`att_op_op_op_op_id`);

--
-- Indexes for table `b_acc_coa`
--
ALTER TABLE `b_acc_coa`
  ADD PRIMARY KEY (`row_id`) USING BTREE,
  ADD UNIQUE KEY `row_id` (`HeadName`) USING BTREE;

--
-- Indexes for table `b_acc_transaction`
--
ALTER TABLE `b_acc_transaction`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `b_cost_factor_tbl`
--
ALTER TABLE `b_cost_factor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_level_quatation_attributes`
--
ALTER TABLE `b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_level_quatation_tbl`
--
ALTER TABLE `b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_level_qutation_details`
--
ALTER TABLE `b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_menusetup_tbl`
--
ALTER TABLE `b_menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_menusetup_tbl_default`
--
ALTER TABLE `b_menusetup_tbl_default`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_notification_tbl`
--
ALTER TABLE `b_notification_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_role_permission_tbl`
--
ALTER TABLE `b_role_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_role_tbl`
--
ALTER TABLE `b_role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_to_b_level_quatation_attributes`
--
ALTER TABLE `b_to_b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_to_b_level_quatation_tbl`
--
ALTER TABLE `b_to_b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_user_access_tbl`
--
ALTER TABLE `b_user_access_tbl`
  ADD PRIMARY KEY (`role_acc_id`);

--
-- Indexes for table `b_user_catalog_products`
--
ALTER TABLE `b_user_catalog_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_user_catalog_request`
--
ALTER TABLE `b_user_catalog_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `city_state_tbl`
--
ALTER TABLE `city_state_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_tbl`
--
ALTER TABLE `color_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_profile`
--
ALTER TABLE `company_profile`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `customer_commet_tbl`
--
ALTER TABLE `customer_commet_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_file_tbl`
--
ALTER TABLE `customer_file_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_phone_type_tbl`
--
ALTER TABLE `customer_phone_type_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_query`
--
ALTER TABLE `customer_query`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `custom_sms_tbl`
--
ALTER TABLE `custom_sms_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_card_info`
--
ALTER TABLE `c_card_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_cost_factor_tbl`
--
ALTER TABLE `c_cost_factor_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_level_upcharges`
--
ALTER TABLE `c_level_upcharges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_manufactur`
--
ALTER TABLE `c_manufactur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_manufactur_details`
--
ALTER TABLE `c_manufactur_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `c_menusetup_tbl`
--
ALTER TABLE `c_menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_notification_tbl`
--
ALTER TABLE `c_notification_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_quatation_details`
--
ALTER TABLE `c_quatation_details`
  ADD PRIMARY KEY (`qd_id`);

--
-- Indexes for table `c_quatation_table`
--
ALTER TABLE `c_quatation_table`
  ADD PRIMARY KEY (`qt_id`);

--
-- Indexes for table `c_us_state_tbl`
--
ALTER TABLE `c_us_state_tbl`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_id` (`image_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `gateway_tbl`
--
ALTER TABLE `gateway_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_row_column_price_tbl`
--
ALTER TABLE `group_row_column_price_tbl`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `hana_package`
--
ALTER TABLE `hana_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hana_package_option`
--
ALTER TABLE `hana_package_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iframe_code_tbl`
--
ALTER TABLE `iframe_code_tbl`
  ADD PRIMARY KEY (`frame_id`);

--
-- Indexes for table `log_info`
--
ALTER TABLE `log_info`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `mail_config_tbl`
--
ALTER TABLE `mail_config_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturing_b_level_quatation_attributes`
--
ALTER TABLE `manufacturing_b_level_quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturing_b_level_qutation_details`
--
ALTER TABLE `manufacturing_b_level_qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufactur_data`
--
ALTER TABLE `manufactur_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapping_measurement`
--
ALTER TABLE `mapping_measurement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menusetup_tbl`
--
ALTER TABLE `menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_details`
--
ALTER TABLE `order_return_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_return_tbl`
--
ALTER TABLE `order_return_tbl`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `pattern_model_tbl`
--
ALTER TABLE `pattern_model_tbl`
  ADD PRIMARY KEY (`pattern_model_id`);

--
-- Indexes for table `payment_card_tbl`
--
ALTER TABLE `payment_card_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_check_tbl`
--
ALTER TABLE `payment_check_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `price_chaild_style`
--
ALTER TABLE `price_chaild_style`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_model_mapping_tbl`
--
ALTER TABLE `price_model_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_style`
--
ALTER TABLE `price_style`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `product_attribute`
--
ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option`
--
ALTER TABLE `product_attr_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option_option`
--
ALTER TABLE `product_attr_option_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option_option_option`
--
ALTER TABLE `product_attr_option_option_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_attr_option_option_option_option`
--
ALTER TABLE `product_attr_option_option_option_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_conditions_tbl`
--
ALTER TABLE `product_conditions_tbl`
  ADD PRIMARY KEY (`condition_id`);

--
-- Indexes for table `product_condition_mapping`
--
ALTER TABLE `product_condition_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchase_details_tbl`
--
ALTER TABLE `purchase_details_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_tbl`
--
ALTER TABLE `purchase_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_id` (`purchase_id`);

--
-- Indexes for table `quatation_attributes`
--
ALTER TABLE `quatation_attributes`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_comparison_details`
--
ALTER TABLE `quote_comparison_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `quote_comparison_rooms`
--
ALTER TABLE `quote_comparison_rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `quote_comparison_tbl`
--
ALTER TABLE `quote_comparison_tbl`
  ADD PRIMARY KEY (`quote_id`);

--
-- Indexes for table `qutation_details`
--
ALTER TABLE `qutation_details`
  ADD PRIMARY KEY (`row_id`);

--
-- Indexes for table `raw_material_color_mapping_tbl`
--
ALTER TABLE `raw_material_color_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_material_return_tbl`
--
ALTER TABLE `raw_material_return_tbl`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `role_permission_tbl`
--
ALTER TABLE `role_permission_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `row_column`
--
ALTER TABLE `row_column`
  ADD PRIMARY KEY (`style_id`);

--
-- Indexes for table `row_material_stock_tbl`
--
ALTER TABLE `row_material_stock_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `row_material_tbl`
--
ALTER TABLE `row_material_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment_data`
--
ALTER TABLE `shipment_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_method`
--
ALTER TABLE `shipping_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  ADD PRIMARY KEY (`gateway_id`);

--
-- Indexes for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_raw_material_mapping`
--
ALTER TABLE `supplier_raw_material_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tag_tbl`
--
ALTER TABLE `tag_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tms_payment_setting`
--
ALTER TABLE `tms_payment_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_of_conversion`
--
ALTER TABLE `unit_of_conversion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_of_measurement`
--
ALTER TABLE `unit_of_measurement`
  ADD PRIMARY KEY (`uom_id`);

--
-- Indexes for table `user_access_tbl`
--
ALTER TABLE `user_access_tbl`
  ADD PRIMARY KEY (`role_acc_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `us_state_tbl`
--
ALTER TABLE `us_state_tbl`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `width_height_fractions`
--
ALTER TABLE `width_height_fractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_payments`
--
ALTER TABLE `ws_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accesslog`
--
ALTER TABLE `accesslog`
  MODIFY `sl_no` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=889;
--
-- AUTO_INCREMENT for table `acc_coa`
--
ALTER TABLE `acc_coa`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1712;
--
-- AUTO_INCREMENT for table `acc_customer_income`
--
ALTER TABLE `acc_customer_income`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_glsummarybalance`
--
ALTER TABLE `acc_glsummarybalance`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appointment_calendar`
--
ALTER TABLE `appointment_calendar`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attribute_tbl`
--
ALTER TABLE `attribute_tbl`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `attr_options`
--
ALTER TABLE `attr_options`
  MODIFY `att_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `attr_options_option_option_tbl`
--
ALTER TABLE `attr_options_option_option_tbl`
  MODIFY `att_op_op_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `attr_options_option_tbl`
--
ALTER TABLE `attr_options_option_tbl`
  MODIFY `op_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `attr_op_op_op_op_tbl`
--
ALTER TABLE `attr_op_op_op_op_tbl`
  MODIFY `att_op_op_op_op_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `b_acc_coa`
--
ALTER TABLE `b_acc_coa`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=669;
--
-- AUTO_INCREMENT for table `b_acc_transaction`
--
ALTER TABLE `b_acc_transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_cost_factor_tbl`
--
ALTER TABLE `b_cost_factor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_level_quatation_attributes`
--
ALTER TABLE `b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_level_quatation_tbl`
--
ALTER TABLE `b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `b_level_qutation_details`
--
ALTER TABLE `b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_menusetup_tbl`
--
ALTER TABLE `b_menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=781;
--
-- AUTO_INCREMENT for table `b_menusetup_tbl_default`
--
ALTER TABLE `b_menusetup_tbl_default`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;
--
-- AUTO_INCREMENT for table `b_notification_tbl`
--
ALTER TABLE `b_notification_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `b_role_permission_tbl`
--
ALTER TABLE `b_role_permission_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_role_tbl`
--
ALTER TABLE `b_role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_level_quatation_attributes`
--
ALTER TABLE `b_to_b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_level_quatation_tbl`
--
ALTER TABLE `b_to_b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_level_qutation_details`
--
ALTER TABLE `b_to_b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_to_b_shipment_data`
--
ALTER TABLE `b_to_b_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_user_access_tbl`
--
ALTER TABLE `b_user_access_tbl`
  MODIFY `role_acc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `b_user_catalog_products`
--
ALTER TABLE `b_user_catalog_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `b_user_catalog_request`
--
ALTER TABLE `b_user_catalog_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `city_state_tbl`
--
ALTER TABLE `city_state_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `color_tbl`
--
ALTER TABLE `color_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2545;
--
-- AUTO_INCREMENT for table `company_profile`
--
ALTER TABLE `company_profile`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `customer_commet_tbl`
--
ALTER TABLE `customer_commet_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_file_tbl`
--
ALTER TABLE `customer_file_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer_phone_type_tbl`
--
ALTER TABLE `customer_phone_type_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `customer_query`
--
ALTER TABLE `customer_query`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom_sms_tbl`
--
ALTER TABLE `custom_sms_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `c_card_info`
--
ALTER TABLE `c_card_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `c_cost_factor_tbl`
--
ALTER TABLE `c_cost_factor_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `c_level_upcharges`
--
ALTER TABLE `c_level_upcharges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `c_manufactur`
--
ALTER TABLE `c_manufactur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `c_manufactur_details`
--
ALTER TABLE `c_manufactur_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `c_menusetup_tbl`
--
ALTER TABLE `c_menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT for table `c_notification_tbl`
--
ALTER TABLE `c_notification_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `c_quatation_details`
--
ALTER TABLE `c_quatation_details`
  MODIFY `qd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `c_quatation_table`
--
ALTER TABLE `c_quatation_table`
  MODIFY `qt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `c_us_state_tbl`
--
ALTER TABLE `c_us_state_tbl`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery_image_tbl`
--
ALTER TABLE `gallery_image_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gateway_tbl`
--
ALTER TABLE `gateway_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `group_row_column_price_tbl`
--
ALTER TABLE `group_row_column_price_tbl`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hana_package`
--
ALTER TABLE `hana_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hana_package_option`
--
ALTER TABLE `hana_package_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `iframe_code_tbl`
--
ALTER TABLE `iframe_code_tbl`
  MODIFY `frame_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_info`
--
ALTER TABLE `log_info`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mail_config_tbl`
--
ALTER TABLE `mail_config_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_quatation_attributes`
--
ALTER TABLE `manufacturing_b_level_quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_quatation_tbl`
--
ALTER TABLE `manufacturing_b_level_quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_b_level_qutation_details`
--
ALTER TABLE `manufacturing_b_level_qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufacturing_shipment_data`
--
ALTER TABLE `manufacturing_shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `manufactur_data`
--
ALTER TABLE `manufactur_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mapping_measurement`
--
ALTER TABLE `mapping_measurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menusetup_tbl`
--
ALTER TABLE `menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `order_return_details`
--
ALTER TABLE `order_return_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `order_return_tbl`
--
ALTER TABLE `order_return_tbl`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pattern_model_tbl`
--
ALTER TABLE `pattern_model_tbl`
  MODIFY `pattern_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;
--
-- AUTO_INCREMENT for table `payment_card_tbl`
--
ALTER TABLE `payment_card_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_check_tbl`
--
ALTER TABLE `payment_check_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_tbl`
--
ALTER TABLE `payment_tbl`
  MODIFY `payment_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `price_chaild_style`
--
ALTER TABLE `price_chaild_style`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500;
--
-- AUTO_INCREMENT for table `price_model_mapping_tbl`
--
ALTER TABLE `price_model_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;
--
-- AUTO_INCREMENT for table `price_style`
--
ALTER TABLE `price_style`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4504;
--
-- AUTO_INCREMENT for table `product_attribute`
--
ALTER TABLE `product_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=566;
--
-- AUTO_INCREMENT for table `product_attr_option`
--
ALTER TABLE `product_attr_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1465;
--
-- AUTO_INCREMENT for table `product_attr_option_option`
--
ALTER TABLE `product_attr_option_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1309;
--
-- AUTO_INCREMENT for table `product_attr_option_option_option`
--
ALTER TABLE `product_attr_option_option_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1117;
--
-- AUTO_INCREMENT for table `product_attr_option_option_option_option`
--
ALTER TABLE `product_attr_option_option_option_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `product_conditions_tbl`
--
ALTER TABLE `product_conditions_tbl`
  MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_condition_mapping`
--
ALTER TABLE `product_condition_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `purchase_details_tbl`
--
ALTER TABLE `purchase_details_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_tbl`
--
ALTER TABLE `purchase_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quatation_attributes`
--
ALTER TABLE `quatation_attributes`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quatation_tbl`
--
ALTER TABLE `quatation_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quote_comparison_details`
--
ALTER TABLE `quote_comparison_details`
  MODIFY `row_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote_comparison_rooms`
--
ALTER TABLE `quote_comparison_rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote_comparison_tbl`
--
ALTER TABLE `quote_comparison_tbl`
  MODIFY `quote_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qutation_details`
--
ALTER TABLE `qutation_details`
  MODIFY `row_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `raw_material_color_mapping_tbl`
--
ALTER TABLE `raw_material_color_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `raw_material_return_tbl`
--
ALTER TABLE `raw_material_return_tbl`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_permission_tbl`
--
ALTER TABLE `role_permission_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `row_column`
--
ALTER TABLE `row_column`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `row_material_stock_tbl`
--
ALTER TABLE `row_material_stock_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `row_material_tbl`
--
ALTER TABLE `row_material_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shipment_data`
--
ALTER TABLE `shipment_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shipping_method`
--
ALTER TABLE `shipping_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  MODIFY `gateway_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sqm_price_model_mapping_tbl`
--
ALTER TABLE `sqm_price_model_mapping_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `supplier_raw_material_mapping`
--
ALTER TABLE `supplier_raw_material_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `supplier_tbl`
--
ALTER TABLE `supplier_tbl`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tag_tbl`
--
ALTER TABLE `tag_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tms_payment_setting`
--
ALTER TABLE `tms_payment_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `unit_of_conversion`
--
ALTER TABLE `unit_of_conversion`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unit_of_measurement`
--
ALTER TABLE `unit_of_measurement`
  MODIFY `uom_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_access_tbl`
--
ALTER TABLE `user_access_tbl`
  MODIFY `role_acc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `us_state_tbl`
--
ALTER TABLE `us_state_tbl`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `width_height_fractions`
--
ALTER TABLE `width_height_fractions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ws_payments`
--
ALTER TABLE `ws_payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gallery_tag_tbl`
--
ALTER TABLE `gallery_tag_tbl`
  ADD CONSTRAINT `gallery_tag_tbl_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `gallery_image_tbl` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gallery_tag_tbl_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag_tbl` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
