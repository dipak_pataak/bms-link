ALTER TABLE `user_info` ADD `advanced_trail` TINYINT(0) NOT NULL DEFAULT '0' COMMENT '0:trail pending, 1:trail done' AFTER `sub_domain`;
ALTER TABLE `user_info` ADD `pro_trail` TINYINT NOT NULL DEFAULT '0' COMMENT '0:trail pending, 1:trail done' AFTER `advanced_trail`;
ALTER TABLE `user_info` ADD `package_expire` TIMESTAMP NULL DEFAULT NULL AFTER `pro_trail`;