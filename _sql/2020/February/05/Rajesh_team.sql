-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2020 at 12:16 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmslink`
--

-- --------------------------------------------------------

--
-- Table structure for table `ret_attr_options`
--

CREATE TABLE `ret_attr_options` (
  `retailer_att_op_id` int(11) NOT NULL,
  `att_op_id` int(11) NOT NULL,
  `retailer_price_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=$,2=%',
  `retailer_price` float DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ret_attr_options_option_option_tbl`
--

CREATE TABLE `ret_attr_options_option_option_tbl` (
  `retailer_att_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_id` int(11) NOT NULL,
  `retailer_att_op_op_op_price_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=$,2=%',
  `retailer_att_op_op_op_price` float DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ret_attr_options_option_tbl`
--

CREATE TABLE `ret_attr_options_option_tbl` (
  `retailer_op_op_id` int(11) NOT NULL,
  `op_op_id` int(11) NOT NULL,
  `retailer_att_op_op_price_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=$,2=%',
  `retailer_att_op_op_price` float DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ret_attr_op_op_op_op_tbl`
--

CREATE TABLE `ret_attr_op_op_op_op_tbl` (
  `retailer_att_op_op_op_op_id` int(11) NOT NULL,
  `att_op_op_op_op_id` int(11) NOT NULL,
  `retailer_att_op_op_op_op_price_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=$,2=%',
  `retailer_att_op_op_op_op_price` float DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ret_attr_options`
--
ALTER TABLE `ret_attr_options`
  ADD PRIMARY KEY (`retailer_att_op_id`),
  ADD KEY `att_op_id` (`att_op_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `ret_attr_options_option_option_tbl`
--
ALTER TABLE `ret_attr_options_option_option_tbl`
  ADD PRIMARY KEY (`retailer_att_op_op_op_id`),
  ADD KEY `att_op_op_op_id` (`att_op_op_op_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `ret_attr_options_option_tbl`
--
ALTER TABLE `ret_attr_options_option_tbl`
  ADD PRIMARY KEY (`retailer_op_op_id`),
  ADD KEY `op_op_id` (`op_op_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `ret_attr_op_op_op_op_tbl`
--
ALTER TABLE `ret_attr_op_op_op_op_tbl`
  ADD PRIMARY KEY (`retailer_att_op_op_op_op_id`),
  ADD KEY `att_op_op_op_op_id` (`att_op_op_op_op_id`),
  ADD KEY `created_by` (`created_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ret_attr_options`
--
ALTER TABLE `ret_attr_options`
  MODIFY `retailer_att_op_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ret_attr_options_option_option_tbl`
--
ALTER TABLE `ret_attr_options_option_option_tbl`
  MODIFY `retailer_att_op_op_op_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ret_attr_options_option_tbl`
--
ALTER TABLE `ret_attr_options_option_tbl`
  MODIFY `retailer_op_op_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ret_attr_op_op_op_op_tbl`
--
ALTER TABLE `ret_attr_op_op_op_op_tbl`
  MODIFY `retailer_att_op_op_op_op_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
