CREATE TABLE `b_favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'current login user id',
  `favorite_title` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL COMMENT 'store route',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;