UPDATE `menusetup_tbl` SET `page_url` = 'retailer-new-quotation' WHERE `menusetup_tbl`.`page_url` = 'c_level/quotation_controller/add_new_quotation';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-new-quotation' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/quotation_controller/add_new_quotation';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-manage-quotation' WHERE `menusetup_tbl`.`page_url` = 'c_level/quotation_controller/manage_quotation';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-manage-quotation' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/quotation_controller/manage_quotation';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-manage-upcharges' WHERE `menusetup_tbl`.`page_url` = 'c_level/quotation_controller/manageUpcharges';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-manage-upcharges' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/quotation_controller/manageUpcharges';
