ALTER TABLE `shipping_method` ADD `is_default` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : No | 1 : Yes (1 means create defaul while create user from super admin)' AFTER `status`;

UPDATE shipping_method SET is_default = '1' WHERE method_name IN ('UPS','Customer Pickup','Delivery in Person');