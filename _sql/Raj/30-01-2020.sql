ALTER TABLE `attr_options` ADD `attributes_images` TEXT NULL AFTER `op_condition`;
ALTER TABLE `attr_options_option_tbl` ADD `att_op_op_images` TEXT NULL AFTER `attribute_id`;
ALTER TABLE `attr_options_option_option_tbl` ADD `att_op_op_op_images` TEXT NULL AFTER `att_op_op_op_condition`;