UPDATE `menusetup_tbl` SET `page_url` = 'retailer-payment-log' WHERE `menusetup_tbl`.`page_url` = 'c_level/payment_log/get_log';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-payment-log' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/payment_log/get_log';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-package-my-subscription' WHERE `menusetup_tbl`.`page_url` = 'c_level/package_controller/my_subscription';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-package-my-subscription' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/package_controller/my_subscription';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-password-change' WHERE `menusetup_tbl`.`page_url` = 'c-password-change';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-password-change' WHERE `c_menusetup_tbl`.`page_url` = 'c-password-change';
