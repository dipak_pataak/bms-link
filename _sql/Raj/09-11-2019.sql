UPDATE `menusetup_tbl` SET `page_url` = 'retailer-menu-setup' WHERE `menusetup_tbl`.`page_url` = 'c-menu-setup';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-menu-setup' WHERE `c_menusetup_tbl`.`page_url` = 'c-menu-setup';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer_gallery_image' WHERE `menusetup_tbl`.`page_url` = 'manage_c_gallery_image';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer_gallery_image' WHERE `c_menusetup_tbl`.`page_url` = 'manage_c_gallery_image';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-cost-factor' WHERE `menusetup_tbl`.`page_url` = 'c-cost-factor';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-cost-factor' WHERE `c_menusetup_tbl`.`page_url` = 'c-cost-factor';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-sms' WHERE `menusetup_tbl`.`page_url` = 'c-sms';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-sms' WHERE `c_menusetup_tbl`.`page_url` = 'c-sms';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-us-state' WHERE `menusetup_tbl`.`page_url` = 'c-us-state';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-us-state' WHERE `c_menusetup_tbl`.`page_url` = 'c-us-state';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-email' WHERE `menusetup_tbl`.`page_url` = 'c-email';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-email' WHERE `c_menusetup_tbl`.`page_url` = 'c-email';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-state' WHERE `menusetup_tbl`.`page_url` = 'c-state';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-state' WHERE `c_menusetup_tbl`.`page_url` = 'c-state';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-sms-configure' WHERE `menusetup_tbl`.`page_url` = 'c-sms-configure';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-sms-configure' WHERE `c_menusetup_tbl`.`page_url` = 'c-sms-configure';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-mail-configure' WHERE `menusetup_tbl`.`page_url` = 'c-mail-configure';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-mail-configure' WHERE `c_menusetup_tbl`.`page_url` = 'c-mail-configure';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer_commission_report' WHERE `menusetup_tbl`.`page_url` = 'c_commission_report';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer_commission_report' WHERE `c_menusetup_tbl`.`page_url` = 'c_commission_report';

UPDATE `menusetup_tbl` SET `page_url` = 'retailer-account' WHERE `menusetup_tbl`.`page_url` = 'c-my-account';
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-account' WHERE `c_menusetup_tbl`.`page_url` = 'c-my-account';