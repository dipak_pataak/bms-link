ALTER TABLE `order_return_details` ADD `return_comments` TEXT NULL AFTER `replace_date`;
ALTER TABLE `order_return_details` ADD `return_type` INT NULL DEFAULT '1' COMMENT '1-Replace, 2- Repair' AFTER `return_comments`;
ALTER TABLE `order_return_details`  ADD `return_img` TEXT NULL  AFTER `return_type`;