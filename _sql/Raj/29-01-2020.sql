ALTER TABLE `product_tbl` ADD `product_img` TEXT NULL AFTER `colors`, ADD `is_taxable` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : No | 1 : Yes ' AFTER `product_img`, ADD `product_description` TEXT NULL AFTER `is_taxable`;

ALTER TABLE `color_tbl` ADD `color_img` TEXT NULL AFTER `color_number`;