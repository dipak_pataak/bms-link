UPDATE `b_menusetup_tbl_default` SET `page_url` = 'wholesaler-dashboard' WHERE `b_menusetup_tbl_default`.`page_url` = 'b-level-dashboard';
UPDATE `b_menusetup_tbl` SET `page_url` = 'wholesaler-dashboard' WHERE `b_menusetup_tbl`.`page_url` = 'b-level-dashboard';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'add-wholesaler-customer' WHERE `b_menusetup_tbl_default`.`page_url` = 'add-b-customer';
UPDATE `b_menusetup_tbl` SET `page_url` = 'add-wholesaler-customer' WHERE `b_menusetup_tbl`.`page_url` = 'add-b-customer';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'wholesaler-customer-list' WHERE `b_menusetup_tbl_default`.`page_url` = 'b-customer-list';
UPDATE `b_menusetup_tbl` SET `page_url` = 'wholesaler-customer-list' WHERE `b_menusetup_tbl`.`page_url` = 'b-customer-list';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'wholesaler-order-list' WHERE `b_menusetup_tbl_default`.`page_url` = 'b-order-list';
UPDATE `b_menusetup_tbl` SET `page_url` = 'wholesaler-order-list' WHERE `b_menusetup_tbl`.`page_url` = 'b-order-list';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'catalog-wholesaler-user-order-list' WHERE `b_menusetup_tbl_default`.`page_url` = 'catalog-b-user-order-list';
UPDATE `b_menusetup_tbl` SET `page_url` = 'catalog-wholesaler-user-order-list' WHERE `b_menusetup_tbl`.`page_url` = 'catalog-b-user-order-list';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'add_wholesaler_gallery_image' WHERE `b_menusetup_tbl_default`.`page_url` = 'add_b_gallery_image';
UPDATE `b_menusetup_tbl` SET `page_url` = 'add_wholesaler_gallery_image' WHERE `b_menusetup_tbl`.`page_url` = 'add_b_gallery_image';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'manage_wholesaler_gallery_image' WHERE `b_menusetup_tbl_default`.`page_url` = 'manage_b_gallery_image';
UPDATE `b_menusetup_tbl` SET `page_url` = 'manage_wholesaler_gallery_image' WHERE `b_menusetup_tbl`.`page_url` = 'manage_b_gallery_image';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'wholesaler-customer-return' WHERE `b_menusetup_tbl_default`.`page_url` = 'b-customer-return';
UPDATE `b_menusetup_tbl` SET `page_url` = 'wholesaler-customer-return' WHERE `b_menusetup_tbl`.`page_url` = 'b-customer-return';

UPDATE `b_menusetup_tbl_default` SET `page_url` = 'wholesaler-scan-product' WHERE `b_menusetup_tbl_default`.`page_url` = 'b-level-scan-product';
UPDATE `b_menusetup_tbl` SET `page_url` = 'wholesaler-scan-product' WHERE `b_menusetup_tbl`.`page_url` = 'b-level-scan-product';