/*** Menu change **/
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-credit-card' WHERE `c_menusetup_tbl`.`page_url` = 'c_level/setting_controller/card_info';
UPDATE `menusetup_tbl` SET `page_url` = 'retailer-credit-card' WHERE `menusetup_tbl`.`page_url` = 'c_level/setting_controller/card_info';
UPDATE `menusetup_tbl` set `page_url`='retailer-dashboard' WHERE `page_url` = 'c-level-dashboard';
UPDATE `c_menusetup_tbl` set `page_url`='retailer-dashboard' WHERE `page_url` = 'c-level-dashboard';
UPDATE `menusetup_tbl` set `page_url`='customer-bulk-upload' WHERE `page_url` = 'c-customer-bulk-upload';
UPDATE `c_menusetup_tbl` set `page_url`='customer-bulk-upload' WHERE `page_url` = 'c-customer-bulk-upload';
UPDATE `menusetup_tbl` set `page_url`='purchase-return' WHERE `page_url` = 'c-purchase-return';
UPDATE `c_menusetup_tbl` set `page_url`='purchase-return' WHERE `page_url` = 'c-purchase-return';