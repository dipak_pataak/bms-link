-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 10:30 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `wholesaler_hana_package`
--

CREATE TABLE `wholesaler_hana_package` (
  `id` int(11) NOT NULL,
  `package` varchar(255) DEFAULT NULL,
  `trial_days` int(11) NOT NULL DEFAULT '0',
  `installation_price` double(8,2) NOT NULL DEFAULT '0.00',
  `monthly_price` double(8,2) NOT NULL DEFAULT '0.00',
  `yearly_price` double(8,2) NOT NULL DEFAULT '0.00',
  `monthly_plan_id` varchar(255) DEFAULT NULL,
  `yearly_plan_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wholesaler_hana_package`
--

INSERT INTO `wholesaler_hana_package` (`id`, `package`, `trial_days`, `installation_price`, `monthly_price`, `yearly_price`, `monthly_plan_id`, `yearly_plan_id`) VALUES
(1, 'wholesaler', 20, 1999.00, 199.00, 2388.00, 'wholesaler_monthly', 'wholesaler_yearly'),
(2, 'wholesaler bridge', 30, 3999.00, 225.00, 3060.00, 'wholesaler_bridge_monthly', 'wholesaler_bridge_yearly');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wholesaler_hana_package`
--
ALTER TABLE `wholesaler_hana_package`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wholesaler_hana_package`
--
ALTER TABLE `wholesaler_hana_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
