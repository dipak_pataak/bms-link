-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 10:31 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `wholesaler_package_transaction`
--

CREATE TABLE `wholesaler_package_transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `amount` double(8,2) NOT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0:failed, 1:success',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `coupon` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wholesaler_package_transaction`
--

INSERT INTO `wholesaler_package_transaction` (`id`, `user_id`, `package_id`, `amount`, `transaction_type`, `transaction_id`, `status`, `created_at`, `remark`, `coupon`, `discount`, `card_no`) VALUES
(1, 3, 3, 225.00, 'credit', '', 1, '2019-11-15 08:39:16', 'Package Upgrade By Admin', NULL, NULL, NULL),
(2, 3, 3, 225.00, 'credit', '', 1, '2019-11-15 14:41:52', 'Package Upgrade By Admin', NULL, NULL, NULL),
(3, 3, 3, 225.00, 'credit', '', 1, '2019-11-27 19:10:40', 'Package Upgrade By Admin', NULL, NULL, NULL),
(4, 3, 2, 199.00, 'credit', '', 1, '2019-11-27 19:11:09', 'Package downgrade By Admin', NULL, NULL, NULL),
(5, 3, 3, 225.00, 'credit', '', 1, '2019-11-27 19:12:04', 'Package Upgrade By Admin', NULL, NULL, NULL),
(6, 3, 2, 1999.00, 'credit', '5049466179', 1, '2019-12-04 05:25:15', NULL, '', '', NULL),
(7, 3, 3, 3999.00, 'credit', '5049480516', 1, '2019-12-04 05:45:32', NULL, '', '', NULL),
(8, 3, 2, 1999.00, 'credit', '5049481047', 1, '2019-12-04 05:46:19', NULL, '', '', NULL),
(9, 3, 2, 1999.00, 'credit', '5049496253', 1, '2019-12-04 06:08:35', NULL, '', '', NULL),
(10, 3, 3, 3999.00, 'credit', '5049498186', 1, '2019-12-04 06:11:19', NULL, '', '', NULL),
(11, 3, 3, 3199.20, 'credit', '5049504987', 1, '2019-12-04 06:21:08', NULL, 'DISCOUNT20', '799.8', NULL),
(12, 3, 3, 3999.00, 'credit', '5049505762', 1, '2019-12-04 06:21:39', NULL, '', '', NULL),
(13, 3, 2, 1999.00, 'credit', '5049519405', 1, '2019-12-04 06:33:29', NULL, '', '', NULL),
(14, 3, 2, 1999.00, 'credit', '5049520789', 1, '2019-12-04 06:36:10', NULL, '', '', NULL),
(15, 3, 3, 1611.00, 'credit', '5049521890', 1, '2019-12-04 06:38:12', NULL, '', '', NULL),
(16, 33, 3, 3060.00, 'credit', '', 1, '2019-12-05 19:22:26', 'Package Upgrade By Admin', NULL, NULL, NULL),
(17, 34, 3, 3060.00, 'credit', '', 1, '2019-12-05 19:24:44', 'Package Upgrade By Admin', NULL, NULL, NULL),
(18, 3, 2, 1999.00, 'credit', '5061558251', 1, '2019-12-11 03:20:21', NULL, '', '', '4111-1111-1111-1111'),
(19, 3, 3, 3999.00, 'credit', '5061559389', 1, '2019-12-11 03:22:04', NULL, '', '', '4111-1111-1111-1111'),
(20, 46, 2, 1999.00, 'credit', '5104148959', 0, '2020-01-04 13:56:31', NULL, '', '', '4242-4242-4242-4242'),
(21, 47, 2, 1999.00, 'credit', '5107551765', 0, '2020-01-06 11:07:34', NULL, '', '', '4242-4242-4242-4242'),
(22, 47, 2, 1999.00, 'credit', '5107554542', 1, '2020-01-06 11:10:57', NULL, '', '', '4111-1111-1111-1111'),
(23, 48, 2, 1999.00, 'credit', '5109545159', 1, '2020-01-07 10:25:54', NULL, '', '', '4111-1111-1111-1111'),
(24, 48, 3, 3999.00, 'credit', '5109582336', 1, '2020-01-07 10:47:38', NULL, '', '', '4111-1111-1111-1111'),
(25, 48, 2, 1999.00, 'credit', '5109587806', 1, '2020-01-07 10:51:07', NULL, '', '', '4111-1111-1111-1111'),
(26, 58, 3, 3060.00, 'credit', '', 1, '2020-01-20 10:44:14', 'Package Upgrade By Admin', NULL, NULL, NULL),
(27, 3, 3, 3999.00, 'credit', '5135730871', 1, '2020-01-21 09:54:50', NULL, '', '', '4111-1111-1111-1111'),
(28, 35, 3, 225.00, 'credit', '', 1, '2020-01-23 09:46:27', 'Package Upgrade By Admin', NULL, NULL, NULL),
(29, 2, 2, 3999.00, 'credit', '5139361540', 1, '2020-01-23 10:28:43', NULL, '', '', '4111-1111-1111-1111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wholesaler_package_transaction`
--
ALTER TABLE `wholesaler_package_transaction`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wholesaler_package_transaction`
--
ALTER TABLE `wholesaler_package_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
