-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 10:30 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `wholesaler_package_option`
--

CREATE TABLE `wholesaler_package_option` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `package_key` varchar(255) DEFAULT NULL,
  `basic` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `advanced` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active',
  `pro` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:Not Active, 1Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wholesaler_package_option`
--

INSERT INTO `wholesaler_package_option` (`id`, `title`, `package_key`, `basic`, `advanced`, `pro`) VALUES
(1, 'Wholeseller Connection', 'b_level_package', 1, 1, 1),
(2, 'New Order Form', 'new_order_form', 1, 1, 1),
(3, 'Wholeseller-Retailer Status', 'b_c_status', 1, 1, 1),
(4, 'Accouting (Basic)', 'accounting', 1, 1, 1),
(5, 'Settings (Basic)', 'setting_basic', 1, 1, 1),
(6, 'Gallery (Basic)', 'gallery', 1, 1, 1),
(7, 'Customer (D-level)', 'customer', 0, 1, 1),
(8, 'Multiqoute', 'multiqoute', 0, 1, 1),
(9, 'Appointment', 'appointment', 0, 1, 1),
(10, 'Catalog (View existing product without price + Add new products with price)', 'catalog', 0, 1, 1),
(11, 'Transfer Order and Email', 'transfer_order_an_email', 0, 1, 1),
(12, 'Bulk SMS', 'bulk_sms', 0, 1, 1),
(13, 'Bulk Email', 'bulk_email', 0, 1, 1),
(14, 'Sales Commission', 'sales_commission', 0, 1, 1),
(15, 'Setting Advanced', 'setting_advanced', 0, 1, 1),
(16, 'Production (MFG)', 'production', 0, 0, 1),
(17, 'Inventory', 'inventory', 0, 0, 1),
(18, 'Shipping', 'shipping', 0, 0, 1),
(19, 'Setting (Pro)', 'setting_pro', 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wholesaler_package_option`
--
ALTER TABLE `wholesaler_package_option`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wholesaler_package_option`
--
ALTER TABLE `wholesaler_package_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
