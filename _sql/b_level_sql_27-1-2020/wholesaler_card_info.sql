-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 10:29 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `wholesaler_card_info`
--

CREATE TABLE `wholesaler_card_info` (
  `id` int(11) NOT NULL,
  `card_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_year` int(11) NOT NULL,
  `cvv` int(3) DEFAULT NULL,
  `card_holder_first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_holder_last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_date` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wholesaler_card_info`
--

INSERT INTO `wholesaler_card_info` (`id`, `card_number`, `expiry_month`, `expiry_year`, `cvv`, `card_holder_first_name`, `card_holder_last_name`, `email`, `phone_number`, `address`, `city`, `state`, `zip_code`, `country_code`, `company`, `created_by`, `create_date`, `update_by`, `update_date`, `is_active`) VALUES
(4, '4242-4242-4242-4242', '01', 20, 123, 'TEWRTEWQE', 'T345252', 'demo@testing.com', '+1 222 222 2222', '1004 Ridge Hollow Tr, Irving, TX, USA', 'Irving', 'TX', '75063', 'US', '', 3, 2019, 3, 2019, 0),
(5, '3ejb-weih-fjsh-dah8', '01', 22, 123, 'Testing', 'Test', 'test@gmail.com', '+1 478 287 1258', '100 Federal Street, Boston, MA, USA', 'Boston', 'MA', '02110', 'US', 'Testing', 2, 2019, 3, 2019, 0),
(6, '4111-1111-1111-1111', '01', 21, 123, 'jake', 'wright', 'jake@mailinator.com', '+1 997 745 2125', 'India Gate, Rajpath, India Gate, New Delhi, Delhi, India', 'New Delhi', 'IN', '110001', 'IN', 'ti', 2, 2019, 0, 2020, 1),
(7, '4242-4242-4242-4242', '03', 20, 123, 'Harry', 'Wiliam', 'harry@mailinator.com', '+1 444 346 7898', '300 West 31st Street, New York, NY, USA', 'New York', 'US', '10001', 'US', 'Harry Company', 46, 2020, 0, 0, 1),
(8, '4242-4242-4242-4242', '08', 22, 456, 'Ivan', 'Ivan K Ivan', 'ivan@ivan.com', '+1 567 890 5444', '12333 West Olympic Boulevard, Los Angeles, CA, USA', 'Los Angeles', 'CA', '90064', 'US', '', 36, 2020, 47, 2020, 1),
(9, '4111-1111-1111-1111', '04', 20, 456, 'Ivan', 'Ivan', 'ivan@ivan.com', '+1 678 333 3444', '5665 Peachtree Dunwoody Road, Atlanta, GA, USA', 'Sandy Springs', 'GA', '30342', 'US', '', 47, 2020, 0, 0, 1),
(10, '4111-1111-1111-1111', '06', 20, 123, 'Glen', 'John', 'info@bmsdecor.com', '+1 445 644 4444', '111 Centre Street, New York, NY, USA', 'New York', 'US', '10013', 'US', '', 6, 2020, 0, 0, 1),
(11, '4242-4242-4242-4242', '01', 20, NULL, 'Rajiv', 'Pastula', 'rajiv@pataak.com', '+1 478 287 1258', '1000 Farm to Market 2004, La Marque, TX, USA', 'La Marque', 'TX', '77568', 'US', '', 53, 2020, 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wholesaler_card_info`
--
ALTER TABLE `wholesaler_card_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wholesaler_card_info`
--
ALTER TABLE `wholesaler_card_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
