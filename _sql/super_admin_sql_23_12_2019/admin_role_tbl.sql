-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2019 at 02:53 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_tbl`
--

CREATE TABLE `admin_role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_role_tbl`
--

INSERT INTO `admin_role_tbl` (`id`, `role_name`, `description`, `created_by`, `create_date`, `role_status`) VALUES
(44, 'pawan patidar', '1232', 1, '2019-12-19 10:22:40', 1),
(45, 'p1', 'dsgtdfg', 1, '2019-12-19 10:28:37', 1),
(46, 'p1', 'dsgtdfg', 1, '2019-12-19 10:31:52', 1),
(47, 'p1', 'dsgtdfg', 1, '2019-12-19 10:32:22', 1),
(48, 'p1', 'dsgtdfg', 1, '2019-12-19 10:34:44', 1),
(49, 'pawan ', 'dyhfg', 1, '2019-12-19 10:34:55', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_role_tbl`
--
ALTER TABLE `admin_role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_role_tbl`
--
ALTER TABLE `admin_role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
