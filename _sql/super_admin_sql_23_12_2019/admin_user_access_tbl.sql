-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 23, 2019 at 02:54 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_access_tbl`
--

CREATE TABLE `admin_user_access_tbl` (
  `role_acc_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` varchar(11) CHARACTER SET utf8 NOT NULL,
  `created_by` varchar(11) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_access_tbl`
--

INSERT INTO `admin_user_access_tbl` (`role_acc_id`, `role_id`, `user_id`, `created_by`) VALUES
(1, 3, '32', '1'),
(26, 37, '', '1'),
(27, 38, '', '1'),
(28, 39, '', '1'),
(29, 37, '114', '1'),
(30, 39, '114', '1'),
(35, 38, '115', '1'),
(36, 37, '120', '1'),
(37, 38, '120', '1'),
(39, 49, '135', '1'),
(42, 49, '137', '1'),
(43, 44, '138', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user_access_tbl`
--
ALTER TABLE `admin_user_access_tbl`
  ADD PRIMARY KEY (`role_acc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user_access_tbl`
--
ALTER TABLE `admin_user_access_tbl`
  MODIFY `role_acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
