-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 10, 2019 at 01:21 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmslink1`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa_default`
--

CREATE TABLE `acc_coa_default` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `level_id` int(11) DEFAULT NULL COMMENT 'log_info.user_id',
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa_default`
--

INSERT INTO `acc_coa_default` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `level_id`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
('1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010202', 'Computers & Printers etc', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010401', 'Phones', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010404', 'Internal office Design', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020101', 'Petty Cash', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020102', 'Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102010201', 'Bank Of America', 'Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020103', 'Credit Card', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020104', 'Paypal', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('10202', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('201', 'Share Holders Equity', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('20101', 'Share Capital', 'Share Holders Equity', 2, 1, 1, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('301', 'Business Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('302', 'Other Income', 'Income', 1, 1, 0, 1, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('30203', 'Bank Interest', 'Other Income', 2, 1, 1, 0, 'I', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4', 'Expense', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('402', 'Other Expenses', 'Expense', 1, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40201', 'Travel & Food', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40202', 'Telephone Bill', 'Other Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40203', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40204', 'Software Development Expenses', 'Other Expenses', 2, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40205', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4020501', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('4020502', 'Miscellaneous Benefits', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40206', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('40207', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50101', 'Long Term Loans', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50201', 'Short Term Loans', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5020201', 'Supplier', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL),
('5020301', 'Taxes', 'Liabilities for Expenses', 3, 1, 1, 0, 'L', 0, 0, '0.00', 0, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_coa_default`
--
ALTER TABLE `acc_coa_default`
  ADD UNIQUE KEY `HeadCode` (`HeadCode`,`level_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;