
UPDATE b_menusetup_tbl SET page_url = REPLACE(page_url, 'b-user', 'wholesaler')
UPDATE b_menusetup_tbl SET page_url = REPLACE(page_url, 'b-level', 'wholesaler')
UPDATE c_menusetup_tbl SET page_url = REPLACE(page_url, 'c-level', 'retailer')
UPDATE c_menusetup_tbl SET page_url = REPLACE(page_url, 'c-user', 'retailer')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'c-user', 'retailer')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'c-level', 'retailer')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'b-user', 'wholesaler')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'b-level', 'wholesaler')
UPDATE c_menusetup_tbl SET page_url = REPLACE(page_url, 'c-', 'retailer-')
UPDATE menusetup_tbl SET page_url = REPLACE(page_url, 'c-', 'retailer-')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'c-', 'retailer-')
UPDATE b_menusetup_tbl SET page_url = REPLACE(page_url, 'b-', 'wholesaler-')
UPDATE admin_menusetup_tbl SET page_url = REPLACE(page_url, 'b-', 'wholesaler-')
UPDATE menusetup_tbl SET page_url = REPLACE(page_url, 'b-', 'wholesaler-')
UPDATE menusetup_tbl SET page_url = REPLACE(page_url, 'retailer_gallery_image', 'wholeseller_images') WHERE menu_title ='Gallery'
UPDATE c_menusetup_tbl SET page_url = REPLACE(page_url, 'retailer_gallery_image', 'wholeseller_images') WHERE menu_title ='Gallery'
UPDATE `c_menusetup_tbl` SET `page_url` = 'customer-cost-factor' WHERE `c_menusetup_tbl`.`menu_id` = 30
UPDATE `c_menusetup_tbl` SET `page_url` = 'retailer-cost-factor' WHERE `c_menusetup_tbl`.`menu_id` = 112
UPDATE `menusetup_tbl` SET `page_url` = 'customer-cost-factor' WHERE `menusetup_tbl`.`id` = 30;
UPDATE `menusetup_tbl` SET `page_url` = 'retailer-cost-factor' WHERE `menusetup_tbl`.`id` = 112;
UPDATE `user_info` SET `package_id` = '1'
