-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 25, 2019 at 02:15 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hana_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menusetup_tbl`
--

CREATE TABLE `admin_menusetup_tbl` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `korean_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `page_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `module` varchar(200) CHARACTER SET utf8 NOT NULL,
  `ordering` int(3) DEFAULT NULL,
  `parent_menu` int(11) NOT NULL,
  `menu_type` int(2) NOT NULL COMMENT '1 = left, 2 = system, 3 = top',
  `is_report` tinyint(1) NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(2) NOT NULL,
  `level_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menusetup_tbl`
--

INSERT INTO `admin_menusetup_tbl` (`id`, `menu_id`, `menu_title`, `korean_name`, `page_url`, `module`, `ordering`, `parent_menu`, `menu_type`, `is_report`, `icon`, `status`, `level_id`, `created_by`, `create_date`) VALUES
(1, 0, 'dashboard', 'Dashboard', 'super-admin/dashboard', 'dashboard', 1, 0, 1, 0, NULL, 0, 0, 1, '2019-12-20 04:42:15'),
(12, 0, 'user', 'User', '', 'user', 2, 0, 1, 0, 'this', 1, 0, 1, '2019-12-20 05:49:48'),
(13, 0, 'wholeselar', 'Wholeselar', 'super-admin/b-user/list', 'user', 0, 12, 1, 0, 'flaticon2-user', 1, 0, 1, '2019-12-20 05:51:49'),
(14, 0, 'retailer', 'Retailer', 'super-admin/customer/list', 'user', 0, 12, 1, 0, 'flaticon2-user', 1, 0, 1, '2019-12-20 05:53:42'),
(15, 0, 'package', 'Package', '', 'package', 3, 0, 1, 0, 'this', 1, 0, 1, '2019-12-20 09:07:38'),
(17, 0, 'package list', 'Package', 'super-admin/package/list', 'package', 0, 15, 1, 0, 'this', 1, 0, 1, '2019-12-21 03:17:05'),
(18, 0, 'package  attribute', 'Package Attribute', 'super-admin/package-attribute/list', 'package', 0, 15, 1, 0, 'this', 1, 0, 1, '2019-12-21 03:20:01'),
(19, 0, 'setting', 'Setting', '', 'setting', 4, 0, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:36:59'),
(20, 0, 'tms setting', 'Tms Setting', 'super-admin/tms-payment-setting', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:38:48'),
(21, 0, 'email configration', 'Email Configration', 'super-admin/mail', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:41:38'),
(22, 0, 'sms configration ', 'Sms configration', 'super-admin/sms-configure', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:43:21'),
(23, 0, 'email costom', 'Email Custom', 'super-admin/email', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:44:37'),
(24, 0, 'sms custom', 'sms custom', 'super-admin/sms', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:46:00'),
(25, 0, 'employee', 'Employee', 'super-admin-users', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:47:40'),
(26, 0, 'menu setup', 'menu setup', 'super-admin-menu-setup', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:48:27'),
(27, 0, 'role list', 'Role List', 'super-admin-role-list', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:49:20'),
(28, 0, 'role permission', 'Role-Permission', 'super-admin-role-permission', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:50:52'),
(29, 0, 'access role', 'Access Role', 'super-admin-access-role-list', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:52:29'),
(30, 0, 'employee role', 'Employee Role', 'super-admin-role-assign', 'setting', 0, 19, 1, 0, 'this', 1, 0, 1, '2019-12-21 07:53:28'),
(32, 0, 'coupon', 'coupon', 'super-admin/coupon/list', 'Coupon', 4, 0, 1, 0, 'this', 1, 0, 1, '2019-12-24 08:20:43'),
(33, 0, 'add coupon ', 'Add Coupon', 'super-admin/coupon/list', 'Coupon', 0, 32, 1, 0, 'this', 1, 0, 1, '2019-12-24 08:22:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menusetup_tbl`
--
ALTER TABLE `admin_menusetup_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menusetup_tbl`
--
ALTER TABLE `admin_menusetup_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
