$(document).ready(function () {
    $('.phone').on('keypress', function (e) {
        var key = e.charCode || e.keyCode || 0;
        var phone = $(this);
        if (phone.val().length === 0) {
            phone.val(phone.val() + '+1 (');
        }
        // Auto-format- do not expose the mask as the user begins to type
        if (key !== 8 && key !== 9) {
            //alert("D");
            if (phone.val().length === 6) {
                phone.val(phone.val());
            }
            if (phone.val().length === 7) {
                phone.val(phone.val() + ') ');
            }
            if (phone.val().length === 12) {
                phone.val(phone.val() + '-');
            }
            if (phone.val().length >= 17) {
                phone.val(phone.val().slice(0, 16));
            }
        }
        // Allow numeric (and tab, backspace, delete) keys only
        return (key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    })
            .on('focus', function () {
                phone = $(this);

                if (phone.val().length === 0) {

                    phone.val('+1 (');
                } else {
                    var val = phone.val();
                    phone.val('').val(val); // Ensure cursor remains at the end
                }
            })

            .on('blur', function () {
                $phone = $(this);

                if ($phone.val() === '(') {
                    $phone.val('');
                }
            });

});
////$(document).ready(function(){
//    /***phone number format***/
//    $(".phone").keypress(function (e) {
//
//        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
//            return false;
//        }
//        var curchr = this.value.length;
//
//        var curval =  $(this).val();
//
//        if (curchr == 0 && curval.indexOf("(") <= -1) {
//            $(this).val("+1" +"(" + curval );
//        }
//        else if (curchr == 4 && curval.indexOf("(") <= -1) {
//            $(this).val("+1" + curval + + ")" +  "-");
//        } 
//        else if (curchr == 6 && curval.indexOf("(") > -1) {
//            $(this).val(curval + ")-");
//        } 
//        else if (curchr == 9 && curval.indexOf(")") > -1) {
//            $(this).val(curval + "");
//        } 
//        else if (curchr == 11) {
//            $(this).val(curval + "-");
//            $(this).attr('maxlength', '16');
//        }
//    });
//});   