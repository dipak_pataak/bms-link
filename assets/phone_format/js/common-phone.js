function load_country_dropdown(id, default_country) {

	var input = document.querySelector('#' + id);

	var iti = window.intlTelInput(input, {
		preferredCountries: [default_country],
		autoHideDialCode: false,
		nationalMode: false,
		utilsScript: mybase_url + "assets/phone_format/js/javascript.util.min.js",
	});

	var cleavePhone = new Cleave('#' + id, {
		phone: true,
		phoneRegionCode: default_country
	});

	// listen to the telephone input for changes
	input.addEventListener('countrychange', function (e) {
		var country_code = iti.getSelectedCountryData().iso2;
		var dial_code = iti.getSelectedCountryData().dialCode;

		if ($('#' + id).val() == '') {
			$('#' + id).val('+' + dial_code);
		}
		var cleavePhone = new Cleave('#' + id, {
			phone: true,
			phoneRegionCode: country_code
		});
	});

	$(".iti__country-list li.iti__country").click(function () {
		$(this).parent('ul').parent('.iti__flag-container').siblings('.phone-format').val('');
	});
}