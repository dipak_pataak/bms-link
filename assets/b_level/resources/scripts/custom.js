function init_datatable(id, url, orderable, lengthMenu=[[10, 25, 50, 100], [10, 25, 50, 100]]) {
    
    $('#' + id).DataTable({
        "lengthMenu": lengthMenu,
        "processing": true,
        "serverSide": true,
        "order": [],
        language: { search: "", searchPlaceholder: "Search..." },
        "ajax": {
            "url": url,
            "type": "POST"
        },
        "columnDefs": [{
            "targets": orderable,
            "orderable": false
        }]
    });
}

// Remove into favorite list
function remove_favorite(favorite_id, input) {
    if (favorite_id != '') {
        var page_url = $(input).attr('data-page-url');
        var data = { "favorite_id": favorite_id, "page_url": page_url };
        confirm_popup('remove_fav', data, mathod = "post", url = "b_level/dashboard/remove_favorite");
    }
}


// Add to favorites list
function add_favorite() {
    var page_url = $('#fav_page_url').val();
    var title = $('#fav_title').val();
    if (page_url != '') {
        $.ajax({
            url: mybase_url + "b_level/dashboard/add_favorite",
            type: "post",
            data: { 'page_url': page_url, 'title': title },
            success: function (response) {
                var response = $.parseJSON(response);
                if (response.status == true) {
                    $('.fav_class').addClass('favorites_icon');
                    $('.fav_class').removeClass('notfavorite_icon');
                    $('.fav_class').attr('onclick', 'remove_favorite(' + response.favorite_id + ',this)');
                    load_favorites_sidebar();
                    toastr.success(response.message);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
}

//  Load Favorites sidebar menu
function load_favorites_sidebar() {
    $.ajax({
        url: mybase_url + "b_level/dashboard/load_favorites_sidebar",
        success: function (response) {
            $('#favorite-main-div').html(response);
            var $sortable = $("#favorites-sortable");
            $sortable.sortable({
                stop: function (event, ui) {
                    // parameters will be a string "id[]=1&id[]=3&id[]=10"...etc
                    var parameters = $sortable.sortable("serialize");
                    $.ajax({
                        url: mybase_url + "b_level/dashboard/favorite_menu_sorting",
                        type: "post",
                        data: parameters,
                        success: function (response) {
                            var response = $.parseJSON(response);
                            if (response.status == true) {
                                load_favorites_sidebar();
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
            });;
        }
    });
}

//  Open-close Favorites sidebar menu
function toggle_fav_menu(input) {
    $(input).toggleClass("close");
    $(input).toggleClass("open");
    $('#favorite-main-div').toggleClass('opened');
}

// Common sweet alert
function confirm_popup(action, data, method, url) {
    swal({
        title: "Are you sure?",
        // text: "You will not be able to recover this data!",
        icon: "warning",
        buttons: ['Cancel', 'Remove'],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: mybase_url + url,
                type: method,
                data: data,
                success: function (response) {
                    var response = $.parseJSON(response);
                    if (action == 'remove_fav') {
                        if (response.status == true) {
                            $('.fav_class').attr('onclick', 'add_favorite()');
                            $('.fav_class').addClass('notfavorite_icon');
                            $('.fav_class').removeClass('favorites_icon');
                            load_favorites_sidebar();
                            toastr.success(response.message);
                        } else {
                            toastr.error(response.message);
                        }
                    }
                }
            });
        }
    });
}

function setShipMethod(_this){

    // var method_name = $(_this+' :selected').attr('data-method-name');
    var method_name = $(_this).find(':selected').attr('data-method-name');
    if(method_name == 'Customer Pickup' || method_name =='Delivery in Person'){
        $("#length").removeAttr('required');
        $("#weight").removeAttr('required');
        $("#delivery_date").prop('required',true);
        $("#dp_cp").slideDown();
        $('#up').slideUp();
    }else if(method_name == 'UPS'){
        $("#length").prop('required',true);
        $("#weight").prop('required',true);
        $("#delivery_date").removeAttr('required');
        $("#up").slideDown();
        $('#dp_cp').slideUp();
    }else{
        $("#length").removeAttr('required');
        $("#weight").removeAttr('required');
        $("#delivery_date").removeAttr('required');
        $("#up").slideUp();
        $('#dp_cp').slideUp();
    }

}

// For allow only 2 decimal value : START
function masked_two_decimal(_this,precision=10,decimal=2){
    // var regex = new RegExp("^\\d{0," + precision + "}(\\.\\d{0," + decimal + "})?$");
    // if (!regex.test(_this.value)) {
    //     _this.value = _this.value.substring(0, _this.value.length - 1);
    // }

    var val = _this.value;
    var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
    var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
    if (re.test(val)) {
        //do something here
        console.log('valid');
    } else {
        val = re1.exec(val);
        if (val) {
            _this.value = val[0];
        } else {
            _this.value = "";
        }
    }
}
// For allow only 2 decimal value : END